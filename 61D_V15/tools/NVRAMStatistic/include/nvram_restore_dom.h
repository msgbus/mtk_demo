/* LID system settings */
typedef enum
{
    NVRAM_EF_SMSAL_COMMON_PARAM_LID,       /* (null) */
    NVRAM_EF_CB_DEFAULT_CH_LID,            /* (null) */
    NVRAM_EF_CB_CH_INFO_LID,               /* (null) */
    NVRAM_EF_RAC_PREFERENCE_LID,           /* (null) */
    NVRAM_EF_CAMERA_MAIN_SENSOR_ID_LID,    /* (null) */
    NVRAM_EF_PHB_SPEEDDIAL_LID,            /* (null) */
    NVRAM_EF_CAMERA_SETTING_LID,           /* (null) */
    NVRAM_EF_VIDEO_SETTING_LID,            /* (null) */
    NVRAM_EF_WALLPAPER_FILENAME_LID,       /* (null) */
    NVRAM_EF_SHORTCUTS_LID,                /* (null) */
    NVRAM_EF_EBOOK_SETTINGS_LID,           /* (null) */
    NVRAM_EF_AUDIO_VOICE_CHANGER_LID,      /* (null) */
    NVRAM_EF_SRV_PROF_SETTING_LID,         /* (null) */
    NVRAM_EF_SRV_PROF_EXT_MELODY_LID,      /* (null) */
    NVRAM_LAST_LID_ELEMENT
}LIDDATA;

