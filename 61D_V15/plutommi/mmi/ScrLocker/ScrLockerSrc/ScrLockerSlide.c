/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2010
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
/*****************************************************************************
 *
 * Filename:
 * ---------
 *  ScrLockerSlide.c
 *
 * Project:
 * --------
 *  MAUI
 *
 * Description:
 * ------------
 *  This file implements the fancy screen locker.
 *
 * Author:
 * -------
 * -------
 * -------
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------

 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

/****************************************************************************
 * Include
 ****************************************************************************/

#include "MMI_features.h"

#if defined(__MMI_SLIDE_SCREEN_LOCK__)

#include "MMIDataType.h"
#include "wgui_categories_idlescreen.h"
#include "mmi_rp_app_scr_locker_def.h"
#include "DialerCuiGprot.h"
#include "kal_general_types.h"
#include "kal_public_api.h"
#include "Unicodexdcl.h"
#include "wgui_categories_util.h"
#include "ScrLockerObject.h"
#include "DebugInitDef_Int.h"
#include "mmi_frm_scenario_gprot.h"
#include "GlobalConstants.h"
#include "string.h"
#include "mmi_frm_events_gprot.h"
#include "GlobalResDef.h"
#include "wgui_categories_list.h"
#include "ScrLockerGprot.h"
#include "mmi_frm_input_gprot.h"
#include "ScrLockerSlide.h"
#include "AlertScreen.h"
#include "CommonScreensResDef.h"
#include "ScrLockerFactory.h"

#include "MMI_ap_dcm_config.h"
#ifdef __MMI_AP_DCM_SCRLOCK__
#pragma arm section rodata = "DYNAMIC_CODE_SCRLOCK_RODATA" , code = "DYNAMIC_CODE_SCRLOCK_ROCODE"
#endif
/**********************************************************************
 * Typedef
 **********************************************************************/
 

/**********************************************************************
 * Event Registration
 **********************************************************************/


/****************************************************************************
 * Function
 ****************************************************************************/


/*****************************************************************************
 * FUNCTION
 *  mmi_slk_slide_update_screen
 * DESCRIPTION
 *  This function updates the screen.
 * PARAMETERS
 *  obj             : [IN]          Object
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_slk_slide_update_screen(mmi_slk_obj_struct *obj)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    WCHAR *aligned_wchar_ptr;
    S32 i, num;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    MMI_ASSERT(obj);

    /*
     * Check if the active screen is the fancy lock.
     */
    if (mmi_frm_group_get_active_id() != obj->this_gid ||
        mmi_frm_scrn_get_active_id() != SCR_ID_SLK_MAIN)
    {
        return;
    }

    /*
     * Show the texts in the handset lock screen.
     */
    wgui_slide_update_details();
}


/*****************************************************************************
 * FUNCTION
 *  mmi_slk_slide_leave_proc
 * DESCRIPTION
 *  This function is the leave proc.
 * PARAMETERS
 *  event               : [IN]              Event
 * RETURNS
 *  mmi_ret
 *****************************************************************************/
static mmi_ret mmi_slk_slide_leave_proc(mmi_event_struct *event)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    mmi_ret ret;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    MMI_ASSERT(event);

    switch (event->evt_id)
    {
        case EVT_ID_SCRN_GOBACK_IN_END_KEY:     /* fall-throught */
        case EVT_ID_SCRN_DELETE_REQ_IN_END_KEY:
            ret = -1; /* Do not close me. */
            break;

        default:
            ret = MMI_RET_OK;
            break;
    }

    return ret;
}


/*****************************************************************************
 * FUNCTION
 *  mmi_slk_slide_enter
 * DESCRIPTION
 *  This function enters the fancy screen lock.
 * PARAMETERS
 *  param               : [IN]              Screen parameter
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_slk_slide_enter(mmi_scrn_essential_struct *param)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    mmi_slk_obj_struct *obj;
    MMI_BOOL ret;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    MMI_ASSERT(param && param->user_data);
    obj = (mmi_slk_obj_struct *)param->user_data;

    ret = mmi_frm_scrn_enter(
            param->group_id,
            param->scrn_id,
            NULL,
            (FuncPtr)mmi_slk_slide_enter,
            MMI_FRM_FULL_SCRN);

    if (!ret)
    {
        return;
    }

    mmi_frm_scrn_set_leave_proc(
        param->group_id,
        param->scrn_id,
        mmi_slk_slide_leave_proc);

    wgui_slide_show(
        get_string(STR_ID_SLK_SLIDE_HELP_MSG),
        mmi_scr_locker_close,
        NULL);
    
    /* [MAUI_02634724] 
     * In locked screen,
     * if no digital key: call sos when pressing send key
     * Otherwise, input "112" and then press send key to call sos
     *
     * __MMI_REDUCED_KEYPAD__ : no digital key
     *
     */

    /* Do not allow to shutdown the handset. */
    ClearKeyHandler(KEY_END, KEY_LONG_PRESS);

    mmi_slk_slide_update_screen(obj);
}

static U16 mmi_slk_slide_get_unlock_trigger_key(void)
{
        return KEY_LSK;
}

static U16 mmi_slk_slide_get_unlock_final_key(void)
{
    return KEY_STAR;
}

static mmi_ret mmi_slk_slide_popup_callback(mmi_event_struct *event)
{
    mmi_alert_result_evt_struct *evt = (mmi_alert_result_evt_struct *)event;
    mmi_slk_slide_struct *obj = (mmi_slk_slide_struct *)evt->user_tag;

    switch(evt->evt_id)
    {
        case EVT_ID_ALERT_ENTER:
            obj->is_unlocking = MMI_TRUE;
            break;
        
        case EVT_ID_ALERT_QUIT:
        {
            switch(evt->result)
            {
                case MMI_ALERT_NORMAL_EXIT:
                case MMI_ALERT_INTERRUPT_EXIT:
                    obj->popup_id = 0;
                    obj->is_unlocking = MMI_FALSE;
                    break;
                default:
                    break;
            }
        }
        break;
    }

    return MMI_RET_OK;
}

static void mmi_slk_slide_popup_unlock_me(mmi_slk_slide_struct *obj)
{
    mmi_popup_property_struct arg;
    U16 str_id;

    MMI_ASSERT(obj);

    mmi_popup_property_init(&arg);
    arg.parent_id = mmi_idle_get_group_id();
    arg.callback = mmi_slk_slide_popup_callback;
    arg.user_tag = obj;
    str_id = STR_ID_SLK_KBD_UNLOCK_ME_MSG2;
    obj->popup_id = mmi_popup_display(get_string(str_id), MMI_EVENT_INFO, &arg);

    srv_prof_play_tone(SRV_PROF_TONE_SUCCESS, NULL);
}

static void mmi_slk_slide_popup_help_msg(mmi_slk_slide_struct *obj)
{
    mmi_popup_property_struct arg;

    MMI_ASSERT(obj);

    mmi_popup_property_init(&arg);
    arg.parent_id = mmi_idle_get_group_id();
    mmi_popup_display(get_string(STR_ID_SLK_KBD_HELP_MSG3), MMI_EVENT_INFO, &arg);
}


static mmi_ret mmi_slk_slide_on_key(
    mmi_slk_obj_struct *obj,
    mmi_frm_key_evt_struct *key_evt)
{
    mmi_slk_slide_struct *p;
    mmi_ret ret;

    /*
     * Ask parent.
     */
    ret = mmi_slk_obj_on_key(obj, key_evt);
    if (ret != MMI_RET_OK)
    {
        return ret;
    }

    p = (mmi_slk_slide_struct *)obj;

    /*
     * Continue the key routing for unlocking screen (non-touch project only).
     */
    if ((p->is_unlocking == MMI_TRUE) &&
        (key_evt->key_code == mmi_slk_slide_get_unlock_final_key()))
    {
    	mmi_scr_locker_close();
        return MMI_RET_OK;
    }

    /*
     * Start unlock process or prompt the user to unlock phone.
     */
    if (key_evt->key_type == KEY_EVENT_DOWN)
    {
        if (key_evt->key_code == mmi_slk_slide_get_unlock_trigger_key())
        {
            mmi_slk_slide_popup_unlock_me(p); /* Shall stop key routing. */
        }
        else
        {
            mmi_slk_slide_popup_help_msg(p);  /* Shall stop key routing. */
        }

    }

    return MMI_RET_STOP_KEY_HANDLE;
}

/*****************************************************************************
 * FUNCTION
 *  mmi_slk_slide_on_run
 * DESCRIPTION
 *  This function runs the screen locker.
 * PARAMETERS
 *  obj             : [IN]          Object
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_slk_slide_on_run(mmi_slk_obj_struct *obj)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    mmi_slk_obj_on_run(obj);

    mmi_frm_group_enter(obj->this_gid, MMI_FRM_NODE_SMART_CLOSE_FLAG);

    mmi_frm_scrn_first_enter(
        obj->this_gid,
        SCR_ID_SLK_MAIN,
        (FuncPtr)mmi_slk_slide_enter,
        obj);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_slk_slide_on_init
 * DESCRIPTION
 *  This function initializes the object.
 * PARAMETERS
 *  obj             : [IN]          Object
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_slk_slide_on_init(void *obj)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    mmi_slk_slide_struct *p;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    mmi_slk_obj_on_init((mmi_slk_obj_struct *)obj);

    p = (mmi_slk_slide_struct *)obj;

    /* Member variable. */
    p->type = MMI_SCR_LOCKER_TYPE_SLIDE;
    p->scenario = MMI_SCENARIO_ID_IDLE;

    /* Member function. */
    p->on_run = mmi_slk_slide_on_run;
    p->on_key = mmi_slk_slide_on_key;
}

#ifdef __MMI_AP_DCM_SCRLOCK__
#pragma arm section rodata , code
#endif

#endif /* defined(__MMI_SLIDE_SCREEN_LOCK__) */

