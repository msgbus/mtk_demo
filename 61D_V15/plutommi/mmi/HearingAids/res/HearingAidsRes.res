/* Needed header files of the compile option in XML files, if you need others need to add here */
#include "mmi_features.h"
#include "custresdef.h"

/* Need this line to tell parser that XML start, must after all #include. */
<?xml version="1.0" encoding="UTF-8"?>

/* APP tag, include your app name defined in MMIDataType.h */
<APP id="APP_HEARING_AIDS">

    /* When you use any ID of other module, you need to add
       that header file here, so that Resgen can find the ID */
    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h"/>

    <!-----------------------------------------------------String Resource Area----------------------------------------------------->
    <STRING id="STR_ID_HEARING_AIDS_TITLE"/>
	<STRING id="STR_ID_HEARING_AIDS_HELP"/>
	<STRING id="STR_ID_HEARING_AIDS_PLEASE_PLUG_IN_EARPHONE"/>
#ifdef __MMI_AUDIO_PLAYER__
	<STRING id="STR_ID_HEARING_AIDS_PLEASE_CLOSE_AUDIO_PLAYER"/>
#endif
#ifdef __MMI_FM_RADIO__
	<STRING id="STR_ID_HEARING_AIDS_PLEASE_CLOSE_FM_RADIO"/>
#endif

#ifdef __MMI_HEARING_AIDS__
	<STRING id="STR_ID_SIMPLE_TTS_PLS_CLOSE_BACK_APPLICATION"/>
#endif
    <!-----------------------------------------------------Image Resource Area------------------------------------------------------>

    <!------------------------------------------------------Menu Resource Area------------------------------------------------------>
    <MENUITEM
        id="MENU_ID_HEARING_AIDS_MENUID"
        str="STR_ID_HEARING_AIDS_TITLE"
        highlight="mmi_hearing_aids_menu_highlight" />

    <!------------------------------------------------------Other Resource---------------------------------------------------------->
    <SCREEN id="GRP_ID_HEARING_AIDS"/>
	<SCREEN id="SCR_ID_HEARING_AIDS"/>
    
</APP>
