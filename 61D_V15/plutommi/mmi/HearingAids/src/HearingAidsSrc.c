#include "MMI_features.h"
#include "MMI_include.h"

#include "mmi_rp_app_hearing_aids_def.h"
#include "Aud_main.h"
#include "Gpiosrvgprot.h"
#include "AlertScreen.h"
#include "Mdi_audio.h"

static U8 gMicVolumeBackup; 
U8   entry_hearing_aids_flag=0;
static void mmi_hearing_aids_set_loopback(U32 nStatus, void* pParam)
{
    if (nStatus)
    {
     	L1SP_SetAfeLoopback(KAL_TRUE);
		L1SP_Afe_On(L1SP_AFE_MODE_UL_OPEN | L1SP_AFE_MODE_DL_OPEN);
    }
    else
    {
    	L1SP_Afe_Off();
		L1SP_SetAfeLoopback(KAL_FALSE);
    }
}
kal_bool mmi_hearing_state(void)
{
	if(entry_hearing_aids_flag ==1)
	{
		return KAL_TRUE;
	}
	else
	{
		return KAL_FALSE;
	}
}

static void mmi_hearing_aids_exit(void)
{
   
  //  mdi_audio_set_audio_mode(AUD_MODE_HEADSET);
    L1SP_SetMicrophoneVolume(gMicVolumeBackup);
    L1SP_SetOutputVolume(gMicVolumeBackup,0);
    entry_hearing_aids_flag=0;
    aud_util_proc_in_med(MOD_MMI,
		mmi_hearing_aids_set_loopback,
		MMI_FALSE,
		NULL);
    mdi_audio_resume_background_play();
}

#ifdef __MMI_HEARING_AIDS__
extern MMI_BOOL gIsAudioPause;
#endif
static void mmi_hearing_aids_entry(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    mmi_frm_scrn_close(GRP_ID_ROOT, SCR_ID_HEARING_AIDS);

    if (mmi_frm_scrn_enter(
            GRP_ID_ROOT, 
            SCR_ID_HEARING_AIDS, 
            mmi_hearing_aids_exit, 
            mmi_hearing_aids_entry, 
            MMI_FRM_FULL_SCRN) == MMI_FALSE)
    {
        return ;
    }

    if (!srv_earphone_is_plug_in())
    {
        mmi_popup_display_simple_ext(
            STR_ID_HEARING_AIDS_PLEASE_PLUG_IN_EARPHONE,
            MMI_EVENT_INFO,
            SCR_ID_HEARING_AIDS, 
            NULL);
        return;
    }

    mdi_audio_suspend_background_play();
/*    
	//gary add for bug 624
	{
		extern BOOL mmi_fmrdo_is_power_on(void);
		extern MMI_BOOL mmi_audply_is_play_activated(void);
		#ifdef __MMI_FM_RADIO__
		if(mmi_fmrdo_is_power_on())
		{
			mmi_popup_display_simple_ext(
	            STR_ID_HEARING_AIDS_PLEASE_CLOSE_FM_RADIO,
	            MMI_EVENT_INFO,
	            SCR_ID_HEARING_AIDS, 
	            NULL);
	        return;
		}
		#endif

		#ifdef __MMI_AUDIO_PLAYER__
		if(mmi_audply_is_play_activated())
		{
			mmi_popup_display_simple_ext(
	            STR_ID_HEARING_AIDS_PLEASE_CLOSE_AUDIO_PLAYER,
	            MMI_EVENT_INFO,
	            SCR_ID_HEARING_AIDS, 
	            NULL);
	        return;
		}
		#endif

		#ifdef __MMI_HEARING_AIDS__
		//if(gIsAudioPause)
		if(0) //gary
		{
			gIsAudioPause = MMI_FALSE;
			mmi_popup_display_simple_ext(
	            STR_ID_SIMPLE_TTS_PLS_CLOSE_BACK_APPLICATION,
	            MMI_EVENT_INFO,
	            SCR_ID_HEARING_AIDS, 
	            NULL);
	        return;
		}
		#endif
	}
	//gary .
*/
    gMicVolumeBackup = L1SP_GetMicrophoneVolume();
      entry_hearing_aids_flag=1;
    Media_Stop();
    
    mdi_audio_set_audio_mode(AUD_MODE_HEADSET);
    
    /* open loopback */
    kal_sleep_task(kal_milli_secs_to_ticks(200));
    aud_util_proc_in_med(
        MOD_MMI,
		mmi_hearing_aids_set_loopback,
		MMI_TRUE,
		NULL);
	
    ShowCategory7Screen(
        NULL,
        NULL,
        NULL,
        NULL,
        STR_GLOBAL_EXIT,
        NULL,
        (U8*)GetString(STR_ID_HEARING_AIDS_HELP),
        NULL);
	
    L1SP_SetMicrophoneVolume(255);
    L1SP_SetOutputVolume(255,0);
  //  L1SP_SetInputSource(AUDIO_DEVICE_MIC1);
    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
}

void mmi_hearing_aids_menu_highlight(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    ChangeCenterSoftkey(0, IMG_GLOBAL_COMMON_CSK);

    SetLeftSoftkeyFunction(mmi_hearing_aids_entry, KEY_EVENT_UP);
    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
    SetCenterSoftkeyFunction(mmi_hearing_aids_entry, KEY_EVENT_UP);
}
