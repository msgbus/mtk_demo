#ifdef __ZMAEE_VOICE_DIALER__

#include "SSCStringHandle.h"
#include "FontRes.h"
#include "FontDcl.h"
#include "mdi_datatype.h"
#include "device.h"
#include "app_mem.h"
#include "mdi_audio.h"
#include "ImeGprot.h"
#include "med_utility.h"
#include "IdleAppResDef.h"
#include "zmaee_voice_dialer.h"
#include "nvram_common_defs.h"
#include "mmi_rp_app_mainmenu_def.h"
#include "UcmSrvGprot.h"
#include "ProfilesSrv.h"
#include "GlobalResDef.h"
#include "SettingDefs.h"
#include "ProfilesSrvGprot.h"
#include "mmi_rp_app_zmaee_def.h"
typedef struct {
	zmaee_vd_finish_notify		callback;	
}ZMAEE_VDINFE_CONTEXT;

static ZMAEE_VDINFE_CONTEXT sg_zmaee_vdinfe_cntx = {NULL};


extern void zmaee_vd_broadcast_free_amr_buffer(void);

static
void zmaee_vd_audio_play_callback(mdi_result result, void *user_data)
{
	ZMAEE_MediaResult ret;
	extern unsigned char *p_amr_buff;

	switch(result) {
		case MDI_AUDIO_END_OF_FILE:
		case MDI_AUDIO_TERMINATED:
		case MDI_AUDIO_SUCCESS:
			ret = ZMAEE_MEDIA_COMPLETE;
			break;

		case MDI_AUDIO_DISC_FULL:
			ret = ZMAEE_MEDIA_FULL;
			break;

		default:
			ret = ZMAEE_MEDIA_FAIL;
	}

	zmaee_vd_broadcast_free_amr_buffer();

	if(sg_zmaee_vdinfe_cntx.callback) {
		sg_zmaee_vdinfe_cntx.callback(ret);
	} 
}

#if 0
int zmaee_vd_broadcast_data_amr(unsigned char *amr_data, unsigned int data_len, int volume, zmaee_vd_finish_notify callback)
{
	mdi_result ret;
	sg_zmaee_vdinfe_cntx.callback = callback;
	ret = mdi_audio_play_string_with_vol_path(
									(void*)amr_data, 
									data_len, 
									MDI_FORMAT_PCM_8K, 
									DEVICE_AUDIO_PLAY_ONCE, 
									zmaee_vd_audio_play_callback, 
									NULL, 
									volume, 
									MDI_DEVICE_SPEAKER2);

	if(ret == MDI_AUDIO_SUCCESS) {
		return E_ZM_AEE_SUCCESS;
	} else {
		return E_ZM_AEE_FAILURE;
	}
}
#else
int zmaee_vd_broadcast_data_amr(unsigned char *amr_data, unsigned int data_len, int volume, zmaee_vd_finish_notify callback)
{
	mdi_result ret;
	sg_zmaee_vdinfe_cntx.callback = callback;
	ret = mdi_audio_play_string_with_vol_path(
									(void*)amr_data, 
									data_len, 
									MDI_FORMAT_WAV, 
									DEVICE_AUDIO_PLAY_ONCE, 
									zmaee_vd_audio_play_callback, 
									NULL, 
									volume, 
									MDI_DEVICE_SPEAKER2);

	
	ZMAEE_DebugPrint("zmaee_vd_broadcast_data_amr(): ret = %d", ret);

	if(ret == MDI_AUDIO_SUCCESS) {
		return E_ZM_AEE_SUCCESS;
	} else {
		return E_ZM_AEE_FAILURE;
	}
}

#endif
int zmaee_keycode(int pen_code)
{
	int ret = MMI_IMC_VK_DIALER_PEN_MAX;
	
	switch(pen_code)
	{
		case MMI_IMC_VK_DIALER_PEN_0:
			ret = ZMAEE_KEY_NUM0;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_1:
			ret = ZMAEE_KEY_NUM1;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_2:
			ret = ZMAEE_KEY_NUM2;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_3:
			ret = ZMAEE_KEY_NUM3;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_4:
			ret = ZMAEE_KEY_NUM4;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_5:	
			ret = ZMAEE_KEY_NUM5;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_6:	
			ret = ZMAEE_KEY_NUM6;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_7:	
			ret = ZMAEE_KEY_NUM7;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_8:
			ret = ZMAEE_KEY_NUM8;
			break;
			
		case MMI_IMC_VK_DIALER_PEN_9:	
			ret = ZMAEE_KEY_NUM9;
			break;

		case MMI_IMC_VK_DIALER_PEN_STAR:
		case KEY_STAR:
			ret = ZMAEE_KEY_STAR;
			break;
			
   		case MMI_IMC_VK_DIALER_PEN_POUND:
		case KEY_POUND:
			ret = ZMAEE_KEY_HASH;
			break;
			
		default:
			break;
	}
	
	return ret;
}


/**
 * 停止播报
 */
void zmaee_vd_stop_broadcast(void)
{
	mdi_audio_stop_string();
}


/**
 * 内存分配
 */
void* zmaee_vd_malloc(unsigned int size)
{
	return (void*)med_alloc_ext_mem_cacheable(size);
}

void zmaee_vd_free(void *ptr)
{
	if(ptr) {
		med_free_ext_mem(&ptr);
	}
}


/**
 * 读写NVRAM
*/
int zmaee_vd_read_nvram(void)
{	
	int nvram_value = 0;
	short err_code;
	int ret;

	ret = ReadRecord(NVRAM_EF_ZMAEE_VD_CONFIG_LID, 1, &nvram_value, sizeof(nvram_value), &err_code);

	ZMAEE_DebugPrint("zmaee_vd_read_nvram(): ret = %d", ret);

	if((ret != sizeof(nvram_value)) || (err_code != NVRAM_READ_SUCCESS)) {
		nvram_value = E_ZM_AEE_TRUE;
	}

	return nvram_value;	
}

 void zmaee_vd_write_nvram(int value)
{
	int nvram_value = value;
	S16 err_code;
	int ret;
	
	ret = WriteRecord(NVRAM_EF_ZMAEE_VD_CONFIG_LID, 1, &nvram_value, sizeof(nvram_value), &err_code);	

	ZMAEE_DebugPrint("zmaee_vd_write_nvram(): ret = %d", ret);

	if((ret != sizeof(nvram_value)) || (err_code != NVRAM_WRITE_SUCCESS)) {
		WriteRecord(NVRAM_EF_ZMAEE_VD_CONFIG_LID, 1, &nvram_value, sizeof(nvram_value), &err_code);
	}
}


/**
 * 辅助函数
 */
void* zmaee_vd_memcpy(void* dest, void* str, int size_t)
{
	memcpy(dest, str, size_t);
}


void* zmaee_vd_memset(void* buf, int c, unsigned int size_t)
{
	memset(buf, c, size_t);
}

S32 glb_zmaee_yybh_cur_sel = 0;

/**
 * 智能语音使能控制(包含情景模式)
 */
int zmaee_condition_call_is_available ( void )
{
	int available = 1;
	if ( srv_ucm_query_call_count(SRV_UCM_CALL_STATE_ALL, SRV_UCM_CALL_TYPE_ALL, NULL) > 0 )  
	{
		available = 0;
	}
	return available;
}
 

int zmaee_condition_screen_is_available ( void )
{
	U16 screen_id = 1;
	if ( (mmi_frm_group_get_active_id() ==7)|| (zmaee_condition_call_is_available()==0) )
	{
		return 0;
	}
	return screen_id;
}


int ZMAEE_Prof(void)
{
	extern srv_prof_cntx_struct g_srv_prof;

	ZMAEE_DebugPrint("ZMAEE_Prof(): cur_prof = %d", g_srv_prof.cur_prof);

	if(g_srv_prof.cur_prof == 2)
		return TRUE;
	else
		return FALSE;
}


/**
 * 智能语音的菜单实现
 */

void ZMAEE_setup_YYBH_hilitehandler(S32 index)
{
	glb_zmaee_yybh_cur_sel = index;
}


void zmaee_yybh_setup_lskhandler(void)
{
	zmaee_vd_enable_voice(glb_zmaee_yybh_cur_sel);
	GoBackHistory();
	DisplayPopup((PU8) GetString(STR_GLOBAL_DONE), IMG_GLOBAL_ACTIVATED, 1, 1000, SUCCESS_TONE);
}


static void zmaee_yybb_setup(void)
{
	U8* pGuiBuffer;

	glb_zmaee_yybh_cur_sel=zmaee_vd_is_enable();
#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_SC_YYBB_SETUP_ID, NULL, zmaee_yybb_setup, NULL);
	pGuiBuffer = GetCurrGuiBuffer(SCR_SC_YYBB_SETUP_ID);
#else
	mmi_frm_scrn_enter(GRP_ID_ROOT, SCR_SC_YYBB_SETUP_ID, NULL, zmaee_yybb_setup, MMI_FRM_FULL_SCRN);
	pGuiBuffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_SC_YYBB_SETUP_ID);
#endif


	subMenuDataPtrs[0] = (PU8)GetString(STR_GLOBAL_OFF);
	subMenuDataPtrs[1] = (PU8)GetString(STR_GLOBAL_ON);

	RegisterHighlightHandler(ZMAEE_setup_YYBH_hilitehandler);
	
	ShowCategory36Screen( STR_ZMAEE_VOICE_DIALER, NULL,
						  STR_GLOBAL_OK, IMG_GLOBAL_OK,
						  STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
						  2, (U8**)subMenuDataPtrs, glb_zmaee_yybh_cur_sel, pGuiBuffer);


	SetKeyHandler(zmaee_yybh_setup_lskhandler, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(zmaee_yybh_setup_lskhandler, KEY_ENTER, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(zmaee_yybh_setup_lskhandler, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_VOICE_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(zmaee_yybb_setup, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(zmaee_yybb_setup, KEY_ENTER, KEY_EVENT_UP);
	
	SetLeftSoftkeyFunction(zmaee_yybb_setup, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

#ifdef WIN32
 void zmaee_vd_broadcast_numstr(unsigned char *num, int len, zmaee_vd_finish_notify notify)
{}
 void zmaee_vd_enable_voice(int enable)
{}
 void zmaee_vd_broadcast_keyval(ZMAEE_KeyCode key_value)
{}
  void zmaee_vd_broadcast_free_amr_buffer(void)
 {}
  int zmaee_vd_is_enable(void)
{
	return 0;
}
#endif
#endif	// __ZMAEE_VOICE_DIALER__
