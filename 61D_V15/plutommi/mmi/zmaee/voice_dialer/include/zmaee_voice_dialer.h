#ifndef __ZMAEE_VOICE_DIALER_H__
#define __ZMAEE_VOICE_DIALER_H__

#include "zmaee_typedef.h"



typedef void (*zmaee_vd_broadcast_callback)(ZMAEE_MediaResult result);

/**
 * 播报长号码的结束通知函数
 * @ result		E_ZM_AEE_SUCCESS		成功
 * 				E_ZM_AEE_FAILURE		失败
 */
typedef void (*zmaee_vd_finish_notify)(int result);

/**
 * 使能语音播报
 * @ enable			E_ZM_AEE_TRUE	使能
 * 					E_ZM_AEE_FALSE	禁止
 */
extern void zmaee_vd_enable_voice(int enable);


/**
 * 播报一个按键值
 * @ key_value		播报的键值
 */
extern void zmaee_vd_broadcast_keyval(ZMAEE_KeyCode key_value);


/**
 * 播报一段长号码
 * @ num			UTF8的号码字符串
 * @ wcsLen			号码字符串的字符长度
 * @ notify			号码播报结束的通知函数
 */
extern void zmaee_vd_broadcast_numstr(unsigned char *num, int len, zmaee_vd_finish_notify notify);


/**
 * 停止播报
 */
extern void zmaee_vd_stop_broadcast(void);


/**
 * 设置播报的音量
 * @ volume			0 - 7，音量
 */
extern void zmaee_vd_set_broadcast_volume(int volume);


/**
 * 获取播报的音量
 * RETURN:
 * 0 - 7的音量
 */
extern int zmaee_vd_get_broadcast_volume(void);


#endif	// __ZMAEE_VOICE_DIALER_H__
