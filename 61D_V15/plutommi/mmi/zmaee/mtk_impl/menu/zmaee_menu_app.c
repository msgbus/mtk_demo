#ifdef __ZMAEE_APP__

#include "zmaee_menu_helper.h"
#include "mmi_rp_app_zmaee_def.h"

#include "mmi_rp_app_mainmenu_def.h"

extern char *GetString(unsigned short StringId);

/*********************************************/
/* 主菜单UI内容							    */
/*********************************************/
const ZMAEE_MAIN_MENU_ITEM c_zmaee_main_menu_ui_contents[] = {
	{STR_ZMAEE_MAIN_MENU_GAMECENTER, 0},			/* 游戏中心 */
	{STR_ZMAEE_MAIN_MENU_QQ, 0},					/* 手机QQ */
};

/**
 * 获取主菜单标题字符串ID
 */
int zmaee_main_menu_get_title_str_id(void)
{
	return MAIN_MENU_FUNANDGAMES_TEXT;
}

/**
 * 获取主菜单菜单项UI内容
 */
ZMAEE_MAIN_MENU_ITEM* zmaee_main_menu_get_items_ui_contonts(void)
{
	return (ZMAEE_MAIN_MENU_ITEM*)c_zmaee_main_menu_ui_contents;
}

/**
 * 获取字符串
 */
char* zmaee_main_menu_get_string(int str_id)
{
	return GetString(str_id);
}

#endif
