
#define ZMAEE_SYSTEM_DECODE_RAM_SIZE	(130 * 1024)
#define ZMAEE_PLUGIN_RAM_SIZE			(20  * 1024)
#define ZMAEE_APPMEM_SIZE				(52  * 1024)

#if defined (__MMI_MAINLCD_240X320__) || defined (__MMI_MAINLCD_320X240__) || defined (__MMI_MAINLCD_240X400__) || defined (__MMI_MAINLCD_400X240__) || defined (__MMI_MAINLCD_176X220__) || defined (__MMI_MAINLCD_220X176__)
#define ZMAEE_APPRAM_SIZE				(600 * 1024)
#define ZMAEE_THEME_RAM_SIZE			(90 * 1024)
#elif defined (__MMI_MAINLCD_320X480__) || defined (__MMI_MAINLCD_480X320__) || defined (__MMI_MAINLCD_360X600__) || defined (__MMI_MAINLCD_600X360__) || defined (__MMI_MAINLCD_360X640__) || defined (__MMI_MAINLCD_640X360__)
#define ZMAEE_APPRAM_SIZE				(2048 * 1024)
#define ZMAEE_THEME_RAM_SIZE			(200 * 1024)
#elif defined (__MMI_MAINLCD_480X800__) || defined (__MMI_MAINLCD_800X480__)
#define ZMAEE_APPRAM_SIZE				(4096 * 1024)
#define ZMAEE_THEME_RAM_SIZE			(500 * 1024)
#elif defined (__ARM9_MMU__) || defined (__ARM11_MMU__)
#define ZMAEE_APPRAM_SIZE				(2048 * 1024)
#define ZMAEE_THEME_RAM_SIZE			(200 * 1024)
#else
#error "Please modify ram size"
#endif

#ifdef __ZMAEE_APP_DESKTOP__
#ifndef ZMAEE_DESKTOP_RAM_SIZE
#if 0	// ֧�ֿ�����ֽ����#if 1
#if defined (__MMI_MAINLCD_240X320__)
#define ZMAEE_DESKTOP_RAM_SIZE		(650 * 1024)
#elif defined (__MMI_MAINLCD_240X400__)
#define ZMAEE_DESKTOP_RAM_SIZE		(700 * 1024)
#elif defined (__MMI_MAINLCD_320X480__)
#define ZMAEE_DESKTOP_RAM_SIZE		(1020 * 1024)
#endif
#else
#if defined (__MMI_MAINLCD_240X320__)
#define ZMAEE_DESKTOP_RAM_SIZE		(250 * 1024)
#elif defined (__MMI_MAINLCD_240X400__)
#define ZMAEE_DESKTOP_RAM_SIZE		(300 * 1024)
#elif defined (__MMI_MAINLCD_320X480__)
#define ZMAEE_DESKTOP_RAM_SIZE		(400 * 1024)
#endif
#endif
#endif
#endif


#if defined __ZMAEE_CODE_IN_MEDMEM_H__

#ifdef __ZMAEE_APP_THEME__
	kal_uint8 MED_EXT_ZMAEE[MED_MEM_CONC3_SIZE(ZMAEE_APPRAM_SIZE, ZMAEE_THEME_RAM_SIZE, ZMAEE_SYSTEM_DECODE_RAM_SIZE)];
#else // __ZMAEE_APP_THEME
	kal_uint8 MED_EXT_ZMAEE[MED_MEM_CONC2_SIZE(ZMAEE_APPRAM_SIZE, ZMAEE_SYSTEM_DECODE_RAM_SIZE)];
#endif	// __ZMAEE_APP_THEME

#if defined (__ZMAEE_APP_DESKTOP__) && defined (__ZMAEE_APP_DWALLPAPER__)
#ifdef __ZMAEE_APP_THEME__
	kal_uint8 MED_EXT_ZMAEE_DESKTOP[MED_MEM_CONC4_SIZE(ZMAEE_APPRAM_SIZE, ZMAEE_THEME_RAM_SIZE, ZMAEE_SYSTEM_DECODE_RAM_SIZE, ZMAEE_DESKTOP_RAM_SIZE)];
#else
	kal_uint8 MED_EXT_ZMAEE_DESKTOP[MED_MEM_CONC3_SIZE(ZMAEE_APPRAM_SIZE, ZMAEE_SYSTEM_DECODE_RAM_SIZE, ZMAEE_DESKTOP_RAM_SIZE)];
#endif
#endif

#endif	// __ZMAEE_CODE_IN_MEDMEM_H__

