#ifndef __ZMAEE_PRIV_H_
#define __ZMAEE_PRIV_H_

#ifndef zm_extern
#ifdef __cplusplus
#define zm_extern extern "C"
#else
#define zm_extern extern
#endif
#endif//zm_extern

#include "zmaee_typedef.h"

/**
 * mtk平台版本定义
 */
#define ZM_AEE_MTK_SOFTVERN				0x1303

/**
 * ram大小定义
 */
#include "zmaee_mem_define.h"

#define ZMAEE_WORK_ROOT_DIR				":\\zmaee\\"
#define ZMAEE_ENTRY_WORKDIR				":\\zmaee\\"
#define ZMAEE_ENTRY_THEMEDIR			":\\zmaee\\theme\\"
#define ZMAEE_ENTRY_APP					":\\zmaee\\app\\"
#define ZMAEE_ENTRY_GAME				":\\zmaee\\game\\"


#define ZMAEE_KEYLOCK_CLSID				0x002
#define ZMAEE_DWPAPER_CLSID				0x003
#define ZMAEE_THEME_CLSID				0x004

#ifdef __ZMAEE_APP_DESKTOP__
#define ZMAEE_DESKTOP_APP_CLSID 					0x00000009
#endif


/**
 * 是否使用库中的MD5接口
 */
//#define ZMAEE_MD5_INLIB

/**
 * 是否使用库中的DES接口
 */
#define ZMAEE_DES_INLIB

/**
 * 操作系统
 */
#define ZMAEE_OS_NAME					"MTK60D"

/**
 * 移植版本的工程名字，可参考*.mak文件中的*，
 * 为ascii字符串，比如"M77A"
 * 0xFF - unknown
 */
#define ZMAEE_MODEL_NAME				"PY_F168"

/**
 * 是否有数字键盘或者全键盘, 0xFF is unknown
 * 值参考e_ZMAEE_HW_KEYPAD_TYPE宏定义，可以将多个枚举与起来
 * 比如 (ZMAEE_HW_KEYPAD_ARROW | ZMAEE_HW_KEYPAD_OK)
 * 如果为仿苹果的全触屏手机，即只有一个挂机键的，则写0
 */
#define ZMAEE_KEYPAD_TYPE				(ZMAEE_HW_KEYPAD_NORMAL)

/**
 * 是否支持触摸屏
 * 0xFF - unknown
 * 0 - 不支持触摸屏
 * 1 - 支持触摸屏
 */
#define ZMAEE_SUPPORT_TOUCHSCREEN		(1)

/**
 * 是否支持横竖屏
 * 0xFF - unknown
 * 0 - 不支持横竖屏
 * 1 - 支持横竖屏
 */
#define ZMAEE_SUPPORT_ROTATE			(0)

/**
 * 是否支持GPS导航
 * 0xFF - unknown
 * 0 - 不支持GPS导航
 * 1 - 支持GPS导航
 */
#define ZMAEE_SUPPORT_GPS				(0)

/**
 * 是否支持重力感应器
 * 0xFF - unknown
 * 0 - 不支持重力感应器
 * 1 - 支持重力感应器
 */
#define ZMAEE_SUPPORT_GSENSOR			(0)

/**
 * 应用和游戏的菜单入口是否分开
 * 0xFF - unknown
 * 0 - 应用和游戏合并成一个入口
 * 1 - 应用和游戏分成两个入口
 */
#define ZMAEE_SUPPORT_SEPARE_APPANDGAME	(0)

/**
 * Period Timer是否使用定时器
 */
//#define ZMAEE_PERIOD_USE_TIMER

#ifdef ZMAEE_PERIOD_USE_TIMER
/**
 * Period Handler的时间片，单位(ms)
 */
#define ZMAEE_PERIOD_TIME_SLICE			50

#endif	// ZMAEE_PERIOD_USE_TIMER

/**
 * FAE的名字拼音，中间不要加空格
 * 用字符串，例如“LongYun”
 */
#define ZMAEE_FAE_NAME					"MaJinKang"

/**
 * FAE最近一次更新时间
 * 格式为YYYYMMDD，年月日，为数字，
 * 例如2012年12月9日，则填写20121209
 */
#define ZMAEE_FAE_BUILD_DATE			20140708
 

/**
 * 挂机程序是否使用APPMEM
 */
//#define ZMAEE_GDLL_USE_APPMEM

/**
 * 进入平台之前是否要关闭主题
 * 定义了则在进入平台时关闭主题，没有定义则进入平台时不关闭主题
 */
//#define ZMAEE_CLOSE_THEME_BEFORE_ENTRY

/**
 * 未插T卡是否不允许运行APP
 * 定义了则未插入T卡时不允许运行APP
 * 未定义则未插入T卡也可以运行APP
 */
//#define ZMAEE_CANNOT_ENTRY_WITHOUT_TCARD

/**
 * 打开此宏，再加上gdi_image_png.c的移植步骤，可以将系统的PNG解码替换成我们自己的PNG解码，
 * 我们自己的PNG解码需要大概50K左右的多媒体内存
 */
//#define ZMAEE_SUPPORT_PNG_DECODE

#ifdef __ZMAEE_APP_DESKTOP__
/**
 * 是否使用客户定制的壁纸
 */
//#define ZMAEE_DESKTOP_USE_CUSTOM_MAINMENU_WALLPAPER
#endif

/**
 * APP加载是否使用Cache内存，如果使用Cache内存则会提高APP的运行速度
 * 打开此宏，表示使用Cache内存
 * 关闭此宏，表示不使用Cache内存
 */
#ifdef __MMI_MEM_USE_ASM__//ZMAEEMODIFY
#define ZMAEE_USE_CACHE_MEMORY
#else
#define ZMAEE_USE_CACHE_MEMORY
#endif

// 支持多个号码拨号
#define ZMAEE_MAKEVOICE_WITH_NUMLIST

/**
 * 默认的UI类clsId
 */
// 默认的锁屏clsId
#define ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID		0

// 默认的动态壁纸clsId
#define ZMAEE_DEFAULT_DWALLPAPER_CLSID		0

// 默认的主题clsId
#define ZMAEE_DEFAULT_THEME_CLSID			0

/**
 * GPRS profile 位置定义
 */
#define ZMAEE_GPRS_WAP_ACCOUNTID		(custom_get_csd_profile_num() + custom_get_gprs_profile_num()-2)
#define ZMAEE_GPRS_NET_ACCOUNTID		(custom_get_csd_profile_num() + custom_get_gprs_profile_num()-1)
#define ZMAEE_GPRS_WAP_PROFILEID		(custom_get_gprs_profile_num() -1)
#define ZMAEE_GPRS_NET_PROFILEID		(custom_get_gprs_profile_num())

typedef int	AEE_IBitmap;
typedef int AEE_IDisplay;
typedef int AEE_IFile;
typedef int AEE_IFileMgr;
typedef int AEE_IImage;
typedef int AEE_IMedia;
typedef int AEE_ISocket;
typedef int	AEE_IHttp;
typedef int	AEE_INetMgr;
typedef int AEE_ISetting;
typedef int AEE_IShell;
typedef int AEE_ITAPI;
typedef int AEE_IAddrBook;
typedef int AEE_IApplet;
typedef int AEE_IZip;
typedef int AEE_IAStream;
typedef int AEE_IGps;
typedef int AEE_IGSensor;
typedef int AEE_ITheme;
typedef int AEE_IMemStream;
typedef int AEE_IStatusBar;
typedef int AEE_IMessage;
typedef int AEE_IDesktop;
///////////// fot mms //////////////
typedef int	AEE_IMMS;

zm_extern void	ZMAEE_Init(void);
zm_extern void* ZMAEE_Malloc(int size);
zm_extern void	ZMAEE_Free(void* p);
zm_extern void* ZMAEE_CreateVtable(ZM_AEECLSID clsId, int nCount, ...);
zm_extern void* ZMAEE_GetVtable(ZM_AEECLSID clsId);
zm_extern int	ZMAEE_GetRamSize(void);
zm_extern void* ZMAEE_MallocRam(void);
zm_extern void  ZMAEE_FreeRam(void *p);
zm_extern int 	ZMAEE_GetPluginRamSize(void);
zm_extern void* ZMAEE_PlugInMallocRam(void);
zm_extern void  ZMAEE_PlugInFreeRam(void* p);
zm_extern void 	ZMAEE_BackToIdle(void);
zm_extern void 	ZMAEE_Exit(void);
zm_extern int 	ZMAEE_Platform_HandleEvent(ZMAEE_AppletEvent ev, unsigned long wParam, unsigned long lParam);
zm_extern void 	ZMAEE_Callback_Init(ZMAEE_CALLBACK* pCb, void* pfn, void* pUser);
zm_extern int   ZMAEE_Callback_Valid(ZMAEE_CALLBACK* pCb);
zm_extern int	ZMAEE_INetMgr_EventHandle(void* inMsg);
zm_extern int 	ZMAEE_QQ_IsRunning(void);
zm_extern void 	ZMAEE_QQ_Exit(void);


/**
 * 菜单入口函数
 */
zm_extern void ZMAEE_Main_Highlight(void);

#endif//__ZMAEE_PRIV_H_
