#ifndef __ZMAEE_MENU_HELPER_H__
#define __ZMAEE_MENU_HELPER_H__

#include "zmaee_typedef.h"

// 菜单高亮函数原型
typedef void (*ZMAEE_MENU_HIGHLIGHT_FUNC)(int highlight_index);

// 按键响应函数原型
typedef void (*ZMAEE_MENU_KEY_ENTRY)(void);

// 自定义时间的通知函数
typedef void (*ZMAEE_MENU_CUSTOM_TIME_NOTIFY)(int start_hour, int end_hour);

// 列表菜单
#define ZMAEE_MENU_LISTMENU_MAX_COUNT		20

typedef struct {
	int					menu_count;
	int					menu_title_str_id;
	
	unsigned short		menu_item_name[ZMAEE_MENU_LISTMENU_MAX_COUNT][AEE_MAX_APPLET_NAME];
	ZMAEE_PFNFUNCEX		menu_item_func[ZMAEE_MENU_LISTMENU_MAX_COUNT];
	void*				menu_item_data[ZMAEE_MENU_LISTMENU_MAX_COUNT];
	ZMAEE_PFNFUNCEX		menu_exit_func;
}ZMAEE_MENU_LISTMENU;

typedef struct {
	unsigned int		str_id;		/* 菜单项名称 */
	unsigned int		img_id;		/*  */
}ZMAEE_MAIN_MENU_ITEM;


/**
 * 返回上一层菜单
 */
void ZMAEE_Menu_GoBackHistory();

/**
 * 显示"完成"的Pop框
 */
void ZMAEE_Menu_DisplayPop_Done();

/**
 * 初始化单选菜单页面的显示数据
 * title_str_id				标题文字资源ID
 * menuitem_str_ids		菜单项的内容文字资源ID
 * menuitem_count		菜单项的个数
 * highlight_func			高亮函数
 * left_softkey_pre_func	左软键预处理函数，调用完之后，还会调用GoBackHistory和DisplayPopup提示完成
 * right_softkey_pre_func	右软键预处理函数，调用完之后，还会调用GoBackHistory
 */
void ZMAEE_Menu_InitCheckListMenu(int title_str_id,	
										int menuitem_str_ids[], int menuitem_count,
										int highlight_index,
										ZMAEE_MENU_HIGHLIGHT_FUNC highlight_func,
										ZMAEE_MENU_KEY_ENTRY left_softkey_pre_func, 
										ZMAEE_MENU_KEY_ENTRY right_softkey_pre_func);

/**
 * 修改高亮函数
 */
void ZMAEE_Menu_SetCheckListHighlight(int highlight_index);

/**
 * 显示单选菜单页面
 */
void ZMAEE_Menu_ShowCheckList();

/**
 * 初始化开关页面的显示数据，索引0为OFF，索引1为ON
 * title_str_id				标题文字资源ID
 * highlight_func			高亮函数
 * left_softkey_pre_func	左软键预处理函数，调用完之后，还会调用GoBackHistory和DisplayPopup提示完成
 * right_softkey_pre_func	右软键预处理函数，调用完之后，还会调用GoBackHistory
 */
void ZMAEE_Menu_InitSwitchMenu(int title_str_id, int highlight_index,
										ZMAEE_MENU_HIGHLIGHT_FUNC highlight_func,
										ZMAEE_MENU_KEY_ENTRY left_softkey_pre_func, 
										ZMAEE_MENU_KEY_ENTRY right_softkey_pre_func);

/**
 * 显示开关页面
 */
void ZMAEE_Menu_ShowSwitch();

/**
 * 初始化自定义时间段页面
 * title_str_id				标题
 * init_start_hour			初始化的开始时间
 * init_end_hour			初始化的结束时间
 * notify_func				页面操作成功后，通知回调
 */
void ZMAEE_Menu_InitCustomTime(int title_str_id, 
										int init_start_hour, int init_end_hour,
										ZMAEE_MENU_CUSTOM_TIME_NOTIFY notify_func);


/**
 * 显示自定义时间段页面
 */
void ZMAEE_Menu_CustomTime_Show();

/**
   * 创建一个系统列表菜单
   * @pListMenu 					输入参数，列表菜单的内容
   * RETURN:
   *	E_ZM_AEE_BADPARAM		参数有误
   *	E_ZM_AEE_NOMEMORY		超出最多可创建的列表菜单个数
   *	> 0 								创建的列表菜单的句柄
   */
int ZMAEE_Menu_Create_ListMenu(ZMAEE_MENU_LISTMENU *pListMenu);

/**
 * 显示已创建的系统列表菜单
 * @handle							列表菜单的句柄
 * @bHistory						0 - 高亮菜单项从0开始，1 - 高亮菜单项为历时纪录中的高亮项
 */
void ZMAEE_Menu_Show_ListMenu(int handle, int bHistory);

/**
 * 销毁已创建的系统列表菜单，函数可被重入
 * @handle							列表菜单的句柄
 */
void ZMAEE_Menu_Destroy_ListMenu(int handle);

/**
 * 显示弹出框
 * @content				UCS2编码格式的内容
 * @type					0 - 成功，1 - 错误，2 - info
 * @duration				显示时间，单位:ms
 */
void zmaee_menu_show_popup(unsigned char *content, int type, unsigned int duration);


#endif
