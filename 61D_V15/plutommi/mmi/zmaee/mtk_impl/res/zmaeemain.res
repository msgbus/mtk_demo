#include "mmi_features.h"
#include "custresdef.h"

<?xml version="1.0" encoding="UTF-8"?>

#ifdef __ZMAEE_APP__
<APP id="APP_ZMAEE" 
    package_name="native.mtk.zmaeemain"
    name="STR_ZMAEE_MAINSCREEN"
    img="IMG_ZMAEE_MAINSCREEN"
    launch="vapp_zmaeemain_launch" >
    <INCLUDE file="GlobalResDef.h"/>
    
	
    <STRING id="STR_ZMAEE_MAINSCREEN"/>
    <IMAGE id="IMG_ZMAEE_MAINSCREEN">"..\\\\..\\\\Customer\\\\Images\\\\zmaee\\\\MM_Zmaee.png"</IMAGE>

	<STRING id="STR_ZMAEE_MAIN"/>
	<STRING id="STR_ZMAEE_LOAD_ERROR"/>
	<STRING id="STR_ZMAEE_SIM_ERROR"/>
	<STRING id="STR_ZMAEE_INIT_ERROR"/>
	<STRING id="STR_ZMAEE_USB_ERROR"/>
	<STRING id="STR_ZMAEE_TCARD_ERROR"/>
	<STRING id="STR_ZMAEE_SSC_INFO"/>
	<STRING id="STR_ZMAEE_NOMEMORY"/>
	<STRING id="STR_ZMAEE_SSC_TITLE"/>
	<STRING id="STR_ZMAEE_WPBVIEW_MSG"/>
	<STRING id="STR_ZMAEE_VUIVIEW_MSG"/>
	<STRING id="STR_ZMAEE_APPMEM1"/>
	<STRING id="STR_ZMAEE_APPMEM2"/>
	<STRING id="STR_ZMAEE_APPMEM3"/>
	<STRING id="STR_ZMAEE_APPMEM4"/>
	<STRING id="STR_ZMAEE_QQ"/>
	<STRING id="STR_ZMAEE_HMGS"/>
	<STRING id="STR_ZMAEE_EXITQQ"/>
	<STRING id="STR_ZMAEE_SJDW"/>
	<STRING id="STR_ZMAEE_OPER"/>


#ifdef __ZMAEE_REDUCE_DOWNLOAD__
	<STRING id="STR_ZMAEE_DOWN_INIT"/>
	<STRING id="STR_ZMAEE_DOWN_NETERR"/>
#endif


	<IMAGE id="IMG_ZMAEE_MAIN">"..\\\\..\\\\Customer\\\\Images\\\\zmaee\\\\24X24\\\\main.gif"</IMAGE>


	<MENU id="MENU_ZMAEE_SSC_MAIN" str="STR_ZMAEE_MAIN" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_Main_Highlight">
		<MENUITEM_ID>MENU_ZMAEE_MAIN</MENUITEM_ID>
		<MENUITEM_ID>MENU_ZMAEE_SSC_INFO</MENUITEM_ID>
	</MENU>

	<MENU id="MENU_ZMAEE_SSC_INFO" str="STR_ZMAEE_SSC_INFO" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_SSC_Info_Highlight"/>
	<MENU id="MENU_ZMAEE_MAIN" str="STR_ZMAEE_MAIN" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_Main_Highlight"/>

	<MENU id="MENU_ZMAEE_QQ" str="STR_ZMAEE_QQ" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_QQ_Highlight"/>
	<MENU id="MENU_ZMAEE_HMGS" str="STR_ZMAEE_HMGS" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_HMGS_Highlight"/>
	<MENU id="MENU_ZMAEE_SJDW" str="STR_ZMAEE_SJDW" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_SJDW_Highlight"/>
	<MENU id="MENU_ZMAEE_OPER" str="STR_ZMAEE_OPER" img="IMG_ZMAEE_MAIN" highlight="ZMAEE_OPER_Highlight"/>


	<RECEIVER id="EVT_ID_USB_ENTER_MS_MODE" proc="ZMAEE_Usb_EnterMS_Hdlr"/>
	<RECEIVER id="EVT_ID_USB_EXIT_MS_MODE" proc="ZMAEE_Usb_ExitMS_Hdlr"/>
	<RECEIVER id="EVT_ID_SRV_PROF_IS_PLAYING" proc="ZMAEE_AudioPlayer_Play_Event_Callback"/>

	<STRING id="STR_ZMAEE_MAIN_MENU_GAMECENTER" />
	<STRING id="STR_ZMAEE_MAIN_MENU_QQ" />	
	<STRING id="STR_ZMAEE_CUSTOM_TIME_START" />
	<STRING id="STR_ZMAEE_CUSTOM_TIME_END" />

</APP>
#endif
