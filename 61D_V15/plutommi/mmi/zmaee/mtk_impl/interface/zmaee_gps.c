#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"
#include "med_utility.h"
#include "zmaee_typedef.h"
#include "gdi_include.h"
#include "zmaee_idle_clock.h"
#include "gui.h"

typedef int S32;

#ifdef ZMAEE_USE_CACHE_MEMORY 
extern void zmaee_cache_free_cb(void *mem_ptr);
#endif  

int ZMAEE_IGps_New(ZM_AEECLSID clsId,void **ppobj)
{
	*ppobj = NULL;
	return E_ZM_AEE_CLASSNOTSUPPORT;
}


#if defined(__ZMAEE_IDLE_CLOCK_USE_MY_PIC__) || defined(__ZMAEE_IDLE_CALCULATOR_USE_MY_PIC__)


void* zmaee_gps_malloc(int size)
{
	void* pBuf = NULL;
#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() < size)
		return NULL;

#ifdef ZMAEE_USE_CACHE_MEMORY
	return applib_mem_ap_cache_alloc(size, zmaee_cache_free_cb);
#else
	return applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, size);
#endif

#else
	kal_prompt_trace(LAST_MOD_ID, "zmaee_gbhw_malloc.....size=%d, max=%d", size, ZMAEE_GetMaxAllocMedMem());
	if( ZMAEE_GetMaxAllocMedMem() < size )
		return NULL;

#ifdef ZMAEE_USE_CACHE_MEMORY
	pBuf = med_alloc_ext_mem_cacheable(size);
#else
	pBuf = med_alloc_ext_mem( size );
#endif

#endif

	kal_prompt_trace(LAST_MOD_ID, "zmaee_gbhw_malloc.....buf=%x", pBuf);
	return pBuf;
	
}

void zmaee_gps_free(void *ptr)
{	
#ifdef __MED_IN_ASM__

#ifdef ZMAEE_USE_CACHE_MEMORY
	applib_mem_ap_cache_free(ptr);
#else	
	applib_mem_ap_free(ptr);
#endif

#else

#ifdef ZMAEE_USE_CACHE_MEMORY
	med_free_ext_mem(&ptr);
#else
	med_free_ext_mem(&ptr);
#endif

#endif
}



void zmaee_measure_image_clock(unsigned char idx, int* nWidth, int* nHeight)
{
#if 1
	if(idx == 'c')
	{
		*nWidth = 240;
		*nHeight = 242;
	}
	else
	{
		*nWidth = 41;
		*nHeight = 57;
	}
#else
	switch(idx)
	{
	case '0':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num0, nWidth, nHeight);
		break;
	case '1':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num1, nWidth, nHeight);
		break;
	case '2':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num2, nWidth, nHeight);
		break;
	case '3':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num3, nWidth, nHeight);
		break;
	case '4':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num4, nWidth, nHeight);
		break;
	case '5':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num5, nWidth, nHeight);
		break;
	case '6':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num6, nWidth, nHeight);
		break;
	case '7':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num7, nWidth, nHeight);
		break;
	case '8':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num8, nWidth, nHeight);
		break;
	case '9':
		gui_measure_image((unsigned char*)sg_zmaee_pic_clock_num9, nWidth, nHeight);
		break;
	case 'c'://������
		gui_measure_image((unsigned char*)sg_zmaee_calc_bgimg_rom, nWidth, nHeight);
		break;
	default:
		break;
	}
#endif
}


void zmaee_draw_image_clock(unsigned char idx, int x, int y, int nWidth, int nHeight)
{
#if defined(__ZMAEE_IDLE_CALCULATOR_USE_MY_PIC__) || defined(__ZMAEE_IDLE_CLOCK_USE_MY_PIC__)
	zm_extern int					ZMAEE_IShell_CreateInstance(AEE_IShell * po, ZM_AEECLSID clsId, void** ppobj);
	zm_extern AEE_IShell*		ZMAEE_GetShell(void);

	AEE_IDisplay* pDisplay = NULL;
	AEE_IImage* pImage = NULL;
	ZMAEE_IShell_CreateInstance(ZMAEE_GetShell(), ZM_AEE_CLSID_DISPLAY, (void**)&pDisplay);

	ZMAEE_IDisplay_CreateImage(pDisplay, zmaee_gps_malloc, zmaee_gps_free, (void**)&pImage);

	switch(idx)
	{
	#ifdef __ZMAEE_IDLE_CLOCK_USE_MY_PIC__
	case '0':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num0, sizeof(sg_zmaee_pic_clock_num0));
		break;
	case '1':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num1, sizeof(sg_zmaee_pic_clock_num1));
		break;
	case '2':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num2, sizeof(sg_zmaee_pic_clock_num2));
		break;
	case '3':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num3, sizeof(sg_zmaee_pic_clock_num3));
		break;
	case '4':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num4, sizeof(sg_zmaee_pic_clock_num4));
		break;
	case '5':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num5, sizeof(sg_zmaee_pic_clock_num5));
		break;
	case '6':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num6, sizeof(sg_zmaee_pic_clock_num6));
		break;
	case '7':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num7, sizeof(sg_zmaee_pic_clock_num7));
		break;
	case '8':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num8, sizeof(sg_zmaee_pic_clock_num8));
		break;
	case '9':
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_pic_clock_num9, sizeof(sg_zmaee_pic_clock_num9));
		break;
	#endif
	#ifdef __ZMAEE_IDLE_CALCULATOR_USE_MY_PIC__
	case 'c'://������
		ZMAEE_IImage_SetData(pImage, 1, sg_zmaee_calc_bgimg_rom, sizeof(sg_zmaee_calc_bgimg_rom));
		break;
	#endif
	default:
		break;
	}
	

	ZMAEE_IDisplay_DrawImage(pDisplay, x, y, pImage, 0);

	ZMAEE_IImage_Release(pImage);
	
	ZMAEE_IDisplay_Release(pDisplay);
#endif	

}


#endif//__ZMAEE_IDLE_CLOCK_USE_MY_PIC__


#ifndef __MMI_GUOBI_HANDWRITING__
int GBGetWinguoVerificationCode(void)
{
	return 0;
}
#endif

#ifdef __MMI_GUOBI_HANDWRITING__
/**
 * ��������
 */
const unsigned char zmaee_prv_hw_data[] = {
#include "zmaee_gb_hw.h"
};


void* zmaee_gbhw_malloc(int size)
{
	void* pBuf = NULL;
#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() < size)
		return NULL;

#ifdef ZMAEE_USE_CACHE_MEMORY
	return applib_mem_ap_cache_alloc(size, zmaee_cache_free_cb);
#else
	return applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, size);
#endif
  
#else
	kal_prompt_trace(LAST_MOD_ID, "zmaee_gbhw_malloc.....size=%d, max=%d", size, ZMAEE_GetMaxAllocMedMem());
	if( ZMAEE_GetMaxAllocMedMem() < size )
		return NULL;

#ifdef ZMAEE_USE_CACHE_MEMORY
	pBuf = med_alloc_ext_mem_cacheable(size);
#else
	pBuf = med_alloc_ext_mem( size );
#endif

#endif

	kal_prompt_trace(LAST_MOD_ID, "zmaee_gbhw_malloc.....buf=%x", pBuf);
	return pBuf;
	
}

void zmaee_gbhw_free(void *ptr)
{	
#ifdef __MED_IN_ASM__

#ifdef ZMAEE_USE_CACHE_MEMORY
	applib_mem_ap_cache_free(ptr);
#else	
	applib_mem_ap_free(ptr);
#endif

#else

#ifdef ZMAEE_USE_CACHE_MEMORY
	med_free_ext_mem(&ptr);
#else
	med_free_ext_mem(&ptr);
#endif

#endif
}

const unsigned char* zmaee_gbhw_get_data_ptr(void)
{
	return zmaee_prv_hw_data;
}

const unsigned int zmaee_gbhw_get_data_size(void)
{
	return sizeof(zmaee_prv_hw_data);
}

extern const void* zmaee_gbhw_data_create(void);
extern void zmaee_gbhw_data_destroy(const void *gbhw_data);
extern unsigned long *zmaee_prv_HWDataArray;		  
void  zmaee_imc_connect(void)
{ 
	int ret; 		   
	zmaee_prv_HWDataArray = (unsigned long *)zmaee_gbhw_data_create();
	kal_prompt_trace(LAST_MOD_ID,"zmaee_imc_connect: ZMAEE_GetMaxAllocMedMem()=%d,zmaee_prv_HWDataArray=%d",ZMAEE_GetMaxAllocMedMem(),zmaee_prv_HWDataArray);						   
	if(zmaee_prv_HWDataArray) 
	{
		ret = GBHWNew((const void*)zmaee_prv_HWDataArray, 0); 
	}
}

void  zmaee_imc_disconnect(void)
{
	 zmaee_gbhw_data_destroy((const void*)zmaee_prv_HWDataArray); 
	kal_prompt_trace(LAST_MOD_ID,"zmaee_imc_disconnect......ZMAEE_GetMaxAllocMedMem=%d",ZMAEE_GetMaxAllocMedMem());		 				   
}

#else
void* zmaee_gbhw_malloc(int size)
{
	return NULL;
}

void zmaee_gbhw_free(void *ptr)
{
}

const unsigned char* zmaee_gbhw_get_data_ptr(void)
{
	return NULL;
}

const unsigned int zmaee_gbhw_get_data_size(void)
{
	return 0;
}

void  zmaee_imc_connect(void)
{ 
}

void  zmaee_imc_disconnect(void)
{ 				   
}


#endif


#endif	// __ZMAEE_APP__
