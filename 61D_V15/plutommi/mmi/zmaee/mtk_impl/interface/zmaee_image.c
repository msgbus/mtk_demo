#ifdef __ZMAEE_APP__

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"
#include "fs_type.h"

#include "gdi_include.h"
#include "gdi_image.h"
#include "gdi_bytestream.h"
#include "gdi_image_gif.h"
//#include "sw_gif_decoder.h"

#include "Unicodexdcl.h"

#include "zmaee_priv.h"


extern U16 gdi_image_gif_transparent_index;
extern U8 gdi_image_gif_palette_size;


extern int ZMAEE_IImage_PNG_Decode(AEE_IAStream* source, 
							AEE_PFN_MALLOC pMalloc,
							AEE_PFN_FREE pFree,
							void** bitmap);
extern int ZMAEE_IImage_GIF_Decode(AEE_IAStream* source, 
							AEE_PFN_MALLOC pMalloc,
							AEE_PFN_FREE pFree,
							void** bitmap,
							int nFrame);
extern AEE_IFile* ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode);

zm_extern int ZMAEE_Utf8_2_Ucs2(const char* src, int len, unsigned short* wcsDst, int nSize);
zm_extern ZMAEE_ColorFormat ZMAEE_IBitmap_GetColorFormat(AEE_IBitmap* po);
zm_extern AEE_PFN_MALLOC ZMAEE_IBitmap_GetMallocFunc(AEE_IBitmap* po);
zm_extern AEE_PFN_FREE ZMAEE_IBitmap_GetFreeFunc(AEE_IBitmap* po);
zm_extern int ZMAEE_IDisplay_CF2GDICF(ZMAEE_ColorFormat cf, gdi_color_format *gdi_cf);
zm_extern int ZMAEE_IDisplay_ZMCF2BytsPerPixel(ZMAEE_ColorFormat cf);
zm_extern int ZMAEE_IBitmap_New(ZM_AEECLSID clsId,void ** pObj, int bit_size, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
zm_extern int ZMAEE_IBitmap_Release(AEE_IBitmap* po);
zm_extern int ZMAEE_IBitmap_SetInfo(AEE_IBitmap* po, ZMAEE_BitmapInfo* info);
#ifdef WIN32
zm_extern int ZMAEE_IFileMgr_New(ZM_AEECLSID clsId, void** pObj);//WLJ
zm_extern int ZMAEE_IFileMgr_Test_Ext(AEE_IFileMgr* po, const char* pszFile);
zm_extern int ZMAEE_IBitmap_Create(int width, 
						 int height, 
						 int pal_size,
						 ZMAEE_ColorFormat cf, 
						 AEE_PFN_MALLOC pMalloc, 
						 AEE_PFN_FREE pFree, 
						 void** pBitmap);
zm_extern int ZMAEE_IBitmap_GetInfo(AEE_IBitmap* po, ZMAEE_BitmapInfo* pBI);
zm_extern ZMAEE_IMemStream_Create(int , void** , unsigned char* , unsigned int);//WLJ
zm_extern int ZMAEE_IMemStream_Release(AEE_IMemStream* po);
zm_extern int ZMAEE_Ucs2_2_Utf8(const unsigned short* wcsSrc, int nwcsLen, char* pDst, int nSize);
zm_extern unsigned int zmaee_wcslen(const unsigned short * str);
zm_extern int ZMAEE_IFile_Release(AEE_IFile* po);
zm_extern int ZMAEE_IFileMgr_Release(AEE_IFileMgr* po);
zm_extern int AEE_IImage_SetResData(AEE_IImage* po,int nType, unsigned char* pData, int nLen);
#endif


#define ZMAEE_IIMAGE_FUNC_COUNT			8

static int ZMAEE_IImage_AddRef(AEE_IImage* po);
zm_extern int ZMAEE_IImage_Release(AEE_IImage* po);
zm_extern int ZMAEE_IImage_SetData(AEE_IImage* po, int nType, unsigned char* pData, int nLen);
static int ZMAEE_IImage_GetFrameCount(AEE_IImage* po);
static int ZMAEE_IImage_Width(AEE_IImage* po);
static int ZMAEE_IImage_Height(AEE_IImage* po);
static ZMAEE_ImageType ZMAEE_IImage_GetType(AEE_IImage* po);
static int ZMAEE_IImage_Decord(AEE_IImage* po, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap, int nFrame);


typedef struct {
	void			*pVtbl;
	int     		nRef;
	unsigned short	image_ucs2_path[AEE_MAX_PATH_NAME];
	unsigned char 	*image_data;
	int				image_data_size;
	int				image_data_type;

	int 			image_frames;
	int				image_type;
	int				image_width;
	int				image_height;

	int				is_filled_data;

	AEE_PFN_MALLOC	malloc_func;
	AEE_PFN_FREE	free_func;
}ZMAEE_IMAGE;

int ZMAEE_IImage_New(ZM_AEECLSID clsId,void ** pObj, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree)
{
	ZMAEE_IMAGE* pThis = 0;

	if(!pMalloc || !pFree) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis = (ZMAEE_IMAGE*)pMalloc(sizeof(ZMAEE_IMAGE));
	if(pThis == 0) return E_ZM_AEE_NOMEMORY;
	
	memset(pThis, 0, sizeof(ZMAEE_IMAGE));
	pThis->pVtbl = ZMAEE_GetVtable(clsId);
	if(pThis->pVtbl == 0)
	{
		pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IIMAGE_FUNC_COUNT, 		
										ZMAEE_IImage_AddRef,
										ZMAEE_IImage_Release,
										ZMAEE_IImage_SetData,
										ZMAEE_IImage_GetFrameCount,
										ZMAEE_IImage_Width,
										ZMAEE_IImage_Height,
										ZMAEE_IImage_GetType,
										ZMAEE_IImage_Decord);
	}

	pThis->nRef = 1;
	pThis->malloc_func = pMalloc;
	pThis->free_func = pFree;
	pThis->is_filled_data = 0;

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;	
}

static
int ZMAEE_IImage_AddRef(AEE_IImage* po)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_IImage_Release(AEE_IImage* po)
{
	ZMAEE_IMAGE* pThis = (ZMAEE_IMAGE*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;
	if(pThis->nRef == 0)
	{
		if((pThis->image_data_type == 2) && (pThis->image_data != NULL)) {
			pThis->free_func(pThis->image_data);
		}
		pThis->free_func(pThis);
	}
	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置图像数据
 * 参数:
 * 	po						AEE_IImage实例
 * 	nType					0 - 文件名，1 - 无需释放的buf，2 - 需接口释放的buf
 * 	pData					数据buf
 * 	nLen						buf字节长度
 * 返回:
 *	 E_ZM_AEE_SUCCESS	数据设置成功.
 *	 E_ZM_AEE_BADPARAM	pData为空, nLen无效, nType类型非法
 *	 E_ZM_AEE_NOMEMORY	内存不足
 *	 E_ZM_AEE_FAILURE	Image已经被设置
 */
int ZMAEE_IImage_SetData(AEE_IImage* po, int nType, unsigned char* pData, int nLen)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if((!pData) || (nLen <= 0))
		return E_ZM_AEE_BADPARAM;

	if(pThis->is_filled_data == 1)
		return E_ZM_AEE_FAILURE;

	if(nType == 0) {
		AEE_IFileMgr *pFileMgr = NULL;

		ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
		
		if(ZMAEE_IFileMgr_Test_Ext(pFileMgr, pData) == E_ZM_AEE_SUCCESS) {
			if(ZMAEE_Utf8_2_Ucs2((const char*)pData, nLen, (unsigned short*)pThis->image_ucs2_path, sizeof(pThis->image_ucs2_path) - 2) < 0) {
				memset(pThis->image_ucs2_path, 0, sizeof(pThis->image_ucs2_path));
				return E_ZM_AEE_BADPARAM;
			}

			pThis->image_type = gdi_image_get_type_from_file((S8*)pThis->image_ucs2_path);
		} else {
			return AEE_IImage_SetResData(po, nType, pData, nLen);
		}
	} else {
		pThis->image_data = pData;
		pThis->image_data_size = nLen;
		pThis->image_data_type = nType;

		pThis->image_type = gdi_image_get_type_from_mem((PS8)pThis->image_data);
	}

	pThis->image_frames = 0;
	pThis->image_width = 0;
	pThis->image_height = 0;
	pThis->is_filled_data = 1;
	
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取图片的帧数
 * 参数:
 * 	po 						AEE_IImage实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	> 0						图片帧数
 */
static
int ZMAEE_IImage_GetFrameCount(AEE_IImage* po)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;
	GDI_RESULT gdi_ret;
	gdi_image_src_enum src_type;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->image_frames != 0) {
		return pThis->image_frames;
	}

	if((pThis->image_data == NULL) && (mmi_ucs2strlen((const S8*)pThis->image_ucs2_path) > 0)) {
		src_type = GDI_IMAGE_SRC_FROM_FILE;		
		gdi_ret = gdi_image_codec_get_frame_count(src_type, (U8*)pThis->image_ucs2_path, pThis->image_type, 
												(U8)mmi_ucs2strlen((const S8*)pThis->image_ucs2_path),
												(S32*)&(pThis->image_frames), 0);
	} else if(pThis->image_data) {
		if(pThis->image_type == GDI_IMAGE_TYPE_INVALID)
			src_type = GDI_IMAGE_SRC_FROM_RESOURCE;
		else
			src_type = GDI_IMAGE_SRC_FROM_MEMORY;
		gdi_ret = gdi_image_codec_get_frame_count(src_type, (U8*)pThis->image_data, pThis->image_type, (S32)pThis->image_data_size,
												(S32*)&(pThis->image_frames), 0);
	} else {
		return E_ZM_AEE_FAILURE;
	}

	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}
	
	return pThis->image_frames;
}

/**
 * 获取图片宽度
 * 参数:
 * 	po						AEE_IImage实例
 * 参数:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	> 0						图片宽度
 */
static
int ZMAEE_IImage_Width(AEE_IImage* po)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;
	GDI_RESULT gdi_ret;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->image_width != 0) {
		return pThis->image_width;
	}

	if((pThis->image_data == NULL) && (mmi_ucs2strlen((const char*)pThis->image_ucs2_path) > 0)) {	
		gdi_ret = gdi_image_get_dimension_file((S8*)pThis->image_ucs2_path, (S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
	} else if(pThis->image_data) {
		if(pThis->image_type != GDI_IMAGE_TYPE_INVALID)
			gdi_ret = gdi_image_get_dimension_mem((U8)pThis->image_type, (U8*)pThis->image_data, (S32)pThis->image_data_size,
												(S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
		else
			gdi_ret = gdi_image_get_dimension((U8*)pThis->image_data, (S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
	} else {
		return E_ZM_AEE_FAILURE;
	}

	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	return pThis->image_width;
}

/**
 * 获取图片高度
 * 参数:
 * 	po						AEE_IImage实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	> 0						图片高度
 */
static
int ZMAEE_IImage_Height(AEE_IImage* po)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;
	GDI_RESULT gdi_ret;

	if(pThis == NULL) {
		return 0;
	}

	if(pThis->image_height != 0) {
		return pThis->image_height;
	}

	if((pThis->image_data == NULL) && (mmi_ucs2strlen((const char*)pThis->image_ucs2_path) > 0)) {	
		gdi_ret = gdi_image_get_dimension_file((S8*)pThis->image_ucs2_path, (S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
	} else if(pThis->image_data) {
		if(pThis->image_type != GDI_IMAGE_TYPE_INVALID)
			gdi_ret = gdi_image_get_dimension_mem((U8)pThis->image_type, (U8*)pThis->image_data, (S32)pThis->image_data_size,
												(S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
		else
			gdi_ret = gdi_image_get_dimension((U8*)pThis->image_data, (S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
	} else {
		return 0;
	}

	if(gdi_ret != GDI_SUCCEED) {
		return 0;
	}

	return pThis->image_height;
}

/**
 * 获取图片类型
 * 参数:
 * 	po						AEE_IImage实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	ZMAEE_ImageType			图片类型
 */
static
ZMAEE_ImageType ZMAEE_IImage_GetType(AEE_IImage* po)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;

	if(pThis == NULL) {
		return -1;
	}

	if(pThis->image_type == 0) {
		return -1;
	}

	switch(pThis->image_type) {
		case GDI_IMAGE_TYPE_JPG:
		case GDI_IMAGE_TYPE_JPG_FILE:
			return ZMAEE_IMAGE_JPG;
		case GDI_IMAGE_TYPE_GIF:
		case GDI_IMAGE_TYPE_GIF_FILE:
			return ZMAEE_IMAGE_GIF;
		case GDI_IMAGE_TYPE_PNG:
		case GDI_IMAGE_TYPE_PNG_FILE:
			return ZMAEE_IMAGE_PNG;
		default:
			return -1;
	}
}

/**
 * 解码图片，并创建一个Bitmap对象, GIF图片每帧的图片大小必须一致
 * 参数:
 * 	po						AEE_IImage实例
 * 	pMalloc					分配函数
 * 	pFree					释放函数
 * 	cf						解码的颜色格式
 * 	pBitmap					输出参数，输出解码后的Bitmap对象, 如果是动态gif，则是一个Bitmap对象数组
 * 	nFrame					帧数，只对动态gif有效
 * 返回:
 *	 E_ZM_AEE_SUCCESS	解码成功.
 *	 E_ZM_AEE_BADPARAM	pMalloc为空, pFree为空, pBitmap为空, cf格式不合法, 指定的nFrame不存在
 *	 E_ZM_AEE_NOMEMORY	内存不足
 *	 E_ZM_AEE_FAILURE	解码失败
 */
#include "cache_sw.h"

static
int ZMAEE_IImage_Decord(AEE_IImage* po, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap, int nFrame)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;
	GDI_RESULT gdi_ret = GDI_SUCCEED;
	gdi_color_format gdi_cf;
	int byts_per_pixel;
	ZMAEE_BitmapInfo bmp_info = {0};	
	
	if((pThis == NULL) || (pMalloc == NULL) || (pFree == NULL) || (pBitmap == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	// 解码
	switch(pThis->image_type) {
		case GDI_IMAGE_TYPE_JPG:
		case GDI_IMAGE_TYPE_JPG_FILE:
		{
			int bit_size;
			
			// 获取Image的长和宽
			if((pThis->image_width == 0) || (pThis->image_height == 0)) {
				if((pThis->image_data == NULL) && (mmi_ucs2strlen((const char*)pThis->image_ucs2_path) > 0)) {	
					gdi_ret = gdi_image_get_dimension_file((S8*)pThis->image_ucs2_path, (S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
				} else if(pThis->image_data) {
					gdi_ret = gdi_image_get_dimension_mem((U8)pThis->image_type, (U8*)pThis->image_data, (S32)pThis->image_data_size,
														(S32*)&(pThis->image_width), (S32*)&(pThis->image_height));
				} else {
					return E_ZM_AEE_FAILURE;
				}
			
				if(gdi_ret != GDI_SUCCEED) {
					return E_ZM_AEE_FAILURE;
				}
			}

			bit_size = pThis->image_height * pThis->image_width * 2;	
			if(bit_size < 0) {
				return E_ZM_AEE_FAILURE;
			}
			
			// 创建Bitmap对象
			if(ZMAEE_IBitmap_Create(pThis->image_width, pThis->image_height, 0, ZMAEE_COLORFORMAT_16, pMalloc, pFree, pBitmap) != E_ZM_AEE_SUCCESS) {
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;				
			}

			if(ZMAEE_IBitmap_GetInfo((AEE_IBitmap*)*pBitmap, &bmp_info) != E_ZM_AEE_SUCCESS) {
				ZMAEE_IBitmap_Release((AEE_IBitmap*)*pBitmap);
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;
			}

			if(pThis->image_type == GDI_IMAGE_TYPE_JPG) {
				unsigned char *pBits = (unsigned char*)bmp_info.pBits;
				
				if(INT_QueryIsCachedRAM(pBits, bit_size)) {
					pBits = (unsigned char*)virt_to_phys((unsigned int*)bmp_info.pBits);
				}
				
				gdi_ret = gdi_image_jpeg_draw_internal(0, 0, (S32)pThis->image_width, (S32)pThis->image_height,
													-1, -1, -1, -1,
													(U8*)pThis->image_data, (U32)pThis->image_data_size,
													FALSE, FALSE, FALSE,
													(U8*)pBits, bit_size, NULL, GDI_COLOR_FORMAT_16);

			} else {
				unsigned char *pBits = (unsigned char*)bmp_info.pBits;
				
				if(INT_QueryIsCachedRAM(pBits, bit_size)) {
					pBits = (unsigned char*)virt_to_phys((unsigned int*)bmp_info.pBits);
				}				
				
				gdi_ret = gdi_image_jpeg_draw_internal(0, 0, (S32)pThis->image_width, (S32)pThis->image_height,
													-1, -1, -1, -1,
													(U8*)pThis->image_ucs2_path, (U32)mmi_ucs2strlen((const S8*)pThis->image_ucs2_path) * 2,
													TRUE, FALSE, FALSE,
													(U8*)pBits, bit_size, NULL, GDI_COLOR_FORMAT_16);
			}
		}
			break;
			
		case GDI_IMAGE_TYPE_PNG:
		{
			AEE_IMemStream *iMemStream;

			if(ZMAEE_IMemStream_Create(ZM_AEE_CLSID_MEMSTREAM, (void**)&iMemStream, (unsigned char*)pThis->image_data, (unsigned long)pThis->image_data_size) != E_ZM_AEE_SUCCESS) {
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;
			}

			if(ZMAEE_IImage_PNG_Decode((AEE_IAStream*)iMemStream, pMalloc, pFree, pBitmap) != E_ZM_AEE_SUCCESS) {
				gdi_ret = GDI_FAILED;
			}

			ZMAEE_IMemStream_Release(iMemStream);
		}
			break;
		
		case GDI_IMAGE_TYPE_PNG_FILE:
		{
			AEE_IFile *iFile;
			AEE_IFileMgr *iFileMgr;
			char image_utf8_path[AEE_MAX_PATH_NAME] = {0};

			if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&iFileMgr) != E_ZM_AEE_SUCCESS) {
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;
			}

			ZMAEE_Ucs2_2_Utf8((const unsigned short*)pThis->image_ucs2_path, zmaee_wcslen((const unsigned short*)pThis->image_ucs2_path), 
								image_utf8_path, sizeof(image_utf8_path));
			iFile = ZMAEE_IFileMgr_OpenFile(iFileMgr, (const char*)image_utf8_path, ZM_OFM_READONLY);
			if(!iFile) {
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;
			}

			if(ZMAEE_IImage_PNG_Decode((AEE_IAStream*)iFile, pMalloc, pFree, pBitmap) != E_ZM_AEE_SUCCESS) {
				gdi_ret = GDI_FAILED;
			}

			ZMAEE_IFile_Release(iFile);
			ZMAEE_IFileMgr_Release(iFileMgr);
		}
			break;
			
		case GDI_IMAGE_TYPE_GIF:
		{
			AEE_IMemStream *iMemStream;

			if(ZMAEE_IMemStream_Create(ZM_AEE_CLSID_MEMSTREAM, (void**)&iMemStream, (unsigned char*)pThis->image_data, (unsigned long)pThis->image_data_size) != E_ZM_AEE_SUCCESS) {
				*pBitmap = NULL;
				return E_ZM_AEE_FAILURE;
			}

			if(ZMAEE_IImage_GIF_Decode((AEE_IAStream*)iMemStream, pMalloc, pFree, pBitmap, nFrame) != E_ZM_AEE_SUCCESS) {
				gdi_ret = GDI_FAILED;
			}

			ZMAEE_IMemStream_Release(iMemStream);
		}
			break;
		case GDI_IMAGE_TYPE_GIF_FILE:
		{
			AEE_IFile *iFile;
			AEE_IFileMgr *iFileMgr;
			char image_utf8_path[AEE_MAX_PATH_NAME] = {0};

			if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&iFileMgr) != E_ZM_AEE_SUCCESS) {
				*pBitmap = NULL;
				ZMAEE_DebugPrint("ZMAEE_IImage_Decord: Create file mgr failed");
				return E_ZM_AEE_FAILURE;
			}

			ZMAEE_Ucs2_2_Utf8((const unsigned short*)pThis->image_ucs2_path, zmaee_wcslen((const unsigned short*)pThis->image_ucs2_path), 
								image_utf8_path, sizeof(image_utf8_path));
			iFile = ZMAEE_IFileMgr_OpenFile(iFileMgr, (const char*)image_utf8_path, ZM_OFM_READONLY);
			if(!iFile) {
				*pBitmap = NULL;
				ZMAEE_DebugPrint("ZMAEE_IImage_Decord: Open fail, pszFile = %s", image_utf8_path);
				return E_ZM_AEE_FAILURE;
			}

			if(ZMAEE_IImage_GIF_Decode((AEE_IAStream*)iFile, pMalloc, pFree, pBitmap, nFrame) != E_ZM_AEE_SUCCESS) {
				gdi_ret = GDI_FAILED;
			}

			ZMAEE_IFile_Release(iFile);
			ZMAEE_IFileMgr_Release(iFileMgr);
		}	
			break;
		default:
			gdi_ret = GDI_FAILED;
	};

	if(gdi_ret != GDI_SUCCEED) {
		if(*pBitmap)
			ZMAEE_IBitmap_Release(*pBitmap);
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 绘制图片
 * 参数:
 * 	po						AEE_IImage实例
 * 	x						图片x坐标
 * 	y						图片y坐标
 * 	nFrame					图片帧数
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		绘制失败
 * 	E_ZM_AEE_SUCCESS		绘制成功
 */
int ZMAEE_IImage_DrawImage(AEE_IImage* po, int x, int y, int cx, int cy, int nFrame)
{
	ZMAEE_IMAGE *pThis = (ZMAEE_IMAGE*)po;
	GDI_RESULT gdi_ret;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if((pThis->image_type == GDI_IMAGE_TYPE_PNG) || (pThis->image_type == GDI_IMAGE_TYPE_PNG_FILE)) {
		unsigned int flag;
		unsigned char *data_ptr;
		unsigned int img_size;
		
		if((pThis->image_data == NULL) && (mmi_ucs2strlen((const S8*)pThis->image_ucs2_path) > 0)) {
			flag = GDI_IMAGE_CODEC_FLAG_IS_FILE;
			data_ptr = (unsigned char*)pThis->image_ucs2_path;
			img_size = mmi_ucs2strlen((const S8*)pThis->image_ucs2_path);
		} else {
			flag = GDI_IMAGE_CODEC_FLAG_IS_MEM;
			data_ptr = pThis->image_data;
			img_size = pThis->image_data_size;
		}

		ZMAEE_DebugPrint("ZMAEE_IImage_DrawImage: flag = %d, data_ptr = 0x%08x, img_size = %d", flag, data_ptr, img_size);
		gdi_ret = zmaee_gdi_image_png_draw_handler(flag, 0, x, y, cx, cy, data_ptr, img_size);
		if(gdi_ret != GDI_SUCCEED)
			return E_ZM_AEE_FAILURE;
		else
			return E_ZM_AEE_SUCCESS;
	}

	if((pThis->image_data == NULL) && (mmi_ucs2strlen((const S8*)pThis->image_ucs2_path) > 0)) {
		gdi_ret = gdi_image_codec_draw(GDI_IMAGE_SRC_FROM_FILE, (U8*)pThis->image_ucs2_path,
										(U8)pThis->image_type, 0, (S32)x, (S32)y, cx, cy, 0, (U32)nFrame);
	} else {
		if(pThis->image_type != GDI_IMAGE_TYPE_INVALID)
			gdi_ret = gdi_image_codec_draw(GDI_IMAGE_SRC_FROM_MEMORY, (U8*)pThis->image_data, (U8)pThis->image_type,
										(S32)pThis->image_data_size, (S32)x, (S32)y, cx, cy, 0, (U32)nFrame);
		else
			gdi_ret = gdi_image_codec_draw(GDI_IMAGE_SRC_FROM_RESOURCE, (U8*)pThis->image_data, (U8)pThis->image_type,
										(S32)pThis->image_data_size, (S32)x, (S32)y, cx, cy, 0, (U32)nFrame);
	}

	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;	
}

#ifdef __COSMOS_MMI_PACKAGE__
//************************************************************************************//
//ZMBM图片解码
//************************************************************************************//
#include "gdi_internal.h"
#include "gdi_imgdec_internal.h"

#define ZMAEE_ZBMP_HEADFLAG			"ZMBM"

void *gdi_image_zbmp_malloc(int size)
{	
#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() < size)
		return NULL;

	return applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, size);
#else
	if( ZMAEE_GetMaxAllocMedMem() < size )
		return NULL;

	return med_alloc_ext_mem( size );
#endif
}


void gdi_image_zbmp_free(void *ptr)
{
#ifdef __MED_IN_ASM__
	applib_mem_ap_free(ptr);
#else
	med_free_ext_mem(&ptr);
#endif
}


void zmaee_appmgr_32To24(int cx_32, int cy_32, unsigned char *layer32_mem, unsigned int layer32_size,
								int x_24, int y_24, int cx_24, int cy_24, unsigned char *layer24_mem, unsigned int layer24_size)
{
	int i, j;

	for(i = 0; i < cy_32; i++) {
		for(j = 0; j < cx_32; j++) {
			unsigned char *layer_addr_24 = layer24_mem + ((y_24 + i) * cx_24 + x_24 + j) * 3;
			unsigned char *layer_addr_32 = layer32_mem + (i * cx_32 + j) * 4;

			unsigned int pixtel_24 = (0xFF << 24) | layer_addr_24[0] | (layer_addr_24[1] << 8) | (layer_addr_24[2] << 16) | (layer_addr_24[3] << 24);
			unsigned int pixtel_32 = layer_addr_32[0] | (layer_addr_32[1] << 8) | (layer_addr_32[2] << 16) | (layer_addr_32[3] << 24);

			{
				unsigned int __s = (unsigned int)(pixtel_32); 
				unsigned int __sa = __s >> 24;
				if (__sa) {                         
					if (__sa != 255) {              
						unsigned int __d = (unsigned int)(pixtel_24);                   
						unsigned int __da = __d >> 24;                            
						if (__da) {                                               
							unsigned int __srb = __s & 0x00FF00FF;                
							unsigned int __sg  = __s & 0x0000FF00;                
							unsigned int __drb = __d & 0x00FF00FF;                
							unsigned int __dg  = __d & 0x0000FF00;                
							if (__da == 255) {                                    
								__s = 0xFF000000;                                 
							}                                               
							__srb *= __sa;                                             
							__sg  *= __sa;                                             
							__da = 256 - __sa;                                         
							__drb *= __da;                                             
							__dg  *= __da;                                             
							__srb += __drb;                                            
							__sg  += __dg;                                             
							__srb &= 0xFF00FFFF;                                       
							__sg  &= 0x00FF0000;                                       
							__s |= (__srb | __sg) >> 8;                                
						}			                                       
					}     
					
					layer_addr_24[0] = __s & 0xFF;
					layer_addr_24[1] = (__s & 0xFF00) >> 8;
					layer_addr_24[2] = (__s & 0xFF0000) >> 16;
				}                                                        
			}
		}
	}
}


void zmaee_appmgr_get_active_info(ZMAEE_LayerInfo *pLI)
{
	gdi_layer_struct *layer_struct;		
	gdi_color_format cf = 0;
	BOOL opacity_enable;
	U8 opacity_value;
	BOOL src_key_enable;
	gdi_color src_key_color;
	unsigned long a, r, g, b;

	gdi_layer_get_active((gdi_handle*)&layer_struct);
	gdi_layer_get_color_format(&cf);
	ZMAEE_IDisplay_GDICF2CF(cf, &pLI->cf);
	pLI->nScreenX = layer_struct->offset_x;
	pLI->nScreenY = layer_struct->offset_y;		
	pLI->nWidth = layer_struct->width;
	pLI->nHeight = layer_struct->height;		
	pLI->rcClip.x = layer_struct->clipx1;
	pLI->rcClip.y = layer_struct->clipy1;
	pLI->rcClip.width = layer_struct->clipx2 - layer_struct->clipx1 + 1;
	pLI->rcClip.height = layer_struct->clipy2 - layer_struct->clipy1 + 1;
	gdi_layer_get_buffer_ptr(&pLI->pFrameBuf);

	gdi_layer_get_opacity(&opacity_enable, &opacity_value);
	gdi_layer_get_source_key(&src_key_enable, &src_key_color);
	pLI->opacity_enable = (opacity_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
	pLI->opacity_value = opacity_value;
	pLI->trans_enable = (src_key_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
	gdi_act_color_to_rgb(&a, &r, &g, &b, src_key_color);
	pLI->trans_color = ZMAEE_GET_RGBA(r, g, b, a);
}

void zmaee_appmgr_draw_bitmap24(int x, int y, ZMAEE_LayerInfo *layer_info_24, ZMAEE_BitmapInfo *bmp_info, 
										ZMAEE_Rect *src_rc, int bTransparent)
{
	gdi_handle layer_32 = GDI_LAYER_EMPTY_HANDLE;
	unsigned int layer_size_32 = bmp_info->width * bmp_info->height * 4;
	unsigned char *layer_mem_32 = (unsigned char*)gdi_image_zbmp_malloc(layer_size_32);

	if(layer_mem_32) {
		memset(layer_mem_32, 0, layer_size_32);
		
		gdi_layer_create_cf_using_outside_memory(GDI_COLOR_FORMAT_32,
												0, 0, bmp_info->width, bmp_info->height, 
												&layer_32, 
												layer_mem_32, layer_size_32);
		if(layer_32 != GDI_LAYER_EMPTY_HANDLE) {
			ZMAEE_LayerInfo layer_info_32 = {0};
			unsigned int layer_size_24 = layer_info_24->nWidth * layer_info_24->nHeight * 3;

			gdi_layer_push_and_set_active(layer_32);
			zmaee_appmgr_get_active_info(&layer_info_32);
			ZMAEE_GDI_BitBlt(&layer_info_32, 0, 0, bmp_info, src_rc, bTransparent);			
			gdi_layer_pop_and_restore_active();

			zmaee_appmgr_32To24(bmp_info->width, bmp_info->height, 
								layer_mem_32, layer_size_32,
								x, y, layer_info_24->nWidth, layer_info_24->nHeight, 
								(unsigned char*)layer_info_24->pFrameBuf, layer_size_24);

			gdi_layer_free(layer_32);			
		}

		gdi_image_zbmp_free(layer_mem_32);
	}	
}


void zmaee_appmgr_draw_bitmap(AEE_IDisplay* po, int x, int y,  AEE_IBitmap* pBmp, ZMAEE_Rect* pSrcRc, int bTransparent)
{
	ZMAEE_BitmapInfo bmp_info = {0};
	ZMAEE_LayerInfo layer_info = {0};
	
	if((pBmp == NULL) || (pSrcRc == NULL)) {
		return;
	}

	// 获取bitmap对象信息
	ZMAEE_IBitmap_GetInfo(pBmp, &bmp_info);
	zmaee_appmgr_get_active_info(&layer_info);
	
	// 绘制位图
	pSrcRc->width = bmp_info.width;
	pSrcRc->height = bmp_info.height;

	if(layer_info.cf == ZMAEE_COLORFORMAT_24) {
		zmaee_appmgr_draw_bitmap24(x, y, &layer_info, &bmp_info, pSrcRc, bTransparent);
	}else {		
		ZMAEE_GDI_BitBlt(&layer_info, (int)x, (int)y, &bmp_info, pSrcRc, bTransparent);
	}
}



GDI_RESULT gdi_image_zbmp_draw_handler(
            U32 flag, 
            U32 frame_pos, 
            S32 x, 
            S32 y, 
            S32 w, 
            S32 h, 
            U8 *data_ptr, 
            U32 img_size)
{
	AEE_IDisplay *pDisplay;
	int ret = GDI_FAILED;
	ZMAEE_Rect bmp_rc = {0};	
	char sz_path[AEE_MAX_FILE_NAME] = {0};
	AEE_IBitmap *pBitmap = NULL;
	AEE_IMemStream *pIMemStream = NULL;

	if(ZMAEE_IDisplay_New(ZM_AEE_CLSID_DISPLAY, (void**)&pDisplay) != E_ZM_AEE_SUCCESS)
		return GDI_FAILED;

	gdi_image_codec_set_flag_begin(flag);
	GDI_ENTER_CRITICAL_SECTION(gdi_image_zbmp_draw_handler);
	GDI_IMGDEC_HW_LOCK();

	
	if(flag & GDI_IMAGE_CODEC_FLAG_IS_FILE) {
		ZMAEE_Ucs2_2_Utf8((const unsigned short*)data_ptr, mmi_ucs2strlen((char*)data_ptr), sz_path, sizeof(sz_path));	
		ZMAEE_IDisplay_LoadBitmap(pDisplay, sz_path, gdi_image_zbmp_malloc, gdi_image_zbmp_free, (void**)&pBitmap);
	} else if(flag & GDI_IMAGE_CODEC_FLAG_IS_MEM) {
		ZMAEE_IMemStream_Create(ZM_AEE_CLSID_MEMSTREAM, (void**)&pIMemStream, data_ptr, img_size);
		if(pIMemStream) {
			ZMAEE_IBitmap_Load(&pBitmap, (AEE_IAStream*)pIMemStream, gdi_image_zbmp_malloc, gdi_image_zbmp_free);
			ZMAEE_IMemStream_Release(pIMemStream);
		}
	}

	if(pBitmap) {
		zmaee_appmgr_draw_bitmap(pDisplay, x, y, pBitmap, &bmp_rc, E_ZM_AEE_TRUE);
		ZMAEE_IBitmap_Release(pBitmap);
		ret = GDI_SUCCEED;
	}

	ZMAEE_IDisplay_Release(pDisplay);
	
	GDI_IMGDEC_HW_UNLOCK();
	GDI_EXIT_CRITICAL_SECTION(gdi_image_zbmp_draw_handler);
	gdi_image_codec_set_flag_end();
	
	return ret;
}




GDI_RESULT gdi_image_zbmp_get_dimension_internal(U8 *png_src, U32 size, S32 *width, S32 *height)
{
    U8 BUFFER[20];
    U8 *buffer;

    while (1)
    {
        if (size == 0)  /* from file */
        {
            int h = gdi_lfs_open(gdi_lfs_handle,(U16*) png_src, FS_READ_ONLY | FS_OPEN_NO_DIR | FS_OPEN_SHARED, 0);

            if (h >= 0)
            {
                U32 len;
                h = gdi_lfs_handle;

                buffer = BUFFER;
                gdi_lfs_read(h, buffer, 20, &len);
                gdi_lfs_close(h);
                if (len == 20)
                    break;  /* success read data */
            }
        }
        else if (size >= 20)    /* from memory */
        {
            memcpy(BUFFER, png_src, 20);
            buffer = BUFFER;   /* success get data */
            break;
        }
        
        return GDI_FAILED;
    }
    if (memcmp(buffer, ZMAEE_ZBMP_HEADFLAG, strlen(ZMAEE_ZBMP_HEADFLAG)) == 0) 
	{
        if(width)
			*width = buffer[4] | (buffer[5] << 8);

		if(height)
			*height = buffer[6] | (buffer[7] << 8);

		return GDI_SUCCEED;
    }
	return GDI_FAILED;
}


GDI_RESULT gdi_image_zbmp_get_dimension_handler(
            U32 flag, 
            U8 *data_ptr, 
            U32 img_size, 
            S32 *width, 
            S32 *height)
{
	return gdi_image_zbmp_get_dimension_internal(data_ptr, (flag & GDI_IMAGE_CODEC_FLAG_IS_FILE)? 0: img_size, width, height);

}


BOOL gdi_image_is_zbmp_file(U8 *file_name)
{
    int f;
    U8 buf[4];
    U32 len;
    U32 result;
	int ret;
    f = gdi_lfs_open(gdi_lfs_handle,(U16*) file_name, FS_READ_ONLY | FS_OPEN_NO_DIR | FS_OPEN_SHARED, 0);
    if (f < 0) {
		return FALSE;
	}
    f = gdi_lfs_handle;

    result = gdi_lfs_read(f, buf, 4, &len);

    gdi_lfs_close(f);

    if (result == FS_NO_ERROR)
    {
        if (len == 4)
        {
            if (memcmp(buf, ZMAEE_ZBMP_HEADFLAG, strlen(ZMAEE_ZBMP_HEADFLAG)) == 0)
            {
	#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
				return GDI_SUCCEED;
	#else		
				return TRUE;
#endif
            }
        }
    }		

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
	return GDI_FAILED;
#else
	return FALSE;
#endif

}

#endif

#endif	// __ZMAEE_APP__
