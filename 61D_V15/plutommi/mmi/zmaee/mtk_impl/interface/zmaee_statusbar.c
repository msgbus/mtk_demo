#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "gdi_datatype.h"
#include "wgui_include.h"
#include "wgui_categories_util.h"

#include "gui_typedef.h" 
#include "gui_status_icon_bar.h"
#include "gui_status_icon.h"
#include "IdleAppResDef.h"
#if !defined (__COSMOS_MMI_PACKAGE__)
#include "mmi_rp_app_mainmenu_def.h"
#else
#if (ZM_AEE_MTK_SOFTVERN >= 0x1220)
#include "mmi_rp_vapp_launcher_mm_def.h"
#endif
#endif

#ifdef __COSMOS_MMI_PACKAGE__
#include "mmi_rp_app_usbsrv_def.h"
#endif


#define ZMAEE_STATUSICON_ID_BASE		0xFF

extern gdi_handle 			ZMAEE_IDisplay_GetLayerHandle(int layer_idx);
extern void					ZMAEE_ChangeStatusImagePtr(S16 icon_ID, U8 *image_data);

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
extern void 				wgui_status_icon_set_display_layer(GDI_HANDLE display_layer);
#else
extern void					wgui_status_icon_bar_set_target_layer(wgui_status_icon_bar_enum bar_id,gdi_handle target_layer);
#endif
extern void 				wgui_status_icon_register_hide_status_icon_bar(U8 status_bar_ID, void (*f) (void));
extern void 				wgui_status_icon_set_status_icon_bar_clip(U8 status_bar_ID, S32 x1, S32 y1, S32 x2, S32 y2);
extern void 				wgui_status_icon_get_status_icon_bar_clip(U8 status_bar_ID, S32 *x1, S32 *y1, S32 *x2, S32 *y2);
extern void 				wgui_status_icon_show_status_icon_bar(U8 status_bar_ID);
extern void					wgui_status_icon_hide_status_icon(S16 icon_ID);
extern void					wgui_status_icon_show_status_icon(S16 icon_ID);
extern void					wgui_status_icon_update_status_icons(void);


#define ZMAEE_ISTATUSBAR_FUNC_COUNT		15
zm_extern int				ZMAEE_IStatusBar_AddRef(AEE_IStatusBar *po);
zm_extern int				ZMAEE_IStatusBar_Release(AEE_IStatusBar *po);
zm_extern int 				ZMAEE_IStatusBar_InitStatusBar(AEE_IStatusBar *po, int layer_idx, void (*hide_func)(void));
zm_extern int 				ZMAEE_IStatusBar_SetStatusBarClip(AEE_IStatusBar *po, int x, int y , int cx, int cy);
zm_extern int 				ZMAEE_IStatusBar_GetStatusBarClip(AEE_IStatusBar *po, int *x, int *y, int *cx, int *cy);
zm_extern void 				ZMAEE_IStatusBar_DrawStatusBar(AEE_IStatusBar *po);
zm_extern int 				ZMAEE_IStatusBar_GetFreeStatusIcon(AEE_IStatusBar *po, int *count, int icon_ids[]);
zm_extern void 				ZMAEE_IStatusBar_LockStatusIcon(AEE_IStatusBar *po, int icon_ids);
zm_extern void 				ZMAEE_IStatusBar_UnLockStatusIcon(AEE_IStatusBar *po, int icon_ids);
zm_extern int 				ZMAEE_IStatusBar_SetStatusIconImage(AEE_IStatusBar *po, int icon_ids, unsigned char *img_ptr);
zm_extern void 				ZMAEE_IStatusBar_ShowStatusIcon(AEE_IStatusBar *po, int icon_ids);
zm_extern void 				ZMAEE_IStatusBar_HideStatusIcon(AEE_IStatusBar *po, int icon_ids);
zm_extern void 				ZMAEE_IStatusBar_UpdateStatusIcons(AEE_IStatusBar *po);
zm_extern void				ZMAEE_IStatusBar_DeinitStatusBar(AEE_IStatusBar *po);
zm_extern void				ZMAEE_IStatusBar_BlinkStatusIcon(AEE_IStatusBar *po, int icon_id);
#ifdef WIN32
zm_extern ZM_AEECLSID		ZMAEE_IShell_ActiveApplet(AEE_IShell * po);
zm_extern AEE_IShell*		ZMAEE_GetShell(void);
zm_extern void				ZMAEE_AppletEntry(ZM_AEECLSID cls_id, const char* work_dir, void* param, int param_len, char *appName, char *appIcon, unsigned int nIconLen, int bkg, int app_mask, void (*reentry_func)(void), unsigned short scr_id);
#endif



#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)
void 						ZMAEE_IStatusBar_SetAppNotify(ZM_AEECLSID clsId, int index);
extern AEE_IShell*			ZMAEE_GetShell(void);
void 						ZMAEE_IStatusBar_ClearAppNotify(ZM_AEECLSID clsId, int index);
#endif

#ifdef __COSMOS_MMI_PACKAGE__
extern mmi_ret 				ZMAEE_StatusBar_Exit_MS_Hdlr(mmi_event_struct *evt);
extern mmi_ret 				ZMAEE_StatusBar_Entry_MS_Hdlr(mmi_event_struct *evt);
#endif


extern gui_status_icon_bar_icon_struct wgui_status_icon_bar_icons[MAX_STATUS_ICONS];
extern gui_status_icon_bar_struct wgui_status_icon_bar_bars[WGUI_STATUS_ICON_BAR_TOTAL_BAR];


typedef struct {
	void			*pVtbl;
	int				nRef;

	int				icon_stat[AEE_MAX_STATUSICON_COUNT];
	unsigned char	*icon_data[AEE_MAX_STATUSICON_COUNT];
	int 			icon_index[AEE_MAX_STATUSICON_COUNT];

	ZMAEE_CALLBACK  hide_callback;
}ZMAEE_STATUSBAR;


static ZMAEE_STATUSBAR	sg_zmaee_statusbar = {0};

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)
typedef struct {
	int				bValid;
	unsigned short  wcsDisplay[128];	
	unsigned int    clsAppletId;
	unsigned short  wcsName[16];
}ZMAEE_STATUSBAR_NOTIFY;

static ZMAEE_STATUSBAR_NOTIFY sg_zmaee_statusbar_notify[AEE_MAX_STATUSICON_COUNT] = {0};
#endif





int ZMAEE_IStatusBar_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_STATUSBAR* pThis = 0;

	pThis = &sg_zmaee_statusbar;

	/*初始化*/
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_ISTATUSBAR_FUNC_COUNT,
											ZMAEE_IStatusBar_AddRef,
											ZMAEE_IStatusBar_Release,
											ZMAEE_IStatusBar_InitStatusBar,
											ZMAEE_IStatusBar_SetStatusBarClip,
											ZMAEE_IStatusBar_GetStatusBarClip,
											ZMAEE_IStatusBar_DrawStatusBar,
											ZMAEE_IStatusBar_GetFreeStatusIcon,
											ZMAEE_IStatusBar_LockStatusIcon,
											ZMAEE_IStatusBar_UnLockStatusIcon,
											ZMAEE_IStatusBar_SetStatusIconImage,
											ZMAEE_IStatusBar_ShowStatusIcon,
											ZMAEE_IStatusBar_HideStatusIcon,
											ZMAEE_IStatusBar_UpdateStatusIcons,
											ZMAEE_IStatusBar_DeinitStatusBar,
				 							ZMAEE_IStatusBar_BlinkStatusIcon);
		}
	}

	pThis->nRef = 1;
	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

int ZMAEE_IStatusBar_AddRef(AEE_IStatusBar *po)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;

	if(!pThis)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_IStatusBar_Release(AEE_IStatusBar *po)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;

	if(!pThis)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef--;
	if(pThis->nRef == 0) {
		
	}

	return E_ZM_AEE_SUCCESS;
}

void ZMAEE_IStatusBar_HideFunc(void)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)&sg_zmaee_statusbar;

	if(ZMAEE_Callback_Valid(&pThis->hide_callback)) {
		void (*callback)(void) = (void (*)(void))pThis->hide_callback.pfn;

		if(callback)
			callback();
	}
}

/**
 * 初始化状态栏的基本信息
 * @ layer_idx			- 状态栏绘制的图层
 * @ hide_func		- 隐藏状态栏图标时调用的隐藏函数，用于绘制状态栏的背景
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		po, hide_func is null, layer_idx is invalid
 * 	E_ZM_AEE_FAILURE		初始化失败
 * 	E_ZM_AEE_SUCCESS		初始化成功
 */
int ZMAEE_IStatusBar_InitStatusBar(AEE_IStatusBar *po, int layer_idx, void (*hide_func)(void))
{
	gdi_handle layer_hdlr;
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;

	if(!po || !hide_func)
		return E_ZM_AEE_BADPARAM;

	layer_hdlr = ZMAEE_IDisplay_GetLayerHandle(layer_idx);

	if(!layer_hdlr)
		return E_ZM_AEE_FAILURE;

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	wgui_status_icon_set_display_layer(layer_hdlr);
#else
	wgui_status_icon_bar_set_target_layer(0, layer_hdlr);
#endif
	ZMAEE_Callback_Init(&(pThis->hide_callback), (void*)hide_func, NULL);
	register_hide_status_icon_bar(0, ZMAEE_IStatusBar_HideFunc);

	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置状态栏绘制时的裁剪区域，一般cx为屏幕宽度，cy为ZMAEE_THEME_SCRREG中的status_bar_height
 * @ x				- x坐标
 * @ y				- y坐标
 * @ cx				- 宽度
 * @ cy				- 高度
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		po is null
 * 	E_ZM_AEE_FAILURE		设置失败
 * 	E_ZM_AEE_SUCCESS		设置成功
 */
int ZMAEE_IStatusBar_SetStatusBarClip(AEE_IStatusBar *po, int x, int y , int cx, int cy)
{
	if(!po)
		return E_ZM_AEE_BADPARAM;

	set_status_icon_bar_clip(0, x, y, x + cx - 1, y + cy - 1);
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取状态栏绘制时的裁剪区域，一般在注册的隐藏函数中使用
 * @ cx				- 输出参数，宽度
 * @ cy				- 输出参数，高度
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		po, x, y, cx, cy is null
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	E_ZM_AEE_SUCCESS		获取成功
 */
int ZMAEE_IStatusBar_GetStatusBarClip(AEE_IStatusBar *po, int *x, int *y, int *cx, int *cy)
{
	int x2, y2;

	if(!po || !x || !y || !cx || !cy)
		return E_ZM_AEE_BADPARAM;

	get_status_icon_bar_clip(0, x, y, &x2, &y2);
	*cx = x2 - *x + 1;
	*cy = y2 - *y + 1;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 绘制状态栏
 */
void ZMAEE_IStatusBar_DrawStatusBar(AEE_IStatusBar *po)
{
	if(!po)
		return;

	show_status_icon_bar(0);
	hide_status_icon_bar(1);

	show_status_icons();
}

/**
 * 获取可用的状态图标个数和ID，最多为4个
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	po, count, icon_ids is null
 * 	E_ZM_AEE_FAILURE	获取失败
 * 	E_ZM_AEE_SUCCESS	获取成功，count == 0时也返回成功
 */
int ZMAEE_IStatusBar_GetFreeStatusIcon(AEE_IStatusBar *po, int *count, int icon_ids[])
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int i;

	if(!pThis || !count || !icon_ids)
		return E_ZM_AEE_BADPARAM;

	for(i = 0, *count = 0; i < AEE_MAX_STATUSICON_COUNT; i++) {
		if(pThis->icon_stat[i] == E_ZM_AEE_FALSE) {
			icon_ids[*count] = i + ZMAEE_STATUSICON_ID_BASE;
			(*count)++;

		}
	}

	ZMAEE_DebugPrint("ZMAEE_IStatusBar_GetFreeStatusIcon: count = %d", *count);

	return E_ZM_AEE_SUCCESS;
}

/**
 * 加锁状态栏图标，加锁后该图标ID无法通过GetFreeStatusIcon获取到，直到解锁后才可获取到
 */
void ZMAEE_IStatusBar_LockStatusIcon(AEE_IStatusBar *po, int icon_id)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;
	ZM_AEECLSID clsId;

	if(!pThis)
		return;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return;

	clsId = ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell());
	if(clsId){
		sg_zmaee_statusbar.icon_index[icon_idx] = clsId;
#if (ZM_AEE_MTK_SOFTVERN >= 0x1224) && defined (__COSMOS_MMI_PACKAGE__)
	ZMAEE_IStatusBar_SetAppNotify(clsId, icon_idx);
	
	mmi_frm_cb_reg_event(EVT_ID_USB_ENTER_MS_MODE, ZMAEE_StatusBar_Entry_MS_Hdlr, NULL);
	mmi_frm_cb_reg_event(EVT_ID_USB_EXIT_MS_MODE, ZMAEE_StatusBar_Exit_MS_Hdlr, NULL);
#endif
	}

	pThis->icon_stat[icon_idx] = E_ZM_AEE_TRUE;
}

/**
 * 解锁状态栏图标
 */
void ZMAEE_IStatusBar_UnLockStatusIcon(AEE_IStatusBar *po, int icon_id)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;

	if(!pThis)
		return;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return;

	if(sg_zmaee_statusbar.icon_index[icon_idx]){
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)
		int is_clear_notify = 1;
		int i;

		for(i = 0; i < AEE_MAX_STATUSICON_COUNT; i++) {
			if((i != icon_idx) && (sg_zmaee_statusbar.icon_index[icon_idx] == sg_zmaee_statusbar.icon_index[i])) {
				is_clear_notify = 0;
				break;
			}
		}
		
		if(is_clear_notify)
			ZMAEE_IStatusBar_ClearAppNotify(sg_zmaee_statusbar.icon_index[icon_idx], icon_idx);
#endif

		sg_zmaee_statusbar.icon_index[icon_idx] = 0;
	}
	pThis->icon_stat[icon_idx] = E_ZM_AEE_FALSE;
}

/**
 * 设置状态栏图标的图片数据
 * @ icon_ids				状态栏图标ID
 * @ img_ptr				状态栏图标的图片数据，必须为MakeBmp.exe生成的gif主题包数据
 * RETURN:
 *	E_ZM_AEE_BADPARAM	po, img_ptr is null, or icon_ids is invalid
 *	E_ZM_AEE_FAILURE	设置失败
 *	E_ZM_AEE_SUCCESS	设置成功
 */
int ZMAEE_IStatusBar_SetStatusIconImage(AEE_IStatusBar *po, int icon_id, unsigned char *img_ptr)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;

	if(!pThis || !img_ptr)
		return E_ZM_AEE_BADPARAM;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return E_ZM_AEE_BADPARAM;

	ZMAEE_ChangeStatusImagePtr(STATUS_ICON_ZMAEE1 + icon_idx, img_ptr);
	pThis->icon_data[icon_idx] = img_ptr;
	HideStatusIcon(STATUS_ICON_ZMAEE1 + icon_idx);
	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置状态栏图标为显示状态
 */
void ZMAEE_IStatusBar_ShowStatusIcon(AEE_IStatusBar *po, int icon_id)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;

	if(!pThis)
		return;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return;

	ShowStatusIcon(STATUS_ICON_ZMAEE1 + icon_idx);
}

/**
 * 设置状态栏图标为隐藏状态
 */
void ZMAEE_IStatusBar_HideStatusIcon(AEE_IStatusBar *po, int icon_id)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;

	if(!pThis)
		return;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return;

	ZMAEE_DebugPrint("ZMAEE_IStatusBar_HideStatusIcon: icon_id = %d", icon_idx);
	HideStatusIcon(STATUS_ICON_ZMAEE1 + icon_idx);
}

/**
 * 刷新状态栏图标
 */
void ZMAEE_IStatusBar_UpdateStatusIcons(AEE_IStatusBar *po)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;

	if(!pThis)
		return;

	UpdateStatusIcons();
}

void ZMAEE_IStatusBar_DeinitStatusBar(AEE_IStatusBar *po)
{
	gdi_handle base_layer;

	gdi_layer_get_base_handle(&base_layer);
	
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	wgui_status_icon_set_display_layer(base_layer);
#else
	wgui_status_icon_bar_set_target_layer(0, base_layer);
#endif
	register_hide_status_icon_bar(0, UI_dummy_function);

	hide_status_icon_bar(0);
    hide_status_icon_bar(1);
}

void ZMAEE_IStatusBar_BlinkStatusIcon(AEE_IStatusBar *po, int icon_id)
{
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)po;
	int icon_idx = icon_id - ZMAEE_STATUSICON_ID_BASE;

	if(!pThis)
		return;

	if((icon_idx < 0) || (icon_idx >= AEE_MAX_STATUSICON_COUNT))
		return;

	BlinkStatusIcon(STATUS_ICON_ZMAEE1 + icon_idx);
}

unsigned char* ZMAEE_IStatusBar_GetImage(int icon_id)
{
	int icon_idx = icon_id - STATUS_ICON_ZMAEE1;
	ZMAEE_STATUSBAR *pThis = (ZMAEE_STATUSBAR*)&sg_zmaee_statusbar;

	if(pThis->icon_stat[icon_idx] == 1) {
		return pThis->icon_data[icon_idx];
	} else {
		return NULL;
	}
}

void ZMAEE_IStatusBar_DeInit_Ext(void)
{
	ZMAEE_IStatusBar_DeinitStatusBar((AEE_IStatusBar*)&sg_zmaee_statusbar);
}


S32 zmaee_status_icon_bar_translate_pen_position(S32 x, S32 y)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    S32 i;
    S32 icon_id;
    S32 x1, y1, x2, y2;
    S32 bar_id;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
	ZMAEE_DebugPrint("wgui_status_icon_bar_translate_pen_position:in");
    for (bar_id = 0; bar_id < WGUI_STATUS_ICON_BAR_TOTAL_BAR; bar_id++)
    {
        if ((wgui_status_icon_bar_bars[bar_id].flags & WGUI_STATUS_ICON_BAR_FLAG_BAR_DISPLAY) &&
            (wgui_status_icon_bar_bars[bar_id].icon_list != NULL))
        {
            x1 = wgui_status_icon_bar_bars[bar_id].x1;
            y1 = wgui_status_icon_bar_bars[bar_id].y1;
            x2 = wgui_status_icon_bar_bars[bar_id].x2;
            y2 = wgui_status_icon_bar_bars[bar_id].y2;
            if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
            {
                for (i = 0; i < wgui_status_icon_bar_bars[bar_id].n_icons; i++)
                {
                    icon_id = wgui_status_icon_bar_bars[bar_id].icon_list[i];
                    if (((wgui_status_icon_bar_icons[icon_id].flags & GUI_STATUS_ICON_BAR_ICON_DISPLAY)
						|| (wgui_status_icon_bar_icons[icon_id].flags & GUI_STATUS_ICON_BAR_ICON_BLINK)
						|| (wgui_status_icon_bar_icons[icon_id].flags & GUI_STATUS_ICON_BAR_ICON_BLINK_ON))
                        && (wgui_status_icon_bar_icons[icon_id].image_id != IMAGE_ID_NULL))
                    {
                        x1 = wgui_status_icon_bar_bars[bar_id].x + wgui_status_icon_bar_icons[icon_id].x;
                        x2 = x1 + wgui_status_icon_bar_icons[icon_id].width - 1;
                        y1 = wgui_status_icon_bar_bars[bar_id].y + wgui_status_icon_bar_icons[icon_id].y;
                        y2 = y1 + wgui_status_icon_bar_icons[icon_id].height - 1;
                        if (x >= x1 && x <= x2 && y >= y1 && y <= y2)
                        {
                            return icon_id;
                        }
                    }
                }
            }
        }
    }
	
	ZMAEE_DebugPrint("wgui_status_icon_bar_translate_pen_position:icon_id = %d",icon_id);
    return STATUS_ICON_INVALID_ID;
}

//int zmaee_entry_applet(int nHandle, void* pUser)//2012.5.15.zm_blj
void zmaee_entry_applet(void* pUser)

{	
	unsigned short  wcsName[16] = {0};
	ZM_AEECLSID    cls_id;

	if(!pUser)
		return;

#ifdef __ZMAEE_APP_DESKTOP__
	{
		extern void AEE_Desktop_Exit();
		AEE_Desktop_Exit();
	}
#endif
	
	cls_id = (ZM_AEECLSID)pUser;
	ZMAEE_AppletEntry(cls_id,ZMAEE_ENTRY_WORKDIR, NULL, 0,wcsName, NULL, 0, 0, 0xFFFFFFFF, NULL, 0);
}


void ZMAEE_Start_IconBar_App(int x, int y)
{
	int icon_idx;
	int status_icon_index;
	extern int ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
	
	ZMAEE_DebugPrint("ZMAEE_Start_IconBar_App:in");

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(GetActiveScreenId() != IDLE_SCREEN_ID)
#else
	if(GetActiveScreenId() != SCR_ID_IDLE_MAIN)
#endif
		return;

	status_icon_index = zmaee_status_icon_bar_translate_pen_position(x,y);
	if(status_icon_index >= 0 && status_icon_index <= STATUS_ICON_ZMAEE4 && status_icon_index >= STATUS_ICON_ZMAEE1){
		icon_idx = status_icon_index - STATUS_ICON_ZMAEE1;
		//ZMAEE_StartPeriodHandler(0, (void*)sg_zmaee_statusbar.icon_index[icon_idx], zmaee_entry_applet);//2012.5.15.zm_blj
		gui_start_timer_ex(0, zmaee_entry_applet,(void*)sg_zmaee_statusbar.icon_index[icon_idx]);
	}
}




#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)
void ZMAEE_IStatusBar_SetNotify(int index, ZM_AEECLSID cls_id, const unsigned short *wcsName, const unsigned short *wcsDisplay)
{
	int i;

	if(wcsName && (index >= 0 && index < AEE_MAX_STATUSICON_COUNT)) {
		for(i = 0; i < AEE_MAX_STATUSICON_COUNT; i++) {
			if(sg_zmaee_statusbar_notify[i].bValid && (sg_zmaee_statusbar_notify[i].clsAppletId == cls_id))
				return;
		}
		
		memset(&sg_zmaee_statusbar_notify[index], 0, sizeof(sg_zmaee_statusbar_notify[index]));
		
		sg_zmaee_statusbar_notify[index].bValid = E_ZM_AEE_TRUE;
		sg_zmaee_statusbar_notify[index].clsAppletId = cls_id;
		mmi_ucs2ncpy((char*)sg_zmaee_statusbar_notify[index].wcsName, (const char*)wcsName, sizeof(sg_zmaee_statusbar_notify[index].wcsName) / sizeof(sg_zmaee_statusbar_notify[index].wcsName[0]));
		
		if(wcsDisplay)
			mmi_ucs2ncpy((char*)sg_zmaee_statusbar_notify[index].wcsDisplay, (const char*)wcsDisplay, sizeof(sg_zmaee_statusbar_notify[index].wcsDisplay) / sizeof(sg_zmaee_statusbar_notify[index].wcsDisplay[0]));

		zmaee_notification_create(index);
	}
}


int ZMAEE_IStatusBar_GetNotify(int index, ZMAEE_STATUSBAR_NOTIFY **pNotify)
{
	if(pNotify && (index >= 0 && index < AEE_MAX_STATUSICON_COUNT) && sg_zmaee_statusbar_notify[index].bValid) {
		*pNotify = &sg_zmaee_statusbar_notify[index];
		
		return E_ZM_AEE_SUCCESS;
	}

	return E_ZM_AEE_FAILURE;
}


void ZMAEE_IStatusBar_SetAppNotify(ZM_AEECLSID clsId, int index)
{
	extern int ZMAEE_IShell_QueryClass(AEE_IShell * po, ZM_AEECLSID clsId, ZMAEEAppInfo * pai);
	extern AEE_IShell* ZMAEE_GetShell(void);

	ZMAEEAppInfo app_info = {0};

	if(ZMAEE_IShell_QueryClass(ZMAEE_GetShell(), clsId, &app_info) == E_ZM_AEE_TRUE) {
		unsigned short ucs2_name[AEE_MAX_APPLET_NAME] = {0};

		ZMAEE_Utf8_2_Ucs2(app_info.szName, strlen(app_info.szName), ucs2_name, sizeof(ucs2_name) / sizeof(ucs2_name[0]));
		ZMAEE_IStatusBar_SetNotify(index, clsId, ucs2_name, NULL);
	}
}


void ZMAEE_IStatusBar_ClearAppNotify(ZM_AEECLSID clsId, int index)
{
	for(index = 0; index < AEE_MAX_STATUSICON_COUNT; index++) {
		if(sg_zmaee_statusbar_notify[index].bValid && sg_zmaee_statusbar_notify[index].clsAppletId == clsId) {
			sg_zmaee_statusbar_notify[index].bValid = E_ZM_AEE_FALSE;
			zmaee_notification_close(index);
		}
	}
}
#endif


#ifdef __COSMOS_MMI_PACKAGE__
mmi_ret ZMAEE_StatusBar_Exit_MS_Hdlr(mmi_event_struct *evt)
{
	int index;
	for(index = 0;index < AEE_MAX_STATUSICON_COUNT;index++){
		if(sg_zmaee_statusbar_notify[index].bValid)
			zmaee_notification_create(index);
	}
}	

mmi_ret ZMAEE_StatusBar_Entry_MS_Hdlr(mmi_event_struct *evt)
{
	int index;
	for(index = 0;index < AEE_MAX_STATUSICON_COUNT;index++){
		if(sg_zmaee_statusbar_notify[index].bValid)
			zmaee_notification_close(index);
	}
}	
#endif




#endif	// __ZMAEE_APP__
