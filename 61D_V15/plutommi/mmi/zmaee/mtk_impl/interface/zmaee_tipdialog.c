#include "gui_data_types.h"
#include "MMIDataType.h"
#include "globalResDef.h"
#include "globalConstants.h"
#include "Wgui.h"

#ifndef __MMI_FRM_HISTORY__
#include "IdleAppResDef.h"
#endif

extern int mmi_ucs2strlen(const char *);
extern char *mmi_ucs2ncpy(char *, const char *, unsigned int);
extern void ShowCategory165Screen(
        U16 left_softkey,
        U16 left_softkey_icon,
        U16 right_softkey,
        U16 right_softkey_icon,
        UI_string_type message,
        U16 message_icon,
        U8 *history_buffer);
extern void SetRightSoftkeyFunction(void (*f) (void), MMI_key_event_type k);
extern void UI_common_screen_pre_exit(void);
extern void UI_common_screen_exit(void);
extern void DisplayPopup(U8 *string, U16 imageId, U8 imageOnBottom, U32 popupDuration, U8 toneId);
extern void GoBackHistory(void);

#ifdef __MMI_FRM_HISTORY__
extern U8* GetCurrGuiBuffer(U16 scrnid);
extern U8 EntryNewScreen(U16 newscrnID, FuncPtr newExitHandler, FuncPtr newEntryHandler, void *flag);
extern U16 DeleteScreenIfPresent(U16 ScrId);
#else
extern mmi_ret zmaee_group_proc(mmi_event_struct* evt);
#endif


///////////////////////////////////////////////////////////////////////////
//��ʾ��
///////////////////////////////////////////////////////////////////////////
//#define ZMAEE_DIALOG_SCREENID	50010
#define ZMAEE_DOWN_DIALOG_SCREENID 50010

static UI_character_type g_zmaee_Dialog_info[32]={0};
static int g_zmaee_Dialog_suspend = 0;
static FuncPtr g_zmaee_Dialog_cancelFnc = 0;

void zmaee_destroyDialog(void);




static void zmaee_exitDialog(void)
{
	g_zmaee_Dialog_suspend = 1;
}


static void zmaee_dialog_dummy(void)
{
	//dummy
}


static void zmaee_dialog_onCancel(void)
{
	if(g_zmaee_Dialog_cancelFnc) {
		g_zmaee_Dialog_cancelFnc();
	}
	
	zmaee_destroyDialog();
}

static void zmaee_Dialog_EndKey_Down_Hdlr(void)
{
	extern void zmaee_down_cancel(void);
	zmaee_down_cancel();
	mmi_idle_display();
}

static void zmaee_entryDialog(void)
{
	U8 *guiBuffer = NULL;

#ifdef __MMI_FRM_HISTORY__
	guiBuffer = GetCurrGuiBuffer(ZMAEE_DOWN_DIALOG_SCREENID);
	
	EntryNewScreen(ZMAEE_DOWN_DIALOG_SCREENID, 
					zmaee_exitDialog, 
					zmaee_entryDialog, NULL);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, ZMAEE_DOWN_DIALOG_SCREENID, zmaee_exitDialog, zmaee_entryDialog, MMI_FRM_FULL_SCRN);

	guiBuffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, ZMAEE_DOWN_DIALOG_SCREENID);
#endif
	
	g_zmaee_Dialog_suspend = 0;

	ShowCategory165Screen(NULL, NULL, 
							STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
					 		g_zmaee_Dialog_info, IMG_GLOBAL_LOADING, 
					 		guiBuffer);

	SetRightSoftkeyFunction(zmaee_dialog_onCancel,KEY_EVENT_UP);
	SetRightSoftkeyFunction(zmaee_dialog_dummy,KEY_EVENT_DOWN);
	SetKeyHandler(zmaee_Dialog_EndKey_Down_Hdlr, KEY_END, KEY_EVENT_DOWN);
}


void zmaee_createDialog(UI_string_type strInfo, FuncPtr onCancel)
{
	if(strInfo == 0) {
		return;
	}
	
	mmi_ucs2ncpy((char*)g_zmaee_Dialog_info, (const char*)strInfo, sizeof(g_zmaee_Dialog_info) / 2 - 1);
	g_zmaee_Dialog_cancelFnc = onCancel;
	zmaee_entryDialog();
}


void zmaee_refreshDialog(UI_string_type strInfo)
{
	U8 *guiBuffer = NULL;
	if(strInfo == 0)
		return;
	
//	mmi_ucs2ncpy((char*)g_zmaee_Dialog_info, (const char*)strInfo, sizeof(g_zmaee_Dialog_info)/sizeof(g_zmaee_Dialog_info[0]));
	mmi_ucs2ncpy((char*)g_zmaee_Dialog_info, (const char*)strInfo, sizeof(g_zmaee_Dialog_info) / 2 - 1);

	if(g_zmaee_Dialog_suspend)
		return;

#ifdef __MMI_FRM_HISTORY__
	guiBuffer = GetCurrGuiBuffer(ZMAEE_DOWN_DIALOG_SCREENID);
#else
	guiBuffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, ZMAEE_DOWN_DIALOG_SCREENID);
#endif
	
	UI_common_screen_pre_exit();
	UI_common_screen_exit();

	ShowCategory165Screen(NULL, NULL, STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
					 		g_zmaee_Dialog_info, IMG_GLOBAL_LOADING, guiBuffer);

	SetRightSoftkeyFunction(zmaee_dialog_onCancel, KEY_EVENT_UP);
	SetRightSoftkeyFunction(zmaee_dialog_dummy, KEY_EVENT_DOWN);
	SetKeyHandler(zmaee_Dialog_EndKey_Down_Hdlr, KEY_END, KEY_EVENT_DOWN);
}


void zmaee_destroyDialog(void)
{
	if(g_zmaee_Dialog_suspend) {
#ifdef __MMI_FRM_HISTORY__
		DeleteScreenIfPresent(ZMAEE_DOWN_DIALOG_SCREENID);
#else
		mmi_frm_scrn_close(GRP_ID_ZMAEE, ZMAEE_DOWN_DIALOG_SCREENID);
#endif
	} else {
		if(GetActiveScreenId() == ZMAEE_DOWN_DIALOG_SCREENID)
			GoBackHistory();
	}
	g_zmaee_Dialog_suspend = 0;
}


void zmaee_popupMsg(UI_string_type strInfo, int dur)
{
	DisplayPopup((U8*)strInfo, IMG_GLOBAL_INFO, 0, dur, 0);
}


