#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "CommonScreens.h"
#if!(defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))&& !(defined MT6260) && !(defined MT6261)//sgf
#include "UserProfilesResDef.h"
#endif
#include "AudioInc.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "IdleAppResDef.h"
#include "ProfilesAppGprot.h"
#include "Profilessrv.h"
#include "GlobalResDef.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "GeneralDeviceGprot.h"
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#include "OslMemory_Int.h"
#endif
#else

#endif
#include "SettingGprots.h"
#include "PhoneSetup.h"
#include "WallpaperDefs.h"
#include "FileMgr.h"
#if!(defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))&& !(defined MT6260)&& !(defined MT6261) //sgf
#include "ProfileMgrGProt.h"
#include "ProfileGprots.h"
#endif
#include "SettingProfile.h"
#include "gpioInc.h"
#include "custom_mmi_default_value.h"
#include "app_datetime.h"
#include "DateTimeType.h"

#include "med_api.h"
#include "l1audio.h"
#include "aud_defs.h"
#include "med_global.h"
#include "med_context.h"
#ifdef WIN32
#include "DateTimeGprot.h"//WLJ
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
#include "gpiosrvgprot.h"
#endif

#ifdef __MMI_TOUCH_SCREEN__
#ifndef WIN32
#include "touch_panel.h"  //WLJ
#endif
#endif


#define ZMAEE_SETTING_DEBUG					(1)
#define ZMAEE_SET_TEST                      (1)
#define ZMAEE_ISETTING_FUNC_COUNT			(13)
#define ZMAEE_MINVALUE_VOLUME_RANG          (0)     
#define ZMAEE_MAXVALUE_VOLUME_RANG          (16)
#define ZMAEE_DEAFULT_VOLUME                (4)   
#define ZMAEE_DSM_MASTER_SIM                (-1)
#define ZMAEE_DSM_SLAVE_SIM                 (-2)

#if (ZM_AEE_MTK_SOFTVERN >= 0x0852) && (ZM_AEE_MTK_SOFTVERN < 0x10A0)
extern mmi_profiles_cntx_struct gprofiles[MAX_ELEMENTS];
extern mmi_profiles_cntx_struct gcurrentprofile;
#else
extern PROFILE gprofiles[MAX_ELEMENTS];
extern PROFILE gcurrentprofile;
#endif
extern U8 gprofileId;

static int ZMAEE_ISetting_AddRef(AEE_ISetting* po);
static int ZMAEE_ISetting_Release(AEE_ISetting* po);
static int ZMAEE_ISetting_SetProfileFileURL(AEE_ISetting* po,ZMAEE_SETTING_FILEITEM type, char* pUrlFile, int nMaxFileLen);
static int ZMAEE_ISetting_GetUserProfile(AEE_ISetting* po, ZMAEE_SETTING_USERPROFILE* pProfile);
static int ZMAEE_ISetting_SetUserProfile(AEE_ISetting* po, ZMAEE_SETTING_USERPROFILE nProfile);
static int ZMAEE_ISetting_SetKbdTone(AEE_ISetting* po, int bOn);
static int ZMAEE_ISetting_SetScreenAlwaysHilight(AEE_ISetting* po, int bOn);
static int ZMAEE_ISetting_GetSoundVolume(AEE_ISetting* po, int* pVol, int* nMinVol, int* nMaxVol);
static int ZMAEE_ISetting_SetSoundVolume(AEE_ISetting* po, int nVol);
zm_extern int ZMAEE_ISetting_GetSystemTime(AEE_ISetting* po, int* nTimeZone, ZMAEE_DATETIME* pLocalTime);
static int ZMAEE_ISetting_SetSystemTime(AEE_ISetting* po, int nTimeZone, const ZMAEE_DATETIME* pLocalTime);
zm_extern void ZMAEE_ISetting_SetPenEventParam(AEE_ISetting *po, int *pen_sample, int *stroke_sample, int *pen_based_offset, int *stroke_based_offset, int *pen_longtap_offset, int *stroke_longtap_offset);
zm_extern void ZMAEE_ISetting_GetDateTimeFormat(AEE_ISetting *po, ZMAEE_DATETIME_TYPE *time_format, ZMAEE_DATETIME_TYPE *date_format, unsigned char *seprator, unsigned int size);

extern int ZMAEE_Utf8_2_Ucs2(const char* src, int len, unsigned short* wcsDst, int nSize);
extern void ZMAEE_DebugPrint(const char *pszFormat, ...);
#ifdef WIN32
extern int 	ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
#endif
typedef struct {
	void						*pVtbl;
	int     					nRef;  
	ZMAEE_SETTING_USERPROFILE	UserProfile;
}ZMAEE_ISETTING;

int sg_zmaee_setting_volume;
static ZMAEE_ISETTING sg_zmaee_setting = {0};


int ZMAEE_ISetting_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_ISETTING* pThis = 0;

	pThis = &sg_zmaee_setting;

	/*初始化*/
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_ISETTING_FUNC_COUNT,
											ZMAEE_ISetting_AddRef,
											ZMAEE_ISetting_Release,
											ZMAEE_ISetting_SetProfileFileURL,
											ZMAEE_ISetting_GetUserProfile,
											ZMAEE_ISetting_SetUserProfile,
											ZMAEE_ISetting_SetKbdTone,
											ZMAEE_ISetting_SetScreenAlwaysHilight,
											ZMAEE_ISetting_GetSoundVolume,
											ZMAEE_ISetting_SetSoundVolume,
											ZMAEE_ISetting_GetSystemTime,
											ZMAEE_ISetting_SetSystemTime,
											ZMAEE_ISetting_SetPenEventParam,
											ZMAEE_ISetting_GetDateTimeFormat);
		}
	}

	pThis->nRef = 1;
	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

static 
int ZMAEE_ISetting_AddRef(AEE_ISetting* po)
{
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

static
int ZMAEE_ISetting_Release(AEE_ISetting* po)
{
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;
	if(pThis->nRef == 0)
	{	
	}
	return E_ZM_AEE_SUCCESS;
}

/**
 *  setup @type  item with a filename.
 * @pUrlFile file name, UTF-8 format
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
	E_ZM_AEE_BADPARAM: type无效或pUrlFile为空
 */
static 
int ZMAEE_ISetting_SetProfileFileURL(AEE_ISetting* po,ZMAEE_SETTING_FILEITEM type, char* pUrlFile, int nMaxFileLen)
{	
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern U8 gprofileId;
	extern U8 gactivatedprofile;
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	
	if(pThis == 0 || pUrlFile == 0)
		return E_ZM_AEE_BADPARAM;

	if(nMaxFileLen <= 0) {
		nMaxFileLen = strlen(pUrlFile);
	}
	
	switch(type)
	{
		case ZMAEE_SETTING_FILE_WALLPAPER:
		{
			extern void PhnsetWpUsrDefPreView(void);
			extern void PhnsetWpUsrDefFreeBuf(void);
			extern PHNSET_CNTX* g_phnset_cntx_p;
			extern S8*     phnset_dispchar_strp;
			unsigned short		wcs_filepath[FMGR_PATH_BUFFER_SIZE/2]={0};

			//设置当前背景
			ZMAEE_Utf8_2_Ucs2(pUrlFile,nMaxFileLen,wcs_filepath,sizeof(wcs_filepath)/sizeof(wcs_filepath[0]));
			g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
			g_phnset_cntx_p->isShort = 0;
			g_phnset_cntx_p->ScrAnchor = IDLE_SCREEN_ID;
			PhnsetWpUsrDefFreeBuf();
			
			if(phnset_dispchar_strp == NULL)
			{
				phnset_dispchar_strp = (S8*)OslMalloc(FMGR_PATH_BUFFER_SIZE);
    			memset(phnset_dispchar_strp, 0, FMGR_PATH_BUFFER_SIZE);
    			memcpy((void*)phnset_dispchar_strp,(void*)wcs_filepath, 2 * mmi_ucs2strlen((const S8*)wcs_filepath));
			}
			PhnsetWpUsrDefPreView();
		}
		break;
		case ZMAEE_SETTING_FILE_RINGTONE_INCOMING:
		{
			U16 ring_id;
			U8 melody_idx = 0;
			int err = PMG_ADDFILE_ERR_UNKNOWN;
			unsigned short	wcs_filepath[AEE_MAX_PATH_NAME/2] = {0};
			int ret = E_ZM_AEE_TRUE;
			
			//设置铃声关联
			ZMAEE_Utf8_2_Ucs2(pUrlFile,nMaxFileLen,wcs_filepath,sizeof(wcs_filepath)/sizeof(wcs_filepath[0]));	
			PmgSetShortFileName( FALSE );
			if(PmgIsEmptySlot( melody_idx ) == MMI_FALSE)
			{
				err = PmgReplaceExtMelodyBySlotPathFileName((S8*)wcs_filepath, melody_idx);
				if(err != 1)
					return E_ZM_AEE_FAILURE;
			}
			else
			{
				err = PmgAddExtMelodyBySlotPathFileName((S8*)wcs_filepath, melody_idx);
				if(err != 1)
					return E_ZM_AEE_FAILURE;
			}

			//设置为当前铃声
			ring_id = (U16)melody_idx+PMG_EXT_MELODY_BEGIN;
			gprofileId = gactivatedprofile;
			
			if(SetRingToneID(ring_id) != 0)
				ret = E_ZM_AEE_FAILURE;
#ifdef __MMI_DUAL_PROFILE_SUPPORT__
			if(SetCard2RingToneID(ring_id) != 0)
				ret = E_ZM_AEE_FAILURE;
#endif

			return ret;
		}
		break;
		
		case ZMAEE_SETTING_FILE_RINGTONE_ALARM:
		{
			U16 alarm_id;
			U8 melody_idx = 1;
			PMG_ADDFILE_ERRNO err=PMG_ADDFILE_ERR_UNKNOWN;
			unsigned short	wcs_filepath[AEE_MAX_PATH_NAME/2]={0};
			int ret = E_ZM_AEE_TRUE;
			
			ZMAEE_Utf8_2_Ucs2(pUrlFile,nMaxFileLen,wcs_filepath,sizeof(wcs_filepath)/sizeof(wcs_filepath[0]));	

			//设置铃声关联
			PmgSetShortFileName( FALSE );
			if(PmgIsEmptySlot(melody_idx) == MMI_FALSE)
			{
				err=PmgReplaceExtMelodyBySlotPathFileName((S8*)wcs_filepath, melody_idx);
				if(err != 1)
					return E_ZM_AEE_FAILURE;
			}
			else
			{
				err=PmgAddExtMelodyBySlotPathFileName((S8*)wcs_filepath, melody_idx);
				if(err != 1)
					return E_ZM_AEE_FAILURE;
			}

			//设置为当前闹铃铃声
			alarm_id = (U16)melody_idx+PMG_EXT_MELODY_BEGIN;
			gprofileId = gactivatedprofile;
			
			if(SetAlarmToneID(alarm_id) != 0)
				ret = E_ZM_AEE_FAILURE;

			return ret;
		}
		break;
		/*在模拟器上调试*/
		case ZMAEE_SETTING_FILE_RINGTONE_MESSAGE:
		{	
			return E_ZM_AEE_FAILURE;
		}
		break;
		
		default:
			return E_ZM_AEE_FAILURE;
	}
	
	return E_ZM_AEE_SUCCESS;
#else
	extern srv_prof_ret srv_prof_set_current_profile_tone_with_file_path(srv_prof_settings_enum setting,WCHAR* path);

	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	unsigned short ucs2_UrlFile[AEE_MAX_PATH_NAME] = {0};

	if(pThis == 0 || pUrlFile == 0)
		return E_ZM_AEE_BADPARAM;

	if(nMaxFileLen <= 0) {
		nMaxFileLen = strlen(pUrlFile);
	}

	ZMAEE_Utf8_2_Ucs2((const char*)pUrlFile, nMaxFileLen, (unsigned short*)ucs2_UrlFile, sizeof(ucs2_UrlFile) / sizeof(ucs2_UrlFile[0]));

	switch(type)
	{
		case ZMAEE_SETTING_FILE_WALLPAPER:
		{
	#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			extern void PhnsetWpUsrDefPreView(MMI_ID parent_gid);
		#else
			extern void PhnsetWpUsrDefPreView(void);
		#endif
			extern void PhnsetWpUsrDefFreeBuf(void);
			extern PHNSET_CNTX* g_phnset_cntx_p;
			extern S8*	   phnset_dispchar_strp;
			
			//éè??μ±?°±3?°
			g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
			g_phnset_cntx_p->isShort = 0;
			g_phnset_cntx_p->ScrAnchor = SCR_ZMAEE_MAIN;
			PhnsetWpUsrDefFreeBuf();
			
			if(phnset_dispchar_strp == NULL)
			{
				phnset_dispchar_strp = (S8*)OslMalloc(sizeof(ucs2_UrlFile));
				memset(phnset_dispchar_strp, 0, sizeof(ucs2_UrlFile));
				memcpy((void*)phnset_dispchar_strp, (void*)ucs2_UrlFile, 2 * mmi_ucs2strlen((const S8*)ucs2_UrlFile));
			}

		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			PhnsetWpUsrDefPreView(GRP_ID_ROOT);
		#else
			PhnsetWpUsrDefPreView();
		#endif
	#else
		#if (ZM_AEE_MTK_SOFTVERN == 0x11A0) || defined(__COSMOS_MMI_PACKAGE__)
		extern void ZMAEE_ISetting_SetWallpaper_FileName(unsigned short *ucs2FileName);
			
		ZMAEE_ISetting_SetWallpaper_FileName(ucs2_UrlFile);
		DisplayPopup(GetString(STR_GLOBAL_DONE), IMG_GLOBAL_SUCCESS, NULL, 2000, 0);
		#else
			extern PHNSET_CNTX* g_phnset_cntx_p;
			extern WCHAR *g_phnset_view_setting_src_file_name;
			void mmi_phnset_view_setting_entry_common_file_preview(MMI_ID parent_gid, WCHAR* file_name);
			
			//设置当前背景
			g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
			#ifndef __MMI_PHNSET_SLIM__//ZMAEEMODIFY
			g_phnset_cntx_p->isShort = 0;
			g_phnset_cntx_p->ScrAnchor = SCR_ZMAEE_MAIN;
			#endif
			if (g_phnset_view_setting_src_file_name)
			{
				OslMfree(g_phnset_view_setting_src_file_name);
				g_phnset_view_setting_src_file_name = NULL;
			}
			
			if(g_phnset_view_setting_src_file_name == NULL)
			{
			    g_phnset_view_setting_src_file_name = OslMalloc(SRV_FMGR_PATH_BUFFER_SIZE);	
				memset(g_phnset_view_setting_src_file_name, 0, sizeof(g_phnset_view_setting_src_file_name));
    			mmi_ucs2cpy((CHAR*)g_phnset_view_setting_src_file_name, (const CHAR*)ucs2_UrlFile);
			}

			mmi_phnset_view_setting_entry_common_file_preview(GRP_ID_ROOT, g_phnset_view_setting_src_file_name);
			
		#endif
	#endif
		}
		break;
		
		case ZMAEE_SETTING_FILE_RINGTONE_INCOMING:
		{
			srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_MT_CALL_TONE, (WCHAR*)ucs2_UrlFile);
				
		#ifdef __MMI_DUAL_SIM_MASTER__
			#ifdef MMI_MAX_SIM_NUM
				#if (MMI_MAX_SIM_NUM >= 3)
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_MT_CALL_CARD3_TONE, (WCHAR*)ucs2_UrlFile);
				#elif (MMI_MAX_SIM_NUM >= 4)
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_MT_CALL_CARD4_TONE, (WCHAR*)ucs2_UrlFile);
				#else
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_MT_CALL_CARD2_TONE, (WCHAR*)ucs2_UrlFile);
				#endif
			#else	// MMI_MAX_SIM_NUM
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_MT_CALL_CARD2_TONE, (WCHAR*)ucs2_UrlFile);
			#endif	// MMI_MAX_SIM_NUM
		#endif	
		}
		break;
		
		case ZMAEE_SETTING_FILE_RINGTONE_ALARM:
			return E_ZM_AEE_FAILURE;
			
		case ZMAEE_SETTING_FILE_RINGTONE_MESSAGE:
		{	
			srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_SMS_TONE, (WCHAR*)ucs2_UrlFile);

		#ifdef __MMI_DUAL_SIM_MASTER__
			#ifdef MMI_MAX_SIM_NUM
				#if (MMI_MAX_SIM_NUM >= 3)
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_SMS_CARD3_TONE, (WCHAR*)ucs2_UrlFile);
				#elif (MMI_MAX_SIM_NUM >= 4)
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_SMS_CARD4_TONE, (WCHAR*)ucs2_UrlFile);
				#else
					srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_SMS_CARD2_TONE, (WCHAR*)ucs2_UrlFile);
				#endif
			#else	// MMI_MAX_SIM_NUM
				srv_prof_set_current_profile_tone_with_file_path(SRV_PROF_SETTINGS_SMS_CARD2_TONE, (WCHAR*)ucs2_UrlFile);
			#endif	// MMI_MAX_SIM_NUM
		#endif
		}
		break;
		
		default:
			return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
#endif
}

/**
 * get phone user profile[情景模式]
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
	E_ZM_AEE_BADPARAM: pProfile为空
 */

static 
int ZMAEE_ISetting_GetUserProfile(AEE_ISetting* po, ZMAEE_SETTING_USERPROFILE* pProfile)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern U8 gactivatedprofile;
	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	if(pThis == NULL || pProfile == NULL)
		return E_ZM_AEE_BADPARAM;

	switch(gactivatedprofile) {
		case MMI_PROFILE_GENERAL:
			*pProfile = ZMAEE_SETTING_PROFILE_GENERAL;
			break;
		case MMI_PROFILE_MEETING:
			*pProfile = ZMAEE_SETTING_PROFILE_MEETING;
			break;
		case MMI_PROFILE_OUTDOOR:
			*pProfile = ZMAEE_SETTING_PROFILE_OUTDOOR;
			break;
		case MMI_PROFILE_HEADSET:
			*pProfile = ZMAEE_SETTING_PROFILE_HEADSET;
			break;
		default:
			*pProfile = ZMAEE_SETTING_PROFILE_OTHER;
	}
 	
	return E_ZM_AEE_SUCCESS;
#else
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	mmi_prof_id_enum prof_id_enum;

	extern srv_prof_internal_profile_enum srv_prof_get_activated_profile(void);

	if(pThis == NULL || pProfile == NULL)
		return E_ZM_AEE_BADPARAM;

	prof_id_enum = (mmi_prof_id_enum)srv_prof_get_activated_profile();

	switch(prof_id_enum) {
		case MMI_PROFILE_GENERAL:
			*pProfile = ZMAEE_SETTING_PROFILE_GENERAL;
			break;
		case MMI_PROFILE_MEETING:
			*pProfile = ZMAEE_SETTING_PROFILE_MEETING;
			break;
		case MMI_PROFILE_OUTDOOR:
			*pProfile = ZMAEE_SETTING_PROFILE_OUTDOOR;
			break;
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
		case MMI_PROFILE_HEADSET:
			*pProfile = ZMAEE_SETTING_PROFILE_HEADSET;
			break;
#endif
		default:
			*pProfile = ZMAEE_SETTING_PROFILE_OTHER;
	}

	return E_ZM_AEE_SUCCESS;
#endif
}


/**
 * set phone user profile[情景模式]
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
 */
static 
int ZMAEE_ISetting_SetUserProfile(AEE_ISetting* po, ZMAEE_SETTING_USERPROFILE nProfile)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern U8 gprofileId;
	extern U8 gactivatedprofile;
	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	short error;
	
	if(pThis == NULL) return E_ZM_AEE_FAILURE;
	switch(nProfile)
	{
		case ZMAEE_SETTING_PROFILE_GENERAL:
			gprofileId = MMI_PROFILE_GENERAL;
			break;
		case ZMAEE_SETTING_PROFILE_MEETING:
			gprofileId = MMI_PROFILE_MEETING;
			break;
		case ZMAEE_SETTING_PROFILE_HEADSET:
			gprofileId = MMI_PROFILE_HEADSET;
			break;
		case ZMAEE_SETTING_PROFILE_OUTDOOR:
			gprofileId = MMI_PROFILE_OUTDOOR;
			break;
		case ZMAEE_SETTING_PROFILE_OTHER:
			gprofileId = MMI_PROFILE_INDOOR;
			break;
		default:
			return E_ZM_AEE_FAILURE;
	}

    gactivatedprofile = gprofileId;
    WriteValue(NVRAM_PROFILES_ACTIVATED, &gactivatedprofile, DS_BYTE, &error);
    ActivateProfile();
	
	return E_ZM_AEE_SUCCESS;
#else
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	int ret = 0;

	if(pThis == NULL) return E_ZM_AEE_FAILURE;

	switch(nProfile) {
		case ZMAEE_SETTING_PROFILE_GENERAL :
			ret = MMI_PROFILE_GENERAL;
			break;
			
		case ZMAEE_SETTING_PROFILE_MEETING :
			ret = MMI_PROFILE_MEETING;
			break;
			
		case ZMAEE_SETTING_PROFILE_OUTDOOR :
			ret = MMI_PROFILE_OUTDOOR;
			break;
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)			
		case ZMAEE_SETTING_PROFILE_HEADSET :
			ret = MMI_PROFILE_HEADSET;
			break;

		case ZMAEE_SETTING_PROFILE_OTHER:
			ret = MMI_PROFILE_MY_STYLE;
			break;
#endif

		default:
			return E_ZM_AEE_BADPARAM;
	}	

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
	mmi_prof_activate_profile((mmi_prof_id_enum)ret, MMI_FALSE);
#else
	srv_prof_activate_profile((srv_prof_internal_profile_enum)ret);
#endif

	return E_ZM_AEE_SUCCESS;
#endif
}


/**
 * set keybord ring tone
 * @bOn  TRUE on, FALSE  off
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
  */
static 
int ZMAEE_ISetting_SetKbdTone(AEE_ISetting* po, int bOn)
{	
	int ret = E_ZM_AEE_FALSE;
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;

	if(pThis == NULL) return E_ZM_AEE_FAILURE;
	
	if(bOn == FALSE)
	{
	#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
		srv_profiles_stop_tone((srv_prof_tone_enum)GetCurKeypadTone());
	#else
		AudioStopReq(GetCurKeypadTone());
	#endif
        mmi_frm_kbd_set_tone_state(MMI_KEY_TONE_DISABLED);
		ret = E_ZM_AEE_SUCCESS;
	}
	else if(bOn == TRUE)
	{
		mmi_frm_kbd_set_tone_state(MMI_KEY_TONE_ENABLED);
		ret = E_ZM_AEE_SUCCESS;
	}
	
	return ret;
}

static
int ZMAEE_ISetting_ScreenHilight_CB(int nHandle, void* pUser)
{
	static int zmaee_backlight_mutex = 0;
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0) 
	if(pUser) {
		if(mmi_gpio_get_backlight_timer_state() != MMI_GPIO_BACKLIGHT_TIMER_STATE_NO_TIMER) {
			TurnOnBacklight(GPIO_BACKLIGHT_PERMANENT);
			zmaee_backlight_mutex = 1;
		}
	} else {
		if(zmaee_backlight_mutex) {
			TurnOffBacklight();
			zmaee_backlight_mutex = 0;
		}
	}
#else
	if(pUser) {
		if(srv_backlight_get_timer_state() != SRV_BACKLIGHT_TIMER_STATE_NO_TIMER) {
			srv_backlight_turn_on(SRV_BACKLIGHT_PERMANENT);
			zmaee_backlight_mutex = 1;
		}
		
		ZMAEE_DebugPrint("ZMAEE_ISetting_ScreenHilight_CB():on: zmaee_backlight_mutex = %d", zmaee_backlight_mutex);
	} else {
		if(zmaee_backlight_mutex) {
			srv_backlight_turn_off();
			zmaee_backlight_mutex = 0;
		}
		
		ZMAEE_DebugPrint("ZMAEE_ISetting_ScreenHilight_CB():off: zmaee_backlight_mutex = %d", zmaee_backlight_mutex);
	}
#endif

	return 0;
}


/**
 * Set LCD always hilight on/off
 * @bOn  TRUE on, FALSE  off
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
 */
static 
int ZMAEE_ISetting_SetScreenAlwaysHilight(AEE_ISetting* po, int bOn)
{	
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0) 
	extern void TurnOnBacklight(gpio_backlight_time_enum);
	extern void TurnOffBacklight(void);
#endif
	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	if(pThis == 0) return E_ZM_AEE_FAILURE;

	ZMAEE_StartPeriodHandler(0, (void*)bOn, ZMAEE_ISetting_ScreenHilight_CB);
		
	return E_ZM_AEE_SUCCESS;
}

/**
 * get current system volume
 * @pVol  [out] for get volume
 * @nMinVol [out] for get volume range,min value
 * @nMaxVol [out] for get volume range max value
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
 * 备注:
 *  pVol, nMinVol, nMaxVol可以为空
 */
static 
int ZMAEE_ISetting_GetSoundVolume(AEE_ISetting* po, int* pVol, int* nMinVol, int* nMaxVol)
{	
	extern aud_context_struct* aud_context_p;
	int volume;
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	
	if(pThis == NULL) 
	{
		return E_ZM_AEE_FAILURE;
	}

	volume = aud_context_p->acoustic_data.volume[aud_context_p->audio_mode][VOL_TYPE_MEDIA];

	//转化过程
	if(MDI_AUD_VOL_IS_EX_LEVEL(volume) && MDI_AUD_VOL_IS_MUTE_LEVEL(volume)) {
		volume -= MDI_AUD_VOL_EX_MUTE(0);
	} else if(MDI_AUD_VOL_IS_EX_LEVEL(volume)) {
		volume -= MDI_AUD_VOL_EX(0);
	} else if(MDI_AUD_VOL_IS_MUTE_LEVEL(volume)) {
		volume -= MDI_AUD_VOL_MUTE(0);
	}

	ZMAEE_DebugPrint("ZMAEE_ISetting_GetSoundVolume():after change, volume = %d\n", volume);

	if(pVol)
		*pVol = volume;
	
	if(nMinVol)
		*nMinVol = ZMAEE_MINVALUE_VOLUME_RANG;

	if(nMaxVol)
		*nMaxVol = ZMAEE_MAXVALUE_VOLUME_RANG;
 	
	return E_ZM_AEE_SUCCESS;

}


/**
 * Set current system volume
 * @nVol  this value should nMinVol<=nVol<=nMaxVol
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
 */
static 
int ZMAEE_ISetting_SetSoundVolume(AEE_ISetting* po, int nVol)
{	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;

	if(pThis == NULL)
		return E_ZM_AEE_FAILURE;
 
	if(nVol <= ZMAEE_MINVALUE_VOLUME_RANG) 
	{
		nVol = ZMAEE_MINVALUE_VOLUME_RANG;
	} 
	else if(nVol >= ZMAEE_MAXVALUE_VOLUME_RANG) 
	{
		nVol = ZMAEE_MAXVALUE_VOLUME_RANG;
	}
	
	nVol = MDI_AUD_VOL_EX_MUTE(nVol);

	mdi_audio_set_volume(VOL_TYPE_MEDIA, nVol);
	sg_zmaee_setting_volume = nVol;
 
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取系统事件
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
	E_ZM_AEE_BADPARAM: pLocalTime为空
 */
int ZMAEE_ISetting_GetSystemTime(AEE_ISetting* po, int* nTimeZone, ZMAEE_DATETIME* pLocalTime)
{	
	MYTIME          sysTime;
	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	if((pThis == 0) || (!pLocalTime))
		return E_ZM_AEE_BADPARAM;

	/*获得当前时间*/
    DTGetRTCTime(&sysTime);
    if( pLocalTime )
    {
        pLocalTime->nYear= sysTime.nYear;
        pLocalTime->nMonth= sysTime.nMonth;
        pLocalTime->nDay= sysTime.nDay;
        pLocalTime->nHour= sysTime.nHour;
        pLocalTime->nMin= sysTime.nMin;
        pLocalTime->nSec= sysTime.nSec;
        pLocalTime->nWeekDay = sysTime.DayIndex;

		return E_ZM_AEE_SUCCESS;
    }
	
	return E_ZM_AEE_FAILURE;
}


/**
 * 设置系统事件
 * RETURN:
	E_ZM_AEE_SUCCESS: 成功
	E_ZM_AEE_FAILURE: 失败
	E_ZM_AEE_BADPARAM: pLocalTime为空
 */
static 
int ZMAEE_ISetting_SetSystemTime(AEE_ISetting* po, int nTimeZone, const ZMAEE_DATETIME* pLocalTime)
{
	extern BOOL SetFromAT;
	MYTIME sysTime;
	int    ret = 0;
	
	ZMAEE_ISETTING* pThis = (ZMAEE_ISETTING*)po;
	if((pThis == 0) || (!pLocalTime))
		return E_ZM_AEE_BADPARAM;

	/*设置时区*/
 	
	memset(&sysTime, 0, sizeof(sysTime));
	/*设置时间*/
	if(pLocalTime)
	{	
       sysTime.nYear = pLocalTime->nYear;
       sysTime.nMonth = pLocalTime->nMonth;
       sysTime.nDay	= pLocalTime->nDay;
       sysTime.nHour= pLocalTime->nHour;
       sysTime.nMin	= pLocalTime->nMin;
       sysTime.nSec	= pLocalTime->nSec;
	   sysTime.DayIndex =  pLocalTime->nWeekDay;
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)	   
	   SetFromAT = TRUE;
	   ret = PhnsetSetDTFromAT(sysTime);
#else
	   mmi_dt_set_rtc_dt(&sysTime);
	   ret = 1;
#endif
	}
	
	if(ret == 0)
	   	return E_ZM_AEE_FAILURE;
	else 
		return E_ZM_AEE_SUCCESS;
	
}

/**
 * 接口开机初始化
 */
void ZMAEE_ISetting_Init(void)
{
	//sg_zmaee_setting_volume = gprofiles[gprofileId].ringVolumeLevel;
	sg_zmaee_setting_volume = aud_context_p->acoustic_data.volume[aud_context_p->audio_mode][VOL_TYPE_MEDIA];
}



/*
*接口音量up和down
*/
int ZMAEE_AudioPlayer_VolumeDown(void)
{
	int ret;
	int volume = 0, max_vol = 0, min_vol = 0;

	ret = ZMAEE_ISetting_GetSoundVolume((AEE_ISetting*)&sg_zmaee_setting, &volume, &min_vol, &max_vol);

	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	if(volume == min_vol) {
		return E_ZM_AEE_FAILURE;
	}

	volume = volume - 1;

	ret = ZMAEE_ISetting_SetSoundVolume((AEE_ISetting*)&sg_zmaee_setting, volume);
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	} else {
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_AUDVOL_IND, 1, 0);
		return E_ZM_AEE_SUCCESS;
	}
	
}

int ZMAEE_AudioPlayer_VolumeUp(void)
{
	int ret;
	int volume = 0, max_vol = 0, min_vol = 0;

	ret = ZMAEE_ISetting_GetSoundVolume((AEE_ISetting*)&sg_zmaee_setting, &volume, &min_vol, &max_vol);

	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	if(volume == max_vol) {
		return E_ZM_AEE_FAILURE;
	}

	volume = volume + 1;

	ret = ZMAEE_ISetting_SetSoundVolume((AEE_ISetting*)&sg_zmaee_setting, volume);	 	
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	} else {
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_AUDVOL_IND, 0, 0);
		return E_ZM_AEE_SUCCESS;
	}
}

void ZMAEE_ISetting_SetPenEventParam(AEE_ISetting *po, int *pen_sample, int *stroke_sample, int *pen_based_offset, int *stroke_based_offset, int *pen_longtap_offset, int *stroke_longtap_offset)
{
#ifdef __MMI_TOUCH_SCREEN__
#ifndef WIN32   //WLJ
	extern TouchPanelDataStruct TP;

	int prev_pen_sample = TP.low_sample_period;
	int prev_stroke_sample = TP.high_sample_period;
	int prev_pen_based_offset = TP.pen_offset;
	int prev_stroke_based_offset = TP.storke_offset;
	int prev_pen_longtap_offset = TP.longtap_pen_offset;
	int prev_stroke_longtap_offset = TP.longtap_stroke_offset;

	if(pen_sample) {
		prev_pen_sample = *pen_sample;
		*pen_sample = TP.low_sample_period;
	}

	if(stroke_sample) {
		prev_stroke_sample = *stroke_sample;
		*stroke_sample = TP.high_sample_period;
	}

	if(pen_based_offset) {
		prev_pen_based_offset = *pen_based_offset;
		*pen_based_offset = TP.pen_offset;
	}

	if(stroke_based_offset) {
		prev_stroke_based_offset = *stroke_based_offset;
		*stroke_based_offset = TP.storke_offset;
	}

	if(pen_longtap_offset) {
		prev_pen_longtap_offset = *pen_longtap_offset;
		*pen_longtap_offset = TP.longtap_pen_offset;
	}

	if(stroke_longtap_offset) {
		prev_stroke_longtap_offset = *stroke_longtap_offset;
		*stroke_longtap_offset = TP.longtap_stroke_offset;
	}

	mmi_pen_config_sampling_period((kal_uint32)prev_pen_sample, (kal_uint32)prev_stroke_sample);
	mmi_pen_config_move_offset((kal_uint32)prev_pen_based_offset, (kal_uint32)prev_stroke_based_offset, 
								(kal_uint32)prev_pen_longtap_offset, (kal_uint32)prev_stroke_longtap_offset);
#endif		
#endif
}

/**
 * 105版本接口
 *
 * 获取系统的日期时间格式
 * @time_format			输出参数，时间格式，指针可以为空
 * @date_format			输出参数，日期格式，指针可以为空
 * @seprator				输出参数，日期分隔符，编码格式为UTF8，指针可以为空
 * @size					seprator的字节长度
 */
void ZMAEE_ISetting_GetDateTimeFormat(AEE_ISetting *po, ZMAEE_DATETIME_TYPE *time_format, ZMAEE_DATETIME_TYPE *date_format, unsigned char *seprator, unsigned int size)
{
	extern unsigned char PhnsetGetTimeFormat(void);
	extern unsigned char PhnsetGetDateSeperatorFormat(void);
	extern unsigned char PhnsetGetDateFormat(void);

	if(time_format) {
		switch(PhnsetGetTimeFormat()) {
			case 0:
				*time_format = ZMAEE_DATETIME_TIME_12;
				break;

			case 1:
			default:
				*time_format = ZMAEE_DATETIME_TIME_24;
		}
	}

	if(date_format) {
		switch(PhnsetGetDateFormat()) {
			case 0:
				*date_format = ZMAEE_DATETIME_DATE_DMY;
				break;

			case 1:
				*date_format = ZMAEE_DATETIME_DATE_MDY;
				break;

			case 2:
			default:
				*date_format = ZMAEE_DATETIME_DATE_YMD;
		}
	}

	if(seprator) {
		switch(PhnsetGetDateSeperatorFormat()) {
			case 1:
				strncpy(seprator, ":", size);
				break;

			case 2:
				strncpy(seprator, "/", size);
				break;

			case 3:
				strncpy(seprator, "-", size);
				break;

			case 0:
			default:
				strncpy(seprator, ".", size);
		}
	}
}


#endif// __ZMAEE_APP__
