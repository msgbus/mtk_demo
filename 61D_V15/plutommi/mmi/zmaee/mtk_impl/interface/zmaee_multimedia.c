#ifdef __ZMAEE_APP__


#include "mdi_datatype.h"
#include "Mdi_audio.h"
#include "Mdi_datatype.h"
#include "Device.h"
#include "med_main.h"
#include "fs_errcode.h"
#include "GameFramework.h"
#include "GeneralDeviceGprot.h"
#include "idleappresdef.h"
#include "mainmenudef.h"

#ifdef __MMI_MEDIA_PLAYER__
#include "mediaplayerprot.h"
#include "mediaplayerenumdef.h"
#endif

#include "zmaee_priv.h"


#define ZMAEE_IMEDIA_FUNC_COUNT			(23)

static int ZMAEE_IMedia_AddRef(AEE_IMedia* po);
static int ZMAEE_IMedia_Release(AEE_IMedia* po);
static unsigned long ZMAEE_IMedia_GetMediaType(AEE_IMedia* po);
static unsigned long ZMAEE_IMedia_GetMediaCap(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundPlay(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser);
static int ZMAEE_IMedia_SoundClose(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundPause(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundResume(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundGetDuration(AEE_IMedia* po, const char* file);
static int ZMAEE_IMedia_SoundGetProgress(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundSetProgress(AEE_IMedia* po, int progress);
static int ZMAEE_IMedia_StartSoundRecord(AEE_IMedia* po, ZMAEE_MediaType type, 
								const char* filename, ZMAEE_MEDIA_PFNCB pfn, void*pUser);
static int ZMAEE_IMedia_StartSoundRecordMem(AEE_IMedia* po, ZMAEE_MediaType type, 
								char* buf, int maxLen, ZMAEE_MEDIA_RECMEMCB pfn, void*pUser);
static int ZMAEE_IMedia_StopSoundRecord(AEE_IMedia* po);
static int ZMAEE_IMedia_MmaOpen(AEE_IMedia* po,ZMAEE_MediaType type, 
										   const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser);
static int ZMAEE_IMedia_MmaPlay(AEE_IMedia* po, int handle);
static int ZMAEE_IMedia_MmaPause(AEE_IMedia* po,int handle);
static int ZMAEE_IMedia_MmaResume(AEE_IMedia* po,int handle);
static int ZMAEE_IMedia_MmaStop(AEE_IMedia* po,int handle);
static int ZMAEE_IMedia_MmaClose(AEE_IMedia* po,int handle);
static int ZMAEE_IMedia_Mp3IsPlaying(AEE_IMedia* po);
static int ZMAEE_IMedia_SoundPlayEx(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB_EX pfn, void* pUser);
static int ZMAEE_IMedia_SoundCloseEx(AEE_IMedia* po, int handle);


extern int ZMAEE_Utf8_2_Ucs2(const char* src, int len, unsigned short* wcsDst, int nSize);
extern int   ZMAEE_Callback_Valid(ZMAEE_CALLBACK* pCb);
extern void ZMAEE_Callback_Init(ZMAEE_CALLBACK* pCb, void* pfn, void* pUser);
int ZMAEE_IMedia_StartSoundMem_Record_Flag(void);
int ZMAEE_IMedia_StartSoundMem_Record(unsigned char* buf_p,unsigned int len, unsigned int *record_len);
static int ZMAEE_IMedia_ClearBKG_And_PlayInfo(AEE_IMedia* po, int handle);



zm_extern void* 	zmaee_memset(void * dest, char c, unsigned int count );
zm_extern void 		ZMAEE_DebugPrint(const char *pszFormat, ...);
#ifdef WIN32
zm_extern int 		ZMAEE_Background_IsRunning(char *dll_name);
zm_extern int		AEE_IMedia_ResSoundPlay(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser);
#endif
typedef enum{
	ZMAEE_MEDIA_STAT_IDLE = 0,
	ZMAEE_MEDIA_STAT_PLAYING,
	ZMAEE_MEDIA_STAT_PAUSED ,
	ZMAEE_MEDIA_STAT_STOP,
	ZMAEE_MEDIA_STAT_RECORD,
	ZMAEE_MEDIA_STAT_OPEN
 }zmaee_media_stat;
typedef int ZMAEE_MediaState;

typedef struct {
	ZMAEE_CALLBACK		mma_cb;
	int					mma_hdlr;
	int					mma_stat;
	ZMAEE_MediaType		mma_type;
}ZMAEE_MMACB;


#define 	ZMAEE_SOUNDPLAY_SPACE_COUNT				4
typedef struct {
	ZMAEE_CALLBACK			callback[ZMAEE_SOUNDPLAY_SPACE_COUNT];
	int 		   			bg_hdlr[ZMAEE_SOUNDPLAY_SPACE_COUNT];
	int						stack_index[ZMAEE_SOUNDPLAY_SPACE_COUNT];
	int 					curr_play;
	int						active_close;
}ZMAEE_Background;

typedef struct {
	void 			   *pVtbl;
	int    		 		nRef;

	// SMA
	int					sma_handle;
	ZMAEE_MediaType		sma_type;
	ZMAEE_MediaState	sma_state;		
	ZMAEE_CALLBACK		sma_callback;

	// MMA
	ZMAEE_MediaState	mma_state;
	ZMAEE_MediaType		mma_type;
	ZMAEE_MMACB			mma_callback[AEE_MAX_MMA_HANDLE];

	//Background
	ZMAEE_Background	sma_background;
}ZMAEE_IMEDIA;

extern int sg_zmaee_setting_volume;

typedef struct{
	int				record_flag ;
	unsigned char	*record_buf ;
	unsigned int	record_max_size ;
	unsigned int	record_size;
	unsigned int	record_total_size;
	int				is_stoping;
}ZMAEE_MEDIA_RECORD_BUF;

ZMAEE_MEDIA_RECORD_BUF zmaee_record_buf =  {0};
static ZMAEE_IMEDIA sg_zmaee_media = {0};


int ZMAEE_IMedia_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_IMEDIA* pThis = 0;
	pThis = &sg_zmaee_media;
	
	/*初始化*/
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IMEDIA_FUNC_COUNT,
											ZMAEE_IMedia_AddRef,
											ZMAEE_IMedia_Release,
											ZMAEE_IMedia_GetMediaType,
											ZMAEE_IMedia_GetMediaCap,
											ZMAEE_IMedia_SoundPlay,
											ZMAEE_IMedia_SoundClose,
											ZMAEE_IMedia_SoundPause,
											ZMAEE_IMedia_SoundResume,
											ZMAEE_IMedia_SoundGetDuration,
											ZMAEE_IMedia_SoundGetProgress,
											ZMAEE_IMedia_SoundSetProgress,
											ZMAEE_IMedia_StartSoundRecord,
											ZMAEE_IMedia_StartSoundRecordMem,
											ZMAEE_IMedia_StopSoundRecord,
											ZMAEE_IMedia_MmaOpen,
											ZMAEE_IMedia_MmaPlay,
											ZMAEE_IMedia_MmaPause,
											ZMAEE_IMedia_MmaResume,
											ZMAEE_IMedia_MmaStop,
											ZMAEE_IMedia_MmaClose,
											ZMAEE_IMedia_Mp3IsPlaying,
											ZMAEE_IMedia_SoundPlayEx,
											ZMAEE_IMedia_SoundCloseEx);
		}
	}
	/*状态等初始化*/
	if(pThis->nRef == 0) {
		pThis->nRef = 1;
		pThis->sma_type = 0;
		pThis->sma_state = ZMAEE_MEDIA_STAT_IDLE;

		// MMA State Initialization
		pThis->mma_type = 0;
		pThis->mma_state = ZMAEE_MEDIA_STAT_IDLE;

		//background
		pThis->sma_background.curr_play = 0;
		pThis->sma_background.active_close = 0;
	}
	
	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

static int ZMAEE_IMedia_AddRef(AEE_IMedia* po)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
		return E_ZM_AEE_SUCCESS;
}

static int ZMAEE_IMedia_Release(AEE_IMedia* po)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
		
	return E_ZM_AEE_SUCCESS;
}

/**
* get support media type
*/	
static unsigned long ZMAEE_IMedia_GetMediaType(AEE_IMedia* po)
{	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	unsigned long support_type = 0;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	support_type |= ZMAEE_MEDIATYPE_MIDI | ZMAEE_MEDIATYPE_AMR | ZMAEE_MEDIATYPE_WAV | ZMAEE_MEDIATYPE_MP3 | ZMAEE_MEDIATYPE_PCM;
	return support_type;
}


/**
* get media capabilities
*/

static unsigned long ZMAEE_IMedia_GetMediaCap(AEE_IMedia* po)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	unsigned long capability = 0;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	capability |= ZMAEE_MC_SOUNDPLAY;
	capability |= ZMAEE_MC_SOUNDRECORD;
#ifdef DSP_WT_SYN
	capability |= ZMAEE_MC_MULTICHANNEL;
#endif

	return capability;
}

//single sound ----------------------------------------------------------------------------------------------------------------
/**
* play single sound 
*/
static void ZMAEE_Soundplay_Callback(mdi_result result)
{	
	ZMAEE_MEDIA_PFNCB callback;
	void *pUser;
	
	ZMAEE_IMedia_ClearBKG_And_PlayInfo((AEE_IMedia *)&sg_zmaee_media, sg_zmaee_media.sma_background.bg_hdlr[0]);

	sg_zmaee_media.sma_state = ZMAEE_MEDIA_STAT_IDLE;
	if(ZMAEE_Callback_Valid(&(sg_zmaee_media.sma_callback)) != 1) {
		return;
	}

	callback = (ZMAEE_MEDIA_PFNCB)(sg_zmaee_media.sma_callback.pfn);
	pUser = sg_zmaee_media.sma_callback.pUser;
	
	switch(result)
	{	
		/*传参接收到的参数*/
		case MDI_AUDIO_END_OF_FILE:		
		case MDI_AUDIO_TERMINATED:	
		case MDI_AUDIO_SUCCESS:	
			if(callback) {
				callback(pUser, 0, ZMAEE_MEDIA_COMPLETE);
			}
			break;
			
		case MDI_AUDIO_DISC_FULL:
			if(callback) {
				callback(pUser, 0, ZMAEE_MEDIA_FULL);
			}
			break;

		case MDI_AUDIO_RESUME:
			break;
			
		default:
			if(callback) {
				callback(pUser, 0, ZMAEE_MEDIA_FAIL);
			}
			break;
	}
	
}


static int ZMAEE_IMedia_SoundPlay(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser)
{
	int ret;

	ret = ZMAEE_IMedia_SoundPlayEx(po, type, ptr, len, repeat, NULL, pUser);	
	if(ret > 0) {
		ZMAEE_Callback_Init(&sg_zmaee_media.sma_callback, (void*)pfn, pUser);
		return E_ZM_AEE_SUCCESS;
	} else {
		return E_ZM_AEE_FAILURE;
	}
}


static int ZMAEE_IMedia_SoundClose(AEE_IMedia* po)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_FAILURE;
	}
	
	return ZMAEE_IMedia_SoundCloseEx(po, pThis->sma_background.bg_hdlr[0]);
}


/**
* PauseSound
*/

static  void ZMAEE_IMedia_Pause_Callback(int result)
{	
}


static int ZMAEE_IMedia_SoundPause(AEE_IMedia* po)
{
	mdi_result result;
	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	if(pThis->sma_state != ZMAEE_MEDIA_STAT_PLAYING)
		return E_ZM_AEE_FAILURE;
	/*audio pause*/
	
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	result = mdi_audio_pause((mdi_ext_callback)ZMAEE_IMedia_Pause_Callback, NULL);
#else
	result = mdi_audio_pause(0, ZMAEE_IMedia_Pause_Callback);
#endif
	
	if(result == MDI_AUDIO_SUCCESS) {
		pThis->sma_state = ZMAEE_MEDIA_STAT_PAUSED;
		return E_ZM_AEE_SUCCESS;
	} else
		return E_ZM_AEE_FAILURE;
}

/**
* Resume Sound恢复播放音频
*/
static void ZMAEE_IMedia_Resume_Callback(int result)
{	
}

static void ZMAEE_Soundplay_CallbackEx(mdi_result result);

static int ZMAEE_IMedia_SoundResume(AEE_IMedia* po)
{
	mdi_result result;
	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	if(pThis->sma_state != ZMAEE_MEDIA_STAT_PAUSED)
		return E_ZM_AEE_FAILURE;
	/*audio resume*/
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	result = mdi_audio_resume((mdi_ext_callback)ZMAEE_Soundplay_CallbackEx, NULL);
#else
	result = mdi_audio_resume(0, ZMAEE_Soundplay_CallbackEx);
#endif
	
	if(result == MDI_AUDIO_SUCCESS) {
		pThis->sma_state = ZMAEE_MEDIA_STAT_PLAYING;
		return E_ZM_AEE_SUCCESS;
	} else
		return E_ZM_AEE_FAILURE;
}

/**
* get audio file duration获得音频文件播放总时间
*/

static int ZMAEE_IMedia_SoundGetDuration(AEE_IMedia* po, const char* file)
{	
	mdi_result result;
	int time=0;
	unsigned short wcs_file[AEE_MAX_PATH_NAME]={0};

	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0) return E_ZM_AEE_BADPARAM;
	/*get duration*/
	ZMAEE_Utf8_2_Ucs2(file,strlen(file),wcs_file,sizeof(wcs_file)/sizeof(wcs_file[0]));
	result = mdi_audio_get_duration(wcs_file, (U32*)&time);
	
	if(result == MDI_AUDIO_SUCCESS)
		return time;
	else
		return E_ZM_AEE_FAILURE;
}

/**
* get sound progress 获得音频播放进度(此功能仅限于文件播放)
*/

static int ZMAEE_IMedia_SoundGetProgress(AEE_IMedia* po)
{
	mdi_result result;
	kal_uint32 progress_time;

	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0)  return E_ZM_AEE_BADPARAM;
		
	if(pThis->sma_state == ZMAEE_MEDIA_STAT_PLAYING || 
		pThis->sma_state == ZMAEE_MEDIA_STAT_PAUSED )
	{	
		/*get progress*/
		result = mdi_audio_get_progress_time(&progress_time);
		if(result == MDI_AUDIO_SUCCESS)
			return (int)progress_time;
		else 
			return E_ZM_AEE_FAILURE;
	}
	else 
	{
		return E_ZM_AEE_FAILURE;
	}
}

/**
* set sound progress设置当前视频播放的进度
*/

static int ZMAEE_IMedia_SoundSetProgress(AEE_IMedia* po, int progress)
{	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	mdi_result ret =0;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	if(pThis->sma_state == ZMAEE_MEDIA_STAT_PLAYING || 
		pThis->sma_state == ZMAEE_MEDIA_STAT_PAUSED )
	{
		ret= mdi_audio_set_progress_time((kal_uint32)progress);
		if(ret == MDI_AUDIO_SUCCESS)
			return E_ZM_AEE_SUCCESS;
		else 
			return E_ZM_AEE_FAILURE;
		
	}
	else 
		return E_ZM_AEE_FAILURE;
}

//record---这里是录音失败后的状态----------------------------------------------------------------------------------------------------------
static void ZMAEE_IMedia_Record_Callback(mdi_result result)
{
	ZMAEE_MEDIA_PFNCB callback;
	void *pUser;

	sg_zmaee_media.sma_state = ZMAEE_MEDIA_STAT_IDLE;
	if(ZMAEE_Callback_Valid(&(sg_zmaee_media.sma_callback)) != 1) {
		return;
	}

	callback = (ZMAEE_MEDIA_PFNCB)(sg_zmaee_media.sma_callback.pfn);
	pUser = sg_zmaee_media.sma_callback.pUser;
	
	switch(result)
	{
		case MDI_AUDIO_END_OF_FILE:
		case MDI_AUDIO_SUCCESS:
		case MDI_AUDIO_TERMINATED:
			if(callback)
				callback(pUser, 0,ZMAEE_MEDIA_COMPLETE);
			break;
		case MDI_AUDIO_DISC_FULL:
			if(callback)
				callback(pUser,0,ZMAEE_MEDIA_FULL);
			break;
		default:
			if(callback)
				callback(pUser,0,ZMAEE_MEDIA_FAIL);
			break;
	}
	
}

/*录音只有AMR的*/
static int ZMAEE_IMedia_StartSoundRecord(AEE_IMedia* po, ZMAEE_MediaType type, 
								const char* filename, ZMAEE_MEDIA_PFNCB pfn, void*pUser)
{	
	U8 	  format;
	unsigned short  wcs_filepath[AEE_MAX_PATH_NAME]={0};
	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	if((pThis->sma_state != ZMAEE_MEDIA_STAT_IDLE) || 
		(pThis->mma_state != ZMAEE_MEDIA_STAT_IDLE))
		return E_ZM_AEE_FAILURE;
	
	if(type == ZMAEE_MEDIATYPE_AMR)
	{
		format = MDI_FORMAT_AMR;
	}
	else if(type == ZMAEE_MEDIATYPE_WAV)
	{
		format = MDI_FORMAT_WAV;
	}
	else if(type == ZMAEE_MEDIATYPE_PCM)
	{
		format = MDI_FORMAT_PCM_8K;
	}
	else 
	{
		return E_ZM_AEE_FAILURE;
	}

	ZMAEE_Utf8_2_Ucs2(filename,strlen(filename),wcs_filepath,sizeof(wcs_filepath)/sizeof(wcs_filepath[0]));

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	if(mdi_audio_start_record(wcs_filepath, format, 0, (mdi_ext_callback)ZMAEE_IMedia_Record_Callback, NULL) == MDI_AUDIO_SUCCESS)
#else
	if(mdi_audio_start_record(wcs_filepath, format, 0, NULL, (mdi_callback)ZMAEE_IMedia_Record_Callback) == MDI_AUDIO_SUCCESS)
#endif		
	{	
		pThis->sma_type = type;
		ZMAEE_Callback_Init(&(pThis->sma_callback), (void*)pfn, pUser);
		pThis->sma_state = ZMAEE_MEDIA_STAT_RECORD;
		return E_ZM_AEE_SUCCESS;
	}
	else {
		return E_ZM_AEE_FAILURE;
	}
		
}

/*内存录音后的状态*/

static void ZMAEE_IMedia_MemRecord_Callback(mdi_result result)
{
	ZMAEE_MEDIA_RECMEMCB callback;
	void *pUser;

	zmaee_record_buf.record_flag = 0;	
	sg_zmaee_media.sma_state = ZMAEE_MEDIA_STAT_IDLE;
	
	if(ZMAEE_Callback_Valid(&(sg_zmaee_media.sma_callback)) != 1)
	{	
		return;
	}

	callback = (ZMAEE_MEDIA_RECMEMCB)(sg_zmaee_media.sma_callback.pfn);
	pUser = sg_zmaee_media.sma_callback.pUser;

	ZMAEE_DebugPrint("ZMAEE_IMedia_MemRecord_Callback: result = %d", result);

	switch(result)
	{	
		case MDI_AUDIO_SUCCESS:
		case MDI_AUDIO_TERMINATED:
		case MDI_AUDIO_END_OF_FILE:
		{			
			if(callback)
				callback(pUser,0,ZMAEE_MEDIA_COMPLETE, &zmaee_record_buf.record_buf, &zmaee_record_buf.record_size);
		}
			break;
		case MDI_AUDIO_DISC_FULL:
			if(callback)
				callback(pUser,0,ZMAEE_MEDIA_FULL, &zmaee_record_buf.record_buf, &zmaee_record_buf.record_size);
			break;
		default:
			if(callback)
				callback(pUser,0,ZMAEE_MEDIA_FAIL, 0, 0);
			break;
	}
	
}


/*
*录成BUF
*
*
*/
static int ZMAEE_IMedia_StartSoundRecordMem(AEE_IMedia* po, ZMAEE_MediaType type, 
								char* buf, int maxLen, ZMAEE_MEDIA_RECMEMCB pfn, void*pUser)
{	
	U8				format;
	unsigned short  *wcs_filepath; 
	ZMAEE_IMEDIA*	pThis = (ZMAEE_IMEDIA*)po;
	mdi_result		rec_ret;
	
	if(pThis == 0)
	{	
		return E_ZM_AEE_BADPARAM;
	}
	
	if((pThis->sma_state != ZMAEE_MEDIA_STAT_IDLE) || 
		(pThis->mma_state != ZMAEE_MEDIA_STAT_IDLE)) 
	{
		ZMAEE_DebugPrint("ZMAEE_IMedia_StartSoundRecordMem: invalid state, stat = %d", pThis->sma_state);
		return E_ZM_AEE_FAILURE;
	}
	
	if(type == ZMAEE_MEDIATYPE_AMR)
	{
		format = MDI_FORMAT_AMR;
		wcs_filepath = L"c:\\zmaee\\zmdata\\memrecord.amr";
	}
	else if(type == ZMAEE_MEDIATYPE_WAV)
	{
		format = MDI_FORMAT_WAV;
		wcs_filepath = L"c:\\zmaee\\zmdata\\memrecord.wav";
	}
	else if(type == ZMAEE_MEDIATYPE_PCM)
	{
		format = MDI_FORMAT_PCM_8K;
		wcs_filepath = L"c:\\zmaee\\zmdata\\memrecord.pcm";
	}
	else 
	{
		ZMAEE_DebugPrint("ZMAEE_IMedia_StartSoundRecordMem: invalid format, format = %d", type);
		return E_ZM_AEE_FAILURE;
	}

	zmaee_record_buf.record_flag = 1;
	zmaee_record_buf.is_stoping = 0;
	zmaee_record_buf.record_buf = (unsigned char*)buf;
	zmaee_record_buf.record_size = 0;
	zmaee_record_buf.record_max_size = maxLen;
	zmaee_record_buf.record_total_size = 0;

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	rec_ret = mdi_audio_start_record(wcs_filepath, format, 0, (mdi_ext_callback)ZMAEE_IMedia_MemRecord_Callback, NULL);
#else
	rec_ret = mdi_audio_start_record(wcs_filepath, format, 0, NULL, (mdi_callback)ZMAEE_IMedia_MemRecord_Callback);
#endif
	if(rec_ret == MDI_AUDIO_SUCCESS)
	{	
		/*初始化*/
		pThis->sma_type = type;
		ZMAEE_Callback_Init(&(pThis->sma_callback), (void*)pfn, pUser);
		pThis->sma_state = ZMAEE_MEDIA_STAT_RECORD;

		return E_ZM_AEE_SUCCESS;
	}
	else 
	{
		ZMAEE_DebugPrint("ZMAEE_IMedia_StartSoundRecordMem: rec_ret = %d", rec_ret);
		zmaee_record_buf.record_flag = 0;
		zmaee_record_buf.record_buf = 0;
		zmaee_record_buf.record_size = 0;
		zmaee_record_buf.record_max_size = 0;
		zmaee_record_buf.record_total_size = 0;
		return E_ZM_AEE_FAILURE;
	}

}

int ZMAEE_IMedia_StartSoundMem_Record_Flag(void)
{
	return  zmaee_record_buf.record_flag;
}


/**
*buf_p  拷贝首地址
*len 需要拷贝的长度
*
*/

int ZMAEE_IMedia_StartSoundMem_Record(unsigned char* buf_p,unsigned int len, unsigned int *record_len)
{
	unsigned int cpy_len = len;
	ZMAEE_MEDIA_RECMEMCB callback;
	void* pUser; 

	callback = (ZMAEE_MEDIA_RECMEMCB)sg_zmaee_media.sma_callback.pfn;
	pUser = sg_zmaee_media.sma_callback.pUser;

	if(!callback)
		return -1;

	if(zmaee_record_buf.record_size + cpy_len > zmaee_record_buf.record_max_size) 
	{
		if(callback)
		{
			int result;
			result = callback(pUser, 0, ZMAEE_MEDIA_RECMEM_FRAME_FULL, &zmaee_record_buf.record_buf, &zmaee_record_buf.record_size);
			if(result == 0) 
			{
				return FS_DISK_FULL;
			}
			else
			{
				if(result == 1)
				{
					zmaee_record_buf.record_max_size = zmaee_record_buf.record_size;
					zmaee_record_buf.record_size = 0;
					if(zmaee_record_buf.record_size + cpy_len > zmaee_record_buf.record_max_size)
					{
						return FS_DISK_FULL;
					}
				}
			}
		}
	}

	ZMAEE_DebugPrint("ZMAEE_IMedia_StartSoundMem_Record: len = %d, cpy_len = %d", len, cpy_len);
	ZMAEE_DebugPrint("ZMAEE_IMedia_StartSoundMem_Record: record_size = %d, record_max_size = %d", zmaee_record_buf.record_size, zmaee_record_buf.record_max_size);
		
	if(cpy_len > 0) {
		memcpy(zmaee_record_buf.record_buf + zmaee_record_buf.record_size, buf_p, cpy_len);
		zmaee_record_buf.record_size += cpy_len;
		*record_len = cpy_len;
		zmaee_record_buf.record_total_size += cpy_len;
		
		return FS_NO_ERROR;
	} 
	else 
	{
		return FS_ERROR_RESERVED;
	}
}

unsigned int ZMAEE_IMedia_RecordMem_TotalSize(void)
{
	return zmaee_record_buf.record_total_size;
}

int ZMAEE_IMedia_StartSoundMem_Record_Idx(unsigned char* buf_p,unsigned int len, unsigned int *record_len, int offset)
{
	unsigned int cpy_len = len;
	ZMAEE_MEDIA_RECMEMCB callback;
	void* pUser; 

	callback = (ZMAEE_MEDIA_RECMEMCB)sg_zmaee_media.sma_callback.pfn;
	pUser = sg_zmaee_media.sma_callback.pUser;

	if(!callback)
		return -1;

	if(sg_zmaee_media.sma_type == ZMAEE_MEDIATYPE_WAV)
	{
		if(callback)
		{
			callback(pUser, 0, ZMAEE_MEDIA_HEAD_DATA_NOTIFY, &buf_p, &len);
			*record_len = cpy_len;
			
			return 0;
		}
		
		return -1;
	}

	return -1;

}



/*stop sound record停止录音的时候，将标志量复位*/
static int ZMAEE_IMedia_StopSoundRecord(AEE_IMedia* po)
{	
	mdi_result ret = 0;
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	ZMAEE_DebugPrint("ZMAEE_IMedia_StopSoundRecord: sma_state = %d", pThis->sma_state);
	if(pThis->sma_state == ZMAEE_MEDIA_STAT_RECORD) {
		ret = mdi_audio_stop_record();
		if(ret == MDI_AUDIO_SUCCESS)
	    {	
	    	sg_zmaee_media.sma_state = ZMAEE_MEDIA_STAT_IDLE;
			
	        return E_ZM_AEE_SUCCESS;
	    } else	    	
			return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

//multi channel sound----------------------------------------------------------------------------------------------------------------
static
ZMAEE_MMACB* ZMAEE_IMedia_GetFreeCB(void)
{
	int i;

	for(i = 0; i < AEE_MAX_MMA_HANDLE; i++) {
		if(sg_zmaee_media.mma_callback[i].mma_cb.pfn == NULL) {
			return &sg_zmaee_media.mma_callback[i];
		}
	}

	return NULL;
}

static
ZMAEE_MMACB* ZMAEE_IMedia_GetCBByHdlr(int handle)
{
	int i;

	for(i = 0; i < AEE_MAX_MMA_HANDLE; i++) {
		if(sg_zmaee_media.mma_callback[i].mma_hdlr == handle) {
			return &sg_zmaee_media.mma_callback[i];
		}
	}

	return NULL;
}

static
int ZMAEE_IMedia_GetValidCBCount(void)
{
	int count, i;

	for(i = 0, count = 0; i < AEE_MAX_MMA_HANDLE; i++) {
		if(sg_zmaee_media.mma_callback[i].mma_cb.pfn) {
			count++;
		}
	}

	return count;
}

/**
* RETURN:
mma_handle  if success
0 if failue
*/

static void ZMAEE_IMedia_Open_Callback(kal_int32 handle, kal_int32 result)
{	
	ZMAEE_MEDIA_PFNCB callback;
	void *pUser;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);

	if(!mma_cb) {
		return;
	}

	mma_cb->mma_stat = ZMAEE_MEDIA_STAT_STOP;
	if(ZMAEE_Callback_Valid(&(mma_cb->mma_cb)) != 1) {
		return;
	}
	
	callback = (ZMAEE_MEDIA_PFNCB)(mma_cb->mma_cb.pfn);
	pUser = mma_cb->mma_cb.pUser;

	if(result != MDI_AUDIO_SUCCESS)
	{
		if(result == MDI_AUDIO_DISC_FULL) {
		   	if(callback)
		   		callback(pUser, handle, ZMAEE_MEDIA_FULL);
		} else if((result == MDI_AUDIO_END_OF_FILE) || (result == MDI_AUDIO_TERMINATED)) {
			if(callback)
				callback(pUser, handle, ZMAEE_MEDIA_COMPLETE);
		} else {
		   	if(callback)
		   		callback(pUser, handle, ZMAEE_MEDIA_FAIL);
		}
	} else {
		if(callback)
			callback(pUser, handle, ZMAEE_MEDIA_COMPLETE);
	}
}


/*要将我们的type转化成系统的type，系统函数才可以调用*/
static int ZMAEE_IMedia_MmaOpen(AEE_IMedia* po,ZMAEE_MediaType type, 
					const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser)
{	
	U8 format = 0;
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	ZMAEE_MMACB *mma_cb;
	if(pThis == 0) return E_ZM_AEE_BADPARAM;

	// MMA,SMA互斥
	if(pThis->sma_state != ZMAEE_MEDIA_STAT_IDLE) {
		return E_ZM_AEE_ITEMBUSY;
	}

	// 只支持同一种声音类型的文件
	if((pThis->mma_type != 0) && (pThis->mma_type != type)) {
		return E_ZM_AEE_BADPARAM;
	}

	// 第一次MmaOpen时设置type
	if(pThis->mma_type == 0) {
		pThis->mma_type = type;
	}

	// 获取MMACB的槽位
	mma_cb = ZMAEE_IMedia_GetFreeCB();
	if(!mma_cb) {
		ZMAEE_DebugPrint("ZMAEE_IMedia_MmaOpen: get mma_cb item failed");
		return E_ZM_AEE_FAILURE;
	}

	// 类型转换
	switch(type)
	{
		case ZMAEE_MEDIATYPE_MIDI:
			format = MDI_FORMAT_SMF;
			break;
		case ZMAEE_MEDIATYPE_AMR:
			format = MDI_FORMAT_AMR;
			break;
		case ZMAEE_MEDIATYPE_WAV:
			format = MDI_FORMAT_WAV;
			break;
		case ZMAEE_MEDIATYPE_MP3:
			format = MDI_FORMAT_DAF;
			break;
		case ZMAEE_MEDIATYPE_FILE:		// MMA不支持File类型
			return E_ZM_AEE_BADPARAM;
		default:
			return E_ZM_AEE_BADPARAM;
	}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	mma_cb->mma_hdlr = mdi_audio_mma_open_string(0, (void*)ptr, len, format, repeat, (mdi_mma_callback)ZMAEE_IMedia_Open_Callback, NULL);//WLJ
#else
	mma_cb->mma_hdlr = mdi_audio_mma_open_string((void*)ptr, len, format, repeat, ZMAEE_IMedia_Open_Callback);
#endif
	if(mma_cb->mma_hdlr != 0) {
		mma_cb->mma_type = type;
		pThis->mma_state = ZMAEE_MEDIA_STAT_OPEN;

		ZMAEE_Callback_Init(&(mma_cb->mma_cb), (void*)pfn, pUser);			
		return mma_cb->mma_hdlr;
	}

	ZMAEE_DebugPrint("ZMAEE_IMedia_MmaOpen: mma_cb->mma_hdlr = %d", mma_cb->mma_hdlr);
	return E_ZM_AEE_FAILURE;
}

/**
*@handle  MmaOpen return valid handle
* RETURN:   mdi_result型的话
E_ZM_AEE_SUCCESS
E_ZM_AEE_FAILURE
*/

static int ZMAEE_IMedia_MmaPlay(AEE_IMedia* po, int handle)
{	
	mdi_result result = MDI_AUDIO_FAIL;	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);
	
	if(pThis == 0 || mma_cb == 0) 
		return E_ZM_AEE_BADPARAM;
	
	result = mdi_audio_mma_play(handle);
	if(result != MDI_AUDIO_SUCCESS)
		return 	E_ZM_AEE_FAILURE;
	
	mma_cb->mma_stat = ZMAEE_MEDIA_STAT_PLAYING;
	
	return E_ZM_AEE_SUCCESS;
}

/**
*@handle  MmaOpen return valid handle
* RETURN:
E_ZM_AEE_SUCCESS
E_ZM_AEE_FAILURE
*/

static int ZMAEE_IMedia_MmaPause(AEE_IMedia* po,int handle)
{
	mdi_result result = MDI_AUDIO_FAIL;	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);
	
	if(pThis == 0|| mma_cb == 0)
		return E_ZM_AEE_BADPARAM;
	
	if(mma_cb->mma_stat != ZMAEE_MEDIA_STAT_PLAYING)
		return E_ZM_AEE_FAILURE;
	
	result = mdi_audio_mma_pause(handle);
	
	if( result == MDI_AUDIO_SUCCESS )
	{
		mma_cb->mma_stat = ZMAEE_MEDIA_STAT_PAUSED;
		
		return E_ZM_AEE_SUCCESS;
	}
	else 
		return E_ZM_AEE_FAILURE;
	
}

/**
*@handle  MmaOpen return valid handle
* RETURN:
E_ZM_AEE_SUCCESS
E_ZM_AEE_FAILURE
*/	

static int ZMAEE_IMedia_MmaResume(AEE_IMedia* po,int handle)
{
	mdi_result result = MDI_AUDIO_FAIL;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
	if(pThis == 0||mma_cb == 0)
		return E_ZM_AEE_BADPARAM;
	
	if(mma_cb->mma_stat != ZMAEE_MEDIA_STAT_PAUSED)
		return E_ZM_AEE_FAILURE;
	
	result = mdi_audio_mma_resume(handle);
	
	if( result == MDI_AUDIO_SUCCESS )
	{
		mma_cb->mma_stat = ZMAEE_MEDIA_STAT_PLAYING;
		
		return E_ZM_AEE_SUCCESS;
	}
	else 
		return E_ZM_AEE_FAILURE;
}

/**
*@handle  MmaOpen return valid handle
* RETURN:
E_ZM_AEE_SUCCESS
E_ZM_AEE_FAILURE
*/

static int ZMAEE_IMedia_MmaStop(AEE_IMedia* po,int handle)
{	
	mdi_result result = MDI_AUDIO_FAIL;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);		
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
	if(pThis == 0||mma_cb == 0)
		return E_ZM_AEE_BADPARAM;
	
	if((mma_cb->mma_stat != ZMAEE_MEDIA_STAT_PLAYING) && (mma_cb->mma_stat != ZMAEE_MEDIA_STAT_PAUSED))
		return E_ZM_AEE_FAILURE;
	
	result = mdi_audio_mma_stop(handle);
	if( result == MDI_AUDIO_SUCCESS )
	{
		mma_cb->mma_stat = ZMAEE_MEDIA_STAT_STOP;
		
		return E_ZM_AEE_SUCCESS;
	}
	else 
		return E_ZM_AEE_FAILURE;
}

/**
*@handle  MmaOpen return valid handle
* RETURN:
E_ZM_AEE_SUCCESS
E_ZM_AEE_FAILURE
*/	

static int ZMAEE_IMedia_MmaClose(AEE_IMedia* po,int handle)
{
	mdi_result result = MDI_AUDIO_FAIL;
	ZMAEE_MMACB *mma_cb = ZMAEE_IMedia_GetCBByHdlr(handle);	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
	if(pThis == 0 || mma_cb == 0)
		return E_ZM_AEE_BADPARAM;

	// Make sure stop before close
	if(mma_cb->mma_stat != ZMAEE_MEDIA_STAT_STOP) {
		ZMAEE_IMedia_MmaStop(po, handle);
	}
	
	result = mdi_audio_mma_close(handle);
	if(result == MDI_AUDIO_SUCCESS)
	{
		zmaee_memset(&mma_cb->mma_cb, 0, sizeof(mma_cb->mma_cb));
		mma_cb->mma_hdlr = -1;
		mma_cb->mma_type = 0;
		mma_cb->mma_stat = ZMAEE_MEDIA_STAT_IDLE;

		if(ZMAEE_IMedia_GetValidCBCount() == 0) {
			pThis->mma_state = ZMAEE_MEDIA_STAT_IDLE;
			pThis->mma_type = 0;
		}
		
		return E_ZM_AEE_SUCCESS;
	}
	else 
		return E_ZM_AEE_FAILURE;
}

/**
 * 判断系统的音乐播放器是否在播放音乐
 * RETURN:
 * 	E_ZM_AEE_TRUE		正在播放
 *	E_ZM_AEE_FALSE		未在播放
 */

static
int ZMAEE_IMedia_Mp3IsPlaying(AEE_IMedia* po)
{
#ifdef __MMI_AUDIO_PLAYER__
{

	extern MMI_BOOL mmi_audply_is_playing(void);

	if(mmi_audply_is_playing() == MMI_TRUE)
		return E_ZM_AEE_TRUE;
	else
		return E_ZM_AEE_FALSE;
}

#else
{

	#ifdef __MMI_MEDIA_PLAYER__
	
		extern medply_struct g_medply;
	
		if(g_medply.state == MEDPLY_STATE_AUDIO_PLAY) {
			return E_ZM_AEE_TRUE;
		} else {
			return E_ZM_AEE_FALSE;
		}
	#else

		return E_ZM_AEE_FALSE;

	#endif
}

#endif
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
static
ZMAEE_MediaResultEx ZMAEE_IMedia_SysMediaResult2ZMMediaResultEx(mdi_result result)
{
	switch(result)
	{
		case MDI_AUDIO_END_OF_FILE:
			return ZMAEE_MEDIA_EX_END_OF_FILE;

		case MDI_AUDIO_RESUME:
			return ZMAEE_MEDIA_BG_RESUME;

		case MDI_AUDIO_SUCCESS:
			return ZMAEE_MEDIA_EX_SUCCESS;

		case MDI_AUDIO_DISC_FULL:
			return ZMAEE_MEDIA_EX_FULL;

		default:
			return ZMAEE_MEDIA_EX_FAIL;	
	}
}

static
void ZMAEE_SoundCallback(int type, mdi_result result)
{
	ZMAEE_MEDIA_PFNCB_EX callback_ex;
	void *pUser;
	int index;
		
	sg_zmaee_media.sma_state = ZMAEE_MEDIA_STAT_IDLE;

	index = sg_zmaee_media.sma_background.stack_index[sg_zmaee_media.sma_background.curr_play - 1] & (~0xF0);	

	ZMAEE_DebugPrint("ZMAEE_SoundCallback(): type = %d, result = %d, index = %d\n", type, result, index);

	if(index < 0 || index >= ZMAEE_SOUNDPLAY_SPACE_COUNT) {
		return;
	}

	if(ZMAEE_Callback_Valid(&(sg_zmaee_media.sma_background.callback[index])) != 1) {
		ZMAEE_IMedia_ClearBKG_And_PlayInfo((AEE_IMedia*)&sg_zmaee_media, sg_zmaee_media.sma_background.bg_hdlr[index]);
		return;
	}

	callback_ex = (ZMAEE_MEDIA_PFNCB_EX)(sg_zmaee_media.sma_background.callback[index].pfn);
	pUser = sg_zmaee_media.sma_background.callback[index].pUser;

	if((result == MDI_AUDIO_TERMINATED) && (sg_zmaee_media.sma_background.active_close == 1)) {
		return;
	}

	switch(result) 
	{
		case MDI_AUDIO_TERMINATED:
			{
				if(type == 0) {
					callback_ex(pUser, 0, ZMAEE_MEDIA_EX_TERMINATED);
				} else if(type == 1){
					callback_ex(pUser, 0, ZMAEE_MEDIA_BG_TERMINATED);
				}
			}
			break;
			
		case MDI_AUDIO_END_OF_FILE:	
		case MDI_AUDIO_SUCCESS:
		case MDI_AUDIO_RESUME:
		case MDI_AUDIO_DISC_FULL:
		default:
			{
				ZMAEE_MediaResultEx msg;
				msg = ZMAEE_IMedia_SysMediaResult2ZMMediaResultEx(result);
				
				ZMAEE_IMedia_ClearBKG_And_PlayInfo((AEE_IMedia*)&sg_zmaee_media, sg_zmaee_media.sma_background.bg_hdlr[index]);
				callback_ex(pUser, 0, msg);
			}
			break;
	}
}


static
BOOL ZMAEE_Background_Callback(mdi_result result)
{
	if((sg_zmaee_media.sma_background.stack_index[sg_zmaee_media.sma_background.curr_play - 1]) == 0xF0) {
		ZMAEE_Soundplay_Callback(result);
	} else {
		ZMAEE_SoundCallback(1, result);
	}

	return E_ZM_AEE_TRUE;
}


static 
int ZMAEE_IMedia_SetBKG_And_PlayInfo(AEE_IMedia* po, ZMAEE_MEDIA_PFNCB_EX pfn, void* pUser)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	int i = 0, pos;

	if(pfn == NULL) {
		pos = 0;
	} else {
		for(i = 1; i < ZMAEE_SOUNDPLAY_SPACE_COUNT; i++) {
			if(pThis->sma_background.bg_hdlr[i] == 0) {
				pos = i;
				break;
			}		
		}
	}

	if(i >= ZMAEE_SOUNDPLAY_SPACE_COUNT) {
		return E_ZM_AEE_FAILURE;
	}

	if(pThis->sma_background.curr_play == 0) {
		mdi_result ret;
#if(ZM_AEE_MTK_SOFTVERN >= 0x11A0)
		ret = mdi_audio_set_background_handler(MDI_BACKGROUND_APP_ZMAEE_AUDIO, (mdi_bg_callback)ZMAEE_Background_Callback, NULL);
#else	
		ret = mdi_audio_set_background_handler(MDI_BACKGROUND_APP_ZMAEE_AUDIO, (mdi_bg_callback)ZMAEE_Background_Callback);
#endif
		if(ret != MDI_AUDIO_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}
	}

	memset(&(pThis->sma_background.callback[pos]), 0, sizeof(ZMAEE_CALLBACK));
	ZMAEE_Callback_Init(&(pThis->sma_background.callback[pos]), (void*)pfn, (void*)pUser);

	pThis->sma_background.bg_hdlr[pos] = pos | 0xF0;
	pThis->sma_background.stack_index[pThis->sma_background.curr_play] = pThis->sma_background.bg_hdlr[pos];
	pThis->sma_background.curr_play++;

	return pThis->sma_background.bg_hdlr[pos];
}


static 
int ZMAEE_IMedia_ClearBKG_And_PlayInfo(AEE_IMedia* po, int handle)
{
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	mdi_result ret;
	int i;

	i = handle & (~0xF0);
	if(i < 0 || i >= ZMAEE_SOUNDPLAY_SPACE_COUNT)
		return E_ZM_AEE_BADPARAM;	

	if(pThis->sma_background.curr_play > 0) {
		//判断传入handle是否是当前栈顶的handle
		if(handle != pThis->sma_background.stack_index[pThis->sma_background.curr_play - 1]) {
			//栈块搬移
			int j;
			
			for(j = 0; j < pThis->sma_background.curr_play; j++) {
				if(handle == pThis->sma_background.stack_index[j]) {
					break;
				}
			}
			
			if(j >= pThis->sma_background.curr_play) {
				return E_ZM_AEE_SUCCESS;
			}
			
			for(j; j < pThis->sma_background.curr_play - 1; j++) {
				pThis->sma_background.stack_index[j] = pThis->sma_background.stack_index[j+1];
			}
		}
		
		pThis->sma_background.bg_hdlr[i] = 0;
		pThis->sma_background.stack_index[pThis->sma_background.curr_play - 1] = 0;
		pThis->sma_background.curr_play--;
	}

	if(pThis->sma_background.curr_play == 0) {
		ret = mdi_audio_clear_background_handler(MDI_BACKGROUND_APP_ZMAEE_AUDIO);
		if(ret != MDI_AUDIO_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}
	}
		
	return E_ZM_AEE_SUCCESS;
}



static 
void ZMAEE_Soundplay_CallbackEx(mdi_result result)
{
	if((sg_zmaee_media.sma_background.stack_index[sg_zmaee_media.sma_background.curr_play - 1]) == 0xF0) {
		ZMAEE_Soundplay_Callback(result);
	} else {
		ZMAEE_SoundCallback(0, result);
	}
}


/*
*@ pfn :  不能为空
* return a handle for soundplay handle, this handle is used to AEE_IMedia_SoundCloseEx
*/
static 
int ZMAEE_IMedia_SoundPlayEx(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB_EX pfn, void* pUser)
{
	U8 format = 0,playstyle = 0;
	mdi_result result;
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)	
#ifndef WIN32
	U16 scrId = GetActiveScreenId();
	if(((scrId >= CAMERA_BASE)&&(scrId <= CAMERA_BASE_MAX))||
		((scrId >= VDOPLY_BASE)&&(scrId <= VDOPLY_BASE_MAX))
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		|| ((scrId >= GFX_GAME_SCREEN) && (scrId <= GFX_GAMEOVER_SCREEN))
	#endif
		)
	{
		PlayVibratorOnce();
		return E_ZM_AEE_SUCCESS;
	}
#endif
#endif

	if(pThis == 0|| ptr == 0)
		return E_ZM_AEE_BADPARAM;

	// MMA,SMA互斥
	if(pThis->mma_state != ZMAEE_MEDIA_STAT_IDLE) {
		return E_ZM_AEE_ITEMBUSY;
	}

	if(pThis->sma_background.curr_play >= ZMAEE_SOUNDPLAY_SPACE_COUNT) {
		return E_ZM_AEE_FAILURE;
	}
	
	// 类型转换
	switch(type)
	{
		case ZMAEE_MEDIATYPE_MIDI:
			 format = MDI_FORMAT_SMF;
			 break;
		case ZMAEE_MEDIATYPE_AMR:
			 format = MDI_FORMAT_AMR;
			 break;
		case ZMAEE_MEDIATYPE_WAV:
			 format = MDI_FORMAT_WAV;
			 break;
		case ZMAEE_MEDIATYPE_MP3:
			 format = MDI_FORMAT_DAF;
			 break;
		case ZMAEE_MEDIATYPE_PCM:
			 format = MDI_FORMAT_PCM_8K;
			 break;
		case ZMAEE_MEDIATYPE_FILE:
			 break;
		default:
			 return E_ZM_AEE_BADPARAM;
	}

	playstyle = (repeat)? DEVICE_AUDIO_PLAY_INFINITE: DEVICE_AUDIO_PLAY_ONCE;

	ZMAEE_DebugPrint("ZMAEE_IMedia_SoundPlay: volume = %d", sg_zmaee_setting_volume);
	
	if(type != ZMAEE_MEDIATYPE_FILE)
	{
		#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
			result = mdi_audio_play_string_with_vol_path_non_block((void*)ptr, (U32)len,
					format, playstyle, (mdi_ext_callback)ZMAEE_Soundplay_CallbackEx, NULL, sg_zmaee_setting_volume, MDI_DEVICE_SPEAKER2);
		#else
			result = mdi_audio_play_string_with_vol_path_non_block((void*)ptr, (U32)len, 
					 format, playstyle, FALSE,(mdi_callback)ZMAEE_Soundplay_CallbackEx,sg_zmaee_setting_volume, MDI_DEVICE_SPEAKER2);
		#endif
		
		if(result == MDI_AUDIO_SUCCESS)
		{
    		pThis->sma_type = type;
		}
		else
			return E_ZM_AEE_FAILURE;
	}
	else
	{
		unsigned short ucs2_filepath[AEE_MAX_PATH_NAME]={0};
		ZMAEE_Utf8_2_Ucs2(ptr,strlen(ptr),ucs2_filepath,sizeof(ucs2_filepath)/sizeof(ucs2_filepath[0]));

		#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
			result = mdi_audio_play_file_with_vol_path(ucs2_filepath, playstyle, NULL,
					(mdi_ext_callback)ZMAEE_Soundplay_CallbackEx, NULL, sg_zmaee_setting_volume, MDI_DEVICE_SPEAKER2);
		#else
			result = mdi_audio_play_file_with_vol_path(ucs2_filepath, playstyle, NULL, 
					 (mdi_callback)ZMAEE_Soundplay_CallbackEx, sg_zmaee_setting_volume, MDI_DEVICE_SPEAKER2);
		#endif
		
		if(result == MDI_AUDIO_SUCCESS)
		{
    		pThis->sma_type = type;
		}
		else 
		{
			int ret = E_ZM_AEE_FAILURE;

			ret = AEE_IMedia_ResSoundPlay(po, type, ptr, len, repeat, (ZMAEE_MEDIA_PFNCB)pfn, pUser);
			return ret;
		}
	}

	if (result == MDI_AUDIO_SUCCESS) 
	{
		pThis->sma_handle = 0;
		pThis->sma_state  = ZMAEE_MEDIA_STAT_PLAYING;
		
		return ZMAEE_IMedia_SetBKG_And_PlayInfo(po, pfn, pUser);
	} 
	else  
	{
		return E_ZM_AEE_FAILURE;
	}

}


static int ZMAEE_IMedia_SoundCloseEx(AEE_IMedia* po, int handle)
{
	mdi_result result = MDI_AUDIO_SUCCESS;
	int equ_stack_top_handle_flag = 0;
	
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;
	if(pThis == 0) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->sma_state == ZMAEE_MEDIA_STAT_RECORD)
		return E_ZM_AEE_BADPARAM;

	ZMAEE_DebugPrint("ZMAEE_IMedia_SoundCloseEx(): handle = %d, curr_play = %d, stack_top_handle = %d\n", handle, pThis->sma_background.curr_play, pThis->sma_background.stack_index[pThis->sma_background.curr_play - 1]);

	if(handle == pThis->sma_background.stack_index[pThis->sma_background.curr_play - 1]) {
		equ_stack_top_handle_flag = 1;
	}

	/*关闭声音*/
	/*传入的handle是当前栈顶句柄*/
	if((equ_stack_top_handle_flag == 1) && ((pThis->sma_state == ZMAEE_MEDIA_STAT_PLAYING) || (pThis->sma_state == ZMAEE_MEDIA_STAT_PAUSED))) {
		mdi_result (*stop_func)(void);

		if(pThis->sma_type == ZMAEE_MEDIATYPE_FILE) {
			stop_func = mdi_audio_stop_file;
		} else {
			stop_func = mdi_audio_stop_string;
		}

		pThis->sma_background.active_close = 1;
		result = stop_func();
		pThis->sma_background.active_close = 0;
		if(result != MDI_AUDIO_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}
	}

	if(result == MDI_AUDIO_SUCCESS) {	
		if(equ_stack_top_handle_flag == 1) {
			pThis->sma_state = ZMAEE_MEDIA_STAT_IDLE;
			pThis->sma_type  = 0;
		}
		
		return ZMAEE_IMedia_ClearBKG_And_PlayInfo(po, handle);
	} else { 
		return E_ZM_AEE_FAILURE;
	}
}


int ZMAEE_AudioPlayer_IsPlaying(void)
{
	return ZMAEE_Background_IsRunning("mplayer.dll");
}

mmi_ret ZMAEE_AudioPlayer_Play_Event_Callback(mmi_event_struct *event)
{
	MMI_BOOL ret = MMI_FALSE;

	if(ZMAEE_AudioPlayer_IsPlaying())
		ret = MMI_TRUE;

	return ret;
}

#ifdef __ZMAEE_APP_TTF__
int ZMAEE_IMedia_TTF_SoundPlayEx(AEE_IMedia* po, ZMAEE_MediaType type, 
				const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB_EX pfn, void* pUser)
{
	U8 format = 0,playstyle = 0;
	mdi_result result;
	ZMAEE_IMEDIA* pThis = (ZMAEE_IMEDIA*)po;

	extern int zmaee_volume_get_volume_value(void);
	
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)	
#ifndef WIN32
	U16 scrId = GetActiveScreenId();
	if(((scrId >= CAMERA_BASE)&&(scrId <= CAMERA_BASE_MAX))||
		((scrId >= VDOPLY_BASE)&&(scrId <= VDOPLY_BASE_MAX))
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		|| ((scrId >= GFX_GAME_SCREEN) && (scrId <= GFX_GAMEOVER_SCREEN))
	#endif
		)
	{
		PlayVibratorOnce();
		return E_ZM_AEE_SUCCESS;
	}
#endif
#endif

	if(pThis == 0|| ptr == 0)
		return E_ZM_AEE_BADPARAM;

	// MMA,SMA互斥
	if(pThis->mma_state != ZMAEE_MEDIA_STAT_IDLE) {
		return E_ZM_AEE_ITEMBUSY;
	}

	if(pThis->sma_background.curr_play >= ZMAEE_SOUNDPLAY_SPACE_COUNT) {
		return E_ZM_AEE_FAILURE;
	}
	
	// 类型转换
	switch(type)
	{
		case ZMAEE_MEDIATYPE_MIDI:
			 format = MDI_FORMAT_SMF;
			 break;
		case ZMAEE_MEDIATYPE_AMR:
			 format = MDI_FORMAT_AMR;
			 break;
		case ZMAEE_MEDIATYPE_WAV:
			 format = MDI_FORMAT_WAV;
			 break;
		case ZMAEE_MEDIATYPE_MP3:
			 format = MDI_FORMAT_DAF;
			 break;
		case ZMAEE_MEDIATYPE_PCM:
			 format = MDI_FORMAT_PCM_8K;
			 break;
		case ZMAEE_MEDIATYPE_FILE:
			 break;
		default:
			 return E_ZM_AEE_BADPARAM;
	}

	playstyle = (repeat)? DEVICE_AUDIO_PLAY_INFINITE: DEVICE_AUDIO_PLAY_ONCE;
	
	if(type != ZMAEE_MEDIATYPE_FILE)
	{
		#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
			result = mdi_audio_play_string_with_vol_path_non_block((void*)ptr, (U32)len,
					format, playstyle, (mdi_ext_callback)ZMAEE_Soundplay_CallbackEx, NULL, zmaee_volume_get_volume_value(), MDI_DEVICE_SPEAKER2);
		#else
			result = mdi_audio_play_string_with_vol_path_non_block((void*)ptr, (U32)len, 
					 format, playstyle, FALSE,(mdi_callback)ZMAEE_Soundplay_CallbackEx, zmaee_volume_get_volume_value(), MDI_DEVICE_SPEAKER2);
		#endif
		
		if(result == MDI_AUDIO_SUCCESS)
		{
    		pThis->sma_type = type;
		}
		else
			return E_ZM_AEE_FAILURE;
	}
	else
		
	{
		unsigned short ucs2_filepath[AEE_MAX_PATH_NAME]={0};
		ZMAEE_Utf8_2_Ucs2(ptr,strlen(ptr),ucs2_filepath,sizeof(ucs2_filepath)/sizeof(ucs2_filepath[0]));

		#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
			result = mdi_audio_play_file_with_vol_path(ucs2_filepath, playstyle, NULL,
					(mdi_ext_callback)ZMAEE_Soundplay_CallbackEx, NULL, zmaee_volume_get_volume_value(), MDI_DEVICE_SPEAKER2);
		#else
			result = mdi_audio_play_file_with_vol_path(ucs2_filepath, playstyle, NULL, 
					 (mdi_callback)ZMAEE_Soundplay_CallbackEx, zmaee_volume_get_volume_value(), MDI_DEVICE_SPEAKER2);
		#endif
		
		if(result == MDI_AUDIO_SUCCESS)
		{
    		pThis->sma_type = type;
		}
		else 
		{
			int ret = E_ZM_AEE_FAILURE;

			ret = AEE_IMedia_ResSoundPlay(po, type, ptr, len, repeat, (ZMAEE_MEDIA_PFNCB)pfn, pUser);
			return ret;
		}
	}

	if (result == MDI_AUDIO_SUCCESS) 
	{
		pThis->sma_handle = 0;
		pThis->sma_state  = ZMAEE_MEDIA_STAT_PLAYING;
		
		return ZMAEE_IMedia_SetBKG_And_PlayInfo(po, pfn, pUser);
	} 
	else  
	{
		return E_ZM_AEE_FAILURE;
	}

}

int ZMAEE_IMedia_TTF_SoundPlay(AEE_IMedia* po, ZMAEE_MediaType type, const void* ptr, int len, int repeat, ZMAEE_MEDIA_PFNCB pfn, void* pUser)
{
	int ret;

	ret = ZMAEE_IMedia_TTF_SoundPlayEx(po, type, ptr, len, repeat, NULL, pUser);	
	if(ret > 0) {
		ZMAEE_Callback_Init(&sg_zmaee_media.sma_callback, (void*)pfn, pUser);
		return E_ZM_AEE_SUCCESS;
	} else {
		return E_ZM_AEE_FAILURE;
	}
}

#endif


#endif	// __ZMAEE_APP__

