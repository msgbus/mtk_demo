#ifdef __ZMAEE_APP__

#include "zmaee_menu_helper.h"

#include "MMIDataType.h"
#include "IdleAppResDef.h"
#include "mmi_frm_history_gprot.h"
#include "GlobalResDef.h"
#include "ProfilesSrvGprot.h"

#include "InlineCuiGprot.h"

#include "mmi_rp_app_zmaee_def.h"
#include "zmaee_typedef.h"

#include "mmi_rp_app_mainmenu_def.h"

/**************************************/
/*外部函数声明					    */
/**************************************/
#ifdef __MMI_FRM_HISTORY__
extern U8 *GetCurrGuiBuffer(U16 scrnid);
#else
extern mmi_ret zmaee_group_proc(mmi_event_struct* evt);
#endif
extern PU8 get_image(MMI_ID_TYPE i);

/**************************************/
/* 数据结构							    */
/**************************************/
#define MENUITEM_MAX_COUNT		32


#define CUI_ID_TIME_START_CAPTION		(CUI_INLINE_ITEM_ID_BASE + 1)
#define CUI_ID_TIME_START_DATA			(CUI_INLINE_ITEM_ID_BASE + 2)
#define CUI_ID_TIME_END_CAPTION			(CUI_INLINE_ITEM_ID_BASE + 3)
#define CUI_ID_TIME_END_DATA			(CUI_INLINE_ITEM_ID_BASE + 4)

#define CUI_ID_TIME_DATA_LENGTH			4

#define LISTMENU_MAX_HDLR_COUNT			3


/**
 * 单选菜单页面显示内容
 */
typedef struct {
	int			title_str_id;
	int			highlight_index;

	int			menuitem_str_ids[MENUITEM_MAX_COUNT];
	int			menuitem_count;

	ZMAEE_MENU_HIGHLIGHT_FUNC 	highlight_func;
	ZMAEE_MENU_KEY_ENTRY		left_softkey_pre_func;
	ZMAEE_MENU_KEY_ENTRY		right_softkey_pre_func;
}ZMAEE_MENU_CHECKLIST_CONTEXT;

/**
 * 开关菜单页面显示内容
 */
typedef struct {
	int							title_str_id;
	int							highlight_index;

	ZMAEE_MENU_HIGHLIGHT_FUNC	highlight_func;
	ZMAEE_MENU_KEY_ENTRY		left_softkey_pre_func;
	ZMAEE_MENU_KEY_ENTRY		right_softkey_pre_func;
}ZMAEE_MENU_SWITCH_CONTEXT;

/**
 * 自定义时间段页面显示内容
 */
typedef struct {
	int								title_str_id;

	ZMAEE_MENU_CUSTOM_TIME_NOTIFY	notify_func;
}ZMAEE_MENU_CUSTOMTIME_CONTEXT;

/**
 * 列表菜单页面显示内容
 */
typedef struct {
	ZMAEE_MENU_LISTMENU 	list_menu[LISTMENU_MAX_HDLR_COUNT];
	int 					is_valid[LISTMENU_MAX_HDLR_COUNT];
	int 					highlight_item[LISTMENU_MAX_HDLR_COUNT];
	int 					active_index;
	int						active_stack[LISTMENU_MAX_HDLR_COUNT];
}ZMAEE_MENU_LISTMENU_CONTEXT;

static ZMAEE_MENU_CHECKLIST_CONTEXT 	sg_zmaee_checklist_context = {0};
static ZMAEE_MENU_SWITCH_CONTEXT		sg_zmaee_switch_context = {0};
static ZMAEE_MENU_CUSTOMTIME_CONTEXT	sg_zmaee_custom_time_context = {0};
static ZMAEE_MENU_LISTMENU_CONTEXT 		sg_zmaee_menu_listmenu_cntx = {0};


static unsigned char *sg_sub_menuitems[MENUITEM_MAX_COUNT] = {0};

static const cui_inline_item_caption_struct sg_zmaee_broadcast_custom_start = {STR_ZMAEE_CUSTOM_TIME_START};
static const cui_inline_item_caption_struct sg_zmaee_broadcast_custom_end = {STR_ZMAEE_CUSTOM_TIME_END};

static const cui_inline_item_text_edit_struct sg_zmaee_broadcast_custom_start_text = {
	0,
	0,
	CUI_ID_TIME_DATA_LENGTH,
	IMM_INPUT_TYPE_DECIMAL_NUMERIC,
	0,
	NULL
};

static const cui_inline_item_text_edit_struct sg_zmaee_broadcast_custom_end_text = {
	0,
	0,
	CUI_ID_TIME_DATA_LENGTH,
	IMM_INPUT_TYPE_DECIMAL_NUMERIC,
	0,
	NULL
};

static cui_inline_set_item_struct sg_zmaee_broadcast_custom_items[] = {
	{CUI_ID_TIME_START_CAPTION, CUI_INLINE_ITEM_TYPE_CAPTION, IMG_TIME, &sg_zmaee_broadcast_custom_start},
	{CUI_ID_TIME_START_DATA, CUI_INLINE_ITEM_TYPE_TEXT_EDIT, 0, &sg_zmaee_broadcast_custom_start_text},
	{CUI_ID_TIME_END_CAPTION, CUI_INLINE_ITEM_TYPE_CAPTION, IMG_TIME, &sg_zmaee_broadcast_custom_end},
	{CUI_ID_TIME_END_DATA, CUI_INLINE_ITEM_TYPE_TEXT_EDIT, 0, &sg_zmaee_broadcast_custom_start_text}
};

static cui_inline_struct sg_zmaee_custom_inline = {
	sizeof(sg_zmaee_broadcast_custom_items) / sizeof(cui_inline_set_item_struct),
	NULL,
	NULL,
	CUI_INLINE_SCREEN_DEFAULT_TEXT | CUI_INLINE_SCREEN_DISABLE_DONE,
	NULL,
	sg_zmaee_broadcast_custom_items
};

static U16 sg_zmaee_custom_start_buffer[CUI_ID_TIME_DATA_LENGTH] = {0};
static U16 sg_zmaee_custom_end_buffer[CUI_ID_TIME_DATA_LENGTH] = {0};


/**************************************/
/*通用接口							    */
/**************************************/

/**
 * 返回上一层菜单
 */
void ZMAEE_Menu_GoBackHistory()
{
	GoBackHistory();
}

/**
 * 显示"完成"的Pop框
 */
void ZMAEE_Menu_DisplayPop_Done()
{
	DisplayPopup((PU8)GetString(STR_GLOBAL_DONE), IMG_GLOBAL_ACTIVATED, 1, 1000, SUCCESS_TONE);
}


/**************************************/
/* 单选菜单							    */
/**************************************/

static
void ZMAEE_Menu_CheckList_Highlight(int index)
{
	if(sg_zmaee_checklist_context.highlight_func)
		sg_zmaee_checklist_context.highlight_func(index);
}

static
void ZMAEE_Menu_CheckList_LeftSoftkey_Func()
{
	if(sg_zmaee_checklist_context.left_softkey_pre_func)
		sg_zmaee_checklist_context.left_softkey_pre_func();
}

static
void ZMAEE_Menu_CheckList_RightSoftkey_Func()
{
	if(sg_zmaee_checklist_context.right_softkey_pre_func)
		sg_zmaee_checklist_context.right_softkey_pre_func();
}

/**
 * 初始化单选菜单页面的显示数据
 * title_str_id				标题文字资源ID
 * menuitem_str_ids		菜单项的内容文字资源ID
 * menuitem_count		菜单项的个数
 * highlight_func			高亮函数
 * left_softkey_pre_func	左软键预处理函数，调用完之后，还会调用GoBackHistory和DisplayPopup提示完成
 * right_softkey_pre_func	右软键预处理函数，调用完之后，还会调用GoBackHistory
 */
void ZMAEE_Menu_InitCheckListMenu(int title_str_id,	
										int menuitem_str_ids[], int menuitem_count,
										int highlight_index,
										ZMAEE_MENU_HIGHLIGHT_FUNC highlight_func,
										ZMAEE_MENU_KEY_ENTRY left_softkey_pre_func, 
										ZMAEE_MENU_KEY_ENTRY right_softkey_pre_func)
{
	int i;
										
	zmaee_memset(&sg_zmaee_checklist_context, 0, sizeof(sg_zmaee_checklist_context));
	
	sg_zmaee_checklist_context.title_str_id = title_str_id;
	sg_zmaee_checklist_context.highlight_index = highlight_index;
	
	sg_zmaee_checklist_context.highlight_func = highlight_func;
	sg_zmaee_checklist_context.left_softkey_pre_func = left_softkey_pre_func;
	sg_zmaee_checklist_context.right_softkey_pre_func = right_softkey_pre_func;

	for(i = 0; i < menuitem_count; i++) {
		sg_zmaee_checklist_context.menuitem_str_ids[i] = menuitem_str_ids[i];
	}
	sg_zmaee_checklist_context.menuitem_count = menuitem_count;
}

/**
 * 修改高亮函数
 */
void ZMAEE_Menu_SetCheckListHighlight(int highlight_index)
{
	sg_zmaee_checklist_context.highlight_index = highlight_index;
}

/**
 * 显示单选菜单页面
 */
void ZMAEE_Menu_ShowCheckList() {
	unsigned char *pGuiBuffer;
	int i;

#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_MENU_CHECKLIST, NULL, ZMAEE_Menu_ShowCheckList, NULL);
	pGuiBuffer = GetCurrGuiBuffer(SCR_ZMAEE_MENU_CHECKLIST);
#else
	mmi_frm_scrn_enter(GRP_ID_ROOT, SCR_ZMAEE_MENU_CHECKLIST, NULL, ZMAEE_Menu_ShowCheckList, MMI_FRM_FULL_SCRN);
	pGuiBuffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_MENU_CHECKLIST);
#endif

	for(i = 0; i < sg_zmaee_checklist_context.menuitem_count && i < sizeof(sg_sub_menuitems); i++) {
		sg_sub_menuitems[i] = (unsigned char*)GetString(sg_zmaee_checklist_context.menuitem_str_ids[i]);
	}

	RegisterHighlightHandler(ZMAEE_Menu_CheckList_Highlight);

	ShowCategory36Screen(sg_zmaee_checklist_context.title_str_id, NULL,
						STR_GLOBAL_OK, IMG_GLOBAL_OK, STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
						sg_zmaee_checklist_context.menuitem_count, sg_sub_menuitems,
						sg_zmaee_checklist_context.highlight_index, pGuiBuffer);

	SetKeyHandler(ZMAEE_Menu_CheckList_LeftSoftkey_Func, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_CheckList_LeftSoftkey_Func, KEY_ENTER, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_Menu_CheckList_LeftSoftkey_Func, KEY_EVENT_UP);
	
	SetKeyHandler(ZMAEE_Menu_CheckList_RightSoftkey_Func, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_Menu_CheckList_RightSoftkey_Func, KEY_EVENT_UP);
}

/**************************************/
/* 开关菜单							    */
/**************************************/

static
void ZMAEE_Menu_Switch_Highlight(int index)
{
	if(sg_zmaee_switch_context.highlight_func)
		sg_zmaee_switch_context.highlight_func(index);
}

static
void ZMAEE_Menu_Switch_LeftSoftkey_Func()
{
	if(sg_zmaee_switch_context.left_softkey_pre_func)
		sg_zmaee_switch_context.left_softkey_pre_func();

	GoBackHistory();
	DisplayPopup((PU8)GetString(STR_GLOBAL_DONE), IMG_GLOBAL_ACTIVATED, 1, 1000, SUCCESS_TONE);
}

static
void ZMAEE_Menu_Switch_RightSoftkey_Func()
{
	if(sg_zmaee_switch_context.right_softkey_pre_func)
		sg_zmaee_switch_context.right_softkey_pre_func();

	GoBackHistory();
}


/**
 * 初始化开关页面的显示数据，索引0为OFF，索引1为ON
 * title_str_id				标题文字资源ID
 * highlight_func			高亮函数
 * left_softkey_pre_func	左软键预处理函数，调用完之后，还会调用GoBackHistory和DisplayPopup提示完成
 * right_softkey_pre_func	右软键预处理函数，调用完之后，还会调用GoBackHistory
 */
void ZMAEE_Menu_InitSwitchMenu(int title_str_id, int highlight_index,
										ZMAEE_MENU_HIGHLIGHT_FUNC highlight_func,
										ZMAEE_MENU_KEY_ENTRY left_softkey_pre_func, 
										ZMAEE_MENU_KEY_ENTRY right_softkey_pre_func) {
	zmaee_memset(&sg_zmaee_switch_context, 0, sizeof(sg_zmaee_switch_context));
	
	sg_zmaee_switch_context.title_str_id = title_str_id;
	sg_zmaee_switch_context.highlight_index = highlight_index;

	sg_zmaee_switch_context.highlight_func = highlight_func;
	sg_zmaee_switch_context.left_softkey_pre_func = left_softkey_pre_func;
	sg_zmaee_switch_context.right_softkey_pre_func = right_softkey_pre_func;
}

/**
 * 显示开关页面
 */
void ZMAEE_Menu_ShowSwitch() {
	unsigned char *pGuiBuffer;

#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_MENU_SWITCH, NULL, ZMAEE_Menu_ShowSwitch, NULL);
	pGuiBuffer = GetCurrGuiBuffer(SCR_ZMAEE_MENU_SWITCH);
#else
	mmi_frm_scrn_enter(GRP_ID_ROOT, SCR_ZMAEE_MENU_SWITCH, NULL, ZMAEE_Menu_ShowSwitch, MMI_FRM_FULL_SCRN);
	pGuiBuffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_MENU_SWITCH);	
#endif

	sg_sub_menuitems[0] = (unsigned char*)GetString(STR_GLOBAL_OFF);
	sg_sub_menuitems[1] = (unsigned char*)GetString(STR_GLOBAL_ON);

	RegisterHighlightHandler(ZMAEE_Menu_Switch_Highlight);

	ShowCategory36Screen(sg_zmaee_switch_context.title_str_id, NULL,
						STR_GLOBAL_OK, IMG_GLOBAL_OK, STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
						2, sg_sub_menuitems,
						sg_zmaee_switch_context.highlight_index, pGuiBuffer);

	SetKeyHandler(ZMAEE_Menu_Switch_LeftSoftkey_Func, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_Switch_LeftSoftkey_Func, KEY_ENTER, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_Menu_Switch_LeftSoftkey_Func, KEY_EVENT_UP);
	
	SetKeyHandler(ZMAEE_Menu_Switch_RightSoftkey_Func, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_Menu_Switch_RightSoftkey_Func, KEY_EVENT_UP);
}

/**************************************/
/* 自定义时间段					    */
/**************************************/
static int sg_custom_time_start_hour;
static int sg_custom_time_end_hour;

static
int ZMAEE_Menu_CustomTime_TimeCheck(PU8 start_time, PU8 end_time) {
	int start_hour, end_hour;
	U8 utf8_start_time[CUI_ID_TIME_DATA_LENGTH] = {0};
	U8 utf8_end_time[CUI_ID_TIME_DATA_LENGTH] = {0};

	ZMAEE_Ucs2_2_Utf8((const unsigned short*)start_time, zmaee_wcslen((const unsigned short*)start_time),
					(char*)utf8_start_time, sizeof(utf8_start_time));
	ZMAEE_Ucs2_2_Utf8((const unsigned short*)end_time, zmaee_wcslen((const unsigned short*)end_time),
					(char*)utf8_end_time, sizeof(utf8_end_time));
	
	if(strlen(utf8_start_time) <= 0 || strlen(utf8_end_time) <= 0)
		return 0;
	
	sg_custom_time_start_hour = atoi(utf8_start_time);
	sg_custom_time_end_hour = atoi(utf8_end_time);

	if(sg_custom_time_start_hour < 0 || sg_custom_time_start_hour > 23)
		return 0;

	if(sg_custom_time_end_hour < 0 || sg_custom_time_end_hour > 23)
		return 0;

	if(sg_custom_time_start_hour >= sg_custom_time_end_hour)
		return 0;

	return 1;
}


static
mmi_ret ZMAEE_Menu_CustomTime_Group_Proc(mmi_event_struct* evt)
{
	cui_event_inline_notify_struct *notify_event = (cui_event_inline_notify_struct*)evt;

	switch(evt->evt_id) {
		case EVT_ID_CUI_INLINE_SUBMIT:
	    case EVT_ID_CUI_INLINE_CSK_PRESS:        
			{
				cui_inline_get_value(notify_event->sender_id, CUI_ID_TIME_START_DATA, (void*)sg_zmaee_custom_start_buffer);
				cui_inline_get_value(notify_event->sender_id, CUI_ID_TIME_END_DATA, (void*)sg_zmaee_custom_end_buffer);

				if(ZMAEE_Menu_CustomTime_TimeCheck((PU8)sg_zmaee_custom_start_buffer, (PU8)sg_zmaee_custom_end_buffer) == 1) {
					if(sg_zmaee_custom_time_context.notify_func)
						sg_zmaee_custom_time_context.notify_func(sg_custom_time_start_hour, sg_custom_time_end_hour);
					
					cui_inline_close(mmi_frm_group_get_active_id());	
				} else {
					mmi_display_popup(GetString(STR_GLOBAL_ERROR), MMI_EVENT_FAILURE);
				}
	    	}
	        break;
	        
	    case EVT_ID_CUI_INLINE_ABORT:
	        cui_inline_close(mmi_frm_group_get_active_id());
	        break;
	}


	return MMI_RET_OK;
}


/**
 * 初始化自定义时间段页面
 * title_str_id				标题
 * init_start_hour			初始化的开始时间
 * init_end_hour			初始化的结束时间
 * notify_func				页面操作成功后，通知回调
 */
void ZMAEE_Menu_InitCustomTime(int title_str_id, 
										int init_start_hour, int init_end_hour,
										ZMAEE_MENU_CUSTOM_TIME_NOTIFY notify_func)
{
	unsigned char temp_buffer[CUI_ID_TIME_DATA_LENGTH] = {0};

	zmaee_memset(&sg_zmaee_custom_time_context, 0, sizeof(sg_zmaee_custom_time_context));

	sg_zmaee_custom_time_context.title_str_id = title_str_id;
	sg_zmaee_custom_time_context.notify_func = notify_func;

	zmaee_memset(&sg_zmaee_custom_start_buffer, 0, sizeof(sg_zmaee_custom_start_buffer));
	zmaee_memset(&sg_zmaee_custom_end_buffer, 0, sizeof(sg_zmaee_custom_end_buffer));

	sprintf(temp_buffer, "%d", init_start_hour);
	ZMAEE_Utf8_2_Ucs2((const char*)temp_buffer, strlen(temp_buffer), 
						(unsigned short*)sg_zmaee_custom_start_buffer, CUI_ID_TIME_DATA_LENGTH);
	
	zmaee_memset(temp_buffer, 0, sizeof(temp_buffer));
	sprintf(temp_buffer, "%d", init_end_hour);
	ZMAEE_Utf8_2_Ucs2((const char*)temp_buffer, strlen(temp_buffer), 
						(unsigned short*)sg_zmaee_custom_end_buffer, CUI_ID_TIME_DATA_LENGTH);	
}

/**
 * 显示自定义时间段页面
 */
void ZMAEE_Menu_CustomTime_Show()
{
	mmi_id cid;

	mmi_frm_group_create(GRP_ID_ROOT, SCR_ZMAEE_MENU_INLINE, ZMAEE_Menu_CustomTime_Group_Proc, NULL);
	mmi_frm_group_enter(SCR_ZMAEE_MENU_INLINE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	cid = cui_inline_create(SCR_ZMAEE_MENU_INLINE, &sg_zmaee_custom_inline);
	cui_inline_set_value(cid, CUI_ID_TIME_START_DATA, (void*)sg_zmaee_custom_start_buffer);
	cui_inline_set_value(cid, CUI_ID_TIME_END_DATA, (void*)sg_zmaee_custom_end_buffer);
	cui_inline_set_all_items_softkey_text(cid, MMI_LEFT_SOFTKEY, STR_GLOBAL_SAVE);
	cui_inline_run(cid);	
}

/**************************************/
/* 列表菜单							    */
/**************************************/

void ZMAEE_Menu_Show_ListMenu(int handle, int bHistory);


static
int ZMAEE_Menu_GetListMenuItem(int item_index, UI_string_type str_buff, PU8* img_buff_p, U8 str_img_mask)
{
	extern const U16 gIndexIconsImageList[];

	if(item_index < 0 || item_index >= ZMAEE_MENU_LISTMENU_MAX_COUNT) {
		return FALSE;
	}

	mmi_ucs2cpy((S8*)str_buff, (const S8*)sg_zmaee_menu_listmenu_cntx.list_menu[sg_zmaee_menu_listmenu_cntx.active_index].menu_item_name[item_index]);
	*img_buff_p = get_image(gIndexIconsImageList[item_index]);

	return TRUE;
}


static
void ZMAEE_Menu_ListMenu_LeftSoftkeyFunction(void)
{
	int hilighted_index = sg_zmaee_menu_listmenu_cntx.highlight_item[sg_zmaee_menu_listmenu_cntx.active_index];
	int active_index = sg_zmaee_menu_listmenu_cntx.active_index;


	if(hilighted_index < 0 || hilighted_index >= ZMAEE_MENU_LISTMENU_MAX_COUNT) {
		return;
	}

	if(sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_item_func[hilighted_index]) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_item_func[hilighted_index];
		pfn(sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_item_data[hilighted_index]);
	}
}

static
void ZMAEE_Menu_ListMenu_RightSoftkeyFunction(void)
{
	int hilighted_index = sg_zmaee_menu_listmenu_cntx.highlight_item[sg_zmaee_menu_listmenu_cntx.active_index];
	int active_index = sg_zmaee_menu_listmenu_cntx.active_index;

	if(sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_exit_func) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_exit_func;
		pfn((void*)sg_zmaee_menu_listmenu_cntx.list_menu[active_index].menu_item_data[hilighted_index]);
	}

	memset(&sg_zmaee_menu_listmenu_cntx.list_menu[active_index], 0, sizeof(ZMAEE_MENU_LISTMENU));
	sg_zmaee_menu_listmenu_cntx.is_valid[active_index] = 0;
	sg_zmaee_menu_listmenu_cntx.highlight_item[active_index] = 0;
}


static 
void ZMAEE_Menu_ListMenu_EntryScreenFunction(void)
{
	if(sg_zmaee_menu_listmenu_cntx.is_valid[sg_zmaee_menu_listmenu_cntx.active_index] == 1) {
		ZMAEE_Menu_Show_ListMenu(sg_zmaee_menu_listmenu_cntx.active_index | 0xF0, 1);
	}
}

static 
void ZMAEE_Menu_ListMenu_ExitScreenFunction(void)
{
}



static
int ZMAEE_Menu_GetIndexFromListMenuHandle(int handle)
{
	int idx = handle & ~(0xF0);

	if(idx < 0 || idx >= LISTMENU_MAX_HDLR_COUNT || sg_zmaee_menu_listmenu_cntx.is_valid[idx] != 1)
		return E_ZM_AEE_NOTEXIST;
	else
		return idx;
}


/**
   * 创建一个系统列表菜单
   * @pListMenu 					输入参数，列表菜单的内容
   * RETURN:
   *	E_ZM_AEE_BADPARAM		参数有误
   *	E_ZM_AEE_NOMEMORY		超出最多可创建的列表菜单个数
   *	> 0 								创建的列表菜单的句柄
   */
int ZMAEE_Menu_Create_ListMenu(ZMAEE_MENU_LISTMENU *pListMenu)
{
	int i, j;
	
	if(!pListMenu || !pListMenu->menu_exit_func || 
		pListMenu->menu_count <= 0 || pListMenu->menu_count > ZMAEE_MENU_LISTMENU_MAX_COUNT)
		return E_ZM_AEE_BADPARAM;

	for(i = 0; i < LISTMENU_MAX_HDLR_COUNT; i++) 
	{
		if(sg_zmaee_menu_listmenu_cntx.is_valid[i] == 0)
			break;
	}

	if(i >= LISTMENU_MAX_HDLR_COUNT) {
		return E_ZM_AEE_NOMEMORY;
	}

	sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_title_str_id = pListMenu->menu_title_str_id;
	sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_count = pListMenu->menu_count;
	for(j = 0; j < pListMenu->menu_count; j++) {
		memcpy(sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_item_name[j], pListMenu->menu_item_name[j], sizeof(pListMenu->menu_item_name[j]));
	}
	
	for(j = 0; j < pListMenu->menu_count; j++) {
		sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_item_func[j] = pListMenu->menu_item_func[j];
		sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_item_data[j] = pListMenu->menu_item_data[j];
	}

	sg_zmaee_menu_listmenu_cntx.list_menu[i].menu_exit_func = pListMenu->menu_exit_func;
	
	sg_zmaee_menu_listmenu_cntx.is_valid[i] = 1;

	return (i | 0xF0);
}

static
void ZMAEE_Menu_ListMenu_Highlight(S32 item_index)
{
	int active_index = sg_zmaee_menu_listmenu_cntx.active_index;

	sg_zmaee_menu_listmenu_cntx.highlight_item[active_index] = item_index;

	
	SetLeftSoftkeyFunction(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_Menu_ListMenu_RightSoftkeyFunction, KEY_EVENT_UP);

	SetKeyHandler(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_ENTER, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_ListMenu_RightSoftkeyFunction, KEY_LEFT_ARROW, KEY_EVENT_UP);

	
#ifdef __ZMAEE_APP_TTS__
	{
		extern void zmaee_tts_menu_broadcast(unsigned short* menu_item_text, unsigned int text_length);
		extern ZMAEE_MAIN_MENU_ITEM* zmaee_main_menu_get_items_ui_contonts(void);

		MMI_ID grp_id;
		MMI_ID scrn_id;

		mmi_frm_get_active_scrn_id(&grp_id, &scrn_id);

		if(scrn_id == SCR_ZMAEE_MENU_LISTMENU1) {
			ZMAEE_MENU_LISTMENU *p_listmenu = &sg_zmaee_menu_listmenu_cntx.list_menu[sg_zmaee_menu_listmenu_cntx.active_index];
			unsigned short *menu_text = p_listmenu->menu_item_name[item_index];
			zmaee_tts_menu_broadcast(menu_text, zmaee_wcslen(menu_text));
		}
	}
#endif
}

void ZMAEE_Menu_ListMenu_AddStack(int active_index)
{
	int empty_index = -1;
	int i;

	for(i = 0; i < LISTMENU_MAX_HDLR_COUNT; i++) {
		if(sg_zmaee_menu_listmenu_cntx.active_stack[i] == active_index)
			return;
	}
	
	for(i = 0; i < LISTMENU_MAX_HDLR_COUNT; i++) {
		if(sg_zmaee_menu_listmenu_cntx.active_stack[i] == -1) {
			empty_index = i;
			break;
		}
	}

	// 堆栈已满
	if(i >= LISTMENU_MAX_HDLR_COUNT)
		return;

	for(i = empty_index; i > 0; i--) {
		sg_zmaee_menu_listmenu_cntx.active_stack[i] = sg_zmaee_menu_listmenu_cntx.active_stack[i - 1];
	}
	sg_zmaee_menu_listmenu_cntx.active_stack[0] = active_index;	
}

/**
 * RETURN: 返回出栈后栈顶的值
 */
int ZMAEE_Menu_ListMenu_DelStack()
{
	int i;

	for(i = 0; i < LISTMENU_MAX_HDLR_COUNT - 1; i++) {
		sg_zmaee_menu_listmenu_cntx.active_stack[i] = sg_zmaee_menu_listmenu_cntx.active_stack[i + 1];
	}
	sg_zmaee_menu_listmenu_cntx.active_stack[LISTMENU_MAX_HDLR_COUNT - 1] = -1;

	return sg_zmaee_menu_listmenu_cntx.active_stack[0];
}

/**
 * 显示已创建的系统列表菜单
 * @handle							列表菜单的句柄
 * @bHistory						0 - 高亮菜单项从0开始，1 - 高亮菜单项为历时纪录中的高亮项
 */
void ZMAEE_Menu_Show_ListMenu(int handle, int bHistory)
{
	unsigned char *history_buffer;
	int idx;

	idx = ZMAEE_Menu_GetIndexFromListMenuHandle(handle);
	
#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_MENU_LISTMENU1 + idx, ZMAEE_Menu_ListMenu_ExitScreenFunction, 
				ZMAEE_Menu_ListMenu_EntryScreenFunction, NULL);
	history_buffer = GetCurrGuiBuffer(SCR_ZMAEE_MENU_LISTMENU1  + sg_zmaee_menu_listmenu_cntx.active_index);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_MENU_LISTMENU1 + idx, ZMAEE_Menu_ListMenu_ExitScreenFunction,
						ZMAEE_Menu_ListMenu_EntryScreenFunction, MMI_FRM_FULL_SCRN);
	history_buffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_MENU_LISTMENU1 + idx);
#endif
	sg_zmaee_menu_listmenu_cntx.active_index = idx;
	// 入栈
	ZMAEE_Menu_ListMenu_AddStack(idx);

	RegisterHighlightHandler(ZMAEE_Menu_ListMenu_Highlight);

	if(sg_zmaee_menu_listmenu_cntx.highlight_item[idx] >= sg_zmaee_menu_listmenu_cntx.list_menu[idx].menu_count) {
		sg_zmaee_menu_listmenu_cntx.highlight_item[idx] = 0;
	}

	ShowCategory184Screen(
				sg_zmaee_menu_listmenu_cntx.list_menu[sg_zmaee_menu_listmenu_cntx.active_index].menu_title_str_id, 
				NULL,
				STR_GLOBAL_OK, 
				IMG_GLOBAL_OK,
				STR_GLOBAL_BACK, 
				IMG_GLOBAL_BACK,
				sg_zmaee_menu_listmenu_cntx.list_menu[sg_zmaee_menu_listmenu_cntx.active_index].menu_count,
				(GetItemFuncPtr)ZMAEE_Menu_GetListMenuItem,
				NULL,
				bHistory? sg_zmaee_menu_listmenu_cntx.highlight_item[idx]: 0,
				bHistory? history_buffer: NULL);

	SetLeftSoftkeyFunction(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_Menu_ListMenu_RightSoftkeyFunction, KEY_EVENT_UP);

	SetKeyHandler(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_ENTER, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_ListMenu_LeftSoftkeyFunction, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_Menu_ListMenu_RightSoftkeyFunction, KEY_LEFT_ARROW, KEY_EVENT_UP);
}

/**
 * 销毁已创建的系统列表菜单，函数可被重入
 * @handle							列表菜单的句柄
 */
void ZMAEE_Menu_Destroy_ListMenu(int handle)
{
	U16 scr_id = GetActiveScreenId();
	int idx = ZMAEE_Menu_GetIndexFromListMenuHandle(handle);

	if(idx >= 0) {
		sg_zmaee_menu_listmenu_cntx.active_index = ZMAEE_Menu_ListMenu_DelStack();
		
		ExitCategory184Screen();
		
	#ifdef __MMI_FRM_HISTORY__
		if(scr_id == SCR_ZMAEE_MENU_LISTMENU1 + idx) {
			GoBackHistory();
		} else {
			DeleteScreenIfPresent(SCR_ZMAEE_MENU_LISTMENU1 + idx);
		}
	#else
		mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_MENU_LISTMENU1 + idx);
	#endif

		memset(&sg_zmaee_menu_listmenu_cntx.list_menu[idx], 0, sizeof(ZMAEE_MENU_LISTMENU));
		sg_zmaee_menu_listmenu_cntx.is_valid[idx] = 0;
		sg_zmaee_menu_listmenu_cntx.highlight_item[idx] = 0;
	}
}

void ZMAEE_Menu_Idle_Nofity(void)
{
	int i;

	for(i = 0; i < LISTMENU_MAX_HDLR_COUNT; i++) {
		memset(&sg_zmaee_menu_listmenu_cntx.list_menu[i], 0, sizeof(ZMAEE_MENU_LISTMENU));
		sg_zmaee_menu_listmenu_cntx.is_valid[i] = 0;
		sg_zmaee_menu_listmenu_cntx.highlight_item[i] = 0;

		sg_zmaee_menu_listmenu_cntx.active_stack[i] = -1;
	}
}

/**************************************/
/* Popup 菜单							    */
/**************************************/

/**
 * 显示弹出框
 * @content				UCS2编码格式的内容
 * @type					0 - 成功，1 - 错误，2 - info
 * @duration				显示时间，单位:ms
 */
void zmaee_menu_show_popup(unsigned char *content, int type, unsigned int duration)
{
	int img_id;

	switch(type) {
		case 0:
			img_id = IMG_GLOBAL_SUCCESS;
			break;

		case 1:
			img_id = IMG_GLOBAL_ERROR;
			break;

		case 2:
			img_id = IMG_GLOBAL_INFO;
			break;
	}

	DisplayPopup(content, img_id, 0, duration, 0);
}

/**
 * 获取掌盟主菜单的父菜单ID
 */
unsigned int zmaee_menu_get_parent_menuid(void)
{
	return (unsigned int)MAIN_MENU_MULTIMEDIA_MENUID;
}

extern FuncPtr get_softkey_function(MMI_key_event_type k, WGUI_SOFTKEY_ENUM key);
ZMAEE_PFNFUNC zmaee_main_menu_get_menu_entry_func(int menu_id)
{
	ZMAEE_PFNFUNC hilite_func;
	
	ZMAEE_PFNFUNC left_soft_key_down_func, left_soft_key_up_func;
	ZMAEE_PFNFUNC right_soft_key_down_func, right_soft_key_up_func;

	ZMAEE_PFNFUNC entry_func;

	hilite_func = mmi_frm_get_hilite_hdlr(menu_id);
	
	left_soft_key_down_func = get_softkey_function(KEY_EVENT_DOWN, MMI_LEFT_SOFTKEY);
	left_soft_key_up_func = get_softkey_function(KEY_EVENT_UP, MMI_LEFT_SOFTKEY);

	right_soft_key_down_func = get_softkey_function(KEY_EVENT_DOWN, MMI_RIGHT_SOFTKEY);
	right_soft_key_up_func = get_softkey_function(KEY_EVENT_UP, MMI_RIGHT_SOFTKEY);

	hilite_func();

	entry_func = get_softkey_function(KEY_EVENT_UP, MMI_LEFT_SOFTKEY);

	SetLeftSoftkeyFunction(left_soft_key_down_func, KEY_EVENT_DOWN);
	SetLeftSoftkeyFunction(left_soft_key_up_func, KEY_EVENT_UP);

	SetRightSoftkeyFunction(right_soft_key_down_func, KEY_EVENT_DOWN);
	SetRightSoftkeyFunction(right_soft_key_up_func, KEY_EVENT_UP);

	return entry_func;
}


#endif	// __ZMAEE_APP__
