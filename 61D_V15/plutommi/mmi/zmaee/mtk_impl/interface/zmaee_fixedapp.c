#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"
#include "zmaee_download.h"

#ifdef WIN32
extern void * memcpy(void * dest, const void * src, unsigned int count);
extern int AEE_GetResFixedApp(ZM_AEECLSID clsId, char **pRom, unsigned int *pSize);
#endif
typedef struct
{
	ZM_AEECLSID clsId;
	int			 bZip;
	const char* 	pROM;
	int			nSize;
}ZMAEE_FIXEDAPP;

typedef struct
{
	ZM_AEECLSID clsId;
	const char	*pRes;
	int			nSize;
}ZMAEE_FIXEDRES;

static const ZMAEE_FIXEDAPP g_zmaee_fixedapp[] =
{
	//
	// add fixed app here
	//
#ifndef __ZMAEE_REDUCE_DOWNLOAD__
	{ZMAEE_DOWNLOAD_CLSID, 1, (const char*)sg_zmaee_download_rom, sizeof(sg_zmaee_download_rom)}
#else
	{0}
#endif
};

static const ZMAEE_FIXEDRES g_zmaee_fixedres[] = 
{
	//
	// add fixed res here before 0xFFFFFFFF
	//
	{0xFFFFFFFF, NULL, 0}
};

static const ZM_AEECLSID sg_zmaee_halls[] = 
{
	1, 2, 3, 4, 9
};

static int sg_zmaee_fixedapp_enum_index = 0;

int ZMAEE_GetFixedApplet(ZM_AEECLSID clsId, const char** pROM, int *nSize, int* bZip)
{
	int i;
	for(i = 0; i < sizeof(g_zmaee_fixedapp)/sizeof(g_zmaee_fixedapp[0]); ++i)
	{

		if(g_zmaee_fixedapp[i].clsId == clsId)
		{

			*pROM = g_zmaee_fixedapp[i].pROM;
			*nSize = g_zmaee_fixedapp[i].nSize;
			*bZip = g_zmaee_fixedapp[i].bZip;
			return 1;

		}
	}

	return 0;
}

int ZMAEE_IsAppletInROM(ZM_AEECLSID clsId)
{
	int i;

	for(i = 0; i < sizeof(g_zmaee_fixedres) / sizeof(g_zmaee_fixedres[0]); ++i) {
		if(g_zmaee_fixedres[i].clsId == clsId)
			return E_ZM_AEE_TRUE;
	}

	return E_ZM_AEE_FALSE;
}

int ZMAEE_GetFixedAppletRes(ZM_AEECLSID clsId, const char **pRes, int *nSize)
{
	int i;
	for(i = 0; i < sizeof(g_zmaee_fixedres) / sizeof(g_zmaee_fixedres[0]); ++i) {
		if(g_zmaee_fixedres[i].clsId == clsId) {
			*pRes = g_zmaee_fixedres[i].pRes;
			*nSize = g_zmaee_fixedres[i].nSize;

			return E_ZM_AEE_SUCCESS;
		}
	}

	return E_ZM_AEE_NOTEXIST;
}

void ZMAEE_FixedAppEnumInit(void)
{
	sg_zmaee_fixedapp_enum_index = 0;
}

int ZMAEE_IsEnum(ZM_AEECLSID clsId)
{
	int i;

	for(i = 0; i < sizeof(sg_zmaee_halls) / sizeof(sg_zmaee_halls[0]); i++) {
		if(clsId == sg_zmaee_halls[i])
			return 0;
	}

	return 1;
}

int ZMAEE_FixedAppEnumNext(ZMAEEAppInfo* aif)
{
	int i, count = -1;

	for(i = 0; (i < sizeof(g_zmaee_fixedres) / sizeof(g_zmaee_fixedres[0])) && 
		(sg_zmaee_fixedapp_enum_index < sizeof(g_zmaee_fixedres) / sizeof(g_zmaee_fixedres[0])); ++i) 
	{
		count++;
		if(sg_zmaee_fixedapp_enum_index == count) {
			char *pRom;
			unsigned int nSize;
			
			sg_zmaee_fixedapp_enum_index++;
			if((g_zmaee_fixedres[i].clsId != 0xFFFFFFFF) && ZMAEE_IsEnum(g_zmaee_fixedres[i].clsId) 
				&& AEE_GetResFixedApp(g_zmaee_fixedres[i].clsId, &pRom, &nSize)) {
				memcpy(aif, pRom, sizeof(ZMAEEAppInfo));
				return E_ZM_AEE_SUCCESS;
			}
		}
	}

	return E_ZM_AEE_FAILURE;
}

int ZMAEE_GetAppletInfo(ZM_AEECLSID clsId, ZMAEEAppInfo *pai)
{
	int i;

	for(i = 0; i < sizeof(g_zmaee_fixedres) / sizeof(g_zmaee_fixedres[0]); ++i) {
		if(g_zmaee_fixedres[i].clsId == clsId) {
			char *pRom;
			unsigned int nSize;

			if(AEE_GetResFixedApp(clsId, &pRom, &nSize)) {
				memcpy(pai, pRom, sizeof(ZMAEEAppInfo));
				return E_ZM_AEE_SUCCESS;
			} else {
				return E_ZM_AEE_FAILURE;
			}
		}
	}

	return E_ZM_AEE_FAILURE;
}

#endif
