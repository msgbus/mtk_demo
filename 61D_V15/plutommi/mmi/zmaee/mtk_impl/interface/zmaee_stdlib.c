#ifdef __ZMAEE_APP__

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdarg.h"
#include "math.h"

#include "zmaee_priv.h"

/**
 *	zmaee_sprintf -  Write formatted data to a string.
 *	@buffer: Storage location for output
 *	@format: Format-control string
 *	@argument: Optional arguments
 *	RETURN VALUE
 *		The number of characters written, or 每1 if an error occurred.
 */
int zmaee_sprintf(char * buffer, const char *format, ...)
{
	int iReturn;
	va_list Args;
	va_start(Args,format);
	iReturn = vsprintf(buffer,format,Args);
	va_end(Args);
	return iReturn;
}

int zmaee_vsprintf(char *buffer, const char *format, va_list args)
{
	return vsprintf(buffer, format, args);
}

/**
 *	zmaee_srand -  Sets a random starting point.
 *	@seed: Seed for random-number generation
 *	RETURN VALUE
 *		not return a value.
 */
void zmaee_srand(unsigned int seed)
{
	srand(seed);
}

/**
 *	zmaee_rand -  Generates a pseudorandom number. 
 *	RETURN VALUE
 *		return a pseudorandom number.
 */
int zmaee_rand(void)
{
	return rand();
}


/**
 *	zmaee_memchr -  find byte in memory
 *	@buf: Pointer to buffer.
 *	@c: Character to look for.
 *	@count: Number of characters to check.
 *	RETURN VALUE
 *		If successful, returns a pointer to the first location of c in buf. Otherwise it returns NULL.
 */
void * zmaee_memchr(void * buf, char c, unsigned int count)
{
	return memchr(buf,(int)c,count);
}

/**
 *	zmaee_memcmp -  Compare characters in two buffers.
 *	@buf1: First buffer.
 *	@buf2: Second buffer.
 *	@count: Number of bytes.
 *	RETURN VALUE
 *		< 0: buf1 less than buf2
 *		0: buf1 identical to buf2
 *		>0: buf1 greater than buf2
 */
int zmaee_memcmp(void * buf1, void * buf2, unsigned int count)
{
	return memcmp((const void *)buf1,(const void *)buf2,count);
}


/**
 *	zmaee_memmove -  Moves one buffer to another.
 *	@dest: Destination object.
 *	@src: Source object.
 *	@count: Number of bytes to copy.
 *	RETURN VALUE
 *		The value of dest.
 */
void * zmaee_memmove(void * dest, void * src, unsigned int count)
{
	return memmove(dest,(const void *)src,count);
}

/**
 *	zmaee_memcpy -  copy bytes in memory
 *	@dest: New buffer.
 *	@src: Buffer to copy from.
 *	@count: Number of characters to copy.
 *	RETURN VALUE
 *		The value of dest.
 */
void * zmaee_memcpy(void * dest, const void * src, unsigned int count)
{
	return memcpy(dest,src,count);
}

/**
 *	zmaee_memset -  Sets buffers to a 1 bytes specified character.
 *	@dest: Pointer to destination.
 *	@c: Character to set.
 *	@count: Number of characters.
 *	RETURN VALUE
 *		The value of dest.
 */
void * zmaee_memset(void * dest, char c, unsigned int count )
{
	return memset(dest,(int)c,count);
}

/**
 *	zmaee_strtod -  Convert strings to a double-precision value.
 *	@nptr: Null-terminated string to convert.
 *	@endptr : Pointer to character that stops scan.
 *	RETURN VALUE
 *		returns the value of the floating-point number, returns 0 if no conversion can be performed or an underflow occurs.
 */
double zmaee_strtod(char * nptr, char ** endptr)
{
	return strtod((const char *)nptr,endptr);
}

/**
 *	zmaee_strtol -  Convert strings to a long-integer value.
 *	@nptr: Null-terminated string to convert.
 *	@endptr: Pointer to character that stops scan.
 *	@base: Number base to use.
 *	RETURN VALUE
 *		return the value represented in the string nptr, returns 0 if no conversion can be performed.
 */
long zmaee_strtol(char * str, char ** endptr, int base)
{
	return strtol((const char *)str,endptr,base);
}

/**
 *	zmaee_strcat -  Append a string. 
 *	@strDestination: Null-terminated destination string.
 *	@strSource : Null-terminated source string.
 *	RETURN VALUE
 *		return the destination string (strDestination). No return value is reserved to indicate an error.
 */
char * zmaee_strcat(char * strDestination, const char * strSource )
{
	return strcat(strDestination,strSource);
}

/**
 *	zmaee_strchr -  Append a string. 
 *	@str: Null-terminated source string.
 *	@c: Character to be located.
 *	RETURN VALUE
 *		return a pointer to the first occurrence of c in str, or NULL if c is not found.
 */
char * zmaee_strchr(const char * str, int c)
{
	return strchr(str,c);
}

/**
 *	zmaee_strcmp -  Compare strings.
 *	@string1: Null-terminated strings to compare.
 *	@string2: Null-terminated strings to compare.
 *	RETURN VALUE
 *		< 0: string1 less than string2
 *		  0: string1 identical to string2
 *		> 0: string1 greater than string2
 */
int zmaee_strcmp(const char * string1, const char * string2 )
{
	return strcmp(string1,string2);
}

/**
 *	zmaee_strncmp -  Compare characters of two strings
 *	@string1: Strings to compare.
 *	@string2: Strings to compare.
 *	@count: Number of characters to compare.
 *	RETURN VALUE
 *		< 0: string1 substring less than string2 substring
 *		  0: string1 substring identical to string2 substring
 *		> 0: string1 substring greater than string2 substring
 */
int zmaee_strncmp(const char * string1, const char * string2, unsigned int count)
{
	return strncmp(string1,string2,count);
}

/**
 *	zmaee_strcpy -  Copy a string.
 *	@strDestination: Destination string.
 *	@strSource: Null-terminated source string.
 *	RETURN VALUE
 *		return the destination string. No return value is reserved to indicate an error.
 */
char * zmaee_strcpy(char * strDestination, const char * strSource)
{
	return strcpy(strDestination, strSource);
}



/**
 *	zmaee_strlen -  Get the length of a string
 *	@str: Null-terminated string.
 *	RETURN VALUE
 *		return the number of characters in str, excluding the terminal NULL. No return value is reserved to indicate an error
 */
unsigned int zmaee_strlen(const char * str)
{
	return (unsigned int)strlen(str);
}

/**
 *	zmaee_strncat -  Append characters of a string
 *	@strDest: Null-terminated destination string.
 *	@strSource: Null-terminated source string.
 *	@count: Number of characters to append.
 *	RETURN VALUE
 *		Return a pointer to the destination string. No return value is reserved to indicate an error.
 */
char * zmaee_strncat(char * strDest, const char * strSource, unsigned int count)
{
	return strncat(strDest,strSource,count);
}

/**
 *	zmaee_strncpy -  Copy characters of one string to another.
 *	@strDest: Destination string.
 *	@strSource: Source string.
 *	@count: Number of characters to be copied.
 *	RETURN VALUE
 *		returns strDest. If count is less than or equal to the length of strSource, 
 *		a null character is not appended automatically to the copied string. 
 *		If count is greater than the length of strSource, 
 *		the destination string is padded with null characters up to length count.
 */
char * zmaee_strncpy(char * strDest, const char * strSource, unsigned int count)
{
	return strncpy(strDest,strSource,count);
}



/**
 *	zmaee_strcspn -  Returns the index of the first occurrence of a character in a string that belongs to a set of characters.
 *	@str: Null-terminated searched string.
 *	@strCharSet: Null-terminated character set.
 *	RETURN VALUE
 *		return the index of the first character in str that is in strCharSet. 
 *		If none of the characters in str is in strCharSet, then the return value is the length of str.
 */
unsigned int zmaee_strcspn(const char * str, const char * strCharSet)
{
	return (unsigned int)strcspn(str,strCharSet);
}

/**
 *	zmaee_strpbrk -  Scan strings for characters in specified character sets.
 *	@str: Null-terminated, searched string.
 *	@strCharSet: Null-terminated character set.
 *	RETURN VALUE
 *		Returns a pointer to the first occurrence of any character from strCharSet in str, 
 *		or a NULL pointer if the two string arguments have no characters in common.
 */
char * zmaee_strpbrk(const char * str, const char * strCharSet)
{
	return strpbrk(str,strCharSet);
}

/**
 *	zmaee_strrchr -  Scan a string for the last occurrence of a character.
 *	@str: Null-terminated string to search.
 *	@c: Character to be located.
 *	RETURN VALUE
 *		Return a pointer to the last occurrence of c in str, or NULL if c is not found.
 */
char * zmaee_strrchr(const char * str, int c)
{
	return strrchr(str,c);
}

/**
 *	zmaee_strspn -  Return the index of the first character in a string that does not belong to a set of characters.
 *	@str: Null-terminated string to search.
 *	@strCharSet: Null-terminated character set.
 *	RETURN VALUE
 *		Return an integer value specifying the length of the substring in str that consists entirely of characters in strCharSet. 
 *		If str begins with a character not in strCharSet, the function return 0.
 */
unsigned int zmaee_strspn(const char * str, const char * strCharSet)
{
	return (unsigned int)strspn(str,strCharSet);
}

/**
 *	zmaee_strstr -  Return a pointer to the first occurrence of a search string in a string.
 *	@str: Null-terminated string to search.
 *	@strSearch: Null-terminated string to search for.
 *	RETURN VALUE
 *		Return a pointer to the first occurrence of strSearch in str, or NULL if strSearch does not appear in str. 
 *		If strSearch points to a string of zero length, the function return str.
 */
char * zmaee_strstr(const char * str, const char * strSearch)
{
	return strstr(str,strSearch);
}

/**
 *	zmaee_wcscat -  Append a string. 
 *	@strDestination: Null-terminated destination string.
 *	@strSource: Null-terminated source string.
 *	RETURN VALUE
 *		return the destination string (strDestination). No return value is reserved to indicate an error.
 */
unsigned short * zmaee_wcscat(unsigned short * strDestination, const unsigned short * strSource)
{
	extern char *mmi_ucs2cat(char *, const char *);
	return (unsigned short*)mmi_ucs2cat((char*) strDestination, (const char*)strSource);
	
}

/**
*	zmaee_wcschr -  Append a string. 
*	@str: Null-terminated source string.
*	@c: Character to be located.
*	RETURN VALUE
*		return a pointer to the first occurrence of c in str, or NULL if c is not found.
*/
unsigned short * zmaee_wcschr(const unsigned short * str, unsigned short c)
{
	extern char *mmi_ucs2chr(const char *, unsigned short);
	return (unsigned short*)mmi_ucs2chr((const char*)str, c);
}

/**
*	zmaee_wcsrchr -  Scan a string for the last occurrence of a character.
*	@str: Null-terminated string to search.
*	@c: Character to be located.
*	RETURN VALUE
*		Return a pointer to the last occurrence of c in str, or NULL if c is not found.
*/
unsigned short * zmaee_wcsrchr(const unsigned short * str, unsigned short c)
{
	extern char *mmi_ucs2rchr(const char *, unsigned short);
	return (unsigned short *)mmi_ucs2rchr((const char *)str, c);
}


/**
*	zmaee_wcscmp -  Compare strings.
*	@string1: Null-terminated strings to compare.
*	@string2: Null-terminated strings to compare.
*	RETURN VALUE
*		< 0: string1 less than string2
*		  0: string1 identical to string2
*		> 0: string1 greater than string2
*/
int zmaee_wcscmp(const unsigned short * string1, const unsigned short * string2)
{
	extern int mmi_ucs2cmp(const char *, const char *);
	return mmi_ucs2cmp((const char*)string1, (const char*)string2);
}

/**
*	zmaee_wcsncmp -  Compare characters of two strings
*	@string1: Strings to compare.
*	@string2: Strings to compare.
*	@count: Number of characters to compare.
*	RETURN VALUE
*		< 0: string1 substring less than string2 substring
*		  0: string1 substring identical to string2 substring
*		> 0: string1 substring greater than string2 substring
*/
int zmaee_wcsncmp(const unsigned short * string1, const unsigned short * string2, unsigned int count)
{
	extern int mmi_ucs2ncmp(const char *, const char *, unsigned int);
	return mmi_ucs2ncmp((const char *)string1, (const char *)string2, count);
}

/**
*	zmaee_wcscpy -  Copy a string.
*	@strDestination: Destination string.
*	@strSource: Null-terminated source string.
*	RETURN VALUE
*		return the destination string. No return value is reserved to indicate an error.
*/
unsigned short * zmaee_wcscpy(unsigned short * strDestination, const unsigned short * strSource)
{
	extern char *mmi_ucs2cpy(char *, const char *);
	return (unsigned short*)mmi_ucs2cpy((char*)strDestination, (const char*)strSource);
}

/**
*	zmaee_wcsicmp -  Perform a lowercase comparison of strings.
*	@string1: Null-terminated strings to compare.
*	@string2: Null-terminated strings to compare.
*	RETURN VALUE
*		< 0: string1 less than string2
*		  0: string1 identical to string2
*		> 0: string1 greater than string2
*/
int zmaee_wcsicmp(const unsigned short * string1, const unsigned short * string2)
{
	extern int mmi_ucs2icmp(const char *, const char *);
	return mmi_ucs2icmp((const char*)string1, (const char*)string2);
}

/**
*	zmaee_wcslen -  Get the length of a string
*	@str: Null-terminated string.
*	RETURN VALUE
*		return the number of characters in str, excluding the terminal NULL. No return value is reserved to indicate an error
*/
unsigned int zmaee_wcslen(const unsigned short * str)
{
	extern int mmi_ucs2strlen(const char *);
	return (unsigned int)mmi_ucs2strlen((const char*)str);
}

/**
*	zmaee_wcsncat -  Append characters of a string
*	@strDest: Null-terminated destination string.
*	@strSource: Null-terminated source string.
*	@count: Number of characters to append.
*	RETURN VALUE
*		Return a pointer to the destination string. No return value is reserved to indicate an error.
*/
unsigned short * zmaee_wcsncat(unsigned short * strDest, const unsigned short * strSource, unsigned int count)
{
	extern char *mmi_ucs2ncat(char *, const char *, unsigned int);
	return (unsigned short *)mmi_ucs2ncat((char*)strDest, (const char *)strSource, count);
}

/**
*	zmaee_wcsncpy -  Copy characters of one string to another.
*	@strDest: Destination string.
*	@strSource: Source string.
*	@count: Number of characters to be copied.
*	RETURN VALUE
*		returns strDest. If count is less than or equal to the length of strSource, 
*		a null character is not appended automatically to the copied string. 
*		If count is greater than the length of strSource, 
*		the destination string is padded with null characters up to length count.
*/
unsigned short * zmaee_wcsncpy(unsigned short * strDest, const unsigned short * strSource, unsigned int count)
{
	extern char *mmi_ucs2ncpy(char *, const char *, unsigned int);
	return (unsigned short*)mmi_ucs2ncpy((char*)strDest, (const char*)strSource, count);
}

/**
*	zmaee_wcsnicmp -  Compare characters of two strings without regard to case.
*	@string1: Null-terminated strings to compare.
*	@string2: Null-terminated strings to compare.
*	@count: Number of characters to compare.
*	RETURN VALUE
*		< 0: string1 substring less than string2 substring 
*		  0: string1 substring identical to string2 substring 
*		> 0: string1 substring greater than string2 substring 
*/
int zmaee_wcsnicmp(const unsigned short * string1, const unsigned short * string2, unsigned int count)
{
	extern int mmi_ucs2nicmp(const char *, const char *, unsigned int);
	return mmi_ucs2nicmp((const char*)string1, (const char*)string2, count);
}


/**
*	zmaee_wcsstr -  Return a pointer to the first occurrence of a search string in a string.
*	@str: Null-terminated string to search.
*	@strSearch: Null-terminated string to search for.
*	RETURN VALUE
*		Return a pointer to the first occurrence of strSearch in str, or NULL if strSearch does not appear in str. 
*		If strSearch points to a string of zero length, the function return str.
*/
unsigned short * zmaee_wcsstr(const unsigned short * str, const unsigned short * strSearch)
{
	extern char *mmi_ucs2str(const char *, const char *);
	return (unsigned short*)mmi_ucs2str((const char *)str, (const char *)strSearch);
}

/**
*	zmaee_wcslwr -  Convert a string to lowercase. 
*	@str: Null-terminated string to convert to lowercase.
*	RETURN VALUE
*		return a pointer to the converted string
*/
unsigned short * zmaee_wcslwr(unsigned short * str)
{
	extern char *mmi_ucs2lwr(char *);
	return (unsigned short *)mmi_ucs2lwr((char *)str);
}

/**
*	zmaee_wcsupr -  Convert a string to uppercase. 
*	@str: String to capitalize.
*	RETURN VALUE
*		Return a pointer to the altered string. 
*/
unsigned short * zmaee_wcsupr(unsigned short * str)
{
	extern char *mmi_ucs2upr(char *);
	return (unsigned short*)mmi_ucs2upr((char*)str);
}


/**
 *	zmaee_hypot - Calculates the hypotenuse.
 *	@x: Floating-point value.
 *	@y: Floating-point value.
 *	RETURN VALUE
 *		return the length of the hypotenuse
 */
double zmaee_hypot(double x, double y)
{
//	return _FPMANGLE(hypot)(x, y);
	return hypot(x,y);
}

/**
 *	zmaee_fabs - Calculates the absolute value of the floating-point argument.
 *	@x: Floating-point value.
 *	RETURN VALUE
 *		return the absolute value of its argument. 
 */
double zmaee_fabs(double x)
{
	extern double fabs(double);
	return fabs(x);
}


/**
 *	zmaee_pow - Calculates x raised to the power of y.
 *	@x: Base.
 *	@y: Exponent.
 *	RETURN VALUE
 *		Return the value of xy.
 */
double zmaee_pow(double x, double y)
{
	extern double pow(double,double);
	return pow(x,y);
}

/**
 *	zmaee_sqrt - Calculates the square root.
 *	@x: Nonnegative floating-point value
 *	RETURN VALUE
 *		return the square-root of x. If x is negative, sqrt returns an indefinite, by default
 */
double zmaee_sqrt(double x)
{
	extern double sqrt(double);
	return sqrt(x);
}

/**
 *	zmaee_acos - Calculate the arccosine.
 *	@x: Value between 每1 and 1 whose arccosine is to be calculated.
 *	RETURN VALUE
 *		return the arccosine of x in the range 0 to 羽 radians.
 *		If x is less than 每1 or greater than 1, return an indefinite by default.
 */
double zmaee_acos(double x)
{
	extern double acos(double);
	return acos(x);
}

/**
 *	zmaee_asin - Calculate the arcsine.
 *	@x: Value whose arcsine is to be calculated.
 *	RETURN VALUE
 *		return the arcsine of x in the range 每羽/2 to 羽/2 radians.
 *		If x is less than 每1 or greater than 1, return an indefinite by default.
 */
double zmaee_asin(double x)
{
	extern double asin(double);
	return asin(x);
}

/**
 *	zmaee_atan - Calculates the arctangent of x
 *	@x: Any number.
 *	RETURN VALUE
 *		return the arctangent of x in the range of 每羽/2 to 羽/2 radians.
 */
double zmaee_atan(double x)
{
	extern double atan(double);
	return atan(x);
}

/**
 *	zmaee_cos - Calculate the cosine
 *	@x: Angle in radians.
 *	RETURN VALUE
 *		The cosine of x.
 */
double zmaee_cos(double x)
{
	extern double cos(double);
	return cos(x);
}

/**
 *	zmaee_sin - Calculate sines and hyperbolic sines.
 *	@x: Angle in radians.
 *	RETURN VALUE
 *		return the sine of x.
 */
double zmaee_sin(double x)
{
	extern double sin(double);
	return sin(x);
}

/**
 *	zmaee_tan - Calculate the tangent 
 *	@x: Angle in radians.
 *	RETURN VALUE
 *		return the tangent of x.
 */
double zmaee_tan(double x)
{

	extern double tan(double);
	return tan(x);
}


/**
 *	zmaee_ceil - Calculates the ceiling of a value.
 *	@x: Floating-point value.
 *	RETURN VALUE
 *		 return a double value representing the smallest integer that is greater than or equal to x.
 */
double zmaee_ceil(double x)
{
	extern double ceil(double);
	return ceil(x);
}

/**
 *	zmaee_floor - Calculates the floor of a value.
 *	@x: Floating-point value.
 *	RETURN VALUE
 *		 return a floating-point value representing the largest integer that is less than or equal to x.
 */
double zmaee_floor(double x)
{
	extern double floor(double);
	return floor(x);
}

/**
 *	zmaee_memset16 -  Sets buffers to a 2 bytes specified character.
 *	@dest: Pointer to destination.
 *	@c: Character to set.
 *	@count: Number of characters.
 *	RETURN VALUE
 *		The value of dest.
 */
void * zmaee_memset16(void * dest, short c, unsigned int count )
{
	extern void gd_memset_16(unsigned char*, unsigned int, unsigned int);
	gd_memset_16((unsigned char *)dest, (unsigned int)c, count);
	return (void *)dest;
}

/**
 *	zmaee_memset32 -  Sets buffers to a 4 bytes specified character.
 *	@dest: Pointer to destination.
 *	@c: Character to set.
 *	@count: Number of characters.
 *	RETURN VALUE
 *		The value of dest.
 */
void * zmaee_memset32(void * dest, int c, unsigned int count )
{
	extern void gd_memset_32(unsigned char*, unsigned int, unsigned int);
	gd_memset_32((unsigned char *)dest, (unsigned int)c, count);
	return (void *)dest;
}


#endif	// __ZMAEE_APP__
