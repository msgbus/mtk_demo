#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"


#if ((ZMAEE_SUPPORT_GSENSOR == 1) && (defined(MTK6235_09A_BB) || defined(KXP84_SPI) || defined(KXP84_I2C) || defined(KXP74_SPI) || defined(MXC6202_I2C) || defined (__BOSCH_BMA220__)))

#define		ZMAEE_GSENSOR_MAX_ACC_VALUE		(500)
#define		ZMAEE_GSENSOR_MIN_ACC_VALUE		(-500)

#ifdef __BOSCH_BMA220__
#define		ZMAEE_GSENSOR_MAX_ACC_X_VALUE	(16)
#define		ZMAEE_GSENSOR_MAX_ACC_Y_VALUE	(21)
#define		ZMAEE_GSENSOR_MAX_ACC_Z_VALUE	(18)
#endif

#define ZMAEE_IGSENSOR_FUNC_COUNT	3

static int 	ZMAEE_IGSensor_AddRef(AEE_IGSensor * po);
static int	ZMAEE_IGSensor_Release(AEE_IGSensor * po);
static int	ZMAEE_IGSensor_Retrive(AEE_IGSensor * po, int* x, int * y, int * z);

static void ZMAEE_IGSensor_PowerOn(AEE_IGSensor* po);
static void ZMAEE_IGSensor_PowerOff(AEE_IGSensor* po);

#ifdef __BOSCH_BMA220__
static void ZMAEE_IGSensor_JustifyValue(short *x, short *y, short *z);
#endif


typedef struct {
	void			*pVtbl;
	int     		nRef;
}ZMAEE_GSENSOR;
static ZMAEE_GSENSOR sg_zmaee_gsensor = {0};



int  ZMAEE_IGSensor_New(ZM_AEECLSID clsId, void** pObj) 
{
	ZMAEE_GSENSOR*  pThis = NULL;

	pThis = &sg_zmaee_gsensor;
	
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IGSENSOR_FUNC_COUNT,
									ZMAEE_IGSensor_AddRef,
									ZMAEE_IGSensor_Release,
									ZMAEE_IGSensor_Retrive);
		}
	}

	if(pThis->nRef == 0) {
		pThis->nRef = 1;
		ZMAEE_IGSensor_PowerOn((AEE_IGSensor*)&sg_zmaee_gsensor);
	}else {
		pThis->nRef++;
	}
	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
}


static
int ZMAEE_IGSensor_AddRef(AEE_IGSensor* po)
{
	ZMAEE_GSENSOR* pThis = (ZMAEE_GSENSOR*)po;
	
	if(!pThis) {
		return E_ZM_AEE_BADPARAM;
	}
	
	pThis->nRef++;
	
	return E_ZM_AEE_SUCCESS;
}


static
int ZMAEE_IGSensor_Release(AEE_IGSensor* po)
{
	ZMAEE_GSENSOR* pThis = (ZMAEE_GSENSOR*)po;
	
	if(!pThis) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef--;

	if(pThis->nRef == 0) {
		ZMAEE_IGSensor_PowerOff(po);
	}

	return E_ZM_AEE_SUCCESS;
}


/**
* retrive GSensor X, Y, Z，
*  只有当屏幕亮着的时候，才能获取到有效值，建议PowerOn以后将屏幕常量
*
* x ,y ,z 是三维坐标(z暂时没用，不考虑)		x, y值的范围(-500, 500)
* 当 x=y=0 时，手机处于水平放置状态。
* y<0 时，手机顶部的水平位置要大于底部。y值越小高度相差越大。
* y>0 时，手机顶部的水平位置要小于底部。y值越大高度相差越大。
* x<0 时，手机右侧的水平位置要大于左侧。x值越小高度相差越大。
* x>0 时，手机右侧的水平位置要小于左侧。x值越大高度相差越大。
*
* return :
*		E_ZM_AEE_BADPARAM		   po, x, y, or z is null
*  		E_ZM_AEE_SUCCESS           获取成功
*/
static
int ZMAEE_IGSensor_Retrive(AEE_IGSensor* po, int* x, int* y, int* z)
{
	ZMAEE_GSENSOR* pThis = (ZMAEE_GSENSOR*)po;
	short data_x = 0, data_y = 0, data_z = 0;

	if(!pThis || !x || !y || !z) {
		return E_ZM_AEE_BADPARAM;
	}

#if defined(MTK6235_09A_BB)
	{	
		ZMAEE_DebugPrint("ZMAEE_IGSensor_Retrive():define MTK6235_09A_BB");
		acc_sensor_get_bytes(&data_x, &data_y, &data_z);

		//获取到的数据按相应比例缩放到(-500,500)范围内。
		if(data_x > ZMAEE_GSENSOR_MAX_ACC_VALUE) {
			data_x = ZMAEE_GSENSOR_MAX_ACC_VALUE;
		} else if(data_x < ZMAEE_GSENSOR_MIN_ACC_VALUE) {
			data_x = ZMAEE_GSENSOR_MIN_ACC_VALUE;
		}

		if(data_y > ZMAEE_GSENSOR_MAX_ACC_VALUE) {
			data_y = ZMAEE_GSENSOR_MAX_ACC_VALUE;
		} else if(data_y < ZMAEE_GSENSOR_MIN_ACC_VALUE) {
			data_y = ZMAEE_GSENSOR_MIN_ACC_VALUE;
		}
	}
#elif defined(KXP84_SPI)
	{
		ZMAEE_DebugPrint("ZMAEE_IGSensor_Retrive():define KXP84_SPI");
		acc_sensor_get_data((unsigned short*)&data_x, (unsigned short*)&data_y, (unsigned short*)&data_z);
		//获取到的数据按相应比例缩放到(-500,500)范围内。
	}
#elif defined(KXP84_I2C)
	{
		ZMAEE_DebugPrint("ZMAEE_IGSensor_Retrive():define KXP84_I2C");
		acc_sensor_get_5bytes((unsigned short*)&data_x, (unsigned short*)&data_y, (unsigned short*)&data_z);
		//获取到的数据按相应比例缩放到(-500,500)范围内。
	}
#elif defined(KXP74_SPI)
	{
		ZMAEE_DebugPrint("ZMAEE_IGSensor_Retrive():define KXP74_SPI");
		acc_sensor_get_data((unsigned short*)&data_x, (unsigned short*)&data_y, (unsigned short*)&data_z);
		//获取到的数据按相应比例缩放到(-500,500)范围内。
	}
#elif defined(MXC6202_I2C)
	{
		ZMAEE_DebugPrint("ZMAEE_IGSensor_Retrive():define MXC6202_I2C");
		acc_sensor_get_bytes(&data_x, &data_y, &data_z);
		//获取到的数据按相应比例缩放到(-500,500)范围内。
	}
#elif defined (__BOSCH_BMA220__)
	{
		acc_sensor_get_6bytes((unsigned short*)&data_x, (unsigned short*)&data_y, (unsigned short*)&data_z);
		ZMAEE_IGSensor_JustifyValue(&data_x, &data_y, &data_z);
	}
#endif

	*x = data_x;
	*y = data_y;
	*z = data_z;

	return E_ZM_AEE_SUCCESS;
}



/**
 * 启动重力感应器设备
 */
static
void ZMAEE_IGSensor_PowerOn(AEE_IGSensor* po)
{
	ZMAEE_GSENSOR* pThis = (ZMAEE_GSENSOR*)po;

	if(!pThis) {
		return;
	}

	acc_sensor_init();
	acc_sensor_pwr_up();
}




/**
 * 关闭重力感应器设备
 */
static
void ZMAEE_IGSensor_PowerOff(AEE_IGSensor* po)
{
	ZMAEE_GSENSOR* pThis = (ZMAEE_GSENSOR*)po;

	if(!pThis) {
		return;
	}
	
	acc_sensor_pwr_down();
}

#ifdef __BOSCH_BMA220__

#define ZMAEE_JUSTIFY_VALUE(value, max_value, tar_value, tar_range)		\
	do {																\
		tar_value = value * tar_range / max_value;						\
		if(tar_value < 0) {												\
			if(tar_value < ZMAEE_GSENSOR_MIN_ACC_VALUE)					\
				tar_value = ZMAEE_GSENSOR_MIN_ACC_VALUE;				\
		} else {														\
			if(tar_value > ZMAEE_GSENSOR_MAX_ACC_VALUE)					\
				tar_value = ZMAEE_GSENSOR_MAX_ACC_VALUE;				\
		}																\
	}while(0)

static
void ZMAEE_IGSensor_JustifyValue(short *x, short *y, short *z)
{
	short value_x = *x;
	short value_y = *y;

	ZMAEE_JUSTIFY_VALUE(value_x, ZMAEE_GSENSOR_MAX_ACC_X_VALUE, value_x, ZMAEE_GSENSOR_MAX_ACC_VALUE);
	ZMAEE_JUSTIFY_VALUE(value_y, ZMAEE_GSENSOR_MAX_ACC_Y_VALUE, value_y, ZMAEE_GSENSOR_MAX_ACC_VALUE);

	if(z < 0) {
		*x = value_x;
		*y = -value_y;
	} else {
		*x = -value_x;
		*y = value_y;
	}

	*z = 2048;	// XXX: fixed value
}
#endif

#else //((ZMAEE_SUPPORT_GSENSOR == 1) && (defined(MTK6235_09A_BB) || defined(KXP84_SPI) || defined(KXP84_I2C) || defined(KXP74_SPI) || defined(MXC6202_I2C)))

int ZMAEE_IGSensor_New(ZM_AEECLSID clsId,void **ppobj)
{
	*ppobj = NULL;
	return E_ZM_AEE_CLASSNOTSUPPORT;
}

#endif



#endif	// __ZMAEE_APP__

