#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"
#include "kal_non_specific_general_types.h"

#include "med_smalloc.h"
#include "med_utility.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#include "AudioPlayerPlayList.h"  		
#endif

#include "GlobalConstants.h"

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_mem_gprot.h"
#include "mmi_frm_events_gprot.h"
#include "cbm_api.h"
#else

#endif


#include "IdleAppResDef.h"
#include "wgui_categories_util.h"
#include "gdi_include.h"
#include "CommonScreens.h"

#include "MessagesMiscell.h"
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
#include "SMSApi.h"
#include "SMSStruct.h"
#include "wgui_status_icons.h"
#endif
#include "MessagesResourceData.h"

#include "statusiconres.h"

#include "UcmGprot.h"
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
#include "UCMCallProt.h"
#endif

#include "MainMenuDef.h"
#include "PhoneSetup.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0)
#include "smssrvgprot.h"
#endif

#include "IdleAppDef.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "GlobalResDef.h"
#include "TimerEvents.h"
#if !defined (__COSMOS_MMI_PACKAGE__)
#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
#include "mmi_rp_app_mainmenu_def.h"
#if defined(__MMI_VUI_LAUNCHER__) || defined (MT6250)||defined (MT6260)||defined (MT6261)//sgf
#include "mmi_rp_app_zmaee_def.h"
#endif
#endif
#else
#if (ZM_AEE_MTK_SOFTVERN >= 0x1220)
#include "mmi_rp_vapp_launcher_mm_def.h"
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#include "mmi_rp_app_mainmenu_def.h" 
#else
#include "mmi_rp_app_idle_def.h"
#endif 
#include "simctrlsrvgprot.h"
#include "bootupsrvgprot.h"
#include "mmi_frm_events.h"
#include "CommonScreensResDef.h"
#endif

#include "FileMgrType.h"
#include "AudioPlayerDef.h"
#include "AudioPlayerPlayList.h"
#include "AudioPlayerProt.h"
#include "gui_buttons.h"
#include "PhoneSetup.h"
#include "PhoneSetupGprots.h"
#ifdef WIN32
#include "ScrLockerGprot.h"//WLJ
#endif
#ifdef MT6236
#include "MediaPlayerPlayList.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
#include "mmi_rp_vapp_zmaeemain_def.h"
#ifdef __ZMAEE_APP_KEYPAD_LOCK__
#include "mmi_rp_vapp_zmaeekeylock_def.h"
#endif
#ifdef __ZMAEE_APP_THEME__
#include "mmi_rp_vapp_zmaeetheme_def.h"
#endif
#ifdef __ZMAEE_APP_DWALLPAPER__
#include "mmi_rp_vapp_zmaeewallpaper_def.h"
#endif
#include "mmi_rp_vapp_cosmos_mainmenu_def.h"
#endif
#endif

#if (defined MT6252 && (ZM_AEE_MTK_SOFTVERN == 0x11B0)) || (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
#ifndef __COSMOS_MMI_PACKAGE__
#include "ViewSettingCommonProt.h"
#endif
#include "SmsGuiInterfaceType.h"

#endif

#ifdef ZMAEE_SUPPORT_PNG_DECODE
#include "lcd_if.h"
#endif

#ifdef WIN32
#include "mmi_frm_prot.h"//WLJ
#else
#include "touch_panel.h"
#endif

#include "USBSrvGprot.h"//WLJ
#include "gpiosrvgprot.h"

#ifdef __ZMAEE_APP_MENU_USE_RES__
#include "mmi_rp_vapp_zmaeemain_def.h"
#endif

#ifdef __COSMOS_MMI_PACKAGE__
#include "mmi_rp_app_usbsrv_def.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
int g_zmaee_keep_wallpaper_appid;
#endif


#define ZMAEE_POWERONINIT_RETRYTIMES		10

#ifdef __MMI_TOUCH_SCREEN__
typedef struct {
	unsigned short sample_low;
	unsigned short sample_high;
	unsigned short offset;
	unsigned short long_tap_offset;
	unsigned short stroke_offset;
	unsigned short stroke_long_tap_offset;
}ZMAEE_PENEVENT_PARAM;
static ZMAEE_PENEVENT_PARAM sg_zmaee_penevent_param = {0};
#endif


static int g_zmaee_plugin_mem_type = 0;
static void (*sg_zmaee_reentry_func)(void) = 0;
int sg_zmaee_restart_theme = 0;
static int pwron_init_times = 0;
#ifdef __ZMAEE_APP_THEME__
int sg_zmaee_mainmenu_keylock_flag = 0;
#endif
extern int ZMAEE_ReadRecord(void *pBuffer, U16 nBufferSize);
extern int ZMAEE_WriteRecord(void *pBuffer, U16 nBufferSize);
#define ZMAEE_NVRAM_CONFIG_PATH        "E:\\zmaee\\ui_cfg.dat"  

int sg_zmaee_start_screensaver = 0;
int sg_zmaee_dwp_set_mask = 0;
static unsigned short sg_zmaee_wnd_screen_id = 0;

#ifdef __ZMAEE_APP_DWALLPAPER__
static void *sg_zmaee_wallpaper_object = 0;
#endif

// <<< 下载器启动应用支持启动参数
static unsigned char sg_zmaee_download_params_content[256] = {0};
static int			 sg_zmaee_download_params_length = 0;
//  下载器启动应用支持启动参数>>>

extern void 	mmi_msg_stop_msg_sub_anm(void);
extern void 	mmi_msg_start_msg_sub_anm(void);
extern void 	SetMessagesCurrScrnID(U16 scrnID);
extern U8 		IsKeyPadLockState(void);
extern kal_uint32 applib_mem_screen_get_max_alloc_size(void);
#if (ZM_AEE_MTK_SOFTVERN < 0x0852)
extern void* 	mmi_frm_scrmem_alloc(U32 mem_size);
extern void 	mmi_frm_scrmem_free(void * mem_ptr);
#endif
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
extern pBOOL 	mmi_bootup_is_network_searching_complete(void);
#ifdef __MMI_DUAL_SIM_MASTER__
extern pBOOL 	mmi_bootup_is_sim2_network_searching_complete(void);
#endif
#endif
extern int 		ZMAEE_GetMaxAllocMedMem(void);
extern AEE_IFile* ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode);
#ifdef WIN32
extern void mmi_idle_display(void);
#endif

zm_extern AEE_IShell* 			ZMAEE_GetShell(void);
zm_extern int 					ZMAEE_IShell_SetWorkDir(AEE_IShell * po, const char*szWorkDir, unsigned int mask);
zm_extern int 					ZMAEE_IShell_StartApplet(AEE_IShell * po, ZM_AEECLSID clsId, void *pUser, int len);
zm_extern int 					ZMAEE_IShell_GetEndKeyMask(void);
zm_extern unsigned int			zmaee_wcslen(const unsigned short * str);
zm_extern unsigned int			zmaee_strlen(const char * str);
zm_extern unsigned short*		zmaee_wcsncpy(unsigned short * strDest,const unsigned short * strSource,unsigned int count);
zm_extern void 					ZMAEE_Exit(void);
zm_extern int 					ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
zm_extern int 					ZMAEE_IShell_CanStartApplet(AEE_IShell * po, ZM_AEECLSID clsId);
zm_extern void 					ZMAEE_IShell_SetEndKeyMask(AEE_IShell* po, int mask);
zm_extern void 					ZMAEE_DebugPrint(const char *pszFormat, ...);
zm_extern int					ZMAEE_DebugSwitch(int nOn);
zm_extern void 					ZMAEE_IShell_FreeGlobalLibrary(AEE_IShell *po, int handle);
unsigned int 					ZMAEE_GetThemeAppId(int type);
zm_extern int 					ZMAEE_IShell_StartApplet(AEE_IShell * po, ZM_AEECLSID clsId, void *pUser, int len);
zm_extern int 					ZMAEE_IFile_Release(AEE_IFile* po);
zm_extern int 					ZMAEE_IMemStream_Release(AEE_IMemStream* po);
zm_extern AEE_IFile* 			ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode);
zm_extern char* 				ZMAEE_GetRootDir(void);
zm_extern unsigned short* 		ZMAEE_File_ConvertFileName(const char* pszFile);
zm_extern int 					ZMAEE_Background_IsRunning(char *dll_name);
zm_extern void 					ZMAEE_Background_Exit(char *dll_name);
zm_extern const char* 			ZMAEE_IShell_GetWorkDir(AEE_IShell * po);
zm_extern void 					ZMAEE_IDisplay_FreeWallpaperLayer(void);
zm_extern unsigned int  		ZMAEE_IShell_GetAppletMask(AEE_IShell* po);
zm_extern int 					ZMAEE_FileInlib_Release(AEE_IFile * po);
#ifdef WIN32
zm_extern void					ZMAEE_Exit_Ext(void);
zm_extern int 					ZMAEE_IShell_CreateInstance(AEE_IShell * po, ZM_AEECLSID clsId, void** ppobj);
zm_extern char 					ZMAEE_IFileMgr_GetDriver(AEE_IFileMgr* po, int type);
zm_extern int 					ZMAEE_IFileMgr_Test(AEE_IFileMgr* po, const char* pszFile);
zm_extern int					ZMAEE_IFileMgr_MkDir(AEE_IFileMgr* po, const char* pszDir);
zm_extern int					ZMAEE_IsAppletInROM(ZM_AEECLSID clsId);
zm_extern int					ZMAEE_IShell_ValidateApplet(AEE_IShell *po, ZM_AEECLSID clsId);
zm_extern int 					ZMAEE_Ucs2_2_Utf8(const unsigned short* wcsSrc, int nwcsLen, char* pDst, int nSize);
zm_extern int 					ZMAEE_IFileMgr_GetInfo(AEE_IFileMgr* po, const char* pszFile, ZMAEE_FileInfo* pfi);
zm_extern int 					ZMAEE_IFileMgr_Release(AEE_IFileMgr* po);
zm_extern int					ZMAEE_FileInlib_Read(AEE_IFileMgr* po, char * pTmp, int num);
zm_extern int 					ZMAEE_IFile_Write(AEE_IFile * po, void * pSrc, unsigned long nWant);

zm_extern int					ZMAEE_CheckPlat_IsRunning(void);
zm_extern int					ZMAEE_Theme_IsValidate(void);
zm_extern ZM_AEECLSID			ZMAEE_IShell_ActiveApplet(AEE_IShell * po);
zm_extern int					ZMAEE_IFileMgr_New(ZM_AEECLSID clsId, void** pObj);
zm_extern int 					ZMAEE_IFile_Read(AEE_IFile * po, void * pDest, unsigned long nWant);
zm_extern int					ZMAEE_IMemStream_Create(int CLS_ID, void** iMemStream, unsigned char* image_data, unsigned long data_size);
#endif


#ifdef __COSMOS_MMI_PACKAGE__
extern mmi_ret 				ZMAEE_IdleNotify_Exit_MS_Hdlr(mmi_event_struct *evt);
extern mmi_ret 				ZMAEE_IdleNotify_Entry_MS_Hdlr(mmi_event_struct *evt);
extern U8 *GetCurrGuiBuffer(U16 scrnid);
#endif

#ifndef __MMI_FRM_HISTORY__
void zmaee_dummy_entry(void)
{
}

mmi_ret zmaee_group_proc(mmi_event_struct* evt)
{
	return MMI_RET_OK;
}
#endif


/**
  * 大块内存获取,内部使用
  */
int ZMAEE_GetRamSize(void)
{
	return ZMAEE_APPRAM_SIZE;
}

#ifdef ZMAEE_USE_CACHE_MEMORY   
// XXX:
void zmaee_cache_free_cb(void *mem_ptr)
{
	
}
#endif  
 
void* ZMAEE_MallocRam(void)
{
#ifdef __MED_IN_ASM__
	//if(applib_mem_ap_get_max_alloc_size() < ZMAEE_APPRAM_SIZE)
	//	return NULL;
	applib_mem_ap_free_all_cache();

#ifdef ZMAEE_USE_CACHE_MEMORY
	return applib_mem_ap_cache_alloc(ZMAEE_APPRAM_SIZE, zmaee_cache_free_cb);
#else
	return applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, ZMAEE_APPRAM_SIZE);
#endif

#else
	if( ZMAEE_GetMaxAllocMedMem() < ZMAEE_APPRAM_SIZE )
		return NULL;

#ifdef ZMAEE_USE_CACHE_MEMORY
	return med_alloc_ext_mem_cacheable(ZMAEE_APPRAM_SIZE);
#else
    return med_alloc_ext_mem( ZMAEE_APPRAM_SIZE );
#endif

#endif
}

void  ZMAEE_FreeRam(void* p)
{
#ifdef __MED_IN_ASM__

#ifdef ZMAEE_USE_CACHE_MEMORY
	applib_mem_ap_cache_free(p);
#else	
	applib_mem_ap_free(p);
#endif

#else

#ifdef ZMAEE_USE_CACHE_MEMORY
	med_free_ext_mem(&p);
#else
	med_free_ext_mem(&p);
#endif

#endif
}

int ZMAEE_GetPluginRamSize(void)
{
	return ZMAEE_PLUGIN_RAM_SIZE;
}

void* ZMAEE_PlugInMallocRam( void )
{
#ifndef __MED_IN_ASM__
	if( ZMAEE_GetMaxAllocMedMem() > ZMAEE_PLUGIN_RAM_SIZE )
	{
		g_zmaee_plugin_mem_type = 1;
	#ifdef ZMAEE_USE_CACHE_MEMORY
		return med_alloc_ext_mem_cacheable(ZMAEE_PLUGIN_RAM_SIZE);
	#else
		return med_alloc_ext_mem( ZMAEE_PLUGIN_RAM_SIZE );
	#endif
	}
#else
	if(applib_mem_ap_get_max_alloc_size() > ZMAEE_PLUGIN_RAM_SIZE)
	{
		g_zmaee_plugin_mem_type = 1;
	#ifdef ZMAEE_USE_CACHE_MEMORY
		return applib_mem_ap_cache_alloc(ZMAEE_PLUGIN_RAM_SIZE, zmaee_cache_free_cb);
	#else
		return applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, ZMAEE_PLUGIN_RAM_SIZE);
	#endif
	}
#endif

	if( applib_mem_screen_get_max_alloc_size() > ZMAEE_PLUGIN_RAM_SIZE )
	{
		g_zmaee_plugin_mem_type = 2;
		return mmi_frm_scrmem_alloc(ZMAEE_PLUGIN_RAM_SIZE);
	}
	return NULL;
}

void  ZMAEE_PlugInFreeRam(void* p)
{
	if(p == NULL) 
		return;

	switch(g_zmaee_plugin_mem_type)
	{
	case 1:
		#ifdef __MED_IN_ASM__
			#ifdef ZMAEE_USE_CACHE_MEMORY
				applib_mem_ap_cache_free(p);
			#else
				applib_mem_ap_free(p);
			#endif
		#else
			#ifdef ZMAEE_USE_CACHE_MEMORY
				med_free_ext_mem(&p);
			#else
				med_free_ext_mem(&p);
			#endif
		#endif
		break;
	case 2:
		mmi_frm_scrmem_free(p);
		break;
	}

}

/**
 * Idle Notify
 */
void ZMAEE_BackToIdle(void)
{
#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
	mmi_idle_display();
#else
	DisplayIdleScreen();
#endif
}

/**
 * Go Back History
 */
void ZMAEE_DestroyWnd(void)
{
#ifdef __MMI_FRM_HISTORY__
	U16 screenId = GetActiveScreenId();
	
	if(screenId == SCR_ZMAEE_MAIN)
	{
		GoBackHistory();
	}
	else if(screenId != IDLE_SCREEN_ID)
	{
		DeleteScreenIfPresent(SCR_ZMAEE_MAIN);
		DeleteScreenIfPresent(SCR_ZMAEE_INPUT_WND);
	}
#else
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN);
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_INPUT_WND);	
#endif
}

/**
 * 选卡操作
 * 参数:
 * 	owner					0 - call, 1- sms, 2 - network
 * 	channel					0 - sim1, 1 - sim2
 * 返回:
 * 	void
 */
void ZMAEE_ChangeChannel(int owner, int channel)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
#ifdef __MMI_DUAL_SIM_MASTER__
	E_MTPNP_AD_CHANNELS_OWNER r_owner;
	E_MTPNP_AD_CHANNELS		  r_channel;

	ZMAEE_DebugPrint("ZMAEE_ChangeChannel: owner = %d, channel = %d", owner, channel);

	// 选卡
	switch(channel) {
		case 0:
			r_channel = MTPNP_AD_CHANNEL_MASTER;
			break;
		case 1:
			r_channel = MTPNP_AD_CHANNEL_SLAVE;
			break;
		default:
			return;
	};

	// 选通道
	if(owner != 2) {
		switch(owner) {
			case 0:
				r_owner = MTPNP_AD_CALL_CHANNEL;
				break;
			case 1:
				r_owner = MTPNP_AD_SMS_CHANNEL;
				break;
			default:
				return;
		};

		MTPNP_AD_Get_Channel(r_owner);
		MTPNP_AD_Free_Channel(r_owner);
		MTPNP_AD_Set_Channel(r_owner, r_channel);
	}
#endif//__MMI_DUAL_SIM_MASTER__
#endif
}

/*********************************************************************************
 * 短信截取函数
 *********************************************************************************/
int sg_zmaee_sms_silent = 0;

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0)

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#define MSG_NUM_LEN			AEE_MAX_NUM_SIZE
#endif

unsigned char ZMAEE_Sms_Check(srv_sms_event_new_sms_struct* event_info)
{
	U16 msg_id = event_info->msg_id;
	srv_sms_new_msg_struct *new_msg_data = (srv_sms_new_msg_struct*)event_info->msg_data;
	int asc_num_len;
	ZMAEE_NotifySms notify_sms = {0};

	// sms number
	asc_num_len = (strlen((S8*)new_msg_data->number) >= sizeof(notify_sms.szPhone))? (sizeof(notify_sms.szPhone) - 1) : strlen((S8*)new_msg_data->number);
	if(memcmp(new_msg_data->number, "86", 2) == 0) {
		memcpy(notify_sms.szPhone, new_msg_data->number + 2, asc_num_len - 2);
	} else if(memcmp(new_msg_data->number, "+86", 3) == 0 ) {
		memcpy(notify_sms.szPhone, new_msg_data->number + 3, asc_num_len - 3);
	} else {
		memcpy(notify_sms.szPhone, new_msg_data->number, asc_num_len);
	}

	// sms content
	if (new_msg_data->dcs == SMSAL_UCS2_DCS)
	{
		int len = 2 * mmi_ucs2strlen((const CHAR*)event_info->content);
		if(len >= sizeof(notify_sms.wcsMsg))
			len = sizeof(notify_sms.wcsMsg) - 2;
		memcpy(notify_sms.wcsMsg, event_info->content, len);
	}
	else
	{
		int len = strlen((const char*)event_info->content);
		if(len < new_msg_data->message_len)
		{
			int _len = 2 * mmi_ucs2strlen((const CHAR*)event_info->content);
			if(_len >= sizeof(notify_sms.wcsMsg))
				_len = sizeof(notify_sms.wcsMsg) - 2;
			memcpy(notify_sms.wcsMsg, event_info->content, _len);
		}
		else
		{
			int n;
			if(len >= sizeof(notify_sms.wcsMsg)/2)
				len = sizeof(notify_sms.wcsMsg)/2 - 1;
			for(n=0; n < len; ++n)
				notify_sms.wcsMsg[n] = event_info->content[n];
		}
	}

	// sms port
	notify_sms.nPort = new_msg_data->dest_port;

	// handle sms
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_NOTIFY, (unsigned long)ZMAEE_NOTIFY_SMS, (unsigned long)&notify_sms);

	ZMAEE_DebugPrint("ZMAEE_Sms_Check: notify_sms.nResult: %d", notify_sms.nResult);

	if(notify_sms.nResult == 1) {
		return 1;
	} else if(notify_sms.nResult == 2) {
		sg_zmaee_sms_silent = 1;

		return 0;
	} else {
		return 0;
	}
}

#else
unsigned char ZMAEE_Sms_Check(void* data, int withobject, void* content)
{
	mmi_frm_sms_deliver_msg_struct *msg_info = (mmi_frm_sms_deliver_msg_struct*)data;
	int asc_num_len;
	ZMAEE_NotifySms notify_sms = {0};

	if(withobject) {
	   return 0;
	}

	// sms number
	asc_num_len = (msg_info->addr_number.length >= sizeof(notify_sms.szPhone))? (sizeof(notify_sms.szPhone) - 1) : msg_info->addr_number.length;
	if (msg_info->addr_number.type == CSMCC_INTERNATIONAL_ADDR)
	{
		if(memcmp(msg_info->addr_number.number, "+86", 3) == 0 )
		{
			memcpy(notify_sms.szPhone, msg_info->addr_number.number + 3, asc_num_len - 3);
		}
		else
		{
			memcpy(notify_sms.szPhone, msg_info->addr_number.number, asc_num_len);
		}
	}
	else
	{
		memcpy(notify_sms.szPhone, msg_info->addr_number.number, asc_num_len);
	}

	//sms content
	if(msg_info->dcs == SMSAL_UCS2_DCS)
	{
		int len = zmaee_wcslen((const unsigned short*)content) * 2;
		if(len >= sizeof(notify_sms.wcsMsg))
			len = sizeof(notify_sms.wcsMsg) - 2;
		memcpy(notify_sms.wcsMsg, content, len);
	}
	else
	{
		int len = strlen((const char*)content);
		if(len < msg_info->no_msg_data)
		{
			int _len = zmaee_wcslen((const unsigned short*)content) * 2;
			if(_len >= sizeof(notify_sms.wcsMsg))
				_len = sizeof(notify_sms.wcsMsg) - 2;
			memcpy(notify_sms.wcsMsg, content, _len);
		}
		else
		{
			int n;
			if(len >= sizeof(notify_sms.wcsMsg)/2)
				len = sizeof(notify_sms.wcsMsg)/2 - 1;
			for(n=0; n < len; ++n)
				notify_sms.wcsMsg[n] = ((unsigned char*)content)[n];
		}
	}

	// sms port
	notify_sms.nPort = msg_info->dest_port;

	ZMAEE_DebugPrint("ZMAEE_Sms_Check: withobject = %d, num=%s", withobject, notify_sms.szPhone);

	// handle sms
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_NOTIFY, (unsigned long)ZMAEE_NOTIFY_SMS, (unsigned long)&notify_sms);

	ZMAEE_DebugPrint("ZMAEE_Sms_Check: notify_sms.nResult: %d", notify_sms.nResult);

	if(notify_sms.nResult == 1) {
		return 1;
	} else if(notify_sms.nResult == 2) {
		sg_zmaee_sms_silent = 1;

		return 0;
	} else {
		return 0;
	}
}

unsigned char ZMAEE_Sms_Handle(void* data, void* content)
{
    mmi_frm_sms_deliver_msg_struct *msg_info = (mmi_frm_sms_deliver_msg_struct*) data;
    
	if(mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_APP_INBOX) != MMI_FRM_SMS_INVALID_INDEX)
		mmi_frm_sms_delete_sms(NULL, MOD_MMI, MMI_FRM_SMS_APP_AWAITS, msg_info->index);

    return TRUE;
}
#endif

/*********************************************************************************
 *来电截取函数
 *********************************************************************************/
int sg_zmaee_call_norecord = 0;
int sg_zmaee_call_connect = 0;

 /**
  * @direction: 0 - outcoming, 1 - incoming
  * @nOn_Off: 0 - On, 1 - Off
  */
unsigned char ZMAEE_Call_Handle(char *szNum, short *wcsDisp, unsigned int wcsDispLen, int direction, int nOn_Off)
{
	ZMAEE_NotifyCall notify_call = {0};
	int len;

	if(strlen(szNum) > 10 && memcmp(szNum, "+86", 3) == 0)
		szNum = szNum + 3;
	else if(strlen(szNum) > 10 && memcmp(szNum, "86", 2) == 0)
		szNum = szNum + 2;

	strncpy(notify_call.szPhone, szNum, sizeof(notify_call.szPhone) - 1);

	len = zmaee_wcslen((const unsigned short*)wcsDisp);
	if(len >= sizeof(notify_call.wcsDisplay) / 2)	len = sizeof(notify_call.wcsDisplay) / 2 - 1;
	memcpy(notify_call.wcsDisplay, wcsDisp, len * 2);

	notify_call.nIn_Out = direction;
	notify_call.nOn_Off = nOn_Off;
	notify_call.bConnect = sg_zmaee_call_connect;

	// 事件处理
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_NOTIFY, (unsigned long)ZMAEE_NOTIFY_CALL, (unsigned long)&notify_call);

	// 修改Display Name
	len = zmaee_wcslen((const unsigned short*)notify_call.wcsDisplay);
	if(len >= wcsDispLen)	len = wcsDispLen - 1;
	memcpy(wcsDisp, notify_call.wcsDisplay, len * 2);
	wcsDisp[len] = 0x0000;
	
	if(notify_call.nResult == 2)
		sg_zmaee_call_norecord = 1;
	
	return notify_call.nResult;	
}

/*********************************************************************************
 *Idle界面通知机制
 *********************************************************************************/
#define AEE_MAX_IDLENOTIFY_COUNT		2

typedef struct {
	int				bValid;
	unsigned short  wcsDisplay[128];	
	ZM_AEECLSID     clsAppletId;
	unsigned short  wcsName[16];
}ZMAEE_IDLENOTIFY_ITEM;

static ZMAEE_IDLENOTIFY_ITEM sg_zmaee_idle_notify_items[AEE_MAX_IDLENOTIFY_COUNT] = {0};

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0)

static
int ZMAEE_Idle_Notify_GetValidIdx(void)
{
	int i;

	for(i = 0; i < AEE_MAX_IDLENOTIFY_COUNT; i++) {
		if(sg_zmaee_idle_notify_items[i].bValid)
			return i;
	}

	return -1;
}

static
void ZMAEE_Idle_Notify_GoBack(void)
{
	sg_zmaee_idle_notify_items[ZMAEE_Idle_Notify_GetValidIdx()].bValid = 0;
	GoBackHistory();
}

static
void ZMAEE_Idle_Notify_View(void)
{
	extern void ZMAEE_AppletEntry(ZM_AEECLSID cls_id, const char* work_dir, void* param, int param_len, char *appName, char *appIcon, unsigned int nIconLen, int bkg, int app_mask, void (*reentry_func)(void), unsigned short scr_id);	
	int idx;

	idx = ZMAEE_Idle_Notify_GetValidIdx();
	sg_zmaee_idle_notify_items[idx].bValid = 0;
	
	ZMAEE_AppletEntry(sg_zmaee_idle_notify_items[idx].clsAppletId, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)sg_zmaee_idle_notify_items[idx].wcsName, NULL, 0, 0, 0xFFFFFFFF, NULL, 0);
}

/**
 * Idle界面是否显示Notify界面
 * RETURN:
 * 1 - Need Notify
 * 0 - No Notify
 */
unsigned char ZMAEE_Idle_Need_Notify(void)
{
	int valid_idx = ZMAEE_Idle_Notify_GetValidIdx();

	ZMAEE_DebugPrint("ZMAEE_Idle_Need_Notify: valid_idx = %d", valid_idx);
	if(valid_idx >= 0)
		return TRUE;
	else
		return FALSE;
}

/**
 * Idle Notify Screen
 */
void ZMAEE_Idle_Notify(void)
{
#ifndef __COSMOS_MMI_PACKAGE__
	int idx;

#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_IDLE_NOTIFY_WND, mmi_msg_stop_msg_sub_anm, NULL, NULL);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_IDLE_NOTIFY_WND, mmi_msg_stop_msg_sub_anm, NULL, MMI_FRM_FULL_SCRN);
#endif
	SetMessagesCurrScrnID(SCR_ZMAEE_IDLE_NOTIFY_WND);
	ForceSubLCDScreen(mmi_msg_start_msg_sub_anm);

	idx = ZMAEE_Idle_Notify_GetValidIdx();

#if(ZM_AEE_MTK_SOFTVERN < 0x11B0) 
	if (IsKeyPadLockState() == 0) 
	{
#endif	
		ShowCategory154Screen(
			0,
			0,
			STR_GLOBAL_VIEW,
			IMG_GLOBAL_OK,
			STR_GLOBAL_BACK,
			IMG_GLOBAL_BACK,
			(PU8)sg_zmaee_idle_notify_items[idx].wcsDisplay,
			(PU8)NULL,
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			IMG_NEW_MESSAGE_NOTIFICATION_MSG,
		#else
			IMG_NEW_MESSAGE_NOTIFICATION_MSG_IN_IDLE,
		#endif	
	
			NULL);

		SetRightSoftkeyFunction(ZMAEE_Idle_Notify_GoBack, KEY_EVENT_UP);
		SetKeyHandler(ZMAEE_Idle_Notify_GoBack, KEY_END, KEY_EVENT_DOWN);
		SetLeftSoftkeyFunction(ZMAEE_Idle_Notify_View, KEY_EVENT_UP);

#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)		
	}
	else	/* keypad is locked */
	{
		ShowCategory154Screen(
			0,
			0,
			g_keylock_context.KeyLockLSKStringID,
			g_keylock_context.KeyLockLSKIconID,
			g_keylock_context.KeyLockRSKStringID,
			g_keylock_context.KeyLockRSKIconID,
			(PU8)sg_zmaee_idle_notify_items[idx].wcsDisplay,
			(PU8)NULL,
			
            #if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
      			IMG_NEW_MESSAGE_NOTIFICATION_MSG,
            #else
       			IMG_NEW_MESSAGE_NOTIFICATION_MSG_IN_IDLE,
            #endif	
			
			NULL);
		ClearKeyHandler(KEY_END, KEY_EVENT_DOWN);
	}
#endif
#endif // __COSMOS_MMI_PACKAGE__	
}

#endif

/**
 * 设置Idle Notify
 * 参数:
 * 	cls_id				class id
 * 	wcsName				app name
 * 	wcsDisplay			display tip
 * 返回:
 * 	E_ZM_AEE_FAILURE	Idle Notify已满
 * 	E_ZM_AEE_BADPARAM	无效参数
 * 	E_ZM_AEE_SUCCESS	设置成功
 */
int ZMAEE_Set_IdleNotify(ZM_AEECLSID cls_id, const unsigned short *wcsName, const unsigned short *wcsDisplay)
{
	int i;

	if((!wcsName) || (!wcsDisplay)) {
		return E_ZM_AEE_BADPARAM;
	}

	for(i = 0; i < AEE_MAX_IDLENOTIFY_COUNT; i++) {
		if(sg_zmaee_idle_notify_items[i].clsAppletId == cls_id)
			break;
	}

	if(i >= AEE_MAX_IDLENOTIFY_COUNT) {
		for(i = 0; i < AEE_MAX_IDLENOTIFY_COUNT; i++) {
			if(sg_zmaee_idle_notify_items[i].bValid == 0) {
				break;
			}
		}
	}

	if(i >= AEE_MAX_IDLENOTIFY_COUNT) {
		return E_ZM_AEE_FAILURE;
	}

	sg_zmaee_idle_notify_items[i].bValid = E_ZM_AEE_TRUE;
	sg_zmaee_idle_notify_items[i].clsAppletId = cls_id;
	memset(sg_zmaee_idle_notify_items[i].wcsName, 0, sizeof(sg_zmaee_idle_notify_items[i].wcsName));
	memset(sg_zmaee_idle_notify_items[i].wcsDisplay, 0, sizeof(sg_zmaee_idle_notify_items[i].wcsDisplay));
	zmaee_wcsncpy((unsigned short*)sg_zmaee_idle_notify_items[i].wcsName, (const unsigned short*)wcsName,
					sizeof(sg_zmaee_idle_notify_items[i].wcsName) / 2 - 1);
	zmaee_wcsncpy((unsigned short*)sg_zmaee_idle_notify_items[i].wcsDisplay, (const unsigned short*)wcsDisplay,
					sizeof(sg_zmaee_idle_notify_items[i].wcsDisplay) / 2 - 1);

#if (ZM_AEE_MTK_SOFTVERN < 1224)
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#ifdef __COSMOS_MMI_PACKAGE__
	{
		extern void ZMAEE_Notify_Ind(void);
		ZMAEE_Notify_Ind();
	}
#endif
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x1224) && defined (__COSMOS_MMI_PACKAGE__)
	zmaee_notificationEvent_create(i);
	//注册消息
	mmi_frm_cb_reg_event(EVT_ID_USB_ENTER_MS_MODE, ZMAEE_IdleNotify_Entry_MS_Hdlr, NULL);
	mmi_frm_cb_reg_event(EVT_ID_USB_EXIT_MS_MODE, ZMAEE_IdleNotify_Exit_MS_Hdlr, NULL);
#endif


	return E_ZM_AEE_SUCCESS;
}


#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
int ZMAEE_Get_IdleNotify_Item(int idx, ZMAEE_IDLENOTIFY_ITEM *item)
{
	if(idx >= 0 && idx < AEE_MAX_IDLENOTIFY_COUNT && item) {
		memcpy(item, &sg_zmaee_idle_notify_items[idx], sizeof(ZMAEE_IDLENOTIFY_ITEM));
		return 0;
	}

	return -1;
}

int ZMAEE_Clear_IdleNotify_Item(int idx)
{
	if(idx >= 0 && idx < AEE_MAX_IDLENOTIFY_COUNT) {
		sg_zmaee_idle_notify_items[idx].bValid = E_ZM_AEE_FALSE;
#if (ZM_AEE_MTK_SOFTVERN >= 0x1224) && defined (__COSMOS_MMI_PACKAGE__)
		zmaee_notificationEvent_close(idx);
#endif
		return 0;
	}

	return -1;
}


#ifdef __COSMOS_MMI_PACKAGE__
mmi_ret ZMAEE_IdleNotify_Exit_MS_Hdlr(mmi_event_struct *evt)
{
	int index;
	for(index = 0;index < AEE_MAX_IDLENOTIFY_COUNT;index++){
		if(sg_zmaee_idle_notify_items[index].bValid)
			zmaee_notificationEvent_create(index);
	}
}	

mmi_ret ZMAEE_IdleNotify_Entry_MS_Hdlr(mmi_event_struct *evt)
{
	int index;
	for(index = 0;index < AEE_MAX_IDLENOTIFY_COUNT;index++){
		if(sg_zmaee_idle_notify_items[index].bValid)
			zmaee_notificationEvent_close(index);
	}
}	
#endif



char* ZMAEE_Get_IdleNotify_WorkDir(void)
{
	extern char* ZMAEE_IShell_GetAppDir(AEE_IShell *po, int app_attr);
	return ZMAEE_IShell_GetAppDir(ZMAEE_GetShell(), ZMAEE_APPATTR_APPLET_APPLICATION);
}

int ZMAEE_Is_IdleNotify_Valid(int idx)
{
	if(idx >= 0 && idx < AEE_MAX_IDLENOTIFY_COUNT) {
		if(sg_zmaee_idle_notify_items[idx].bValid)
			return 1;
	}

	return 0;
}
#endif

/*********************************************************************************
 *USB 事件通知
 *********************************************************************************/
/**
 * @plug_type: 0 - out, 1 - in
 */
void ZMAEE_USB_Notify(int plug_type)
{
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_USBCABLE, (unsigned long)plug_type, 0);
}

/*********************************************************************************
 *Memory Card 事件通知
 *********************************************************************************/
/**
 * @plug_type: 0 - out, 1 - in
 */
void ZMAEE_MemoryCard_Notify(int plug_type)
{
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_MEMORYCARD, (unsigned long)plug_type, 0);
}


/*********************************************************************************
 * 菜单入口函数
 *********************************************************************************/

static void ZMAEE_Create_Wnd(void);

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#ifdef __ZMAEE_APP_THEME__
static
void ZMAEE_IdleHandleKeypadLockProcess(void)
{
	sg_zmaee_mainmenu_keylock_flag = 0;
}

static
void ZMAEE_IdleHandlePoundKeyForKeypadLock(void)
{
	mmi_idle_display();
	mmi_scr_locker_launch();
}
#endif
#endif

void ZMAEE_SuspendWnd(void)
{
	extern unsigned long sg_zmaee_async_key_state;
#ifdef __MMI_TOUCH_SCREEN__	
	void ZMAEE_Restore_PenEvent_Param(void);
	ZMAEE_Restore_PenEvent_Param();
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	if(sg_zmaee_start_screensaver) {
		sg_zmaee_start_screensaver = 0;
		ZMAEE_Exit_Ext();
#ifdef __MMI_FRM_HISTORY__
		DeleteScreenIfPresent(SCR_ZMAEE_MAIN);
#else
		mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN);
#endif
		return;
	}
#endif

	ZMAEE_Platform_HandleEvent(ZMAEE_EV_SUSPEND, 0, 0);
	sg_zmaee_async_key_state = 0;
}


void ZMAEE_RepaintWnd(void)
{
	ZMAEE_Create_Wnd();
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_RESUME, 0, 0);
}

void ZMAEE_KeyPressHandler(void)
{
	U16 key_code, key_type;
	ZMAEE_AppletEvent event;
	ZMAEE_KeyCode key_val;
	extern unsigned long sg_zmaee_async_key_state;

#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		GoBackHistory();
		return;
	}
#else
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		ZMAEE_Exit();
		return;
	}
#endif
#endif
	
    GetkeyInfo(&key_code, &key_type);

	// 挂机键的特殊处理
	if((key_code == KEY_END) && (key_type == KEY_EVENT_DOWN)) {
		int end_key_mask;

		end_key_mask = ZMAEE_IShell_GetEndKeyMask();
		if(end_key_mask != 1) {			
			#ifdef __ZMAEE_APP_DESKTOP__
			{
				extern void ZMAEE_Desktop_Reset_MainMenu_State(void);
				ZMAEE_Desktop_Reset_MainMenu_State();
			}
			#endif
			ZMAEE_Exit_Ext();
#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
	mmi_idle_display();
#else
	DisplayIdleScreen();
#endif
			return;
		}
	}



#ifdef __ZMAEE_APP_KEYPAD_LOCK__
{
	extern int g_zmaee_keylock_flag;
	if(g_zmaee_keylock_flag != 0)
	{
		if(ZMAEE_KEYPAD_TYPE != ZMAEE_HW_KEYPAD_NORMAL)
			return;
	}
}
#endif

	// 按键类型转换
	switch(key_type) {
		case KEY_EVENT_DOWN:
			event = ZMAEE_EV_KEY_PRESS;
			break;
		case KEY_EVENT_UP:
			event = ZMAEE_EV_KEY_RELEASE;
			break;
		case KEY_EVENT_LONG_PRESS:
			event = ZMAEE_EV_KEY_LONGPRESS;
			break;
		case KEY_EVENT_REPEAT:
			event = ZMAEE_EV_KEY_REPEAT;
			break;
		default:
			return;
	}

	// 按键键值转换
	switch(key_code) {
		case KEY_0:
			key_val = ZMAEE_KEY_NUM0;
			break;
		case KEY_1:
			key_val = ZMAEE_KEY_NUM1;
			break;
		case KEY_2:
			key_val = ZMAEE_KEY_NUM2;
			break;
		case KEY_3:
			key_val = ZMAEE_KEY_NUM3;
			break;
		case KEY_4:
			key_val = ZMAEE_KEY_NUM4;
			break;
		case KEY_5:
			key_val = ZMAEE_KEY_NUM5;
			break;
		case KEY_6:
			key_val = ZMAEE_KEY_NUM6;
			break;
		case KEY_7:
			key_val = ZMAEE_KEY_NUM7;
			break;
		case KEY_8:
			key_val = ZMAEE_KEY_NUM8;
			break;
		case KEY_9:
			key_val = ZMAEE_KEY_NUM9;
			break;
		case KEY_LSK:
			key_val = ZMAEE_KEY_SOFTLEFT;
			break;
		case KEY_RSK:
			key_val = ZMAEE_KEY_SOFTRIGHT;
			break;
		case KEY_UP_ARROW:
			key_val = ZMAEE_KEY_UP;
			break;
		case KEY_DOWN_ARROW:
			key_val = ZMAEE_KEY_DOWN;
			break;
		case KEY_LEFT_ARROW:
			key_val = ZMAEE_KEY_LEFT;
			break;
		case KEY_RIGHT_ARROW:
			key_val = ZMAEE_KEY_RIGHT;
			break;
		case KEY_SEND:
			key_val = ZMAEE_KEY_SOFTSEND;
			break;
		case KEY_CLEAR:
			key_val = ZMAEE_KEY_SOFTCLEAR;
			break;
		case KEY_STAR:
			key_val = ZMAEE_KEY_STAR;
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		#ifdef __ZMAEE_APP_THEME__
		#if (defined(__MMI_KEYPAD_LOCK_PATTERN_2__) && !defined(__MMI_DISABLE_KEYPAD_LOCK__))
		{
			extern void IdleHandlePoundKeyForKeypadLock(void);
			extern idle_context_struct g_idle_context;
			if((sg_zmaee_mainmenu_keylock_flag == 1) && (key_type == KEY_EVENT_UP) && (g_idle_context.ToMainMenuScrFromIdleApp == 1)) {
				sg_zmaee_mainmenu_keylock_flag = 0;
				ZMAEE_Exit_Ext();
				IdleHandlePoundKeyForKeypadLock();				
				return;
			}
		}
		#endif
		#endif	// __ZMAEE_APP_THEME__
	#else
		#ifdef __ZMAEE_APP_THEME__
		{
			if(sg_zmaee_mainmenu_keylock_flag == 1) {
				sg_zmaee_mainmenu_keylock_flag = 0;
				ZMAEE_Exit_Ext();
				ZMAEE_IdleHandlePoundKeyForKeypadLock();
			}
		}
		#endif
	#endif
			break;
		case KEY_POUND:
			key_val = ZMAEE_KEY_HASH;
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			#ifdef __ZMAEE_APP_THEME__
			#if !defined(__MMI_DISABLE_KEYPAD_LOCK__) && defined(__MMI_KEYPAD_LOCK_PATTERN_1__) && defined(__MMI_OP11_HOMESCREEN__)
			{
				extern void IdleHandlePoundKeyForKeypadLock(void);
				extern idle_context_struct g_idle_context;
			    if((sg_zmaee_mainmenu_keylock_flag == 1) && (key_type == KEY_EVENT_UP) && (g_idle_context.RskPressedFromIdleApp == 1 && mmi_hs_is_enable())) {
					sg_zmaee_mainmenu_keylock_flag = 0;
					ZMAEE_Exit_Ext();
					IdleHandlePoundKeyForKeypadLock();					
					return;
			    }
			}
			#endif
			#endif	// __ZMAEE_APP_THEME__
	#endif
			break;
		case KEY_VOL_UP:
			key_val = ZMAEE_KEY_VOLUP;
			break;
		case KEY_VOL_DOWN:
			key_val = ZMAEE_KEY_VOLDOWN;
			break;
		case KEY_QUICK_ACS:
			key_val = ZMAEE_KEY_QUICKASK;
			break;
		case KEY_ENTER:
			key_val = ZMAEE_KEY_SOFTOK;
			break;
		case KEY_END:
			key_val = ZMAEE_KEY_SOFTEND;
			break;
		default:
			ZMAEE_DebugPrint("ZMAEE_KeyPressHandler: key_code = %d", key_code);
			return;
	}

	if(event == ZMAEE_EV_KEY_PRESS) {
		sg_zmaee_async_key_state |= (1 << key_val);
	} else if(event == ZMAEE_EV_KEY_RELEASE) {
		sg_zmaee_async_key_state &= ~(1 << key_val);
	}

	ZMAEE_Platform_HandleEvent(event, (unsigned long)key_val, 0);
}
	
#ifdef __MMI_TOUCH_SCREEN__
void ZMAEE_PenDownHandler(mmi_pen_point_struct pos)
{
	extern void ZMAEE_Start_IconBar_App(int x,int y);
	ZMAEE_Start_IconBar_App(pos.x, pos.y);

#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		GoBackHistory();
		return;
	}
#else
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		ZMAEE_Exit();
		return;
	}
#endif
#endif
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_DOWN, (unsigned long)pos.x, (unsigned long)pos.y);
}

void ZMAEE_PenUpHandler(mmi_pen_point_struct pos)
{
#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		GoBackHistory();
		return;
	}
#else
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		ZMAEE_Exit();
		return;
	}
#endif
#endif

	ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_UP, (unsigned long)pos.x, (unsigned long)pos.y);
}

void ZMAEE_PenMoveHandler(mmi_pen_point_struct pos)
{
	if(pos.y > UI_device_height)
		return;
		
#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		GoBackHistory();
		return;
	}
#else
	if(sg_zmaee_start_screensaver == 1) {
		sg_zmaee_start_screensaver = 0;
		ZMAEE_Exit();
		return;
	}
#endif
#endif

	ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_MOVE, (unsigned long)pos.x, (unsigned long)pos.y);
}
#endif	//__MMI_TOUCH_SCREEN__

#ifdef __MMI_TOUCH_SCREEN__
void ZMAEE_Restore_PenEvent_Param(void)
{
	mmi_pen_config_sampling_period(sg_zmaee_penevent_param.sample_low, sg_zmaee_penevent_param.sample_high);
	mmi_pen_config_move_offset(sg_zmaee_penevent_param.offset, sg_zmaee_penevent_param.stroke_offset,
								sg_zmaee_penevent_param.long_tap_offset, sg_zmaee_penevent_param.stroke_long_tap_offset);
}
#endif

void ZMAEE_SetAll_InputHandler()
{
	extern const U16 PresentAllKeys[];

	entry_full_screen();

	// free layers
	{
		extern void ZMAEE_LazyFreeLayers(void);
		ZMAEE_LazyFreeLayers();
	}

	// 
	gdi_layer_multi_layer_enable();

	// clip
	gdi_layer_reset_clip();
	gdi_layer_reset_text_clip();

	// 按键
	ClearAllKeyHandler();
	clear_category_screen_key_handlers();

	SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_DOWN);
	SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_UP);
	SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_LONG_PRESS);
	SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_REPEAT);
	mmi_frm_set_default_power_onoff_key();
	
#ifdef __MMI_TOUCH_SCREEN__
#ifndef WIN32	   //WLJ
	{
		extern TouchPanelDataStruct TP;
	
		sg_zmaee_penevent_param.sample_low = TP.low_sample_period;
		sg_zmaee_penevent_param.sample_high = TP.high_sample_period;
		sg_zmaee_penevent_param.offset = TP.pen_offset;
		sg_zmaee_penevent_param.stroke_offset = TP.storke_offset;
		sg_zmaee_penevent_param.long_tap_offset = TP.longtap_pen_offset;
		sg_zmaee_penevent_param.stroke_long_tap_offset = TP.longtap_stroke_offset;
	
		mmi_pen_config_sampling_period(2, 2);
		mmi_pen_config_move_offset(2, 2, 4, 4);
	
	
		wgui_register_pen_down_handler(ZMAEE_PenDownHandler);
		wgui_register_pen_up_handler(ZMAEE_PenUpHandler);
		wgui_register_pen_move_handler(ZMAEE_PenMoveHandler);
	}
#endif
#endif
}

static
void ZMAEE_Create_Wnd(void)
{
#ifdef __MMI_FRM_HISTORY__
	if(sg_zmaee_wnd_screen_id == 0)
		sg_zmaee_wnd_screen_id = SCR_ZMAEE_MAIN;

	if(sg_zmaee_reentry_func == NULL)
		EntryNewScreen(sg_zmaee_wnd_screen_id, ZMAEE_SuspendWnd, ZMAEE_RepaintWnd, NULL);
	else
		EntryNewScreen(sg_zmaee_wnd_screen_id, ZMAEE_SuspendWnd, sg_zmaee_reentry_func, NULL);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN, ZMAEE_SuspendWnd, ZMAEE_RepaintWnd, MMI_FRM_FULL_SCRN);
#endif
	
	ZMAEE_SetAll_InputHandler();
}

void ZMAEE_AppletEntry_CreateWorkDir(const char* work_dir)
{
	char full_path[AEE_MAX_FILE_NAME] = {0};
	AEE_IFileMgr *pFileMgr = NULL;
	AEE_IShell *pShell = ZMAEE_GetShell();

	ZMAEE_IShell_CreateInstance(pShell, ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
	if(!pFileMgr)
		return;

	full_path[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);
	strncpy(&full_path[1], work_dir, sizeof(full_path) - 2);

	if(ZMAEE_IFileMgr_Test(pFileMgr, full_path) != E_ZM_AEE_SUCCESS) {
		ZMAEE_IFileMgr_MkDir(pFileMgr, full_path);
	}

	full_path[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
	if(ZMAEE_IFileMgr_Test(pFileMgr, full_path) != E_ZM_AEE_SUCCESS) {
		ZMAEE_IFileMgr_MkDir(pFileMgr, full_path);
	}
}

int ZMAEE_SimSearchingComplete(void)
{
#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	if(!srv_bootup_is_completed()) 
#else
	if(mmi_bootup_is_network_searching_complete() != MMI_TRUE
	#ifdef __MMI_DUAL_SIM_MASTER__	
		|| mmi_bootup_is_sim2_network_searching_complete() != MMI_TRUE
	#endif
	)
#endif
	{
		return E_ZM_AEE_FALSE;
	}

	return E_ZM_AEE_TRUE;
}

#ifdef __ZMAEE_REDUCE_DOWNLOAD__
typedef struct {
	unsigned short app_name[16];
	ZMAEE_DOWNLOAD_PARAM down_param;
} ZMAEE_REDUCE_DOWNLOAD_CNTX;

static ZMAEE_REDUCE_DOWNLOAD_CNTX sg_zmaee_reduce_download_cntx = {0};

extern int zmaee_down_check(void);
extern void* ZMAEE_MallocScreenMem(int size);
extern void ZMAEE_FreeScreenMem(void * pScreenMem);

char* ZMAEE_Down_GetTipStr(int type)
{
	switch(type) {
		case 0:
			return GetString(STR_ZMAEE_DOWN_INIT);

		case 1:
			return GetString(STR_ZMAEE_DOWN_NETERR);

		default:
			return NULL;
	}
}

void ZMAEE_Download_DllInfe_Resp(int result)
{
	if(result == 0) {	
		int ret;

		zmaee_destroyDialog();
		ZMAEE_AppletEntry(ZMAEE_DOWNLOAD_CLSID, ZMAEE_ENTRY_WORKDIR, (void*)&sg_zmaee_reduce_download_cntx.down_param, sizeof(sg_zmaee_reduce_download_cntx.down_param),
						(char*)sg_zmaee_reduce_download_cntx.app_name,
						NULL, 0, 0, 0, NULL, 0);
	} else {
		DisplayPopup(GetString(STR_GLOBAL_ERROR), IMG_GLOBAL_ERROR, 0, 2000, 0);
		zmaee_destroyDialog();
	}

	zmaee_down_cancel();	
}

void* zmaee_down_malloc(int size)
{
	return med_alloc_ext_mem(size);
}

void zmaee_down_free(void* ptr)
{
	med_free_ext_mem(&ptr);
}

void zmaee_down_query(void) 
{
	extern int zmaee_down_start(void (*pfn)(int), AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
	extern void* ZMAEE_Mem_MallocMedMem(unsigned int size);
	extern void ZMAEE_Mem_FreeMedMem(void *ptr);
	int ret;

	zmaee_down_cancel();
	ret = zmaee_down_start(ZMAEE_Download_DllInfe_Resp, zmaee_down_malloc, zmaee_down_free);
	switch(ret) {
		case -2:
			DisplayPopup(GetString(STR_ZMAEE_TCARD_ERROR), IMG_GLOBAL_ERROR, 0, 2000, 0);
			break;
		default:
			if(ZMAEE_GetValidSimCount() < 1){
				DisplayPopup(GetString(STR_ZMAEE_SIM_ERROR), IMG_GLOBAL_ERROR, 0, 2000, 0);
			}
			else if(ret != 1){
				DisplayPopup(GetString(STR_ZMAEE_INIT_ERROR), IMG_GLOBAL_ERROR, 0, 2000, 0);
			}
	}
}

#endif

int zmaee_sim_id = -1;
void ZMAEE_AppletEntry(ZM_AEECLSID cls_id, const char* work_dir, void* param, int param_len, char *appName, char *appIcon, unsigned int nIconLen, int bkg, int app_mask, void (*reentry_func)(void), unsigned short scr_id)
{
	extern void ZMAEE_ITAPI_InitSIMInfo(void);
	extern void ZMAEE_ITAPI_InitIMEI(void);
	extern int 	ZMAEE_GetValidSimCount(void);
	extern int 	ZMAEE_InitIMSI_Done(void);
	extern int 	ZMAEE_InitSCADDR_Done(void);
	extern void ZMAEE_PowerOnInit(void);
	extern int	ZMAEE_InitIMEI_Done(void);
	extern void ZMAEE_AppletEntry_OnEnter(void);
	extern void ZMAEE_ITAPI_InitSCADDR(void);
	extern void ZMAEE_Init();

	AEE_IShell *shell_ptr;
	AEE_IFileMgr *pFileMgr = NULL;
	int ret;
	unsigned short err_str_id = STR_ZMAEE_LOAD_ERROR;
	int nDisplayPopup = E_ZM_AEE_TRUE;

	ZMAEE_DebugSwitch(1);
	ZMAEE_Init();

	//和外部数据连接选择SIM卡同步
	kal_prompt_trace(LAST_MOD_ID,"zmaee_sim_id = %d",zmaee_sim_id);
	if(zmaee_sim_id != -1)
	{
		AEE_ITAPI *pTapiMgr = NULL;
		
		ZMAEE_IShell_CreateInstance(ZMAEE_GetShell(), ZM_AEE_CLSID_TAPI, (void**)&pTapiMgr);
		if(pTapiMgr)
		{	
			
			if(zmaee_sim_id == 1){
				ZMAEE_ITAPI_SetActiveSimCard(pTapiMgr,0);
			}
			else if(zmaee_sim_id ==2){
				ZMAEE_ITAPI_SetActiveSimCard(pTapiMgr,1);
			}
			ZMAEE_ITAPI_Release(pTapiMgr);
		}
	}
	if((!work_dir) || (!appName)) {
		err_str_id = STR_ZMAEE_LOAD_ERROR;
		goto zmaee_main_exception;
	}

	sg_zmaee_reentry_func = reentry_func;
	sg_zmaee_wnd_screen_id = scr_id;
	if(!bkg) {
		ZMAEE_Create_Wnd();
		#ifdef __COSMOS_MMI_PACKAGE__
		{
			int ZMAEE_DWallPaper_IsStart(void);
			void ZMAEE_DWallPaper_Exit(void);
			
			if(ZMAEE_DWallPaper_IsStart())
				ZMAEE_DWallPaper_Exit();
		}
		#endif
	}

	ZMAEE_AppletEntry_OnEnter();
	ZMAEE_AppletEntry_CreateWorkDir(work_dir);

	// USB模式的判断
#ifdef __USB_IN_NORMAL_MODE__
#ifndef WIN32
	{
	#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
		extern pBOOL mmi_usb_is_in_mass_storage_mode(void);
	#else
		extern MMI_BOOL srv_usb_is_in_mass_storage_mode(void);
	#endif
		if(!(ZMAEE_IsAppletInROM(cls_id) && cls_id >= 2000)) {
		#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
			if (mmi_usb_is_in_mass_storage_mode() || sg_zmaee_restart_theme == 1)
		#else
			if(srv_usb_is_in_mass_storage_mode() || sg_zmaee_restart_theme == 1)
		#endif
			{
				err_str_id = STR_ZMAEE_USB_ERROR;
				goto zmaee_main_exception;
			}
		}
	}
#endif
#endif

#if (defined __ZMAEE_APP_THEME__) || (defined __ZMAEE_APP_KEYPAD_LOCK__) || (defined __ZMAEE_APP_DWALLPAPER__)
	{
		if((cls_id == ZMAEE_GetThemeAppId(0)) || (cls_id == ZMAEE_GetThemeAppId(1)) || (cls_id == ZMAEE_GetThemeAppId(2))) {
			nDisplayPopup = E_ZM_AEE_FALSE;
			goto zmaee_startapp_code;
		}
	}
#endif

#ifdef ZMAEE_CANNOT_ENTRY_WITHOUT_TCARD
		ZMAEE_IShell_CreateInstance(shell_ptr, ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
		if(pFileMgr) {
			char driver = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
	
			ZMAEE_IFileMgr_Release(pFileMgr);
			if(driver == 0) {
				err_str_id = STR_ZMAEE_TCARD_ERROR;
				goto zmaee_main_exception;
			}
		}
#endif

	// 搜网判断
	if(!ZMAEE_SimSearchingComplete()) {
		err_str_id = STR_ZMAEE_INIT_ERROR;
		ZMAEE_DebugPrint("ZMAEE_AppletEntry: Searching");
		goto zmaee_main_exception;
	}

	// 有效SIM卡判断	
	ZMAEE_ITAPI_InitSIMInfo();
	if(ZMAEE_GetValidSimCount() < 1) {
		err_str_id = STR_ZMAEE_SIM_ERROR;
		goto zmaee_main_exception;
	}

	// 是否获取到IMSI号
	if(ZMAEE_InitIMSI_Done() != E_ZM_AEE_TRUE) {
		pwron_init_times = 1;
		ZMAEE_PowerOnInit();
		err_str_id = STR_ZMAEE_INIT_ERROR;
		ZMAEE_DebugPrint("ZMAEE_AppletEntry: Imsi error");
		goto zmaee_main_exception;
	}

	// 获取短信中心号码
	if(ZMAEE_InitSCADDR_Done() != E_ZM_AEE_TRUE) {
		pwron_init_times = 1;
		ZMAEE_PowerOnInit();
		// XXX: 短消息中心获取不到，也允许进入平台
	}
	ZMAEE_ITAPI_InitSCADDR();

	// 获取IMEI号
	if(ZMAEE_InitIMEI_Done() != E_ZM_AEE_TRUE) {
		ZMAEE_ITAPI_InitIMEI();
	}

zmaee_startapp_code:	
	shell_ptr = ZMAEE_GetShell();
	ZMAEE_DebugPrint("ZMAEE_AppletEntry: clsid = %d, param = 0x%x, len = %d", cls_id, param, param_len);
	ret = ZMAEE_IShell_SetWorkDir(shell_ptr, work_dir, app_mask);
	ZMAEE_DebugPrint("ZMAEE_AppletEntry: set work dir = %d, %s", ret, ZMAEE_IShell_GetWorkDir(shell_ptr));
	if(ret != E_ZM_AEE_TRUE)
	{
		err_str_id = STR_ZMAEE_LOAD_ERROR;
		goto zmaee_main_exception;
	}

	ret = ZMAEE_IShell_StartApplet(shell_ptr, cls_id, param, param_len);
	ZMAEE_DebugPrint("ZMAEE_AppletEntry: start ret = %d", ret);
	if(ret == E_ZM_AEE_CLASSNOTSUPPORT) {
		extern int ZMAEE_IShell_StartAppletROM(AEE_IShell * po, ZM_AEECLSID clsId, void *pUser, int len);
		ZMAEE_DOWNLOAD_PARAM download_param = {0};

		// <<< 下载器启动应用支持启动参数
		zmaee_memset(sg_zmaee_download_params_content, 0, sizeof(sg_zmaee_download_params_content));
		zmaee_memcpy(sg_zmaee_download_params_content, param, param_len);
		sg_zmaee_download_params_length = param_len;
		// 下载器启动应用支持启动参数>>>

		download_param.clsId = cls_id;
		download_param.productName = (unsigned short*)appName;
		download_param.pImage = (unsigned char*)appIcon;
		download_param.nImageLen = nIconLen;
		// <<< 下载器启动应用支持启动参数
		download_param.param = sg_zmaee_download_params_content;
		download_param.len = sg_zmaee_download_params_length;
		// 下载器启动应用支持启动参数>>>

		ret = ZMAEE_IShell_StartApplet(shell_ptr, ZMAEE_DOWNLOAD_CLSID, (void*)&download_param, sizeof(download_param));
		ZMAEE_DebugPrint("ZMAEE_AppletEntry: start download app ret = %d", ret);
		if(ret == E_ZM_AEE_CLASSNOTSUPPORT) {
#ifndef __ZMAEE_REDUCE_DOWNLOAD__
			ret = ZMAEE_IShell_StartAppletROM(shell_ptr, ZMAEE_DOWNLOAD_CLSID, (void*)&download_param, sizeof(download_param));
			if(ret != E_ZM_AEE_SUCCESS) {
				err_str_id = STR_ZMAEE_LOAD_ERROR;
				goto zmaee_main_exception;
			}
#else
			memcpy(sg_zmaee_reduce_download_cntx.app_name, appName, zmaee_wcslen((const unsigned short*)appName) * 2);
			memcpy(&sg_zmaee_reduce_download_cntx.down_param, &download_param, sizeof(download_param));
			sg_zmaee_reduce_download_cntx.down_param.productName = sg_zmaee_reduce_download_cntx.app_name;

			gui_start_timer(100, zmaee_down_query);
			
			nDisplayPopup = E_ZM_AEE_FALSE;
			goto zmaee_main_exception;
#endif
		} else if(ret != E_ZM_AEE_SUCCESS) {
			err_str_id = STR_ZMAEE_LOAD_ERROR;
			goto zmaee_main_exception;
		}
	} else if(ret != E_ZM_AEE_SUCCESS) {
		err_str_id = STR_ZMAEE_LOAD_ERROR;
		goto zmaee_main_exception;
	}

	return;

zmaee_main_exception:
	{
		{
			extern void ZMAEE_AppletExit_OnExit(void);
			ZMAEE_AppletExit_OnExit();
		}
	
		GoBackHistory();
		if(nDisplayPopup)
			DisplayPopup((U8*)GetString(err_str_id), IMG_GLOBAL_ERROR, NULL, 1000, 0);
		return;
	}

}
#if 0
void ZMAEE_QQ(void)
{
	ZMAEE_AppletEntry(1998, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_QQ),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);

}

void ZMAEE_QQ_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_QQ, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_QQ, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_QQ, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_EBook(void)
{
	ZMAEE_AppletEntry(1001, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_EBOOK),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
}


void ZMAEE_EBook_Highlight(void)
{
		ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
		ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);
	
		SetKeyHandler(ZMAEE_EBook, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
		SetKeyHandler(ZMAEE_EBook, KEY_ENTER, KEY_EVENT_UP);
#endif
		SetLeftSoftkeyFunction(ZMAEE_EBook, KEY_EVENT_UP);
		
		SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
		SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_OPER(void)
{
	ZMAEE_AppletEntry(1233, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_OPER),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);

}

void ZMAEE_OPER_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_OPER, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_OPER, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_OPER, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

#endif

void ZMAEE_OPER(void)
{
#ifdef __ZMAEE_APP__//andy.xin
	 {
		
		extern int ZMAEE_QQ_IsRunning(void);
		extern void ZMAEEShowExitInfo(void);
		if(ZMAEE_QQ_IsRunning())
	   {
		   ZMAEEShowExitInfo();
		   return;
	   }
		
	}
#endif

	ZMAEE_AppletEntry(1233, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_OPER),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);

}

void ZMAEE_OPER_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_OPER, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_OPER, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_OPER, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_Main(void)
{
#if !defined (__COSMOS_MMI_PACKAGE__) || ((ZM_AEE_MTK_SOFTVERN >= 0x1224) && defined (__COSMOS_MMI_PACKAGE__) && defined (MT6250))
	ZMAEE_AppletEntry(ZM_AEE_APPLET_CLSID_BASE + 1, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_MAIN),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
#else
	ZMAEE_AppletEntry(5, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_MAIN),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
#endif
}

void ZMAEE_Main_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_Main, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_Main, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_Main, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}


void ZMAEE_QQ(void)
{

	ZMAEE_AppletEntry(1999, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_QQ),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
}

void ZMAEE_QQ_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_QQ, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_QQ, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_QQ, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_SJDW(void)
{

	ZMAEE_AppletEntry(1276, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_SJDW),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
}

void ZMAEE_SJDW_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_SJDW, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_SJDW, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_SJDW, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}

void ZMAEE_HMGS(void)
{

#ifdef __ZMAEE_APP__//andy.xin
 {
	
	extern int ZMAEE_QQ_IsRunning(void);
	extern void ZMAEEShowExitInfo(void);
	if(ZMAEE_QQ_IsRunning())
   {
	   ZMAEEShowExitInfo();
	   return;
   }
	
}
#endif

	ZMAEE_AppletEntry(1029, ZMAEE_ENTRY_WORKDIR, NULL, 0, (char*)GetString(STR_ZMAEE_HMGS),
		NULL, 0, 0, (ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME), NULL, 0);
}

void ZMAEE_HMGS_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_HMGS, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if 1//((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_HMGS, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_HMGS, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}


#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0 && ZM_AEE_MTK_SOFTVERN < 0x11B0) || defined (__COSMOS_MMI_PACKAGE__)
void ZMAEE_Sms_Callback(srv_sms_callback_struct* callback_data)
{
	
}

MMI_BOOL ZMAEE_Sms_Interrupt_Hdlr(srv_sms_event_struct* event_data)
{
	srv_sms_event_new_sms_struct *new_sms_data = (srv_sms_event_new_sms_struct*)event_data->event_info;
	
	if(ZMAEE_Sms_Check(new_sms_data) == 1) {
		srv_sms_delete_msg_bg(new_sms_data->msg_id, ZMAEE_Sms_Callback, NULL);
		return MMI_TRUE;
	} else {
		if(sg_zmaee_sms_silent) {
			srv_sms_unhide_msg(new_sms_data->msg_id);
			sg_zmaee_sms_silent = 0;
			return MMI_TRUE;
		}
		
		return MMI_FALSE;
	}
}
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#ifdef __MED_IN_ASM__
void ZMAEE_ASM_MemStop(void)
{
	ZMAEE_Exit();
	applib_mem_ap_notify_stop_finished(VAPP_ZMAEEPLAT, KAL_TRUE);
}
#endif
#endif

#ifdef __ZMAEE_APP_DESKTOP__
S32 mmi_zmaee_sleep_inout_callback_handler(mmi_event_struct* param)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		if(GetActiveScreenId() == IDLE_SCREEN_ID)
#else
		if(GetActiveScreenId() == SCR_ID_IDLE_MAIN)
#endif
		{
			switch(param->evt_id)
			{
			case EVT_ID_GPIO_LCD_SLEEP_IN:
				AEE_Desktop_HandleEvent(ZMAEE_EV_SUSPEND, 0, 0);
				break;
			case EVT_ID_GPIO_LCD_SLEEP_OUT:
				AEE_Desktop_HandleEvent(ZMAEE_EV_RESUME, 0, 0);
				break;
			}
		}
		return MMI_RET_OK;

}
#endif

void ZMAEE_PowerOnInit(void)
{
	extern void ZMAEE_ITAPI_InitIMSI(void);
	extern void ZMAEE_ITAPI_InitSCADDR(void);
	extern void ZMAEE_ITAPI_InitIMEI(void);
	extern void ZMAEE_ITAPI_InitSIMInfo(void);
	extern int	ZMAEE_InitIMEI_Done(void);
	extern int	ZMAEE_GetValidSimCount(void);
	extern int	ZMAEE_InitIMSI_Done(void);
	extern int	ZMAEE_InitSCADDR_Done(void);


	StopTimer(ZMAEE_INIT_TIMER);

	if(pwron_init_times >= ZMAEE_POWERONINIT_RETRYTIMES)
		return;
	
	if(pwron_init_times == 0) {
		StartTimer(ZMAEE_INIT_TIMER, 15000, ZMAEE_PowerOnInit);
		pwron_init_times++;
	#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0 && ZM_AEE_MTK_SOFTVERN < 0x11B0)) || defined (__COSMOS_MMI_PACKAGE__)
		srv_sms_set_interrupt_event_handler(EVT_ID_SRV_SMS_NEW_MSG, ZMAEE_Sms_Interrupt_Hdlr, NULL);
		#ifdef __MED_IN_ASM__
		applib_mem_ap_register(VAPP_ZMAEEPLAT, STR_ZMAEE_MAIN, NULL, ZMAEE_ASM_MemStop);
		#endif
	#endif	
	
	#ifdef __ZMAEE_APP_DESKTOP__
		mmi_frm_cb_reg_event (EVT_ID_GPIO_LCD_SLEEP_IN, mmi_zmaee_sleep_inout_callback_handler, NULL);
	    mmi_frm_cb_reg_event (EVT_ID_GPIO_LCD_SLEEP_OUT, mmi_zmaee_sleep_inout_callback_handler, NULL);
	#endif
		return;
	}

	if(ZMAEE_SimSearchingComplete())
	{
		int b_init_retry = E_ZM_AEE_FALSE;
	
		// 有效卡判断
		ZMAEE_ITAPI_InitSIMInfo();
		if(ZMAEE_GetValidSimCount() < 1) {
			b_init_retry = E_ZM_AEE_TRUE;
		}

		if(ZMAEE_InitIMEI_Done() != E_ZM_AEE_TRUE) {
			ZMAEE_ITAPI_InitIMEI();
		}

		if(ZMAEE_InitIMSI_Done() != E_ZM_AEE_TRUE) {
			ZMAEE_ITAPI_InitIMSI();
			b_init_retry = E_ZM_AEE_TRUE;
		}

		if(ZMAEE_InitSCADDR_Done() != E_ZM_AEE_TRUE) {
			ZMAEE_ITAPI_InitSCADDR();
			b_init_retry = E_ZM_AEE_TRUE;
		}

		if(b_init_retry == E_ZM_AEE_FALSE)
			return;
	}

	StartTimer(ZMAEE_INIT_TIMER, 5000, ZMAEE_PowerOnInit);
	pwron_init_times++;
}

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
int g_zmaee_keylock_flag = 0;
int g_zmaee_keylock_entry = 0;
int g_zmaee_keylock_unlocking = 0;
int g_zmaee_keylock_status = 0;//WLJ
int  zmaee_keylock_screen_classid(void)
{
	#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
		extern pBOOL mmi_usb_is_in_mass_storage_mode(void);
	#else
	#ifndef WIN32
		extern MMI_BOOL srv_usb_is_in_mass_storage_mode(void); //WLJ
	#endif
	#endif
	AEE_IShell* shell_ptr;
	ZM_AEECLSID clsId;
	int ret = 0;
	char work_dir[AEE_MAX_FILE_NAME] = {0};
	unsigned int applet_mask;

#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
	if(mmi_usb_is_in_mass_storage_mode())
#else 
	#ifndef WIN32
	if(srv_usb_is_in_mass_storage_mode()) //WLJ
	#endif
#endif
		return 0;

	if(sg_zmaee_restart_theme == 1)
		return 0;

	clsId = ZMAEE_GetThemeAppId(0);
	if(clsId == 0 || ZMAEE_IsAppletInROM(clsId))
		return clsId;
	
	shell_ptr = ZMAEE_GetShell();
	strncpy(work_dir, ZMAEE_IShell_GetWorkDir(shell_ptr), sizeof(work_dir) - 1);
	applet_mask = ZMAEE_IShell_GetAppletMask(shell_ptr);
	if(!ZMAEE_IShell_SetWorkDir(shell_ptr, ZMAEE_ENTRY_THEMEDIR, 0))
		return 0;
	
	if(ZMAEE_IShell_ValidateApplet(shell_ptr, clsId)) {
		if(ZMAEE_IShell_CanStartApplet(shell_ptr, clsId)) {
			ret = clsId;
		}
	}

	ZMAEE_IShell_SetWorkDir(shell_ptr, work_dir, applet_mask);
	return ret;
}

int zmaee_keylock_unlock_handler(int nHandle, void* pUser)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern void HandlePoundKeyForUnlock(void);
#endif

	g_zmaee_keylock_unlocking = 0;

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0) && !defined (__COSMOS_MMI_PACKAGE__)
	{
		extern int zmaee_keylock_id;
		if(zmaee_keylock_id)
			zmaee_keylock_id = 0;
	}
#endif

	g_zmaee_keylock_flag = 0;
	g_zmaee_keylock_entry = 0;
	ZMAEE_IShell_SetEndKeyMask(ZMAEE_GetShell(), 0);
	ZMAEE_Exit_Ext();
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	HandlePoundKeyForUnlock();
#elif (ZM_AEE_MTK_SOFTVERN < 0x11A0)
	DisplayIdleScreen();
#else
	GoBackHistory();
#endif
	
	if(g_zmaee_keylock_status == 1){
		srv_backlight_turn_on(SRV_BACKLIGHT_SHORT_TIME);
		mmi_usb_enter_normal_group();
		mmi_usb_entry_usb_detect_query_scrn();
		g_zmaee_keylock_status = 0;
	}

	return 0;
}

void zmaee_keylock_unlock(void)
{
	if(g_zmaee_keylock_flag) {
		if(!g_zmaee_keylock_unlocking) {
			g_zmaee_keylock_unlocking = 1;
			ZMAEE_StartPeriodHandler(0, NULL, zmaee_keylock_unlock_handler);
		}
	}
}

void zmaee_keylock_forceexit(void)
{

	if(g_zmaee_keylock_flag)
	{
		ZMAEE_IShell_SetEndKeyMask(ZMAEE_GetShell(), 0);
		ZMAEE_Exit_Ext();
		g_zmaee_keylock_flag = 0;
		g_zmaee_keylock_entry = 0;

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0) && !defined (__COSMOS_MMI_PACKAGE__)
		{
			extern int zmaee_keylock_id;
			if(zmaee_keylock_id)
				zmaee_keylock_id = 0;
		}
#endif
	}
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)

#ifdef __COSMOS_MMI_PACKAGE__
int zmaee_keylock_entry_system_app_callback(int nHandle, void* pUser)
{
	void (*system_app)(void*, unsigned int) = (void (*)(void*, unsigned int))pUser;

	zmaee_keylock_forceexit();
	system_app(NULL, 0);
	ZMAEE_DestroyWnd();

	return 0;
}


void zmaee_keylock_entry_system_app(void (*func)(void*, unsigned int))
{
	ZMAEE_StartPeriodHandler(0, (void*)func, zmaee_keylock_entry_system_app_callback);
}
#endif

void zmaee_keylock_message_entry(void)
{
#ifdef __COSMOS_MMI_PACKAGE__	
	extern MMI_ID vapp_msg_launch(void *param, U32 param_size);
	zmaee_keylock_entry_system_app(vapp_msg_launch);	
#else
	extern void mmi_um_entry_main_message_menu(void);
	mmi_um_entry_main_message_menu();
#endif
}

void zmaee_keylock_phb_entry(void)
{
#ifdef __COSMOS_MMI_PACKAGE__
	extern MMI_ID vapp_contact_launch(void *param, U32 param_size);
	zmaee_keylock_entry_system_app(vapp_contact_launch);
#else
	extern void mmi_phb_entry_main_menu(void);
	mmi_phb_entry_main_menu();
#endif
}

void zmaee_keylock_calllog_entry(void)
{
#ifdef __COSMOS_MMI_PACKAGE__
	extern MMI_ID vapp_contact_launch(void *param, U32 param_size);
	zmaee_keylock_entry_system_app(vapp_contact_launch);
#else
	extern void mmi_clog_sendkey_launch(void);
	mmi_clog_sendkey_launch();
#endif
}
#endif

void zmaee_keylock_screen_entry(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern void EntryScrMessagesMenuList(void);
	extern void mmi_phb_entry_main_menu(void);
#ifdef __MMI_DUAL_SIM_MASTER__
	extern void MTPNP_PFAL_ReadDualMixedCallLog(void);
#else
	extern void CHISTGetCallLogBySENDKey(void);
#endif
#else
	extern void mmi_um_entry_main_message_menu(void);
	extern void mmi_phb_entry_main_menu(void);
	extern void mmi_clog_sendkey_launch(void);
#endif

	ZM_AEECLSID cldId = ZMAEE_GetThemeAppId(0);
	ZMAEE_KEYLOCK_PARAM keylock_param = {zmaee_keylock_unlock, 
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		EntryScrMessagesMenuList, 
		mmi_phb_entry_main_menu,
	#ifdef __MMI_DUAL_SIM_MASTER__
		MTPNP_PFAL_ReadDualMixedCallLog
	#else
		CHISTGetCallLogBySENDKey
	#endif
#elif (ZM_AEE_MTK_SOFTVERN < 0x11A0)		
		mmi_um_entry_main_message_menu,
		mmi_phb_entry_main_menu,
		mmi_clog_sendkey_launch
#else
		zmaee_keylock_message_entry,
		zmaee_keylock_phb_entry,
		zmaee_keylock_calllog_entry
#endif
		};
	unsigned short wcsName[] = {0x9501, 0x5C4F, 0x0000};

	#ifndef __MMI_KEYPADLOCK_WITH_KEYTONE__
        mmi_frm_kbd_set_tone_state(MMI_KEY_TONE_DISABLED);
	#endif
	ZMAEE_IShell_SetEndKeyMask(ZMAEE_GetShell(), 1);	
	if(g_zmaee_keylock_flag == 0) {
		ZMAEE_AppletEntry(cldId, ZMAEE_ENTRY_THEMEDIR,  (void*)&keylock_param, sizeof(keylock_param), (char*)wcsName, NULL, 0, 0, 0xFFFFFFFF, NULL, 0);
		g_zmaee_keylock_flag = 1;
		g_zmaee_keylock_entry = 1;
	}
}
#endif//__ZMAEE_APP_KEYPAD_LOCK__

/* ------------------------------------------------------------------------- */

/* ------------------------------------------------------------------------- */
#include "zlib.h"

typedef struct {
	z_stream 		stream;
	AEE_PFN_MALLOC 	pMalloc;
	AEE_PFN_FREE	pFree;
}ZMAEE_ZSTREAM;

static void*
ZMAEE_ZStream_Alloc(void* t, unsigned int a, unsigned int b)
{
	ZMAEE_ZSTREAM *zstream = (ZMAEE_ZSTREAM*)t;
	return zstream->pMalloc(a * b);
}

static void
ZMAEE_ZStream_Free(void* t, void* p) 
{	
	ZMAEE_ZSTREAM *zstream = (ZMAEE_ZSTREAM*)t;
	zstream->pFree(p);
}


void* 
ZMAEE_ZStreamCreate(int gzip, 
					AEE_PFN_MALLOC pMalloc, 
					AEE_PFN_FREE pFree)
{
	ZMAEE_ZSTREAM* stream;
	int r;
	if (pMalloc == NULL || pFree == NULL)
		return NULL;
	
	stream = pMalloc(sizeof(ZMAEE_ZSTREAM));
	if (stream == NULL)
		return NULL;
	stream->pMalloc = pMalloc;
	stream->pFree = pFree;

//	stream->heap_ptr = (unsigned char*)stream->heap;
//	zmaee_memset(stream->items, 0, sizeof(stream->items));
	((z_stream*)stream)->opaque = (void*)stream;
	((z_stream*)stream)->zalloc = ZMAEE_ZStream_Alloc;
	((z_stream*)stream)->zfree = ZMAEE_ZStream_Free;

	r = zmaee_inflateInit2_((z_stream*)stream, 
		gzip ? -MAX_WBITS : MAX_WBITS, ZLIB_VERSION, sizeof(z_stream));
	if (r != Z_OK) {
		pFree(stream);
		return NULL;
	}
	return stream;
}

void
ZMAEE_ZStreamDestroy(void* stream)
{
	zmaee_inflateEnd((z_stream*)stream);
	if (stream && ((ZMAEE_ZSTREAM*)stream)->pFree)
		((ZMAEE_ZSTREAM*)stream)->pFree(stream);
}


void 
ZMAEE_InflateReset(void* stream)
{
	zmaee_inflateReset((z_stream*)stream);
}

void 
ZMAEE_InflateSetNextIn(void* stream, 
					   unsigned char* next_in, unsigned int avail_in)
{
	z_stream* z;
	z = (z_stream*)stream;
	if (z == NULL)
		return;
	z->next_in = next_in;
	z->avail_in = avail_in;
}


void 
ZMAEE_InflateSetNextOut(void* stream,
						unsigned char* next_out, unsigned int avail_out)
{
	z_stream* z;
	z = (z_stream*)stream;
	if (z == NULL)
		return;
	z->next_out = next_out;
	z->avail_out = avail_out;
}

void ZMAEE_InflateGetInfoIn(void* stream, 
						    unsigned int* avail_in,
						    unsigned int* total_in)
{
	z_stream* z;
	z = (z_stream*)stream;
	if (z == NULL)
		return;
	if (avail_in)
		*avail_in = z->avail_in;
	if (total_in)
		*total_in = z->total_in;
}
void ZMAEE_InflateGetInfoOut(void* stream, 
						     unsigned int* avail_out,
						     unsigned int* total_out)
{
	z_stream* z;
	z = (z_stream*)stream;
	if (z == NULL)
		return;
	if (avail_out)
		*avail_out = z->avail_out;
	if (total_out)
		*total_out = z->total_out;
}

int ZMAEE_Inflate(void* stream)
{
	int r;
	if (stream == NULL)
		return ZMAEE_ZIPERR_BADPARAM;
	r = zmaee_inflate((z_stream*)stream, Z_SYNC_FLUSH);
	switch (r) {
	case Z_OK:
		return ZMAEE_ZIP_SUCCESS;
	case Z_STREAM_END:
		return ZMAEE_ZIP_FINISH;
	case Z_STREAM_ERROR:
		return ZMAEE_ZIPERR_IAERR;
	case Z_DATA_ERROR:
		return ZMAEE_ZIPERR_BADFILE;
	case Z_MEM_ERROR:
		return ZMAEE_ZIPERR_NOMEMORY;
	case Z_NEED_DICT:
	case Z_ERRNO:
	case Z_BUF_ERROR:
	case Z_VERSION_ERROR:
	default:
		break;
	}
	return ZMAEE_ZIPERR_UNKNOW;
}

/* ------------------------------------------------------------------------- 
   SSC Table入口
 ------------------------------------------------------------------------- */

#define AEE_MAX_INFO_BUFFER_SIZE		768

static unsigned char *sg_info_buffer = NULL;

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
void ZMAEE_SSC_Init_MenuItems(void)
{	
	extern void ZMAEE_SSC_Info_Highlight(void);
	extern void ZMAEE_Main_Highlight(void);
	extern void ZMAEE_SSC_Info_Highlight(void);
#ifdef __ZMAEE_APP_KEYPAD_LOCK__
	extern void	ZMAEE_APP_KeyLock_Highlight(void);
#endif
#ifdef __ZMAEE_APP_DWALLPAPER__
	extern void ZMAEE_APP_DWPaper_Highlight(void);
#endif
#ifdef __ZMAEE_APP_THEME__
	extern void	ZMAEE_APP_Theme_Highlight(void);
#endif

	SetHiliteHandler(MENU_ZMAEE_MAIN, ZMAEE_Main_Highlight);
	SetHiliteHandler(MENU_ZMAEE_SSC_INFO, ZMAEE_SSC_Info_Highlight);
#ifdef __ZMAEE_APP_KEYPAD_LOCK__
	SetHiliteHandler(MENU_ZMAEE_SSC_KEYLOCK,ZMAEE_APP_KeyLock_Highlight);
#endif
#ifdef __ZMAEE_APP_DWALLPAPER__
    SetHiliteHandler(MENU_ZMAEE_SSC_DWPAPER,ZMAEE_APP_DWPaper_Highlight);
#endif
#ifdef __ZMAEE_APP_THEME__
    SetHiliteHandler(MENU_ZMAEE_SSC_THEME,ZMAEE_APP_Theme_Highlight);
#endif
}
#endif

void ZMAEE_SSC_Entry(void)
{
	int number_of_items, i;
	unsigned short list_of_items[MAX_SUB_MENUS] = {0};
	unsigned short list_of_icons[MAX_SUB_MENUS] = {0};
	unsigned char *history_buffer;

#ifdef __ZMAEE_APP_DESKTOP__
	{
		extern void AEE_Desktop_Exit(void);
		AEE_Desktop_Exit();
	}
#endif
	
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
	ZMAEE_SSC_Init_MenuItems();
#endif

#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_SSC_MAIN, NULL, ZMAEE_SSC_Entry, NULL);
	history_buffer = GetCurrGuiBuffer(SCR_ZMAEE_SSC_MAIN);
#else
	mmi_frm_scrn_enter(GRP_ID_ROOT, SCR_ZMAEE_SSC_MAIN, NULL, ZMAEE_SSC_Entry, MMI_FRM_FULL_SCRN);
	history_buffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_SSC_MAIN);
#endif

	RegisterHighlightHandler(ExecuteCurrHiliteHandler);
	SetParentHandler(MENU_ZMAEE_SSC_MAIN);
	number_of_items = GetNumOfChild_Ext(MENU_ZMAEE_SSC_MAIN);
	GetSequenceStringIds_Ext(MENU_ZMAEE_SSC_MAIN, list_of_items);

	ASSERT(number_of_items <= 30);
	for(i = 0; i < number_of_items; i++) {
		list_of_icons[i] = IMG_GLOBAL_L1 + i;
	}
	
	ShowCategory15Screen(STR_ZMAEE_SSC_TITLE, NULL, STR_GLOBAL_OK, IMG_GLOBAL_OK, STR_GLOBAL_BACK, IMG_GLOBAL_BACK, 
		number_of_items, list_of_items, list_of_icons, LIST_MENU, 0, history_buffer);

	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
	SetKeyHandler(GoBackHistory, KEY_BACK, KEY_EVENT_UP);
}


void ZMAEE_SSC_Info_Highlight(void)
{
	void ZMAEE_SSC_Info_Entry(void);

	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_SSC_Info_Entry, KEY_RIGHT_ARROW, KEY_EVENT_UP);
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_SSC_Info_Entry, KEY_ENTER, KEY_EVENT_UP);
#endif
	SetLeftSoftkeyFunction(ZMAEE_SSC_Info_Entry, KEY_EVENT_UP);
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
	
}

void ZMAEE_SSC_Info_GoBack(void)
{
	if(sg_info_buffer) {
		ZMAEE_Free(sg_info_buffer);
		sg_info_buffer = NULL;
	}
	
	GoBackHistory();
}

void ZMAEE_SSC_Info_Exit(void)
{
	if(sg_info_buffer) {
		ZMAEE_Free(sg_info_buffer);
		sg_info_buffer = NULL;
	}
}

void ZMAEE_SSC_Info_Entry(void)
{
	extern kal_char* build_date_time(void);
	extern int ZMAEE_IShell_GetDeviceInfo(AEE_IShell * po, ZMAEEDeviceInfo* pdi);
	extern int ZMAEE_Utf8_2_Ucs2(const char* src, int len, unsigned short* des, int nSize);

	unsigned char *history_buffer;
	char asc_info_buffer[AEE_MAX_INFO_BUFFER_SIZE/2] = {0};
	AEE_IShell *pShell = ZMAEE_GetShell();
	ZMAEEDeviceInfo dev_info = {0};

	sg_info_buffer = (unsigned char*)ZMAEE_Malloc(AEE_MAX_INFO_BUFFER_SIZE);
	if(!sg_info_buffer) {	
		DisplayPopup((U8*)GetString(STR_ZMAEE_NOMEMORY), IMG_GLOBAL_ERROR, NULL, 1000, 0);
		return;
	}

#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_SSC_INFO, ZMAEE_SSC_Info_Exit, ZMAEE_SSC_Info_Entry, NULL);
	history_buffer = GetCurrGuiBuffer(SCR_ZMAEE_SSC_INFO);
#else
	mmi_frm_scrn_enter(GRP_ID_ROOT, SCR_ZMAEE_SSC_INFO, ZMAEE_SSC_Info_Exit, 
						ZMAEE_SSC_Info_Entry, MMI_FRM_FULL_SCRN);
	history_buffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_SSC_INFO);
#endif

	ZMAEE_IShell_GetDeviceInfo(pShell, &dev_info);

	sprintf(asc_info_buffer, "MTK Soft Ver:0x%04x\n", ZM_AEE_MTK_SOFTVERN);
#if defined MT6253
	strcat(asc_info_buffer, "MTK HW Ver:MT6253\n");
#elif defined MT6225
	strcat(asc_info_buffer, "MTK HW Ver:MT6225\n");
#elif defined MT6223
	strcat(asc_info_buffer, "MTK HW Ver:MT6223\n");
#elif defined MT6235
	strcat(asc_info_buffer, "MTK HW Ver:MT6235\n");
#elif defined MT6236
	strcat(asc_info_buffer, "MTK HW Ver:MT6236\n");
#elif defined MT6235B
	strcat(asc_info_buffer, "MTK HW Ver:MT6235B\n");
#elif defined MT6252
	strcat(asc_info_buffer, "MTK HW Ver:MT6252\n");
#elif defined MT6250
	strcat(asc_info_buffer, "MTK HW Ver:MT6250\n");
#else
	strcat(asc_info_buffer, "MTK HW Ver:Unknown\n");
#endif
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Ver:0x%x\n", (int)dev_info.nVersion);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "UsrId:%d\n", (int)dev_info.nUserId);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Os:%s\n", dev_info.szOS);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "OsVern:%s\n", dev_info.szOsVern);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Model:%s\n", dev_info.szModel);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Company:%s\n", dev_info.szCompany);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Width:%d,Height:%d\n", dev_info.cxScreen, dev_info.cyScreen);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "MaxRam:%d\n", dev_info.nMaxRam);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Kbd:%d\n", dev_info.bKbd);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "TouchScreen:%d\n", dev_info.bTouchScreen);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Cap:0x%x\n", dev_info.dwCapabilities);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Macro:");
#ifdef	__ZMAEE_APP_THEME__
	strcat(asc_info_buffer, " __ZMAEE_APP_THEME__");
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	strcat(asc_info_buffer, " __ZMAEE_APP_DWALLPAPER__");
#endif

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
	strcat(asc_info_buffer, " __ZMAEE_APP_KEYPAD_LOCK__");
#endif	
	strcat(asc_info_buffer, "\n");

#if (defined __ZMAEE_APP_THEME__) || (defined __ZMAEE_APP_KEYPAD_LOCK__) || (defined __ZMAEE_APP_DWALLPAPER__)
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "UI=%d %d %d\n", ZMAEE_GetThemeAppId(0), ZMAEE_GetThemeAppId(1), ZMAEE_GetThemeAppId(2));
#endif

	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "FAE: %s\n", dev_info.szFAEName);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Build Date: %s\n", dev_info.szBuildDate);
	sprintf(&asc_info_buffer[strlen(asc_info_buffer)], "Build Time: %s\n", (char*)build_date_time());

	ZMAEE_Utf8_2_Ucs2((const char*)asc_info_buffer, strlen(asc_info_buffer), (unsigned short*)sg_info_buffer, AEE_MAX_INFO_BUFFER_SIZE/2);	

	ShowCategory74Screen(STR_ZMAEE_SSC_INFO, NULL, NULL, NULL, STR_GLOBAL_BACK, IMG_GLOBAL_BACK,
		sg_info_buffer, AEE_MAX_INFO_BUFFER_SIZE, history_buffer);

	ClearKeyHandler(KEY_LSK, KEY_EVENT_DOWN);
	ClearKeyHandler(KEY_LSK, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_SSC_Info_GoBack, KEY_EVENT_UP);
}


/* ------------------------------------------------------------------------- 
   UI主题的NVRAM接口
 ------------------------------------------------------------------------- */
#if (defined __ZMAEE_APP_THEME__) || (defined __ZMAEE_APP_KEYPAD_LOCK__) || (defined __ZMAEE_APP_DWALLPAPER__)

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0)
static void ZMAEE_RestoreThemeWallPaper(void);

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
static S32 ZMAEE_mmi_phnset_del_file(const WCHAR* file_path)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    S32 attr = 0;
    U8 path_asc[FMGR_MAX_PATH_LEN + FMGR_MAX_FILE_LEN];

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    attr = FS_GetAttributes((const WCHAR*) file_path);
    if (attr & FS_ATTR_READ_ONLY)
    {
        FS_SetAttributes((const WCHAR*) file_path, (U8) (attr & ~(FS_ATTR_READ_ONLY)));
    }

    mmi_ucs2_to_asc((PS8) path_asc, (PS8) file_path);

    MMI_TRACE(MMI_COMMON_TRC_G7_SETTING, MMI_WALLPAPER_TRACE_021, path_asc);

    return FS_Delete((const WCHAR*)file_path);
}


static void ZMAEE_mmi_phnset_get_postfix_ucs2(U8 *postfix, U8 *inBuffer)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    U8 *ext = NULL;
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if ((inBuffer == NULL) || (postfix == NULL))
    {
        return;
    }

    /* search the '.' from end of file_name */
    {
        U8 *pos = (U8 *) inBuffer;
        
        while (1)
        {
            if ((*pos == 0) && (*(pos+1) == 0))
                break;

            if (*pos == L'.')
            {
                ext = pos + 2;
            }
            pos += 2;
        }
    }
    
    if (ext)
    {
        mmi_ucs2_to_asc((PS8)postfix, (PS8)ext);
    }
    else
    {
        mmi_ucs2_to_asc((PS8)postfix, (PS8)L"");
    }
}

void ZMAEE_SetThemeWallPaper_Internal(S8* _path)
{
	extern PHNSET_WPSS_FILENAME_STRUCT phnset_wpss_default_file_1;
	extern PHNSET_WPSS_FILENAME_STRUCT phnset_wpss_default_file_2;
	extern U8 PHNSET_DEFAULT_WP_PATH_1[];
	extern U8 PHNSET_DEFAULT_WP_PATH_2[];
	extern U8 phnset_dispchar_wp_cp_file[];
	
	#ifdef __MMI_FILE_MANAGER__
	#define FMGR_DEFAULT_FOLDER_SETTINGS   "Settings"
	#endif 	

	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	FS_HANDLE fh;
	U32 new_file_len = 0;	/* size of new file */
	U64 disk_free_space;
	U64 disk_total_space;
	S32 fs_ret;
	S8 file_path_ascii[5];
	S8 file_path_ascii_2[5] = ":\\";
	S8 file_path[10];
	U8 path_asc[FMGR_MAX_PATH_LEN + FMGR_MAX_FILE_LEN + FMGR_MAX_EXT_LEN + 1];
	U8 postfix[PHNSET_MAX_EXT_LEN];
	U8 path_ucs2[(FMGR_MAX_PATH_LEN + FMGR_MAX_FILE_LEN + FMGR_MAX_EXT_LEN + 1) * ENCODING_LENGTH];
	FS_DiskInfo disk_info;
	S16 drive;
	U8 *pTmp;
	FS_HANDLE df;
	int rlen,wlen;
	S8 *phnset_dispchar_strp_bk;
	unsigned int d_size = 0, s_size = 0;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	phnset_dispchar_strp_bk = phnset_dispchar_strp;
	
	phnset_dispchar_strp = _path;
   
	drive = MMI_PUBLIC_DRV;
	if (drive > 0)
	{
		s8 path_ascii[64];
		S8 path[64 * ENCODING_LENGTH];
		FS_HANDLE fh;
		S32 result = FS_NO_ERROR;
	
		/* Create Path */
		memset(path_ascii, 0x00, 64);
		sprintf(path_ascii, "%c:\\%s", (S8)drive, FMGR_DEFAULT_FOLDER_SETTINGS);
		mmi_asc_to_ucs2(path, path_ascii);
		
		fh = FS_Open((U16*) path, FS_OPEN_DIR | FS_READ_ONLY);
		
		/* Folder Exists */
		if (fh > 0)
		{
			FS_SetAttributes((U16*) path, FS_ATTR_DIR | FS_ATTR_HIDDEN);
			FS_Close(fh);
		}
		/* Folder Does Not Exist */
		else
		{
			result = FS_CreateDir((U16*) path);
			/* Create Success */
			if (result == FS_NO_ERROR)
			{
				FS_SetAttributes((U16*) path, FS_ATTR_DIR | FS_ATTR_HIDDEN);
			}
		}
	}

	if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_WP)
	{
		phnset_wpss_default_file_1.isWPShort = 0;
		phnset_wpss_default_file_2.isWPShort = 0;
	
	}

	mmi_phnset_init_default_file();

	/* check device status */	
	mmi_phnset_del_dirty_file();

	{
		S16 drive;

		drive = MMI_PUBLIC_DRV;
		sprintf(file_path_ascii, "%c%s", drive, file_path_ascii_2);
		mmi_asc_to_ucs2((PS8)file_path, file_path_ascii);
		fs_ret = FS_GetDiskInfo((PU16) file_path, &disk_info, FS_DI_BASIC_INFO | FS_DI_FREE_SPACE);
		
		disk_free_space = 0;
		disk_total_space = 0;
		if (fs_ret >= 0)
		{
			disk_free_space = disk_info.FreeClusters * disk_info.SectorsPerCluster * disk_info.BytesPerSector;
			disk_total_space = disk_info.TotalClusters * disk_info.SectorsPerCluster * disk_info.BytesPerSector;
		}
	}

	/* disk free space is OK or old file size more */
	if (new_file_len < disk_free_space)
	{
		/* copy new file to default folder */
		{
			/* mmi_ucs2_to_asc((PS8) path_asc, (PS8) phnset_dispchar_strp); */
			ZMAEE_mmi_phnset_get_postfix_ucs2((PU8) postfix, (PU8) phnset_dispchar_strp);
			
			if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_WP)
			{
				memset(phnset_dispchar_wp_cp_file, 0x00, ((FMGR_MAX_PATH_LEN + 1) * ENCODING_LENGTH));

				/* default file 1 is empty */
				if ((phnset_wpss_default_file_1.isWPShort == 0) && (mmi_ucs2cmp((PS8)phnset_wpss_default_file_1.wallpaper, (PS8)L"") == 0))
				{
					phnset_wpss_default_file_1.isWPShort = 1;
					sprintf((PS8) path_asc, "%s.%s", PHNSET_DEFAULT_WP_PATH_1, postfix);
				}
				else if ((phnset_wpss_default_file_2.isWPShort == 0) && (mmi_ucs2cmp((PS8)phnset_wpss_default_file_2.wallpaper, (PS8)L"") == 0))
				{
					phnset_wpss_default_file_2.isWPShort = 1;
					sprintf((PS8) path_asc, "%s.%s", PHNSET_DEFAULT_WP_PATH_2, postfix);
				}

				mmi_asc_to_ucs2((PS8) path_ucs2, (PS8) path_asc);
				mmi_ucs2cpy((PS8) phnset_dispchar_wp_cp_file, (PS8) path_ucs2);
			}

			/* delete exist file with same name */
			{
				fh = FS_Open((U16 *)path_ucs2, FS_READ_ONLY);
				if (fh >= FS_NO_ERROR)
				{
					S32 ret_del;

					FS_Close(fh);
					ret_del = ZMAEE_mmi_phnset_del_file((const unsigned short *)path_ucs2);
				}
			}
		}

		pTmp = mmi_frm_scrmem_alloc(1024);
		if(pTmp == NULL)
		{
			phnset_dispchar_strp = phnset_dispchar_strp_bk;
			ZMAEE_RestoreThemeWallPaper();
			return;
		}

		df = FS_Open((WCHAR *)path_ucs2,FS_CREATE|FS_READ_WRITE);		
		if(df < FS_NO_ERROR)
		{
			phnset_dispchar_strp = phnset_dispchar_strp_bk;
			mmi_frm_scrmem_free(pTmp);
			ZMAEE_RestoreThemeWallPaper();
			return;
		}

		{
			char asc_wp_path[128] = {0};
			ZMAEE_FileInfo file_info = {0};
			AEE_IFileMgr *pFileMgr = NULL;
			AEE_IFile *pFile = NULL;
			int read_size = 0;
			
			ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
			ZMAEE_Ucs2_2_Utf8((const unsigned short*)phnset_dispchar_strp, mmi_ucs2strlen((const S8*)phnset_dispchar_strp),
				asc_wp_path, sizeof(asc_wp_path));
			pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr, (const char*)asc_wp_path, ZM_OFM_READONLY);
			if(!pFile) {				
				phnset_dispchar_strp = phnset_dispchar_strp_bk;
				FS_Close(df);
				mmi_frm_scrmem_free(pTmp);
				ZMAEE_RestoreThemeWallPaper();
				ZMAEE_IFileMgr_Release(pFileMgr);
				return;
			}

			
			ZMAEE_IFileMgr_GetInfo(pFileMgr, asc_wp_path, &file_info);
			s_size = file_info.dwSize;
			
			do {
				rlen = ZMAEE_FileInlib_Read(pFile, pTmp, 1024);
				if(rlen > 0) {
					FS_Write(df, pTmp, rlen, (unsigned int*)&wlen);
					read_size += rlen;
				}
			}while((rlen > 0) && (read_size < s_size));

			FS_GetFileSize(df, &d_size);

			mmi_frm_scrmem_free(pTmp);
			FS_Close(df);
			ZMAEE_FileInlib_Release(pFile);
			ZMAEE_IFileMgr_Release(pFileMgr);

			if(!d_size || !s_size || d_size != s_size) {
				phnset_dispchar_strp = phnset_dispchar_strp_bk;
				ZMAEE_RestoreThemeWallPaper();
				return;
			}

			if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_WP)
			mmi_phnset_copy_wpss_save_setting(phnset_dispchar_wp_cp_file);
		}
	}else {
		ZMAEE_RestoreThemeWallPaper();
	}
	phnset_dispchar_strp = phnset_dispchar_strp_bk;
}

#else
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
static U8 zmaee_mmi_phnset_copy_wallpaper_screensaver(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
	extern mmi_phnset_wp_ss_pwrdisp_common_filename_struct g_phnset_wallpaper_default_file_1;
	extern mmi_phnset_wp_ss_pwrdisp_common_filename_struct g_phnset_wallpaper_default_file_2;
	extern mmi_phnset_wp_ss_pwrdisp_common_filename_struct g_phnset_screensaver_default_file_1;
	extern mmi_phnset_wp_ss_pwrdisp_common_filename_struct g_phnset_screensaver_default_file_2;
	extern U8 phnset_dispchar_wp_cp_file[(SRV_FMGR_PATH_MAX_LEN + 1) * 2];
	
    U8 path_ucs2[(SRV_FMGR_PATH_MAX_LEN + 1) * 2];
    S16 drive;
    U8 process_result;
	int ret;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    MMI_TRACE(MMI_COMMON_TRC_G7_SETTING, MMI_WALLPAPER_TRACE_022);

    drive = SRV_FMGR_PUBLIC_DRV;

    process_result = mmi_phnset_wallpaper_prepare_folder(drive);
    if( process_result != PHNSET_WP_SS_SET_NO_ERROR)
    {
        return process_result;
    }

    if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_WP)
    {
        g_phnset_wallpaper_default_file_1.is_short = 0;
        g_phnset_wallpaper_default_file_2.is_short = 0;        
    }
    else if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_SS)
    {
        g_phnset_screensaver_default_file_1.is_short = 0;
        g_phnset_screensaver_default_file_2.is_short = 0; 
    }

    mmi_phnset_init_default_file();
    
    mmi_phnset_del_dirty_file();

    /* disk free space is OK or old file size more */
    MMI_TRACE(MMI_COMMON_TRC_G7_SETTING, MMI_WALLPAPER_TRACE_029);

    process_result = mmi_phnset_wallpaper_prepare_file_path(path_ucs2);
    if(process_result != PHNSET_WP_SS_SET_NO_ERROR)
    {
        return process_result;
    }

    process_result = mmi_phnset_wallpaper_check_disk(drive);    
    if(process_result != PHNSET_WP_SS_SET_NO_ERROR)
    {
        return process_result;
    }
	
    /* use FMT to copy a file */
    {
    	char *pTmp;
		FS_HANDLE df;
		int s_size, d_size;
		int rlen, wlen;
		
		pTmp = mmi_frm_scrmem_alloc(1024);
		if(pTmp == NULL)
		{
			ZMAEE_RestoreThemeWallPaper();
			return -1;
		}

		df = FS_Open((WCHAR *)path_ucs2,FS_CREATE|FS_READ_WRITE);		
		if(df < FS_NO_ERROR)
		{
			mmi_frm_scrmem_free(pTmp);
			ZMAEE_RestoreThemeWallPaper();
			return -1;
		}

		{
			char asc_wp_path[128] = {0};
			ZMAEE_FileInfo file_info = {0};
			AEE_IFileMgr *pFileMgr = NULL;
			AEE_IFile *pFile = NULL;
			int read_size = 0;
			
			ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
			ZMAEE_Ucs2_2_Utf8((const unsigned short*)phnset_dispchar_strp, mmi_ucs2strlen((const S8*)phnset_dispchar_strp),
				asc_wp_path, sizeof(asc_wp_path));
			pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr, (const char*)asc_wp_path, ZM_OFM_READONLY);
			if(!pFile) {				
				FS_Close(df);
				mmi_frm_scrmem_free(pTmp);
				ZMAEE_RestoreThemeWallPaper();
				ZMAEE_IFileMgr_Release(pFileMgr);
				mmi_phnset_del_file((const unsigned short *)phnset_dispchar_wp_cp_file);
				return -1;
			}

			ZMAEE_IFileMgr_GetInfo(pFileMgr, asc_wp_path, &file_info);
			s_size = file_info.dwSize;
			
			do {
				rlen = ZMAEE_FileInlib_Read(pFile, pTmp, 1024);
				if(rlen > 0) {
					FS_Write(df, pTmp, rlen, (unsigned int*)&wlen);
					read_size += rlen;
				}
			}while((rlen > 0) && (read_size < s_size));

			FS_GetFileSize(df, &d_size);

			mmi_frm_scrmem_free(pTmp);
			FS_Close(df);
			ZMAEE_FileInlib_Release(pFile);
			ZMAEE_IFileMgr_Release(pFileMgr);

			if(!d_size || !s_size || d_size != s_size) {
				ZMAEE_RestoreThemeWallPaper();
				mmi_phnset_del_file((const unsigned short *)phnset_dispchar_wp_cp_file);
				return -1;
			}
		}
    }
	
    if (g_phnset_cntx_p->wpss_type == MMI_PHNSET_TYPE_WP)
    {
        mmi_phnset_copy_wpss_save_setting(phnset_dispchar_wp_cp_file);
    }
	      
        
    return PHNSET_WP_SS_SET_NO_ERROR;
}

void ZMAEE_SetThemeWallPaper_Internal(S8* _path)
{
	extern void mmi_phnset_wp_ss_backup_para(void);
	extern void mmi_phnset_wp_ss_restore_para(void);
	
	char *phnset_dispchar_strp_backup = NULL;

	phnset_dispchar_strp_backup = phnset_dispchar_strp;
	phnset_dispchar_strp = _path;
	mmi_phnset_wp_ss_backup_para();
	g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
	g_phnset_cntx_p->isShort = 1;
	zmaee_mmi_phnset_copy_wallpaper_screensaver();		
	phnset_dispchar_strp = phnset_dispchar_strp_backup;
	mmi_phnset_wp_ss_restore_para();
}

#else

static 
int ZMAEE_DumpThemeWallpaperRomToFile(S8* path, unsigned int appId)
{
	if(ZMAEE_IsAppletInROM(appId)) {
		char utf8_file_path[AEE_MAX_PATH_NAME] = {0};
		char dump_file_path[AEE_MAX_PATH_NAME] = {0};
		char dir_path[AEE_MAX_PATH_NAME] = {0};
		char* tmp = NULL;
		char drv = 0;
		ZMAEE_FileInfo file_info = {0};
		unsigned long free_space_size = 0;
		char* pTmp = NULL;
		AEE_IFileMgr* pFileMgr = NULL;
		AEE_IFile* in_rom_pFile = NULL;
		AEE_IFile* dump_file_pFile = NULL;

		int read_size = 0, write_size = 0, rlen = 0;
		extern unsigned long ZMAEE_IFileMgr_GetFreeSpace(AEE_IFileMgr* po, char driver);
		
		ZMAEE_IShell_CreateInstance(ZMAEE_GetShell(), ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
		if(pFileMgr == NULL) {
			ZMAEE_RestoreThemeWallPaper();
			return E_ZM_AEE_FALSE;
		}

		ZMAEE_Ucs2_2_Utf8((const unsigned short*)path, mmi_ucs2strlen(path), utf8_file_path, sizeof(utf8_file_path)/sizeof(utf8_file_path[0]));
		ZMAEE_IFileMgr_GetInfo(pFileMgr, utf8_file_path, &file_info);

		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): path=%s", utf8_file_path);

		drv = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
		if(drv == 0) {
			drv = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);
		}
		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): drv=%d, E=%d", drv, 'E');
		
		free_space_size = ZMAEE_IFileMgr_GetFreeSpace(pFileMgr, drv);
		if(free_space_size <= file_info.dwSize) {
			ZMAEE_IFileMgr_Release(pFileMgr);
			ZMAEE_RestoreThemeWallPaper();
			return E_ZM_AEE_FALSE;
		}
		
		//dump文件到t卡或者系统盘
		strcpy(dump_file_path, (const char*)utf8_file_path);
		dump_file_path[0] = drv;
		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): dump_path=%s", dump_file_path);

		pTmp = (char*)med_alloc_ext_mem(1024);
		if(pTmp == NULL) {
			ZMAEE_IFileMgr_Release(pFileMgr);
			ZMAEE_RestoreThemeWallPaper();
			return E_ZM_AEE_FALSE;
		}

		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): pTmp = 0x%08x", pTmp);

		strcpy(dir_path, dump_file_path);
		tmp = strrchr(dir_path, '\\');
		*(tmp+1) = 0; 
		{
			int ret = 0;
			ZMAEE_IFileMgr_MkDir(pFileMgr, dir_path);
			ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): dir_path = %s, result = %d", dir_path, ret);
		}

		in_rom_pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr, utf8_file_path, ZM_OFM_READONLY);
		dump_file_pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr, dump_file_path, ZM_OFM_CREATE | ZM_OFM_READWRITE);
		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile():in_rom_pFile = 0x%08x, dump_file_pFile = 0x%08x", in_rom_pFile, dump_file_pFile);

		if(in_rom_pFile == NULL ||  dump_file_pFile == NULL) {
			med_free_ext_mem((void**)&pTmp);
			if(in_rom_pFile) {
				ZMAEE_IFile_Release(in_rom_pFile);
			}
			if(dump_file_pFile) {
				ZMAEE_IFile_Release(dump_file_pFile);
			}
			ZMAEE_IFileMgr_Release(pFileMgr);
			ZMAEE_RestoreThemeWallPaper();
			return E_ZM_AEE_FALSE;
		}

		do{
			int wlen = 0;	
			rlen = ZMAEE_FileInlib_Read(in_rom_pFile, pTmp, 1024);
			if(rlen > 0) {
				wlen = ZMAEE_IFile_Write(dump_file_pFile, pTmp, rlen);
				read_size += rlen;
				write_size += wlen;
			}
		}while((rlen > 0) &&(read_size < file_info.dwSize));

		med_free_ext_mem((void**)&pTmp);
		ZMAEE_IFile_Release(in_rom_pFile);
		ZMAEE_FileInlib_Release(dump_file_pFile);
		ZMAEE_IFileMgr_Release(pFileMgr);

		ZMAEE_DebugPrint("ZMAEE_DumpThemeWallpaperRomToFile(): write_size=%d, file_size=%d", write_size, file_info.dwSize);
		
		if(write_size != file_info.dwSize) {
			ZMAEE_RestoreThemeWallPaper();
			return E_ZM_AEE_FALSE;
		}
	}

	return E_ZM_AEE_SUCCESS;
}


static 
U8 zmaee_mmi_phnset_copy_wallpaper_screensaver(void* obj, const WCHAR* file_name)
{
	extern void zmaee_mmi_phnset_wallpaper_direct_set_file(void* obj, const WCHAR* file_name);

	zmaee_mmi_phnset_wallpaper_direct_set_file(obj, file_name);
	       
    return PHNSET_WP_SS_SET_NO_ERROR;
}


void ZMAEE_SetThemeWallPaper_Internal(S8* _path)
{	
#if (ZM_AEE_MTK_SOFTVERN < 0x1220)
	WCHAR *g_phnset_view_setting_src_file_name_backup = NULL;
	phnset_wpss_type_enum type_backup = 0;
	mmi_setting_display_fmgr_setting_object_struct *current_setting_obj;
	extern WCHAR *g_phnset_view_setting_src_file_name;

	g_phnset_view_setting_src_file_name_backup = g_phnset_view_setting_src_file_name;
	g_phnset_view_setting_src_file_name = (WCHAR *)_path;
	type_backup = g_phnset_cntx_p->wpss_type;
	g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
	g_phnset_cntx_p->isShort = 1;
	zmaee_mmi_phnset_copy_wallpaper_screensaver((void*)current_setting_obj, (const WCHAR*)_path);	
	g_phnset_view_setting_src_file_name = g_phnset_view_setting_src_file_name_backup;
	g_phnset_cntx_p->wpss_type = type_backup;
#endif
}


#endif
#endif
#endif


static
void ZMAEE_SetThemeWallPaper(int type, unsigned int appId)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0) || (ZM_AEE_MTK_SOFTVERN >= 0x11B0 && ZM_AEE_MTK_SOFTVERN < 0x1220)
	unsigned short wcs_wp_path[AEE_MAX_PATH_NAME / 2] ={0};
	char ascii_wp_path[AEE_MAX_PATH_NAME / 2] = {0};

	AEE_IShell *pShell = NULL;
	AEE_IFileMgr *pFileMgr = NULL;
	int i;

	if(appId == 0)
		return;

	pShell = ZMAEE_GetShell();
	ZMAEE_IShell_CreateInstance(pShell, ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
	if(!pFileMgr)
		return;

	sprintf(&ascii_wp_path[1], "%s%08x\\%08x.jpg", ZMAEE_ENTRY_THEMEDIR, appId, appId);

	for(i = 1; i >= 0; i--) 
	{
		ascii_wp_path[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, i);
		ZMAEE_DebugPrint("ZMAEE_SetThemeWallPaper: ascii_wp_path = %s", ascii_wp_path);

		if(ascii_wp_path[0]) 
		{
			if((ZMAEE_IFileMgr_Test(pFileMgr, ascii_wp_path) == E_ZM_AEE_SUCCESS) || 
				(ZMAEE_IsAppletInROM(appId)))
			{
				extern PHNSET_CNTX *g_phnset_cntx_p;				
				g_phnset_cntx_p->wpss_type = MMI_PHNSET_TYPE_WP;
				ZMAEE_Utf8_2_Ucs2(ascii_wp_path, strlen(ascii_wp_path), wcs_wp_path, sizeof(wcs_wp_path)/2);
				
			#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
			{
				int ret;
				ret = ZMAEE_DumpThemeWallpaperRomToFile((S8*)wcs_wp_path, appId);
				if(ret != E_ZM_AEE_SUCCESS) {
					break;
				}
			}
			#endif

				ZMAEE_SetThemeWallPaper_Internal((S8*)wcs_wp_path);
				break;
			}
		}
	}
	
	if(pFileMgr) {
		ZMAEE_IFileMgr_Release(pFileMgr);
	}
#endif	
}

static
void ZMAEE_RestoreThemeWallPaper(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0) || (ZM_AEE_MTK_SOFTVERN >= 0x11B0 && ZM_AEE_MTK_SOFTVERN < 0x1220)
	g_phnset_cntx_p->curtWpID = RstGetWallpaperImgIdDefault();

	mmi_dispchar_set_img_id(NVRAM_FUNANDGAMES_SETWALLPAPER, &(g_phnset_cntx_p->curtWpID));
	SetIdleScreenWallpaperID(g_phnset_cntx_p->curtWpID, MMI_TRUE);
#endif
}

static
void ZMAEE_CloseVuiHomeScreen(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#ifdef __MMI_VUI_HOMESCREEN__
	extern PHNSET_CNTX *g_phnset_cntx_p;
	S16 error;

	g_phnset_cntx_p->vui_home_screen = 0;
    WriteValue(NVRAM_VUI_PHNSET_HOMESCREEN, &(g_phnset_cntx_p->vui_home_screen), DS_BYTE, &error);
#endif
#endif
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
void ZMAEE_ClearWallpaperAppId(void)
{
	short err_code;
	int ret;
	ZMAEE_THEME_APPID theme_appid = {0};

	if(ZMAEE_ReadRecord(&theme_appid, sizeof(theme_appid)) != E_ZM_AEE_SUCCESS) {
		theme_appid.lockscreen_appid = ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID;
		theme_appid.wallpaper_appid = ZMAEE_DEFAULT_DWALLPAPER_CLSID;
		theme_appid.mainmenu_appid = ZMAEE_DEFAULT_THEME_CLSID;
	}

	theme_appid.wallpaper_appid = 0;

	ZMAEE_WriteRecord(&theme_appid, sizeof(theme_appid));
}
#endif

/**
 * 保存主题的appid接口
 * @type			0 - 锁屏appid，1 - 动态桌布appid，2 - 主菜单appid
 * @appId		appId
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		type无效
 * 	E_ZM_AEE_FAILURE		保存失败
 * 	E_ZM_AEE_SUCCESS		保存成功
 */
int ZMAEE_SetThemeAppId(int type, unsigned int appId)
{
	short err_code;
	int ret;
	ZMAEE_THEME_APPID theme_appid = {0};

	if((type < 0) || (type > 2)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(ZMAEE_ReadRecord(&theme_appid, sizeof(theme_appid)) != E_ZM_AEE_SUCCESS) {
		theme_appid.lockscreen_appid = ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID;
		theme_appid.wallpaper_appid = ZMAEE_DEFAULT_DWALLPAPER_CLSID;
		theme_appid.mainmenu_appid = ZMAEE_DEFAULT_THEME_CLSID;
	}
	
	switch(type) {
		case 0:
			theme_appid.lockscreen_appid = appId;
			break;
		case 1:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			ZMAEE_CloseVuiHomeScreen();
#endif


#if (ZM_AEE_MTK_SOFTVERN == 0x10A0)
#ifdef __ZMAEE_APP_DWALLPAPER__
#ifdef __MMI_VUI_HOMESCREEN__
	{
		extern void ZMAEE_Phnset_WallPaper_SetVuiIndex(int clsId);
		ZMAEE_Phnset_WallPaper_SetVuiIndex(appId);
	}
#endif
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN == 0x11A0) || defined (__MMI_VUI_LAUNCHER_KEY__)
#ifdef __ZMAEE_APP_DWALLPAPER__
	if(appId == 0) {
		extern void ZMAEE_ClearWallpaperId();
		ZMAEE_ClearWallpaperId();
	} else {
		extern void ZMAEE_SetWallpaperId(void);
		ZMAEE_SetWallpaperId();
	}
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN == 0x11B0) && defined (__MMI_VUI_LAUNCHER__)
#ifdef __ZMAEE_APP_DWALLPAPER__
	{
		extern void ZMAEE_Phnset_WallPaper_SetVuiIndex(int clsId);
		ZMAEE_Phnset_WallPaper_SetVuiIndex(appId);
	}
#endif
#endif
			theme_appid.wallpaper_appid = appId;
			break;
		case 2:			
			ZMAEE_SetThemeWallPaper(type, appId);
			theme_appid.mainmenu_appid = appId;
			if(appId == 0)
				ZMAEE_RestoreThemeWallPaper();
			break;
		default:
			return E_ZM_AEE_BADPARAM;
	}

	if(ZMAEE_WriteRecord(&theme_appid, sizeof(theme_appid)) != E_ZM_AEE_SUCCESS)
		return E_ZM_AEE_FAILURE;

#ifdef __ZMAEE_APP_THEME__
#ifndef ZMAEE_CLOSE_THEME_BEFORE_ENTRY
	if(type == 2) {
		extern void ZMAEE_Theme_Exit(void);
		ZMAEE_Theme_Exit();
		if(appId != 0) {
			extern void ZMAEE_Theme_Start(void);
			ZMAEE_Theme_Start();
		}
	}
#endif
#endif

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取主题的appid
 * @type			0 - 锁屏appid，1 - 动态桌布appid，2 - 主菜单appid
 * RETURN:
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	E_ZM_AEE_BADPARAM		type无效
 * 	> 0						appId
 */
unsigned int ZMAEE_GetThemeAppId(int type)
{
	#ifndef WIN32
	extern MMI_BOOL srv_usb_is_in_mass_storage_mode(void);
	#endif
	short err_code;
	int ret;
	ZMAEE_THEME_APPID theme_appid = {0};

	#ifndef WIN32
	if(srv_usb_is_in_mass_storage_mode())
		return 0;
	#endif

	if((type < 0) || (type > 2)) {
		return 0;
	}
	
	if(ZMAEE_ReadRecord(&theme_appid, sizeof(theme_appid)) != E_ZM_AEE_SUCCESS) {
		switch(type) {
			case 0:
				return ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID;
			case 1:
				return ZMAEE_DEFAULT_DWALLPAPER_CLSID;
			case 2:
				return ZMAEE_DEFAULT_THEME_CLSID;
			default:
				return 0;
		}
	}
	
	switch(type) {
		case 0:
			return theme_appid.lockscreen_appid;
		case 1:
		{
		#ifdef __ZMAEE_APP_DWALLPAPER__
			extern int sg_zmaee_dwp_exp_timers;
			sg_zmaee_dwp_exp_timers = (theme_appid.wallpaper_appid & 0xF0000000) >> 28;
			if(sg_zmaee_dwp_exp_timers < 3)
				theme_appid.wallpaper_appid &= 0x0FFFFFFF;
		#endif
			return theme_appid.wallpaper_appid;
		}
		case 2:
			return theme_appid.mainmenu_appid;
	}

	return 0;
}
#endif

/***********************************************************
 动态壁纸
***********************************************************/

#ifdef __ZMAEE_APP_DWALLPAPER__

typedef struct {
	int				validating;			// 动态壁纸是否生效
}ZMAEE_DWP_CNTX;

static ZMAEE_DWP_CNTX sg_zmaee_dwp_cntx = {0};
static int sg_zmaee_dwp_exp_timers = 0;
void ZMAEE_DWallPaper_SetMask(void)
{
	ZM_AEECLSID clsId = ZMAEE_GetThemeAppId(1);

	if((clsId > 0) && (!ZMAEE_IsAppletInROM(clsId))) 
	{
		sg_zmaee_dwp_exp_timers++;
		clsId |= (sg_zmaee_dwp_exp_timers << 28);
		ZMAEE_SetThemeAppId(1, clsId);	
	}

}

void ZMAEE_DWallPaper_ClrMask(void)
{
	ZM_AEECLSID clsId = ZMAEE_GetThemeAppId(1);

	if((clsId > 0) && (!ZMAEE_IsAppletInROM(clsId))) 
	{
		sg_zmaee_dwp_exp_timers = 0;
		clsId &= 0x0FFFFFFF;
		ZMAEE_SetThemeAppId(1, clsId);
	}
}	

/**
 * 启动动态壁纸
 */
void ZMAEE_DWallPaper_Start_param(int param);
void ZMAEE_DWallPaper_Start(void)
{
	ZMAEE_DWallPaper_Start_param(1);
}

void ZMAEE_DWallPaper_Start_param(int param)
{
	ZM_AEECLSID clsId;
	AEE_IShell *shell_ptr;
	char start_param = (char)param;
	char appName[] = {0xA8, 0x52, 0x01, 0x60, 0x4C, 0x68, 0x03, 0x5E, 0x00, 0x00};

	ZMAEE_DebugPrint("ZMAEE_DWallPaper_Start: Entry, restart_theme = %d", sg_zmaee_restart_theme);
	
	if(sg_zmaee_restart_theme == 1)
		return;
	
	clsId = ZMAEE_GetThemeAppId(1);
	ZMAEE_DebugPrint("ZMAEE_DWallPaper_Start: clsId = %d", clsId);
	if((clsId == ZM_AEE_APPLET_CLSID_BASE) || (clsId > ZM_AEE_APPLET_CLSID_MAX)) {
		sg_zmaee_dwp_cntx.validating = 0;
		return;
	}

#ifdef __COSMOS_MMI_PACKAGE__
	if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) == clsId)
		return;
#endif
	
	shell_ptr = ZMAEE_GetShell();
	if(!ZMAEE_IShell_SetWorkDir(shell_ptr, ZMAEE_ENTRY_THEMEDIR, 0)) {
		sg_zmaee_dwp_cntx.validating = 0;
		return;
	}

	if(ZMAEE_IsAppletInROM(clsId) || (ZMAEE_IShell_CanStartApplet(shell_ptr, clsId) && ZMAEE_IShell_ValidateApplet(shell_ptr, clsId))) {
		sg_zmaee_dwp_set_mask = 1;
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		ZMAEE_CloseVuiHomeScreen();
	#endif
		ZMAEE_AppletEntry(clsId, ZMAEE_ENTRY_THEMEDIR, &start_param, sizeof(start_param), appName, NULL, 0, 1, 0xFFFFFFFF, NULL, 0);		
		if(ZMAEE_CheckPlat_IsRunning()) {
			sg_zmaee_dwp_cntx.validating = 1;			
			return;
		}
		ZMAEE_DWallPaper_ClrMask();
	} 
}


/**
 * 退出动态壁纸
 */
void ZMAEE_DWallPaper_Exit(void)
{
	if(sg_zmaee_dwp_cntx.validating) {
		ZMAEE_Exit_Ext();
		ZMAEE_DWallPaper_ClrMask();		
		sg_zmaee_dwp_cntx.validating = 0;
	}
}

/**
 * 动态壁纸是否生效
 */
int ZMAEE_DWallPaper_Validating(void)
{

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if((sg_zmaee_dwp_cntx.validating == 1) && (GetActiveScreenId() == IDLE_SCREEN_ID))
#else
	if((sg_zmaee_dwp_cntx.validating == 1) && (GetActiveScreenId() == SCR_ID_IDLE_MAIN))
#endif
	{
		return E_ZM_AEE_TRUE;
	}

	return E_ZM_AEE_FALSE;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
int ZMAEE_DWallPaper_IsStart(void)
{
	return sg_zmaee_dwp_cntx.validating;
}
#endif

/**
 * User Draw Notify
 */
void ZMAEE_DWallPaper_Paint_Notify(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)

	gdi_handle active_layer;
#if defined(__MMI_VUI_HOMESCREEN__)
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern MMI_BOOL mmi_vhs_is_enable(void);
#endif
#endif
	gdi_layer_get_active(&active_layer);

#ifndef MT6252
#if defined(__MMI_VUI_HOMESCREEN__)
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(mmi_vhs_is_enable())
#else
	if(1)
#endif
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, NULL, NULL);
	else
#endif
#endif
	{
		extern gdi_handle ZMAEE_IDisplay_GetWallpaperLayer(void);
		gdi_handle active_layer, wallpaper_layer, temp_layer;
			
		gdi_layer_get_active(&active_layer);
		wallpaper_layer = dm_get_wallpaper_layer();
		temp_layer = ZMAEE_IDisplay_GetWallpaperLayer();
		
		if(wallpaper_layer == GDI_ERROR_HANDLE)
			return;

		if(active_layer == wallpaper_layer) {
			ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, NULL, NULL);
			temp_layer = ZMAEE_IDisplay_GetWallpaperLayer();
			if(temp_layer) {
				gdi_layer_set_active(active_layer);
				gdi_layer_flatten(temp_layer, NULL, NULL, NULL);
			}
		} else {
			if(!temp_layer)
				gdi_layer_flatten_with_clipping(active_layer, wallpaper_layer, NULL, NULL);
			else
				gdi_layer_flatten_with_clipping(active_layer, temp_layer, NULL, NULL);
		}
	}

	gdi_layer_set_active(active_layer);

#else
	ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, NULL, NULL);
#endif
}

/**
 * Pen Event Notify
 * @ pen_event: 0 - Pen Down, 1 - Pen Up, 2 - Pen Move
 */
void ZMAEE_DWallPaper_PenEvent_Notify(int x, int y, int pen_event)
{
	switch(pen_event) {
		case 0:
			ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_DOWN, (unsigned long)x, (unsigned long)y);
			break;
		case 1:
			ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_UP, (unsigned long)x, (unsigned long)y);
			break;
		case 2:
			ZMAEE_Platform_HandleEvent(ZMAEE_EV_PEN_MOVE, (unsigned long)x, (unsigned long)y);
			break;
	}
}
#else
void ZMAEE_DWallPaper_SetMask(void)
{
}

void ZMAEE_DWallPaper_ClrMask(void)
{
}

int ZMAEE_DWallPaper_Validating(void)
{
	return 0;
}
#endif	// __ZMAEE_APP_DWALLPAPER__

/***********************************************************
  互动画面的Before View接口
***********************************************************/
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#ifdef __MMI_VUI_HOMESCREEN__

#ifdef __ZMAEE_APP_DWALLPAPER__
typedef struct {
	void	(*entry_func)(void);
}ZMAEE_VUIVIEW;

static ZMAEE_VUIVIEW sg_zmaee_vuiview = {0};

void ZMAEE_VUISwitch_Preview_LskHandle(void)
{	
	ZMAEE_SetThemeAppId(1, 0);
	DeleteScreenIfPresent(SCR_ZMAEE_VUIVIEW);

	if(sg_zmaee_vuiview.entry_func)
		sg_zmaee_vuiview.entry_func();
}

void ZMAEE_VUISwitch_Preview_RskHandle(void)
{	
	DeleteScreenIfPresent(SCR_ZMAEE_VUIVIEW);
	GoBackHistory();
}

void ZMAEE_VUISwitch_Preview_Show(void)
{
	EntryNewScreen(SCR_ZMAEE_VUIVIEW, NULL, ZMAEE_VUISwitch_Preview_Show, NULL);
	DisplayConfirm(STR_GLOBAL_YES, IMG_GLOBAL_YES, STR_GLOBAL_NO, IMG_GLOBAL_NO, 
					(UI_string_type)GetString(STR_ZMAEE_WPBVIEW_MSG), IMG_GLOBAL_QUESTION, 0);

	SetLeftSoftkeyFunction(ZMAEE_VUISwitch_Preview_LskHandle, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_VUISwitch_Preview_RskHandle, KEY_EVENT_UP);
}
#endif

int ZMAEE_VUISwitch_Preview_Entry(void (*EntryFunc)(void))
{
#ifdef __ZMAEE_APP_DWALLPAPER__
	unsigned int dwp_appid;

	dwp_appid = ZMAEE_GetThemeAppId(1);
	if(!dwp_appid)
		return 0;

	sg_zmaee_vuiview.entry_func = EntryFunc;
	ZMAEE_VUISwitch_Preview_Show();
	return 1;
#else
	return 0;
#endif
}
#endif
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x11A0)

void ZMAEE_ScreenRotate_Notify(void)
{
	if((GetActiveScreenId() == SCR_ZMAEE_MAIN)
	#ifdef __ZMAEE_APP_THEME__
		|| (
	#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
		(GetActiveScreenId() == MAIN_MENU_SCREENID) && 
	#endif
		(ZMAEE_CheckPlat_IsRunning() == 1)
		)
	#endif
		) {
		gdi_handle base_layer = 0;

		gdi_layer_get_base_handle(&base_layer);
		gdi_layer_push_and_set_active(base_layer);
		gdi_layer_resize(UI_device_width, UI_device_height);
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, 1, 0);
		gdi_layer_pop_and_restore_active();
	}
}

/***********************************************************
  APPMEM接口
***********************************************************/
typedef struct {
	int				valid;

	int				handle;
	unsigned short	app_name[AEE_MAX_APPLET_NAME / 2];	
}ZMAEE_APPMEM_CNTX;

static ZMAEE_APPMEM_CNTX sg_zmaee_appmem_cntx[AEE_MAX_GDLL_COUNT] = {0};

#ifndef ZMAEE_GDLL_USE_APPMEM
static unsigned int sg_zmaee_appmem_pool[ZMAEE_APPMEM_SIZE/4] = {0};
static int sg_zmaee_appmem_handle = 0;
static int sg_zmaee_appmem_pool_index = 0;
#endif

static
int ZMAEE_GetFreeAppMem(int handle)
{
	int i, j = -1;

	for(i = 0, j = -1; i < AEE_MAX_GDLL_COUNT; i++) {
		if(sg_zmaee_appmem_cntx[i].valid != 0 && sg_zmaee_appmem_cntx[i].handle == handle) {
			return i;
		}

		if(j == -1 && sg_zmaee_appmem_cntx[i].valid == 0) {
			j = i;
			continue;
		}			
	}

	return j;
}

static
int ZMAEE_GetAppMemByHdlr(int handle)
{
	int i;

	for(i = 0; i < AEE_MAX_GDLL_COUNT; i++) {
		if((sg_zmaee_appmem_cntx[i].valid != 0) && (sg_zmaee_appmem_cntx[i].handle == handle))
			return i;
	}

	return -1;
}

static
int ZMAEE_GetAppMemUsedCount(void)
{
	int i, count = 0;

	for(i = 0; i < AEE_MAX_GDLL_COUNT; i++) {
		if(sg_zmaee_appmem_cntx[i].valid != 0) {
			count++;
		}
	}

	return count;
}

static
void ZMAEE_AppMem1StopFunc(void)
{
	extern void ZMAEE_GlobalLibrary_Exit(int handle);
	ZMAEE_GlobalLibrary_Exit(sg_zmaee_appmem_cntx[0].handle);
	applib_mem_ap_notify_stop_finished(APPLIB_MEM_AP_ID_ZMAEE1, KAL_TRUE);
}

static
void ZMAEE_AppMem2StopFunc(void)
{
	extern void ZMAEE_GlobalLibrary_Exit(int handle);
	ZMAEE_GlobalLibrary_Exit(sg_zmaee_appmem_cntx[1].handle);
	applib_mem_ap_notify_stop_finished(APPLIB_MEM_AP_ID_ZMAEE2, KAL_TRUE);
}

static
void ZMAEE_AppMem3StopFunc(void)
{
	extern void ZMAEE_GlobalLibrary_Exit(int handle);
	ZMAEE_GlobalLibrary_Exit(sg_zmaee_appmem_cntx[2].handle);
	applib_mem_ap_notify_stop_finished(APPLIB_MEM_AP_ID_ZMAEE3, KAL_TRUE);
}

static
void ZMAEE_AppMem4StopFunc(void)
{
	extern void ZMAEE_GlobalLibrary_Exit(int handle);
	ZMAEE_GlobalLibrary_Exit(sg_zmaee_appmem_cntx[3].handle);
	applib_mem_ap_notify_stop_finished(APPLIB_MEM_AP_ID_ZMAEE4, KAL_TRUE);
}

static void (*sg_zmaee_appmem_stop_funcs[])(void) = {
	ZMAEE_AppMem1StopFunc,
	ZMAEE_AppMem2StopFunc,
	ZMAEE_AppMem3StopFunc,
	ZMAEE_AppMem4StopFunc
};

char* ZMAEE_GetAppString(unsigned short str_id)
{
	if((str_id >= STR_ZMAEE_APPMEM1) && (str_id <= STR_ZMAEE_APPMEM4)) {
		int idx = str_id - STR_ZMAEE_APPMEM1;

		if(sg_zmaee_appmem_cntx[idx].valid != 0) {
			return (char*)sg_zmaee_appmem_cntx[idx].app_name;
		}
	}

	return NULL;
}

void ZMAEE_MallocAppMem(void **pBuf, unsigned int *nBufLen, unsigned short *app_name, int handle)
{
	int idx;
	int ref = 0;

	if(!pBuf || !nBufLen || !app_name) {
		*pBuf = NULL;
		*nBufLen = 0;
		return;
	}

	idx = ZMAEE_GetFreeAppMem(handle);
	if(idx < 0) {
		*pBuf = NULL;
		*nBufLen = 0;
		return;		
	}

#ifdef ZMAEE_GDLL_USE_APPMEM
	applib_mem_ap_register(APPLIB_MEM_AP_ID_ZMAEE1 + idx, STR_ZMAEE_APPMEM1 + idx, NULL, sg_zmaee_appmem_stop_funcs[idx]);
	*pBuf = applib_mem_ap_alloc(APPLIB_MEM_AP_ID_ZMAEE1 + idx, *nBufLen);
	if(!(*pBuf)) {
		*nBufLen = 0;
		
		return;
	}
#else
	if(sg_zmaee_appmem_handle && handle != sg_zmaee_appmem_handle) {
		*pBuf = NULL;
		*nBufLen = 0;
		return;
	}

	*pBuf = ((unsigned char*)sg_zmaee_appmem_pool) + sg_zmaee_appmem_pool_index;
	sg_zmaee_appmem_pool_index += *nBufLen;
	sg_zmaee_appmem_handle = handle;
#endif

	ref = sg_zmaee_appmem_cntx[idx].valid;
	memset(&sg_zmaee_appmem_cntx[idx], 0, sizeof(ZMAEE_APPMEM_CNTX));
	sg_zmaee_appmem_cntx[idx].valid = ref + 1;
	sg_zmaee_appmem_cntx[idx].handle = handle;
	mmi_ucs2ncpy((S8*)sg_zmaee_appmem_cntx[idx].app_name, (const S8*)app_name, sizeof(sg_zmaee_appmem_cntx[idx].app_name) / 2 - 1);
}

void ZMAEE_FreeAppMem(void *pBuf, int handle)
{
	int idx;

	if(!pBuf)
		return;

	idx = ZMAEE_GetAppMemByHdlr(handle);
	if(idx < 0) {
		return;
	}

	sg_zmaee_appmem_cntx[idx].valid--;
	if(sg_zmaee_appmem_cntx[idx].valid == 0)
	{
#ifndef ZMAEE_GDLL_USE_APPMEM
		sg_zmaee_appmem_handle = 0;
		sg_zmaee_appmem_pool_index = 0;
#endif
	}

#ifdef ZMAEE_GDLL_USE_APPMEM
	applib_mem_ap_free(pBuf);
#endif
}

/***********************************************************
  系统主题接口
***********************************************************/
typedef struct {
	MMI_theme		theme;

	UI_filled_area  *Dow_background_filler;
	UI_filled_area	*Dow_normal_cell_filler;
	UI_filled_area	*Dow_normal_highlighted_cell_filler;
	UI_filled_area	*Dow_selected_highlighted_cell_filler;
	UI_filled_area	*Dow_selected_cell_filler;

	UI_filled_area	*list_menu_Dow_background_filler;
	UI_filled_area	*list_menu_Dow_normal_cell_filler;
	UI_filled_area	*list_menu_Dow_normal_highlighted_cell_filler;
	UI_filled_area	*list_menu_Dow_selected_highlighted_cell_filler;
	UI_filled_area	*list_menu_Dow_selected_cell_filler;

	UI_filled_area	*popup_bkg_filler;
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	UI_filled_area 	 sg_zmaee_softkey_bar_bkg_filler;
	UI_filled_area   sg_zmaee_title_filler;
#endif
	
	char			*global_warning_image;
	char			*global_question_image;
	char			*global_save_image;
	char			*global_delete_image;
	char			*global_default_image;
	char			*global_progress_image;
	char			*global_error_image;
	char			*global_info_image;
	char			*global_success_image;
	char			*global_fail_image;
}ZMAEE_SYSTHM_CNTX;

static ZMAEE_SYSTHM_CNTX sg_zmaee_systhm = {0};
static int sg_zmaee_main_menu_highlight_index = -1;
static void* sg_zmaee_theme_mem = 0;

void ZMAEE_SysTheme_MallocMem(void **ppBuf, unsigned int *nSize)
{
	if(!ppBuf || !nSize)
		return;

	if(sg_zmaee_theme_mem)
	{
		*nSize = ZMAEE_THEME_RAM_SIZE;
		*ppBuf = sg_zmaee_theme_mem;
		return;
	}

#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() < ZMAEE_THEME_RAM_SIZE)
	{
		*ppBuf = NULL;
		*nSize = 0;
		return;
	}

	*ppBuf = applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, ZMAEE_THEME_RAM_SIZE);
#else
	if(med_ext_left_size() < ZMAEE_THEME_RAM_SIZE) 
	{
		*ppBuf = NULL;
		*nSize = 0;
		return;
	}

	*ppBuf = med_alloc_ext_mem(ZMAEE_THEME_RAM_SIZE);
#endif
	*nSize = ZMAEE_THEME_RAM_SIZE;
	sg_zmaee_theme_mem = *ppBuf;
}

void ZMAEE_SysTheme_FreeMem(void *pBuf)
{
	if(pBuf)
	{
	#ifdef __MED_IN_ASM__
		applib_mem_ap_free(pBuf);
	#else
		med_free_ext_mem(&pBuf);
	#endif
		sg_zmaee_theme_mem = 0;
	}
}

void ZMAEE_SysTheme_Init(void **pptr)
{
	if(!pptr)
		return;

	*pptr = (void*)current_MMI_theme;

	memset(&sg_zmaee_systhm.theme, 0, sizeof(sg_zmaee_systhm.theme));
	memcpy(&sg_zmaee_systhm.theme, current_MMI_theme, sizeof(sg_zmaee_systhm.theme));

#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	sg_zmaee_systhm.sg_zmaee_softkey_bar_bkg_filler.flags = ZMAEE_FILLED_AREA_TYPE_NO_BACKGROUND;
	sg_zmaee_systhm.theme.softkey_bar_bkg_filler = &sg_zmaee_systhm.sg_zmaee_softkey_bar_bkg_filler;

	sg_zmaee_systhm.sg_zmaee_title_filler.flags = ZMAEE_FILLED_AREA_TYPE_NO_BACKGROUND;
	sg_zmaee_systhm.theme.title_filler = &sg_zmaee_systhm.sg_zmaee_title_filler;
#endif
}

void ZMAEE_SysTheme_Restore(void *ptr)
{
	if(!ptr)
		return;

	current_MMI_theme = (MMI_theme*)ptr;

	set_MMI_theme(current_MMI_theme);
	MMI_apply_current_theme();
	
#ifdef __ZMAEE_APP_THEME__
	{
		extern void ZMAEE_Init_IMUI(void);
		ZMAEE_Init_IMUI();
	}
#endif
	

	memset(&sg_zmaee_systhm, 0, sizeof(sg_zmaee_systhm));
}

void ZMAEE_SysTheme_Validate(void)
{
	set_MMI_theme(&sg_zmaee_systhm.theme);
	MMI_apply_current_theme();

#ifdef __ZMAEE_APP_THEME__
	{
		extern void ZMAEE_Init_IMUI(void);
		ZMAEE_Init_IMUI();
	}
#endif
}

int ZMAEE_SysTheme_Set(ZMAEE_THEME_ELEMTYPE elem_type, void *arg)
{
	if(!arg) {
		return E_ZM_AEE_FAILURE;
	}
#ifdef __ZMAEE_APP_THEME__
	switch(elem_type) {
		case ZMAEE_ELEM_INPUTBOX_FILLER:
			sg_zmaee_systhm.theme.inputbox_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SCROLLBAR_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.scrollbar_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SCROLLBAR_INDICATOR_FILLER:
			sg_zmaee_systhm.theme.scrollbar_indicator_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TITLE_FILLER:
			sg_zmaee_systhm.theme.title_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.list_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_NORMAL_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_normal_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_SELECTED_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_selected_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_DISABLED_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_disabled_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_SELECTED_ITEM_FILLER:
			sg_zmaee_systhm.theme.matrix_selected_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_FORMATTED_INPUTBOX_NORMAL_FILLER:
			sg_zmaee_systhm.theme.formatted_inputbox_normal_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_FORMATTED_INPUTBOX_SELECTED_FILLER:
			sg_zmaee_systhm.theme.formatted_inputbox_selected_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_NORMAL_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_main_menu_normal_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_SELECTED_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_main_menu_selected_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_DISABLED_ITEM_FILLER:
			sg_zmaee_systhm.theme.list_main_menu_disabled_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_MAIN_MENU_SELECTED_ITEM_FILLER:
			sg_zmaee_systhm.theme.matrix_main_menu_selected_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_GENERAL_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.general_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_DESCRIPTION_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.popup_description_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_NORMAL_ITEM_FILLER:
			sg_zmaee_systhm.theme.inline_list_normal_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_SELECTED_ITEM_FILLER:
			sg_zmaee_systhm.theme.inline_list_selected_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_DISABLED_ITEM_FILLER:
			sg_zmaee_systhm.theme.inline_list_disabled_item_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_FOCUSSED_FILLER:
			sg_zmaee_systhm.theme.inline_edit_focussed_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_UNFOCUSSED_FILLER:
			sg_zmaee_systhm.theme.inline_edit_unfocussed_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_NOBOUNDARY_FILLER:
			sg_zmaee_systhm.theme.inline_edit_noboundary_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_PIN_INPUTBOX_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.PIN_inputbox_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_PIN_SCREEN_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.PIN_screen_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_NETWORK_SCREEN_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.network_screen_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_CM_SCREEN_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.CM_screen_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.dialer_inputbox_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DOWN_FILLER:
			sg_zmaee_systhm.theme.virtual_keyboard_key_down_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SMALL_LIST_MENU_NORMAL_FILLER:
			sg_zmaee_systhm.theme.small_list_menu_normal_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SMALL_LIST_SCREEN_BORDER_FILLER:
			sg_zmaee_systhm.theme.small_list_screen_border_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LITE_DISP_SCR_BG_COLOR:
			sg_zmaee_systhm.theme.lite_disp_scr_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LSK_UP_TEXT_COLOR:
			sg_zmaee_systhm.theme.LSK_up_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LSK_DOWN_TEXT_COLOR:
			sg_zmaee_systhm.theme.LSK_down_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LSK_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.LSK_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_RSK_UP_TEXT_COLOR:
			sg_zmaee_systhm.theme.RSK_up_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_RSK_DOWN_TEXT_COLOR:
			sg_zmaee_systhm.theme.RSK_down_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_RSK_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.RSK_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.inputbox_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inputbox_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_SELECTOR_COLOR:
			sg_zmaee_systhm.theme.inputbox_selector_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_CURSOR_COLOR:
			sg_zmaee_systhm.theme.inputbox_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TITLE_TEXT_COLOR:
			sg_zmaee_systhm.theme.title_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TITLE_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.title_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TITLE_SHORTCUT_TEXT_COLOR:
			sg_zmaee_systhm.theme.title_shortcut_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_normal_text_color = (color*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (MT6252)
			sg_zmaee_systhm.theme.two_line_first_line_text_color = (color*)arg;
			sg_zmaee_systhm.theme.two_line_second_line_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_first_line_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_second_line_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_third_line_text_color = (color*)arg;	
		#endif
			break;

		case ZMAEE_ELEM_LIST_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_selected_text_color = (color*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (MT6252)
			sg_zmaee_systhm.theme.two_line_first_line_highlight_text_color = (color*)arg;
			sg_zmaee_systhm.theme.two_line_second_line_highlight_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_first_line_highlight_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_second_line_highlight_text_color = (color*)arg;
			sg_zmaee_systhm.theme.multirow_third_line_highlight_text_color = (color*)arg;
		#endif
			break;

		case ZMAEE_ELEM_LIST_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.matrix_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.matrix_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.matrix_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_FORMATTED_INPUTBOX_CURSOR_COLOR:
			sg_zmaee_systhm.theme.formatted_inputbox_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_main_menu_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_main_menu_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_MAIN_MENU_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.list_main_menu_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_MAIN_MENU_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.matrix_main_menu_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_MAIN_MENU_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.matrix_main_menu_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_MAIN_MENU_DISABLED_TEXT_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)	
			sg_zmaee_systhm.theme.matrix_main_menu_disabled_text_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_POPUP_SCREEN_BACKGROUND_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)	
			sg_zmaee_systhm.theme.popup_screen_background_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_POPUP_SCREEN_BORDER_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)		
			sg_zmaee_systhm.theme.popup_screen_border_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_POPUP_DESCRIPTION_TEXT_COLOR:
			sg_zmaee_systhm.theme.popup_description_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CM_SCREEN_TEXT_COLOR:
			sg_zmaee_systhm.theme.CM_screen_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_BACKGROUND_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_background_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_DURATION_BACKGROUND_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_duration_background_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_DATE_TEXT_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_date_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_TIME_TEXT_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_time_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_DURATION_TEXT_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_duration_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATETIME_BAR_AOC_TEXT_COLOR:
			sg_zmaee_systhm.theme.datetime_bar_AOC_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INFORMATION_BAR_COLOR:
			sg_zmaee_systhm.theme.information_bar_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUT_METHOD_TEXT_COLOR:
			sg_zmaee_systhm.theme.input_method_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_REMAINING_LENGTH_TEXT_COLOR:
			sg_zmaee_systhm.theme.remaining_length_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_FOCUSSED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_edit_focussed_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_UNFOCUSSED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_edit_unfocussed_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_edit_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_SELECTOR_COLOR:
			sg_zmaee_systhm.theme.inline_edit_selector_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_CURSOR_COLOR:
			sg_zmaee_systhm.theme.inline_edit_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_list_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_list_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INLINE_LIST_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.inline_list_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_PIN_INPUTBOX_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.PIN_inputbox_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_PIN_INPUTBOX_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.PIN_inputbox_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_PIN_INPUTBOX_SELECTOR_COLOR:
			sg_zmaee_systhm.theme.PIN_inputbox_selector_color = (color*)arg;
			break;

		case ZMAEE_ELEM_PIN_INPUTBOX_CURSOR_COLOR:
			sg_zmaee_systhm.theme.PIN_inputbox_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_SELECTOR_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_selector_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_CURSOR_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DOWN_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_down_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_UP_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_up_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_DEAD_KEY_DOWN_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_dead_key_down_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_DEAD_KEY_UP_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_dead_key_up_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_DISP_AREA_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_disp_area_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_DATE_COLOR:
			sg_zmaee_systhm.theme.idle_scr_date_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_TIME_COLOR:
			sg_zmaee_systhm.theme.idle_scr_time_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_DATE_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_date_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_TIME_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_time_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_NAME_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_name_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_NAME_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_name_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_STATUS_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_status_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_STATUS_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_status_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_EXTRA_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_extra_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_NETWORK_EXTRA_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_network_extra_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_SCROLLBAR_SIZE:
			sg_zmaee_systhm.theme.scrollbar_size = *((S32*)arg);
			break;

		case ZMAEE_ELEM_BG_OPACITY_FULL:
			sg_zmaee_systhm.theme.bg_opacity_full = *((S32*)arg);
			break;

		case ZMAEE_ELEM_BG_OPACITY_HIGH:
			sg_zmaee_systhm.theme.bg_opacity_high = *((S32*)arg);
			break;

		case ZMAEE_ELEM_BG_OPACITY_MEDIUM:
			sg_zmaee_systhm.theme.bg_opacity_medium = *((S32*)arg);
			break;

		case ZMAEE_ELEM_BG_OPACITY_LOW:
			sg_zmaee_systhm.theme.bg_opacity_low = *((S32*)arg);
			break;

		case ZMAEE_ELEM_MENUITEM_SINGLE_LINE_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.menuitem_single_line_highlight_filler = (UI_filled_area*)arg;
		#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
			sg_zmaee_systhm.theme.menuitem_two_line_highlight_filler = (UI_filled_area*)arg;
			sg_zmaee_systhm.theme.menuitem_two_line_highlight_without_scroll_filler = (UI_filled_area*)arg;
			sg_zmaee_systhm.theme.menuitem_multirow_highlight_filler = (UI_filled_area*)arg;
			sg_zmaee_systhm.theme.menuitem_multirow_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.menuitem_single_line_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
			break;

		case ZMAEE_ELEM_MENUITEM_TWO_LINE_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.menuitem_two_line_highlight_filler = (UI_filled_area*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.menuitem_two_line_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
			break;

		case ZMAEE_ELEM_MENUITEM_MULTIROW_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.menuitem_multirow_highlight_filler = (UI_filled_area*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.menuitem_multirow_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
			break;

		case ZMAEE_ELEM_MENUITEM_THICK_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.menuitem_thick_highlight_filler = (UI_filled_area*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.menuitem_thick_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
			break;

		case ZMAEE_ELEM_MENUITEM_THICK_WITH_TAB_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.menuitem_thick_with_tab_highlight_filler = (UI_filled_area*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.menuitem_thick_with_tab_highlight_without_scroll_filler = (UI_filled_area*)arg;
		#endif
			break;

		case ZMAEE_ELEM_SYMBOL_PICKER_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.symbol_picker_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.matrix_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MAIN_MENU_BKG_FILLER:
			sg_zmaee_systhm.theme.main_menu_bkg_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SUB_MENU_BKG_FILLER:
#if (UI_DEVICE_WIDTH < UI_DEVICE_HEIGHT)
			sg_zmaee_systhm.theme.sub_menu_bkg_filler = (UI_filled_area*)arg;
#else
			sg_zmaee_systhm.theme.rotated_bkg_filler = (UI_filled_area*)arg;
#endif
			break;

		case ZMAEE_ELEM_IDLE_BKG_FILLER:
			sg_zmaee_systhm.theme.idle_bkg_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_BKG_IMAGE:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.popup_bkg_image = (PU8)arg;
#endif
			break;

		case ZMAEE_ELEM_POPUP_TEXT_COLOR:
			sg_zmaee_systhm.theme.popup_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_WEEKDAY_COLOR:
			sg_zmaee_systhm.theme.idle_scr_weekday_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_WEEKDAY_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_weekday_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATE_TEXT_COLOR:
			sg_zmaee_systhm.theme.date_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DATE_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.date_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TIME_TEXT_COLOR:
			sg_zmaee_systhm.theme.time_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TIME_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.time_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ANALOG_HAND_HOUR_COLOR:
			sg_zmaee_systhm.theme.analog_hand_hour_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ANALOG_HAND_MIN_COLOR:
			sg_zmaee_systhm.theme.analog_hand_min_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ANALOG_AXIS_COLOR:
			sg_zmaee_systhm.theme.analog_axis_color = (color*)arg;
			break;

		case ZMAEE_ELEM_SOFTKEY_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.softkey_text_border_color = (color*)arg;
		#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
			sg_zmaee_systhm.theme.softkey_down_text_border_color = (color*)arg;
		#endif
			break;

		case ZMAEE_ELEM_SOFTKEY_BAR_BKG_FILLER:
			sg_zmaee_systhm.theme.softkey_bar_bkg_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TAB_TITLE_FILLER:
			sg_zmaee_systhm.theme.tab_title_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TAB_TITLE_TEXT_COLOR:
			sg_zmaee_systhm.theme.tab_title_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TAB_TITLE_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.tab_title_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TAB_TITLE_SHORTCUT_TEXT_COLOR:
			sg_zmaee_systhm.theme.tab_title_shortcut_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_TITLE_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_title_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_BORDER_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_VERTICAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_vertical_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_string_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CURRENT_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_current_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_ONE_TASK_COLOR:
			sg_zmaee_systhm.theme.calendar_one_task_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_MORE_TASK_COLOR:
			sg_zmaee_systhm.theme.calendar_more_task_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_MENSTRUAL_COLOR:
			sg_zmaee_systhm.theme.calendar_menstrual_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_PP_COLOR:
			sg_zmaee_systhm.theme.calendar_PP_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_DANGER_COLOR:
			sg_zmaee_systhm.theme.calendar_danger_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INVALID_COLOR:
			sg_zmaee_systhm.theme.calendar_invalid_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_VALID_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_valid_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INVALID_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_invalid_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.calendar_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INFOBOX_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_infobox_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INFOBOX_ENTRY_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_infobox_entry_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INFOBOX_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_infobox_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_STATUS_ICON_BAR_FILLER:
			sg_zmaee_systhm.theme.status_icon_bar_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_THICK_TITLE_FILLER:
			sg_zmaee_systhm.theme.thick_title_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_THICK_TITLE_TEXT_COLOR:
			sg_zmaee_systhm.theme.thick_title_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_THICK_TITLE_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.thick_title_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_THICK_TITLE_SHORTCUT_TEXT_COLOR:
			sg_zmaee_systhm.theme.thick_title_shortcut_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ROTATED_BKG_FILLER:
#if (UI_DEVICE_WIDTH < UI_DEVICE_HEIGHT)
			sg_zmaee_systhm.theme.rotated_bkg_filler = (UI_filled_area*)arg;
#else
			sg_zmaee_systhm.theme.sub_menu_bkg_filler = (UI_filled_area*)arg;
#endif
			break;

		case ZMAEE_ELEM_ROTATED_POPUP_BKG_IMAGE:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.rotated_popup_bkg_image = (PU8)arg;
#endif
			break;

		case ZMAEE_ELEM_ROTATED_TITLE_FILLER:
			sg_zmaee_systhm.theme.rotated_title_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SMALL_SCREEN_TITLE_FILLER:
			sg_zmaee_systhm.theme.small_screen_title_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TAB_NORMAL_TAB_FILLER:
			sg_zmaee_systhm.theme.tab_normal_tab_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TAB_HIGHLIGHT_TAB_FILLER:
			sg_zmaee_systhm.theme.tab_highlight_tab_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TAB_BLINK_TAB_FILLER:
			sg_zmaee_systhm.theme.tab_blink_tab_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MAIN_MENU_BIDEGREE_TAB_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.main_menu_bidegree_tab_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MATRIX_MAIN_MENU_HIGHLIGHT_IMAGE:
			sg_zmaee_systhm.theme.matrix_main_menu_highlight_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_MAIN_MENU_BIDEGREE_TAB_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.main_menu_bidegree_tab_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_SEPARATOR_COLOR:
			sg_zmaee_systhm.theme.list_separator_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SEPARATOR_COLOR:
			sg_zmaee_systhm.theme.cascade_menu_separator_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_NORMAL_TEXT_COLOR:
			sg_zmaee_systhm.theme.cascade_menu_normal_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SELECTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.cascade_menu_selected_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.cascade_menu_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_BORDER_COLOR:
			sg_zmaee_systhm.theme.cascade_menu_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.cascade_menu_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.cascade_menu_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SUBMENU_LEFT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.cascade_menu_submenu_left_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SUBMENU_RIGHT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.cascade_menu_submenu_right_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_LIST_CHECK_SELECTED_IMAGE:
			sg_zmaee_systhm.theme.list_check_selected_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_LIST_CHECK_UNSELECTED_IMAGE:
			sg_zmaee_systhm.theme.list_check_unselected_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_LIST_RADIO_SELECTED_IMAGE:
			sg_zmaee_systhm.theme.list_radio_selected_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_LIST_RADIO_UNSELECTED_IMAGE:
			sg_zmaee_systhm.theme.list_radio_unselected_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_LEFT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.inline_edit_left_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_INLINE_EDIT_RIGHT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.inline_edit_right_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_BASE_LINE_COLOR:
			sg_zmaee_systhm.theme.inputbox_base_line_color = (color*)arg;
			break;

		case ZMAEE_ELEM_INPUTBOX_EMS_CR_BASE_LINE_COLOR:
			sg_zmaee_systhm.theme.inputbox_ems_cr_base_line_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_TEXT_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DIALER_INPUTBOX_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.dialer_inputbox_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_SOFTKEY_LSK_DOWN_FILLER:
			sg_zmaee_systhm.theme.softkey_lsk_down_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SOFTKEY_LSK_UP_FILLER:
			sg_zmaee_systhm.theme.softkey_lsk_up_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SOFTKEY_RSK_DOWN_FILLER:
			sg_zmaee_systhm.theme.softkey_rsk_down_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SOFTKEY_RSK_UP_FILLER:
			sg_zmaee_systhm.theme.softkey_rsk_up_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_TITLE_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_title_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_string_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_VERTICAL_SELECT_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_vertical_select_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_ARRAY_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_array_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_FRAME_LINE_COLOR:
			sg_zmaee_systhm.theme.calendar_frame_line_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_LINE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_line_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_CURRENT_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_current_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_POPUP_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_popup_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_POPUP_BG_COLOR:
			sg_zmaee_systhm.theme.calendar_popup_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_POPUP_BORDER_COLOR:
			sg_zmaee_systhm.theme.calendar_popup_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_INFOBOX_BG_ALPHA:
			sg_zmaee_systhm.theme.calendar_infobox_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_TITLE_BG_ALPHA:
			sg_zmaee_systhm.theme.calendar_title_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_BG_ALPHA:
			sg_zmaee_systhm.theme.calendar_horizontal_string_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_VERTICAL_SELECT_BG_ALPHA:
			sg_zmaee_systhm.theme.calendar_vertical_select_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_ARRAY_BG_ALPHA:
			sg_zmaee_systhm.theme.calendar_cell_array_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_FRAME_LINE_ALPHA:
			sg_zmaee_systhm.theme.calendar_frame_line_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_TEXT_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_string_text_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_BG_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_horizontal_string_bg_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_VALID_TEXT_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_valid_text_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_INVALID_TEXT_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_invalid_text_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_ARRAY_BG_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_array_bg_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_CURRENT_TEXT_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_current_text_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_CURRENT_BG_FOR_IDLE_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_cell_current_bg_for_idle_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_HIGHLIGHT_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_highlight_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_FRAME_LINE_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_frame_line_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_LINE_FOR_IDLE_COLOR:
			sg_zmaee_systhm.theme.calendar_cell_line_for_idle_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_BG_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_horizontal_string_bg_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_HORIZONTAL_STRING_TEXT_FOR_IDLE_ALPHA:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_horizontal_string_text_for_idle_alpha = *((S32*)arg);
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_VALID_TEXT_FOR_IDLE_ALPHA:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_cell_valid_text_for_idle_alpha = *((S32*)arg);
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_ARRAY_BG_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_cell_array_bg_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_CURRENT_TEXT_FOR_IDLE_ALPHA:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_cell_current_text_for_idle_alpha = *((S32*)arg);
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_CURRENT_BG_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_cell_current_bg_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_HIGHLIGHT_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_cell_highlight_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_FRAME_LINE_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_frame_line_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_CELL_LINE_FOR_IDLE_ALPHA:
			sg_zmaee_systhm.theme.calendar_cell_line_for_idle_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_ARROW_INDICATION_COLOR:
			sg_zmaee_systhm.theme.arrow_indication_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ARROW_INDICATION_BORDER_COLOR:
			sg_zmaee_systhm.theme.arrow_indication_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_IDLE_BKG_FILLER:
			sg_zmaee_systhm.theme.dcd_idle_bkg_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_DCD_IDLE_READ_TEXT_COLOR:
			sg_zmaee_systhm.theme.dcd_idle_read_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_IDLE_UNREAD_TEXT_COLOR:
			sg_zmaee_systhm.theme.dcd_idle_unread_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_CHANNEL_LIST_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.dcd_channel_list_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_DCD_STORY_TEXT_COLOR:
			sg_zmaee_systhm.theme.dcd_story_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_STORY_VISITED_TEXT_COLOR:
			sg_zmaee_systhm.theme.dcd_story_visited_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_STORY_UNVISITED_TEXT_COLOR:
			sg_zmaee_systhm.theme.dcd_story_unvisited_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_DCD_STORY_HIGHLIGHT_BAR_COLOR:
			sg_zmaee_systhm.theme.dcd_story_highlight_bar_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_SIM2_NETWORK_NAME_COLOR:
			sg_zmaee_systhm.theme.idle_scr_sim2_network_name_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_SIM2_NETWORK_NAME_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_sim2_network_name_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_SIM2_NETWORK_STATUS_COLOR:
			sg_zmaee_systhm.theme.idle_scr_sim2_network_status_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IDLE_SCR_SIM2_NETWORK_STATUS_BORDER_COLOR:
			sg_zmaee_systhm.theme.idle_scr_sim2_network_status_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_POPUP_FULL_HEIGHT_BKG_IMAGE:
			#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
			sg_zmaee_systhm.theme.popup_full_height_bkg_image = (PU8)arg;
			#endif
			break;

		case ZMAEE_ELEM_POPUP_CONTENT_TEXT_COLOR:
			sg_zmaee_systhm.theme.popup_content_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MAIN_MENU_TITLE_COLOR:
			sg_zmaee_systhm.theme.main_menu_title_color =(color*)arg;
			break;

		case ZMAEE_ELEM_MAIN_MENU_TITLE_BORDER_COLOR:
			sg_zmaee_systhm.theme.main_menu_title_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_UNDERLINE_COLOR:
			sg_zmaee_systhm.theme.ime_underline_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_INDICATOR_COLOR:
			sg_zmaee_systhm.theme.ime_indicator_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_FOCUSED_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.ime_focused_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_IME_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.ime_background_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_IME_FOCUSED_BORDER_COLOR:
			sg_zmaee_systhm.theme.ime_focused_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_BORDER_COLOR:
			sg_zmaee_systhm.theme.ime_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_SEPARATOR_WIDTH:
			sg_zmaee_systhm.theme.ime_separator_width = *((S32*)arg);
			break;

		case ZMAEE_ELEM_IME_SEPARATOR_NORMAL_COLOR:
			sg_zmaee_systhm.theme.ime_separator_normal_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_FOCUSED_SEPARATOR_COLOR:
			sg_zmaee_systhm.theme.ime_focused_separator_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_TEXT_SPACE:
			sg_zmaee_systhm.theme.ime_text_space = *((S32*)arg);
			break;

		case ZMAEE_ELEM_IME_LABEL_COLOR:
			sg_zmaee_systhm.theme.ime_label_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_CURSOR_COLOR:
			sg_zmaee_systhm.theme.ime_cursor_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_ARROW_DISABLE_COLOR:
			sg_zmaee_systhm.theme.ime_arrow_disable_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_ARROW_DOWN_COLOR:
			sg_zmaee_systhm.theme.ime_arrow_down_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_ARROW_UP_COLOR:
			sg_zmaee_systhm.theme.ime_arrow_up_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_CAND_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_cand_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_CAND_HIGHLIGHTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_cand_highlighted_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_CAND_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.ime_cand_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_COMP_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_comp_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_COMP_HIGHLIGHTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_comp_highlighted_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_COMP_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.ime_comp_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_INPUT_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_input_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_INPUT_HIGHLIGHTED_TEXT_COLOR:
			sg_zmaee_systhm.theme.ime_input_highlighted_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_INPUT_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.ime_input_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_STROKE_COLOR:
			sg_zmaee_systhm.theme.ime_stroke_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_HIGHLIGHTED_STROKE_COLOR:
			sg_zmaee_systhm.theme.ime_highlighted_stroke_color = (color*)arg;
			break;

		case ZMAEE_ELEM_IME_STROKE_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.ime_stroke_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TAB_SPECIAL_FILLER:
			sg_zmaee_systhm.theme.tab_special_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MINI_INDICATOR_BG_COLOR:
			sg_zmaee_systhm.theme.mini_indicator_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MINI_INDICATOR_NORMAL_COLOR:
			sg_zmaee_systhm.theme.mini_indicator_normal_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MINI_INDICATOR_TAB_COLOR:
			sg_zmaee_systhm.theme.mini_indicator_tab_color = (color*)arg;
			break;

		case ZMAEE_ELEM_STATUS_ICON_BAR_TIME_BORDER_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.status_icon_bar_time_border_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_STATUS_ICON_BAR_TIME_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.status_icon_bar_time_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_MEDIA_PLAYER_CONTENT_TEXT_COLOR:
			sg_zmaee_systhm.theme.media_player_content_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MEDIA_PLAYER_CONTENT_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.media_player_content_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_FIRST_LINE_TEXT_COLOR:
			sg_zmaee_systhm.theme.two_line_first_line_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_FIRST_LINE_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.two_line_first_line_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_SECOND_LINE_TEXT_COLOR:
			sg_zmaee_systhm.theme.two_line_second_line_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_SECOND_LINE_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.two_line_second_line_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_SECOND_LINE_SELECTOR_FILLER:
			sg_zmaee_systhm.theme.two_line_second_line_selector_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_TWO_LINE_SECOND_LINE_SELECTOR_TEXT_COLOR:
			sg_zmaee_systhm.theme.two_line_second_line_selector_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_FIRST_LINE_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_first_line_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_FIRST_LINE_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_first_line_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_SECOND_LINE_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_second_line_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_SECOND_LINE_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_second_line_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_THIRD_LINE_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_third_line_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_MULTIROW_THIRD_LINE_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.multirow_third_line_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_GRID_LINE_COLOR:
			sg_zmaee_systhm.theme.list_grid_line_color = (color*)arg;
			break;

		case ZMAEE_ELEM_LIST_GRID_LINE_ALPHA:
			sg_zmaee_systhm.theme.list_grid_line_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_MENUITEM_SINGLE_LINE_HIGHLIGHT_WITHOUT_SCROLL_FILLER:
			sg_zmaee_systhm.theme.menuitem_single_line_highlight_without_scroll_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MENUITEM_TWO_LINE_HIGHLIGHT_WITHOUT_SCROLL_FILLER:
			sg_zmaee_systhm.theme.menuitem_two_line_highlight_without_scroll_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MENUITEM_MULTIROW_HIGHLIGHT_WITHOUT_SCROLL_FILLER:
			sg_zmaee_systhm.theme.menuitem_multirow_highlight_without_scroll_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MENUITEM_THICK_HIGHLIGHT_WITHOUT_SCROLL_FILLER:
			sg_zmaee_systhm.theme.menuitem_thick_highlight_without_scroll_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_MENUITEM_THICK_WITH_TAB_HIGHLIGHT_WITHOUT_SCROLL_FILLER:
			sg_zmaee_systhm.theme.menuitem_thick_with_tab_highlight_without_scroll_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_DIALING_SCREEN_BG_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_bg_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_DIALING_SCREEN_SKEY_UP_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_skey_up_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_DIALING_SCREEN_SKEY_DOWN_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_skey_down_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_DIALING_SCREEN_LKEY_UP_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_lkey_up_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_DIALING_SCREEN_LKEY_DOWN_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_lkey_down_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_VOIP_DIALER_EDITOR_TEXT_COLOR:
			sg_zmaee_systhm.theme.voip_dialer_editor_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_HEADER_INFORMATION_TEXT_COLOR:
			sg_zmaee_systhm.theme.header_information_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CM_SCREEN_POPUP_BKG_IMAGE:
			sg_zmaee_systhm.theme.CM_screen_popup_bkg_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_LSK_NORMAL_UP_FILLER:
			sg_zmaee_systhm.theme.popup_embedded_lsk_normal_up_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_LSK_NORMAL_DOWN_FILLER:
			sg_zmaee_systhm.theme.popup_embedded_lsk_normal_down_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_RSK_NORMAL_UP_FILLER:
			sg_zmaee_systhm.theme.popup_embedded_rsk_normal_up_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_RSK_NORMAL_DOWN_FILLER:
			sg_zmaee_systhm.theme.popup_embedded_rsk_normal_down_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_SOFTKEY_TEXT_COLOR:
			sg_zmaee_systhm.theme.popup_embedded_softkey_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_POPUP_EMBEDDED_SOFTKEY_TEXT_BORDER_COLOR:
			sg_zmaee_systhm.theme.popup_embedded_softkey_text_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_POPUP_SOFTKEY_BAR_FILLER:
			sg_zmaee_systhm.theme.popup_softkey_bar_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SMALL_LIST_MENU_HIGHLIGHT_FILLER:
			sg_zmaee_systhm.theme.small_list_menu_highlight_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_SMALL_LIST_MENU_HIGHLIGHT_TEXT_COLOR:
			sg_zmaee_systhm.theme.small_list_menu_highlight_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_SMALL_LIST_MENU_TEXT_COLOR:
			sg_zmaee_systhm.theme.small_list_menu_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_IDLE_LARGE_CURRENT_DAY_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_idle_large_current_day_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_IDLE_LARGE_CURRENT_DAY_TEXT_ALPHA:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_idle_large_current_day_text_alpha = *((S32*)arg);
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_IDLE_LARGE_CURRENT_DATE_TEXT_COLOR:
			sg_zmaee_systhm.theme.calendar_idle_large_current_date_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_CALENDAR_IDLE_LARGE_CURRENT_DATE_TEXT_ALPHA:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_idle_large_current_date_text_alpha = *((S32*)arg);
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_INFOBOX_ENTRY_ALPHA:
			sg_zmaee_systhm.theme.calendar_infobox_entry_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_CALENDAR_CURRENT_BG_IMAGE:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.calendar_current_bg_image = (PU8)arg;
#endif
			break;

		case ZMAEE_ELEM_CALENDAR_IDLE_BG_IMAGE:
			sg_zmaee_systhm.theme.calendar_idle_bg_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_ICON_BAR_HINT_TEXT_COLOR:
			sg_zmaee_systhm.theme.icon_bar_hint_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ICON_BAR_HINT_FILLER:
			sg_zmaee_systhm.theme.icon_bar_hint_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_ALPHABET_BAR_HIGHLIGHT_COLOR:
			sg_zmaee_systhm.theme.alphabet_bar_highlight_color = (color*)arg;
			break;

		case ZMAEE_ELEM_ALPHABET_BAR_HIGHLIGHT_ALPHA:
			sg_zmaee_systhm.theme.alphabet_bar_highlight_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLED_BG_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disabled_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLED_BG_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disabled_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLED_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disabled_text_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_LARGE_ICON_LEFT_IMAGE:
			sg_zmaee_systhm.theme.virtual_keyboard_key_large_icon_left_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_LARGE_ICON_RIGHT_IMAGE:
			sg_zmaee_systhm.theme.virtual_keyboard_key_large_icon_right_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_INFORMATION_BAR_ALPHA:
			sg_zmaee_systhm.theme.information_bar_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_SPECIAL_SCREEN_BKG_FILLER:
			sg_zmaee_systhm.theme.special_screen_bkg_filler = (UI_filled_area*)arg;
			break;

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
		case ZMAEE_ELEM_SMALL_LIST_MENU_HIGHLIGHT_WITHOUT_SCROLLBAR_FILLER:
			sg_zmaee_systhm.theme.small_list_menu_highlight_without_scrollbar_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_BG_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_BG_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_bg_alpha = *((S32*)arg);
			break;	

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_BG_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_BG_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_BORDER_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_BORDER_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_border_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLE_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disable_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLE_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disable_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLE_BORDER_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disable_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_DISABLE_BORDER_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_disable_border_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_HIGHLIGHT_BG_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_highlight_bg_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_HIGHLIGHT_BG_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_highlight_bg_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_HIGHLIGHT_BORDER_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_highlight_border_color = (color*)arg;
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_HIGHLIGHT_BORDER_ALPHA:
			sg_zmaee_systhm.theme.virtual_keyboard_key_highlight_border_alpha = *((S32*)arg);
			break;

		case ZMAEE_ELEM_VIRTUAL_KEYBOARD_KEY_POPUP_HINT_TEXT_COLOR:
			sg_zmaee_systhm.theme.virtual_keyboard_key_popup_hint_text_color = (color*)arg;
			break;	

		case ZMAEE_ELEM_DIALING_SCREEN_LKEY_DISABLE_IMAGE:
			sg_zmaee_systhm.theme.dialing_screen_lkey_disable_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_IME_INFORMATION_BAR_BACKGROUND_FILLER:
			sg_zmaee_systhm.theme.ime_information_bar_background_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_INLINE_DATATIME_FOCUSSED_FILLER:
			sg_zmaee_systhm.theme.inline_datatime_focussed_filler = (UI_filled_area*)arg;
			break;	

		case ZMAEE_ELEM_INLINE_DATATIME_UNFOCUSSED_FILLER:
			sg_zmaee_systhm.theme.inline_datatime_unfocussed_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_ROTATED_POPUP_SCREEN_BACKGROUND_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.rotated_popup_screen_background_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_ROTATED_POPUP_SCREEN_BORDER_COLOR:
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
			sg_zmaee_systhm.theme.rotated_popup_screen_border_color = (color*)arg;
#endif
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SUBMENU_HIGHLIGHT_LEFT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.cascade_menu_submenu_highlight_left_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_CASCADE_MENU_SUBMENU_HIGHLIGHT_RIGHT_ARROW_IMAGE:
			sg_zmaee_systhm.theme.cascade_menu_submenu_highlight_right_arrow_image = (PU8)arg;
			break;

		case ZMAEE_ELEM_UCE_SCREEN_SUBJECT_COLOR:
			sg_zmaee_systhm.theme.uce_screen_subject_color = (color*)arg;
			break;
			
#endif			

		case ZMAEE_ELEM_DOW_BACKGROUND_FILLER:
			sg_zmaee_systhm.Dow_background_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_DOW_NORMAL_CELL_FILLER:
			sg_zmaee_systhm.Dow_normal_cell_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_DOW_NORMAL_HIGHLIGHTED_CELL_FILLER:
			sg_zmaee_systhm.Dow_normal_highlighted_cell_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_DOW_SELECTED_HIGHLIGHTED_CELL_FILLER:
			sg_zmaee_systhm.Dow_selected_highlighted_cell_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_DOW_SELECTED_CELL_FILLER:
			sg_zmaee_systhm.Dow_selected_cell_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_LIST_MENU_DOW_BACKGROUND_FILLER:
			sg_zmaee_systhm.list_menu_Dow_background_filler = (UI_filled_area*)arg;
			break;
			
 	  	case ZMAEE_ELEM_LIST_MENU_DOW_NORMAL_CELL_FILLER:
			sg_zmaee_systhm.list_menu_Dow_normal_cell_filler = (UI_filled_area*)arg;
		   	break;
			
		case ZMAEE_ELEM_LIST_MENU_DOW_NORMAL_HIGHLIGHTED_CELL_FILLER:
			sg_zmaee_systhm.list_menu_Dow_normal_highlighted_cell_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_LIST_MENU_DOW_SELECTED_HIGHLIGHTED_CELL_FILLER:
			sg_zmaee_systhm.list_menu_Dow_selected_highlighted_cell_filler = (UI_filled_area*)arg;
			break;
			
		case ZMAEE_ELEM_LIST_MENU_DOW_SELECTED_CELL_FILLER:
			sg_zmaee_systhm.list_menu_Dow_selected_cell_filler = (UI_filled_area*)arg;
			break;

		case ZMAEE_ELEM_GLOBAL_WARNING_IMAGE:
			sg_zmaee_systhm.global_warning_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_QUESTION_IMAGE:
			sg_zmaee_systhm.global_question_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_SAVE_IMAGE:
			sg_zmaee_systhm.global_save_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_DELETE_IMAGE:
			sg_zmaee_systhm.global_delete_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_DEFAULT_IMAGE:
			sg_zmaee_systhm.global_default_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_PROGRESS_IMAGE:
			sg_zmaee_systhm.global_progress_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_ERROR_IMAGE:
			sg_zmaee_systhm.global_error_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_INFO_IMAGE:
			sg_zmaee_systhm.global_info_image = (char*)arg;
			break;

		case ZMAEE_ELEM_GLOBAL_SUCCESS_IMAGE:
			sg_zmaee_systhm.global_success_image = (char*)arg;
			break;
			
		case ZMAEE_ELEM_GLOBAL_FAIL_IMAGE:
			sg_zmaee_systhm.global_fail_image = (char*)arg;
			break;

		case ZMAEE_ELEM_POPUP_BKG_FILLER:
			sg_zmaee_systhm.popup_bkg_filler = (UI_filled_area*)arg;
			break;

		default:
			return E_ZM_AEE_SUCCESS;
	}
#endif
	return E_ZM_AEE_SUCCESS;
}

UI_filled_area* ZMAEE_SysTheme_GetArea(ZMAEE_THEME_ELEMTYPE elem_type)
{
	if(ZMAEE_Theme_IsValidate()) {
		switch(elem_type) {
			case ZMAEE_ELEM_DOW_BACKGROUND_FILLER:
				return sg_zmaee_systhm.Dow_background_filler;
				
			case ZMAEE_ELEM_DOW_NORMAL_CELL_FILLER:
				return sg_zmaee_systhm.Dow_normal_cell_filler;
				
			case ZMAEE_ELEM_DOW_NORMAL_HIGHLIGHTED_CELL_FILLER:
				return sg_zmaee_systhm.Dow_normal_highlighted_cell_filler;
				
			case ZMAEE_ELEM_DOW_SELECTED_HIGHLIGHTED_CELL_FILLER:
				return sg_zmaee_systhm.Dow_selected_highlighted_cell_filler;
				
			case ZMAEE_ELEM_DOW_SELECTED_CELL_FILLER:
				return sg_zmaee_systhm.Dow_selected_cell_filler;

			case ZMAEE_ELEM_LIST_MENU_DOW_BACKGROUND_FILLER:
				return sg_zmaee_systhm.list_menu_Dow_background_filler;
				
	 	  	case ZMAEE_ELEM_LIST_MENU_DOW_NORMAL_CELL_FILLER:
				return sg_zmaee_systhm.list_menu_Dow_normal_cell_filler;
				
			case ZMAEE_ELEM_LIST_MENU_DOW_NORMAL_HIGHLIGHTED_CELL_FILLER:
				return sg_zmaee_systhm.list_menu_Dow_normal_highlighted_cell_filler;
				
			case ZMAEE_ELEM_LIST_MENU_DOW_SELECTED_HIGHLIGHTED_CELL_FILLER:
				return sg_zmaee_systhm.list_menu_Dow_selected_highlighted_cell_filler;
				
			case ZMAEE_ELEM_LIST_MENU_DOW_SELECTED_CELL_FILLER:
				return sg_zmaee_systhm.list_menu_Dow_selected_cell_filler;
		}
	}

	return NULL;
}

void ZMAEE_SysTheme_DrawPopupBkg(gdi_handle bkg_layer, int x, int y, int cx, int cy)
{
	if(sg_zmaee_systhm.popup_bkg_filler) {
		gdi_layer_push_and_set_active(bkg_layer);
		gui_draw_filled_area(x, y, 
			x + cx + 1,
			y + cy + 1,
			sg_zmaee_systhm.popup_bkg_filler);
		gdi_layer_pop_and_restore_active();
	}
}

#ifdef __ZMAEE_APP_THEME__
void ZMAEE_SysTheme_IdleNotify(void)
{
	sg_zmaee_main_menu_highlight_index = -1;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
int ZMAEE_SysTheme_IsMainMenuStart(void)
{
	ZM_AEECLSID clsId = 0;
	
	clsId = ZMAEE_GetThemeAppId(2);
	if(clsId == ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell())) {
		return E_ZM_AEE_TRUE;
	}

	return E_ZM_AEE_FALSE;
}
#endif
#endif

#if(ZM_AEE_MTK_SOFTVERN < 0x11A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0)
int ZMAEE_SysTheme_EntryMainMenu(void)
{
#ifdef __ZMAEE_APP_THEME__
	extern int ZMAEE_Theme_IsValidate(void);
	extern void ZMAEE_Theme_Start(void);
	extern void goto_main_menu(void);

	ZM_AEECLSID clsId = 0;
	int ret;

	ZMAEE_DebugPrint("ZMAEE_SysTheme_EntryMainMenu: sg_zmaee_restart_theme = %d", sg_zmaee_restart_theme);
	if(sg_zmaee_restart_theme == 1)
		return 0;

	// If is background running, send repaint message
	clsId = ZMAEE_GetThemeAppId(2);
	if(clsId == 0) {
		return 0;
	}	
	if(clsId == ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell())) {
		ZMAEE_RepaintWnd();
		return 1;
	}

	ret = ZMAEE_Theme_IsValidate();
	if(ret == 0) {
		// If haven't start theme, try to start it
		ZMAEE_Theme_Start();
		ret = ZMAEE_Theme_IsValidate();
	}
	if(ret == 1) {
		unsigned short appName[] = {0x4E3B, 0x83DC, 0x5355, 0x0000};
		ZMAEE_THEME_PARAM start_param = {&sg_zmaee_main_menu_highlight_index};
					
		if(!ZMAEE_IShell_SetWorkDir(ZMAEE_GetShell(), ZMAEE_ENTRY_THEMEDIR, 0xFFFFFFFF))
			return 0;
		
		ZMAEE_DebugPrint("ZMAEE_SysTheme_EntryMainMenu: clsId = %d", clsId);
		if(ZMAEE_IsAppletInROM(clsId) || ((ZMAEE_IShell_CanStartApplet(ZMAEE_GetShell(), clsId) == E_ZM_AEE_TRUE) && (ZMAEE_IShell_ValidateApplet(ZMAEE_GetShell(), clsId) == E_ZM_AEE_TRUE)))
		{
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		#if (defined(__MMI_KEYPAD_LOCK_PATTERN_2__) && !defined(__MMI_DISABLE_KEYPAD_LOCK__))
		    if (g_idle_context.ToMainMenuScrFromIdleApp == 1)
		    {
		    	extern void IdleHandleKeypadLockProcess(void);
		        StartTimer(KEYPAD_LOCK_TIMER, KEYPAD_LOCK_TIMEOUT, IdleHandleKeypadLockProcess);
			}
		#endif
		    
		#if !defined(__MMI_DISABLE_KEYPAD_LOCK__) && defined(__MMI_KEYPAD_LOCK_PATTERN_1__) && defined(__MMI_OP11_HOMESCREEN__)
		    if (g_idle_context.RskPressedFromIdleApp == 1 && mmi_hs_is_enable())
		    {
		    	extern void IdleHandleKeypadLockProcess(void);
		        StartTimer(KEYPAD_LOCK_TIMER, KEYPAD_LOCK_TIMEOUT, IdleHandleKeypadLockProcess);
		    }
		#endif
	#else
			StartTimer(KEYPAD_LOCK_TIMER, KEYPAD_LOCK_TIMEOUT, ZMAEE_IdleHandleKeypadLockProcess);
	#endif

			ZMAEE_AppletEntry(clsId, ZMAEE_ENTRY_THEMEDIR, &start_param, sizeof(start_param), (char*)appName, NULL, 0, 0, 0xFFFFFFFF, goto_main_menu, MAIN_MENU_SCREENID);
			sg_zmaee_mainmenu_keylock_flag = 1;
			return 1;
		}
	}
#endif

	return 0;
}
#endif


char* ZMAEE_SysTheme_GetPicData(unsigned short img_id)
{
	if((img_id >= IMG_GLOBAL_L1) && (img_id <= IMG_GLOBAL_L30)) {
		extern int ZMAEE_Theme_IsValidate(void);
		extern unsigned char* ZMAEE_Theme_GetNumPicData(int index);

		if(ZMAEE_Theme_IsValidate()) {
			return (char*)ZMAEE_Theme_GetNumPicData(img_id - IMG_GLOBAL_L1 + 1);
		}
	}

	if(ZMAEE_Theme_IsValidate()) {
		switch(img_id) {
			case IMG_GLOBAL_WARNING:
				return sg_zmaee_systhm.global_warning_image;

			case IMG_GLOBAL_QUESTION:
				return sg_zmaee_systhm.global_question_image;

			case IMG_GLOBAL_SAVE:
				return sg_zmaee_systhm.global_save_image;

			case IMG_GLOBAL_DELETED:
				return sg_zmaee_systhm.global_delete_image;

			case IMG_GLOBAL_DEFAULT:
				return sg_zmaee_systhm.global_default_image;

			case IMG_GLOBAL_PROGRESS:
				return sg_zmaee_systhm.global_progress_image;

			case IMG_GLOBAL_ERROR:
				return sg_zmaee_systhm.global_error_image;

			case IMG_GLOBAL_INFO:
				return sg_zmaee_systhm.global_info_image;

			case IMG_GLOBAL_SUCCESS:
				return sg_zmaee_systhm.global_success_image;

			case IMG_GLOBAL_FAIL:
				return sg_zmaee_systhm.global_fail_image;
		}
	}

	return NULL;
}

void ZMAEE_SysTheme_TranslateAreaFlags(unsigned int *ptr)
{
	UI_filled_area *area = (UI_filled_area*)ptr;
	unsigned int flags = 0;

	switch(area->flags & 0xFF) {
		case ZMAEE_FILLED_AREA_TYPE_COLOR:
			flags |= UI_FILLED_AREA_TYPE_COLOR;
			break;
			
		case ZMAEE_FILLED_AREA_TYPE_GRADIENT_COLOR:
			flags |= UI_FILLED_AREA_TYPE_GRADIENT_COLOR;
			break;

		case ZMAEE_FILLED_AREA_TYPE_TEXTURE:
			flags |= UI_FILLED_AREA_TYPE_TEXTURE;
			break;

		case ZMAEE_FILLED_AREA_TYPE_BITMAP:
			flags |= UI_FILLED_AREA_TYPE_BITMAP;
			break;

		case ZMAEE_FILLED_AREA_TYPE_HATCH_COLOR:
			flags |= UI_FILLED_AREA_TYPE_HATCH_COLOR;
			break;

		case ZMAEE_FILLED_AREA_TYPE_ALTERNATE_HATCH_COLOR:
			flags |= UI_FILLED_AREA_TYPE_ALTERNATE_HATCH_COLOR;
			break;

		case ZMAEE_FILLED_AREA_TYPE_CROSS_HATCH_COLOR:
			flags |= UI_FILLED_AREA_TYPE_CROSS_HATCH_COLOR;
			break;

		case ZMAEE_FILLED_AREA_TYPE_ALTERNATE_CROSS_HATCH_COLOR:
			flags |= UI_FILLED_AREA_TYPE_ALTERNATE_CROSS_HATCH_COLOR;
			break;

		case ZMAEE_FILLED_AREA_TYPE_NO_BACKGROUND:
			flags |= UI_FILLED_AREA_TYPE_NO_BACKGROUND;
			break;

		case ZMAEE_FILLED_AREA_TYPE_CUSTOM_FILL_TYPE1:
			flags |= UI_FILLED_AREA_TYPE_CUSTOM_FILL_TYPE1;
			break;

		case ZMAEE_FILLED_AREA_TYPE_CUSTOM_FILL_TYPE2:
			flags |= UI_FILLED_AREA_TYPE_CUSTOM_FILL_TYPE2;
			break;

		case ZMAEE_FILLED_AREA_TYPE_3D_BORDER:
			flags |= UI_FILLED_AREA_TYPE_3D_BORDER;
			break;
	}

	if((area->flags & ZMAEE_FILLED_AREA_VERTICAL_FILL) == ZMAEE_FILLED_AREA_VERTICAL_FILL) {
		flags |= UI_FILLED_AREA_VERTICAL_FILL;
	} else if((area->flags & ZMAEE_FILLED_AREA_VERTICAL_FILL) == ZMAEE_FILLED_AREA_HORIZONTAL_FILL) {
		flags |= UI_FILLED_AREA_HORIZONTAL_FILL;
	}

	if((area->flags & ZMAEE_FILLED_AREA_FLIP_FILL) == ZMAEE_FILLED_AREA_FLIP_FILL) {
		flags |= UI_FILLED_AREA_FLIP_FILL;
	}

	if((area->flags & ZMAEE_FILLED_AREA_DOUBLE_BORDER) == ZMAEE_FILLED_AREA_DOUBLE_BORDER) {
		flags |= UI_FILLED_AREA_DOUBLE_BORDER;
	} else if((area->flags & ZMAEE_FILLED_AREA_SINGLE_BORDER) == ZMAEE_FILLED_AREA_SINGLE_BORDER) {
		flags |= UI_FILLED_AREA_SINGLE_BORDER;
	}

	if((area->flags & ZMAEE_FILLED_AREA_3D_BORDER) == ZMAEE_FILLED_AREA_3D_BORDER) {
		flags |= UI_FILLED_AREA_3D_BORDER;
	}

	if((area->flags & ZMAEE_FILLED_AREA_ROUNDED_BORDER) == ZMAEE_FILLED_AREA_ROUNDED_BORDER) {
		flags |= UI_FILLED_AREA_ROUNDED_BORDER;
	}

	if((area->flags & ZMAEE_FILLED_AREA_ELEVATED_BORDER) == ZMAEE_FILLED_AREA_ELEVATED_BORDER) {
		flags |= UI_FILLED_AREA_ELEVATED_BORDER;
	} else if((area->flags & ZMAEE_FILLED_AREA_ELEVATED_BORDER) == ZMAEE_FILLED_AREA_DEPRESSED_BORDER) {
		flags |= UI_FILLED_AREA_DEPRESSED_BORDER;
	}

	if((area->flags & ZMAEE_FILLED_AREA_SHADOW_DOUBLE_LINE) == ZMAEE_FILLED_AREA_SHADOW_DOUBLE_LINE) {
		flags |= UI_FILLED_AREA_SHADOW_DOUBLE_LINE;
	} else if((area->flags & ZMAEE_FILLED_AREA_SHADOW) == ZMAEE_FILLED_AREA_SHADOW) {
		flags |= UI_FILLED_AREA_SHADOW;
	}

	if((area->flags & ZMAEE_FILLED_AREA_LEFT_ROUNDED_BORDER) == ZMAEE_FILLED_AREA_LEFT_ROUNDED_BORDER) {
		flags |= UI_FILLED_AREA_LEFT_ROUNDED_BORDER;
	} else if((area->flags & ZMAEE_FILLED_AREA_RIGHT_ROUNDED_BORDER) == ZMAEE_FILLED_AREA_RIGHT_ROUNDED_BORDER) {
		flags |= UI_FILLED_AREA_RIGHT_ROUNDED_BORDER;
	}

	if((area->flags & ZMAEE_FILLED_AREA_NO_VERTICAL_LINE) == ZMAEE_FILLED_AREA_NO_VERTICAL_LINE) {
		flags |= UI_FILLED_AREA_NO_VERTICAL_LINE;
	}
	if((area->flags & ZMAEE_FILLED_AREA_TYPE_TRANSPARENT_COLOR) == ZMAEE_FILLED_AREA_TYPE_TRANSPARENT_COLOR) {
		flags |= UI_FILLED_AREA_TYPE_TRANSPARENT_COLOR;
	}

	area->flags = flags;
}

char* ZMAEE_SysTheme_GetThemeWorkDir(void)
{
	return ZMAEE_ENTRY_THEMEDIR;
}

char* ZMAEE_GetRootDir(void)
{
	return ZMAEE_WORK_ROOT_DIR;
}

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
void ZMAEE_APP_KeyLock_Entry(void)
{
	ZMAEE_AppletEntry(ZMAEE_KEYLOCK_CLSID, ZMAEE_ENTRY_THEMEDIR, NULL, 0, (char*)GetString(STR_ZMAEE_KEYLOCK),
		NULL, 0, 0, ZMAEE_APPATTR_APPLET_KEYPADLOCK, NULL, 0);
}


void ZMAEE_APP_KeyLock_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_APP_KeyLock_Entry, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_APP_KeyLock_Entry, KEY_EVENT_UP);
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_APP_KeyLock_Entry, KEY_ENTER, KEY_EVENT_UP);
#endif
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
void ZMAEE_APP_DWPaper_Entry(void)
{
	ZMAEE_AppletEntry(ZMAEE_DWPAPER_CLSID, ZMAEE_ENTRY_THEMEDIR, NULL, 0, (char*)GetString(STR_ZMAEE_DWPAPER),
		NULL, 0, 0, ZMAEE_APPATTR_APPLET_IDLEAPP, NULL, 0);
}


void ZMAEE_APP_DWPaper_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_APP_DWPaper_Entry, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_APP_DWPaper_Entry, KEY_EVENT_UP);
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_APP_DWPaper_Entry, KEY_ENTER, KEY_EVENT_UP);
#endif
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}
#endif

#ifdef __ZMAEE_APP_THEME__
void ZMAEE_APP_Theme_Entry(void)
{
	ZMAEE_AppletEntry(ZMAEE_THEME_CLSID, ZMAEE_ENTRY_THEMEDIR, NULL, 0, (char*)GetString(STR_ZMAEE_THEME),
		NULL, 0, 0, ZMAEE_APPATTR_APPLET_MAINMENU, NULL, 0);
}


void ZMAEE_APP_Theme_Highlight(void)
{
	ChangeLeftSoftkey(STR_GLOBAL_OK, IMG_GLOBAL_OK);
	ChangeRightSoftkey(STR_GLOBAL_BACK, IMG_GLOBAL_BACK);

	SetKeyHandler(ZMAEE_APP_Theme_Entry, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_APP_Theme_Entry, KEY_EVENT_UP);
#if((ZM_AEE_MTK_SOFTVERN == 0x11B0) && (defined MT6252))
	SetKeyHandler(ZMAEE_APP_Theme_Entry, KEY_ENTER, KEY_EVENT_UP);
#endif
	
	SetKeyHandler(GoBackHistory, KEY_LEFT_ARROW, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
}
#endif

void ZMAEE_Clear_KeyLock_Flag(void)
{
#ifdef __ZMAEE_APP_THEME__
	{
		extern int sg_zmaee_mainmenu_keylock_flag;
		if(sg_zmaee_mainmenu_keylock_flag == 1)
			sg_zmaee_mainmenu_keylock_flag = 0;
	}
#endif
}

void ZMAEE_AppletEntry_OnEnter(void)
{
	#ifdef ZMAEE_CLOSE_THEME_BEFORE_ENTRY
	{
		extern void ZMAEE_Theme_Exit(void);
		ZMAEE_Theme_Exit();
	}
	#endif

	#ifdef __ZMAEE_APP_DESKTOP__
	{
		if(!sg_zmaee_dwp_set_mask) {
			extern void AEE_Desktop_Exit(void);
			AEE_Desktop_Exit();
		}
	}
	#endif
	
		#ifdef __ZMAEE_APP_TTS__
	{
		extern void zmaee_tts_stop_and_free_mem(void);
		zmaee_tts_stop_and_free_mem();
	}
	#endif
	
	#ifdef __ZMAEE_APP_TTS__
	{
		extern void zmaee_tts_stop_and_free_mem(void);
		extern int glb_zmaee_tts_stop;
		
		zmaee_tts_stop_and_free_mem();
		while(!glb_zmaee_tts_stop) {
			zmaee_tts_task_sleep(5);
		}
	}
	#endif

	ZMAEE_DebugPrint("On Enter System");
}

void ZMAEE_AppletExit_OnExit(void)
{
	#ifdef ZMAEE_CLOSE_THEME_BEFORE_ENTRY
	{
		extern void ZMAEE_Theme_Start(void);
		ZMAEE_Theme_Start();
	}
	#endif

	ZMAEE_Clear_KeyLock_Flag();

#ifdef __ZMAEE_APP_KEYPAD_LOCK__	
	if(g_zmaee_keylock_flag)
		g_zmaee_keylock_flag = 0;
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	ZMAEE_IDisplay_FreeWallpaperLayer();
#endif

	ZMAEE_DebugPrint("On Exit System");
}

int ZMAEE_CheckPlat_IsRunning(void)
{
	extern AEE_IApplet* ZMAEE_GetApplet(void);
	if(ZMAEE_GetApplet() != NULL)
		return 1;
	return 0;
}

#ifdef __ZMAEE_APP_DWALLPAPER__
void ZMAEE_DWallPaper_Restart(void)
{
#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() == IDLE_SCREEN_ID) {
		EntryNewScreen(0, NULL, NULL, NULL);
		GoBackHistory();
	}
#else
	if(mmi_frm_scrn_get_active_id() == IDLE_SCREEN_ID) {
		void zmaee_dummy_entry(void);
		mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
		mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);
		mmi_frm_scrn_enter(GRP_ID_ZMAEE, 0, NULL, zmaee_dummy_entry, MMI_FRM_FULL_SCRN);
		mmi_frm_scrn_close(GRP_ID_ZMAEE, 0);
	}
#endif
}
#endif

void ZMAEE_Repaint_Notify(void)
{
	if((GetActiveScreenId() == SCR_ZMAEE_MAIN) 
	#ifdef __ZMAEE_APP_THEME__
			|| (
	#if (ZM_AEE_MTK_SOFTVERN < 0x11A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0)
			(GetActiveScreenId() == MAIN_MENU_SCREENID) && 
	#endif
			(ZMAEE_CheckPlat_IsRunning() == 1)
			)
	#endif
		) {
		gdi_handle active_layer;

		gdi_layer_get_active(&active_layer);
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, 0, 0);
		gdi_layer_set_active(active_layer);
	}
}

#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
int ZMAEE_DWALLPAPER_ScreenSaver_Entry(void)
{
	ZM_AEECLSID clsId;
	AEE_IShell *shell_ptr;
	char start_param = 1;
	char appName[] = {0xA8, 0x52, 0x01, 0x60, 0x4C, 0x68, 0x03, 0x5E, 0x00, 0x00};

	ZMAEE_DebugPrint("ZMAEE_DWallPaper_Start: Entry, restart_theme = %d", sg_zmaee_restart_theme);
	
	if(sg_zmaee_restart_theme == 1)
		return 0;
	
	clsId = ZMAEE_GetThemeAppId(1);
	ZMAEE_DebugPrint("ZMAEE_DWallPaper_Start: clsId = %d", clsId);
	if((clsId == ZM_AEE_APPLET_CLSID_BASE) || (clsId > ZM_AEE_APPLET_CLSID_MAX)) {
		return 0;
	}	
	
	shell_ptr = ZMAEE_GetShell();
	if(!ZMAEE_IShell_SetWorkDir(shell_ptr, ZMAEE_ENTRY_THEMEDIR, 0)) {
		return 0;
	}

	if(ZMAEE_IShell_CanStartApplet(shell_ptr, clsId) && ZMAEE_IShell_ValidateApplet(shell_ptr, clsId)) {
		extern void ZMAEE_IDisplay_Refresh(AEE_IDisplay *po, int x, int y, int cx, int cy);
		
		ZMAEE_AppletEntry(clsId, ZMAEE_ENTRY_THEMEDIR, &start_param, sizeof(start_param), appName, NULL, 0, 1, 0xFFFFFFFF, NULL, 0);

		SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_DOWN);
		SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_UP);
		SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_LONG_PRESS);
		SetGroupKeyHandler(ZMAEE_KeyPressHandler, (PU16)PresentAllKeys, TOTAL_KEYS, KEY_EVENT_REPEAT);
		
#ifdef __MMI_TOUCH_SCREEN__
		wgui_register_pen_down_handler(ZMAEE_PenDownHandler);
		wgui_register_pen_up_handler(ZMAEE_PenUpHandler);
		wgui_register_pen_move_handler(ZMAEE_PenMoveHandler);
#endif

		if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) != 0) {
			sg_zmaee_start_screensaver = 1;
			ZMAEE_IDisplay_Refresh((AEE_IDisplay*)1, 0, 0, UI_device_width, UI_device_height);
			return 1;
		}
	}

	return 0;
}
#else
int ZMAEE_DWALLPAPER_ScreenSaver_Entry(void)
{
	ZM_AEECLSID clsId;
	AEE_IShell *shell_ptr;
	char start_param = 1;
	char appName[] = {0xA8, 0x52, 0x01, 0x60, 0x4C, 0x68, 0x03, 0x5E, 0x00, 0x00};

	if(sg_zmaee_restart_theme == 1) {
		return 0;
	}
	
	clsId = ZMAEE_GetThemeAppId(1);
	if(ZMAEE_DWallPaper_IsStart()) {
		clsId &= ~(0x80000000);
	}
	if((clsId == ZM_AEE_APPLET_CLSID_BASE) || (clsId > ZM_AEE_APPLET_CLSID_MAX)) {
		return 0;
	}	
	
	shell_ptr = ZMAEE_GetShell();
	if(!ZMAEE_IShell_SetWorkDir(shell_ptr, ZMAEE_ENTRY_THEMEDIR, 0)) {
		return 0;
	}

	if(ZMAEE_IShell_CanStartApplet(shell_ptr, clsId) && ZMAEE_IShell_ValidateApplet(shell_ptr, clsId)) {
		extern void ZMAEE_IDisplay_Refresh(AEE_IDisplay *po, int x, int y, int cx, int cy);

		ZMAEE_AppletEntry(clsId, ZMAEE_ENTRY_THEMEDIR, (void*)&start_param, sizeof(start_param), appName,
			NULL, 0, 0, 0, NULL, 0);
		if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) != 0) {
			sg_zmaee_start_screensaver = 1;
			ZMAEE_IDisplay_Refresh((AEE_IDisplay*)1, 0, 0, UI_device_width, UI_device_height);
			return 1;
		}
	}

	return 0;
}

#endif
#endif

#define ZMAEE_QQ_DLLPATH		"ipqq.dll"

char* ZMAEE_GetQQ_DllName(void)
{
	return ZMAEE_QQ_DLLPATH;
}

void ZMAEE_SetProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func)
{
	if(sim_idx == 0) {
		mmi_frm_set_protocol_event_handler(event, func, MMI_FALSE);
	} 
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
#ifdef __MMI_DUAL_SIM_MASTER__
	else if(sim_idx == 1) {
		mmi_frm_set_slave_protocol_event_handler(event, func, MMI_FALSE);
	}
#endif
#endif
}

void ZMAEE_ClearProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func)
{
	if(sim_idx == 0) {
		mmi_frm_clear_protocol_event_handler(event, func);
	} 
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
#ifdef __MMI_DUAL_SIM_MASTER__
	else if(sim_idx == 1) {
		mmi_frm_clear_slave_protocol_event_handler(event, func);
	}
#endif
#endif
}

void ZMAEE_StartApplet_PreHandle(void)
{
#ifdef __MMI_FRM_HISTORY__
	if((GetActiveScreenId() != SCR_ZMAEE_MAIN) && (IsScreenPresent(SCR_ZMAEE_MAIN))) {
		GoBackHistory();
	}
#else
	if((mmi_frm_scrn_get_active_id() != SCR_ZMAEE_MAIN) && (mmi_frm_scrn_is_present(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN, MMI_FRM_NODE_ALL_FLAG))) {
		mmi_frm_scrn_close_active_id();
	}
#endif
}

#if (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
#define MUSIC_FOLDER L"My Music\\"
#endif

/**
 * 获取系统目录的名字
 * @type				0 - photo, 1 - video, 2 - MP3 player
 * @dir_name			utf8 format directory name
 * RETURN:
 * E_ZM_AEE_BADPARAM		type is invalid or dir_name is null
 * E_ZM_AEE_FAILURE			获取失败
 * E_ZM_AEE_SUCCESS			获取成功
 */
int ZMAEE_GetSystemDirName(int type, char *dir_name, int max_len)
{
	unsigned short *ucs2_dir_name;

	switch(type) {
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0) || (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
		case 0:		
			ucs2_dir_name = FMGR_DEFAULT_FOLDER_PHOTOS;
			break;

		case 1:
			ucs2_dir_name = FMGR_DEFAULT_FOLDER_VIDEO;
			break;

		case 2:
			ucs2_dir_name = MUSIC_FOLDER;
			break;
#else
		case 0:		
			ucs2_dir_name = SHOOTING_DOWNLOAD_FOLDER_NAME;
			break;

		case 1:
			ucs2_dir_name = VIDEO_DOWNLOAD_FOLDER_NAME;
			break;

		case 2:
			ucs2_dir_name = AUDIO_DOWNLOAD_FOLDER_NAME;
			break;
#endif
		default:
			return E_ZM_AEE_BADPARAM;
	}

	ZMAEE_Ucs2_2_Utf8(ucs2_dir_name, mmi_ucs2strlen((char*)ucs2_dir_name), dir_name, max_len);
	if(dir_name[strlen(dir_name) - 1] == '\\')
		dir_name[strlen(dir_name) - 1] = 0;
	
	return E_ZM_AEE_SUCCESS;
}


/**************************************************************************************
list menu interface
**************************************************************************************/
#define ZMAEE_LISTMENU_MAX_HDLR_COUNT			3

typedef struct {
	int					menu_count;
	unsigned short		menu_item_name[ZMAEE_LISTMENU_MAX_COUNT][AEE_MAX_APPLET_NAME];
	ZMAEE_CALLBACK		menu_item_func[ZMAEE_LISTMENU_MAX_COUNT];
	ZMAEE_CALLBACK		menu_exit_func;
}ZMAEE_LISTMENU_ITEM;

typedef struct {
	ZMAEE_LISTMENU_ITEM 	list_menu[ZMAEE_LISTMENU_MAX_HDLR_COUNT];
	int						is_valid[ZMAEE_LISTMENU_MAX_HDLR_COUNT];
	int						highlight_item[ZMAEE_LISTMENU_MAX_HDLR_COUNT];
	int 					active_index;
}ZMAEE_LISTMENU_CNTX;

static ZMAEE_LISTMENU_CNTX sg_zmaee_listmenu_cntx = {0};


void ZMAEE_Show_ListMenu(int handle, int bHistory);


static
int ZMAEE_GetListMenuItem(int item_index, UI_string_type str_buff, PU8* img_buff_p, U8 str_img_mask)
{
	extern const U16 gIndexIconsImageList[];

	if(item_index < 0 || item_index >= ZMAEE_LISTMENU_MAX_COUNT) {
		return FALSE;
	}

	mmi_ucs2cpy((S8*)str_buff, (const S8*)sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_item_name[item_index]);
	*img_buff_p = get_image(gIndexIconsImageList[item_index]);

	return TRUE;
}


static
void ZMAEE_ListMenu_LeftSoftkeyFunction(void)
{
	int  hilighted_index = sg_zmaee_listmenu_cntx.highlight_item[sg_zmaee_listmenu_cntx.active_index];

	if(hilighted_index < 0 || hilighted_index >= ZMAEE_LISTMENU_MAX_COUNT) {
		return;
	}

	if(ZMAEE_Callback_Valid(&sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_item_func[hilighted_index])) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_item_func[hilighted_index].pfn;
		pfn(sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_item_func[hilighted_index].pUser);
	}
}

static
void ZMAEE_ListMenu_RightSoftkeyFunction(void)
{
	int hilighted_index = sg_zmaee_listmenu_cntx.highlight_item[sg_zmaee_listmenu_cntx.active_index];

#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() == SCR_ZMAEE_LISTMENU1 + sg_zmaee_listmenu_cntx.active_index) {
		GoBackHistory();
	} else {
		DeleteScreenIfPresent(SCR_ZMAEE_LISTMENU1 + sg_zmaee_listmenu_cntx.active_index);
	}
#else
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_LISTMENU1 + sg_zmaee_listmenu_cntx.active_index);
#endif
	
	if(ZMAEE_Callback_Valid(&sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_exit_func)) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_exit_func.pfn;
		pfn((void*)sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_item_func[hilighted_index].pUser);
	}
}



static 
void ZMAEE_ListMenu_EntryScreenFunction(void)
{
	if(sg_zmaee_listmenu_cntx.is_valid[sg_zmaee_listmenu_cntx.active_index] == 1) {
		ZMAEE_Show_ListMenu(sg_zmaee_listmenu_cntx.active_index | 0xF0, 1);
	}
}

static 
void ZMAEE_ListMenu_ExitScreenFunction(void)
{
	ExitCategory184Screen();
}



static
int ZMAEE_GetIndexFromListMenuHandle(int handle)
{
	int idx = handle & ~(0xF0);

	if(idx < 0 || idx >= ZMAEE_LISTMENU_MAX_HDLR_COUNT || sg_zmaee_listmenu_cntx.is_valid[idx] != 1)
		return E_ZM_AEE_NOTEXIST;
	else
		return idx;
}

/**
   * 创建一个系统列表菜单
   * @pListMenu						输入参数，列表菜单的内容
   * RETURN:
   * 	E_ZM_AEE_BADPARAM		参数有误
   * 	E_ZM_AEE_NOMEMORY		超出最多可创建的列表菜单个数
   * 	> 0									创建的列表菜单的句柄
   */
int ZMAEE_Create_ListMenu(ZMAEE_LISTMENU *pListMenu)
{
	int i, j;
	
	if(!pListMenu || !pListMenu->menu_exit_func || 
		pListMenu->menu_count <= 0 || pListMenu->menu_count > ZMAEE_LISTMENU_MAX_COUNT)
		return E_ZM_AEE_BADPARAM;

	for(i = 0; i < ZMAEE_LISTMENU_MAX_HDLR_COUNT; i++) 
	{
		if(sg_zmaee_listmenu_cntx.is_valid[i] == 0)
			break;
	}

	if(i >= ZMAEE_LISTMENU_MAX_HDLR_COUNT) {
		return E_ZM_AEE_NOMEMORY;
	}

	sg_zmaee_listmenu_cntx.list_menu[i].menu_count = pListMenu->menu_count;
	for(j = 0; j < pListMenu->menu_count; j++) {
		memcpy(sg_zmaee_listmenu_cntx.list_menu[i].menu_item_name[j], pListMenu->menu_item_name[j], sizeof(pListMenu->menu_item_name[j]));
	}
	
	for(j = 0; j < pListMenu->menu_count; j++) {
		ZMAEE_Callback_Init(&sg_zmaee_listmenu_cntx.list_menu[i].menu_item_func[j], 
			(void*)pListMenu->menu_item_func[j], (void*)pListMenu->menu_item_data[j]);
	}

	ZMAEE_Callback_Init(&sg_zmaee_listmenu_cntx.list_menu[i].menu_exit_func, (void*)pListMenu->menu_exit_func, NULL);
	
	sg_zmaee_listmenu_cntx.is_valid[i] = 1;

	return (i | 0xF0);

}


/**
 * 销毁已创建的系统列表菜单，函数可被重入
 * @handle							列表菜单的句柄
 */
void ZMAEE_Destroy_ListMenu(int handle)
{
	U16 scr_id = GetActiveScreenId();
	int idx = ZMAEE_GetIndexFromListMenuHandle(handle);

	if(idx >= 0) {
#ifdef __MMI_FRM_HISTORY__
		if(scr_id == SCR_ZMAEE_LISTMENU1 + idx) {
			GoBackHistory();
		} else {
			DeleteScreenIfPresent(SCR_ZMAEE_LISTMENU1 + idx);
		}
#else
		mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_LISTMENU1 + idx);
#endif

		memset(&sg_zmaee_listmenu_cntx.list_menu[idx], 0, sizeof(ZMAEE_LISTMENU_ITEM));
		sg_zmaee_listmenu_cntx.is_valid[idx] = 0;
		sg_zmaee_listmenu_cntx.highlight_item[idx] = 0;
	}
}

static
void ZMAEE_ListMenu_Highlight(S32 item_index)
{
	int active_index = sg_zmaee_listmenu_cntx.active_index;

	sg_zmaee_listmenu_cntx.highlight_item[active_index] = item_index;
	
	SetLeftSoftkeyFunction(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_ListMenu_RightSoftkeyFunction, KEY_EVENT_UP);

	SetKeyHandler(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_ENTER, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_ListMenu_RightSoftkeyFunction, KEY_LEFT_ARROW, KEY_EVENT_UP);
}

/**
 * 显示已创建的系统列表菜单
 * @handle							列表菜单的句柄
 * @bHistory						0 - 高亮菜单项从0开始，1 - 高亮菜单项为历时纪录中的高亮项
 */
void ZMAEE_Show_ListMenu(int handle, int bHistory)
{
	unsigned char *history_buffer;
	int idx;

	idx = ZMAEE_GetIndexFromListMenuHandle(handle);
	if(sg_zmaee_listmenu_cntx.active_index < 0) {
		return;
	}

	
#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_LISTMENU1 + idx, ZMAEE_ListMenu_ExitScreenFunction, ZMAEE_ListMenu_EntryScreenFunction, NULL);
	sg_zmaee_listmenu_cntx.active_index = idx;
	history_buffer = GetCurrGuiBuffer(SCR_ZMAEE_LISTMENU1  + sg_zmaee_listmenu_cntx.active_index);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_LISTMENU1 + idx, ZMAEE_ListMenu_ExitScreenFunction,
						ZMAEE_ListMenu_EntryScreenFunction, MMI_FRM_FULL_SCRN);
	history_buffer = mmi_frm_scrn_get_gui_buf(GRP_ID_ZMAEE, SCR_ZMAEE_LISTMENU1 + idx);
#endif
	sg_zmaee_listmenu_cntx.active_index = idx;
	RegisterHighlightHandler(ZMAEE_ListMenu_Highlight);

	if(sg_zmaee_listmenu_cntx.highlight_item[idx] >= sg_zmaee_listmenu_cntx.list_menu[idx].menu_count) {
		sg_zmaee_listmenu_cntx.highlight_item[idx] = 0;
	}
	
	ShowCategory184Screen(
				NULL, 
				NULL,
				STR_GLOBAL_OK, 
				IMG_GLOBAL_OK,
				STR_GLOBAL_BACK, 
				IMG_GLOBAL_BACK,
				sg_zmaee_listmenu_cntx.list_menu[sg_zmaee_listmenu_cntx.active_index].menu_count,
				(GetItemFuncPtr)ZMAEE_GetListMenuItem,
				NULL,
				bHistory? sg_zmaee_listmenu_cntx.highlight_item[idx]: 0,
				bHistory? history_buffer: NULL);

	SetLeftSoftkeyFunction(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_ListMenu_RightSoftkeyFunction, KEY_EVENT_UP);

	SetKeyHandler(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_ENTER, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_ListMenu_LeftSoftkeyFunction, KEY_RIGHT_ARROW, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_ListMenu_RightSoftkeyFunction, KEY_LEFT_ARROW, KEY_EVENT_UP);
}



/****************************************************************************
Popup dialog
****************************************************************************/	  
static
U16 ZMAEE_GetPopUpLogoImg(ZMAEE_POPIMG_TYPE type)
{
	switch(type) {
		case ZMAEE_POPIMG_WARNING:
			return IMG_GLOBAL_WARNING;
			
		case ZMAEE_POPIMG_QUESTION:
			return IMG_GLOBAL_QUESTION;
			
		case ZMAEE_POPIMG_SAVE:
			return IMG_GLOBAL_SAVE;
			
		case ZMAEE_POPIMG_DELETE:
			return IMG_GLOBAL_DELETED;
			
		case ZMAEE_POPIMG_PROGRESS:
			return IMG_GLOBAL_PROGRESS;
			
		case ZMAEE_POPIMG_ERROR:
			return IMG_GLOBAL_ERROR;
			
		case ZMAEE_POPIMG_INFO:
			return IMG_GLOBAL_INFO;
			
		case ZMAEE_POPIMG_SUCCESS:
			return IMG_GLOBAL_SUCCESS;
			
		case ZMAEE_POPIMG_FAIL:
			return IMG_GLOBAL_FAIL;

		default:
			return NULL;
	}
}

/**
 * 显示Pop弹出框
 * @ucs2Msg 						ucs2格式的弹出框内容
 * @type							弹出框图片类型，具体参照zmaee_typedef.h中的e_ZMAEE_POPIMG_TYPE
 * @duration						弹出框显示的时间，单位：毫秒(ms)
 */
void ZMAEE_DisplayPopup(unsigned short *ucs2Msg, ZMAEE_POPIMG_TYPE type, unsigned int duration)
{
	unsigned short imageID;
	
	if(ucs2Msg == NULL) {
		return;
	}

	imageID = ZMAEE_GetPopUpLogoImg(type);

	DisplayPopup((U8*)ucs2Msg, imageID, 0, duration, 0);
}


/****************************************************************************
confirm dialog
****************************************************************************/
typedef struct {
	ZMAEE_SOFTKEY_TYPE	lsk_softkey_type;
	ZMAEE_SOFTKEY_TYPE	rsk_softkey_type;
	char				ucs2_msg[AEE_MAX_FILE_NAME];
	ZMAEE_POPIMG_TYPE	pop_img_type;
	ZMAEE_CALLBACK		lsk_callback;
	ZMAEE_CALLBACK		rsk_callback;
	int 				endkey_disable;
}ZMAEE_DISPLAYCONFIRM;

static ZMAEE_DISPLAYCONFIRM sg_zmaee_displayconfirm = {0};
static int sg_zmaee_displayconfirm_reentry = 0;

void ZMAEE_DisplayConfirm(ZMAEE_SOFTKEY_TYPE lsk_type, ZMAEE_SOFTKEY_TYPE rsk_type, unsigned short *ucs2Msg,
						  			ZMAEE_POPIMG_TYPE img_type, ZMAEE_PFNFUNCEX lsk_func, ZMAEE_PFNFUNCEX rsk_func, 
						  			int endkey_disable, void *pUser);


static
void ZMAEE_GetSoftKeyStrAndImg(ZMAEE_SOFTKEY_TYPE type, U16* stringID, U16* imageID)
{
	switch(type) {
		case ZMAEE_SOFTKEY_OK:
			*stringID = STR_GLOBAL_OK;
			*imageID = IMG_GLOBAL_OK;
			break;

		case ZMAEE_SOFTKEY_BACK:
			*stringID = STR_GLOBAL_BACK;
			*imageID = IMG_GLOBAL_BACK;
			break;

		case ZMAEE_SOFTKEY_YES:
			*stringID = STR_GLOBAL_YES;
			*imageID = IMG_GLOBAL_YES;
			break;

		case ZMAEE_SOFTKEY_NO:
			*stringID = STR_GLOBAL_NO;
			*imageID = IMG_GLOBAL_NO;
			break;

		case ZMAEE_SOFTKEY_CANCEL:
			*stringID = STR_GLOBAL_CANCEL;
			*imageID = IMG_GLOBAL_BACK;
			break;

		default:
			*stringID = NULL;
			*imageID = NULL;
	}
}

static
void ZMAEE_DisplayConfirm_LeftSoftKeyFunc(void)
{
#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() == SCR_ZMAEE_DISPLAY_CONFIRM) {
		GoBackHistory();
	} else {
		DeleteScreenIfPresent(SCR_ZMAEE_DISPLAY_CONFIRM);
	}
#else
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM);
#endif

	if(ZMAEE_Callback_Valid(&sg_zmaee_displayconfirm.lsk_callback)) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_displayconfirm.lsk_callback.pfn;
		pfn(sg_zmaee_displayconfirm.lsk_callback.pUser);
	}
}

static
void ZMAEE_DisplayConfirm_RightSoftKeyFunc(void)
{
#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() == SCR_ZMAEE_DISPLAY_CONFIRM) {
		GoBackHistory();
	} else {
		DeleteScreenIfPresent(SCR_ZMAEE_DISPLAY_CONFIRM);
	}
#else
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM);
#endif

	if(ZMAEE_Callback_Valid(&sg_zmaee_displayconfirm.rsk_callback)) {
		ZMAEE_PFNFUNCEX pfn = (ZMAEE_PFNFUNCEX)sg_zmaee_displayconfirm.rsk_callback.pfn;
		pfn(sg_zmaee_displayconfirm.rsk_callback.pUser);
	}
}

static
void ZMAEE_DisplayConfirm_EndKeyFunc(void)
{
	
}


static
void ZMAEE_DisplayConfirm_ExitScreenFunc(void)
{
	ExitCategory165Screen();	
}


static 
void ZMAEE_DisplayConfirm_EntryScreenFunc(void)
{
	sg_zmaee_displayconfirm_reentry = 1;
	ZMAEE_DisplayConfirm(	sg_zmaee_displayconfirm.lsk_softkey_type, 
							sg_zmaee_displayconfirm.rsk_softkey_type, 
							0, 
							sg_zmaee_displayconfirm.pop_img_type,
						 	(ZMAEE_PFNFUNCEX)sg_zmaee_displayconfirm.lsk_callback.pfn, 
						 	(ZMAEE_PFNFUNCEX)sg_zmaee_displayconfirm.rsk_callback.pfn,
						 	sg_zmaee_displayconfirm.endkey_disable, 
						 	sg_zmaee_displayconfirm.lsk_callback.pUser);
}

/**
* 显示确认框
* @lsk_type 			  左软键类型
* @rsk_type 			  右软键类型
* @ucs2Msg			  ucs2格式的确认框文字内容
* @img_type 			  图片类型
* @lsk_func 			  左软键入口函数
* @rsk_func 			  右软键入口函数
* @endkey_disable		  屏蔽挂机键标识(0 不屏蔽    !0 屏蔽)
* @pUser			  用户数据
*/
void ZMAEE_DisplayConfirm(ZMAEE_SOFTKEY_TYPE lsk_type, ZMAEE_SOFTKEY_TYPE rsk_type, unsigned short *ucs2Msg,
						  			ZMAEE_POPIMG_TYPE img_type, ZMAEE_PFNFUNCEX lsk_func, ZMAEE_PFNFUNCEX rsk_func, 
						  			int endkey_disable, void *pUser)
{
	extern icontext_button MMI_softkeys[WGUI_MAX_SOFTKEYS];
	U16 lsk_string_id, lsk_image_id;
	U16 rsk_string_id, rsk_image_id;
	U16 Popup_logo_imgID;
	int len = sizeof(sg_zmaee_displayconfirm.ucs2_msg)-2;

	if(ucs2Msg)
		memset(&sg_zmaee_displayconfirm, 0, sizeof(ZMAEE_DISPLAYCONFIRM));

	sg_zmaee_displayconfirm.lsk_softkey_type = lsk_type;
	sg_zmaee_displayconfirm.rsk_softkey_type = rsk_type;
	sg_zmaee_displayconfirm.pop_img_type = img_type;
	sg_zmaee_displayconfirm.endkey_disable = endkey_disable;
	
	if(ucs2Msg) {
		if(mmi_ucs2strlen((const S8*)ucs2Msg)*2 <= len) 
			len = mmi_ucs2strlen((const S8*)ucs2Msg)*2;
		
		memcpy(sg_zmaee_displayconfirm.ucs2_msg, ucs2Msg, len);
	} else
		ucs2Msg = (unsigned short *)sg_zmaee_displayconfirm.ucs2_msg;

	ZMAEE_Callback_Init(&sg_zmaee_displayconfirm.lsk_callback, (void*)lsk_func, pUser);
	ZMAEE_Callback_Init(&sg_zmaee_displayconfirm.rsk_callback, (void*)rsk_func, pUser); 

#ifdef __MMI_FRM_HISTORY__
	if( (GetActiveScreenId() != SCR_ZMAEE_DISPLAY_CONFIRM) && (IsScreenPresent(SCR_ZMAEE_DISPLAY_CONFIRM)) && !sg_zmaee_displayconfirm_reentry) {
		return;
	} 
	else if( ((GetActiveScreenId() != SCR_ZMAEE_DISPLAY_CONFIRM) && (!IsScreenPresent(SCR_ZMAEE_DISPLAY_CONFIRM))) || sg_zmaee_displayconfirm_reentry) {
		EntryNewScreen(SCR_ZMAEE_DISPLAY_CONFIRM, ZMAEE_DisplayConfirm_ExitScreenFunc, ZMAEE_DisplayConfirm_EntryScreenFunc, NULL);
	}
	else if(GetActiveScreenId() == SCR_ZMAEE_DISPLAY_CONFIRM)  {
		UI_common_screen_pre_exit();
		UI_common_screen_exit();
	}
#else
	if(mmi_frm_scrn_is_present(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM, MMI_FRM_NODE_ALL_FLAG))
	{
		if(sg_zmaee_displayconfirm_reentry)
		{
			if(!mmi_frm_group_is_present(GRP_ID_ZMAEE))
			{
				mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
			}
			if(mmi_frm_group_get_active_id() != GRP_ID_ZMAEE)
			{
				mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);
			}
			mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM, ZMAEE_DisplayConfirm_ExitScreenFunc,
								ZMAEE_DisplayConfirm_EntryScreenFunc, MMI_FRM_FULL_SCRN);
		}
		else
		{
			if(mmi_frm_scrn_get_active_id() != SCR_ZMAEE_DISPLAY_CONFIRM)
			{
				return;
			}
			else
			{
				UI_common_screen_pre_exit();
				UI_common_screen_exit();
			}
		}
	}
	else
	{
		if(!mmi_frm_group_is_present(GRP_ID_ZMAEE))
		{
			mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
		}
		if(mmi_frm_group_get_active_id() != GRP_ID_ZMAEE)
		{
			mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);
		}
		mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM, ZMAEE_DisplayConfirm_ExitScreenFunc,
								ZMAEE_DisplayConfirm_EntryScreenFunc, MMI_FRM_FULL_SCRN);
	}

#endif	 


	if(sg_zmaee_displayconfirm_reentry)
		sg_zmaee_displayconfirm_reentry = 0;

	if(lsk_type == ZMAEE_SOFTKEY_NONE) {
		MMI_softkeys[MMI_LEFT_SOFTKEY].text = NULL;
	}
	if(rsk_type == ZMAEE_SOFTKEY_NONE) {
		MMI_softkeys[MMI_RIGHT_SOFTKEY].text = NULL;
	}
	show_softkey_background();
	
	ZMAEE_GetSoftKeyStrAndImg(lsk_type, &lsk_string_id, &lsk_image_id);
	ZMAEE_GetSoftKeyStrAndImg(rsk_type, &rsk_string_id, &rsk_image_id);

	Popup_logo_imgID = ZMAEE_GetPopUpLogoImg(img_type);

	ShowCategory165Screen(lsk_string_id, lsk_image_id, rsk_string_id, rsk_image_id, 
					(UI_string_type)ucs2Msg, Popup_logo_imgID, NULL);

	if(endkey_disable != 0) 
	{
		SetKeyHandler(ZMAEE_DisplayConfirm_EndKeyFunc, KEY_END, KEY_EVENT_DOWN);		
	}

	SetLeftSoftkeyFunction(ZMAEE_DisplayConfirm_LeftSoftKeyFunc, KEY_EVENT_UP);
	SetRightSoftkeyFunction(ZMAEE_DisplayConfirm_RightSoftKeyFunc, KEY_EVENT_UP);
}


/**
 * 销毁创建的confirm 框
 */
void ZMAEE_DisplayConfirm_Destory(void)
{
#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() == SCR_ZMAEE_DISPLAY_CONFIRM) {
		GoBackHistory();
	} else {
		DeleteScreenIfPresent(SCR_ZMAEE_DISPLAY_CONFIRM);
	}
#else
	mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_DISPLAY_CONFIRM);
#endif

	memset(&sg_zmaee_displayconfirm, 0, sizeof(sg_zmaee_displayconfirm));
}



int ZMAEE_ReadRecord(void *pBuffer, U16 nBufferSize)
{
	AEE_IFileMgr *pFileMgr;
	AEE_IFile *pFile = NULL;
	unsigned long Ret_read = 0;
	int   Ret = E_ZM_AEE_FAILURE;
	char file_name[AEE_MAX_FILE_NAME] = {0}; 

	if(pBuffer == NULL){
		return E_ZM_AEE_FAILURE; 	
	}
	
	strcpy(file_name,ZMAEE_NVRAM_CONFIG_PATH);	
	if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr) != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	file_name[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
	if (file_name[0] == 0)
		file_name[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);
    	
	pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr,(const char *)file_name,ZM_OFM_READONLY);
	if(pFile != NULL){
		Ret_read = ZMAEE_IFile_Read(pFile, pBuffer, nBufferSize);
		if(Ret_read == nBufferSize){
			Ret =  E_ZM_AEE_SUCCESS;
		}
		
		ZMAEE_IFile_Release(pFile);
	}

	ZMAEE_IFileMgr_Release(pFileMgr);
	return Ret;	
		
}

/**
 * @nLID		0 - NVRAM_EF_ZMAEE_CONFIG_LID, 1 - NVRAM_EF_ZMAEE_THEME_CONFIG_LID
 * @ pError	NVRAM_WRITE_SUCCESS, NVRAM_WRITE_FAIL
 * RETURN:   
 *  nBufferSize
 */
int ZMAEE_WriteRecord(void *pBuffer, U16 nBufferSize)
{
	AEE_IFileMgr *pFileMgr;
	AEE_IFile *pFile = NULL;
	unsigned long Ret_write = 0;
	int  Ret = E_ZM_AEE_FAILURE;
	char file_name[AEE_MAX_FILE_NAME] = {0};
	
	if(pBuffer == NULL){
		return 0; 	
	}

	strcpy(file_name, ZMAEE_NVRAM_CONFIG_PATH);
	if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr) != E_ZM_AEE_SUCCESS) {
		return 0;
	}

	file_name[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
	if (file_name[0] == 0)
		file_name[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);

	pFile = ZMAEE_IFileMgr_OpenFile(pFileMgr,(const char *)file_name,ZM_OFM_READWRITE | ZM_OFM_TRUNCATE | ZM_OFM_CREATE);
	if(pFile != NULL){
		Ret_write = ZMAEE_IFile_Write(pFile, pBuffer, nBufferSize);
		if(Ret_write == nBufferSize){
			Ret = E_ZM_AEE_SUCCESS;
		}

		ZMAEE_IFile_Release(pFile);
	}

	ZMAEE_IFileMgr_Release(pFileMgr);
	return Ret;

}

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#ifdef __ZMAEE_APP_DWALLPAPER__
void ZMAEE_SetWallPaperObject(void *pObj)
{
	sg_zmaee_wallpaper_object = pObj;
}

void* ZMAEE_GetWallPaperObject(void)
{
	return sg_zmaee_wallpaper_object;
}
#endif	// __ZMAEE_APP_DWALLPAPER__
#endif

int ZMAEE_Fetion_IsRunning(void)
{
	return ZMAEE_Background_IsRunning("fetionlib.dll");
}

void ZMAEE_Fetion_Exit(void)
{
	ZMAEE_Background_Exit("fetionlib.dll");
}

int ZMAEE_MSN_IsRunning(void)
{
	return ZMAEE_Background_IsRunning("msnlib.dll");
}

void ZMAEE_MSN_Exit(void)
{
	ZMAEE_Background_Exit("msnlib.dll");
}

#if (ZM_AEE_MTK_SOFTVERN == 0x10A0)
#ifdef __ZMAEE_APP_DWALLPAPER__
#ifdef __MMI_VUI_HOMESCREEN__
void ZMAEE_DWallPaper_PreHandle(void)
{
	int ZMAEE_DWallPaper_CheckApp(unsigned int clsId);

	ZM_AEECLSID clsId = ZMAEE_GetThemeAppId(1);
	if(ZMAEE_DWallPaper_CheckApp(clsId)) {
		ZMAEE_Phnset_WallPaper_SetVuiIndex(clsId);
	} else {
		extern int ZMAEE_Phnset_Is_ZMAEE_WallPaper(void);		
		if(ZMAEE_Phnset_Is_ZMAEE_WallPaper()) {
			ZMAEE_SetThemeAppId(1, 0);
		}
	}
}
#endif	// __MMI_VUI_HOMESCREEN__
#endif	// __ZMAEE_APP_DWALLPAPER__
#endif	// (ZM_AEE_MTK_SOFTVERN == 0x10A0)

#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)) || defined (__MMI_VUI_LAUNCHER_KEY__)
#ifdef __ZMAEE_APP_DWALLPAPER__
extern void ZMAEE_SetWallpaperId(void);
extern void ZMAEE_ClearWallpaperId(void);
extern int ZMAEE_IsActive_Wallpaper(void);

void ZMAEE_DWallPaper_PreHandle(void)
{
	ZM_AEECLSID clsId = ZMAEE_GetThemeAppId(1);

	if(clsId && ZMAEE_DWallPaper_CheckApp(clsId)) {
		ZMAEE_SetWallpaperId();
	} else {
		if(ZMAEE_IsActive_Wallpaper()) {
			ZMAEE_ClearWallpaperId();
		}
	}
}
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN == 0x11B0) && defined (__MMI_VUI_LAUNCHER__)
#ifdef __ZMAEE_APP_DWALLPAPER__
void ZMAEE_DWallPaper_PreHandle(void)
{
	int ZMAEE_DWallPaper_CheckApp(unsigned int clsId);

	ZM_AEECLSID clsId = ZMAEE_GetThemeAppId(1);
	if(ZMAEE_DWallPaper_CheckApp(clsId)) {
		ZMAEE_Phnset_WallPaper_SetVuiIndex(clsId);
	} else {
		extern int ZMAEE_Phnset_Is_ZMAEE_WallPaper(void);		
		if(ZMAEE_Phnset_Is_ZMAEE_WallPaper()) {
			ZMAEE_SetThemeAppId(1, 0);
		}
	}
}
#endif	// __ZMAEE_APP_DWALLPAPER__
#endif	// (ZM_AEE_MTK_SOFTVERN == 0x10A0)

#ifdef __ZMAEE_APP_DWALLPAPER__
/**
 * RETURN:
 * 	1				valid app
 *	0				invalid app
 */
int ZMAEE_DWallPaper_CheckApp(unsigned int clsId)
{
	extern unsigned int ZMAEE_IShell_GetAppletMask(AEE_IShell* po);
	extern int ZMAEE_IShell_ValidateApplet(AEE_IShell *po, ZM_AEECLSID clsId);
	
	AEE_IShell *pShell;
	char workDir[AEE_MAX_PATH_NAME] = {0};
	unsigned app_mask = 0;
	int ret = 0;

	pShell = ZMAEE_GetShell();

	strncpy(workDir, ZMAEE_IShell_GetWorkDir(pShell), sizeof(workDir));
	app_mask = ZMAEE_IShell_GetAppletMask(pShell);
	if(!ZMAEE_IShell_SetWorkDir(pShell, ZMAEE_ENTRY_THEMEDIR, app_mask)) {
		return 0;
	}

	if((ZMAEE_IsAppletInROM(clsId)) || (ZMAEE_IShell_CanStartApplet(pShell, clsId) && ZMAEE_IShell_ValidateApplet(pShell, clsId))) {
		ret = 1;
	}

	ZMAEE_IShell_SetWorkDir(pShell, workDir, app_mask);
	return ret;
}
#endif

void ZMAEE_Init_PreHandle(void)
{
#if 0	// 如果内置了号码归属地，则将此代码打开
	zm_extern int ZMAEE_IShell_RegisterNotify(AEE_IShell* po, ZMAEE_NotifyType type, int priority,const char* pszAppFile);

	char pszPhonesearch[64] = {0};
	AEE_IFileMgr *pFileMgr = NULL;
	AEE_IShell *pShell = ZMAEE_GetShell();
	char driver = 0;

	ZMAEE_IShell_CreateInstance(pShell, ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
	if(!pFileMgr)
		return;

	sprintf(pszPhonesearch + 1, "%s00000405\\callsearch.so", ZMAEE_IShell_GetAppDir(pShell, ZMAEE_APPATTR_APPLET_APPLICATION)); 
	pszPhonesearch[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);
	if(!pszPhonesearch[0] || ZMAEE_IFileMgr_Test(pFileMgr, pszPhonesearch) != E_ZM_AEE_SUCCESS) {
		pszPhonesearch[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1);
		if(!pszPhonesearch[0] || ZMAEE_IFileMgr_Test(pFileMgr, pszPhonesearch) != E_ZM_AEE_SUCCESS) {
			pszPhonesearch[0] = 0;
		}
	}
	
	if(pszPhonesearch[0]) { 	
		ZMAEE_IShell_RegisterNotify(ZMAEE_GetShell(), ZMAEE_NOTIFY_CALL, 50, pszPhonesearch);
	}

	ZMAEE_IFileMgr_Release(pFileMgr);
#endif
}

#if ((defined MT6252) || (defined ZMAEE_SUPPORT_PNG_DECODE)) || defined (MT6250) || (defined MT6260)|| defined (MT6261) //sgf
/****************************************************
 PNG接口
****************************************************/
zm_extern int ZMAEE_IImage_PNG_Draw(AEE_IAStream* source, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, ZMAEE_Rect *rect, 
										ZMAEE_LayerInfo *li, ZMAEE_LayerInfo *src_blending);
zm_extern int ZMAEE_IImage_IsPNGFile(AEE_IAStream* source);
zm_extern int ZMAEE_IImage_PNG_GetDimension(AEE_IAStream* source, int *width, int *height);

void* ZMAEE_Mem_MallocMedMem(unsigned int size)
{
	return med_alloc_ext_mem(size);
}

void ZMAEE_Mem_FreeMedMem(void *ptr)
{
	med_free_ext_mem(&ptr);
}

int zmaee_gdicf_to_zmcf(gdi_color_format gdi_cf, ZMAEE_ColorFormat *cf)
{
	if(cf == NULL)
		return E_ZM_AEE_BADPARAM;

	switch(gdi_cf) {
		case GDI_COLOR_FORMAT_8:
			*cf = ZMAEE_COLORFORMAT_8;
			break;
		case GDI_COLOR_FORMAT_16:
			*cf = ZMAEE_COLORFORMAT_16;
			break;
		case GDI_COLOR_FORMAT_32:
			*cf = ZMAEE_COLORFORMAT_32;
			break;
		default:
			return E_ZM_AEE_FAILURE;
	};

	return E_ZM_AEE_SUCCESS;
}


void zmaee_fill_layer_info(gdi_handle handle, ZMAEE_LayerInfo *layer_info)
{
	extern lcd_layer_struct gdi_layer_info[GDI_LAYER_TOTAL_LAYER_COUNT];
	gdi_layer_struct *layer_struct = (gdi_layer_struct*)handle;
	unsigned int a, r, g, b;

	if(!layer_struct)
		return;

	zmaee_gdicf_to_zmcf(layer_struct->cf, &layer_info->cf);
	layer_info->nScreenX = layer_struct->offset_x;
	layer_info->nScreenY = layer_struct->offset_y;
	layer_info->nWidth = layer_struct->width;
	layer_info->nHeight = layer_struct->height;
	layer_info->rcClip.x = layer_struct->clipx1;
	layer_info->rcClip.y = layer_struct->clipy1;
	layer_info->rcClip.width = layer_struct->clipx2 - layer_struct->clipx1 + 1;
	layer_info->rcClip.height = layer_struct->clipy2 - layer_struct->clipy1 + 1;
	layer_info->pFrameBuf = layer_struct->buf_ptr;
	layer_info->opacity_enable = (gdi_layer_info[layer_struct->id].opacity_enable)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
	layer_info->opacity_value = gdi_layer_info[layer_struct->id].opacity_value;
	layer_info->trans_enable = (gdi_layer_info[layer_struct->id].source_key_enable)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;

	gdi_act_color_to_rgb(&a, &r, &g, &b, gdi_layer_info[layer_struct->id].source_key);
	layer_info->trans_color = ZMAEE_GET_RGBA(r, g, b, a);
}

void zmaee_create_iastream(U32 flag, U8 *data_ptr, U32 img_size, AEE_IAStream **pIAStream, int (**pIAStream_Release_Func)(AEE_IAStream*))
{
	if(flag & GDI_IMAGE_CODEC_FLAG_IS_FILE) {
		AEE_IFileMgr *iFileMgr = NULL;
		char utf8_file_path[AEE_MAX_FILE_NAME] = {0};

		if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&iFileMgr) != E_ZM_AEE_SUCCESS) {		
			ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 2");
			return;
		}

		ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 3");
		ZMAEE_Ucs2_2_Utf8((const unsigned short*)data_ptr, mmi_ucs2strlen(data_ptr), utf8_file_path, sizeof(utf8_file_path));
		*pIAStream = (AEE_IAStream*)ZMAEE_IFileMgr_OpenFile(iFileMgr, utf8_file_path, ZM_OFM_READONLY);
		*pIAStream_Release_Func = ZMAEE_FileInlib_Release;
		ZMAEE_IFileMgr_Release(iFileMgr);
	} else {
		ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 4");
		if(ZMAEE_IMemStream_Create(ZM_AEE_CLSID_MEMSTREAM, (void**)pIAStream, data_ptr, img_size) != E_ZM_AEE_SUCCESS)
			return;

		*pIAStream_Release_Func = ZMAEE_IMemStream_Release;
	}
}

GDI_RESULT zmaee_gdi_image_png_draw_handler(U32 flag, U32 frame_pos, S32 x, S32 y, S32 w, S32 h, U8 *data_ptr, U32 img_size)
{
	AEE_IAStream *iAStream = NULL;
	int (*iAStream_Release_Func)(AEE_IAStream*) = NULL;
	ZMAEE_LayerInfo active_layer_info = {0}, blending_layer_info = {0};
	gdi_handle active_layer = NULL, blending_layer = NULL;
	int ret;
	ZMAEE_Rect png_rect = {0};

	ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: Entry, x = %d, y = %d, w = %d, h = %d", x, y, w, h);

	png_rect.x = x;
	png_rect.y = y;
	png_rect.width = w;
	png_rect.height = h;
	
	gdi_layer_get_active(&active_layer);
	zmaee_fill_layer_info(active_layer, &active_layer_info);
	if(active_layer_info.cf == ZMAEE_COLORFORMAT_16) {
		gdi_get_alpha_blending_source_layer(&blending_layer);
		zmaee_fill_layer_info(blending_layer, &blending_layer_info);
		ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 16bit layer");
	}

	ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 1");
	zmaee_create_iastream(flag, data_ptr, img_size, &iAStream, &iAStream_Release_Func);
	
	if(!w && !h) {
		if(ZMAEE_IImage_PNG_GetDimension(iAStream, &png_rect.width, &png_rect.height) != E_ZM_AEE_SUCCESS) {
			iAStream_Release_Func(iAStream);
			ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: 6");
			return GDI_FAILED;
		}

		iAStream_Release_Func(iAStream);
	}

	zmaee_create_iastream(flag, data_ptr, img_size, &iAStream, &iAStream_Release_Func);
	ret = ZMAEE_IImage_PNG_Draw((AEE_IAStream *)iAStream, ZMAEE_Mem_MallocMedMem, ZMAEE_Mem_FreeMedMem, &png_rect, &active_layer_info, 
								(blending_layer)? &blending_layer_info: NULL);
	iAStream_Release_Func(iAStream);
	ZMAEE_DebugPrint("zmaee_gdi_image_png_draw_handler: ret = %d", ret);

	if(ret == E_ZM_AEE_SUCCESS)
		return GDI_SUCCEED;
	else
		return GDI_FAILED;
}

GDI_RESULT zmaee_gdi_image_png_get_dimension_handler(U32 flag, U8 *data_ptr, U32 img_size, S32 *width, S32 *height)
{
	AEE_IAStream *iAStream = NULL;
	int (*iAStream_Release_Func)(AEE_IAStream*) = NULL;
	int ret;

	ZMAEE_DebugPrint("zmaee_gdi_image_png_get_dimension_handler: Entry");

	if(flag & GDI_IMAGE_CODEC_FLAG_IS_FILE) {
		AEE_IFileMgr *iFileMgr = NULL;
		char utf8_file_path[AEE_MAX_FILE_NAME] = {0};

		if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&iFileMgr) != E_ZM_AEE_SUCCESS) {
			ZMAEE_DebugPrint("zmaee_gdi_image_png_get_dimension_handler: 1");
			return GDI_FAILED;
		}

		ZMAEE_Ucs2_2_Utf8((const unsigned short*)data_ptr, mmi_ucs2strlen(data_ptr), utf8_file_path, sizeof(utf8_file_path));
		iAStream = (AEE_IAStream*)ZMAEE_IFileMgr_OpenFile(iFileMgr, utf8_file_path, ZM_OFM_READONLY);
		iAStream_Release_Func = ZMAEE_IFile_Release;
		ZMAEE_IFileMgr_Release(iFileMgr);
		ZMAEE_DebugPrint("zmaee_gdi_image_png_get_dimension_handler: 2");
	} else {		
		if(ZMAEE_IMemStream_Create(ZM_AEE_CLSID_MEMSTREAM, (void**)&iAStream, data_ptr, img_size) != E_ZM_AEE_SUCCESS) {
			ZMAEE_DebugPrint("zmaee_gdi_image_png_get_dimension_handler: 3");
			return GDI_FAILED;
		}

		iAStream_Release_Func = ZMAEE_IMemStream_Release;
	}

	ret = ZMAEE_IImage_PNG_GetDimension(iAStream, width, height);	
	iAStream_Release_Func(iAStream);
	ZMAEE_DebugPrint("zmaee_gdi_image_png_get_dimension_handler: ret = %d", ret);

	if(ret == E_ZM_AEE_SUCCESS)
		return GDI_SUCCEED;
	else
		return GDI_FAILED;
}

BOOL zmaee_gdi_image_is_png_file(U8 *file_name)
{
	AEE_IFileMgr *iFileMgr = NULL;
	AEE_IFile *iFile = NULL;
	char utf8_file_path[AEE_MAX_FILE_NAME] = {0};
	int ret;

	ZMAEE_DebugPrint("zmaee_gdi_image_is_png_file: Entry");
	if(ZMAEE_IFileMgr_New(ZM_AEE_CLSID_FILEMGR, (void**)&iFileMgr) != E_ZM_AEE_SUCCESS) {
		ZMAEE_DebugPrint("zmaee_gdi_image_is_png_file: 1");
		return FALSE;
	}
	
	ZMAEE_Ucs2_2_Utf8((const unsigned short*)file_name, mmi_ucs2strlen(file_name), utf8_file_path, sizeof(utf8_file_path));
	iFile = ZMAEE_IFileMgr_OpenFile(iFileMgr, utf8_file_path, ZM_OFM_READONLY);
	ZMAEE_IFileMgr_Release(iFileMgr);

	ret = ZMAEE_IImage_IsPNGFile((AEE_IAStream*)iFile);
	ZMAEE_IFile_Release(iFile);
	ZMAEE_DebugPrint("zmaee_gdi_image_is_png_file: ret = %d", ret);
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
	if(ret == E_ZM_AEE_SUCCESS)
		return TRUE;
	else
		return FALSE;
#else
	if(ret == E_ZM_AEE_SUCCESS) {
		return 0;
	} else {
		return -1;
	}
#endif
	
}

void ZMAEE_IShell_DllInfeInit(void)
{
}

#ifndef __ZMAEE_REDUCE_DOWNLOAD__
void zmaee_down_cancel(void)
{
}
#endif

#endif

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#if (ZM_AEE_MTK_SOFTVERN < 0x11B0)
int zmaee_keylock_id = 0;
#endif

static
void ZMAEE_KeyHome_Dummy_Handler(void)
{
	// Do Nothing
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
int g_zmaee_clear_keylock_id_before_start = 0;
#endif

/**
 * 移植步骤中启动锁屏
 * RETURN:
 * 	0			启动失败，加载系统的锁屏
 * 	1			启动成功
 */
int ZMAEE_KeyLock_Start(void)
{
#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
	if(g_zmaee_clear_keylock_id_before_start) {
		g_zmaee_clear_keylock_id_before_start = 0;
		ZMAEE_SetThemeAppId(0, 0);
	}
#endif

	// 如果在我们的应用中，则不锁屏(除动态壁纸和主题外)
	if(ZMAEE_CheckPlat_IsRunning() && !ZMAEE_DWallPaper_IsStart()) {
		return 1;
	}

	if(zmaee_keylock_screen_classid()) {
		zmaee_keylock_screen_entry();

		// Clear KEY_HOME handler		
		SetKeyHandler(ZMAEE_KeyHome_Dummy_Handler, KEY_HOME, KEY_EVENT_UP);
		SetKeyHandler(ZMAEE_KeyHome_Dummy_Handler, KEY_HOME, KEY_EVENT_DOWN);
		SetKeyHandler(ZMAEE_KeyHome_Dummy_Handler, KEY_HOME, KEY_EVENT_LONG_PRESS);
		SetKeyHandler(ZMAEE_KeyHome_Dummy_Handler, KEY_HOME, KEY_EVENT_REPEAT);
		
		return 1;
	}

	return 0;
}

/**
 * 移植步骤中强制退出锁屏
 * RETURN:
 * 	0			我们的锁屏没有启动
 * 	1			退出成功
 */
int ZMAEE_KeyLock_Exit(void)
{
	if(g_zmaee_keylock_entry && zmaee_keylock_screen_classid()) {
		zmaee_keylock_forceexit();
		GoBackHistory();

		return 1;
	}

	return 0;
}

/**
 * 移植步骤中判断锁屏是否启动
 * RETURN:
 * 	0			未启动
 * 	1			启动
 */
int ZMAEE_KeyLock_IsStart(void)
{
	return g_zmaee_keylock_entry;
}

#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
mmi_ret ZMAEE_Usb_EnterMS_Hdlr(void)
{
	extern void ZMAEE_DebugPrint(const char * pszFormat,...);
	ZMAEE_DebugPrint("mmi_usb_mmi_notify_callback:srv_usb_get_status=%d",srv_usb_get_status());

	{			
		extern void ZMAEE_Exit(void);
		ZMAEE_Exit();
		
	#ifdef __ZMAEE_APP_THEME__
		{
			extern void ZMAEE_Theme_Exit(void);
			extern void ZMAEE_DebugPrint(const char * pszFormat,...);
			ZMAEE_Theme_Exit();
			ZMAEE_DebugPrint("mmi_usb_mmi_notify_callback:theme exit");
		}
	#endif
	#ifdef __ZMAEE_APP_KEYPAD_LOCK__
		{
			extern int g_zmaee_keylock_entry;
			if(g_zmaee_keylock_entry)
				g_zmaee_keylock_entry = 0;
		}
	#endif
	
	#ifdef __ZMAEE_APP_TTS__
	{
		extern void zmaee_tts_stop_and_free_mem(void);
		zmaee_tts_stop_and_free_mem();
	}
	#endif

	}
	return MMI_OK;
}

int ZMAEE_Usb_Exit_PeriodCB(int nHandle, void* pUser)
{
#if !defined (__COSMOS_MMI_PACKAGE__)

#ifdef __MMI_FRM_HISTORY__
	if(GetActiveScreenId() != MAIN_MENU_SCREENID) {
		EntryNewScreen(0, NULL, NULL, NULL);
		GoBackHistory();
	} else {
		GoBackHistory();
	}
#else
	MMI_ID grp_id, scrn_id;

	mmi_frm_get_active_scrn_id(&grp_id, &scrn_id);
	if(scrn_id != MAIN_MENU_SCREENID) {		
		mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
		mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);
		
		mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN, NULL, zmaee_dummy_entry, MMI_FRM_FULL_SCRN);
		mmi_frm_scrn_close(GRP_ID_ZMAEE, SCR_ZMAEE_MAIN);
	}
#endif
#else
	// dummy
#endif
	return 0;
}

mmi_ret ZMAEE_Usb_ExitMS_Hdlr(void)
{
#ifdef __ZMAEE_APP_THEME__
	ZMAEE_Theme_Start();
	ZMAEE_DebugPrint("ZMAEE_Usb_ExitMS_Hdlr(): ret = %d", ZMAEE_Theme_IsValidate());
	if(ZMAEE_Theme_IsValidate()) {
		ZMAEE_StartPeriodHandler(0, NULL, ZMAEE_Usb_Exit_PeriodCB);
		return MMI_OK;
	}
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	if((GetActiveScreenId() == SCR_ID_IDLE_MAIN) && ZMAEE_GetThemeAppId(1) > 0) {
		ZMAEE_StartPeriodHandler(0, NULL, ZMAEE_Usb_Exit_PeriodCB);
	}
#endif

	return MMI_OK;
}

#endif
//sgf
#if defined __MMI_VUI_LAUNCHER__ || (defined (MT6250) && (ZM_AEE_MTK_SOFTVERN >= 0x1220))|| (defined (MT6260) && (ZM_AEE_MTK_SOFTVERN >= 0x1220))|| (defined (MT6261) && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
MMI_ID vapp_zmaeemain_launch(void *param, U32 param_size)
{
	ZMAEE_Main();
	return MMI_RET_OK;
}

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
MMI_ID vapp_zmaeekeylock_launch(void *param, U32 param_size)
{
	ZMAEE_APP_KeyLock_Entry();
	return MMI_RET_OK;
 
}
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
MMI_ID vapp_zmaeewallpaper_launch(void *param, U32 param_size)
{
	ZMAEE_APP_DWPaper_Entry();
	return MMI_RET_OK;
}
#endif

#ifdef __ZMAEE_APP_THEME__
MMI_ID vapp_zmaeetheme_launch(void *param, U32 param_size)
{
	ZMAEE_APP_Theme_Entry();
	return MMI_RET_OK;
 
}
#endif
#endif


/**
*恢复出厂设置
*/
#ifdef __ZMAEE_APP_DWALLPAPER__
void ZMAEE_DWallPaper_Restore(void)
{
	if(ZMAEE_DEFAULT_DWALLPAPER_CLSID != 0) {
		ZMAEE_SetThemeAppId(1, ZMAEE_DEFAULT_DWALLPAPER_CLSID);
	} else {
		ZMAEE_SetThemeAppId(1, 0);
	}
}
#endif

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
void ZMAEE_KeyLock_Restore(void)
{
	if(ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID != 0) {
		ZMAEE_SetThemeAppId(0, ZMAEE_DEFAULT_KEYPAD_LOCK_CLSID);
	} else {
		ZMAEE_SetThemeAppId(0, 0);
	}
}
#endif

#ifdef __ZMAEE_APP_THEME__
void ZMAEE_Theme_Restore(void)
{
	if(ZMAEE_DEFAULT_THEME_CLSID != 0) {
		ZMAEE_SetThemeAppId(2, ZMAEE_DEFAULT_THEME_CLSID);
	} else {
		ZMAEE_SetThemeAppId(2, 0);
	}
}
#endif

void ZMAEE_QQ_ExitEx(void)
{
	ZMAEE_QQ_Exit();
	GoBackHistory();
}


void ZMAEEShowExitInfo(void)
{
	DisplayConfirm(STR_GLOBAL_YES, IMG_GLOBAL_OK, STR_GLOBAL_NO, IMG_GLOBAL_BACK, (UI_string_type)GetString(STR_ZMAEE_EXITQQ), IMG_GLOBAL_QUESTION, 0);
	SetLeftSoftkeyFunction(ZMAEE_QQ_ExitEx, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
	SetKeyHandler(ZMAEE_QQ_ExitEx, KEY_ENTER, KEY_EVENT_UP);
}


#endif	// __ZMAEE_APP__
