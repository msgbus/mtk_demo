#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
#include "CmErrorTypes.h"
#endif
#include "CallManagementStruct.h"
#include "CallStructureManagementProt.h"
#include "CallManagementStruct.h"
#include "CommonStubsProt.h"

#include "customer_ps_inc.h"

#include "ems.h"
#include "mmi_msg_context.h"
#include "MessagesL4Def.h"
#include "wgui_ems.h"
#include "MessagesMiscell.h"
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
#include "SMSApi.h"
#include "SMSStruct.h"
#include "SMSFunc.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0X11A0)
#include "PhoneBookResDef.h"
#include "ModeSwitchSrvGprot.h" 
#endif


#include "ProtocolEvents.h"
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
#include "mtpnp_pfal_master_def.h"
#include "SimDetectionGprot.h"
#include "SimDetectionDef.h"
#endif

#include "nvram_enums.h"
#include "nvram_interface.h"
#include "MessagesResourceData.h"

#include "EngineerModeDef.h"
#include "EngineerModeType.h"

#include "stack_msgs.h"

#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
#include "SmsGuiInterfaceType.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN <= 0x0824)
#include "wgui_categories_inputsenum.h"
#include "NVRAMType.h"
#include "NVRAMProt.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x0852)
#include "EngineerModeProt.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
#include "CallsStruct.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0)
#include "smssrvgprot.h"
#include "UCMGProt.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "simaccesssrvgprot.h"
#include "nwinfosrvgprot.h"
#ifdef MT6252
#include "modeswitchsrvgprot.h"
#endif
#endif

#include "SIMAccessGprot.h"
#include "sim_common_enums.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#include "NwNameSrvGprot.h"
#elif (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "IdleGprot.h"
#include "IdleObject.h"
#endif
#ifdef WIN32
#include "ImeiSrvGprot.h" //WLJ
#endif


#ifdef ZMAEE_MAKEVOICE_WITH_NUMLIST
#include "TimerEvents.h"
#endif

#ifdef ZMAEE_MAKEVOICE_WITH_NUMLIST
#define ZMAEE_CALL_MAX			4
#define ZMAEE_CALL_LENGTH_MAX	12
#define ZMAEE_CALL_TIME_SLICE	50

static int glb_zmaee_call_numberlist_index = 0, glb_zmaee_call_numberlist_max = 0;
static char glb_zmaee_call_numberlist[ZMAEE_CALL_MAX][ZMAEE_CALL_LENGTH_MAX];
#endif


extern PendingSaveSendDataStruct PendingSaveSendData;
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
extern pwr_context_struct g_pwr_context;
#endif
extern EmCommandTypeEnum currentEmCommand;
#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
extern U8 currentEmMode[];
#else
extern U32 currentEmMode;
#endif
extern EMSData* GetEMSDataForEdit(EMSData **p,U8 force);
#ifdef __MMI_DUAL_SIM_MASTER__
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
extern pwr_context_struct g_pwr_context_2;
#endif
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
extern msg_context_struct g_msg_cntx;
extern U8 DeliveryRepyStates[2];
#endif

void ZMAEE_ITAPI_InitIMSI(void);
void ZMAEE_ITAPI_InitIMEI(void);
void ZMAEE_ITAPI_InitSCADDR(void);
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
static void ZMAEE_ITAPI_SETSCADDR1_Resp(void *inMsg);
static void ZMAEE_ITAPI_SETSCADDR2_Resp(void *inMsg);
static void ZMAEE_ITAPI_SendSmsResp(void* number, module_type mod, U16 result);
#else
static void ZMAEE_ITAPI_SETSCADDR1_Resp(srv_sms_callback_struct *callback_data);
static void ZMAEE_ITAPI_SETSCADDR2_Resp(srv_sms_callback_struct *callback_data);
static void ZMAEE_ITAPI_SendSmsResp(srv_sms_callback_struct* callback_data);
#endif
static int ZMAEE_ITAPI_CanSendSms(AEE_ITAPI *po);
static void ZMAEE_ITAPI_EMSendStopReq(void);
static void ZMAEE_ITAPI_EMParsingData(void *info);
static void ZMAEE_ITAPI_EMSendStartReq(int sim_idx);


zm_extern void ZMAEE_ChangeChannel(int owner, int channel);
zm_extern int ZMAEE_Utf8_2_Ucs2(const char* src, int len, unsigned short* wcsDst, int nSize);
zm_extern void ZMAEE_Callback_Init(ZMAEE_CALLBACK* pCb, void* pfn, void* pUser);
zm_extern int   ZMAEE_Callback_Valid(ZMAEE_CALLBACK* pCb);
zm_extern void ZMAEE_SetProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func);
zm_extern void ZMAEE_ClearProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func);
#ifdef WIN32
zm_extern int 	ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
#endif

#define ZMAEE_ITAPI_FUNC_COUNT			20
static int ZMAEE_ITAPI_AddRef(AEE_ITAPI* po);
zm_extern int ZMAEE_ITAPI_Release(AEE_ITAPI* po);
static int ZMAEE_ITAPI_GetCallerID(AEE_ITAPI * po, const char* pDest, int nSize);
static int ZMAEE_ITAPI_MakeVoiceCall(AEE_ITAPI* po, const char* number); 
static int ZMAEE_ITAPI_SendSms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, ZMAEE_PFNSMSSTATUS pfn, void* pUser);
static int ZMAEE_ITAPI_SendPortSms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, int nPort, ZMAEE_PFNSMSSTATUS pfn, void* pUser);
static int ZMAEE_ITAPI_SendDelaySms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, int nPort);
static int ZMAEE_ITAPI_GetSmsCenter(AEE_ITAPI* po, int sim_idx, char* pSmsC, int maxLen);
static int ZMAEE_ITAPI_SetSmsCenter(AEE_ITAPI* po, int sim_idx, const char* pSmsC);
static int ZMAEE_ITAPI_SimCardCount(AEE_ITAPI* po);
zm_extern int ZMAEE_ITAPI_SetActiveSimCard(AEE_ITAPI* po, int sim_idx);
zm_extern int ZMAEE_ITAPI_GetImsi(AEE_ITAPI* po, int sim_idx, char* pImsi, int maxLen);
static int ZMAEE_ITAPI_GetImei(AEE_ITAPI* po, char* pImei, int maxLen);
static int ZMAEE_ITAPI_StartReportBSID(AEE_ITAPI* po, ZMAEE_PFNBSIDCB pfn, void* pUser);
static int ZMAEE_ITAPI_StopReportBSID(AEE_ITAPI* po);
static int ZMAEE_ITAPI_GetSimCardStatus(AEE_ITAPI* po, int sim_idx);
zm_extern int ZMAEE_ITAPI_GetActiveSimCard(AEE_ITAPI* po);
static int ZMAEE_ITAPI_GetMessageCount(AEE_ITAPI* po, int type);
static int ZMAEE_ITAPI_GetSimServMsg(AEE_ITAPI *po, int sim_idx, unsigned char *buf, int buf_size);
zm_extern int ZMAEE_ITAPI_GetICCID(AEE_ITAPI *po, ZMAEE_PFNICCID pfn, void *pUser);



#define AEE_MAX_SIMCARD_COUNT		4
typedef enum {
	ZMAEE_SIM_TYPE_1 = 0,
	ZMAEE_SIM_TYPE_2 = 1,
	ZMAEE_SIM_TYPE_3 = 2,
	ZMAEE_SIM_TYPE_4 = 3
}e_ZMAEE_SIM_TYPE;
typedef int ZMAEE_SIM_TYPE;

typedef struct{
	int 	sim_id;
	int 	sim_service;
}zmaee_sim_status;

typedef struct {
	int 	sim_count;
	int		sim_active;
	zmaee_sim_status sim_status[AEE_MAX_SIMCARD_COUNT];
}ZMAEE_TAPI_SIMINFO;

typedef struct {
	ZMAEE_CALLBACK		callback;
	int					is_sending;
}ZMAEE_TAPI_SMSINFO;

typedef struct {
	ZMAEE_CALLBACK		callback;
	int					is_request;
	short				disable_notify;
	short				delay_stop;				
}ZMAEE_TAPI_BSIDINFO;

typedef struct {
	void 				*pVtbl;
	int              	nRef;
	ZMAEE_TAPI_SMSINFO	sms_info;
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	ZMAEE_TAPI_BSIDINFO	bsid_info;
#else	
	ZMAEE_CALLBACK		bsid_callback;
	int 				bsid_request;
	short				bsid_stop;
	srv_nw_info_location_info_struct bsid_info;
#endif

	int					iccid_busy;
	char				iccid_buf[AEE_MAX_NUM_SIZE];
	ZMAEE_CALLBACK		iccid_callback;
}ZMAEE_TAPI;

typedef struct {
	char 				imei_buf[AEE_MAX_SERIAL_SIZE];
	char				imsi_buf1[AEE_MAX_SERIAL_SIZE];
	char				sc_addr1[AEE_MAX_NUM_SIZE];
#ifdef __MMI_DUAL_SIM_MASTER__
	char				imsi_buf2[AEE_MAX_SERIAL_SIZE];
	char				sc_addr2[AEE_MAX_NUM_SIZE];
#endif

#if (MMI_MAX_SIM_NUM >= 3)
	char                imsi_buf3[AEE_MAX_SERIAL_SIZE];
#endif

#if (MMI_MAX_SIM_NUM >= 4)
 	char                imsi_buf4[AEE_MAX_SERIAL_SIZE];
#endif



	ZMAEE_TAPI_SIMINFO	sim_info;
	ZMAEE_TAPI			*sms_po;
	ZMAEE_TAPI			*bsid_po;

	char				is_getting_imsi;
	char				is_getting_scaddr;
	char				is_setting_scaddr;
}ZMAEE_TAPI_CNTX;

static ZMAEE_TAPI_CNTX sg_zmaee_tapi_cntx = {0};
#if(ZM_AEE_MTK_SOFTVERN >= 0x0924)
static U8 ZMAEE_DeliveryReplyStates[2] = {0};
static U8 ZMAEE_OrigDelieverReplyStates[2] = {0};
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
static U8 ZMAEE_CloseDeliveryFlag = 0;
static U8 ZMAEE_CloseDeliveryFlag2 = 0;
#endif

int  g_zmaee_call_state = 0;		// 0 - idle, 1 - outcoming call, 2 - incoming call
char g_zmaee_call_num[AEE_MAX_NUM_SIZE] = {0};
short g_zmaee_call_disp[AEE_MAX_FILE_NAME] = {0};


static ZMAEE_TAPI sg_zmaee_tapi = {0};
	

int ZMAEE_ITAPI_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_TAPI* pThis = 0;

	pThis = &sg_zmaee_tapi;

	pThis->pVtbl = ZMAEE_GetVtable(clsId);
	if(pThis->pVtbl == 0)
	{
		pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_ITAPI_FUNC_COUNT,
										ZMAEE_ITAPI_AddRef,
										ZMAEE_ITAPI_Release,
										ZMAEE_ITAPI_GetCallerID,
										ZMAEE_ITAPI_MakeVoiceCall,
										ZMAEE_ITAPI_SendSms,
										ZMAEE_ITAPI_SendPortSms,
										ZMAEE_ITAPI_SendDelaySms,
										ZMAEE_ITAPI_GetSmsCenter,
										ZMAEE_ITAPI_SetSmsCenter,
										ZMAEE_ITAPI_SimCardCount,
										ZMAEE_ITAPI_SetActiveSimCard,
										ZMAEE_ITAPI_GetImsi,
										ZMAEE_ITAPI_GetImei,
										ZMAEE_ITAPI_StartReportBSID,
										ZMAEE_ITAPI_StopReportBSID,
										ZMAEE_ITAPI_GetSimCardStatus,
										ZMAEE_ITAPI_GetActiveSimCard,
										ZMAEE_ITAPI_GetMessageCount,
										ZMAEE_ITAPI_GetSimServMsg,
										ZMAEE_ITAPI_GetICCID);
	}

	if(pThis->nRef == 0) {
		pThis->nRef = 1;
	} else {
		pThis->nRef++;
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

static
int ZMAEE_ITAPI_AddRef(AEE_ITAPI* po)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_ITAPI_Release(AEE_ITAPI* po)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef--;
	if(pThis->nRef == 0) {
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取正在通话中的电话号码
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	pDest					输出参数，获取电话号码的buf
 * 	nSize					输出参数，获取电话号码buf的字节长度
 * 返回:
 *	E_ZM_AEE_SUCCESS		if call in progress and buffer filled. 
 * 	E_ZM_AEE_BADPARAM  		pDest is NULL.
 * 	E_ZM_AEE_FAILURE  		if no call in progress or otherwise.  
 */
static
int ZMAEE_ITAPI_GetCallerID(AEE_ITAPI * po, const char* pDest, int nSize)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if((pThis == NULL) || (!pDest)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(g_zmaee_call_state == 0) {
		return E_ZM_AEE_FAILURE;
	}

	strncpy((char*)pDest, (const char*)g_zmaee_call_num, nSize - 1);

	return E_ZM_AEE_SUCCESS;
}


#ifdef ZMAEE_MAKEVOICE_WITH_NUMLIST
void ZMAEE_ITAPI_FreeNumberList(void)
{
	kal_prompt_trace(MOD_AUX,"ZMAEE_ITAPI_FreeNumberList");
	glb_zmaee_call_numberlist_index = 0;
	glb_zmaee_call_numberlist_max = 0;
	memset(glb_zmaee_call_numberlist, 0, ZMAEE_CALL_MAX*ZMAEE_CALL_LENGTH_MAX);
}

static void ZMAEE_CallNumber_Timer_Callback(void)
{
	glb_zmaee_call_numberlist_index++;

	if(glb_zmaee_call_numberlist_index < glb_zmaee_call_numberlist_max)
	{
		kal_prompt_trace(MOD_AUX,"ZMAEE_CallNumber_Timer_Callback:glb_zmaee_call_numberlist_index < glb_zmaee_call_numberlist_max");
		ZMAEE_ITAPI_MakeVoiceCall_List(glb_zmaee_call_numberlist[glb_zmaee_call_numberlist_index]);
	}
	else
	{
		kal_prompt_trace(MOD_AUX,"ZMAEE_CallNumber_Timer_Callback:ZMAEE_ITAPI_FreeNumberList");
		ZMAEE_ITAPI_FreeNumberList();
	}
}

void ZMAEE_ITAPI_CallNextNumber(void)
{
	kal_prompt_trace(MOD_AUX,"ZMAEE_ITAPI_CallNextNumber,glb_zmaee_call_numberlist_max=%d",glb_zmaee_call_numberlist_max);
	if(glb_zmaee_call_numberlist_max > 0)
		StartTimer(ZMAEE_PERIOD_TIMER, ZMAEE_CALL_TIME_SLICE, ZMAEE_CallNumber_Timer_Callback);
}

static void ZMAEE_ITAPI_PareNumberList(const char* number)
{
	int i = 0, j = 0, k = 0;

	memset(glb_zmaee_call_numberlist, 0, ZMAEE_CALL_MAX*ZMAEE_CALL_LENGTH_MAX);
	if(strlen(number) <= 0)
		return;
	
	for(i = 0; i < strlen(number); i++)
	{
		if(number[i] != '|')
		{
			glb_zmaee_call_numberlist[j][k] = number[i];
			k++;
			
		}
		else
		{
			j++;
			k = 0;
		}
	}
	glb_zmaee_call_numberlist_max = ++j;
	glb_zmaee_call_numberlist_index = 0;

	kal_prompt_trace(MOD_AUX,"ZMAEE_ITAPI_PareNumberList,glb_zmaee_call_numberlist_max=%d",glb_zmaee_call_numberlist_max);
}


static
int ZMAEE_ITAPI_MakeVoiceCall_List(const char* number)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0) 
		extern void MakeCall(PS8 strNumber);
		short ucs2_phone_num[MAX_CM_NUMBER + 1] = {0};
	
		if(strlen(number) > MAX_CM_NUMBER) {
			extern void ShowCallManagementErrorMessage(U16 cause);
			ShowCallManagementErrorMessage(ERR_INVALID_NUMBER_FORMAT);
			return E_ZM_AEE_BADPARAM;
		}
	
		// 选卡
		ZMAEE_ChangeChannel(0, sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);
		
		mmi_asc_n_to_ucs2((S8*)ucs2_phone_num, (S8*)number, (U32)MAX_CM_NUMBER);
	
		
		MakeCall((S8*)ucs2_phone_num);
		return E_ZM_AEE_SUCCESS;
#else
		mmi_ucm_make_call_para_struct param;
		short ucs2_phone_num[MAX_CM_NUMBER + 1] = {0};
		
		// 选卡
		kal_prompt_trace(MOD_AUX,"ZMAEE_ITAPI_MakeVoiceCall_2....IN");
		
		ZMAEE_ChangeChannel(0, sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);
		
		mmi_asc_n_to_ucs2((S8*)ucs2_phone_num, (S8*)number, (U32)MAX_CM_NUMBER);
		mmi_ucm_init_call_para(&param);
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == 0) {
			param.dial_type = SRV_UCM_VOICE_CALL_TYPE;
		} else {
			param.dial_type = SRV_UCM_VOICE_CALL_TYPE_SIM2;
		}
		param.ucs2_num_uri = (U16*)ucs2_phone_num;
		param.adv_para.after_make_call_callback = NULL;
		param.adv_para.after_callback_user_data = NULL;
		mmi_ucm_call_launch(0, &param);
		
		return E_ZM_AEE_SUCCESS;
#endif

}

#endif	// ZMAEE_MAKEVOICE_WITH_NUMLIST

/**
 * 打电话
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	number					ascii字符串的电话号码
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 *
 * 	E_ZM_AEE_SUCCESS		呼叫成功
 */
static
int ZMAEE_ITAPI_MakeVoiceCall(AEE_ITAPI* po, const char* number)
{


#ifdef ZMAEE_MAKEVOICE_WITH_NUMLIST
	ZMAEE_ITAPI_PareNumberList(number);
	if(glb_zmaee_call_numberlist_max > 0)
	{
		kal_prompt_trace(MOD_AUX,"ZMAEE_ITAPI_MakeVoiceCall.....IN,glb_zmaee_call_numberlist_index=%s",glb_zmaee_call_numberlist[glb_zmaee_call_numberlist_index]);
		return ZMAEE_ITAPI_MakeVoiceCall_List(glb_zmaee_call_numberlist[glb_zmaee_call_numberlist_index]);
	}
	return E_ZM_AEE_BADPARAM;
#else

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0) 
	extern void MakeCall(PS8 strNumber);
	short ucs2_phone_num[MAX_CM_NUMBER + 1] = {0};
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(strlen(number) > MAX_CM_NUMBER) {
        extern void ShowCallManagementErrorMessage(U16 cause);
        ShowCallManagementErrorMessage(ERR_INVALID_NUMBER_FORMAT);
		return E_ZM_AEE_BADPARAM;
	}

	// 选卡
	ZMAEE_ChangeChannel(0, sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);
	
	mmi_asc_n_to_ucs2((S8*)ucs2_phone_num, (S8*)number, (U32)MAX_CM_NUMBER);
	MakeCall((S8*)ucs2_phone_num);
	return E_ZM_AEE_SUCCESS;
#else
	mmi_ucm_make_call_para_struct param;
	short ucs2_phone_num[MAX_CM_NUMBER + 1] = {0};
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	// 选卡
	ZMAEE_ChangeChannel(0, sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);

	mmi_asc_n_to_ucs2((S8*)ucs2_phone_num, (S8*)number, (U32)MAX_CM_NUMBER);
	mmi_ucm_init_call_para(&param);
	if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == 0) {
		param.dial_type = SRV_UCM_VOICE_CALL_TYPE;
	} else {
		param.dial_type = SRV_UCM_VOICE_CALL_TYPE_SIM2;
	}
	param.ucs2_num_uri = (U16*)ucs2_phone_num;
	param.adv_para.after_make_call_callback = NULL;
	param.adv_para.after_callback_user_data = NULL;
	mmi_ucm_call_launch(0, &param);

	return E_ZM_AEE_SUCCESS;
#endif
#endif
}

/**
 * 发送短信
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	pszDst					接收方号码
 * 	pszMsg					utf8格式的短信内容
 * 	pfn						回调函数，报告短信发送结果
 * 	pUser					回调函数的用户数据
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_ITEMBUSY		短信模块繁忙
 * 	E_ZM_AEE_FAILURE		发送失败
 * 	E_ZM_AEE_SUCCESS		发送成功
 */
static
int ZMAEE_ITAPI_SendSms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, ZMAEE_PFNSMSSTATUS pfn, void* pUser)
{
	return ZMAEE_ITAPI_SendPortSms(po, pszDst, pszMsg, 0, pfn, pUser);
}

#if ((ZM_AEE_MTK_SOFTVERN >= 0x0924) && (ZM_AEE_MTK_SOFTVERN < 0x09B0))
static
void ZMAEE_ITAPI_GetReplayCB(void *data, module_type mod, U16 result)
{
	if(result == MMI_FRM_SMS_OK) {
		U8 *DrRp = (U8*)data;

		ZMAEE_OrigDelieverReplyStates[0] = DrRp[0];
		ZMAEE_OrigDelieverReplyStates[1] = DrRp[1];
	}
}
#endif

/**
 * 发送短信
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	pszDst					接收方号码
 * 	pszMsg					utf8格式的短信内容
 * 	pfn						回调函数，报告短信发送结果
 * 	nPort					端口号
 * 	pUser					回调函数的用户数据
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_ITEMBUSY		短信模块繁忙
 * 	E_ZM_AEE_FAILURE		发送失败
 * 	E_ZM_AEE_SUCCESS		发送成功
 */
static
int ZMAEE_ITAPI_SendPortSms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, int nPort, ZMAEE_PFNSMSSTATUS pfn, void* pUser)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if((pThis == NULL) || (pszDst == NULL) || (pszMsg == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->sms_info.is_sending == 1) {
		ZMAEE_DebugPrint("ZMAEE_ITAPI_SendPortSms: is_sending == 1");
		return E_ZM_AEE_ITEMBUSY;
	}

	if(ZMAEE_ITAPI_CanSendSms(po) != 1) {
		ZMAEE_DebugPrint("ZMAEE_ITAPI_SendPortSms: can send sms return false");
		return E_ZM_AEE_ITEMBUSY;
	}

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	{
		char *ucs2_text_buf = NULL;
		EMSData *pEMS;
		unsigned char result;
		unsigned short i;
		int utf8_text_len;
#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
		short ucs2_num[AEE_MAX_NUM_SIZE] = {0};
		unsigned short len;
		int num_len;
#endif
		int is_pure_ansi = E_ZM_AEE_FALSE;

		ReleaseEMSEditBuffer();
		pEMS = GetEMSDataForEdit(0, 1);
		if(pEMS == NULL) 
			return E_ZM_AEE_FAILURE;

#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
		g_msg_cntx.sendSaveStatus = SEND_SAVE_NIL;
		num_len = strlen(pszDst) > (sizeof(ucs2_num) / sizeof(ucs2_num[0]))? 
				(sizeof(ucs2_num) / sizeof(ucs2_num[0])): strlen(pszDst);
		mmi_asc_n_to_ucs2((S8*)ucs2_num, (S8*)pszDst, num_len);
		memset(g_msg_cntx.smsPhoneNumber, 0, sizeof(g_msg_cntx.smsPhoneNumber));
		mmi_ucs2cpy(g_msg_cntx.smsPhoneNumber, (const S8*)ucs2_num);
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x0924)
		PendingSaveSendData.totalSegments = 1;
		PendingSaveSendData.mti = SMSAL_MTI_SUBMIT;
		memset(PendingSaveSendData.TPUD, 0, sizeof(PendingSaveSendData.TPUD));
		
		for (i=1; i<mmi_msg_get_seg_num(); i++ ) {
			PendingSaveSendData.TPUDLen[i] = 0;
			PendingSaveSendData.TPUD_p[i] = NULL;
			PendingSaveSendData.l4index[i] = SMS_INVALID_INDEX;
			PendingSaveSendData.TPUD_udhi[i] = FALSE;
		}
		
#if (ZM_AEE_MTK_SOFTVERN < 0x0924)
		g_msg_cntx.sendMessageCase = SEND_CASE_SEND_ONLY;
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x0924)

		if(nPort != 0) {
			pEMS->PortNum.isPortNumSet = TRUE;
			pEMS->PortNum.dst_port = nPort;
			pEMS->PortNum.src_port = 0;
		}

		utf8_text_len = strlen(pszMsg);
		if(!memcmp(pszMsg, "zmasc", 5))
			is_pure_ansi = E_ZM_AEE_TRUE;

		ucs2_text_buf = (char*)OslMalloc((utf8_text_len + 2) * 2);
		if(ucs2_text_buf == NULL) {
			return E_ZM_AEE_FAILURE;
		}

		ucs2_text_buf[(utf8_text_len + 2) * 2 - 1] = 0;
		ucs2_text_buf[(utf8_text_len + 2) * 2 - 2] = 0;
		ZMAEE_Utf8_2_Ucs2((const char*)pszMsg, utf8_text_len, (unsigned short*)ucs2_text_buf, utf8_text_len + 1);

		if(!is_pure_ansi)
			result = AppendEMSString(
				#if (ZM_AEE_MTK_SOFTVERN >= 0x0824)
					IMM_INPUT_TYPE_SENTENCE,
				#else
					INPUT_TYPE_ALPHANUMERIC_SENTENCECASE, 
				#endif
					pEMS, (U8*)ucs2_text_buf, SMSAL_UCS2_DCS, NULL);
		else			
			result = AppendEMSString(
				#if (ZM_AEE_MTK_SOFTVERN >= 0x0824)
					IMM_INPUT_TYPE_SENTENCE,
				#else
					INPUT_TYPE_ALPHANUMERIC_SENTENCECASE, 
				#endif
					pEMS, (U8*)ucs2_text_buf + 10, SMSAL_DEFAULT_DCS, NULL);
				
		OslMfree(ucs2_text_buf);
		
		if(result) {
			mmi_frm_sms_send_struct *sendData = OslMalloc(sizeof(mmi_frm_sms_send_struct));

			if(sendData == NULL) {
				return E_ZM_AEE_FAILURE;
			}

			// 初始化参数
			sg_zmaee_tapi_cntx.sms_po = pThis;
			pThis->sms_info.is_sending = 1;
			ZMAEE_Callback_Init(&(pThis->sms_info.callback), (void*)pfn, pUser);

			// 选卡操作
			ZMAEE_ChangeChannel(1, sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);
			
	#if (ZM_AEE_MTK_SOFTVERN >= 0x0924)
			mmi_frm_sms_get_common_settings(ZMAEE_ITAPI_GetReplayCB, MOD_MMI);
			ZMAEE_DeliveryReplyStates[0] = 0;
			ZMAEE_DeliveryReplyStates[1] = 0;
			mmi_frm_sms_set_common_settings(NULL, MOD_MMI, ZMAEE_DeliveryReplyStates);
	#else	// (ZM_AEE_MTK_SOFTVERN >= 0x0924)	
			if(DeliveryRepyStates[0] == 1) {
				ZMAEE_CloseDeliveryFlag = 1;
				DeliveryRepyStates[0] = 0;
			}

			if(DeliveryRepyStates[1] == 1) {
				ZMAEE_CloseDeliveryFlag2 = 1;
				DeliveryRepyStates[1] = 0;
			}

			if(ZMAEE_CloseDeliveryFlag || ZMAEE_CloseDeliveryFlag2)
				mmi_frm_sms_set_common_settings(NULL, MOD_MMI, DeliveryRepyStates);
	#endif	// (ZM_AEE_MTK_SOFTVERN >= 0x0924)
			memset((S8*)sendData, 0, sizeof(mmi_frm_sms_send_struct));
			memset(sendData->number, 0, sizeof(sendData->number));
			strncpy((S8*)sendData->number, (S8*)pszDst, sizeof(sendData->number));

			sendData->sendcheck = MMI_FRM_SMS_SCR | MMI_FRM_SMS_SC | MMI_FRM_SMS_DA;
			
	#if (ZM_AEE_MTK_SOFTVERN >= 0x0924)
			sendData->sendrequire |= MMI_FRM_SMS_NOT_DISP_SENDING_ICON;
	#else	// (ZM_AEE_MTK_SOFTVERN >= 0x0924)
		#ifdef __MMI_DUAL_SIM_MASTER__
				sendData->sendrequire &= ~MMI_FRM_SMS_DISP_SIM_OPT; 
		#endif	//__MMI_DUAL_SIM_MASTER__
	#endif	// (ZM_AEE_MTK_SOFTVERN >= 0x0924)

			if(nPort != 0) {
				sendData->desport = nPort;
				sendData->srcport = 0;
			}

			mmi_frm_sms_send_sms(ZMAEE_ITAPI_SendSmsResp, MOD_MMI, sendData);
			OslMfree(sendData);
			
			return E_ZM_AEE_SUCCESS;
		}
	}
#else	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	{
		SMS_HANDLE send_handle;		
		short ucs2_num[AEE_MAX_NUM_SIZE] = {0};
		char *ucs2_text_buf = NULL;
		int utf8_text_len;
		int num_len, sim_idx, ucs2_len;
		int is_pure_ansi = E_ZM_AEE_FALSE;
		
		send_handle = srv_sms_get_send_handle();
		if (send_handle == NULL) {
			/* can not get handle */
			return E_ZM_AEE_FAILURE;
		}

		if ((U16)srv_sms_is_bg_send_action_busy() == MMI_TRUE) {
			/* background sending is busy */
			return E_ZM_AEE_ITEMBUSY;
		}

		num_len = strlen(pszDst) > (sizeof(ucs2_num) / sizeof(ucs2_num[0]))? 
				(sizeof(ucs2_num) / sizeof(ucs2_num[0])): strlen(pszDst);
		mmi_asc_n_to_ucs2((S8*)ucs2_num, (S8*)pszDst, num_len);
		
    	/* set address number */
    	srv_sms_set_address(send_handle, (S8*)ucs2_num);

		utf8_text_len = strlen(pszMsg);
		if(!memcmp(pszMsg, "zmasc", 5))
			is_pure_ansi = E_ZM_AEE_TRUE;

		ucs2_text_buf = (char*)OslMalloc((utf8_text_len + 1) * 2);
		if(ucs2_text_buf == NULL) {
			return E_ZM_AEE_FAILURE;
		}
		ucs2_text_buf[(utf8_text_len + 1) * 2 - 1] = 0;
		ucs2_text_buf[(utf8_text_len + 1) * 2 - 2] = 0;

		if((ucs2_len = ZMAEE_Utf8_2_Ucs2((const char*)pszMsg, utf8_text_len, (unsigned short*)ucs2_text_buf, utf8_text_len + 1)) < 0) {
			OslMfree(ucs2_text_buf);
			return E_ZM_AEE_FAILURE;
		}

		if(!is_pure_ansi) {
			srv_sms_set_content_dcs(send_handle, SRV_SMS_DCS_UCS2);		
			srv_sms_set_content(send_handle, (S8*)ucs2_text_buf, 2 * ucs2_len);		
		} else {
			srv_sms_set_content_dcs(send_handle, SRV_SMS_DCS_7BIT);
			srv_sms_set_content(send_handle, (S8*)ucs2_text_buf + 5 * 2, 2 *(ucs2_len - 5));
		}		

		// 初始化参数
		sg_zmaee_tapi_cntx.sms_po = pThis;
		pThis->sms_info.is_sending = 1;
		ZMAEE_Callback_Init(&(pThis->sms_info.callback), (void*)pfn, pUser);

		// 选卡操作
		sim_idx = sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id;
				
		if(sim_idx == 0) {
	    	/* set SIM1 */
	    	srv_sms_set_sim_id(send_handle, SRV_SMS_SIM_1);
		} else {
			/* set SIM1 */
    		srv_sms_set_sim_id(send_handle, SRV_SMS_SIM_2);
		}

		srv_sms_set_no_sending_icon(send_handle);
		srv_sms_set_send_type(send_handle, SRV_SMS_BG_SEND);

		if(nPort != 0) {
			srv_sms_set_data_port(send_handle, 0, nPort);
		}
		srv_sms_set_status_report(send_handle, MMI_FALSE);
		
		/* send request */
		srv_sms_send_msg(send_handle, ZMAEE_ITAPI_SendSmsResp, NULL);

		if(ucs2_text_buf)
			OslMfree(ucs2_text_buf);
		return E_ZM_AEE_SUCCESS;
	}
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

	return E_ZM_AEE_FAILURE;
}


/**
 * 获取短消息中心号码
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	sim_idx					0 - sim1, 1 - sim2
 * 	pSmsC					输出参数，用来保存短消息中心号码
 * 	maxLen					保存短消息中心号码的buf
 * 返回:
 * 	E_ZM_AEE_SUCCESS		if successful.
 * 	E_ZM_AEE_BADPARAM 	 	if pSmsC is NULL.
 * 	E_ZM_AEE_FAILURE	 	if otherwise.
 */
static
int ZMAEE_ITAPI_GetSmsCenter(AEE_ITAPI* po, int sim_idx, char* pSmsC, int maxLen)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if((pThis == NULL) || (pSmsC == NULL) || (maxLen <= 0)) {
		return E_ZM_AEE_BADPARAM;
	}

	if((sim_idx < 0) || (sim_idx >= ZMAEE_ITAPI_SimCardCount(po))) {
		return E_ZM_AEE_BADPARAM;
	}

	if(sim_idx == 0)
	{
		strncpy(pSmsC, sg_zmaee_tapi_cntx.sc_addr1, maxLen);
	}
#ifdef __MMI_DUAL_SIM_MASTER__
	else if(sim_idx == 1)
	{
		strncpy(pSmsC, sg_zmaee_tapi_cntx.sc_addr2, maxLen);
	}
#endif

	if(strlen(pSmsC) == 0) {
		ZMAEE_ITAPI_InitSCADDR();
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置短消息中心号码
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	sim_idx					0 - sim1, 1 - sim2
 * 	pSmsC					设置的短消息中心号码
 * 返回:
 * 
 */
static
int ZMAEE_ITAPI_SetSmsCenter(AEE_ITAPI* po, int sim_idx, const char* pSmsC)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	char  asc_sc_addr[AEE_MAX_NUM_SIZE] = {0};
	int prefix_len, convert_len;

	if((pThis == NULL) || (pSmsC == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if((sim_idx < 0) || (sim_idx >= ZMAEE_ITAPI_SimCardCount(po))) {
		return E_ZM_AEE_BADPARAM;
	}

	// 短消息中心号码规范化
	if(memcmp(pSmsC, "+86", 3) == 0) {
		prefix_len = 0;
	} else if(memcmp(pSmsC, "86", 2) == 0) {
		prefix_len = 1;
		strcat((CHAR*)asc_sc_addr, (CHAR*)"+");
	} else {
		prefix_len = 3;
		strcat((CHAR*)asc_sc_addr, (CHAR*)"+86");
	}

	// 计算转换长度，避免内存溢出
	convert_len = strlen(pSmsC);
	if(convert_len > sizeof(asc_sc_addr) - prefix_len)
		convert_len = sizeof(asc_sc_addr) - prefix_len;

	memcpy((CHAR*)&asc_sc_addr[prefix_len], (CHAR*)pSmsC, convert_len);

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	{
		MMI_FRM_SMS_SET_PROFILE_PARAMS_REQ_STRUCT *msgReq = NULL;
		msgReq = (MMI_FRM_SMS_SET_PROFILE_PARAMS_REQ_STRUCT*)OslConstructDataPtr(sizeof(MMI_FRM_SMS_SET_PROFILE_PARAMS_REQ_STRUCT));

		msgReq->profile_no = 0; 
		msgReq->para_ind = SMSAL_PARA_SCA;

		msgReq->sc_addr.type = CSMCC_INTERNATIONAL_ADDR;
		msgReq->sc_addr.length = (strlen(asc_sc_addr) - 1 > MAX_DIGITS_SMS)? 
									(MAX_DIGITS_SMS - 1): strlen(asc_sc_addr) - 1;
		memcpy(msgReq->sc_addr.number, asc_sc_addr + 1, msgReq->sc_addr.length);

		if(sim_idx == 0)
		{
			memset(sg_zmaee_tapi_cntx.sc_addr1, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr1));
			memcpy(sg_zmaee_tapi_cntx.sc_addr1, asc_sc_addr + 1, msgReq->sc_addr.length);
		
			sg_zmaee_tapi_cntx.is_setting_scaddr = 1;
			mmi_frm_sms_send_message(
				MOD_MMI,
				MOD_L4C,
				0,
				PRT_MSG_ID_MMI_SMS_SET_PROFILE_PARAMS_REQ,
				(oslParaType*) msgReq,
				NULL);
		}
#ifdef __MMI_DUAL_SIM_MASTER__
		else if(sim_idx == 1)
		{
			memset(sg_zmaee_tapi_cntx.sc_addr2, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr2));
			memcpy(sg_zmaee_tapi_cntx.sc_addr2, asc_sc_addr, msgReq->sc_addr.length);
			
			sg_zmaee_tapi_cntx.is_setting_scaddr = 1;
			mmi_frm_sms_send_message(
				MOD_MMI,
				MOD_L4C_2,
				0,
				PRT_MSG_ID_MMI_SMS_SET_PROFILE_PARAMS_REQ,
				(oslParaType*) msgReq,
				NULL);
		}
#endif	// __MMI_DUAL_SIM_MASTER__
	}
#else	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	{
		srv_sms_sim_enum dual_sim;
		char *update_sc_addr_ptr;
		int  update_sc_addr_size;
		short ucs2_sc_addr[AEE_MAX_NUM_SIZE] = {0};

		mmi_asc_to_ucs2((S8*)ucs2_sc_addr, asc_sc_addr);
		if(sim_idx == 0)
		{
			dual_sim = SRV_SMS_SIM_1;
			update_sc_addr_ptr = sg_zmaee_tapi_cntx.sc_addr1;
			update_sc_addr_size = sizeof(sg_zmaee_tapi_cntx.sc_addr1);
		}
#ifdef __MMI_DUAL_SIM_MASTER__
		else if(sim_idx == 1)
		{
			dual_sim = SRV_SMS_SIM_2;
			update_sc_addr_ptr = sg_zmaee_tapi_cntx.sc_addr2;
			update_sc_addr_size = sizeof(sg_zmaee_tapi_cntx.sc_addr2);
		}
#endif	// __MMI_DUAL_SIM_MASTER__
		
		srv_sms_set_setting_para(
			SRV_SMS_ACTIVE_SC_ADDR,
			(void*)ucs2_sc_addr,
			dual_sim,
			(dual_sim == SRV_SMS_SIM_1)? ZMAEE_ITAPI_SETSCADDR1_Resp: ZMAEE_ITAPI_SETSCADDR2_Resp,
			NULL);

		memset(update_sc_addr_ptr, 0, update_sc_addr_size);
		strncpy(update_sc_addr_ptr, asc_sc_addr, update_sc_addr_size);
	}
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取手机支持的SIM卡个数
 * 参数:
 * 	po						AEE_ITAPI实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	> 0						支持的SIM卡个数
 */
static
int ZMAEE_ITAPI_SimCardCount(AEE_ITAPI* po)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return 0;
	}

#ifdef __MMI_DUAL_SIM_MASTER__
	return 2;
#else
	return 1;
#endif
}

/**
 * 设置当前使用的SIM卡
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	sim_idx					sim卡索引，0 - sim1, 1 - sim2
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		设置失败，该卡无效
 * 	E_ZM_AEE_SUCCESS		设置成功
 */
int ZMAEE_ITAPI_SetActiveSimCard(AEE_ITAPI* po, int sim_idx)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	int i;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(sg_zmaee_tapi_cntx.sim_info.sim_count <= 0) {
		return E_ZM_AEE_FAILURE;
	}

	for(i = 0; i < sg_zmaee_tapi_cntx.sim_info.sim_count; i++) {
		if(sim_idx == sg_zmaee_tapi_cntx.sim_info.sim_status[i].sim_id) {
			if(sg_zmaee_tapi_cntx.sim_info.sim_active != i) {
				ZMAEE_NVRAMINFO nvram_info = {0};
				int ret;
				S16 err_code;
				
				ret = ReadRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
				if((ret != sizeof(nvram_info)) || (err_code != NVRAM_READ_SUCCESS)) {
					return E_ZM_AEE_FAILURE;
				}

				nvram_info.sim_idx = sim_idx;
				ret = WriteRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
				if((ret != sizeof(nvram_info)) || (err_code != NVRAM_WRITE_SUCCESS)) {
					return E_ZM_AEE_FAILURE;
				}

				sg_zmaee_tapi_cntx.sim_info.sim_active = i;
			}
			return E_ZM_AEE_SUCCESS;
		}
	}

	return E_ZM_AEE_FAILURE;
}

/**
 * 获取IMSI号
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	sim_idx					0 - sim1，1 - sim2
 * 	pImsi					IMSI的buf
 * 	maxLen					IMSI的buf字节
 * 返回:
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	E_ZM_AEE_SUCCESS		获取成功
 */
int ZMAEE_ITAPI_GetImsi(AEE_ITAPI* po, int sim_idx, char* pImsi, int maxLen)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if((pThis == NULL) || (!pImsi) || (maxLen <= 0)) {
		return E_ZM_AEE_BADPARAM;
	}

	if((sim_idx < 0) || (sim_idx >= ZMAEE_ITAPI_SimCardCount(po))) {
		return E_ZM_AEE_BADPARAM;
	}
	if(sim_idx == 0)
	{
		strncpy(pImsi, sg_zmaee_tapi_cntx.imsi_buf1, maxLen);
	}
#ifdef __MMI_DUAL_SIM_MASTER__
	else if(sim_idx == 1)
	{
		strncpy(pImsi, sg_zmaee_tapi_cntx.imsi_buf2, maxLen);
	}
#if (MMI_MAX_SIM_NUM >= 3)
	else if(sim_idx == 2)
	{
		strncpy(pImsi, sg_zmaee_tapi_cntx.imsi_buf3, maxLen);	
	}
#endif
#if (MMI_MAX_SIM_NUM >= 4)
	else if(sim_idx == 3)
	{
		strncpy(pImsi, sg_zmaee_tapi_cntx.imsi_buf4, maxLen);
	}
#endif
#endif

	if(strlen(pImsi) == 0) {
		ZMAEE_ITAPI_InitIMSI();
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取IMEI号
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	pImei					IMEI号buf
 * 	maxLen					IMEI号buf字节长度
 * 返回:
 * 	
 */
static
int ZMAEE_ITAPI_GetImei(AEE_ITAPI* po, char* pImei, int maxLen)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL || pImei == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(sg_zmaee_tapi_cntx.imei_buf[0] == 0) {
		ZMAEE_ITAPI_InitIMEI();
		return E_ZM_AEE_FAILURE;
	}

	strncpy(pImei, (const char*)sg_zmaee_tapi_cntx.imei_buf, maxLen - 1);
	if(strlen(pImei) == 0) {
		ZMAEE_ITAPI_InitIMEI();
		return E_ZM_AEE_FAILURE;
	}
	
	return E_ZM_AEE_SUCCESS;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
int ZMAEE_Tapi_BSID_CB(int nHandle, void* pUser)
{
	if(sg_zmaee_tapi.bsid_stop == 0 && sg_zmaee_tapi.bsid_request) {
		if(ZMAEE_Callback_Valid(&sg_zmaee_tapi.bsid_callback)) {
			ZMAEE_PFNBSIDCB pfn = (ZMAEE_PFNBSIDCB)sg_zmaee_tapi.bsid_callback.pfn;
			void *pUser = sg_zmaee_tapi.bsid_callback.pUser;
			ZMAEE_BSID_INFO bsid_info = {0};
			char temp[4] = {0};

			bsid_info.nCellId = sg_zmaee_tapi.bsid_info.cell_id;
			bsid_info.nLac = sg_zmaee_tapi.bsid_info.lac;
			memcpy(temp, sg_zmaee_tapi.bsid_info.plmn, 3);
			bsid_info.nMcc = atoi(temp);
			memset(temp, 0, sizeof(temp));
			memcpy(temp, sg_zmaee_tapi.bsid_info.plmn + 3, 2);
			bsid_info.nMnc = atoi(temp);

			pfn(pUser, 1, &bsid_info);
		}
		sg_zmaee_tapi.bsid_request = 0;
	}
	return 0;
}
#endif

/**
 * 开始定位
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	pfn						回调函数
 * 	pUser					用户参数
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		不支持该接口
 * 	E_ZM_AEE_SUCCESS		定位请求成功
 */
static
int ZMAEE_ITAPI_StartReportBSID(AEE_ITAPI* po, ZMAEE_PFNBSIDCB pfn, void* pUser)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	int sim_idx;
	ZMAEE_TAPI* pThis = (ZMAEE_TAPI*)po;

	if((pThis == NULL) || (pfn == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->bsid_info.is_request == 1) {
		return E_ZM_AEE_FAILURE;
	}

	// 设置回调参数
	sg_zmaee_tapi_cntx.bsid_po = pThis;
	pThis->bsid_info.is_request = 1;
	pThis->bsid_info.disable_notify = 1;
	ZMAEE_Callback_Init(&(pThis->bsid_info.callback), (void*)pfn, pUser);

	#ifndef WIN32	
		sim_idx = sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id;

		ZMAEE_ITAPI_EMSendStartReq(sim_idx);
	#endif	// #ifndef WIN32

#else	
	ZMAEE_TAPI* pThis = (ZMAEE_TAPI*)po;
	mmi_sim_enum sim_enum;

	if(pThis->bsid_request)
		return E_ZM_AEE_ITEMBUSY;

	switch(ZMAEE_ITAPI_GetActiveSimCard(po)) {
		case 0:
			sim_enum = MMI_SIM1;
			break;

	#ifdef __MMI_DUAL_SIM_MASTER__
		case 1:
			sim_enum = MMI_SIM2;
			break;

	#if (MMI_MAX_SIM_NUM >= 3)
		case 2:
			sim_enum = MMI_SIM3;
			break;
	#endif

	#if (MMI_MAX_SIM_NUM >= 4)
		case 3:
			sim_enum = MMI_SIM4;
			break;
	#endif
	#endif

		default:
			return E_ZM_AEE_FAILURE;
	}

	memset(&pThis->bsid_info, 0, sizeof(pThis->bsid_info));
	if(srv_nw_info_get_location_info(sim_enum, &pThis->bsid_info) != MMI_TRUE)
		return E_ZM_AEE_FAILURE;

	pThis->bsid_request = 1;
	pThis->bsid_stop = 0;
	ZMAEE_Callback_Init(&(pThis->bsid_callback), (void*)pfn, pUser);

	ZMAEE_StartPeriodHandler(0, NULL, ZMAEE_Tapi_BSID_CB);
#endif
	return E_ZM_AEE_SUCCESS;
}

/**
 * 停止定位
 * 参数:
 * 	po						AEE_ITAPI实例
 * 返回:
 *
 */
static
int ZMAEE_ITAPI_StopReportBSID(AEE_ITAPI* po)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
//#ifdef __MMI_EM_NW_NETWORK_INFO__
#if 1
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	int sim_idx;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->bsid_info.is_request == 0) {
		return E_ZM_AEE_SUCCESS;
	}

	if(pThis->bsid_info.disable_notify) {
		pThis->bsid_info.delay_stop = 1;
		return E_ZM_AEE_SUCCESS;
	}

	sg_zmaee_tapi_cntx.bsid_po = NULL;
	pThis->bsid_info.is_request = 0;
	pThis->bsid_info.disable_notify = 1;
	memset(&(pThis->bsid_info.callback), 0, sizeof(pThis->bsid_info.callback));

#ifndef WIN32
		//currentEmCommand = EM_STOP;
	#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
	
	#else
		//currentEmMode = 0;	
	#endif

	sim_idx = sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id;
	

	#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)	
		
	#else	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)
		//ZMAEE_SetProtocolEventHandler(0, MSG_ID_EM_START_RSP, EngineerModeStartRes);
	#endif	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)

		ZMAEE_ITAPI_EMSendStopReq();
		return E_ZM_AEE_SUCCESS;
#else	// #ifndef WIN32
	return E_ZM_AEE_FAILURE;
#endif
#else	// __MMI_EM_NW_NETWORK_INFO__
	return E_ZM_AEE_CLASSNOTSUPPORT;
#endif	// __MMI_EM_NW_NETWORK_INFO__

#else
	sg_zmaee_tapi.bsid_request = 0;
	sg_zmaee_tapi.bsid_stop = 1;
	return E_ZM_AEE_SUCCESS;
#endif
}

/**
 * 获取SIM卡的状态
 * 参数:
 * 	po						AEE_ITAPI实例
 * 	sim_idx					0 - sim1, 1 - sim2
 * 返回:
 * 	0						SIM卡有效
 * 	-1						SIM卡无效
 */
static
int ZMAEE_ITAPI_GetSimCardStatus(AEE_ITAPI* po, int sim_idx)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	int i;

	if((pThis == NULL)) {
		return -1;
	}

	if((sim_idx < 0) || (sim_idx >= ZMAEE_ITAPI_SimCardCount(po))) {
		return -1;
	}

	for(i = 0; i < sg_zmaee_tapi_cntx.sim_info.sim_count; i++) {
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[i].sim_id == sim_idx) {
			return 0;
		}
	}

	return -1;
}

/**
 * 获取激活卡
 * 参数:
 * 	po						AEE_ITAPI实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 * 	0						sim1
 * 	1						sim2
 */
int ZMAEE_ITAPI_GetActiveSimCard(AEE_ITAPI * po)
{
	ZMAEE_TAPI* pThis = (ZMAEE_TAPI*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	return sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id;
}

/**
 * 获取未读短信、未接电话的个数
 * @type		0 - 未读短信，10 - 未接电话
 * RETURN:
 * >= 0		个数
 * < 0		错误代码
 */
static
int ZMAEE_ITAPI_GetMessageCount(AEE_ITAPI* po, int type)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern call_history_context_struct *chis_p;
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)	
	extern U16 mmi_frm_sms_get_unread_sms_num(void);
#else
	extern U16 srv_sms_get_unread_sms_num(void);
#endif

#ifdef __MMI_DUAL_SIM_MASTER__
	extern MTPNP_UINT8 MTPNP_PFAL_chist_get_num_missed_call_before_view(void);
#endif

	if(!po)
		return E_ZM_AEE_BADPARAM;

	switch(type) {
		case 0:
		{
		#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)	
			int count = mmi_frm_sms_get_unread_sms_num();
		#else
			int count = srv_sms_get_unread_sms_num();
		#endif

			if(count < 0)
				count = 0;
			
			return count;
		}
		case 10:
		{
			int count = chis_p->nMissedCallBeforeView
	#ifdef __MMI_DUAL_SIM_MASTER__
				+ MTPNP_PFAL_chist_get_num_missed_call_before_view()
	#endif
				;

			if(count < 0)
				count = 0;

			return count;
		}
		default:
			return E_ZM_AEE_BADPARAM;
	}
#else
	switch(type) {
		case 0:
		{
			return srv_sms_get_unread_sms_num();
		}
			break;

		case 10:
		{
			return srv_clog_get_unread_missed_call_num();
		}
			break;

		default:
			return E_ZM_AEE_BADPARAM;
	}

#endif
}

/**
 * 获取SIM卡的待机界面服务信息内容
 * @ sim_idx			0 - sim1, 1 - sim2
 * @ serv_msg		服务信息内容
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	po, serv_msg is null, sim_idx is invalid
 * 	E_ZM_AEE_FAILURE	获取失败
 * 	E_ZM_AEE_SUCCESS	获取成功
 */
static
int ZMAEE_ITAPI_GetSimServMsg(AEE_ITAPI *po, int sim_idx, unsigned char *buf, int buf_size)
{
	if(!po || !buf || (buf_size <= 0))
		return E_ZM_AEE_BADPARAM;

	if(sim_idx == 0)
	{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		extern U8 gNWProviderName[MAX_LENGTH_DISPLAY_NAME * ENCODING_LENGTH];
		mmi_ucs2ncpy((S8*)buf, (const S8*)gNWProviderName, (U32)buf_size / 2 - 1);
#elif (ZM_AEE_MTK_SOFTVERN < 0x11A0)

	#ifdef MT6252	// 1108的代码走这里
		extern mmi_idle_obj_struct *mmi_idle_get_obj(void);
		extern void mmi_idle_update_service_indication_context(mmi_idle_obj_struct *obj);
		extern mmi_idle_service_indication_struct *mmi_idle_get_service_indication_context(mmi_idle_obj_struct *obj,mmi_sim_enum sim);
		mmi_idle_service_indication_struct *service_indication;
		mmi_idle_obj_struct* obj = mmi_idle_get_obj();
		if(obj == NULL)
			return E_ZM_AEE_FAILURE;
		mmi_idle_update_service_indication_context(obj);
		service_indication = mmi_idle_get_service_indication_context(obj, MMI_SIM1);
		
		if(mmi_wcslen(service_indication->line1) > 0)
			mmi_wcsncpy((WCHAR*)buf, service_indication->line1, buf_size / 2 - 1);
		else if(mmi_wcslen(service_indication->line2) > 0)
			mmi_wcsncpy((WCHAR*)buf, service_indication->line2, buf_size / 2 - 1);
			
	#else
		extern void mmi_idle_prepare_service_string(mmi_idle_obj_struct *obj);
		extern mmi_idle_obj_struct *mmi_idle_get_obj(void);
		extern mmi_idle_service_area_struct *mmi_idle_get_service_area_context(mmi_idle_obj_struct *obj,mmi_sim_enum sim);
		mmi_idle_service_area_struct* parea = NULL;
		mmi_idle_obj_struct* pObj = mmi_idle_get_obj();
		if(pObj == NULL)
			return E_ZM_AEE_FAILURE;
		mmi_idle_prepare_service_string(pObj);
		parea = mmi_idle_get_service_area_context(pObj, MMI_SIM1);
		if(parea->nw_name != NULL)
			mmi_wcsncpy((WCHAR*)buf, parea->nw_name, buf_size / 2 - 1);
	#endif

#else		
		extern void srv_nw_name_get_service_indication_string(mmi_sim_enum sim,srv_nw_name_service_indication_struct *service_indication);
		srv_nw_name_service_indication_struct tmp_indication;
		
		srv_nw_name_get_service_indication_string(MMI_SIM1, &tmp_indication);
		if(mmi_wcslen(tmp_indication.line1) > 0)
			mmi_wcsncpy((WCHAR*)buf, tmp_indication.line1, buf_size / 2 - 1);
		else if(mmi_wcslen(tmp_indication.line2) > 0)
			mmi_wcsncpy((WCHAR*)buf, tmp_indication.line2, buf_size / 2 - 1);
#endif
	}
#ifdef __MMI_DUAL_SIM_MASTER__
	else if(sim_idx == 1)
	{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		extern MTPNP_UCHAR g_slave_nw_providername[MAX_LENGTH_DISPLAY_NAME * ENCODING_LENGTH];
		mmi_ucs2ncpy((S8*)buf, (const S8*)g_slave_nw_providername, (U32)buf_size / 2 - 1);
#elif (ZM_AEE_MTK_SOFTVERN < 0x11A0)

	#ifdef MT6252	// 1108的代码走这里
		extern mmi_idle_obj_struct *mmi_idle_get_obj(void);
		extern void mmi_idle_update_service_indication_context(mmi_idle_obj_struct *obj);
		extern mmi_idle_service_indication_struct *mmi_idle_get_service_indication_context(mmi_idle_obj_struct *obj,mmi_sim_enum sim);
		mmi_idle_service_indication_struct *service_indication;
		mmi_idle_obj_struct* obj = mmi_idle_get_obj();
		if(obj == NULL)
			return E_ZM_AEE_FAILURE;
		mmi_idle_update_service_indication_context(obj);
		service_indication = mmi_idle_get_service_indication_context(obj, MMI_SIM2);
		
		if(mmi_wcslen(service_indication->line1) > 0)
			mmi_wcsncpy((WCHAR*)buf, service_indication->line1, buf_size / 2 - 1);
		else if(mmi_wcslen(service_indication->line2) > 0)
			mmi_wcsncpy((WCHAR*)buf, service_indication->line2, buf_size / 2 - 1);
	#else
		extern void mmi_idle_prepare_service_string(mmi_idle_obj_struct *obj);
		extern mmi_idle_obj_struct *mmi_idle_get_obj(void);
		extern mmi_idle_service_area_struct *mmi_idle_get_service_area_context(mmi_idle_obj_struct *obj,mmi_sim_enum sim);
		mmi_idle_service_area_struct* parea = NULL;
		mmi_idle_obj_struct* pObj = mmi_idle_get_obj();
		if(pObj == NULL)
			return E_ZM_AEE_FAILURE;
		mmi_idle_prepare_service_string(pObj);
		parea = mmi_idle_get_service_area_context(pObj, MMI_SIM2);
		if(parea->nw_name != NULL)
			mmi_wcsncpy((WCHAR*)buf, parea->nw_name, buf_size / 2 - 1);

	#endif
#else
		extern void srv_nw_name_get_service_indication_string(mmi_sim_enum sim,srv_nw_name_service_indication_struct *service_indication);
		srv_nw_name_service_indication_struct tmp_indication;
		
		srv_nw_name_get_service_indication_string(MMI_SIM2, &tmp_indication);
		if(mmi_wcslen(tmp_indication.line1) > 0)
			mmi_wcsncpy((WCHAR*)buf, tmp_indication.line1, buf_size / 2 - 1);
		else if(mmi_wcslen(tmp_indication.line2) > 0)
			mmi_wcsncpy((WCHAR*)buf, tmp_indication.line2, buf_size / 2 - 1);

#endif
	}
#endif
	else
	{
		return E_ZM_AEE_BADPARAM;
	}

	return E_ZM_AEE_SUCCESS;
}


/**
 * 初始化SIM信息,每次进平台时调用
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
void ZMAEE_ITAPI_InitSIMInfo(void)
{
	ZMAEE_NVRAMINFO nvram_info = {0};
	S16 err_code;
	int ret, prefer_sim_idx;
	memset(&sg_zmaee_tapi_cntx.sim_info, 0, sizeof(sg_zmaee_tapi_cntx.sim_info));

	ret = ReadRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
	if((ret != sizeof(nvram_info)) || (err_code != NVRAM_READ_SUCCESS)) {
		prefer_sim_idx = 0;
	} else {
		prefer_sim_idx = nvram_info.sim_idx;
	}

#if !defined (MT6252) && (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#ifndef __MMI_DUAL_SIM_MASTER__
	if((!mmi_bootup_is_sim_removed())&& (mmi_bootup_is_sim_valid()) 
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		&& (g_pwr_context.CurrentServiceType == FULL_SERVICE)
	#else
		&& (srv_nw_info_get_service_availability(MMI_SIM1) == SRV_NW_INFO_SA_FULL_SERVICE)
	#endif
		) {
		sg_zmaee_tapi_cntx.sim_info.sim_count = 1;
		sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id = ZMAEE_SIM_TYPE_1;
		sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_service = g_pwr_context.CurrentServiceType;
		sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
	}
#else
	{
		E_MTPNP_AD_NVRAM_STATUS boot_mode;
		boot_mode = MTPNP_AD_Get_Startup_Mode();

		if(boot_mode == MTPNP_AD_NVRAM_DUALCARD) {
			if((!mmi_bootup_is_sim_removed())&& (mmi_bootup_is_sim_valid())
			#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
				&& (g_pwr_context.CurrentServiceType == FULL_SERVICE)
			#else
				&& (srv_nw_info_get_service_availability(MMI_SIM1) == SRV_NW_INFO_SA_FULL_SERVICE)
			#endif
				)
			{
				sg_zmaee_tapi_cntx.sim_info.sim_count++;
				sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count - 1].sim_id = ZMAEE_SIM_TYPE_1;
			}

			if((!mmi_bootup_is_sim2_removed()) && (mmi_bootup_is_sim2_valid())
			#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
				&& (g_pwr_context_2.CurrentServiceType == FULL_SERVICE)
			#else
				&& (srv_nw_info_get_service_availability(MMI_SIM2) == SRV_NW_INFO_SA_FULL_SERVICE)
			#endif
				)
			{
				sg_zmaee_tapi_cntx.sim_info.sim_count++;
				sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count - 1].sim_id = ZMAEE_SIM_TYPE_2;
			}

			if(sg_zmaee_tapi_cntx.sim_info.sim_count == 1) {
				sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
			} else if(sg_zmaee_tapi_cntx.sim_info.sim_count > 1) {
				sg_zmaee_tapi_cntx.sim_info.sim_active = prefer_sim_idx;
			}
		} else if(boot_mode == MTPNP_AD_NVRAM_CARD1) {
			if((!mmi_bootup_is_sim_removed())&& (mmi_bootup_is_sim_valid())
			#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
				&& (g_pwr_context.CurrentServiceType == FULL_SERVICE)
			#else
				&& (srv_nw_info_get_service_availability(MMI_SIM1) == SRV_NW_INFO_SA_FULL_SERVICE)
			#endif
				)
			{
				sg_zmaee_tapi_cntx.sim_info.sim_count = 1;
				sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id = ZMAEE_SIM_TYPE_1;
				sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
			}
		} else if(boot_mode == MTPNP_AD_NVRAM_CARD2) {		
			if((!mmi_bootup_is_sim2_removed()) && (mmi_bootup_is_sim2_valid())
			#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
				&& (g_pwr_context_2.CurrentServiceType == FULL_SERVICE)
			#else
				&& (srv_nw_info_get_service_availability(MMI_SIM2) == SRV_NW_INFO_SA_FULL_SERVICE)
			#endif
				)
			{
				sg_zmaee_tapi_cntx.sim_info.sim_count = 1;
				sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id = ZMAEE_SIM_TYPE_2;
				sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
			}
		}
	}
#endif

#else	// #if !defined (MT6252) && (ZM_AEE_MTK_SOFTVERN < 0x11A0)

#ifndef __MMI_DUAL_SIM_MASTER__
	if(srv_sim_ctrl_is_available(MMI_SIM1) && (srv_nw_info_get_service_availability(MMI_SIM1) == SRV_NW_INFO_SA_FULL_SERVICE)) {
		sg_zmaee_tapi_cntx.sim_info.sim_count = 1;
		sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id = ZMAEE_SIM_TYPE_1;//BLJ  sim_id = sim_idS
		sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
	}
#else
	{
		if((srv_mode_switch_get_network_mode(MMI_SIM1) == SRV_MODE_SWITCH_ON) && (srv_sim_ctrl_is_available(MMI_SIM1)) && (srv_nw_info_get_service_availability(MMI_SIM1) == SRV_NW_INFO_SA_FULL_SERVICE)) {
			sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count].sim_id = ZMAEE_SIM_TYPE_1;
			sg_zmaee_tapi_cntx.sim_info.sim_count++;
		}

		if((srv_mode_switch_get_network_mode(MMI_SIM2) == SRV_MODE_SWITCH_ON) && (srv_sim_ctrl_is_available(MMI_SIM2)) && (srv_nw_info_get_service_availability(MMI_SIM2) == SRV_NW_INFO_SA_FULL_SERVICE)) {
			sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count].sim_id = ZMAEE_SIM_TYPE_2;
			sg_zmaee_tapi_cntx.sim_info.sim_count++;
		}

	#if (MMI_MAX_SIM_NUM >= 3)
		if((srv_mode_switch_get_network_mode(MMI_SIM3) == SRV_MODE_SWITCH_ON) && (srv_sim_ctrl_is_available(MMI_SIM3)) && (srv_nw_info_get_service_availability(MMI_SIM3) == SRV_NW_INFO_SA_FULL_SERVICE)) {
			sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count].sim_id = ZMAEE_SIM_TYPE_3;
			sg_zmaee_tapi_cntx.sim_info.sim_count++;
		}
	#endif

	#if (MMI_MAX_SIM_NUM >= 4)
		if((srv_mode_switch_get_network_mode(MMI_SIM4) == SRV_MODE_SWITCH_ON) && (srv_sim_ctrl_is_available(MMI_SIM4)) && (srv_nw_info_get_service_availability(MMI_SIM4) == SRV_NW_INFO_SA_FULL_SERVICE)) {
			sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_count].sim_id = ZMAEE_SIM_TYPE_4;
			sg_zmaee_tapi_cntx.sim_info.sim_count++;
		}
	#endif

		{
			int i;

			for(i = 0; i < sg_zmaee_tapi_cntx.sim_info.sim_count; i++) {
				if(prefer_sim_idx == sg_zmaee_tapi_cntx.sim_info.sim_status[i].sim_id) {
					sg_zmaee_tapi_cntx.sim_info.sim_active = i;
					return;
				}
			}

			sg_zmaee_tapi_cntx.sim_info.sim_active = 0;
		}
	}
#endif
	
#endif
}

/**
 * 开机初始化IMEI号
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
void ZMAEE_ITAPI_InitIMEI(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)

	unsigned char imei_buf[9] = {0};

	memset(sg_zmaee_tapi_cntx.imei_buf, 0, sizeof(sg_zmaee_tapi_cntx.imei_buf));

#if (ZM_AEE_MTK_SOFTVERN >= 0x0912)
	if (nvram_get_imei_value(sizeof(imei_buf) - 1, imei_buf, 1))
#else		
	if (nvram_get_imei_value(sizeof(imei_buf) - 1, imei_buf))
#endif
	{
		imei_buf[7] = imei_buf[7] | 0xf0;
		convert_to_digit(imei_buf, (U8*)sg_zmaee_tapi_cntx.imei_buf);
	}	

#else
	mmi_sim_enum sim_enum;

	memset(sg_zmaee_tapi_cntx.imei_buf, 0, sizeof(sg_zmaee_tapi_cntx.imei_buf));
	if(srv_imei_get_imei(MMI_SIM1, sg_zmaee_tapi_cntx.imei_buf, sizeof(sg_zmaee_tapi_cntx.imei_buf)) != MMI_TRUE)
		memset(sg_zmaee_tapi_cntx.imei_buf, 0, sizeof(sg_zmaee_tapi_cntx.imei_buf));
#endif
}

#ifdef __MMI_DUAL_SIM_MASTER__

/**
 * 获取卡2 IMSI的resp接口
 * 参数:
 * 	msg_p					resp的消息结构体
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_IMSI2_Resp(void* msg_p)
{
	mmi_smu_get_imsi_rsp_struct *local_data_p = (mmi_smu_get_imsi_rsp_struct*)msg_p;
	if(local_data_p->result == ST_SUCCESS)
	{
  		memset(sg_zmaee_tapi_cntx.imsi_buf2, 0, sizeof(sg_zmaee_tapi_cntx.imsi_buf2));
  		memcpy(sg_zmaee_tapi_cntx.imsi_buf2, local_data_p->imsi + 1, 16);
	}
}

/**
 * 获取卡1 IMSI的req接口
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_IMSI2_Req(void)
{
	MYQUEUE msg;
	msg.oslSrcId = MOD_MMI;
	msg.oslDestId = MOD_L4C_2;
	msg.oslMsgId = PRT_GET_IMSI_REQ;
	msg.oslDataPtr = NULL;
	msg.oslPeerBuffPtr = NULL;
	OslMsgSendExtQueue(&msg);
	sg_zmaee_tapi_cntx.is_getting_imsi++;
}

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
/**
 * SIM2的短消息中心号码Resp函数
 * 参数:
 * 	inMsg					Resp消息参数指针
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_SCADDR2_Resp(void *inMsg)
{
    MMI_FRM_SMS_GET_PROFILE_PARAMS_RSP_STRUCT *msgRsp = (MMI_FRM_SMS_GET_PROFILE_PARAMS_RSP_STRUCT*)inMsg;
    if(msgRsp->result == TRUE) {
		int cpy_len = msgRsp->sc_addr.length;
		if(cpy_len >= sizeof(sg_zmaee_tapi_cntx.sc_addr2))
			cpy_len = sizeof(sg_zmaee_tapi_cntx.sc_addr2) - 1;
		
		memset(sg_zmaee_tapi_cntx.sc_addr2, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr2));
		memcpy(sg_zmaee_tapi_cntx.sc_addr2, msgRsp->sc_addr.number, cpy_len);
    }
}

#else	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)
static
void ZMAEE_ITAPI_SCADDR2_Resp(srv_sms_callback_struct* callback_data)
{
	short ucs2_temp[AEE_MAX_NUM_SIZE] = {0};

	#if (ZM_AEE_MTK_SOFTVERN <0x11A0)
	if (mmi_msg_check_interrupt() == TRUE) {
		return;
	}
	#endif
	
	if (callback_data->result == MMI_TRUE) {		
		memset(sg_zmaee_tapi_cntx.sc_addr2, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr2));
		mmi_ucs2ncpy((S8*)ucs2_temp, (const S8*)(callback_data->action_data), sizeof(ucs2_temp) / sizeof(ucs2_temp[0]) - 1);
		mmi_ucs2_to_asc((S8*)sg_zmaee_tapi_cntx.sc_addr2, (S8*)ucs2_temp);
	} else {
		
	}
}
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)


/**
 * 请求SIM2的短消息中心号码
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_SCADDR2_Req(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT* msgReq;
	char index;
	short err_code;

	ReadValue(NVRAM_SLAVE_ACTIVE_PROFILE_IDX, &index, DS_BYTE, &err_code);
	if(err_code != NVRAM_READ_SUCCESS) {
		return;
	}
	
	memset(sg_zmaee_tapi_cntx.sc_addr2, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr2));
    msgReq = (MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT*)OslConstructDataPtr(sizeof (MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT));
    msgReq->profile_no = index;
	sg_zmaee_tapi_cntx.is_getting_scaddr++;
    mmi_frm_sms_send_message(MOD_MMI, MOD_L4C_2, 0, PRT_MSG_ID_MMI_SMS_GET_PROFILE_PARAMS_REQ, (oslParaType *)msgReq, NULL);
#else
	srv_sms_get_setting_para(SRV_SMS_ACTIVE_SC_ADDR, 
			                 NULL, 
			                 SRV_SMS_SIM_2, 
			                 ZMAEE_ITAPI_SCADDR2_Resp, 
			                 NULL);

#endif
}

#endif	// __MMI_DUAL_SIM_MASTER__

/**
 * 获取卡1 IMSI的resp接口
 * 参数:
 * 	msg_p					resp的消息结构体
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_IMSI1_Resp(void* msg_p)
{
	mmi_smu_get_imsi_rsp_struct *local_data_p = (mmi_smu_get_imsi_rsp_struct*)msg_p;
	if(local_data_p->result == ST_SUCCESS)
	{
  		memset(sg_zmaee_tapi_cntx.imsi_buf1, 0, sizeof(sg_zmaee_tapi_cntx.imsi_buf1));
  		memcpy(sg_zmaee_tapi_cntx.imsi_buf1, local_data_p->imsi + 1, 16);
	}
}

/**
 * 获取卡1 IMSI的req接口
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_IMSI1_Req(void)
{
	MYQUEUE msg;
	msg.oslSrcId = MOD_MMI;
	msg.oslDestId = MOD_L4C;
	msg.oslMsgId = PRT_GET_IMSI_REQ;
	msg.oslDataPtr = NULL;
	msg.oslPeerBuffPtr = NULL;
	OslMsgSendExtQueue(&msg);
	sg_zmaee_tapi_cntx.is_getting_imsi++;
}

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
/**
 * SIM1的短消息中心号码Resp函数
 * 参数:
 * 	inMsg					Resp消息参数指针
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_SCADDR1_Resp(void *inMsg)
{	
    MMI_FRM_SMS_GET_PROFILE_PARAMS_RSP_STRUCT *msgRsp = (MMI_FRM_SMS_GET_PROFILE_PARAMS_RSP_STRUCT*)inMsg;
    if(msgRsp->result == TRUE) {
		int cpy_len = msgRsp->sc_addr.length;
		if(cpy_len >= sizeof(sg_zmaee_tapi_cntx.sc_addr1))
			cpy_len = sizeof(sg_zmaee_tapi_cntx.sc_addr1) - 1;
		
		memset(sg_zmaee_tapi_cntx.sc_addr1, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr1));
		memcpy(sg_zmaee_tapi_cntx.sc_addr1, msgRsp->sc_addr.number, cpy_len);
    }
}

static
void ZMAEE_ITAPI_SETSCADDR1_Resp(void *inMsg)
{
	// Dummy
}

#ifdef __MMI_DUAL_SIM_MASTER__
static
void ZMAEE_ITAPI_SETSCADDR2_Resp(void *inMsg)
{
	// Dummy
}
#endif	// __MMI_DUAL_SIM_MASTER__

#else	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

static
void ZMAEE_ITAPI_SCADDR1_Resp(srv_sms_callback_struct* callback_data)
{
	short ucs2_temp[AEE_MAX_NUM_SIZE] = {0};
	
#if (ZM_AEE_MTK_SOFTVERN <0x11A0)
	if (mmi_msg_check_interrupt() == TRUE) {
		return;
	}
#endif
	
	if (callback_data->result == MMI_TRUE) {		
		memset(sg_zmaee_tapi_cntx.sc_addr1, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr1));
		mmi_ucs2ncpy((S8*)ucs2_temp, (const S8*)(callback_data->action_data), sizeof(ucs2_temp) / sizeof(ucs2_temp[0]) - 1);
		mmi_ucs2_to_asc((S8*)sg_zmaee_tapi_cntx.sc_addr1, (S8*)ucs2_temp);
	} else {
		
	}
}

static
void ZMAEE_ITAPI_SETSCADDR1_Resp(srv_sms_callback_struct *callback_data)
{
	// Dummy
}

static
void ZMAEE_ITAPI_SETSCADDR2_Resp(srv_sms_callback_struct *callback_data)
{
	// Dummy
}
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

/**
 * 请求SIM1的短消息中心号码
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
static
void ZMAEE_ITAPI_SCADDR1_Req(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
    MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT* msgReq;
	char index;
	short err_code;

	ReadValue(NVRAM_SMS_ACTIVE_PROFILE_INDEX, &index , DS_BYTE, &err_code);
	if(err_code != NVRAM_READ_SUCCESS) {
		return;
	}
	
	memset(sg_zmaee_tapi_cntx.sc_addr1, 0, sizeof(sg_zmaee_tapi_cntx.sc_addr1));
    msgReq = (MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT*)OslConstructDataPtr(sizeof (MMI_FRM_SMS_GET_PROFILE_PARAMS_REQ_STRUCT));
    msgReq->profile_no = index;
	sg_zmaee_tapi_cntx.is_getting_scaddr++;
    mmi_frm_sms_send_message(MOD_MMI, MOD_L4C, 0, PRT_MSG_ID_MMI_SMS_GET_PROFILE_PARAMS_REQ, (oslParaType *)msgReq, NULL);
#else
	srv_sms_get_setting_para(SRV_SMS_ACTIVE_SC_ADDR, 
			                 NULL, 
			                 SRV_SMS_SIM_1, 
			                 ZMAEE_ITAPI_SCADDR1_Resp, 
			                 NULL);
#endif
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0) || defined (MT6252)

static int ZMAEE_ITAPI_IsValid(ZMAEE_SIM_TYPE type)  
{
	int i;

	for(i = 0; i < sg_zmaee_tapi_cntx.sim_info.sim_count; i++) {
		if(type == sg_zmaee_tapi_cntx.sim_info.sim_status[i].sim_id)
			return E_ZM_AEE_TRUE;
	}

	return E_ZM_AEE_FALSE;
}
#endif

/**
 * 开机初始化IMSI号
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
void ZMAEE_ITAPI_InitIMSI(void)
{
	if(sg_zmaee_tapi_cntx.sim_info.sim_count <= 0) {
		return;
	}

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0) && !defined (MT6252)
{
	sg_zmaee_tapi_cntx.is_getting_imsi = 0;
#ifndef __MMI_DUAL_SIM_MASTER__
	if(sg_zmaee_tapi_cntx.sim_info.sim_count > 0) {
		ZMAEE_ITAPI_IMSI1_Req();
	}
#else
	if(sg_zmaee_tapi_cntx.sim_info.sim_count == 1) {
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id == ZMAEE_SIM_TYPE_1) {
			ZMAEE_ITAPI_IMSI1_Req();
		} else if(sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id == ZMAEE_SIM_TYPE_2) {
			ZMAEE_ITAPI_IMSI2_Req();
		}
	} else if(sg_zmaee_tapi_cntx.sim_info.sim_count == 2) {
		ZMAEE_ITAPI_IMSI1_Req();
		ZMAEE_ITAPI_IMSI2_Req();
	}
#endif
}
#else
{
	if(ZMAEE_ITAPI_IsValid(ZMAEE_SIM_TYPE_1)) {
			if(srv_sim_ctrl_get_imsi(MMI_SIM1, sg_zmaee_tapi_cntx.imsi_buf1, sizeof(sg_zmaee_tapi_cntx.imsi_buf1)) != MMI_TRUE) {
				memset(sg_zmaee_tapi_cntx.imsi_buf1, 0, sizeof(sg_zmaee_tapi_cntx.imsi_buf1));
			} else {
				if(sg_zmaee_tapi_cntx.imsi_buf1[0] == '9') {
					int len = strlen(sg_zmaee_tapi_cntx.imsi_buf1);
					memmove(sg_zmaee_tapi_cntx.imsi_buf1, &sg_zmaee_tapi_cntx.imsi_buf1[1], len - 1);
					sg_zmaee_tapi_cntx.imsi_buf1[len - 1] = 0;
				}
				ZMAEE_DebugPrint("ZMAEE_ITAPI_InitIMSI: Imsi sim1 = %s", sg_zmaee_tapi_cntx.imsi_buf1);
			}
		}
	
#ifdef __MMI_DUAL_SIM_MASTER__
		if(ZMAEE_ITAPI_IsValid(ZMAEE_SIM_TYPE_2)) {
			if(srv_sim_ctrl_get_imsi(MMI_SIM2, sg_zmaee_tapi_cntx.imsi_buf2, sizeof(sg_zmaee_tapi_cntx.imsi_buf2)) != MMI_TRUE) {
				memset(sg_zmaee_tapi_cntx.imsi_buf2, 0, sizeof(sg_zmaee_tapi_cntx.imsi_buf2));
			} else {
				if(sg_zmaee_tapi_cntx.imsi_buf2[0] == '9') {
					int len = strlen(sg_zmaee_tapi_cntx.imsi_buf2);
					memmove(sg_zmaee_tapi_cntx.imsi_buf2, &sg_zmaee_tapi_cntx.imsi_buf2[1], len - 1);
					sg_zmaee_tapi_cntx.imsi_buf2[len - 1] = 0;
				}
				ZMAEE_DebugPrint("ZMAEE_ITAPI_InitIMSI: Imsi sim2 = %s", sg_zmaee_tapi_cntx.imsi_buf2);
			}
		}
#endif 
		
}
#endif
}

/**
 * 开机初始化短消息中心
 * 参数:
 * 	void
 * 返回:
 * 	void
 */
void ZMAEE_ITAPI_InitSCADDR(void)
{
	if(sg_zmaee_tapi_cntx.sim_info.sim_count < 0) {
		return;
	}

	sg_zmaee_tapi_cntx.is_getting_scaddr = 0;
#ifndef __MMI_DUAL_SIM_MASTER__
	if(sg_zmaee_tapi_cntx.sim_info.sim_count > 0) {
		ZMAEE_ITAPI_SCADDR1_Req();
	}
#else
	if(sg_zmaee_tapi_cntx.sim_info.sim_count == 1) {
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id == ZMAEE_SIM_TYPE_1) {
			ZMAEE_ITAPI_SCADDR1_Req();
		} else if(sg_zmaee_tapi_cntx.sim_info.sim_status[0].sim_id == ZMAEE_SIM_TYPE_2) {
			ZMAEE_ITAPI_SCADDR2_Req();
		}
	} else if(sg_zmaee_tapi_cntx.sim_info.sim_count == 2) {
		ZMAEE_ITAPI_SCADDR1_Req();
		ZMAEE_ITAPI_SCADDR2_Req();
	}
#endif

}

/**
 * 是否可以发送短信
 * 参数:
 * 	void
 * 返回:
 * 	0						可以发送
 * 	1						不可以发送
 */
static
int ZMAEE_ITAPI_CanSendSms(AEE_ITAPI *po)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0) && !defined (MT6252)
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
#ifdef __MMI_DUAL_SIM_MASTER__
	U8 ServiceType;
#endif

	if(pThis == NULL) {
		return 0;
	}

#ifdef MMI_ON_WIN32
	return 1;
#endif

#ifndef __MMI_DUAL_SIM_MASTER__
	if(g_pwr_context.CurrentServiceType == NO_SERVICE)
	{
		return 0;
	}
#else
	if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == 1)
		ServiceType = g_pwr_context_2.CurrentServiceType;
	else
		ServiceType = g_pwr_context.CurrentServiceType;
	
	if(ServiceType == NO_SERVICE)
	{
		return 0;
	}
#endif

#if(ZM_AEE_MTK_SOFTVERN < 0x09B0)
	else if(mmi_frm_sms_get_sms_list_size(0x00) == MMI_FRM_SMS_INVALID_INDEX)   //not ready
	{
		return 0;
	}
	else
	{
		if(mmi_frm_sms_check_action_pending())
			return 0;
	}
#endif
#endif
	return 1;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0)
static
void ZMAEE_ITAPI_SendSmsResp(srv_sms_callback_struct* callback_data)
{
	U16 currentSCRID;
	int status;
	ZMAEE_PFNSMSSTATUS callback = NULL;
	void *user_data = NULL;

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
	currentSCRID = GetActiveScreenId();
    if((currentSCRID == SCR_ID_MSG_SENDING)||(currentSCRID == SCR_ID_MSG_PROCESSING))
	{
        GoBackHistory();
    }
#endif
    
	if(callback_data->result == MMI_TRUE) {
		status = 0;
    } else {
    	status = -1;
	}

	sg_zmaee_tapi.sms_info.is_sending = 0;
	if(ZMAEE_Callback_Valid(&(sg_zmaee_tapi.sms_info.callback)) == 0)
		return;
	
	callback = (ZMAEE_PFNSMSSTATUS)sg_zmaee_tapi.sms_info.callback.pfn;
	user_data = sg_zmaee_tapi.sms_info.callback.pUser;
	
	
	memset(&(sg_zmaee_tapi.sms_info.callback), 0, sizeof(sg_zmaee_tapi.sms_info.callback));
	
	if(callback) {
		callback(user_data, status);
	}
}

#else
static
void ZMAEE_ITAPI_SendSmsResp(void* number, module_type mod, U16 result)
{
    U16 currentSCRID;
	int status;
	ZMAEE_PFNSMSSTATUS callback = NULL;
	void *user_data = NULL;
   
#if(ZM_AEE_MTK_SOFTVERN < 0x0924)
    if(ZMAEE_CloseDeliveryFlag || ZMAEE_CloseDeliveryFlag2) {
		if(ZMAEE_CloseDeliveryFlag) {
	        ZMAEE_CloseDeliveryFlag = 0;
	        DeliveryRepyStates[0] = 1;
		}

		if(ZMAEE_CloseDeliveryFlag2) {
			ZMAEE_CloseDeliveryFlag2 = 0;
			DeliveryRepyStates[1] = 1;
		}
		
        mmi_frm_sms_set_common_settings(NULL, MOD_MMI, DeliveryRepyStates);
    }
#else
	mmi_frm_sms_set_common_settings(NULL, MOD_MMI, ZMAEE_OrigDelieverReplyStates);
#endif

    mmi_frm_sms_delete_screen_history();

	currentSCRID = GetActiveScreenId();
    if((currentSCRID == SCR_ID_MSG_SENDING)||(currentSCRID == SCR_ID_MSG_PROCESSING)) {
        GoBackHistory();
    }
    
#if(ZM_AEE_MTK_SOFTVERN < 0x0924)	
	AlmEnableSPOF();
#endif

	ZMAEE_DebugPrint("ZMAEE_ITAPI_SendSmsResp: result = %d", result);
	if(result == MMI_FRM_SMS_OK) {
		status = 0;
    } else {
    	status = -1;
	}

	sg_zmaee_tapi.sms_info.is_sending = 0;
	if(ZMAEE_Callback_Valid(&(sg_zmaee_tapi.sms_info.callback)) == 0)
		return;
	
	callback = (ZMAEE_PFNSMSSTATUS)sg_zmaee_tapi.sms_info.callback.pfn;
	user_data = sg_zmaee_tapi.sms_info.callback.pUser;
	
	
	memset(&(sg_zmaee_tapi.sms_info.callback), 0, sizeof(sg_zmaee_tapi.sms_info.callback));
	
	if(callback) {
		callback(user_data, status);
	}
}
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)

static
void ZMAEE_ITAPI_EMParsingData(void *info)
{
#ifndef WIN32
	mmi_em_status_ind_struct* msg = (mmi_em_status_ind_struct*)info;
	
	if(msg->mod_id == MOD_AS) {
		switch(msg->em_info)
	    {
		  	case RR_EM_LAI_INFO:
		   	{
				if(ZMAEE_Callback_Valid(&(sg_zmaee_tapi_cntx.bsid_po->bsid_info.callback)) != 1) {
					return;
				}
				
				if(sg_zmaee_tapi_cntx.bsid_po->bsid_info.callback.pfn) {
				    rr_em_lai_info_struct *data_ptr;
				   	kal_uint16  mm_pdu_len;
					ZMAEE_BSID_INFO bsid_info;
					
				   	data_ptr = (rr_em_lai_info_struct *)get_pdu_ptr( msg->info, &mm_pdu_len);
				  	bsid_info.nCellId = data_ptr->cell_id;
					bsid_info.nLac = (data_ptr->lac[0] << 8) | (data_ptr->lac[1]);
					bsid_info.nMcc = (data_ptr->mcc[0] << 16) | (data_ptr->mcc[1] << 8) | (data_ptr->mcc[2]);
					bsid_info.nMnc = (data_ptr->mnc[0] << 16) | (data_ptr->mnc[1] << 8) | (data_ptr->mnc[2]);

					((ZMAEE_PFNBSIDCB)sg_zmaee_tapi_cntx.bsid_po->bsid_info.callback.pfn)(sg_zmaee_tapi_cntx.bsid_po->bsid_info.callback.pUser, 1, &bsid_info);
				}
		  	}
		  		break;
		  	default:
		  		break;
	    }
		
	   free_peer_buff(msg->info);
	}
#endif
}

/**
 * @sim_idx: 0 - sim1, 1 - sim2
 */
void ZMAEE_ITAPI_EMSendStartReq(int sim_idx)   
{
#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
    MYQUEUE Message;   
    mmi_em_update_req_struct *em_start_req;
	int i;
	
    Message.oslMsgId = MSG_ID_MMI_EM_UPDATE_REQ;   
    em_start_req = OslConstructDataPtr(sizeof(mmi_em_update_req_struct));   

	for(i = 0; i < EM_INFO_REQ_NUM; i++) {
    	em_start_req->info_request[i] = EM_NC;
	}
	em_start_req->info_request[RR_EM_LAI_INFO] = EM_ON;	
  
    Message.oslDataPtr = (oslParaType*)em_start_req;   
    Message.oslPeerBuffPtr = NULL;   
    Message.oslSrcId = MOD_MMI;  
#ifdef __MMI_DUAL_SIM_MASTER__
    Message.oslDestId = (sim_idx == 0)? MOD_L4C: MOD_L4C_2;
#else
	Message.oslDestId = MOD_L4C;
#endif
    OslMsgSendExtQueue(&Message);
#else	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)
	MYQUEUE Message;
	mmi_em_start_req_struct *em_start_req;

	Message.oslMsgId = MSG_ID_MMI_EM_START_REQ;
	em_start_req = OslConstructDataPtr(sizeof(mmi_em_start_req_struct));
	em_start_req->mod_id = MOD_AS;
	em_start_req->info_request = RR_EM_LAI_INFO;

	Message.oslDataPtr = (oslParaType *)em_start_req;
	Message.oslPeerBuffPtr = NULL;
	Message.oslSrcId = MOD_MMI;
	Message.oslDestId = (sim_idx = 0)? MOD_L4C: MOD_L4C_2;

	OslMsgSendExtQueue(&Message);
#endif	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)
}  

static
void ZMAEE_ITAPI_EMSendStopReq(void)
{
#ifndef WIN32
	#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
		MYQUEUE Message;
		int i=0;
		mmi_em_update_req_struct *em_stop_req;
		
		Message.oslMsgId = MSG_ID_MMI_EM_UPDATE_REQ;
		em_stop_req = OslConstructDataPtr(sizeof(mmi_em_update_req_struct));

		for(i = 0;i<EM_INFO_REQ_NUM ;i++)
		{
			em_stop_req->info_request[i] = EM_NC;
		}
		
		em_stop_req->info_request[RR_EM_LAI_INFO] = EM_OFF;
		
		Message.oslDataPtr = (oslParaType *)em_stop_req;
		Message.oslPeerBuffPtr = NULL;
		Message.oslSrcId = MOD_MMI;
	#ifdef __MMI_DUAL_SIM_MASTER__
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == 1)
			Message.oslDestId = MOD_L4C_2;
		else
	#endif	// __MMI_DUAL_SIM_MASTER__
	
		Message.oslDestId = MOD_L4C;	
		OslMsgSendExtQueue(&Message);
#else	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)
		MYQUEUE Message;
		mmi_em_stop_req_struct *em_stop_req;
		module_type mod_id ;
		request_info_type info_id;   
		mod_id =MOD_AS;
		info_id = 					RR_EM_CELL_SELECT_PARA_INFO|RR_EM_CHANNEL_DESCR_INFO|
									RR_EM_CTRL_CHANNEL_DESCR_INFO|RR_EM_RACH_CTRL_PARA_INFO|
									RR_EM_LAI_INFO|RR_EM_RADIO_LINK_COUNTER_INFO|
									RR_EM_MEASUREMENT_REPORT_INFO|RR_EM_CONTROL_MSG_INFO|
									RR_EM_TBF_INFO|RR_EM_GPRS_GENERAL_INFO;

		Message.oslMsgId = MSG_ID_MMI_EM_STOP_REQ;
		em_stop_req = OslConstructDataPtr(sizeof(mmi_em_stop_req_struct));
		em_stop_req->mod_id = mod_id;
		em_stop_req->info_request = info_id;

		Message.oslDataPtr = (oslParaType *)em_stop_req;
		Message.oslPeerBuffPtr = NULL;
		Message.oslSrcId = MOD_MMI;
	#ifdef __MMI_DUAL_SIM_MASTER__
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == 1)
			Message.oslDestId = MOD_L4C_2;
		else
	#endif	// __MMI_DUAL_SIM_MASTER__
	
		Message.oslDestId = MOD_L4C;
		OslMsgSendExtQueue(&Message);
	#endif	// (ZM_AEE_MTK_SOFTVERN >= 0x0852)
#endif
}

#endif

/**
 * 获取通讯的卡
 * 参数:
 * 	void
 * 返回:
 * 	0					sim1
 * 	1					sim2
 */
int ZMAEE_GetActiveSimCard(void)
{
	ZMAEE_DebugPrint("ZMAEE_GetActiveSimCard: active sim card = %d", sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id);
	return sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id;
}

/*****************************************************************************
 * Delay Sms
 *****************************************************************************/
typedef struct {
	int 	nPort;
	char 	pszDst[AEE_MAX_NUM_SIZE];
	char	pszMsg[AEE_MAX_TXTBYTS_PERLINE];
} ZMAEE_DELAYSMS_INFO;

typedef struct {
	LOCAL_PARA_HDR
	ZMAEE_DELAYSMS_INFO *delay_sms;
}ZMAEE_DELAYSMS_LBUF;

/**
 * Send Delay Sms
 */
static
int ZMAEE_ITAPI_SendDelaySms(AEE_ITAPI* po, const char* pszDst, const char* pszMsg, int nPort)
{
	ilm_struct *ilm_ptr;
	ZMAEE_DELAYSMS_LBUF *delay_sms_info;

	if((!po) || (!pszDst) || (!pszMsg)) {
		return E_ZM_AEE_BADPARAM;
	}

	delay_sms_info = (ZMAEE_DELAYSMS_LBUF*)construct_local_para(sizeof(ZMAEE_DELAYSMS_LBUF), TD_CTRL);
	if(!delay_sms_info) {
		return E_ZM_AEE_NOMEMORY;
	}

	ilm_ptr = allocate_ilm(MOD_MMI);
	if(!ilm_ptr) {
		free_local_para((void*)delay_sms_info);
		return E_ZM_AEE_NOMEMORY;
	}

	delay_sms_info->delay_sms = (ZMAEE_DELAYSMS_INFO*)ZMAEE_Malloc(sizeof(ZMAEE_DELAYSMS_INFO));
	if(!delay_sms_info->delay_sms) {
		free_local_para((void*)delay_sms_info);
		free_ilm(ilm_ptr);
		return E_ZM_AEE_NOMEMORY;
	}

	memset(delay_sms_info->delay_sms, 0, sizeof(ZMAEE_DELAYSMS_INFO));
	strncpy(delay_sms_info->delay_sms->pszDst, pszDst, sizeof(delay_sms_info->delay_sms->pszDst) - 1);
	strncpy(delay_sms_info->delay_sms->pszMsg, pszMsg, sizeof(delay_sms_info->delay_sms->pszMsg) - 1);	
	delay_sms_info->delay_sms->nPort = nPort;

	ilm_ptr->src_mod_id = MOD_MMI;
	ilm_ptr->dest_mod_id = MOD_MMI;
	ilm_ptr->sap_id = MMI_MMI_SAP;
	ilm_ptr->msg_id = MSG_ID_ZMAEE_DELAYSMS;
	ilm_ptr->local_para_ptr = (void*)delay_sms_info;
	ilm_ptr->peer_buff_ptr = NULL;
	
	if(msg_send_ext_queue(ilm_ptr) != KAL_TRUE) {
		free_ilm(ilm_ptr);
		ZMAEE_Free(delay_sms_info->delay_sms);
		free_local_para((void*)delay_sms_info);
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * Resend Delay Sms Msg
 */
static
void ZMAEE_ITAPI_Resend_DelaySms(ZMAEE_DELAYSMS_INFO *delay_sms)
{
	ilm_struct *ilm_ptr;
	ZMAEE_DELAYSMS_LBUF *delay_sms_info;

	if(!delay_sms)
		return;

	delay_sms_info = (ZMAEE_DELAYSMS_LBUF*)construct_local_para(sizeof(ZMAEE_DELAYSMS_LBUF), TD_CTRL);
	if(!delay_sms_info) {
		ZMAEE_Free(delay_sms);
		return;
	}

	ilm_ptr = allocate_ilm(MOD_MMI);
	if(!ilm_ptr) {
		free_local_para((void*)delay_sms_info);
		ZMAEE_Free(delay_sms);
		return;
	}

	delay_sms_info->delay_sms = delay_sms;

	ilm_ptr->src_mod_id = MOD_MMI;
	ilm_ptr->dest_mod_id = MOD_MMI;
	ilm_ptr->sap_id = MMI_MMI_SAP;
	ilm_ptr->msg_id = MSG_ID_ZMAEE_DELAYSMS;
	ilm_ptr->local_para_ptr = (void*)delay_sms_info;
	ilm_ptr->peer_buff_ptr = NULL;
	
	if(msg_send_ext_queue(ilm_ptr) != KAL_TRUE) {
		free_ilm(ilm_ptr);
		ZMAEE_Free(delay_sms);
		free_local_para((void*)delay_sms_info);
		return;
	}
}


/**
 * Delay Sms Callback
 */
static
void ZMAEE_DelaySms_Callback(void *pUser, int status)
{
	ZMAEE_DELAYSMS_INFO *delay_sms = (ZMAEE_DELAYSMS_INFO*)pUser;

	ZMAEE_DebugPrint("ZMAEE_DelaySms_Callback: status = %d", status);

	if(status == 0) {
		ZMAEE_Free(delay_sms);
	} else {
		ZMAEE_DebugPrint("ZMAEE_DelaySms_Callback: Send Sms failed");
		ZMAEE_ITAPI_Resend_DelaySms(delay_sms);
	}
}

/**
 * 处理Delay Sms消息
 */
static
int ZMAEE_Execute_DelaySms(void *lparam, void *pparam)
{
	ZMAEE_DELAYSMS_LBUF *delay_sms_info = (ZMAEE_DELAYSMS_LBUF*)lparam;
	int ret;

	ZMAEE_DebugPrint("ZMAEE_Execute_DelaySms: nPort = %d, pszDst = %s", delay_sms_info->delay_sms->nPort,
		delay_sms_info->delay_sms->pszDst);	
	if(delay_sms_info->delay_sms->nPort == 0) {
		ret = ZMAEE_ITAPI_SendSms((AEE_ITAPI*)&sg_zmaee_tapi, delay_sms_info->delay_sms->pszDst, 
									delay_sms_info->delay_sms->pszMsg,
									ZMAEE_DelaySms_Callback, (void*)delay_sms_info->delay_sms);
	} else {
		ret = ZMAEE_ITAPI_SendPortSms((AEE_ITAPI*)&sg_zmaee_tapi, delay_sms_info->delay_sms->pszDst, 
										delay_sms_info->delay_sms->pszMsg, delay_sms_info->delay_sms->nPort,
										ZMAEE_DelaySms_Callback, (void*)delay_sms_info->delay_sms);
	}

	ZMAEE_DebugPrint("ZMAEE_Execute_DelaySms: ret = %d", ret);
	if(ret != E_ZM_AEE_SUCCESS) {
		ZMAEE_ITAPI_Resend_DelaySms(delay_sms_info->delay_sms);
	} 
	
	return 1;
}


/**
 * 为了避免和第三方的冲突,mmi的系统消息hook处理
 */
int ZMAEE_Execute_Protocol_Handler(int mod_src, U16 eventID, void *MsgStruct, void *peerBuf)
{
	int ret = 0;
	switch(eventID)
	{
	case MSG_ID_APP_SOC_NOTIFY_IND:
#ifndef WIN32
	{
		extern int ZMAEE_INetMgr_EventHandle(void* inMsg);
		ret = ZMAEE_INetMgr_EventHandle(MsgStruct);
	}
#endif	//WIN32
		break;

	case PRT_GET_IMSI_RSP:
		ZMAEE_DebugPrint("ZMAEE_Execute_Protocol_Handler: Recv PRT_GET_IMSI_RSP, is_getting_imsi = %d", sg_zmaee_tapi_cntx.is_getting_imsi);
		if(sg_zmaee_tapi_cntx.is_getting_imsi > 0) {
			sg_zmaee_tapi_cntx.is_getting_imsi--;
#ifndef WIN32
			if(mod_src == MOD_L4C) ZMAEE_ITAPI_IMSI1_Resp(MsgStruct);
	#ifdef __MMI_DUAL_SIM_MASTER__
			else if(mod_src == MOD_L4C_2) ZMAEE_ITAPI_IMSI2_Resp(MsgStruct);
	#endif	// __MMI_DUAL_SIM_MASTER__
#endif	// WIN32
		}
		break;

	case PRT_MSG_ID_MMI_SMS_GET_PROFILE_PARAMS_RSP:
		if(sg_zmaee_tapi_cntx.is_getting_scaddr > 0) {
			sg_zmaee_tapi_cntx.is_getting_scaddr--;
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	#ifndef WIN32
			if(mod_src == MOD_L4C) ZMAEE_ITAPI_SCADDR1_Resp(MsgStruct);
		#ifdef __MMI_DUAL_SIM_MASTER__
			else if(mod_src == MOD_L4C_2) ZMAEE_ITAPI_SCADDR2_Resp(MsgStruct);
		#endif//__MMI_DUAL_SIM_MASTER__
	#endif	// WIN32
#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)
		}
		break;

	case PRT_MSG_ID_MMI_SMS_SET_PROFILE_PARAMS_RSP:
		if(sg_zmaee_tapi_cntx.is_setting_scaddr > 0) {
			sg_zmaee_tapi_cntx.is_setting_scaddr--;
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	#ifndef WIN32
			if(mod_src == MOD_L4C) ZMAEE_ITAPI_SETSCADDR1_Resp(MsgStruct);
		#ifdef __MMI_DUAL_SIM_MASTER__
			else if(mod_src == MOD_L4C_2) ZMAEE_ITAPI_SETSCADDR2_Resp(MsgStruct);
		#endif	// __MMI_DUAL_SIM_MASTER__
	#endif	// WIN32
#endif // (ZM_AEE_MTK_SOFTVERN < 0x09B0)
		}
		break;
		
	case MSG_ID_ZMAEE_PERIOD:
	{
		extern int ZMAEE_ExecutePeriodHandler(void *LPARAM, void *PPARAM);
		
		ret = ZMAEE_ExecutePeriodHandler(MsgStruct, peerBuf);
	}
		break;

	case MSG_ID_FMT_MMI_CARD_PLUG_IN_IND:
	{
		extern void ZMAEE_MemoryCard_Notify(int plug_type);
		ZMAEE_MemoryCard_Notify(1);
	}
		break;

	case MSG_ID_FMT_MMI_CARD_PLUG_OUT_IND:
	{
		extern void ZMAEE_MemoryCard_Notify(int plug_type);	
		ZMAEE_MemoryCard_Notify(0);
	}
		break;

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	case MSG_ID_MMI_EM_STATUS_IND:
	{
		if(sg_zmaee_tapi.bsid_info.delay_stop) {
			sg_zmaee_tapi.bsid_info.delay_stop = 0;

			ZMAEE_ITAPI_StopReportBSID((AEE_ITAPI*)&sg_zmaee_tapi);
		}

		if(sg_zmaee_tapi.bsid_info.is_request) {
			ZMAEE_ITAPI_EMParsingData(MsgStruct);
		}
	}
		break;
#endif

	case MSG_ID_ZMAEE_DELAYSMS:
	{
		ret = ZMAEE_Execute_DelaySms(MsgStruct, peerBuf);
	}
		break;

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	case MSG_ID_MMI_EM_UPDATE_RSP:
	{
		if(sg_zmaee_tapi.bsid_info.disable_notify) {
			mmi_em_update_rsp_struct *ind = (mmi_em_update_rsp_struct*)MsgStruct;

			sg_zmaee_tapi.bsid_info.disable_notify = 0;
			if(ind->result == KAL_FALSE) {
				if(ZMAEE_Callback_Valid(&sg_zmaee_tapi.bsid_info.callback)) {
					ZMAEE_PFNBSIDCB callback = (ZMAEE_PFNBSIDCB)sg_zmaee_tapi.bsid_info.callback.pfn;
					void *pUser = sg_zmaee_tapi.bsid_info.callback.pUser;

					if(callback)
						callback(pUser, 0, NULL);
				}
			}
		}
		
		ret = 1;
	}
		break;
#endif
		
	}


	return ret;
}

/**
 * 获取有效卡的个数
 */
int ZMAEE_GetValidSimCount(void)
{
	return sg_zmaee_tapi_cntx.sim_info.sim_count;
}

/**
 * 是否获取IMSI号成功
 */
int ZMAEE_InitIMSI_Done(void)
{
	if(sg_zmaee_tapi_cntx.sim_info.sim_count == 1) 
	{
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == ZMAEE_SIM_TYPE_1) 
		{
			if(sg_zmaee_tapi_cntx.imsi_buf1[0] != 0)
				return E_ZM_AEE_TRUE;
			else
				return E_ZM_AEE_FALSE;
		} 
#ifdef __MMI_DUAL_SIM_MASTER__
		else 
		{	// sim2
			if(sg_zmaee_tapi_cntx.imsi_buf2[0] != 0)
				return E_ZM_AEE_TRUE;
			else
				return E_ZM_AEE_FALSE;
		}
#endif
	} 
#ifdef __MMI_DUAL_SIM_MASTER__
	else
	{	// double sim
		if((sg_zmaee_tapi_cntx.imsi_buf1[0] != 0) && (sg_zmaee_tapi_cntx.imsi_buf2[0] != 0))
			return E_ZM_AEE_TRUE;
		else
			return E_ZM_AEE_FALSE;
	}
#endif

	return E_ZM_AEE_FALSE;
}

/**
 * 是否获取SCADDR成功
 */
int ZMAEE_InitSCADDR_Done(void)
{
	if(sg_zmaee_tapi_cntx.sim_info.sim_count == 1) 
	{
		if(sg_zmaee_tapi_cntx.sim_info.sim_status[sg_zmaee_tapi_cntx.sim_info.sim_active].sim_id == ZMAEE_SIM_TYPE_1) 
		{
			if(sg_zmaee_tapi_cntx.sc_addr1[0] != 0)
				return E_ZM_AEE_TRUE;
			else
				return E_ZM_AEE_FALSE;
		} 
#ifdef __MMI_DUAL_SIM_MASTER__
		else 
		{	// sim2
			if(sg_zmaee_tapi_cntx.sc_addr2[0] != 0)
				return E_ZM_AEE_TRUE;
			else
				return E_ZM_AEE_FALSE;
		}
#endif
	} 
#ifdef __MMI_DUAL_SIM_MASTER__
	else
	{	// double sim
		if((sg_zmaee_tapi_cntx.sc_addr1[0] != 0) && (sg_zmaee_tapi_cntx.sc_addr2[0] != 0))
			return E_ZM_AEE_TRUE;
		else
			return E_ZM_AEE_FALSE;
	}
#endif

	return E_ZM_AEE_FALSE;
}

int ZMAEE_InitIMEI_Done(void)
{
	if(sg_zmaee_tapi_cntx.imei_buf[0] != 0)
		return E_ZM_AEE_TRUE;
	else
		return E_ZM_AEE_FALSE;
}

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)

static
void ZMAEE_ITAPI_ReadICCID_CB(U16 error_code, U16 read_length)
{
	int i;
	char iccid_buf[AEE_MAX_NUM_SIZE] = {0};
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)&sg_zmaee_tapi;

	pThis->iccid_busy = E_ZM_AEE_FALSE;

	if(ZMAEE_Callback_Valid(&pThis->iccid_callback)) {
		ZMAEE_PFNICCID pfn = (ZMAEE_PFNICCID)pThis->iccid_callback.pfn;
		void *pUser = pThis->iccid_callback.pUser;
		
		for(i = 0; i < read_length; i++) {
			char temp[4] = {0}, temp_c;

			sprintf(temp, "%02x", pThis->iccid_buf[i]);
			temp_c = temp[0];
			temp[0] = temp[1];
			temp[1] = temp_c;
			strcat(iccid_buf, temp);
		}

		pfn(pUser, iccid_buf, strlen(iccid_buf));
	}	
}


/**
 * 获取当前激活卡的ICCID
 * @ pfn					len == 0表示获取失败
 * RETURN:
 *	E_ZM_AEE_BADPARAM	po, pfn is null
 *	E_ZM_AEE_ITEMBUSY	正在获取ICCID，等待pfn回调后再次调用
 *	E_ZM_AEE_FAILURE	获取请求失败
 *	E_ZM_AEE_SUCCESS	获取请求成功，在回调函数中ICCID
 */
int ZMAEE_ITAPI_GetICCID(AEE_ITAPI *po, ZMAEE_PFNICCID pfn, void *pUser)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	sim_interface_enum sim_enum;
	
	if(!pThis || !pfn)
		return E_ZM_AEE_BADPARAM;

	if(pThis->iccid_busy)
		return E_ZM_AEE_ITEMBUSY;

	memset(pThis->iccid_buf, 0, sizeof(pThis->iccid_buf));
	sim_enum = (ZMAEE_GetActiveSimCard() == 0)? SIM1: SIM2;
	if(mmi_sim_access_linear_fixed_ef(FILE_ICCID_IDX, 
		NULL, 
		0, 
		MMI_SIM_ACCESS_READ,
		0,
		10,
		(unsigned char*)pThis->iccid_buf,
		sizeof(pThis->iccid_buf),
		ZMAEE_ITAPI_ReadICCID_CB,
		sim_enum) == MMI_TRUE) {
		memset(&pThis->iccid_callback, 0, sizeof(pThis->iccid_callback));
		ZMAEE_Callback_Init(&pThis->iccid_callback, (void*)pfn, pUser);
		pThis->iccid_busy = E_ZM_AEE_TRUE;

		return E_ZM_AEE_SUCCESS;
	}

	return E_ZM_AEE_FAILURE;
}

#else

static
void ZMAEE_Tapi_GetICCIDCB(srv_sim_cb_struct* callback_data)
{
	srv_sim_data_struct *data = callback_data->data;
	int i;
	char iccid_buf[AEE_MAX_NUM_SIZE] = {0};
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)&sg_zmaee_tapi;

	pThis->iccid_busy = E_ZM_AEE_FALSE;

	if(ZMAEE_Callback_Valid(&pThis->iccid_callback)) {
		ZMAEE_PFNICCID pfn = (ZMAEE_PFNICCID)pThis->iccid_callback.pfn;
		void *pUser = pThis->iccid_callback.pUser;
		
		for(i = 0; i < data->length; i++) {
			char temp[4] = {0}, temp_c;

			sprintf(temp, "%02x", data->data[i]);
			temp_c = temp[0];
			temp[0] = temp[1];
			temp[1] = temp_c;
			strcat(iccid_buf, temp);
		}

		pfn(pUser, iccid_buf, strlen(iccid_buf));
	}	
}



/**
 * 获取当前激活卡的ICCID
 * @ pfn					len == 0表示获取失败
 * RETURN:
 *	E_ZM_AEE_BADPARAM	po, pfn is null
 *	E_ZM_AEE_ITEMBUSY	正在获取ICCID，等待pfn回调后再次调用
 *	E_ZM_AEE_FAILURE	获取请求失败
 *	E_ZM_AEE_SUCCESS	获取请求成功，在回调函数中ICCID
 */
int ZMAEE_ITAPI_GetICCID(AEE_ITAPI *po, ZMAEE_PFNICCID pfn, void *pUser)
{
	ZMAEE_TAPI *pThis = (ZMAEE_TAPI*)po;
	mmi_sim_enum sim_enum;

	if(!pThis || !pfn)
		return E_ZM_AEE_BADPARAM;

	if(pThis->iccid_busy)
		return E_ZM_AEE_ITEMBUSY;

	switch(ZMAEE_ITAPI_GetActiveSimCard(po)) {
		case 0:
			sim_enum = MMI_SIM1;
			break;

	#ifdef __MMI_DUAL_SIM_MASTER__
		case 1:
			sim_enum = MMI_SIM2;
			break;
	#endif

		default:
			return E_ZM_AEE_BADPARAM;
	}

	if(srv_sim_read_record(FILE_ICCID_IDX, NULL, 0, 10, sim_enum, ZMAEE_Tapi_GetICCIDCB, (void*)FILE_ICCID_IDX) == MMI_TRUE) {
		memset(&pThis->iccid_callback, 0, sizeof(pThis->iccid_callback));
		ZMAEE_Callback_Init(&pThis->iccid_callback, (void*)pfn, pUser);
		pThis->iccid_busy = E_ZM_AEE_TRUE;

		return E_ZM_AEE_SUCCESS;
	}

	return E_ZM_AEE_FAILURE;
}


#endif

#ifdef MT6252

unsigned char ZMAEE_Tapi_GetImsi(int sim, char *out_imsi_buffer, unsigned int buffer_size)
{
	int sim_idx = 0;
	
	switch(sim) {
		case MMI_SIM1:
			sim_idx = 0;
			break;
	#ifdef __MMI_DUAL_SIM_MASTER__
		case MMI_SIM2:
			sim_idx = 1;
			break;
	#if (MMI_MAX_SIM_NUM >= 3)
		case MMI_SIM3:
			sim_idx = 2;
			break;
	#endif

	#if (MMI_MAX_SIM_NUM >= 4)
		case MMI_SIM4:
			sim_idx = 3;
			break;
	#endif
	#endif
	
		default:
			return 0;
	}
	
	if(ZMAEE_ITAPI_GetImsi((AEE_ITAPI*)&sg_zmaee_tapi, sim_idx, out_imsi_buffer, buffer_size) == E_ZM_AEE_SUCCESS)
		return E_ZM_AEE_TRUE;
	else
		return E_ZM_AEE_FALSE;
}

int ZMAEE_Tapi_GetSimEnum(int sim_idx)
{
	switch(sim_idx) {
		case 0:
			return MMI_SIM1;

		#ifdef __MMI_DUAL_SIM_MASTER__
			case 1:
				return MMI_SIM2;

			#if (MMI_MAX_SIM_NUM >= 3)
			case 2:
				return MMI_SIM3;
			#endif

			#if (MMI_MAX_SIM_NUM >= 4)
			case 3:
				return MMI_SIM4;
			#endif
		#endif	// __MMI_DUAL_SIM_MASTER__
	}

	return MMI_SIM_NONE;
}

#endif

#endif	// __ZMAEE_APP__
