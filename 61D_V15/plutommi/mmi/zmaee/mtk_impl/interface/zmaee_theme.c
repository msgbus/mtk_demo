#ifdef __ZMAEE_APP__
#include "zmaee_priv.h"

#if (defined __ZMAEE_APP_THEME__) || (defined __ZMAEE_APP_KEYPAD_LOCK__) || (defined __ZMAEE_APP_DWALLPAPER__)

#include "GlobalConstants.h"
#include "CustDataProts.h"
#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_input_gprot.h"
#endif
#include "GlobalMenuItems.h"
#include "GlobalDefs.h"
#include "wgui_categories_util.h"
#include "med_utility.h"
#include "IdleAppResDef.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#if (ZM_AEE_MTK_SOFTVERN >= 0x1220)
#include "mmi_rp_vapp_launcher_mm_def.h"
#else
#include "mmi_rp_app_mainmenu_def.h"
#endif
#endif


#define ZMAEE_ITHEME_FUNC_COUNT			8
zm_extern int   			ZMAEE_ITheme_AddRef(AEE_ITheme* po);
zm_extern int   			ZMAEE_ITheme_Release(AEE_ITheme* po);
zm_extern int  			 	ZMAEE_ITheme_GetMainMenuInfo(AEE_ITheme *po, ZMAEE_THEME_MMINFO *pTMI);
zm_extern int				ZMAEE_ITheme_GetScreenRegion(AEE_ITheme *po, ZMAEE_THEME_SCRREG *pTSR);
zm_extern unsigned short*	ZMAEE_ITheme_GetString(AEE_ITheme *po, unsigned short str_id);
zm_extern int 				ZMAEE_ITheme_SetThemeAppId(AEE_ITheme *po, int type, unsigned long appId);
zm_extern unsigned long 	ZMAEE_ITheme_GetThemeAppId(AEE_ITheme *po, int type);
zm_extern void				ZMAEE_ITheme_LoadEntryFunc(AEE_ITheme *po, void (*entry_func)(void));


zm_extern void*				zmaee_memset(void * dest,char c,unsigned int count);
zm_extern char*				zmaee_strncpy(char * strDest,const char * strSource,unsigned int count);
zm_extern int 				ZMAEE_SetThemeAppId(int type, unsigned int appId);
zm_extern unsigned int 		ZMAEE_GetThemeAppId(int type);
zm_extern gdi_handle 		ZMAEE_IDisplay_GetLayerHandle(int layer_idx);
#ifdef WIN32
zm_extern void				ZMAEE_Exit_Ext(void);//WLJ
zm_extern int 				ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
#endif

extern void GoBackHistory(void);
extern void mmi_idle_display(void);

#if (ZM_AEE_MTK_SOFTVERN >= 0x0816)
extern FuncPtr mmi_frm_get_hilite_hdlr(U16 menu_id);
#endif

typedef struct {
	void			*pVtbl;
	void			(*pEntryFunc)(void);
}ZMAEE_THEME;

static ZMAEE_THEME	sg_zmaee_theme = {0};
static ZMAEE_MENU_TYPE	sg_zmaee_menu_types[] = {
	ZMAEE_MENUTYPE_FILEMGR,
	ZMAEE_MENUTYPE_PHONEBOOK,
	ZMAEE_MENUTYPE_FUNANDGAME,
	ZMAEE_MENUTYPE_CALLLOG,
	ZMAEE_MENUTYPE_MESSAGES,
	ZMAEE_MENUTYPE_MULTIMEDIA,
	ZMAEE_MENUTYPE_TOOLS,
	ZMAEE_MENUTYPE_SETTINGS,
	ZMAEE_MENUTYPE_PROFILES,
	ZMAEE_MENUTYPE_EXTRA,
	ZMAEE_MENUTYPE_NETWORK,
	ZMAEE_MENUTYPE_SHORTCUTS
};

int ZMAEE_ITheme_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_THEME* pThis = 0;

	pThis = &sg_zmaee_theme;

	pThis->pVtbl = ZMAEE_GetVtable(clsId);
	if(pThis->pVtbl == 0)
	{
		pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_ITHEME_FUNC_COUNT,
									ZMAEE_ITheme_AddRef,
									ZMAEE_ITheme_Release,
									ZMAEE_ITheme_GetMainMenuInfo,
									ZMAEE_ITheme_GetScreenRegion,
									ZMAEE_ITheme_GetString,
									ZMAEE_ITheme_SetThemeAppId,
									ZMAEE_ITheme_GetThemeAppId,
									ZMAEE_ITheme_LoadEntryFunc);
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

int ZMAEE_ITheme_AddRef(AEE_ITheme *po)
{
	if(po == NULL)
		return E_ZM_AEE_FAILURE;

	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_ITheme_Release(AEE_ITheme * po)
{
	if(po == NULL)
		return E_ZM_AEE_FAILURE;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取主菜单的菜单信息
 * @pTMI		- 输出参数，菜单信息
 * RETURN:
 *	E_ZM_AEE_BADPARAM	- po,strIDs,imgIDs,nItemCount为NULL
 *	E_ZM_AEE_FAILURE	- 获取失败
 *	E_ZM_AEE_SUCCESS	- 获取成功
 */
int ZMAEE_ITheme_GetMainMenuInfo(AEE_ITheme *po, ZMAEE_THEME_MMINFO *pTMI)
{
	ZMAEE_THEME *pThis = (ZMAEE_THEME*)po;
	FuncPtr lsk_down_func, lsk_up_func, rsk_down_func, rsk_up_func;
	FuncPtr up_arrow_down_func, up_arrow_up_func;
	FuncPtr down_arrow_down_func, down_arrow_up_func;
	FuncPtr left_arrow_down_func, left_arrow_up_func;
	FuncPtr right_arrow_down_func, right_arrow_up_func;
	int i;
	unsigned short menu_item_list[MAX_SUB_MENUS];
#if (ZM_AEE_MTK_SOFTVERN < 0x0816)	
	extern hiliteInfo maxHiliteInfo[];
#endif

	FuncPtr enter_down_func, enter_up_func;

	if(!pThis || !pTMI)
		return E_ZM_AEE_BADPARAM;

	zmaee_memset(pTMI, 0, sizeof(ZMAEE_THEME_MMINFO));
	GetSequenceItemIds(IDLE_SCREEN_MENU_ID, menu_item_list);
	GetSequenceStringIds(IDLE_SCREEN_MENU_ID, pTMI->str_id);	
	pTMI->item_count = GetNumOfChild(IDLE_SCREEN_MENU_ID);

	lsk_down_func = GetKeyHandler(KEY_LSK, KEY_EVENT_DOWN);
	lsk_up_func   = GetKeyHandler(KEY_LSK, KEY_EVENT_UP);

	rsk_down_func = GetKeyHandler(KEY_RSK, KEY_EVENT_DOWN);
	rsk_up_func   = GetKeyHandler(KEY_RSK, KEY_EVENT_UP);

	left_arrow_down_func = GetKeyHandler(KEY_LEFT_ARROW, KEY_EVENT_DOWN);
	left_arrow_up_func   = GetKeyHandler(KEY_LEFT_ARROW, KEY_EVENT_UP);

	right_arrow_down_func = GetKeyHandler(KEY_RIGHT_ARROW, KEY_EVENT_DOWN);
	right_arrow_up_func   = GetKeyHandler(KEY_RIGHT_ARROW, KEY_EVENT_UP);

	up_arrow_down_func = GetKeyHandler(KEY_UP_ARROW, KEY_EVENT_DOWN);
	up_arrow_up_func   = GetKeyHandler(KEY_UP_ARROW, KEY_EVENT_UP);

	down_arrow_down_func = GetKeyHandler(KEY_DOWN_ARROW, KEY_EVENT_DOWN);
	down_arrow_up_func   = GetKeyHandler(KEY_DOWN_ARROW, KEY_EVENT_UP);

	enter_down_func = GetKeyHandler(KEY_ENTER, KEY_EVENT_DOWN);
	enter_up_func = GetKeyHandler(KEY_ENTER, KEY_EVENT_UP);

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	pTMI->exit_func = GoBackHistory;
#else
#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
	pTMI->exit_func = mmi_idle_display;
#else
	pTMI->exit_func = DisplayIdleScreen;
#endif
#endif

	gdi_layer_lock_frame_buffer();
	for(i = 0; i < pTMI->item_count; i++) {
#if (ZM_AEE_MTK_SOFTVERN < 0x0816)
		if(maxHiliteInfo[menu_item_list[i]].entryFuncPtr)
		{
			maxHiliteInfo[menu_item_list[i]].entryFuncPtr();
			pTMI->entry_func[i] = get_left_softkey_function(KEY_EVENT_UP);
		}
#else
		{
			FuncPtr func= mmi_frm_get_hilite_hdlr(menu_item_list[i]);
			if(func)
			{
				func();
				pTMI->entry_func[i] = get_left_softkey_function(KEY_EVENT_UP);
			}
		}
#endif		
	}
	gdi_layer_unlock_frame_buffer();
	
	pTMI->lsk_str_id = STR_GLOBAL_OK;
	pTMI->rsk_str_id = STR_GLOBAL_BACK;
	memcpy(pTMI->menu_types, sg_zmaee_menu_types, sizeof(sg_zmaee_menu_types));

	SetKeyHandler(lsk_down_func, KEY_LSK, KEY_EVENT_DOWN);
	SetKeyHandler(lsk_up_func, KEY_LSK, KEY_EVENT_UP);

	SetKeyHandler(rsk_down_func, KEY_RSK, KEY_EVENT_DOWN);
	SetKeyHandler(rsk_up_func, KEY_RSK, KEY_EVENT_UP);

	SetKeyHandler(left_arrow_down_func, KEY_LEFT_ARROW, KEY_EVENT_DOWN);
	SetKeyHandler(left_arrow_up_func, KEY_LEFT_ARROW, KEY_EVENT_UP);

	SetKeyHandler(right_arrow_down_func, KEY_RIGHT_ARROW, KEY_EVENT_DOWN);
	SetKeyHandler(right_arrow_up_func, KEY_RIGHT_ARROW, KEY_EVENT_UP);

	SetKeyHandler(up_arrow_down_func, KEY_UP_ARROW, KEY_EVENT_DOWN);
	SetKeyHandler(up_arrow_up_func, KEY_UP_ARROW, KEY_EVENT_UP);

	SetKeyHandler(down_arrow_down_func, KEY_DOWN_ARROW, KEY_EVENT_DOWN);
	SetKeyHandler(down_arrow_up_func, KEY_DOWN_ARROW, KEY_EVENT_UP);

	SetKeyHandler(enter_down_func, KEY_ENTER, KEY_EVENT_DOWN);
	SetKeyHandler(enter_up_func, KEY_ENTER, KEY_EVENT_UP);

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取界面组件的范围
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	- po, pTSR is null
 * 	E_ZM_AEE_FAILURE	- 获取失败
 * 	E_ZM_AEE_SUCCESS	- 获取成功
 */
int	ZMAEE_ITheme_GetScreenRegion(AEE_ITheme *po, ZMAEE_THEME_SCRREG *pTSR)
{
	if((po == NULL) || (!pTSR))
		return E_ZM_AEE_BADPARAM;

	pTSR->status_bar_height = MMI_status_bar_height;
	pTSR->button_bar_height = MMI_button_bar_height;
	pTSR->title_bar_height = MMI_title_height;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 根据字符串 ID获取UCS2编码的字符串
 * 参数:
 * 	po					AEE_ITheme实例
 * 	str_id				字符串ID
 * 返回:
 * 	== NULL				po 为空，或者str_id无效
 * 	!= NULL				UCS2编码的字符串，内存的所有权归接口层
 */
unsigned short*	ZMAEE_ITheme_GetString(AEE_ITheme *po, unsigned short str_id)
{
	if(po == NULL)
		return NULL;

	return (unsigned short*)GetString(str_id);
}

/**
 * 保存主题应用的APPID
 * @type				- 0: 锁屏APPID,  1: 动态桌布APPID,	2: 主菜单APPID
 * @appId			- 对应的APPID
 * RETURN:
 *	E_ZM_AEE_SUCCESS	保存APPID成功
 *	E_ZM_AEE_FAILURE	保存失败
 *	E_ZM_AEE_BADPARAM	type无效
 */
int ZMAEE_ITheme_SetThemeAppId(AEE_ITheme *po, int type, unsigned long appId)
{
	if(!po)
		return E_ZM_AEE_BADPARAM;

	return ZMAEE_SetThemeAppId(type, appId);
}

/**
 * 获取主题应用的APPID
 * @type				- 0: 锁屏APPID,  1: 动态桌布APPID,  2: 主菜单APPID
 * RETURN:
 * 	0				type无效，或者获取失败
 * 	> 0				对应的APPID
 */
unsigned long ZMAEE_ITheme_GetThemeAppId(AEE_ITheme *po, int type)
{
	if(!po)
		return E_ZM_AEE_BADPARAM;

	return ZMAEE_GetThemeAppId(type);
}

static
int ZMAEE_ITheme_LoadEntryPeriodPCB(int nHandle, void* pUser)
{
	extern void ZMAEE_IStatusBar_DeInit_Ext(void);
	ZMAEE_IStatusBar_DeInit_Ext();

	ZMAEE_Exit_Ext();
	if(sg_zmaee_theme.pEntryFunc)
		sg_zmaee_theme.pEntryFunc();

	return 0;
}

void ZMAEE_ITheme_LoadEntryFunc(AEE_ITheme *po, void (*entry_func)(void))
{
	ZMAEE_THEME *pThis = (ZMAEE_THEME*)po;
	extern void ZMAEE_Clear_KeyLock_Flag(void);
	
	if(!po || !entry_func)
		return;

	ZMAEE_Clear_KeyLock_Flag();
	pThis->pEntryFunc = entry_func;
	ZMAEE_StartPeriodHandler(0x00, NULL, ZMAEE_ITheme_LoadEntryPeriodPCB);
}


#else // __ZMAEE_APP_THEME__

int ZMAEE_ITheme_New(ZM_AEECLSID clsId, void** pObj)
{
	*pObj = NULL;
	return E_ZM_AEE_CLASSNOTSUPPORT;
}

#endif // (defined __ZMAEE_APP_THEME__) || (defined __ZMAEE_APP_KEYPAD_LOCK__)
#endif // __ZMAEE_APP__
