#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"

#include "kal_release.h"

#include "gdi_datatype.h"
#include "gdi_features.h"
#include "gdi_include.h"
#include "gdi_image.h"
#include "gdi_const.h"

#include "gui.h"
#include "Unicodexdcl.h"
#include "app_mem.h"
#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_mem_gprot.h"
#endif

#include "lcd_if.h"
#include "med_utility.h"
#include "ScreenRotationGprot.h"
#ifdef WIN32
#include "SettingGprots.h"//WLJ
#endif

#ifdef ZMAEE_USE_CACHE_MEMORY
#include "cache_sw.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x0852)
extern void* mmi_frm_scrmem_alloc(U32 mem_size);
extern void mmi_frm_scrmem_free(void * mem_ptr);
#endif
extern void gd_color_to_rgb_16(U32 *A, U32 *R, U32 *G, U32 *B, gdi_color c);
extern void dm_redraw_category_screen(void);


zm_extern void	ZMAEE_GDI_BitBlt(
		ZMAEE_LayerInfo* layer,
		int x,
		int y,
		ZMAEE_BitmapInfo* bi,
		ZMAEE_Rect* rect,
		int btrans);
zm_extern void ZMAEE_GDI_BitBlt_Ext(
	ZMAEE_LayerInfo* layer,
	int x,
	int y,
    ZMAEE_BitmapInfo* bi,
	ZMAEE_Rect* rect,
	int transform,
	int btrans);
zm_extern void ZMAEE_StretchBlt(
	ZMAEE_LayerInfo* li,
	ZMAEE_Rect*	rc_dst,
	ZMAEE_BitmapInfo* bi,
	ZMAEE_Rect*	rc_src,
	int bTrans);
zm_extern int ZMAEE_IBitmap_Create(int width, 
						 int height, 
						 int pal_size,
						 ZMAEE_ColorFormat cf, 
						 AEE_PFN_MALLOC pMalloc, 
						 AEE_PFN_FREE pFree, 
						 void** pBitmap);

zm_extern int ZMAEE_IImage_DrawImage(AEE_IImage* po, int x, int y, int cx, int cy, int nFrame);
zm_extern unsigned int ZMAEE_GetMaxAllocLayerSize(void);
zm_extern int ZMAEE_IBitmap_LoadFile(AEE_IBitmap** po, const char *szFilePath, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
zm_extern int ZMAEE_IBitmap_New(ZM_AEECLSID clsId,void ** pObj, int bit_size, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
zm_extern int ZMAEE_IBitmap_Release(AEE_IBitmap* po);
zm_extern int ZMAEE_IImage_New(ZM_AEECLSID clsId,void ** pObj, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
zm_extern int ZMAEE_IBitmap_GetDimension(AEE_IBitmap* po, int *width, int *height);
zm_extern void ZMAEE_IBitmap_GetInfo(AEE_IBitmap* po, ZMAEE_BitmapInfo* pBI);
zm_extern unsigned int zmaee_wcslen(const unsigned short * str);
zm_extern void ZMAEE_DebugPrint(const char *pszFormat, ...);
zm_extern void* ZMAEE_MALLOC(int size);
zm_extern void ZMAEE_FREE(void *p);
#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
zm_extern int  ZMAEE_GetMaxAllocMedMem(void);
#endif
#ifdef WIN32
extern GDI_HANDLE dm_get_wallpaper_layer(void);//WLJ
#endif

#define ZMAEE_IDISPLAY_FUNC_COUNT		57

static int ZMAEE_IDisplay_AddRef(AEE_IDisplay* po);
zm_extern int ZMAEE_IDisplay_Release(AEE_IDisplay* po);
static int ZMAEE_IDisplay_GetMaxLayerCount(AEE_IDisplay* po);
static int ZMAEE_IDisplay_CreateLayer(AEE_IDisplay* po, int nLayerIdx, ZMAEE_Rect* pRect, ZMAEE_ColorFormat cf);
static int ZMAEE_IDisplay_FreeLayer(AEE_IDisplay* po, int nLayerIdx);
static int ZMAEE_IDisplay_FreeAllLayer(AEE_IDisplay* po);
static int ZMAEE_IDisplay_GetLayerInfo(AEE_IDisplay* po, int nLayerIdx, ZMAEE_LayerInfo* pLI);
static int ZMAEE_IDisplay_SetActiveLayer(AEE_IDisplay* po, int nLayerIdx);
static int ZMAEE_IDisplay_SetLayerPosition(AEE_IDisplay* po, int nLayerIdx, int x, int y);
static void ZMAEE_IDisplay_Update(AEE_IDisplay* po,int x, int y, int cx, int cy);
static int ZMAEE_IDisplay_UpdateEx(AEE_IDisplay* po,ZMAEE_Rect* pRect, int nCount, const int* nLayers);
static int ZMAEE_IDisplay_GetActiveLayer(AEE_IDisplay* po);
static void ZMAEE_IDisplay_LockScreen(AEE_IDisplay* po);
static void ZMAEE_IDisplay_UnlockScreen(AEE_IDisplay* po);
static int ZMAEE_IDisplay_RegisterCustomFont(AEE_IDisplay* po, ZMAEE_CustomFont* cusFont);
static int ZMAEE_IDisplay_SelectFont(AEE_IDisplay* po, ZMAEE_Font nFont);
static int ZMAEE_IDisplay_GetFontWidth(AEE_IDisplay* po);
static int ZMAEE_IDisplay_GetFontHeight(AEE_IDisplay* po);
static int ZMAEE_IDisplay_MeasureString(AEE_IDisplay* po, unsigned short* pwcsStr, int nwcsLen, int *nWidth, int *nHeight);
static int ZMAEE_IDisplay_DrawText(AEE_IDisplay* po, ZMAEE_Rect* pRect, unsigned short* pwcsStr, int nwcsLen, ZMAEE_Color clr, ZMAEE_FontAttr attr, ZMAEE_TextAlign align);
static int ZMAEE_IDisplay_SetTransColor(AEE_IDisplay* po, int trans_enable, ZMAEE_Color clr);
static int ZMAEE_IDisplay_SetClipRect(AEE_IDisplay* po, int x, int y, int cx, int cy);
static int ZMAEE_IDisplay_GetClipRect(AEE_IDisplay* po, int* x, int *y, int *cx, int *cy);
static void ZMAEE_IDisplay_SetPixel(AEE_IDisplay* po, int x, int y, ZMAEE_Color clr);
static void ZMAEE_IDisplay_DrawLine(AEE_IDisplay* po, int x1, int y1, int x2, int y2,  ZMAEE_Color clr);
static void ZMAEE_IDisplay_DrawRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color clr);
static void ZMAEE_IDisplay_FillRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color clr);
static void ZMAEE_IDisplay_DrawCircle(AEE_IDisplay* po, int x, int y, int r, ZMAEE_Color clr);
static void ZMAEE_IDisplay_FillCircle(AEE_IDisplay* po, int x, int y, int r, ZMAEE_Color clr);
static void ZMAEE_IDisplay_DrawArc(AEE_IDisplay* po, int x, int y , int r, int startAngle, int endAngle, ZMAEE_Color clr);
static void ZMAEE_IDisplay_FillArc(AEE_IDisplay* po, int x, int y , int r, int startAngle, int endAngle, ZMAEE_Color clr);
static void ZMAEE_IDisplay_FillGradientRect(AEE_IDisplay* po, ZMAEE_Rect* pRect,ZMAEE_Color clrStart, ZMAEE_Color clrEnd, ZMAEE_Color clrFrame, int nFrameWidth, ZMAEE_GRADIENT_TYPE type);
static void ZMAEE_IDisplay_AlphaBlendRect(AEE_IDisplay* po,ZMAEE_Rect* pRect,  ZMAEE_Color clr32);
 void ZMAEE_IDisplay_DrawImage(AEE_IDisplay* po, int x, int y,  AEE_IImage* pImage, int nFrame);
zm_extern void ZMAEE_IDisplay_DrawBitmap(AEE_IDisplay* po, int x, int y,  AEE_IBitmap* pBmp, ZMAEE_Rect* pSrcRc, int bTransparent);
static void ZMAEE_IDisplay_DrawBitmapEx(AEE_IDisplay* po, int x, int y, AEE_IBitmap* pBmp, ZMAEE_Rect* pSrcRc, ZMAEE_TransFormat nTrans, int bTransparent);
static void ZMAEE_IDisplay_DrawBitmapFrame(AEE_IDisplay* po, ZMAEE_Rect* pRect, AEE_IBitmap* pBmpLeftTop, int bTransparent);
static int ZMAEE_IDisplay_CreateBitmap(AEE_IDisplay* po, int width, int height, ZMAEE_ColorFormat cf, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap);
zm_extern int ZMAEE_IDisplay_LoadBitmap(AEE_IDisplay* po, const char* szFileName, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap);
 int ZMAEE_IDisplay_CreateImage(AEE_IDisplay* po, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pImage);
static void ZMAEE_IDisplay_DrawRoundRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color start_clr, ZMAEE_Color end_clr, int boarder_width);
static int ZMAEE_IDisplay_CreateLayerExt(AEE_IDisplay* po, int nLayerIdx, ZMAEE_Rect* pRect, ZMAEE_ColorFormat cf, void* pMemory, int nSize);
static int ZMAEE_IDisplay_SetOpacity(AEE_IDisplay* po, int opacity_enable, unsigned char opacity_value);
static void ZMAEE_IDisplay_BitBlt(AEE_IDisplay* po, int x, int y, ZMAEE_BitmapInfo* bi, ZMAEE_Rect* rect, ZMAEE_TransFormat transform, int btrans);
static int  ZMAEE_IDisplay_Flatten(AEE_IDisplay* po, int nCount, const int* nLayers, int with_clip);
static void ZMAEE_IDisplay_StretchBlt(AEE_IDisplay* po, ZMAEE_Rect* rcDst, ZMAEE_BitmapInfo* bi, ZMAEE_Rect* rcSrc, int btrans);
static void ZMAEE_IDisplay_DrawAntialiasingLine(AEE_IDisplay *po, int x1, int y1, int x2, int y2, ZMAEE_Color nColor);
static void ZMAEE_IDisplay_DrawWLine(AEE_IDisplay *po, int x1, int y1, int x2, int y2, ZMAEE_Color nColor, int width);
static int ZMAEE_IDisplay_GetDMLayerHdlr(AEE_IDisplay *po, int *layer_count, unsigned int layer_hdlrs[]);
static int ZMAEE_IDisplay_RelevanceLayer(AEE_IDisplay *po, int layer_idx, unsigned int layer_hdlr);
void ZMAEE_IDisplay_Refresh(AEE_IDisplay *po, int x, int y, int cx, int cy);
static void ZMAEE_IDisplay_DrawImageExt(AEE_IDisplay* po, int x, int y, int cx, int cy, AEE_IImage* pImage, int nFrame);
static void ZMAEE_IDisplay_DrawSysWallPaper(AEE_IDisplay *po);
static int  ZMAEE_IDisplay_DrawBorderText(AEE_IDisplay* po, ZMAEE_Rect* pRect, const unsigned short* pwcsStr, int nwcsLen, ZMAEE_Color clr, ZMAEE_Color border_clr, ZMAEE_FontAttr attr, ZMAEE_TextAlign align);
zm_extern int ZMAEE_IDisplay_PushAndSetAlphaLayer(AEE_IDisplay* po, int layer_idx);
zm_extern int ZMAEE_IDisplay_PopAndRestoreAlphaLayer(AEE_IDisplay* po);
zm_extern int ZMAEE_IDisplay_RotateScreen(AEE_IDisplay* po, ZMAEE_SCRROTATE_TYPE type);


typedef enum {
	ZMAEE_LAYERMEM_INVALID = 0,
	ZMAEE_LAYERMEM_BASE,
	ZMAEE_LAYERMEM_INTERNAL,
	ZMAEE_LAYERMEM_SCRMEM,
	ZMAEE_LAYERMEM_OUTMEM,
	ZMAEE_LAYERMEM_DM
}e_ZMAEE_LAYERMEM_TYPE;
typedef int	ZMAEE_LAYERMEM_TYPE;

// Layer Info
typedef struct {
	int 				is_valid;		// is this layer valid
	gdi_handle  		layer_hdlr;		// layer handler
	void				*layer_mem;		// outside layer memory
	unsigned int		layer_mem_size;	// Layer buffer size
	ZMAEE_LAYERMEM_TYPE	layer_mem_type;	// Layer buffer type
	ZMAEE_ColorFormat 	layer_cf;		// Layer color format
}ZMAEE_DISPLAY_LAYER;

// Custom Font
typedef struct {
	int					is_reged;		// is registered
	ZMAEE_CustomFont	cust_font;		// custom font structure
}ZMAEE_DISPLAY_CUSTFONT;

#define ZMAEE_DESKTOP_MAX_LAYER_COUNT 	8
#define ZMAEE_DESKTOP_LAYER_BASE_IDX		1000

typedef struct {
	void*					pVtbl;
	int     					nRef;
	ZMAEE_DISPLAY_LAYER 	layer_info[GDI_LAYER_TOTAL_LAYER_COUNT];
	ZMAEE_DISPLAY_LAYER 	desklayer_info[ZMAEE_DESKTOP_MAX_LAYER_COUNT];
	ZMAEE_DISPLAY_CUSTFONT	cust_font;
	ZMAEE_Font				sel_font;
	GDI_HANDLE				vui_bg_layer;
	unsigned char*			vui_bg_buf;
	int						vui_bg_size;
	int						vui_bg_type;
	int						drawing_swp;
	gdi_handle				active_sys_layer;
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0) || defined (MT6252) || defined (MT6250)
	gdi_handle				wallpaper_layer;
	unsigned char			*wallpaper_buf;
#endif
}ZMAEE_DISPLAY;

extern gdi_layer_struct *gdi_act_layer;
static ZMAEE_DISPLAY sg_zmaee_display = {0};

#ifdef __ZMAEE_APP_DWALLPAPER__
extern int sg_zmaee_start_screensaver;
#endif

/**
 * GUI Alpha值转换成GDI Alpha值
 * 参数:
 * 	alpha					alpha值
 * 返回:
 * 	unsigned int				GDI Alpha值
 */
unsigned int ZMAEE_GUIALPHA2GDIALPHA(unsigned char alpha)
{
	unsigned int alpha_value;

    alpha_value = (((unsigned int)(alpha)) << 8) / 100;
    if (256 <= alpha_value)
    {
        alpha_value = 255;
    }
    return alpha_value;
}


/**
 * 颜色格式转换函数
 * 参数:
 * 	cf						颜色格式
 * 	gdi_cf					输出参数，gdi层的颜色格式
 * 返回:
 * 	E_ZM_AEE_SUCCESS		转换成功
 * 	E_ZM_AEE_FAILURE		转换失败，gdi_cf参数无效
 */
int ZMAEE_IDisplay_CF2GDICF(ZMAEE_ColorFormat cf, gdi_color_format *gdi_cf)
{
	if(gdi_cf == NULL)
		return E_ZM_AEE_BADPARAM;

	switch(cf) {
		case ZMAEE_COLORFORMAT_8:
			*gdi_cf = GDI_COLOR_FORMAT_8;
			break;
		case ZMAEE_COLORFORMAT_16:
			*gdi_cf = GDI_COLOR_FORMAT_16;
			break;
		case ZMAEE_COLORFORMAT_24:
			*gdi_cf = GDI_COLOR_FORMAT_24;
			break;
		case ZMAEE_COLORFORMAT_32:
			*gdi_cf = GDI_COLOR_FORMAT_32;
			break;
		default:
			return E_ZM_AEE_FAILURE;
	};

	return E_ZM_AEE_SUCCESS;
}

/**
 * 调整Coordinate到屏幕内
 * 参数:
 * 	pX						输入输出参数，x位移
 * 	pY						输入输出参数，y位移
 * 	pCx						输入输出参数，宽度
 * 	pCy						输入输出参数，高度
 * 返回:
 * 	void
 */
void ZMAEE_IDisplay_AdjustCoordinate(int *pX, int *pY, int *pCx, int *pCy)
{
	if(!pX || !pY || !pCx || !pCy)
		return;

	if(*pX < 0)
		*pX = 0;

	if(*pY < 0)
		*pY = 0;

	if(*pX > UI_device_width)
		*pX = UI_device_width;

	if(*pY > UI_device_height)
		*pY = UI_device_height;

	if(*pCx < 0)
		*pCx = 0;

	if(*pCy < 0)
		*pCy = 0;

	if((*pX + *pCx) > UI_device_width)
		*pCx = UI_device_width - *pX;

	if((*pY + *pCy) > UI_device_height)
		*pCy = UI_device_height - *pY;
}

/**
 * 调整Rect到屏幕内
 * 参数:
 * 	pRect					输入输出参数，矩形区域
 * 返回:
 * 	void
 */
void ZMAEE_IDisplay_AdjustRect(ZMAEE_Rect *pRect)
{
	if(pRect == NULL)
		return;

	if(pRect->x < 0)
		pRect->x = 0;

	if(pRect->y < 0)
		pRect->y = 0;

	if(pRect->x > UI_device_width)
		pRect->x = UI_device_width;

	if(pRect->y > UI_device_height)
		pRect->y = UI_device_height;

	if(pRect->width < 0)
		pRect->width = 0;

	if(pRect->height < 0)
		pRect->height = 0;

	if((pRect->x + pRect->width) > UI_device_width)
		pRect->width = UI_device_width - pRect->x;

	if((pRect->y + pRect->height) > UI_device_height)
		pRect->height = UI_device_height - pRect->y;
}

/**
 * 字号转换函数
 * 参数:
 * 	zmaee_font				接口字体
 * 	font						输出参数，设备字体
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		转换失败
 * 	E_ZM_AEE_SUCCESS		转换成功
 */
int ZMAEE_IDisplay_ZMFont2Font(ZMAEE_Font zmaee_font, U8 *font)
{
	if(!font)
		return E_ZM_AEE_BADPARAM;

	switch(zmaee_font) {
		case ZMAEE_FONT_SMALL:
			*font = SMALL_FONT;
			break;
		case ZMAEE_FONT_NORMAL:
			*font = MEDIUM_FONT;
			break;
		case ZMAEE_FONT_LARGE:
			*font = LARGE_FONT;
			break;
		default:
			return E_ZM_AEE_FAILURE;
	};

	return E_ZM_AEE_SUCCESS;
}

/**
 * 颜色格式转换函数
 * 参数:
 * 	cf						颜色格式
 * 	gdi_cf					输出参数，gdi层的颜色格式
 * 返回:
 * 	E_ZM_AEE_SUCCESS		转换成功
 * 	E_ZM_AEE_FAILURE		转换失败，gdi_cf参数无效
 */
int ZMAEE_IDisplay_GDICF2CF(gdi_color_format gdi_cf, ZMAEE_ColorFormat *cf)
{
	if(cf == NULL)
		return E_ZM_AEE_BADPARAM;

	switch(gdi_cf) {
		case GDI_COLOR_FORMAT_8:
			*cf = ZMAEE_COLORFORMAT_8;
			break;
		case GDI_COLOR_FORMAT_16:
			*cf = ZMAEE_COLORFORMAT_16;
			break;
		case GDI_COLOR_FORMAT_32:
		case GDI_COLOR_FORMAT_32_PARGB:
			*cf = ZMAEE_COLORFORMAT_32;
			break;
		case GDI_COLOR_FORMAT_24:
			*cf = ZMAEE_COLORFORMAT_24;
			break;
		default:
			return E_ZM_AEE_FAILURE;
	};

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取指定颜色格式的字节/像素
 * 参数:
 * 	cf						颜色格式
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	> 0 						字节/ 像素
 */
int ZMAEE_IDisplay_ZMCF2BytsPerPixel(ZMAEE_ColorFormat cf)
{
	int byts_per_pixel;

	switch(cf) {
		case ZMAEE_COLORFORMAT_8:
			byts_per_pixel = 1;
			break;
		case ZMAEE_COLORFORMAT_16:
			byts_per_pixel = 2;
			break;
		case ZMAEE_COLORFORMAT_24:
		case ZMAEE_COLORFORMAT_32:
			byts_per_pixel = 4;
			break;
		default:
			return E_ZM_AEE_BADPARAM;
	};

	return byts_per_pixel;
}


int ZMAEE_IDisplay_New(ZM_AEECLSID clsId, void** pObj)
{
	ZMAEE_DISPLAY* pThis = 0;
	GDI_RESULT gdi_ret;
	int ret;

	pThis = (ZMAEE_DISPLAY*)&sg_zmaee_display;
	if(pThis == 0) return E_ZM_AEE_NOMEMORY;

	pThis->pVtbl = ZMAEE_GetVtable(clsId);
	if(pThis->pVtbl == 0)
	{
		pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IDISPLAY_FUNC_COUNT,	
										ZMAEE_IDisplay_AddRef,
										ZMAEE_IDisplay_Release,
										ZMAEE_IDisplay_GetMaxLayerCount,
										ZMAEE_IDisplay_CreateLayer,
										ZMAEE_IDisplay_CreateLayerExt,
										ZMAEE_IDisplay_FreeLayer,
										ZMAEE_IDisplay_FreeAllLayer,
										ZMAEE_IDisplay_GetLayerInfo,
										ZMAEE_IDisplay_SetActiveLayer,
										ZMAEE_IDisplay_SetLayerPosition,
										ZMAEE_IDisplay_Update,
										ZMAEE_IDisplay_UpdateEx,
										ZMAEE_IDisplay_GetActiveLayer,
										ZMAEE_IDisplay_LockScreen,
										ZMAEE_IDisplay_UnlockScreen,
										ZMAEE_IDisplay_RegisterCustomFont,
										ZMAEE_IDisplay_SelectFont,
										ZMAEE_IDisplay_GetFontWidth,
										ZMAEE_IDisplay_GetFontHeight,
										ZMAEE_IDisplay_MeasureString,
										ZMAEE_IDisplay_DrawText,
										ZMAEE_IDisplay_SetTransColor,
										ZMAEE_IDisplay_SetOpacity,
										ZMAEE_IDisplay_SetClipRect,
										ZMAEE_IDisplay_GetClipRect,
										ZMAEE_IDisplay_SetPixel,
										ZMAEE_IDisplay_DrawLine,
										ZMAEE_IDisplay_DrawRect,
										ZMAEE_IDisplay_FillRect,
										ZMAEE_IDisplay_DrawRoundRect,
										ZMAEE_IDisplay_DrawCircle,
										ZMAEE_IDisplay_FillCircle,
										ZMAEE_IDisplay_DrawArc,
										ZMAEE_IDisplay_FillArc,
										ZMAEE_IDisplay_FillGradientRect,
										ZMAEE_IDisplay_AlphaBlendRect,
										ZMAEE_IDisplay_DrawImage,
										ZMAEE_IDisplay_DrawBitmap,
										ZMAEE_IDisplay_DrawBitmapEx,
										ZMAEE_IDisplay_DrawBitmapFrame,
										ZMAEE_IDisplay_CreateBitmap,
										ZMAEE_IDisplay_LoadBitmap,
										ZMAEE_IDisplay_CreateImage,
										ZMAEE_IDisplay_BitBlt,
										ZMAEE_IDisplay_Flatten,
										ZMAEE_IDisplay_StretchBlt,
										ZMAEE_IDisplay_DrawAntialiasingLine,
										ZMAEE_IDisplay_DrawWLine,
										ZMAEE_IDisplay_GetDMLayerHdlr,
										ZMAEE_IDisplay_RelevanceLayer,
										ZMAEE_IDisplay_Refresh,
										ZMAEE_IDisplay_DrawImageExt,
										ZMAEE_IDisplay_DrawSysWallPaper,
										ZMAEE_IDisplay_DrawBorderText,										
										ZMAEE_IDisplay_PushAndSetAlphaLayer,
										ZMAEE_IDisplay_PopAndRestoreAlphaLayer,
										ZMAEE_IDisplay_RotateScreen);
	}

	if(pThis->nRef == 0) {
		pThis->nRef = 1;
		pThis->sel_font = ZMAEE_FONT_NORMAL;

		if(pThis->layer_info[0].is_valid == 0) {
			// 初始化base layer
			ret = ZMAEE_IDisplay_GDICF2CF(gdi_act_layer->cf, &(pThis->layer_info[0].layer_cf));
			if(ret != E_ZM_AEE_SUCCESS) {
				return E_ZM_AEE_FAILURE;
			}
			
			gdi_ret = gdi_layer_get_base_handle(&(pThis->layer_info[0].layer_hdlr));
			if(gdi_ret != GDI_SUCCEED) {
				return E_ZM_AEE_FAILURE;
			}

			pThis->layer_info[0].is_valid = 1;
			pThis->layer_info[0].layer_mem_type = ZMAEE_LAYERMEM_BASE;
			//初始化desk base layer
			#ifdef __ZMAEE_APP_DESKTOP__
			pThis->desklayer_info[0] = pThis->layer_info[0];
			#endif
		}
	}else {
		pThis->nRef++;
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

static
int ZMAEE_IDisplay_AddRef(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef++;

	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_IDisplay_Release(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;
	if(pThis->nRef == 0)
	{
	}
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取支持的层数
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	> 0						支持的层数
 */
static
int ZMAEE_IDisplay_GetMaxLayerCount(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	return GDI_LAYER_TOTAL_LAYER_COUNT;
}

static ZMAEE_DISPLAY_LAYER* ZMAEE_IDisplay_CheckLayerIdx(AEE_IDisplay* po, int nLayerIdx, int includeBase)
{
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	
	if(   (includeBase== 0 && nLayerIdx < 1) 
		|| (includeBase == 1 && nLayerIdx < 0)
		|| (nLayerIdx < ZMAEE_DESKTOP_LAYER_BASE_IDX && nLayerIdx >= ZMAEE_IDisplay_GetMaxLayerCount(po)) 
		|| (nLayerIdx >= ZMAEE_DESKTOP_LAYER_BASE_IDX+ZMAEE_DESKTOP_MAX_LAYER_COUNT) )
		return 0;

	if(nLayerIdx >= ZMAEE_DESKTOP_LAYER_BASE_IDX)
	{
		nLayerIdx -= ZMAEE_DESKTOP_LAYER_BASE_IDX;
		pLayer = pThis->desklayer_info + nLayerIdx;
	}
	else
	{
		pLayer = pThis->layer_info+nLayerIdx;
	}

	return pLayer;

}


/**
 * 创建内部层
 * 参数:
 *	 @nLayerIdx 		取值范围 0 < nLayerIdx < MaxLayerCount
	 @pRect 			相对屏幕的区域
	 @cf				层的颜色深度
 * 返回:
	 E_ZM_AEE_SUCCESS	第nLayerIdx位置的层创建成功
	 E_ZM_AEE_NOMEMORY	创建层的内存不足
	 E_ZM_AEE_BADPARAM	nLayerIdx无效, pRect为NULL, cf无效
	 E_ZM_AEE_EXIST		指定层已创建
	 E_ZM_AEE_FAILURE	创建失败
 * 备注:
 *	 层的颜色深度只能为ZMAEE_COLORFORMAT_16或ZMAEE_COLORFORMAT_32
 */
static
int ZMAEE_IDisplay_CreateLayer(AEE_IDisplay* po, int nLayerIdx, 
										ZMAEE_Rect* pRect, ZMAEE_ColorFormat cf)
{
	GDI_RESULT gdi_ret;
	int ret;
	gdi_color_format gdi_cf;
	unsigned int layer_mem_size;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	// 参数校验
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 0);
	if(pLayer == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pRect == NULL)
		return E_ZM_AEE_BADPARAM;

	if((cf != ZMAEE_COLORFORMAT_16) && (cf != ZMAEE_COLORFORMAT_32))
		return E_ZM_AEE_BADPARAM;

	// 当前层被占用
	if(pLayer->is_valid == 1)
		return E_ZM_AEE_EXIST;

	// 颜色格式转换
	ret = ZMAEE_IDisplay_CF2GDICF(cf, &gdi_cf);
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_BADPARAM;
	}

	// 判断内部内存是否足够
	layer_mem_size = ZMAEE_IDisplay_ZMCF2BytsPerPixel(cf) * pRect->width * pRect->height;
	if(layer_mem_size <= ZMAEE_GetMaxAllocLayerSize()) {	// 用内部内存
		// 创建层
		gdi_ret = gdi_layer_create_cf(gdi_cf, 
									pRect->x, pRect->y, pRect->width, pRect->height, 
									&(pLayer->layer_hdlr));
	
		if(gdi_ret != GDI_SUCCEED) {
			return E_ZM_AEE_FAILURE;
		}

		pLayer->layer_mem = NULL;
		pLayer->layer_mem_size = 0;
		pLayer->layer_mem_type = ZMAEE_LAYERMEM_INTERNAL;
	} else {
		// 用外部screen内存
	#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)) || (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
		#ifdef __MED_IN_ASM__
			if(applib_mem_ap_get_max_alloc_size() < layer_mem_size)
		#else
			if(ZMAEE_GetMaxAllocMedMem() < layer_mem_size)		
		#endif
	#else
		if(applib_mem_screen_get_max_alloc_size() < layer_mem_size)
	#endif
		{
			return E_ZM_AEE_NOMEMORY;
		}
	#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)) || (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
		#ifdef __MED_IN_ASM__
			pLayer->layer_mem = (void*)applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, layer_mem_size);
		#else
			pLayer->layer_mem = (void*)med_alloc_ext_mem_framebuffer(layer_mem_size);
		#endif
	#else
		pLayer->layer_mem = (void*)applib_mem_screen_alloc_framebuffer(layer_mem_size);
	#endif
		if(pLayer->layer_mem == NULL) {
			return E_ZM_AEE_NOMEMORY;
		}

		gdi_ret = gdi_layer_create_cf_using_outside_memory(gdi_cf, 
										pRect->x, pRect->y, pRect->width, pRect->height,
										&(pLayer->layer_hdlr),
										(U8*)pLayer->layer_mem, 
										(S32)layer_mem_size);
		
		if(gdi_ret != GDI_SUCCEED) {
			return E_ZM_AEE_FAILURE;
		}

		pLayer->layer_mem_size = layer_mem_size;
		pLayer->layer_mem_type = ZMAEE_LAYERMEM_SCRMEM;
	}

	// 设置层信息
	pLayer->is_valid = 1;
	pLayer->layer_cf = cf;

	gdi_layer_push_and_set_active(pLayer->layer_hdlr);

	// 如果是32的层，则使能Opacity
	if(cf == ZMAEE_COLORFORMAT_32)
		gdi_layer_set_opacity(TRUE, 255);

	// 设置透明色
	gdi_layer_set_source_key(TRUE, gdi_act_color_from_rgb(255, 255, 0, 255));

	// 将图层全屏刷成透明色
	ZMAEE_IDisplay_FillRect(po, 0, 0, pRect->width, pRect->height, ZMAEE_GET_RGBA(255, 0, 255, 255));
	
	gdi_layer_pop_and_restore_active();
	
	return E_ZM_AEE_SUCCESS;
}

/**
 * 释放层, 第 0 层 base layer 不允许被释放,释放之后激活层为 base Layer
 * 参数:
 *   @nLayerIdx    取值范围 0 < nLayerIdx < MaxLayerCount
 * 返回:
	 E_ZM_AEE_SUCCESS  	释放成功，或者指定层已释放
	 E_ZM_AEE_BADPARAM  nLayerIdx无效
	 E_ZM_AEE_FAILURE    	释放失败
 */
static
int ZMAEE_IDisplay_FreeLayer(AEE_IDisplay* po, int nLayerIdx)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	GDI_RESULT gdi_ret;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 0);
	if(pLayer == NULL)
		return E_ZM_AEE_BADPARAM;
	
	// 检验该层是否有效
	if(pLayer->is_valid == 0)
		return E_ZM_AEE_SUCCESS;

	if(ZMAEE_IDisplay_GetActiveLayer(po) == nLayerIdx) {
		ZMAEE_IDisplay_SetActiveLayer(po, 0);
	}

	if(pLayer->layer_mem_type != ZMAEE_LAYERMEM_DM) {
		// 释放该层
		gdi_ret = gdi_layer_free(pLayer->layer_hdlr);
		if(gdi_ret != GDI_SUCCEED) {
			return E_ZM_AEE_FAILURE;
		}

		// Reset层信息
		if(pLayer->layer_mem_type == ZMAEE_LAYERMEM_SCRMEM) {
		#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)) || (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
			#ifdef __MED_IN_ASM__
				applib_mem_ap_free(pLayer->layer_mem);
			#else
				med_free_ext_mem(&(pLayer->layer_mem));
			#endif
		#else
			mmi_frm_scrmem_free(pLayer->layer_mem);
		#endif
		}
	}
	memset(pLayer, 0, sizeof(pThis->layer_info[nLayerIdx]));

	return E_ZM_AEE_SUCCESS;
}

/**
 * 释放所有层
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 * 	E_ZM_AEE_FAILURE		有一层或者几层未成功释放
 * 	E_ZM_AEE_SUCCESS		释放成功
 */
static
int ZMAEE_IDisplay_FreeAllLayer(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	int i, layer_count;
	int ret = E_ZM_AEE_SUCCESS;
	GDI_RESULT gdi_ret;

	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	// 释放base layer信息
	layer_count = ZMAEE_IDisplay_GetMaxLayerCount(po);
	for(i = 1; i < layer_count; i++) {
		if(pThis->layer_info[i].is_valid == 1) {
			if(pThis->layer_info[i].layer_mem_type != ZMAEE_LAYERMEM_DM) {
				gdi_ret = gdi_layer_free(pThis->layer_info[i].layer_hdlr);
				if(gdi_ret != GDI_SUCCEED) {
					ret = E_ZM_AEE_FAILURE;
					continue;
				}

				if(pThis->layer_info[i].layer_mem_type == ZMAEE_LAYERMEM_SCRMEM) {
				#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (__COSMOS_MMI_PACKAGE__)) || (defined MT6250 && (ZM_AEE_MTK_SOFTVERN >= 0x1220))
					#ifdef __MED_IN_ASM__
						applib_mem_ap_free(pThis->layer_info[i].layer_mem);
					#else
						med_free_ext_mem(&pThis->layer_info[i].layer_mem);
					#endif
				#else
					mmi_frm_scrmem_free((void*)pThis->layer_info[i].layer_mem);
				#endif
				}
			}
			memset(&pThis->layer_info[i], 0, sizeof(pThis->layer_info[i]));
		}
	}

	ZMAEE_IDisplay_SetActiveLayer(po, 0);
	return ret;
}

/**
 * 获取Layer的FrameBuffer
 * 参数:
 *	 @nLayerIdx    取值范围 0 <= nLayerIdx < MaxLayerCount
	 @pLI		   用于输出活动层
 * RETURN:
	 E_ZM_AEE_SUCCESS	获取层信息成功
	 E_ZM_AEE_BADPARAM	nLayerIdx无效, pLI为NULL
	 E_ZM_AEE_NOTEXIST	指定层未创建
	 E_ZM_AEE_FAILURE	获取信息失败
 */
static
int ZMAEE_IDisplay_GetLayerInfo(AEE_IDisplay* po, int nLayerIdx, ZMAEE_LayerInfo* pLI)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	U32 a, r, g, b;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 1);
	if(pLayer == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pLI == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pLayer->is_valid == 0)
		return E_ZM_AEE_NOTEXIST;

#ifndef __COSMOS_MMI_PACKAGE__
	{
		extern lcd_layer_struct gdi_layer_info[GDI_LAYER_TOTAL_LAYER_COUNT];
		gdi_layer_struct *layer_struct = (gdi_layer_struct*)(pLayer->layer_hdlr);
		pLI->cf = pLayer->layer_cf;
		pLI->nScreenX = layer_struct->offset_x;
		pLI->nScreenY = layer_struct->offset_y;
		if(nLayerIdx != 0) {
			pLI->nWidth = layer_struct->width;
			pLI->nHeight = layer_struct->height;
		} else {
			pLI->nWidth = UI_device_width;
			pLI->nHeight = UI_device_height;
		}
		pLI->rcClip.x = layer_struct->clipx1;
		pLI->rcClip.y = layer_struct->clipy1;
		pLI->rcClip.width = layer_struct->clipx2 - layer_struct->clipx1 + 1;
		pLI->rcClip.height = layer_struct->clipy2 - layer_struct->clipy1 + 1;
		pLI->pFrameBuf = layer_struct->buf_ptr;
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)		
		pLI->opacity_enable = (gdi_layer_info[layer_struct->id].opacity_enable)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		pLI->opacity_value = gdi_layer_info[layer_struct->id].opacity_value;
		pLI->trans_enable = (gdi_layer_info[layer_struct->id].source_key_enable)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		
		gdi_layer_push_and_set_active(pLayer->layer_hdlr);
		gdi_act_color_to_rgb(&a, &r, &g, &b, gdi_layer_info[layer_struct->id].source_key);
		gdi_layer_pop_and_restore_active();
		pLI->trans_color = ZMAEE_GET_RGBA(r, g, b, a);
#else
{
		BOOL opacity_enable;
		U8 opacity_value;
		BOOL src_key_enable;
		gdi_color src_key_color;
		
		gdi_layer_push_and_set_active(pLayer->layer_hdlr);
		gdi_layer_get_opacity(&opacity_enable, &opacity_value);
		gdi_layer_get_source_key(&src_key_enable, &src_key_color);
		pLI->opacity_enable = (opacity_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		pLI->opacity_value = opacity_value;
		pLI->trans_enable = (src_key_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		gdi_act_color_to_rgb(&a, &r, &g, &b, src_key_color);
		pLI->trans_color = ZMAEE_GET_RGBA(r, g, b, a);
		gdi_layer_pop_and_restore_active();
}
#endif
	}
#else
	{
		int clip_x1, clip_x2, clip_y1, clip_y2;
		BOOL opacity_enable;
		U8 opacity_value;
		BOOL src_key_enable;
		gdi_color src_key_color;

		gdi_layer_push_and_set_active(pLayer->layer_hdlr);
		
		pLI->cf = pLayer->layer_cf;

		gdi_layer_get_position(&pLI->nScreenX, &pLI->nScreenY);		
		if(nLayerIdx != 0) {
			gdi_layer_get_dimension(&pLI->nWidth, &pLI->nHeight);
		} else {
			pLI->nWidth = UI_device_width;
			pLI->nHeight = UI_device_height;
		}

		gdi_layer_get_clip(&clip_x1, &clip_y1, &clip_x2, &clip_y2);
		pLI->rcClip.x = clip_x1;
		pLI->rcClip.y = clip_y1;
		pLI->rcClip.width = clip_x2 - clip_x1 + 1;
		pLI->rcClip.height = clip_y2 - clip_y1 + 1;

		gdi_layer_get_buffer_ptr(&pLI->pFrameBuf);
		
		gdi_layer_get_opacity(&opacity_enable, &opacity_value);
		gdi_layer_get_source_key(&src_key_enable, &src_key_color);
		pLI->opacity_enable = (opacity_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		pLI->opacity_value = opacity_value;
		pLI->trans_enable = (src_key_enable == TRUE)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;
		gdi_act_color_to_rgb(&a, &r, &g, &b, src_key_color);
		pLI->trans_color = ZMAEE_GET_RGBA(r, g, b, a);
		
		gdi_layer_pop_and_restore_active();
	}
#endif

	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置当前活动层
 * 参数:
 *	 @nLayerIdx    取值范围 0 <= nLayerIdx < MaxLayerCount
 * 返回:
	 E_ZM_AEE_SUCCESS	设置活动层成功
	 E_ZM_AEE_BADPARAM	nLayerIdx无效
	 E_ZM_AEE_FAILURE	指定层未创建
 */
static
int ZMAEE_IDisplay_SetActiveLayer(AEE_IDisplay* po, int nLayerIdx)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 1);
	if(pLayer == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pLayer->is_valid == 0) {
		return E_ZM_AEE_FAILURE;
	}

	gdi_layer_set_active(pLayer->layer_hdlr);
	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置层的屏幕开始位置(x,y)
 * 参数:
 *	 @nLayerIdx 		取值范围 0 <= nLayerIdx < MaxLayerCount
 *	 @x 				相对屏幕左上角位置x
 *	 @y 				相对屏幕左上角位置y
 * 返回:
	 E_ZM_AEE_SUCCESS	设置成功
	 E_ZM_AEE_BADPARAM	nLayerIdx无效
	 E_ZM_AEE_FAILURE	指定层未创建，或者设置失败
 */
static
int ZMAEE_IDisplay_SetLayerPosition(AEE_IDisplay* po, int nLayerIdx, int x, int y)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	GDI_RESULT gdi_ret;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 1);
	if(pLayer == NULL) {
		return E_ZM_AEE_BADPARAM;
	}


	if(pLayer->is_valid == 0) {
		return E_ZM_AEE_FAILURE;
	}

	gdi_layer_push_and_set_active(pLayer->layer_hdlr);
	gdi_ret = gdi_layer_set_position((S32)x, (S32)y);
	gdi_layer_pop_and_restore_active();
	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 刷新所有屏幕
 * 参数:
 * 	po						AEE_IDISPLAY实例
 * 	x						x位移
 * 	y						y位移
 * 	cx						宽度
 * 	cy						高度
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_Update(AEE_IDisplay* po,int x, int y, int cx, int cy)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return;

	if((cx == 0) || (cy == 0))
		return;
	
	gdi_layer_blt_ext(pThis->layer_info[0].layer_hdlr, pThis->layer_info[1].layer_hdlr,
					pThis->layer_info[2].layer_hdlr, pThis->layer_info[3].layer_hdlr,
			#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (GDI_6_LAYERS)) || defined (MT6252) || defined (MT6250)||defined (MT6260) //sgf
					0, 0,
			#endif
					(S32)x, (S32)y, (S32)x + cx - 1, (S32)y + cy - 1);
}

/**
 * 刷新到屏幕
 * 参数:
 *	 @pRect 				相对屏幕的刷新区域
 *	 @nCount			要刷新的层的个数，最大为4层
 *	 @nLayers			要刷新的层的索引数组
 * 返回:
	 E_ZM_AEE_SUCCESS	刷新成功
	 E_ZM_AEE_BADPARAM	pRect为NULL, nCount非法, nLayers为NULL
	 E_ZM_AEE_FAILURE	刷新失败
 */
static
int ZMAEE_IDisplay_UpdateEx(AEE_IDisplay* po,ZMAEE_Rect* pRect, int nCount, const int* nLayers)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_handle update_layers[4] = {0};
	int i;
	GDI_RESULT gdi_ret;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(!pRect || !nLayers)
		return E_ZM_AEE_BADPARAM;

	if((pRect->width == 0) || (pRect->height == 0))
		return E_ZM_AEE_BADPARAM;

	if((nCount > 4) || (nCount <= 0))
		return E_ZM_AEE_BADPARAM;

	for(i = 0; i < nCount; i++) {

		ZMAEE_DISPLAY_LAYER* pLayer = 0;
		pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayers[i], 1);
		if(pLayer == NULL)
			return E_ZM_AEE_BADPARAM;

		if(!pLayer->is_valid)
			return E_ZM_AEE_BADPARAM;
		
		update_layers[i] = pLayer->layer_hdlr;
	}

	gdi_ret = gdi_layer_blt_ext(update_layers[0], update_layers[1], update_layers[2], update_layers[3],
						#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (GDI_6_LAYERS)) || defined (MT6252) || defined (MT6250)||defined (MT6260) //sgf
								NULL, NULL,
						#endif
								(S32)pRect->x, (S32)pRect->y, 
								(S32)pRect->x + pRect->width - 1,
								(S32)pRect->y + pRect->height - 1);
	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取当前的激活层
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	>= 0						激活的层索引值
 */
static
int ZMAEE_IDisplay_GetActiveLayer(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	int i, layer_count;
	gdi_handle layer_handle;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	layer_count = ZMAEE_IDisplay_GetMaxLayerCount(po);
	gdi_layer_get_active(&layer_handle);

	for(i = 0; i < layer_count; i++) {
		if(pThis->layer_info[i].is_valid == 1) {
			if(layer_handle == pThis->layer_info[i].layer_hdlr) {
				return i;
			}
		}
	}

	for(i = 0; i < layer_count; ++i){
		if(pThis->desklayer_info[i].is_valid == 1){
			if(layer_handle == pThis->desklayer_info[i].layer_hdlr)
				return (i + ZMAEE_DESKTOP_LAYER_BASE_IDX);
		}
	}

	return E_ZM_AEE_FAILURE;
}

/**
 * 对屏幕加锁，加锁后Update/UpdateEx均无效
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_LockScreen(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return;

	gdi_layer_get_active(&pThis->active_sys_layer);
	gdi_layer_lock_frame_buffer();
}

/**
 * 对屏幕解锁
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_UnlockScreen(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return;

	gdi_layer_unlock_frame_buffer();
	gdi_layer_set_active(pThis->active_sys_layer);
}

/**
 * 注册用户自定义字体
 * 参数:
 *	 @cusFont			用户自定义字体方法结构
 * 返回:
	 E_ZM_AEE_SUCCESS	注册成功
	 E_ZM_AEE_BADPARAM	cusFont为NULL
 */
static
int ZMAEE_IDisplay_RegisterCustomFont(AEE_IDisplay* po, ZMAEE_CustomFont* cusFont)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(!cusFont)
		return E_ZM_AEE_BADPARAM;

	pThis->cust_font.is_reged = 1;
	pThis->cust_font.cust_font.pfnDrawText = cusFont->pfnDrawText;
	pThis->cust_font.cust_font.pfnGetFontHeight = cusFont->pfnGetFontHeight;
	pThis->cust_font.cust_font.pfnGetFontWidth = cusFont->pfnGetFontWidth;
	pThis->cust_font.cust_font.pfnMeasureString = cusFont->pfnMeasureString;
	pThis->cust_font.cust_font.pUser = cusFont->pUser;

	return E_ZM_AEE_SUCCESS;
}

/**
 * Select 字体
 * 参数:
 *	 @nFont 				字体
 * 返回:
	 E_ZM_AEE_SUCCESS	Select字体成功
	 E_ZM_AEE_BADPARAM	nFont不合法
 */
static
int ZMAEE_IDisplay_SelectFont(AEE_IDisplay* po, ZMAEE_Font nFont)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	pThis->sel_font = nFont;
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取字体宽度
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	> 0						字体宽度
 */
static
int ZMAEE_IDisplay_GetFontWidth(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	U8 szBuf[4] = {0x51, 0x7F, 0x00, 0x00};
    stFontAttribute	aFont = {0, 0, 0, MEDIUM_FONT, 0, 1};
	int ret;
	S32 width, height;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->sel_font == ZMAEE_FONT_CUSTOM) {
		if(pThis->cust_font.is_reged == 0) {
			return E_ZM_AEE_FAILURE;
		}

		return pThis->cust_font.cust_font.pfnGetFontWidth(pThis->cust_font.cust_font.pUser);
	}

	ret = ZMAEE_IDisplay_ZMFont2Font(pThis->sel_font, (U8*)&(aFont.size));
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	gui_set_font((UI_font_type)&aFont);
	gui_measure_string((UI_string_type)szBuf, &width, &height);

	return (width / 2);
}

/**
 * 获取字体高度
 * 参数:
 * 	po						AEE_IDisplay实例
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	> 0						字体高度
 */
static
int ZMAEE_IDisplay_GetFontHeight(AEE_IDisplay* po)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	U8 szBuf[4] = {0x51, 0x7F, 0x00, 0x00};
	stFontAttribute aFont = {0, 0, 0, MEDIUM_FONT, 0, 1};
	int ret;
	S32 width, height;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->sel_font == ZMAEE_FONT_CUSTOM) {
		if(pThis->cust_font.is_reged == 0) {
			return E_ZM_AEE_FAILURE;
		}

		return pThis->cust_font.cust_font.pfnGetFontHeight(pThis->cust_font.cust_font.pUser);
	}

	ret = ZMAEE_IDisplay_ZMFont2Font(pThis->sel_font, (U8*)&(aFont.size));
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	gui_set_font((UI_font_type)&aFont);
	gui_measure_string((UI_string_type)szBuf, &width, &height);

	return height + 4;
}

/**
 * 测量字符串字体高度和宽度
 * 参数:
 *	 @pwcsStr			unicode字符串
 *	 @nwcsLen			unicode字符个数，如果nwcsLen为0，则nWidth = 0, nHeight = 0
 *	 @nWidth			输出宽度
 *	 @nHeight			输出高度
 * 返回:
	 E_ZM_AEE_SUCCESS	测量字符串成功
	 E_ZM_AEE_BADPARAM	pwcsStr为NULL
 * 备注:
	 如果nwcsLen小于0, 则测量的是unicode字符串的全部长度. 
	 如果nWidth或nHeight为NULL, 则可以忽略.
 */
static
int ZMAEE_IDisplay_MeasureString(AEE_IDisplay* po, unsigned short* pwcsStr, int nwcsLen, 
										int *nWidth, int *nHeight)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	stFontAttribute aFont = {0, 0, 0, MEDIUM_FONT, 0, 1};
	int ret, wcs_len;
	S32 width, height;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(!pwcsStr)
		return E_ZM_AEE_BADPARAM;

	if(!nWidth && !nHeight)
		return E_ZM_AEE_BADPARAM;

	// 内存拷贝，保证str为0结尾
	wcs_len = zmaee_wcslen((const unsigned short*)pwcsStr);
	if((nwcsLen < 0) || (nwcsLen > wcs_len))
		nwcsLen = wcs_len;	

	if(pThis->sel_font == ZMAEE_FONT_CUSTOM) {
		if(pThis->cust_font.is_reged == 0) {
			return E_ZM_AEE_FAILURE;
		}

		return pThis->cust_font.cust_font.pfnMeasureString(pThis->cust_font.cust_font.pUser, pwcsStr, nwcsLen, nWidth, nHeight);
	}

	ret = ZMAEE_IDisplay_ZMFont2Font(pThis->sel_font, (U8*)&(aFont.size));
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	gui_set_font((UI_font_type)&aFont);
	gui_measure_string_n((UI_string_type)pwcsStr, nwcsLen, (S32*)&width, (S32*)&height);


	if(    pwcsStr[0] == 0xFF0C 
		|| pwcsStr[0] == 0x3002
		|| pwcsStr[0] == 0xFF1B 
		|| pwcsStr[0] == 0xFF01 
		|| pwcsStr[0] == 0xFF1F 
		|| pwcsStr[0] == 0x3001 
		|| pwcsStr[0] == 0x002C 
		|| pwcsStr[0] == 0x002E
		|| pwcsStr[0] == 0x003B 
		|| pwcsStr[0] == 0x0021)

	{
		int tmp_width = 0;
		unsigned short s_zmaee_tmp_char[]={0x6211,0};
		gui_measure_string_n((UI_string_type)s_zmaee_tmp_char,1, &tmp_width,&height);
	}

	if(nWidth)
		*nWidth = width;
	if(nHeight)
		*nHeight = height + 4;

	return E_ZM_AEE_SUCCESS;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0)
int g_zmaee_draw_texting = 0;
#endif


/**
 * 画文本
 * 参数:
 *	 @pRect 			绘制文本的区域
 *	 @pwcsStr			unicode字符串
 *	 @nwcsLen			unicode字符个数
 *	 @clr				文本颜色
 *	 @attr				字体属性
 *	 @align 			文本对齐方式
 * 返回:
	 E_ZM_AEE_SUCCESS	测量字符串成功
	 E_ZM_AEE_BADPARAM	pRect为NULL, pwcsStr为NULL, 
 * 备注:
	 如果nwcsLen小于0, 则测量的是unicode字符串的全部长度.
	 algin处理规则:
	 文本默认靠左;
	 if (align & ZMAEE_ALIGN_RIGHT) 
		文本靠右;
	 else if (align & ZMAEE_ALIGN_CENTER) 
		文本水平居中;
	 文本默认靠上;
	 if (align & ZMAEE_ALIGN_BOTTOM) 
		文本靠下;
	 else if (align & ZMAEE_ALIGN_VCENTER) 
		文本垂直居中;
 */
static
int ZMAEE_IDisplay_DrawText(AEE_IDisplay* po, ZMAEE_Rect* pRect, unsigned short* pwcsStr, int nwcsLen, 
									ZMAEE_Color clr, ZMAEE_FontAttr attr, ZMAEE_TextAlign align)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	stFontAttribute aFont = {0, 0, 0, MEDIUM_FONT, 0, 1};
	int ret, wcs_len;
	int x1, x2, y1, y2;
	int clip_x1, clip_x2, clip_y1, clip_y2;
	S32 width, height;
	color nColor = {0xFF, 0xFF, 0xFF, 100};

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if((!pRect) || (!pwcsStr))
		return E_ZM_AEE_BADPARAM;

	wcs_len = zmaee_wcslen((const unsigned short*)pwcsStr);
	if((nwcsLen < 0) || (nwcsLen > wcs_len))
		nwcsLen = wcs_len;
	
	nColor.alpha = ZMAEE_GET_RGB_A(clr);
	nColor.r = ZMAEE_GET_RGB_R(clr);
	nColor.g = ZMAEE_GET_RGB_G(clr);
	nColor.b = ZMAEE_GET_RGB_B(clr);

	// 自定义字体处理
	if(pThis->sel_font == ZMAEE_FONT_CUSTOM) {
		ZMAEE_LayerInfo layer_info = {0};
		
		if(pThis->cust_font.is_reged == 0) {
			return E_ZM_AEE_FAILURE;
		}

		ret = ZMAEE_IDisplay_GetLayerInfo(po, ZMAEE_IDisplay_GetActiveLayer(po), &layer_info);
		if(ret != E_ZM_AEE_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}
		
		return pThis->cust_font.cust_font.pfnDrawText(pThis->cust_font.cust_font.pUser,
													pRect, &layer_info, pwcsStr, nwcsLen, 
													clr, attr, align);
	}

	// 字号
	ret = ZMAEE_IDisplay_ZMFont2Font(pThis->sel_font, (U8*)&(aFont.size));
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	// 字体属性
	if(attr & ZMAEE_FONT_ATTR_ITALIC) {
		aFont.italic = TRUE;
	}

	if(attr & ZMAEE_FONT_ATTR_BOLD) {
		aFont.bold = TRUE;
	}

	if(attr & ZMAEE_FONT_ATTR_UNLINE) {
		aFont.underline = TRUE;
	}

	// 对齐方式
	x1 = pRect->x;
	x2 = pRect->x + pRect->width - 1;
	y1 = pRect->y;
	y2 = pRect->y + pRect->height - 1;
	
	// 裁剪区域交集
	gdi_layer_get_clip((S32*)&clip_x1, (S32*)&clip_y1, (S32*)&clip_x2, (S32*)&clip_y2);
	if(x1 > clip_x1)	clip_x1 = x1;
	if(x2 < clip_x2)	clip_x2 = x2;
	if(y1 > clip_y1)	clip_y1 = y1;
	if(y2 < clip_y2)	clip_y2 = y2;

	if(clip_x1 > clip_x2 || clip_y1 > clip_y2 || pRect->width <= 0 || pRect->height <= 0)
		return E_ZM_AEE_SUCCESS;

	gui_set_font(&aFont);
	gui_measure_string_n((UI_string_type)pwcsStr, nwcsLen, &width, &height);
	if(nwcsLen == 1)
	{
		if(	   pwcsStr[0] == 0xFF0C 
			|| pwcsStr[0] == 0x3002
			|| pwcsStr[0] == 0xFF1B 
			|| pwcsStr[0] == 0xFF01 
			|| pwcsStr[0] == 0xFF1F 
			|| pwcsStr[0] == 0x3001 
			|| pwcsStr[0] == 0x002C 
			|| pwcsStr[0] == 0x002E
			|| pwcsStr[0] == 0x003B 
			|| pwcsStr[0] == 0x0021)
		{
			height += 4;			
			if((align & ZMAEE_ALIGN_BOTTOM) == 0)
				align = (align & (~(ZMAEE_ALIGN_TOP | ZMAEE_ALIGN_VCENTER))) | ZMAEE_ALIGN_BOTTOM;			
		}
	}
	if(r2lMMIFlag)
	{
		if(align & ZMAEE_ALIGN_LEFT) {
			x1 = x1 + width;
		} else if(align & ZMAEE_ALIGN_RIGHT) {
			x1 = x2;
		} else if(align & ZMAEE_ALIGN_CENTER) {
			x1 = x1 + (pRect->width + width) / 2;
		}
		else
		{
			x1 = x1 + width;
		}
	}
	else
	{
		if(align & ZMAEE_ALIGN_LEFT) {
			;
		} else if(align & ZMAEE_ALIGN_RIGHT) {
			x1 = x2 - width;
		} else if(align & ZMAEE_ALIGN_CENTER) {
			x1 = x1 + (pRect->width - width) / 2;
		}
	}
	if(align & ZMAEE_ALIGN_TOP) {
		;
	} else if(align & ZMAEE_ALIGN_VCENTER) {
		y1 = y1 + (pRect->height - height) / 2;
	} else if(align & ZMAEE_ALIGN_BOTTOM) {
		y1 = y2 - height;
	}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
	g_zmaee_draw_texting = 1;
	if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) == 1)
		ZMAEE_IDisplay_PushAndSetAlphaLayer(po, ZMAEE_IDisplay_GetActiveLayer(po));
#endif

	// 绘制文本
	gui_push_text_clip();
	gui_set_text_clip(clip_x1, clip_y1, clip_x2, clip_y2);
	gui_set_text_color(nColor);
	gui_set_font(&aFont);
	gui_move_text_cursor(x1, y1);
	gui_print_text_n(pwcsStr, nwcsLen);
	gui_pop_text_clip();

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
	g_zmaee_draw_texting = 0;
	if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) == 1)
		ZMAEE_IDisplay_PopAndRestoreAlphaLayer(po);
#endif

	return E_ZM_AEE_SUCCESS;
}

/**
 * 为当前活动层设置透明色
 * 参数:
 *	 @trans_enable		0 - disable, 1 - enable
 *	 @clr				32位颜色值
 * 返回:
	 E_ZM_AEE_SUCCESS	设置成功
	 E_ZM_AEE_FAILURE	设置失败
 */
static
int ZMAEE_IDisplay_SetTransColor(AEE_IDisplay* po, int trans_enable, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color src_clr;
	GDI_RESULT gdi_ret;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}
	src_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr));
	gdi_ret = gdi_layer_set_source_key((trans_enable)? TRUE: FALSE, src_clr);
	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 为当前活动层设置裁剪区域
 * 参数:
 *	 @x 				裁剪区域坐标x
 *	 @y 				裁剪区域坐标y
 *	 @cx				裁剪区域宽度
 *	 @cy				裁剪区域高度
 * 返回:
	 E_ZM_AEE_SUCCESS	设置成功
	 E_ZM_AEE_FAILURE	设置失败
 */
static
int ZMAEE_IDisplay_SetClipRect(AEE_IDisplay* po, int x, int y, int cx, int cy)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_FAILURE;
	}

	gdi_layer_set_clip((S32)x, (S32)y, (S32)(x + cx - 1), (S32)(y + cy - 1));
	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取当前活动层的裁剪区域
 * 参数:
 *	 @x 				输出裁剪区域坐标x
 *	 @y 				输出裁剪区域坐标y
 *	 @cx				输出裁剪区域宽度
 *	 @cy				输出裁剪区域高度
 * 返回:
	 E_ZM_AEE_SUCCESS	获取成功
	 E_ZM_AEE_FAILURE	获取失败
 */
static
int ZMAEE_IDisplay_GetClipRect(AEE_IDisplay* po, int* x, int *y, int *cx, int *cy)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	S32 x2, y2;

	if((pThis == NULL) || (!x) || (!y) || (!cx) || (!cy)) {
		return E_ZM_AEE_FAILURE;
	}

	gdi_layer_get_clip((S32*)x, (S32*)y, (S32*)&x2, (S32*)&y2);
	*cx = x2 - *x + 1;
	*cy = y2 - *y + 1;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置像素点
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						x坐标
 * 	y						y坐标
 * 	clr						颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_SetPixel(AEE_IDisplay* po, int x, int y, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	gdi_draw_point((S32)x, (S32)y, 
					gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr)));
}

/**
 * 绘制直线
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x1						直线起点x坐标
 * 	y1						直线起点y坐标
 * 	x2						直线终点x坐标
 * 	y2						直线终点y坐标
 * 	clr						直线颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawLine(AEE_IDisplay* po, int x1, int y1, int x2, int y2,  ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	gdi_draw_line((S32)x1, (S32)y1, (S32)x2, (S32)y2,
		gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr)));
}

/**
 * 画矩形
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						x坐标
 * 	y						y坐标
 * 	cx						宽度
 * 	cy						高度
 * 	clr						矩形边框颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	gdi_draw_rect((S32)x, (S32)y, (S32)(x + cx - 1), (S32)(y + cy - 1), 
		gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr)));
}

/**
 * 填充矩形颜色
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						x坐标
 * 	y						y坐标
 * 	cx						宽度
 * 	cy						高度
 * 	clr						填充颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_FillRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	gdi_draw_solid_rect((S32)x, (S32)y, (S32)(x + cx - 1), (S32)(y + cy - 1),
		gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr)));
}

/**
 * 画圆
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						圆心x坐标
 * 	y						圆心y坐标
 * 	r						半径
 * 	clr						颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawCircle(AEE_IDisplay* po, int x, int y, int r, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color src_clr;

	if(pThis == NULL) {
		return;
	}

	src_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr));
	gdi_draw_circle((S32)x, (S32)y, (S32)r, src_clr);
}

/**
 * 填充圆形区域
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						圆心x坐标
 * 	y						圆心y坐标
 * 	r						半径
 * 	clr						颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_FillCircle(AEE_IDisplay* po, int x, int y, int r, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color src_clr;

	if(pThis == NULL) {
		return;
	}

	src_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr));
	gdi_draw_solid_circle((S32)x, (S32)y, (S32)r, src_clr);
}

/**
 * 绘制圆弧
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						圆心x坐标
 * 	y						圆心y坐标
 * 	r						半径
 * 	startAngle				开始弧度
 * 	endAngle					结束弧度
 * 	clr						颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawArc(AEE_IDisplay* po, int x, int y , int r, int startAngle, int endAngle, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color src_clr;

	if(pThis == NULL) {
		return;
	}

	src_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr));
	gdi_draw_arc((S32)x, (S32)y, (S32)r, startAngle, endAngle, src_clr);
}

/**
 * 填充圆弧
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						圆心x坐标
 * 	y						圆心y坐标
 * 	r						半径
 * 	startAngle				开始角度
 * 	endAngle					结束角度
 * 	clr						颜色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_FillArc(AEE_IDisplay* po, int x, int y , int r, int startAngle, int endAngle, ZMAEE_Color clr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color src_clr;

	if(pThis == NULL) {
		return;
	}

	src_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clr), ZMAEE_GET_RGB_R(clr), ZMAEE_GET_RGB_G(clr), ZMAEE_GET_RGB_B(clr));
	gdi_draw_solid_arc((S32)x, (S32)y, (S32)r, startAngle, endAngle, src_clr);
}


/**
 * 绘制渐变举行
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	pRect					矩形区域
 * 	clrStart					开始颜色
 * 	clrEnd					结束颜色
 * 	clrFrame					frame颜色
 * 	nFrameWidth				渐变色宽度
 * 参数:
 * 	void
 */
static
void ZMAEE_IDisplay_FillGradientRect(AEE_IDisplay* po, ZMAEE_Rect* pRect,ZMAEE_Color clrStart, ZMAEE_Color clrEnd, ZMAEE_Color clrFrame, int nFrameWidth, ZMAEE_GRADIENT_TYPE type)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_color start_clr, end_clr, frame_clr;
	gdi_gradient_rect_style_enum style;

	if(pThis == NULL) {
		return;
	}

	if(!pRect)
		return;

	switch(type) {
		case ZMAEE_GRADIENT_RECT_HOR:
			style = GDI_GRADIENT_RECT_HOR;
			break;
		case ZMAEE_GRADIENT_RECT_VER:
			style = GDI_GRADIENT_RECT_VER;
			break;
		case ZMAEE_GRADIENT_RECT_DIA:
			style = GDI_GRADIENT_RECT_DIA;
			break;
		case ZMAEE_GRADIENT_RECT_DIA_INV:
			style = GDI_GRADIENT_RECT_DIA_INV;
			break;
		case ZMAEE_GRADIENT_RECT_FLIP:
			style = GDI_GRADIENT_RECT_FLIP;
			break;
		default:
			return;
	}

	start_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clrStart), ZMAEE_GET_RGB_R(clrStart), ZMAEE_GET_RGB_G(clrStart), ZMAEE_GET_RGB_B(clrStart));
	end_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clrEnd), ZMAEE_GET_RGB_R(clrEnd), ZMAEE_GET_RGB_G(clrEnd), ZMAEE_GET_RGB_B(clrEnd));
	frame_clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(clrFrame), ZMAEE_GET_RGB_R(clrFrame), ZMAEE_GET_RGB_G(clrFrame), ZMAEE_GET_RGB_B(clrFrame));	
	gdi_draw_gradient_rect((S32)pRect->x, (S32)pRect->y, (S32)(pRect->x + pRect->width - 1),
							(S32)(pRect->y + pRect->height - 1), start_clr, end_clr, frame_clr, 
							nFrameWidth, style);
}

/**
 * Alpha层混合的矩形
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	pRect					矩形区域
 * 	clr32					颜色
 * 返回:
 * 	void 
 */
static
void ZMAEE_IDisplay_AlphaBlendRect(AEE_IDisplay* po,ZMAEE_Rect* pRect,  ZMAEE_Color clr32)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_handle layer;
	color nColor = {0};

	if(pThis == NULL) {
		return;
	}

	if(!pRect) {
		return;
	}

	gdi_layer_get_active(&layer);
	nColor.alpha = ZMAEE_GET_RGB_A(clr32);
	nColor.r = ZMAEE_GET_RGB_R(clr32);
	nColor.g = ZMAEE_GET_RGB_G(clr32);
	nColor.b = ZMAEE_GET_RGB_B(clr32);
	gdi_effect_alpha_blending_rect(layer, (S32)pRect->x, (S32)pRect->y, (S32)(pRect->x + pRect->width - 1),
									(S32)(pRect->y + pRect->height - 1), (U32)nColor.alpha,
									(U32)nColor.r, (U32)nColor.g, (U32)nColor.b);
}

/**
 * 绘制Image
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						图片的x坐标
 * 	y						图片的y坐标
 * 	pImage					AEE_IImage对象
 * 	nFrame					绘制的帧数
 */

void ZMAEE_IDisplay_DrawImage(AEE_IDisplay* po, int x, int y,  AEE_IImage* pImage, int nFrame)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	ZMAEE_IImage_DrawImage(pImage, x, y, 0, 0, nFrame);
}

/**
 * 绘制位图
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						图片x坐标
 * 	y						图片y坐标
 * 	pBmp					Bitmap对象
 * 	pSrcRc					绘制Bitmap的对象区域
 * 	bTransparent				是否使用bitmap的透明色
 * 返回:
 * 	void
 */
void ZMAEE_IDisplay_DrawBitmap(AEE_IDisplay* po, int x, int y,  AEE_IBitmap* pBmp, ZMAEE_Rect* pSrcRc, int bTransparent)
{
	ZMAEE_DISPLAY *pDisplay = (ZMAEE_DISPLAY*)po;
	ZMAEE_BitmapInfo bmp_info = {0};
	ZMAEE_LayerInfo  layer_info = {0};
	int active_layer;

	if((pDisplay == NULL) || (pBmp == NULL) || (pSrcRc == NULL)) {
		ZMAEE_DebugPrint("ZMAEE_IDisplay_DrawBitmap: bad parameter");
		return;
	}

	// 获取bitmap对象信息
	ZMAEE_IBitmap_GetInfo(pBmp, &bmp_info);

	// 参数转换
	active_layer = ZMAEE_IDisplay_GetActiveLayer(po);
	if(active_layer < 0) {
		ZMAEE_DebugPrint("ZMAEE_IDisplay_DrawBitmap: Get Active layer failed");
		return;
	}
	
	if(ZMAEE_IDisplay_GetLayerInfo(po, active_layer, &layer_info) != E_ZM_AEE_SUCCESS) {
		ZMAEE_DebugPrint("ZMAEE_IDisplay_DrawBitmap: Get layer info failed");
		return;
	}
	
	// 绘制位图
	ZMAEE_GDI_BitBlt(&layer_info, (int)x, (int)y, &bmp_info, pSrcRc, bTransparent);
}

/**
 * 绘制位图，可旋转和镜像
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						x坐标
 * 	y						y坐标
 * 	pBmp					Bitmap对象
 * 	pSrcRc					绘制Bitmap对象的区域
 * 	nTrans					翻转类型
 * 	bTransparent				是否使用Bitmap的透明色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawBitmapEx(AEE_IDisplay* po, int x, int y, AEE_IBitmap* pBmp, ZMAEE_Rect* pSrcRc, ZMAEE_TransFormat nTrans, int bTransparent)
{
	ZMAEE_DISPLAY *pDisplay = (ZMAEE_DISPLAY*)po;	
	ZMAEE_BitmapInfo bmp_info = {0};
	ZMAEE_LayerInfo  layer_info = {0};
	int active_layer;

	if((pDisplay == NULL) || (pBmp == NULL) || (pSrcRc == NULL)) {
		return;
	}

	if((nTrans < 0) || (nTrans >= ZMAEE_TRANSFORMAT_MAX))
		return;
	
	// 获取图片信息
	ZMAEE_IBitmap_GetInfo(pBmp, &bmp_info);

	// 参数转换
	active_layer = ZMAEE_IDisplay_GetActiveLayer(po);
	if(active_layer < 0) {
		return;
	}
	
	if(ZMAEE_IDisplay_GetLayerInfo(po, active_layer, &layer_info) != E_ZM_AEE_SUCCESS) {
		return;
	}
	
	// 绘制位图
	ZMAEE_GDI_BitBlt_Ext(&layer_info, (int)x, (int)y, &bmp_info, pSrcRc, (int)nTrans, bTransparent);
}

/**
 * 画边框, 左上角图片 @pBmpLeftTop , 左右，上下，翻转后，形成4个角的图片边框
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	pRect					边框区域
 * 	pBmpLeftTop				边框图片，为左上角图标的bitmap对象
 * 	bTransparent				是否使用bitmap的透明色
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawBitmapFrame(AEE_IDisplay* po, ZMAEE_Rect* pRect, AEE_IBitmap* pBmpLeftTop, int bTransparent)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	ZMAEE_Rect src_rect = {0};

	if((pThis == NULL) || (pRect == NULL) || (pBmpLeftTop == NULL)) {
		return;
	}

	src_rect.x = src_rect.y = 0;
	if(ZMAEE_IBitmap_GetDimension(pBmpLeftTop, &src_rect.width, &src_rect.height) != E_ZM_AEE_SUCCESS) {
		return;
	}

	ZMAEE_IDisplay_DrawBitmapEx(po, pRect->x, pRect->y, pBmpLeftTop, &src_rect, ZMAEE_TRANSFORMAT_NORMAL, bTransparent);
	ZMAEE_IDisplay_DrawBitmapEx(po, pRect->x + pRect->width - src_rect.width - 1, pRect->y, pBmpLeftTop, &src_rect, 
						ZMAEE_TRANSFORMAT_MIRROR, bTransparent);
	ZMAEE_IDisplay_DrawBitmapEx(po, pRect->x + pRect->width - src_rect.width - 1, pRect->y + pRect->height - src_rect.height - 1, 
						pBmpLeftTop, &src_rect, ZMAEE_TRANSFORMAT_ROTATE_180, bTransparent);
	ZMAEE_IDisplay_DrawBitmapEx(po, pRect->x, pRect->y + src_rect.height - 1, pBmpLeftTop, &src_rect, 
						ZMAEE_TRANSFORMAT_MIRROR_180, bTransparent);
}

/**
 * 创建Bitmap
 * 参数:
 *	 @width 			Bitmap宽度
 *	 @height			Bitmap高度
 *	 @cf				Bitmap颜色深度
 *	 @pMalloc			内存分配函数
 *	 @pFree 			内存释放函数
 *	 @pBitmap			输出位图接口
 * 返回:
	 E_ZM_AEE_SUCCESS	创建位图成功
	 E_ZM_AEE_BADPARAM	width <= 0, height <= 0, cf不合法, pMalloc为NULL, pFree为NULL, pBitmap为NULL
	 E_ZM_AEE_NOMEMORY	内存不足
	 E_ZM_AEE_FAILURE	创建位图失败
 * 备注:
	 如果cf是索引的位图,会创建一个512字节大小的16位颜色的调色板
 */
static
int ZMAEE_IDisplay_CreateBitmap(AEE_IDisplay* po, int width, int height, ZMAEE_ColorFormat cf, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == NULL) {
		*pBitmap = NULL;
		return E_ZM_AEE_BADPARAM;
	}
	
	return ZMAEE_IBitmap_Create(width, height, 512, cf, pMalloc, pFree, pBitmap);
}


/**
 * 从文件载入Bitmap
 * 参数:
 *	 @szFileName		Bitmap文件路径
 *	 @pMalloc			内存分配函数
 *	 @pFree 			内存释放函数
 *	 @pBitmap			输出位图接口
 * 返回:
	 E_ZM_AEE_SUCCESS	载入位图成功
	 E_ZM_AEE_BADPARAM	szFileName为NULL, pMalloc为NULL, pFree为NULL, pBitmap为NULL
	 E_ZM_AEE_NOMEMORY	内存不足
	 E_ZM_AEE_FAILURE	载入位图失败, 文件不存在或者位图格式不正确
 */
int ZMAEE_IDisplay_LoadBitmap(AEE_IDisplay* po, const char* szFileName, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pBitmap)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	int ret;

	if(pThis == NULL) {
		*pBitmap = NULL;
		return E_ZM_AEE_BADPARAM;
	}

	ret = ZMAEE_IBitmap_LoadFile((AEE_IBitmap**)pBitmap, szFileName, pMalloc, pFree);
	if(ret != E_ZM_AEE_SUCCESS) {
		//ZMAEE_IBitmap_Release(*pBitmap);
		*pBitmap = NULL;
		return ret;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 创建Image接口
 * 参数:
 *	 @pMalloc			内存分配函数
 *	 @pFree 			内存释放函数
 *	 @pImage			输出Image接口
 * 返回:
	 E_ZM_AEE_SUCCESS	创建Image接口成功
	 E_ZM_AEE_BADPARAM	pMalloc为NULL, pFree为NULL, pImage为NULL
	 E_ZM_AEE_NOMEMORY	内存不足
	 E_ZM_AEE_FAILURE	载入Image接口失败
 */

int ZMAEE_IDisplay_CreateImage(AEE_IDisplay* po, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree, void** pImage)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	int ret;

	if(pThis == NULL) {
		*pImage = NULL;
		return E_ZM_AEE_BADPARAM;
	}

	ret = ZMAEE_IImage_New(ZM_AEE_CLSID_IMAGE, pImage, pMalloc, pFree);
	if(ret != E_ZM_AEE_SUCCESS) {
		*pImage = NULL;
		return ret;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 绘制渐变圆角矩形
 * 参数:
 * 	po						AEE_IDisplay实例
 * 	x						x坐标
 * 	y						y坐标
 * 	cx						宽度
 * 	cy						高度
 * 	start_clr					渐变起始颜色
 * 	end_clr					渐变结束颜色
 * 	frame_width				渐变区域的宽度
 * 返回:
 * 	void
 */
static
void ZMAEE_IDisplay_DrawRoundRect(AEE_IDisplay* po, int x, int y, int cx, int cy, ZMAEE_Color start_clr, ZMAEE_Color end_clr, int boarder_width)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	color start_color, end_color;

	if(pThis == NULL) return;

	start_color.alpha = ZMAEE_GET_RGB_A(start_clr);
	start_color.r = ZMAEE_GET_RGB_R(start_clr);
	start_color.g = ZMAEE_GET_RGB_G(start_clr);
	start_color.b = ZMAEE_GET_RGB_B(start_clr);

	end_color.alpha = ZMAEE_GET_RGB_A(end_clr);
	end_color.r = ZMAEE_GET_RGB_R(end_clr);
	end_color.g = ZMAEE_GET_RGB_G(end_clr);
	end_color.b = ZMAEE_GET_RGB_B(end_clr);

#if defined(MT6261)
	gui_draw_gradient_rounded_rectangle(x, y, x + cx - 1, y + cy - 1, start_color, end_color);//sgf, boarder_width);
#else
	gui_draw_gradient_rounded_rectangle(x, y, x + cx - 1, y + cy - 1, start_color, end_color, boarder_width);

#endif
}

/**
 * 创建外部内存层
 * 参数:
 *	 @nLayerIdx 		取值范围 0 < nLayerIdx < MaxLayerCount
	 @pRect 			相对屏幕的区域
	 @cf				层的颜色深度
	 @pMemory			外部内存数据指针
	 @nSize 			外部内存数据大小
 * 返回:
	 E_ZM_AEE_SUCCESS	第nLayerIdx位置的层创建成功
	 E_ZM_AEE_BADPARAM	nLayerIdx非法, pRect为NULL, cf非法, pMemory为NULL, size不足够大
	 E_ZM_AEE_EXIST 	指定层已创建
	 E_ZM_AEE_FAILURE	创建失败
 * 备注:
 *	 层的颜色深度只能为ZMAEE_COLORFORMAT_16或ZMAEE_COLORFORMAT_32
 */
static
int ZMAEE_IDisplay_CreateLayerExt(AEE_IDisplay* po, int nLayerIdx, ZMAEE_Rect* pRect, ZMAEE_ColorFormat cf, void* pMemory, int nSize)
{
	GDI_RESULT gdi_ret;
	int ret;
	gdi_color_format gdi_cf;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;
	// 参数校验
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayerIdx, 0);
	if(pLayer == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pRect == NULL)
		return E_ZM_AEE_BADPARAM;

	if((cf != ZMAEE_COLORFORMAT_16) && (cf != ZMAEE_COLORFORMAT_32))
		return E_ZM_AEE_BADPARAM;

	if((pMemory == NULL) || (nSize <= 0))
		return E_ZM_AEE_BADPARAM;

	// 当前层被占用
	if(pLayer->is_valid == 1)
		return E_ZM_AEE_EXIST;

	// 颜色格式转换
	ret = ZMAEE_IDisplay_CF2GDICF(cf, &gdi_cf);
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_BADPARAM;
	}

	// 判断内部内存是否足够
#ifdef	ZMAEE_USE_CACHE_MEMORY
	if(INT_QueryIsCachedRAM(pMemory, nSize)) {
		pLayer->layer_mem = (unsigned char*)virt_to_phys((unsigned int*)pMemory);
	} else 
#endif
	pLayer->layer_mem = pMemory;
	pLayer->layer_mem_size = nSize;
	pLayer->layer_mem_type = ZMAEE_LAYERMEM_OUTMEM;

	
	gdi_ret = gdi_layer_create_cf_using_outside_memory(gdi_cf, 
									pRect->x, pRect->y, pRect->width, pRect->height,
									&(pLayer->layer_hdlr),
									(U8*)pLayer->layer_mem, 
									(S32)nSize);
	
	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	}

	// 设置层信息
	pLayer->is_valid = 1;
	pLayer->layer_cf = cf;

	gdi_layer_push_and_set_active(pLayer->layer_hdlr);
	
	// 如果是32的层，则使能Opacity
	if(cf == ZMAEE_COLORFORMAT_32)
		gdi_layer_set_opacity(TRUE, 255);

	// 设置透明色
	gdi_layer_set_source_key(TRUE, gdi_act_color_from_rgb(255, 255, 0, 255));
	
	gdi_layer_pop_and_restore_active();
	
	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置图层的透明度
 * @opacity_enable		0 - disenable , 1 - enable
 * @opacity_value		0 - 255 的alpha值
 * RETURN:
 *	 E_ZM_AEE_SUCCESS	设置成功
 *	 E_ZM_AEE_FAILURE	设置失败
 */
static
int ZMAEE_IDisplay_SetOpacity(AEE_IDisplay* po, int opacity_enable, unsigned char opacity_value)
{
	ZMAEE_DISPLAY* pThis = (ZMAEE_DISPLAY*)po;
	GDI_RESULT gdi_ret;

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	gdi_ret = gdi_layer_set_opacity((!opacity_enable)? FALSE: TRUE, opacity_value);		

	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	} else {
		return E_ZM_AEE_SUCCESS;
	}
}

/**
 * BitBlt算法
 * 参数:
 *	@po 				AEE_IDisplay实例
 *	@x					绘图起始x坐标
 *	@y					绘图起始y坐标
 *	@bi 				bmpinfo数据
 *	@rect				图片的绘制区域
 *	@transform			翻转类型
 *	@btrans 			是否使用透明色
 * 返回:
 *	void
 */
static
void ZMAEE_IDisplay_BitBlt(AEE_IDisplay* po, int x, int y, ZMAEE_BitmapInfo* bi, ZMAEE_Rect* rect, ZMAEE_TransFormat transform, int btrans)
{
	ZMAEE_LayerInfo layer_info = {0};
	int ret, active_layer;

	if(!po || !bi || !rect)
		return;

	active_layer = ZMAEE_IDisplay_GetActiveLayer(po);
	if(active_layer < 0) {
		return;
	}
	
	ret = ZMAEE_IDisplay_GetLayerInfo(po, active_layer, &layer_info);
	if(ret != E_ZM_AEE_SUCCESS) {
		return;
	}

	ZMAEE_GDI_BitBlt_Ext(&layer_info, x, y, bi, rect, transform, btrans);
}

/**
 * 将多个图层合并到当前激活层上，最多支持4层合并
 * @nCount				合并的层个数
 * @nLayers				合并层的索引数组
 * @with_clip				0 - 不用clip，1 - 用clip，clip为激活层的裁剪区，使用clip，则只合并clip区域内的数据
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	nCount == 0, po, nLayers is null
 * 	E_ZM_AEE_FAILURE	合并失败
 * 	E_ZM_AEE_SUCCESS	合并成功
 */
static int  ZMAEE_IDisplay_Flatten(AEE_IDisplay* po, int nCount, const int* nLayers, int with_clip)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_handle update_layers[4] = {0};
	int i;
	GDI_RESULT gdi_ret;

	if((pThis == NULL) || (!nLayers)) {
		return E_ZM_AEE_BADPARAM;
	}

	if((nCount <= 0) || (nCount > 4)) {
		return E_ZM_AEE_BADPARAM;
	}

	for(i = 0; i < nCount; i++) {
		ZMAEE_DISPLAY_LAYER* pLayer = 0;
		pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, nLayers[i], 1);
		if(pLayer == NULL)
			return E_ZM_AEE_BADPARAM;
		
		if(!pLayer->is_valid)
			return E_ZM_AEE_BADPARAM;

		update_layers[i] = pLayer->layer_hdlr;
	}

	if(with_clip) {
		gdi_ret = gdi_layer_flatten_with_clipping_ext(update_layers[0], update_layers[1], update_layers[2], update_layers[3]
#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (GDI_6_LAYERS)) || defined (MT6252) || defined (MT6250)||defined (MT6260) //sgf
			, NULL, NULL
#endif
		);
	} else {
		gdi_ret = gdi_layer_flatten_ext(update_layers[0], update_layers[1], update_layers[2], update_layers[3]
#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (GDI_6_LAYERS)) || defined (MT6252) || defined (MT6250)||defined (MT6260) //sgf
			, NULL, NULL
#endif
		);		
	}

	if(gdi_ret != GDI_SUCCEED) {
		return E_ZM_AEE_FAILURE;
	} else {
		return E_ZM_AEE_SUCCESS;
	}
}

 /**
 * StretchBlt
 * 参数:
 * 	@po					AEE_IDisplay实例
 * 	@rcDst				屏幕的绘制区域
 * 	@bi					bmpinfo数据
 * 	@rcSrc				位图的显示区域
 *	@btrans				是否使用透明色, 1 - 使用透明色，0 - 不使用透明色
 */
void ZMAEE_IDisplay_StretchBlt(AEE_IDisplay* po, 
						   	   ZMAEE_Rect* rcDst, 
						       ZMAEE_BitmapInfo* bi, 
						       ZMAEE_Rect* rcSrc, 
						       int btrans)
{
	ZMAEE_LayerInfo layer_info = {0};
	
	if ((po == NULL) || (rcDst == NULL) || (bi == NULL) || (bi == NULL)) {
		return;
	}

	if(ZMAEE_IDisplay_GetLayerInfo(po, ZMAEE_IDisplay_GetActiveLayer(po), &layer_info) != E_ZM_AEE_SUCCESS)
		return;
	
	ZMAEE_StretchBlt(&layer_info, rcDst, bi, rcSrc, btrans);
}

/**
 * 带抗锯齿算法的画线接口
 */
static
void ZMAEE_IDisplay_DrawAntialiasingLine(AEE_IDisplay *po, int x1, int y1, int x2, int y2, ZMAEE_Color nColor)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL)
		return;

	gdi_draw_antialiasing_line(x1, y1, x2, y2,
		gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(nColor), ZMAEE_GET_RGB_R(nColor), ZMAEE_GET_RGB_G(nColor), ZMAEE_GET_RGB_B(nColor)));
}

/**
 * 带宽度的画线接口
 * @width				线的宽度，单位像素
 */
static
void ZMAEE_IDisplay_DrawWLine(AEE_IDisplay *po, int x1, int y1, int x2, int y2, ZMAEE_Color nColor, int width)
{
#if (defined MT6250)
	extern S32 UI_line_sign(S32 a);
#endif //ZMAEEMODIFY	
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	int dx, dy, p, c1, c2, x, y, xinc, yinc, i, j;
	gdi_color clr;

	if(pThis == NULL)	return;
	clr = gdi_act_color_from_rgb(ZMAEE_GET_RGB_A(nColor), ZMAEE_GET_RGB_R(nColor), ZMAEE_GET_RGB_G(nColor), ZMAEE_GET_RGB_B(nColor));

	dx = x2 - x1;
	dy = y2 - y1;
#if (defined MT6250)
	xinc = UI_line_sign(dx);
	yinc = UI_line_sign(dy);
#else
	xinc = dx;
	yinc = dy;
#endif//ZMAEEMODIFY
	dx = abs(dx);
	dy = abs(dy);
	x = x1;
	y = y1;
	if (dx >= dy)
	{
		p = (dy << 1) - dx;
		c1 = dy << 1;
		c2 = (dy - dx) << 1;
		for (i = 0; i <= dx; i++)
		{
			for (j = (width >> 1); j >= 0; j--)
			{
				gdi_draw_point(x, y - j, clr);
				gdi_draw_point(x, y + j, clr);
			}
			if (p < 0)
			{
				p += c1;
			}
			else
			{
				p += c2;
				y += yinc;
			}
			x += xinc;
		}
	}
	else
	{
		p = (dx << 1) - dy;
		c1 = dx << 1;
		c2 = (dx - dy) << 1;
		for (i = 0; i <= dy; i++)
		{
			for (j = (width >> 1); j >= 0; j--)
			{
				gdi_draw_point(x + j, y, clr);
				gdi_draw_point(x - j, y, clr);
			}
			if (p < 0)
			{
				p += c1;
			}
			else
			{
				p += c2;
				x += xinc;
			}
			y += yinc;
		}
	}
}

/**
 * 获取DM中的层句柄
 * @ layer_count			输出参数，DM的层个数，等于0表示没有创建的层，返回E_ZM_AEE_SUCCESS
 * @ layer_hdlrs			输出参数，DM的层的句柄，layer_hdlrs[0]表示墙纸层
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	po, layer_count, layer_hdlrs is null
 * 	E_ZM_AEE_FAILURE	获取失败
 * 	E_ZM_AEE_SUCCESS	获取成功，layer_count == 0时也返回成功
 */
static
int ZMAEE_IDisplay_GetDMLayerHdlr(AEE_IDisplay *po, int *layer_count, unsigned int layer_hdlrs[])
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0) && !defined (__MMI_VUI_LAUNCHER__) && !defined (__COSMOS_MMI_PACKAGE__) && !defined (__MMI_VUI_LAUNCHER_KEY__)

	extern void ZMAEE_GetDMLayers(int *count, unsigned int hdlrs[]);
#ifdef __ZMAEE_APP_DWALLPAPER__
	extern int sg_zmaee_start_screensaver;
#endif
#if defined(__MMI_VUI_HOMESCREEN__)
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern MMI_BOOL mmi_vhs_is_enable(void);
#endif
#endif

	if(!po || !layer_count || !layer_hdlrs)
		return E_ZM_AEE_BADPARAM;

#ifdef __ZMAEE_APP_DESKTOP__
{
	extern unsigned long AEE_Desktop_GetWallpaperLayer(void);
	unsigned int nLayerDesk = AEE_Desktop_GetWallpaperLayer();
	if(nLayerDesk)
	{
		*layer_count = 1;
		layer_hdlrs[0] = nLayerDesk;
		return E_ZM_AEE_SUCCESS;
	}
}
#endif//__ZMAEE_APP_DESKTOP__

#if !defined (MT6252) && !defined (MT6250)
#if defined(__MMI_VUI_HOMESCREEN__)
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	if(mmi_vhs_is_enable()) 
#else
	if(1)
#endif
	{
		ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
		
		if(pThis->vui_bg_layer == NULL) {
			*layer_count = 0;
		} else {
			*layer_count = 1;
			layer_hdlrs[0] = pThis->vui_bg_layer;
		}
	}
	else
#endif
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	if(sg_zmaee_start_screensaver == 1)
	{
		gdi_layer_get_base_handle((gdi_handle*)&layer_hdlrs[0]);
	}
	else
#endif
	layer_hdlrs[0] = dm_get_wallpaper_layer();
	if(layer_hdlrs[0] != 0)
		*layer_count = 1;
	else
		*layer_count = 0;

#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0) || (defined MT6252) || defined (MT6250)
	if(layer_hdlrs[0]) 
	{
		ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
		gdi_layer_struct *layer_struct = (gdi_layer_struct*)layer_hdlrs[0];
		
		if((layer_struct->cf != GDI_COLOR_FORMAT_16) && (layer_struct->cf != GDI_COLOR_FORMAT_32) && !pThis->wallpaper_layer) 
		{
			pThis->wallpaper_buf = med_alloc_ext_mem(UI_DEVICE_WIDTH * UI_DEVICE_HEIGHT * 2);
			if(!pThis->wallpaper_buf) {
				return E_ZM_AEE_SUCCESS;
			}

			if(GDI_SUCCEED != gdi_layer_create_cf_using_outside_memory(GDI_COLOR_FORMAT_16, 0, 0, 
				UI_DEVICE_WIDTH, UI_DEVICE_HEIGHT, &pThis->wallpaper_layer, pThis->wallpaper_buf, UI_DEVICE_WIDTH * UI_DEVICE_HEIGHT * 2))
			{
				pThis->wallpaper_layer = NULL;
				med_free_ext_mem((void**)&pThis->wallpaper_buf);
				pThis->wallpaper_buf = NULL;
				return E_ZM_AEE_SUCCESS;
			}

			layer_hdlrs[0] = pThis->wallpaper_layer;
			return E_ZM_AEE_SUCCESS;
		}
		
		if(pThis->wallpaper_layer)
			layer_hdlrs[0] = pThis->wallpaper_layer;
	}		
#endif
#endif

	return E_ZM_AEE_SUCCESS;

#else
	if(!po || !layer_count || !layer_hdlrs)
		return E_ZM_AEE_BADPARAM;

	
#ifdef __ZMAEE_APP_DESKTOP__
	{
		extern unsigned long AEE_Desktop_GetWallpaperLayer(void);
		unsigned int nLayerDesk = AEE_Desktop_GetWallpaperLayer();
		if(nLayerDesk)
		{
			*layer_count = 1;
			layer_hdlrs[0] = nLayerDesk;
			return E_ZM_AEE_SUCCESS;
		}
	}
#endif//__ZMAEE_APP_DESKTOP__

	if(sg_zmaee_start_screensaver == 1) {
		gdi_layer_get_base_handle((gdi_handle*)&layer_hdlrs[0]);
		*layer_count = 1;
	} else {
		extern unsigned long vapp_hs_wallpaper_zmaee_proxy_getlayer(void);
		ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

		layer_hdlrs[0] = vapp_hs_wallpaper_zmaee_proxy_getlayer();
		if(layer_hdlrs[0])
			*layer_count = 1;
		else
			*layer_count = 0;
	}

	return E_ZM_AEE_SUCCESS;
#endif
}

/**
 * 将系统创建的层关联到Display接口，为系统创建的层分配一个layer_idx
 * @ layer_idx				关联的Display层索引，0 < layer_idx < MAX_LAYER_COUNT
 * @ layer_hdlr			系统创建的层句柄
 * RETURN:
 * 	E_ZM_AEE_BADPARAM	po is null, layer_idx is valid
 * 	E_ZM_AEE_FAILURE	关联失败
 * 	E_ZM_AEE_SUCCESS	关联成功
 */
static
int ZMAEE_IDisplay_RelevanceLayer(AEE_IDisplay *po, int layer_idx, unsigned int layer_hdlr)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	gdi_layer_struct *layer_struct;

	if(!po)
		return E_ZM_AEE_BADPARAM;

	if((layer_idx <= 0) || (layer_idx >= ZMAEE_IDisplay_GetMaxLayerCount(po)))
		return E_ZM_AEE_BADPARAM;

	layer_struct = (gdi_layer_struct*)layer_hdlr;
	pThis->layer_info[layer_idx].is_valid = 1;
	pThis->layer_info[layer_idx].layer_hdlr = layer_hdlr;
	ZMAEE_IDisplay_GDICF2CF(layer_struct->cf, &pThis->layer_info[layer_idx].layer_cf);
	pThis->layer_info[layer_idx].layer_mem = NULL;
	pThis->layer_info[layer_idx].layer_mem_size = NULL;
	pThis->layer_info[layer_idx].layer_mem_type = ZMAEE_LAYERMEM_DM;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 刷新屏幕，刷新的层为之前Update过的层
 * @ x, y					刷新区域的x，y坐标
 * @ cx, cy				刷新区域的宽和高
 */
void ZMAEE_IDisplay_Refresh(AEE_IDisplay *po, int x, int y, int cx, int cy)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0) && !defined (__MMI_VUI_LAUNCHER__) && !defined (__COSMOS_MMI_PACKAGE__) && !defined (__MMI_VUI_LAUNCHER_KEY__)

#if defined(__MMI_VUI_HOMESCREEN__)
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	extern MMI_BOOL mmi_vhs_is_enable(void);
#endif
#endif
#ifdef __ZMAEE_APP_DWALLPAPER__
	extern int sg_zmaee_start_screensaver;
#endif

	if(!po)
		return;

#ifdef __ZMAEE_APP_DESKTOP__
{
	extern unsigned long AEE_Desktop_GetWallpaperLayer(void);
	extern void AEE_Desktop_OnUpdateWallpaperLayer(void);
	unsigned int nLayerDesk = AEE_Desktop_GetWallpaperLayer();
	if(nLayerDesk)
	{
		AEE_Desktop_OnUpdateWallpaperLayer();
		return;
	}
}
#endif//__ZMAEE_APP_DESKTOP__

#ifdef __ZMAEE_APP_DWALLPAPER__
	if(sg_zmaee_start_screensaver == 1)
	{
		gdi_handle active_layer, base_layer;

		gdi_layer_get_active(&active_layer);
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, 0, 0);
		gdi_layer_set_active(active_layer);

		gdi_layer_get_base_handle(&base_layer);
		gdi_layer_blt_ext(base_layer, NULL, NULL, NULL,
#if defined (GDI_6_LAYERS) || defined (MT6252) || defined (MT6250) //sgf
			NULL, NULL,
#endif
			0, 0, UI_device_width - 1, UI_device_height -1);
	}
	else
#endif
	{
#if !defined (MT6252) && !defined (MT6250)
	#if defined(__MMI_VUI_HOMESCREEN__)
	#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
		if(mmi_vhs_is_enable()) {
			extern void zmaee_vadp_p2v_hs_valiate(void);
			zmaee_vadp_p2v_hs_valiate();
			return;
		} else
	#else
	{
#ifdef __ZMAEE_APP_DWALLPAPER__ 	
		extern void  ZMAEE_WallPaper_Invalidate(void *pObj);
		extern void* ZMAEE_GetWallPaperObject(void);
		ZMAEE_WallPaper_Invalidate(ZMAEE_GetWallPaperObject());
#endif
		return;	
	}
	#endif
	#endif	
#endif


		dm_redraw_category_screen();
	}

#else

#ifdef __ZMAEE_APP_DESKTOP__
{
	extern unsigned long AEE_Desktop_GetWallpaperLayer(void);
	extern void AEE_Desktop_OnUpdateWallpaperLayer(void);
	unsigned int nLayerDesk = AEE_Desktop_GetWallpaperLayer();
	if(nLayerDesk)
	{
		AEE_Desktop_OnUpdateWallpaperLayer();
		return;
	}
}
#endif//__ZMAEE_APP_DESKTOP__


#ifdef __ZMAEE_APP_DWALLPAPER__ 
{
	if(sg_zmaee_start_screensaver) {
		gdi_handle active_layer, base_layer;

		gdi_layer_get_active(&active_layer);
		ZMAEE_Platform_HandleEvent(ZMAEE_EV_REPAINT, 0, 0);
		gdi_layer_set_active(active_layer);

		gdi_layer_get_base_handle(&base_layer);
		gdi_layer_blt_ext(base_layer, NULL, NULL, NULL,
	#if ((ZM_AEE_MTK_SOFTVERN >= 0x11A0) && defined (GDI_6_LAYERS)) || defined (MT6252) || defined (MT6250)||defined (MT6260) //sgf
			NULL, NULL,
	#endif
			0, 0, UI_device_width - 1, UI_device_height -1);
	} else {
		extern void  ZMAEE_WallPaper_Invalidate(void *pObj);
		extern void* ZMAEE_GetWallPaperObject(void);
		ZMAEE_WallPaper_Invalidate(ZMAEE_GetWallPaperObject());
	}
}
#endif

#endif
}

/**
 * 带缩放功能的绘制图片，目前只支持缩小
 * @ cx					缩放的宽度，如果为0，则不缩放
 * @ cy					缩放的高度，如果为0，则不缩放
 */
static
void ZMAEE_IDisplay_DrawImageExt(AEE_IDisplay* po, int x, int y, int cx, int cy, AEE_IImage* pImage, int nFrame)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	if(pThis == NULL) {
		return;
	}

	if(!cx || !cy)
		ZMAEE_IImage_DrawImage(pImage, x, y, 0, 0, nFrame);
	else
		ZMAEE_IImage_DrawImage(pImage, x, y, cx, cy, nFrame);
	
}



/************************************************************
  外部接口
 ************************************************************/
gdi_handle ZMAEE_IDisplay_GetLayerHandle(int layer_idx)
{
	ZMAEE_DISPLAY_LAYER* pLayer = 0;

	pLayer = ZMAEE_IDisplay_CheckLayerIdx((AEE_IDisplay *)&sg_zmaee_display, layer_idx, 1);
	if(pLayer == NULL)
		return NULL;

	if(pLayer->is_valid == 0)
		return NULL;

	return pLayer->layer_hdlr;
}

#define ZMAEE_BG_LAYER_HEAD_SIZE		(24)
#define ZMAEE_BG_LAYER_TYPE				(0x16)

void ZMAEE_DestryBgLayer(unsigned long h);

void ZMAEE_BgLayer_InitHead(unsigned char *vui_buf, int x, int y, int w, int h, int byts_per_pixtel)
{
	int buf_size = w * h * byts_per_pixtel + 13;
	int byts_per_row = w * byts_per_pixtel;

	vui_buf = vui_buf + 3;
	vui_buf[0] = ZMAEE_BG_LAYER_TYPE;
	vui_buf[1] = 0;
	vui_buf[2] = buf_size & 0xFF;
	vui_buf[3] = (buf_size & 0xFF00) >> 8;
	vui_buf[4] = (buf_size & 0xFF0000) >> 16;
	vui_buf[5] = h & 0xFF;
	vui_buf[6] = ((h & 0x0F00) >> 16) | ((w & 0x0F) << 4);
	vui_buf[7] = (w & 0xFF0) >> 4;

	vui_buf[8] = w & 0xFF;
	vui_buf[9] = (w & 0xFF00) >> 8;
	vui_buf[10] = (w & 0xFF0000) >> 16;
	vui_buf[11] = (w & 0xFF000000) >> 24;

	vui_buf[12] = h & 0xFF;
	vui_buf[13] = (h & 0xFF00) >> 8;
	vui_buf[14] = (h & 0xFF0000) >> 16;

	vui_buf[15] = (h & 0xFF000000) >> 24;

	vui_buf[16] = byts_per_pixtel << 3;

	vui_buf[17] = byts_per_row & 0xFF;
	vui_buf[18] = (byts_per_row & 0xFF00) >> 8;
	vui_buf[19] = (byts_per_row & 0xFF0000) >> 16;
	vui_buf[20] = (byts_per_row & 0xFF000000) >> 24;
}


unsigned long ZMAEE_CreateBgLayer(int x, int y, int w, int h)
{
	GDI_RESULT gdi_ret;

	if(sg_zmaee_display.vui_bg_layer != NULL) {
		ZMAEE_DestryBgLayer(sg_zmaee_display.vui_bg_layer);
	}

	sg_zmaee_display.vui_bg_size = w * h * 2;

#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() > sg_zmaee_display.vui_bg_size) {
		sg_zmaee_display.vui_bg_type = 2;
		sg_zmaee_display.vui_bg_buf = (unsigned char*)applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, sg_zmaee_display.vui_bg_size);
	}
#else
	if(med_ext_left_size() > sg_zmaee_display.vui_bg_size) {
		sg_zmaee_display.vui_bg_type = 2;
		sg_zmaee_display.vui_bg_buf = (unsigned char*)med_alloc_ext_mem_framebuffer(sg_zmaee_display.vui_bg_size);
	} 
#endif
#if (defined MT6250)
	else if(mmi_frm_scrmem_get_available_size() > sg_zmaee_display.vui_bg_size) {
		sg_zmaee_display.vui_bg_type = 1;
		sg_zmaee_display.vui_bg_buf = (unsigned char*)mmi_frm_scrmem_alloc_framebuffer(sg_zmaee_display.vui_bg_size);//(unsigned char*)mmi_frm_scrmem_alloc(sg_zmaee_display.vui_bg_size);
	}
#endif//ZMAEEMODIFY	
	else {
		sg_zmaee_display.vui_bg_type = 3;
		sg_zmaee_display.vui_bg_buf = (unsigned char*)ZMAEE_MALLOC(sg_zmaee_display.vui_bg_size);		
	}

	if(!sg_zmaee_display.vui_bg_buf) {
		return NULL;
	}

	ZMAEE_DebugPrint("ZMAEE_CreateBgLayer: vui_bg_type = %d", sg_zmaee_display.vui_bg_type);

	gdi_ret = gdi_layer_create_cf_using_outside_memory(GDI_COLOR_FORMAT_16, x, y, w, h, &sg_zmaee_display.vui_bg_layer,
		sg_zmaee_display.vui_bg_buf, sg_zmaee_display.vui_bg_size);
	if(gdi_ret != GDI_FAILED) {
		return sg_zmaee_display.vui_bg_layer;
	} else {
		return NULL;
	}
}

void ZMAEE_DestryBgLayer(unsigned long h)
{
	gdi_layer_free(h);

	if(sg_zmaee_display.vui_bg_buf) {
		switch(sg_zmaee_display.vui_bg_type) {
			case 1:
				mmi_frm_scrmem_free(sg_zmaee_display.vui_bg_buf);
				break;
			case 2:
				#ifdef __MED_IN_ASM__
					applib_mem_ap_free(sg_zmaee_display.vui_bg_buf);
				#else
					med_free_ext_mem((void**)&sg_zmaee_display.vui_bg_buf);
				#endif
				break;
			case 3:
				ZMAEE_FREE(sg_zmaee_display.vui_bg_buf);
				break;
		}

		sg_zmaee_display.vui_bg_buf = NULL;
		sg_zmaee_display.vui_bg_layer = NULL;
		sg_zmaee_display.vui_bg_size = 0;
		sg_zmaee_display.vui_bg_type = 0;
	}
}

void ZMAEE_GetBgLayerInfo(unsigned long handle, int* x, int *y, int* w, int* h, unsigned short** pBuf)
{
	gdi_layer_struct *layer_struct = (gdi_layer_struct*)handle;

	if(!x || !y || !w || !h || !pBuf)
		return;
	
	*x = layer_struct->offset_x;
	*y = layer_struct->offset_y;
	*w = layer_struct->width;
	*h = layer_struct->height;
	*pBuf = (unsigned short*)layer_struct->buf_ptr;
}

static
void ZMAEE_Draw_WallPaper(void)
{
	extern MMI_ID_TYPE idle_screen_wallpaper_ID;
	extern S8 *idle_screen_wallpaper_name;
	extern MMI_theme *current_MMI_theme;

	int img_width, img_height;
	int disp_x, disp_y;

	if(idle_screen_wallpaper_ID) {
		if(!mmi_phnset_check_themeid_wallpaper(idle_screen_wallpaper_ID)) {
			gdi_image_get_dimension_id(idle_screen_wallpaper_ID, &img_width, &img_height);

			disp_x = (UI_device_width - img_width) >> 1;
			disp_y = (UI_device_height - img_height) >> 1;
			
        	gdi_image_draw_id(disp_x, disp_y, idle_screen_wallpaper_ID);

			return;
        }
		else
		{
			gui_draw_filled_area(0, 0, UI_device_width, UI_device_height, current_MMI_theme->idle_bkg_filler);
		}
	} else if(idle_screen_wallpaper_name) {
		if(gdi_image_get_dimension_file(idle_screen_wallpaper_name, &img_width, &img_height) == GDI_SUCCEED) 
		{		
			int resized_x, resized_y;
			int resized_width, resized_height;

			if((img_width <= UI_device_width) && (img_height <= UI_device_height)) {
				resized_x = (UI_device_width - img_width) >> 1;
				resized_y = (UI_device_height - img_height) >> 1;

				resized_width = img_width;
				resized_height = img_height;
			} else {
				gdi_image_util_fit_bbox(UI_device_width, UI_device_height, img_width, img_height,
	                    &resized_x, &resized_y, &resized_width, &resized_height);
			}

			gdi_image_draw_resized_file(resized_x, resized_y, resized_width, resized_height, idle_screen_wallpaper_name);

			return;
		}
		else
		{
			PhnsetWallpaperBadFileCallBack(0);
		}
	}
}

static
void ZMAEE_IDisplay_DrawSysWallPaper(AEE_IDisplay *po)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0 || ZM_AEE_MTK_SOFTVERN >= 0x11B0)
	extern void draw_wallpaper(void);
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;

	ZMAEE_Draw_WallPaper();
#else
	extern void ZMAEE_WallPaper_Draw(void);
	ZMAEE_WallPaper_Draw();
#endif
}

int ZMAEE_IDisplay_IsDrawingSWP(void)
{
	return sg_zmaee_display.drawing_swp;
}

/**
 * 画文本
 * 参数:
 *	 @pRect 			绘制文本的区域
 *	 @pwcsStr			unicode字符串
 *	 @nwcsLen			unicode字符个数
 *	 @clr				文本颜色
 *	 @attr				字体属性
 *	 @align 			文本对齐方式
 * 返回:
	 E_ZM_AEE_SUCCESS	测量字符串成功
	 E_ZM_AEE_BADPARAM	pRect为NULL, pwcsStr为NULL, 
 * 备注:
	 如果nwcsLen小于0, 则测量的是unicode字符串的全部长度.
	 algin处理规则:
	 文本默认靠左;
	 if (align & ZMAEE_ALIGN_RIGHT) 
		文本靠右;
	 else if (align & ZMAEE_ALIGN_CENTER) 
		文本水平居中;
	 文本默认靠上;
	 if (align & ZMAEE_ALIGN_BOTTOM) 
		文本靠下;
	 else if (align & ZMAEE_ALIGN_VCENTER) 
		文本垂直居中;
 */
int ZMAEE_IDisplay_DrawBorderText(AEE_IDisplay* po, ZMAEE_Rect* pRect, const unsigned short* pwcsStr, int nwcsLen, ZMAEE_Color clr, ZMAEE_Color border_clr, ZMAEE_FontAttr attr, ZMAEE_TextAlign align)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	stFontAttribute aFont = {0, 0, 0, MEDIUM_FONT, 0, 1};
	int ret, wcs_len;
	int x1, x2, y1, y2;
	int clip_x1, clip_x2, clip_y1, clip_y2;
	S32 width, height;
	color nColor = {0xFF, 0xFF, 0xFF, 100};
	color border_color = {0xFF, 0xFF, 0xFF, 100};

	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if((!pRect) || (!pwcsStr))
		return E_ZM_AEE_BADPARAM;

	wcs_len = zmaee_wcslen((const unsigned short*)pwcsStr);
	if((nwcsLen < 0) || (nwcsLen > wcs_len))
		nwcsLen = wcs_len;
	
	nColor.alpha = ZMAEE_GET_RGB_A(clr);
	nColor.r = ZMAEE_GET_RGB_R(clr);
	nColor.g = ZMAEE_GET_RGB_G(clr);
	nColor.b = ZMAEE_GET_RGB_B(clr);

	border_color.alpha = ZMAEE_GET_RGB_A(border_clr);
	border_color.r = ZMAEE_GET_RGB_R(border_clr);
	border_color.g = ZMAEE_GET_RGB_G(border_clr);
	border_color.b = ZMAEE_GET_RGB_B(border_clr);

	// 自定义字体处理
	if(pThis->sel_font == ZMAEE_FONT_CUSTOM) {
		ZMAEE_LayerInfo layer_info = {0};
		
		if(pThis->cust_font.is_reged == 0) {
			return E_ZM_AEE_FAILURE;
		}

		ret = ZMAEE_IDisplay_GetLayerInfo(po, ZMAEE_IDisplay_GetActiveLayer(po), &layer_info);
		if(ret != E_ZM_AEE_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}
		
		return pThis->cust_font.cust_font.pfnDrawText(pThis->cust_font.cust_font.pUser,
													pRect, &layer_info, pwcsStr, nwcsLen, 
													clr, attr, align);
	}

	// 字号
	ret = ZMAEE_IDisplay_ZMFont2Font(pThis->sel_font, (U8*)&(aFont.size));
	if(ret != E_ZM_AEE_SUCCESS) {
		return E_ZM_AEE_FAILURE;
	}

	// 字体属性
	if(attr & ZMAEE_FONT_ATTR_ITALIC) {
		aFont.italic = TRUE;
	}

	if(attr & ZMAEE_FONT_ATTR_BOLD) {
		aFont.bold = TRUE;
	}

	if(attr & ZMAEE_FONT_ATTR_UNLINE) {
		aFont.underline = TRUE;
	}

	// 对齐方式
	x1 = pRect->x;
	x2 = pRect->x + pRect->width - 1;
	y1 = pRect->y;
	y2 = pRect->y + pRect->height - 1;
	
	// 裁剪区域交集
	gdi_layer_get_clip((S32*)&clip_x1, (S32*)&clip_y1, (S32*)&clip_x2, (S32*)&clip_y2);
	if(x1 > clip_x1)	clip_x1 = x1;
	if(x2 < clip_x2)	clip_x2 = x2;
	if(y1 > clip_y1)	clip_y1 = y1;
	if(y2 < clip_y2)	clip_y2 = y2;

	if(clip_x1 > clip_x2 || clip_y1 > clip_y2 || pRect->width <= 0 || pRect->height <= 0)
		return E_ZM_AEE_SUCCESS;

	gui_set_font(&aFont);
	gui_measure_string_n((UI_string_type)pwcsStr, nwcsLen, &width, &height);
	if(nwcsLen == 1)
	{
		if(	   pwcsStr[0] == 0xFF0C 
			|| pwcsStr[0] == 0x3002
			|| pwcsStr[0] == 0xFF1B 
			|| pwcsStr[0] == 0xFF01 
			|| pwcsStr[0] == 0xFF1F 
			|| pwcsStr[0] == 0x3001 
			|| pwcsStr[0] == 0x002C 
			|| pwcsStr[0] == 0x002E
			|| pwcsStr[0] == 0x003B 
			|| pwcsStr[0] == 0x0021)
		{
			height += 4;			
			if((align & ZMAEE_ALIGN_BOTTOM) == 0)
				align = (align & (~(ZMAEE_ALIGN_TOP | ZMAEE_ALIGN_VCENTER))) | ZMAEE_ALIGN_BOTTOM;			
		}
	}
	if(r2lMMIFlag)
	{
		if(align & ZMAEE_ALIGN_LEFT) {
			x1 = x1 + width;
		} else if(align & ZMAEE_ALIGN_RIGHT) {
			x1 = x2;
		} else if(align & ZMAEE_ALIGN_CENTER) {
			x1 = x1 + (pRect->width + width) / 2;
		}
		else
		{
			x1 = x1 + width;
		}
	}
	else
	{
		if(align & ZMAEE_ALIGN_LEFT) {
			;
		} else if(align & ZMAEE_ALIGN_RIGHT) {
			x1 = x2 - width;
		} else if(align & ZMAEE_ALIGN_CENTER) {
			x1 = x1 + (pRect->width - width) / 2;
		}
	}
	if(align & ZMAEE_ALIGN_TOP) {
		;
	} else if(align & ZMAEE_ALIGN_VCENTER) {
		y1 = y1 + (pRect->height - height) / 2;
	} else if(align & ZMAEE_ALIGN_BOTTOM) {
		y1 = y2 - height;
	}

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
	g_zmaee_draw_texting = 1;
	if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) == 1)
		ZMAEE_IDisplay_PushAndSetAlphaLayer(po, ZMAEE_IDisplay_GetActiveLayer(po));
#endif

	// 绘制文本
	gui_push_text_clip();
	gui_set_text_clip(clip_x1, clip_y1, clip_x2, clip_y2);
	gui_set_text_color(nColor);
	gui_set_text_border_color(border_color);
	gui_set_font(&aFont);
	gui_move_text_cursor(x1, y1);
	gui_print_bordered_text_n((UI_string_type)pwcsStr, nwcsLen);
	gui_pop_text_clip();

#if (ZM_AEE_MTK_SOFTVERN >= 0x11B0) && defined (__COSMOS_MMI_PACKAGE__)
	g_zmaee_draw_texting = 0;
	if(ZMAEE_IShell_ActiveApplet(ZMAEE_GetShell()) == 1)
		ZMAEE_IDisplay_PopAndRestoreAlphaLayer(po);
#endif


	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置Alpha Blending的图层，并保存之前的alpha blending图层。
 * 当在16位的图层上绘制32位png图片时，
 * 设置alpha blending图层就是32位png图片的背景图层，绘制png图片时，会与alpha blending图层
 * 做alpha运算
 * @layer_idx			alpha blending的图层，0 <= layer_idx < max_layer_count
 * RETURN:
 *	E_ZM_AEE_BADPARAM		po is null, or layer_idx is invalid
 *	E_ZM_AEE_FAILURE		设置失败
 *	E_ZM_AEE_SUCCESS		设置成功
 */
int ZMAEE_IDisplay_PushAndSetAlphaLayer(AEE_IDisplay* po, int layer_idx)
{
	ZMAEE_DISPLAY *pThis = (ZMAEE_DISPLAY*)po;
	ZMAEE_DISPLAY_LAYER* pLayer = 0;

	if(!pThis || (pLayer = ZMAEE_IDisplay_CheckLayerIdx(po, layer_idx, 1)) == NULL )
		return E_ZM_AEE_BADPARAM;

	if(pLayer->is_valid == 0)
		return E_ZM_AEE_BADPARAM;

	gdi_push_and_set_alpha_blending_source_layer(pLayer->layer_hdlr);

	return E_ZM_AEE_SUCCESS;
}

/**
 * 恢复之前的alpha blending图层
 * RETURN:
 *	E_ZM_AEE_BADPARAM		po is null
 *	E_ZM_AEE_FAILURE		恢复失败
 *	E_ZM_AEE_SUCCESS		恢复成功
 */
int ZMAEE_IDisplay_PopAndRestoreAlphaLayer(AEE_IDisplay* po)
{
	if(!po)
		return E_ZM_AEE_BADPARAM;

	gdi_pop_and_restore_alpha_blending_source_layer();

	return E_ZM_AEE_SUCCESS;
}

gdi_handle ZMAEE_IDisplay_GetWallpaperLayer(void)
{
#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	return sg_zmaee_display.wallpaper_layer;
#endif
#endif

	return NULL;
}

void ZMAEE_IDisplay_FreeWallpaperLayer(void)
{
#ifdef __ZMAEE_APP_DWALLPAPER__
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	ZMAEE_DISPLAY *pThis = &sg_zmaee_display;

	if(pThis->wallpaper_layer) {
		gdi_layer_free(pThis->wallpaper_layer);
		pThis->wallpaper_layer = NULL;
	}

	if(pThis->wallpaper_buf) {
		med_free_ext_mem((void**)&pThis->wallpaper_buf);
		pThis->wallpaper_buf = NULL;
	}
#endif
#endif
}

/**
 * 翻转屏幕，激活层必须为base layer
 * @type				翻转的角度
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		po is null, or type is invalid
 * 	E_ZM_AEE_FAILURE		激活层不是base layer
 * 	E_ZM_AEE_SUCCESS		翻转成功
 */
int ZMAEE_IDisplay_RotateScreen(AEE_IDisplay* po, ZMAEE_SCRROTATE_TYPE type)
{
#ifdef __MMI_SCREEN_ROTATE__
	int ret = 0;

	if(!po)
		return E_ZM_AEE_BADPARAM;

	ret =  ZMAEE_IDisplay_GetActiveLayer(po);
	if(ret != 0)
		return E_ZM_AEE_FAILURE;
	
	switch(type)
	{
		case ZMAEE_SCRROTATE_0:
        	 mmi_frm_screen_rotate(MMI_FRM_SCREEN_ROTATE_0);
			 break;
		case ZMAEE_SCRROTATE_90:
        	 mmi_frm_screen_rotate(MMI_FRM_SCREEN_ROTATE_90);
			 break;
		case ZMAEE_SCRROTATE_180:
        	 mmi_frm_screen_rotate(MMI_FRM_SCREEN_ROTATE_180);
			 break;
		case ZMAEE_SCRROTATE_270:
        	 mmi_frm_screen_rotate(MMI_FRM_SCREEN_ROTATE_270);
			 break;
		default:
			return E_ZM_AEE_BADPARAM;
	}
	
	return E_ZM_AEE_SUCCESS;
#else
	return E_ZM_AEE_CLASSNOTSUPPORT;
#endif
}


#endif	// __ZMAEE_APP__
