#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0) && (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#include "customer_email_num.h"
#include "PhoneBookCore.h"
#include "PhoneBookProt.h"
#endif
#include "PhoneBookGprot.h"

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
extern U16 PhoneBookEntryCount;
extern U16 g_phb_name_index[MAX_PB_ENTRIES];
extern MMI_PHB_ENTRY_BCD_STRUCT PhoneBook[MAX_PB_ENTRIES];
#endif

zm_extern int 		ZMAEE_Ucs2_2_Utf8(const unsigned short* wcsSrc, int nwcsLen, char* pDst, int nSize);
zm_extern void* 	zmaee_memset(void * dest,char c,unsigned int count);


static int ZMAEE_IAddrBook_IsReady(void);
static int ZMAEE_IAddrBook_GetNumber(char *num, int num_len, int store_index);
static int ZMAEE_IAddrBook_GetFilterType(const char *pszFilter);
static int ZMAEE_IAddrBook_EnumCompare(AEE_IAddrBook *po, const ZMAEE_ADDRREC *pRec);


#define ZMAEE_IADDRBOOK_FUNC_COUNT		6
static int ZMAEE_IAddrBook_AddRef(AEE_IAddrBook* po);
static int ZMAEE_IAddrBook_Release(AEE_IAddrBook* po);
static int ZMAEE_IAddrBook_GetNumRecs(AEE_IAddrBook* po);
static int ZMAEE_IAddrBook_GetAddrRecByIdx(AEE_IAddrBook* po, int idx, ZMAEE_ADDRREC* pRec);
static int ZMAEE_IAddrBook_EnumAddrRecInit(AEE_IAddrBook* po, const char* pszFilter);
static int ZMAEE_IAddrBook_EnumAddrRecNext(AEE_IAddrBook* po, ZMAEE_ADDRREC* pRec);
#ifdef WIN32
extern S32 srv_phb_sse_sort_index_to_store_index(U8 storage, U16 sort_index);//WLJ
#endif
typedef struct {
	void 		*pVtbl;
	int			nRef;

	int			enum_type;		// 0 - full enum, 1 - name, 2 - num & name
	char		enum_filter[AEE_MAX_FILE_NAME];	// utf8
	int			enum_index;

	int			is_ready;
#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
	
#else
	
#endif
}ZMAEE_ADDRBOOK;

static ZMAEE_ADDRBOOK sg_zmaee_addrbook = {0};

int ZMAEE_IAddrBook_New(ZM_AEECLSID clsId,void **pObj)
{
	ZMAEE_ADDRBOOK *pThis = 0;

	if(!pObj) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis = &sg_zmaee_addrbook;
	
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IADDRBOOK_FUNC_COUNT,		
											ZMAEE_IAddrBook_AddRef,
											ZMAEE_IAddrBook_Release,
											ZMAEE_IAddrBook_GetNumRecs,
											ZMAEE_IAddrBook_GetAddrRecByIdx,
											ZMAEE_IAddrBook_EnumAddrRecInit,
											ZMAEE_IAddrBook_EnumAddrRecNext);
		}
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;	
}

static
int ZMAEE_IAddrBook_AddRef(AEE_IAddrBook* po)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef++;

	return E_ZM_AEE_SUCCESS;
}

static
int ZMAEE_IAddrBook_Release(AEE_IAddrBook* po)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	pThis->nRef--;
	if(pThis->nRef == 0) {
		
	}

	return E_ZM_AEE_SUCCESS;
}

#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)

/**
 * 获取手机上联系人的个数
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 返回:
 * 	E_ZM_AEE_FAILURE		无效参数
 * 	>= 0						联系人个数
 */
static
int ZMAEE_IAddrBook_GetNumRecs(AEE_IAddrBook *po)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if(pThis == NULL) {
		return 0;
	}

	if(ZMAEE_IAddrBook_IsReady() != 1) {
		return 0;
	}

	return (int)PhoneBookEntryCount;
}

/**
 * 通过索引值获取联系人
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 	idx						索引值，从0开始
 * 	pRec					输出参数，联系人信息
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	E_ZM_AEE_SUCCESS		获取成功
 */
static
int ZMAEE_IAddrBook_GetAddrRecByIdx(AEE_IAddrBook* po, int idx, ZMAEE_ADDRREC* pRec)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;
	int store_index;
	
	if((pThis == NULL) || (pRec == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(ZMAEE_IAddrBook_IsReady() != 1) {
		return E_ZM_AEE_FAILURE;
	}

	if((idx < 0) || (idx >= PhoneBookEntryCount)) {
		return E_ZM_AEE_FAILURE;
	}

	store_index = g_phb_name_index[idx];
	if(ZMAEE_IAddrBook_GetNumber(pRec->szPhone, sizeof(pRec->szPhone), store_index) != 1) {
		return E_ZM_AEE_FAILURE;
	}

	if(PhoneBook[store_index].alpha_id.name_dcs == SMSAL_DEFAULT_DCS) {
		ZMAEE_Ucs2_2_Utf8((const unsigned short*)PhoneBook[store_index].alpha_id.name, PhoneBook[store_index].alpha_id.name_length,
						(char*)pRec->szName, sizeof(pRec->szName));
	} else if(PhoneBook[store_index].alpha_id.name_dcs == SMSAL_UCS2_DCS) {
		ZMAEE_Ucs2_2_Utf8((const unsigned short*)PhoneBook[store_index].alpha_id.name, PhoneBook[store_index].alpha_id.name_length / 2,
						(char*)pRec->szName, sizeof(pRec->szName));
	} else {
		strncpy((char*)pRec->szName, (const char*)PhoneBook[store_index].alpha_id.name, sizeof(pRec->szName));
	}

	if(store_index > MAX_PB_PHONE_ENTRIES) {
		pRec->nStorage = ZMAEE_ADDRREC_SIM;
	} else {
		pRec->nStorage = ZMAEE_ADDRREC_DEVICE;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 联系人模块是否初始化完成
 * 参数:
 * 	void
 * 返回:
 * 	0						未完成初始化
 * 	1						完成初始化
 */
static
int ZMAEE_IAddrBook_IsReady(void)
{
	if(sg_zmaee_addrbook.is_ready == 1)
		return 1;

#ifdef __MMI_DUAL_SIM_MASTER__
	if(MTPNP_PFAL_Phb_IsReady() == MTPNP_FALSE)
#else   /* __MMI_DUAL_SIM_MASTER__ */
	if(!g_phb_cntx.phb_ready || g_phb_cntx.processing)
#endif  /* __MMI_DUAL_SIM_MASTER__ */
	{
		sg_zmaee_addrbook.is_ready = 0;
	} else {
		sg_zmaee_addrbook.is_ready = 1;
	}

	return sg_zmaee_addrbook.is_ready;
}

/**
 * 根据存储索引获取电话号码
 * 参数:
 * 	num						输出参数，手机号码
 * 	num_len					手机号码buf的字节长度
 * 	store_index				存储索引
 * 返回:
 * 	0						获取成功
 * 	1						获取失败
 */
static
int ZMAEE_IAddrBook_GetNumber(char *num, int num_len, int store_index)
{
    if(((PhoneBook[store_index].tel.type & 0x90) == 0x90) && (PhoneBook[store_index].tel.type != 0xFF)) {
		// 带国际区号的号码
        num[0] = '+';
        #if (ZM_AEE_MTK_SOFTVERN < 0x08A0)
			mmi_phb_convert_to_digit((U8*)(num + 1), PhoneBook[store_index].tel.number, num_len - 1);
		#else
			#ifdef __MMI_DUAL_SIM_MASTER__
				if(store_index >= (MAX_PB_PHONE_ENTRIES+MAX_PB_SIM_ENTRIES))
					mmi_phb_convert_to_digit_by_storage((U8*)(num + 1), PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM2);
				else if (store_index >= MAX_PB_PHONE_ENTRIES && store_index < (MAX_PB_PHONE_ENTRIES+MAX_PB_SIM_ENTRIES))
					mmi_phb_convert_to_digit_by_storage((U8*)(num + 1), PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM);
				else
					mmi_phb_convert_to_digit_by_storage((U8*)(num + 1), PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_NVRAM);
			#else
				if(store_index >= MAX_PB_PHONE_ENTRIES)	/*In SIM*/
					mmi_phb_convert_to_digit_by_storage((U8*)(num + 1), PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM);
				else
					mmi_phb_convert_to_digit_by_storage((U8*)(num + 1), PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_NVRAM);
			#endif
		#endif	
    }else {
    	// 不带区号的电话号码
        #if (ZM_AEE_MTK_SOFTVERN < 0x08A0)
			mmi_phb_convert_to_digit((U8*)num, PhoneBook[store_index].tel.number, num_len - 1);
		#else
			#ifdef __MMI_DUAL_SIM_MASTER__
				if(store_index >= (MAX_PB_PHONE_ENTRIES+MAX_PB_SIM_ENTRIES))
					mmi_phb_convert_to_digit_by_storage((U8*)num, PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM2);
				else if (store_index >= MAX_PB_PHONE_ENTRIES && store_index < (MAX_PB_PHONE_ENTRIES+MAX_PB_SIM_ENTRIES))
					mmi_phb_convert_to_digit_by_storage((U8*)num, PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM);
				else
					mmi_phb_convert_to_digit_by_storage((U8*)num, PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_NVRAM);
			#else
				if(store_index >= MAX_PB_PHONE_ENTRIES)	/*In SIM*/
					mmi_phb_convert_to_digit_by_storage((U8*)num, PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_SIM);
				else
					mmi_phb_convert_to_digit_by_storage((U8*)num, PhoneBook[store_index].tel.number, 
						num_len - 1, MMI_NVRAM);
			#endif
		#endif	
    }

    return 1;	
}


#else 	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

/**
 * 获取手机上联系人的个数
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 返回:
 * 	E_ZM_AEE_FAILURE		无效参数
 * 	>= 0						联系人个数
 */
static
int ZMAEE_IAddrBook_GetNumRecs(AEE_IAddrBook *po)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)

	srv_phb_filter_struct *filter;
	S32 contact_count;
	U16 contact_array[MMI_PHB_ENTRIES] = {0};
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if(pThis == NULL) {
		return 0;
	}

	if(ZMAEE_IAddrBook_IsReady() != 1) {
		return 0;
	}
	
	filter = mmi_phb_list_build_filter(
					PHB_STORAGE_MAX, 
					MMI_PHB_LIST_DFAULT_FILTER,
					NULL,
					NULL,
					0); 
	contact_count = srv_phb_oplib_build_contact_list(
				filter, contact_array, MMI_PHB_ENTRIES);
	mmi_phb_list_free_filter(filter);

	return (int)contact_count;
#else
	return srv_phb_get_used_contact(PHB_STORAGE_MAX);
#endif

}


/**
 * 通过索引值获取联系人
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 	idx						索引值，从0开始
 * 	pRec					输出参数，联系人信息
 * 返回:
 * 	E_ZM_AEE_BADPARAM		参数无效
 * 	E_ZM_AEE_FAILURE		获取失败
 * 	E_ZM_AEE_SUCCESS		获取成功
 */
static
int ZMAEE_IAddrBook_GetAddrRecByIdx(AEE_IAddrBook* po, int idx, ZMAEE_ADDRREC* pRec)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
	srv_phb_filter_struct *filter;
	U16 contact_array[MMI_PHB_ENTRIES] = {0};
	S32 contact_count;
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;
	U16 temp_name[AEE_MAX_NUM_SIZE] = {0};
	U16 temp_num[AEE_MAX_NUM_SIZE] = {0};
	U8 storage_ret;

	if((pThis == NULL) || (pRec == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	filter = mmi_phb_list_build_filter(
				PHB_STORAGE_MAX,
				MMI_PHB_LIST_DFAULT_FILTER,
				NULL,
				NULL, 
				0);
	contact_count = srv_phb_oplib_build_contact_list(
				filter, contact_array, MMI_PHB_ENTRIES);	
	mmi_phb_list_free_filter(filter);

	if(idx < 0 || idx >= contact_count) {
		return E_ZM_AEE_BADPARAM;
	}
	
	srv_phb_get_name(contact_array[idx], (U16*)temp_name, (U16)sizeof(temp_name) / sizeof(temp_name[0]));
	srv_phb_get_number(contact_array[idx], (U16*)temp_num, (U16)sizeof(temp_num) / sizeof(temp_num[0]));

	ZMAEE_Ucs2_2_Utf8((const unsigned short*)temp_name, sizeof(temp_name) / 2,
					 (char*)pRec->szName, sizeof(pRec->szName));
	ZMAEE_Ucs2_2_Utf8((const unsigned short*)temp_num, sizeof(temp_num) / 2,
					 (char*)pRec->szPhone, sizeof(pRec->szPhone));

	storage_ret = srv_phb_get_storage(contact_array[idx]);
	switch(storage_ret) {
		case PHB_STORAGE_NVRAM:
			pRec->nStorage = ZMAEE_ADDRREC_DEVICE;
			break;
		default:
			pRec->nStorage = ZMAEE_ADDRREC_SIM;
	}

	return E_ZM_AEE_SUCCESS;
#else
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;
	int store_index;	
	U16 temp_name[AEE_MAX_NUM_SIZE] = {0};
	U16 temp_num[AEE_MAX_NUM_SIZE] = {0};
	U8 storage_ret;

	if(!pThis || !pRec)
		return E_ZM_AEE_BADPARAM;

	store_index = srv_phb_sse_sort_index_to_store_index(PHB_STORAGE_MAX, idx);
	if(store_index < 0) {
		return E_ZM_AEE_BADPARAM;
	}

	srv_phb_get_name(store_index, (U16*)temp_name, (U16)sizeof(temp_name) / sizeof(temp_name[0]));
	srv_phb_get_number(store_index, (U16*)temp_num, (U16)sizeof(temp_num) / sizeof(temp_num[0]));

	ZMAEE_Ucs2_2_Utf8((const unsigned short*)temp_name, sizeof(temp_name) / 2,
					 (char*)pRec->szName, sizeof(pRec->szName));
	ZMAEE_Ucs2_2_Utf8((const unsigned short*)temp_num, sizeof(temp_num) / 2,
					 (char*)pRec->szPhone, sizeof(pRec->szPhone));

	storage_ret = srv_phb_get_storage(store_index);
	switch(storage_ret) {
		case PHB_STORAGE_NVRAM:
			pRec->nStorage = ZMAEE_ADDRREC_DEVICE;
			break;
		default:
			pRec->nStorage = ZMAEE_ADDRREC_SIM;
	}
}
	return E_ZM_AEE_SUCCESS;
#endif
}

/**
 * 联系人模块是否初始化完成
 * 参数:
 * 	void
 * 返回:
 * 	0						未完成初始化
 * 	1						完成初始化
 */
static
int ZMAEE_IAddrBook_IsReady(void)
{
#if (ZM_AEE_MTK_SOFTVERN < 0x11B0)
	if(sg_zmaee_addrbook.is_ready == 1)
		return 1;

	if(mmi_phb_check_ready(MMI_FALSE) == MMI_TRUE) {
		sg_zmaee_addrbook.is_ready = 1;
	} else {
		sg_zmaee_addrbook.is_ready = 0;	
	}

	return sg_zmaee_addrbook.is_ready;
#else
	return 1;
#endif


}

#endif	// (ZM_AEE_MTK_SOFTVERN < 0x09B0)

/**
 * 初始化电话本搜索
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 	pszFilter					搜索条件，可以为名字，也可以为号码，支持模糊搜索，如果为NULL，则为全遍历
 * 返回:
 * 	E_ZM_AEE_BADPARAM		无效参数
 * 	E_ZM_AEE_FAILURE		初始化失败
 * 	E_ZM_AEE_SUCCESS		初始化成功
 */
static
int ZMAEE_IAddrBook_EnumAddrRecInit(AEE_IAddrBook* po, const char* pszFilter)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if(pThis == NULL) {
		return E_ZM_AEE_BADPARAM;
	}

	if(ZMAEE_IAddrBook_IsReady() != 1) {
		return E_ZM_AEE_FAILURE;
	}

	pThis->enum_type = ZMAEE_IAddrBook_GetFilterType(pszFilter);
	pThis->enum_index = 0;
	zmaee_memset(pThis->enum_filter, 0, sizeof(pThis->enum_filter));;
	strncpy((char*)pThis->enum_filter, (const char*)pszFilter, sizeof(pThis->enum_filter));
	
	return E_ZM_AEE_SUCCESS;
}

/**
 * 搜索电话本
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 	pRec					输出参数，联系人信息
 * 返回:
 * 	E_ZM_AEE_FAILURE		搜索失败
 * 	E_ZM_AEE_SUCCESS		搜索成功
 * 	E_ZM_AEE_BADPARAM		无效参数
 */
static
int ZMAEE_IAddrBook_EnumAddrRecNext(AEE_IAddrBook* po, ZMAEE_ADDRREC* pRec)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;
	int i, count;

	if((pThis == NULL) || (pRec == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(ZMAEE_IAddrBook_IsReady() != 1) {
		return E_ZM_AEE_FAILURE;
	}

	count = ZMAEE_IAddrBook_GetNumRecs(po);
	for(i = pThis->enum_index; i < count; i++) {		
		if(ZMAEE_IAddrBook_GetAddrRecByIdx(po, i, pRec) != E_ZM_AEE_SUCCESS) {
			return E_ZM_AEE_FAILURE;
		}

		if(ZMAEE_IAddrBook_EnumCompare(po, pRec) != E_ZM_AEE_SUCCESS) {
			memset(pRec, 0, sizeof(ZMAEE_ADDRREC));
		} else {
			pThis->enum_index = i + 1;
			return E_ZM_AEE_SUCCESS;
		}
	}

	return E_ZM_AEE_FAILURE;
}

/**
 * 获取搜索条件的类型
 * 参数:
 * 	pszFilter					搜索关键字
 * 返回:
 * 	0						全遍历
 * 	1						姓名遍历
 * 	2						姓名和号码遍历
 */
static
int ZMAEE_IAddrBook_GetFilterType(const char *pszFilter)
{
	int filter_size;
	int i;

	if(pszFilter == NULL)
		return 0;

	filter_size = strlen(pszFilter);
	for(i = 0; i < filter_size; i++) {
		if(((pszFilter[i] < '0') || (pszFilter[i] > '9')) && (pszFilter[i] != '+') && (pszFilter[i] != '*') && (pszFilter[i] != '#')) {
			return 1;
		}
	}

	return 2;
}

/**
 * 判断是否符合搜索条件
 * 参数:
 * 	po						AEE_IAddrBook实例
 * 	pRec					ADDRREC结构体数据
 * 返回:
 * 	E_ZM_AEE_SUCCESS		符合搜索条件
 * 	!= E_ZM_AEE_SUCCESS	不符合搜索条件
 */
static
int ZMAEE_IAddrBook_EnumCompare(AEE_IAddrBook *po, const ZMAEE_ADDRREC *pRec)
{
	ZMAEE_ADDRBOOK *pThis = (ZMAEE_ADDRBOOK*)po;

	if((pThis == NULL) || (pRec == NULL)) {
		return E_ZM_AEE_BADPARAM;
	}

	if(pThis->enum_type == 0) {
		return E_ZM_AEE_SUCCESS;
	} else if(pThis->enum_type == 1) {	
		// 只搜索姓名
		if(strstr((const char*)pRec->szName, (const char*)pThis->enum_filter) != NULL) {
			return E_ZM_AEE_SUCCESS;
		} else {
			return E_ZM_AEE_FAILURE;
		}
	} else {
		// 搜索姓名和号码
		if(strstr((const char*)pRec->szPhone, (const char*)pThis->enum_filter) != NULL) {
			return E_ZM_AEE_SUCCESS;
		} else if(strstr((const char*)pRec->szName, (const char*)pThis->enum_filter) != NULL) {
			return E_ZM_AEE_SUCCESS;
		} else {
			return E_ZM_AEE_FAILURE;
		}
	}

	return E_ZM_AEE_FAILURE;
}

#endif	// __ZMAEE_APP__
