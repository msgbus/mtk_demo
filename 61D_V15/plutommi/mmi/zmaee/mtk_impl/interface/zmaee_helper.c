#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"

#include "sim_common_enums.h"

#include "stack_common.h"
#include "stack_msgs.h"
#include "app_ltlcom.h"
#include "nvram_enums.h"

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_nvram_gprot.h"
#include "MMITimer_Int.h"
#include "ImeGprot.h"
#include "GeneralDeviceGprot.h"
#else
#include "NVRAMType.h"
#include "NVRAMProt.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x0852)
#include "wgui_categories_inputsenum.h"
#endif
#include "nvram_common_defs.h"

#include "TimerEvents.h"

#include "app_mem.h"

#ifndef ZMAEE_MD5_INLIB
#include "app_md5.h"
#endif

#include "gui_config.h"
#include "IdleAppResDef.h"
#include "GlobalConstants.h"

#include "globaldefs.h"

#include <stdio.h>
#include "gpioInc.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "wgui.h"
#include "wgui_categories_inputs.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x11A0)
#include "med_utility.h"
#endif

#if(ZM_AEE_MTK_SOFTVERN >= 0x11B0)
#include "custom_led_patterns.h"
#endif
#ifdef WIN32
#include "gpiosrvgprot.h"//WLJ
#endif

#if defined (MT6250) && (ZM_AEE_MTK_SOFTVERN >= 0x1220)
#include "mmi_rp_app_uiframework_def.h"
#if defined (__COSMOS_MMI_PACKAGE__)
#include "mmi_rp_vapp_launcher_mm_def.h"
#endif
#endif

#if 1//defined (MT6260) && (ZM_AEE_MTK_SOFTVERN >= 0x1220)
#include "mmi_rp_app_uiframework_def.h"
#endif

#ifndef __MMI_FRM_HISTORY__
extern mmi_ret zmaee_group_proc(mmi_event_struct * evt);
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
extern void ZMAEE_ShowCategory_InputText(
        U16 *title_str,
        U16 title_icon,
        U16 left_softkey,
        U16 left_softkey_icon,
        U16 right_softkey,
        U16 right_softkey_icon,
        U32 input_type,
        U8 *buffer,
        S32 buffer_size,
        U8 *history_buffer,
        S16 input_ext_type,
        mmi_imm_input_mode_enum *required_input_mode_set);
#else
extern void ZMAEE_ShowCategory_InputText(
        U16 *title_str,
        U16 title_icon,
        U16 left_softkey,
        U16 left_softkey_icon,
        U16 right_softkey,
        U16 right_softkey_icon,
        S16 input_type,
        U8 *buffer,
        S32 buffer_size,
        U8 *history_buffer,
        S16 input_ext_type,
        S16 *required_input_mode_set,
        U8 *no_show_symbol_list[]);
#endif


extern U8 *GetCurrGuiBuffer(U16 scrnid);
#if (ZM_AEE_MTK_SOFTVERN < 0x0852)
extern void* mmi_frm_scrmem_alloc(U32 mem_size);
extern void mmi_frm_scrmem_free(void * mem_ptr);
#endif
extern void StartTimer(U16 timerid,U32 delay,FuncPtr funcPtr);
extern void StopTimer(U16 timerid);
extern U8 EntryNewScreen(U16 newscrnID,FuncPtr newExitHandler,FuncPtr newEntryHandler,void * flag);
extern void SetKeyHandler(FuncPtr funcPtr,U16 keyCode,U16 keyType);
extern void SetLeftSoftkeyFunction(void(* f)(void),MMI_key_event_type k);
extern void SetCategory5RightSoftkeyFunction(void(* f)(void),MMI_key_event_type k);


zm_extern ZMAEE_PERIOD_ITEM* 			ZMAEE_Period_GetFreeItem(void);
zm_extern ZMAEE_PERIOD_ITEM* 			ZMAEE_Period_GetItem(int handler);
zm_extern void 							ZMAEE_Period_PopHead(void);
zm_extern void 							ZMAEE_Period_PushTail(ZMAEE_PERIOD_ITEM *period_item);
zm_extern void 							ZMAEE_Period_Timer_Callback(void);
zm_extern int 							ZMAEE_GetActiveSimCard(void);
zm_extern int 							ZMAEE_Period_Queue_Count(void);
zm_extern void 							ZMAEE_Period_UpdateQueue(int nHandle);
zm_extern unsigned int 					zmaee_wcslen(const unsigned short * str);
zm_extern void 							ZMAEE_DebugPrint(const char *pszFormat, ...);



typedef struct {
	LOCAL_PARA_HDR
	int			handle;
}ZMAEE_PERIOD_LPARAM;

static int sg_zmaee_debug_switch = 0;

/**
  * Sleep System @ms 毫秒
  */
void ZMAEE_Sleep(unsigned long ms)
{
	kal_uint32 time_in_ticks;

	time_in_ticks = kal_milli_secs_to_ticks((kal_uint32)ms);
	kal_sleep_task((kal_uint32)time_in_ticks);
}

/**
  * 设置用户UserId
  */
void ZMAEE_SetUserID(unsigned long nUserId)
{
	ZMAEE_NVRAMINFO nvram_info = {0};
	S16 err_code;
	int ret;

	ret = ReadRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
	if((ret != sizeof(nvram_info)) || (err_code != NVRAM_READ_SUCCESS)) {
		nvram_info.sim_idx = ZMAEE_GetActiveSimCard();
	}

	nvram_info.user_id = (int)nUserId;

	ret = WriteRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
	if((ret != sizeof(nvram_info)) || (err_code != NVRAM_WRITE_SUCCESS)) {
		WriteRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
	}
}

/**
  * MD5
  */
void ZMAEE_Md5(const char* src, int len , unsigned char* pOut)
{
#ifndef ZMAEE_MD5_INLIB
	applib_md5_ctx md5_cntx = {0};
	
	applib_md5_init(&md5_cntx);
	applib_md5_update(&md5_cntx, (const kal_uint8*)src, (kal_uint32)len);
	applib_md5_final((kal_uint8*)pOut, &md5_cntx);
#else	// #ifndef ZMAEE_MD5_INLIB
	extern void ZMAEE_Md5_Inlib(const char* src, int len, unsigned char* pOut);
	ZMAEE_Md5_Inlib(src, len, pOut);
#endif	// #ifndef ZMAEE_MD5_INLIB
}

/**
  * DES
  */
int ZMAEE_Des(const unsigned char* src, int len, const unsigned char* key, int keyLen, unsigned char* pOut, int type)
{
#ifndef ZMAEE_DES_INLIB

#else
	extern int ZMAEE_Des_Inlib(const char* src, int len, const char* key, int keyLen, unsigned char* pOut, int type);
	return ZMAEE_Des_Inlib((char*)src, len, (char*)key, keyLen, pOut, type);
#endif
}

/**
  * 创建一个周期性自循环, pfn 返回 1： 需要下个周期，  0: 结束.
  return:
	-1 失败.
	>=0 [ handle ]成功.
  */
int ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn)
{
	ilm_struct *ilm_ptr;
	ZMAEE_PERIOD_LPARAM *period_lparam;
	ZMAEE_PERIOD_ITEM *period_item;

	period_lparam = (ZMAEE_PERIOD_LPARAM*)construct_local_para(sizeof(ZMAEE_PERIOD_LPARAM), TD_CTRL);
	if(period_lparam == NULL)
		return -1;

	ilm_ptr = allocate_ilm(MOD_MMI);
	if(ilm_ptr == NULL) {
		free_local_para((void*)period_lparam);
		return -1;
	}

	period_item = ZMAEE_Period_GetFreeItem();
	if(!period_item) {
		free_ilm(ilm_ptr);
		free_local_para((void*)period_lparam);
		return -1;
	}
	
	period_item->is_valid = 1;
	period_item->cls_id = clsId;
	ZMAEE_Callback_Init(&(period_item->callback), (void*)pfn, pUser);

	period_lparam->handle = period_item->period_id;

	ilm_ptr->src_mod_id = MOD_MMI;
	ilm_ptr->dest_mod_id = MOD_MMI;
	ilm_ptr->sap_id = MMI_MMI_SAP;
	ilm_ptr->msg_id = MSG_ID_ZMAEE_PERIOD;
	ilm_ptr->local_para_ptr = (void*)period_lparam;
	ilm_ptr->peer_buff_ptr = NULL;
	
	if(msg_send_ext_queue(ilm_ptr) != KAL_TRUE) {
		free_ilm(ilm_ptr);
		free_local_para((void*)period_lparam);
		return -1;
	}

	return period_item->period_id;
}

void ZMAEE_StopPeriodHandler(ZM_AEECLSID clsId, int nHandle)
{
	ZMAEE_PERIOD_ITEM *period_item;

	period_item = ZMAEE_Period_GetItem(nHandle);
	if(period_item) {
		period_item->is_valid = 0;

#ifdef ZMAEE_PERIOD_USE_TIMER
		ZMAEE_Period_UpdateQueue(nHandle);
#endif
	}
}

void ZMAEE_RestartPeriodHandler(int nHandle)
{
	ilm_struct *ilm_ptr;
	ZMAEE_PERIOD_LPARAM *period_lparam;
	ZMAEE_PERIOD_ITEM *period_item;

	period_lparam = (ZMAEE_PERIOD_LPARAM*)construct_local_para(sizeof(ZMAEE_PERIOD_LPARAM), TD_CTRL);
	if(period_lparam == NULL)
		return;

	ilm_ptr = allocate_ilm(MOD_MMI);
	if(ilm_ptr == NULL) {
		free_local_para((void*)period_lparam);
		return;
	}

	period_item = ZMAEE_Period_GetItem(nHandle);
	if(!period_item) {
		free_ilm(ilm_ptr);
		free_local_para((void*)period_lparam);
		return;
	}
	
	period_lparam->handle = period_item->period_id;

	ilm_ptr->src_mod_id = MOD_MMI;
	ilm_ptr->dest_mod_id = MOD_MMI;
	ilm_ptr->sap_id = 0;
	ilm_ptr->msg_id = MSG_ID_ZMAEE_PERIOD;
	ilm_ptr->local_para_ptr = (void*)period_lparam;
	ilm_ptr->peer_buff_ptr = NULL;
	
	if(msg_send_ext_queue(ilm_ptr) != KAL_TRUE) {
		free_ilm(ilm_ptr);
		free_local_para((void*)period_lparam);
		return;
	}
}


/**
 * return 0 is not handled, return 1 is handled
 */
int ZMAEE_ExecutePeriodHandler(void *LPARAM, void *PPARAM)
{
	ZMAEE_PERIOD_LPARAM *period_lparam = (ZMAEE_PERIOD_LPARAM*)LPARAM;
	int ret;
	ZMAEE_PERIOD_ITEM *period_item = {0}; 

#ifdef ZMAEE_PERIOD_USE_TIMER
	ZMAEE_Period_PopHead();
#endif

	if(period_lparam == NULL)
		goto exec_period_end;

	period_item = ZMAEE_Period_GetItem(period_lparam->handle);
	if(!period_item) {
		goto exec_period_end;
	}

	if(ZMAEE_Callback_Valid(&(period_item->callback)) != 1) {
		goto exec_period_end;
	}

	if((period_item->is_valid) && (period_item->callback.pfn)) {
		ZMAEE_PFNPERIODCB callback = (ZMAEE_PFNPERIODCB)period_item->callback.pfn;

		period_item->is_valid = 0;
		ret = callback(period_lparam->handle, period_item->callback.pUser);
	
		if(ret == 1) {
			period_item->is_valid = 1;
			
		#ifdef ZMAEE_PERIOD_USE_TIMER
			ZMAEE_Period_PushTail(period_item);
		#else
			ZMAEE_RestartPeriodHandler(period_lparam->handle);
		#endif			
		}
	}

exec_period_end:

#ifdef ZMAEE_PERIOD_USE_TIMER
	if(ZMAEE_Period_Queue_Count() > 0) {
		StopTimer(ZMAEE_PERIOD_TIMER);
		StartTimer(ZMAEE_PERIOD_TIMER, ZMAEE_PERIOD_TIME_SLICE, ZMAEE_Period_Timer_Callback);
	}
#endif

	return 1;
}

/**
  * 获取ScreenMem, SUSPEND->zmaee_FreeScreenMem->RESUME->zmaee_MallocScreenMem.
  */
void* ZMAEE_MallocScreenMem(int size)
{
	void *ptr;

#if 1//((ZM_AEE_MTK_SOFTVERN < 0x11A0) || !defined (__COSMOS_MMI_PACKAGE__)) && !defined (MT6250)
	//if(applib_mem_screen_get_max_alloc_size() < size) {
	//	return NULL;
	//}
	
	ptr = (void*)mmi_frm_scrmem_alloc_framebuffer(size);
	return ptr;
#else
#ifdef __MED_IN_ASM__
	if(applib_mem_ap_get_max_alloc_size() < size)
		return NULL;

	ptr = (void*)applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, size);	
#else
	if(ZMAEE_GetMaxAllocMedMem() < size )
		return NULL;
	
	ptr = (void*)med_alloc_ext_mem_framebuffer(size);
#endif
    return ptr;
#endif
}

void ZMAEE_FreeScreenMem(void* pScreenMem)
{
#if 1//((ZM_AEE_MTK_SOFTVERN < 0x11A0) || !defined (__COSMOS_MMI_PACKAGE__)) && !defined (MT6250)
	mmi_frm_scrmem_free(pScreenMem);
#else
	#ifdef __MED_IN_ASM__
		applib_mem_ap_free(pScreenMem);
	#else
		med_free_ext_mem(&pScreenMem);
	#endif
#endif
}

/**
  * 调试输出.
  */
int ZMAEE_DebugSwitch(int nOn)
{
	sg_zmaee_debug_switch = nOn;

	return E_ZM_AEE_SUCCESS;
}

void ZMAEE_DebugPrint(const char *pszFormat, ...)
{
	if(sg_zmaee_debug_switch) {
		va_list Args;
		char out_buf[AEE_MAX_DEBUG_BUF_LEN] = {0};
		int len;
		
		va_start(Args,pszFormat);
		len = vsprintf(out_buf, pszFormat, Args);
	#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
		kal_prompt_trace(MOD_AUX, "%s\n", out_buf);
	#else
		kal_prompt_trace(LAST_MOD_ID, "%s\n", out_buf);
	#endif
		va_end(Args);
	}
}

void ZMAEE_DebugPrintV(const char*pszFormat, void* args)
{
	extern int zmaee_sprintf_v(char* buf, const char* fmt, void* params);
	if(sg_zmaee_debug_switch) 
	{
		char out_buf[AEE_MAX_DEBUG_BUF_LEN] = {0};
		zmaee_sprintf_v(out_buf, pszFormat, args);
		kal_prompt_trace(MOD_AUX, "%s\n", out_buf);
	}
}


/**
 * 震动
 */
void ZMAEE_Vibrate(void)
{
#if(ZM_AEE_MTK_SOFTVERN < 0x11B0)
	PlayVibratorOnce();
#else
	srv_gpio_play_vibrate(VIB_PATTERN_1, PLAY_ONCE);
#endif
}

/********************************************************************************
 * 全屏输入框
 ********************************************************************************/
typedef struct {
	ZMAEE_CALLBACK		callback;
	short				*content;
	short				*title_str;
	int					input_type;
	int					max_len;
}ZMAEE_INPUT_CNTX;

static ZMAEE_INPUT_CNTX sg_zmaee_input_cntx = {0};

static
void ZMAEE_InputText_Cancel(void)
{
	if(sg_zmaee_input_cntx.content)
		ZMAEE_Free(sg_zmaee_input_cntx.content);
	sg_zmaee_input_cntx.content = NULL;
	memset(&(sg_zmaee_input_cntx.callback), 0, sizeof(sg_zmaee_input_cntx.callback));

#if (UI_BLINKING_CURSOR_SUPPORT)
#ifdef __MMI_DUAL_SIM_MASTER__
	StopTimer(BLINKING_CURSOR);
#else
	StopTimer(BLINKING_CURSOR);
#endif
#endif
	GoBackHistory();
}


static
void ZMAEE_InputText_OK(void)
{
	if(sg_zmaee_input_cntx.content) {
		if(ZMAEE_Callback_Valid(&(sg_zmaee_input_cntx.callback)) == 1) {
			ZMAEE_PFN_INPUTTEXT callback = (ZMAEE_PFN_INPUTTEXT)sg_zmaee_input_cntx.callback.pfn;
			if(callback) {
				callback(sg_zmaee_input_cntx.callback.pUser, sg_zmaee_input_cntx.content, 
						zmaee_wcslen((const unsigned short*)sg_zmaee_input_cntx.content));
			}
		}
	}
	
	ZMAEE_InputText_Cancel();
}

static
void ZMAEE_InputText_Entry(void)
{
	U8 *gui_buffer;	
#if (ZM_AEE_MTK_SOFTVERN >= 0x08A0)
	U32 input_type = (U32)sg_zmaee_input_cntx.input_type;
#else
	S16 input_type = (S16)sg_zmaee_input_cntx.input_type;
#endif
	
#ifdef __MMI_FRM_HISTORY__
	EntryNewScreen(SCR_ZMAEE_INPUT_WND, NULL, ZMAEE_InputText_Entry, NULL);
	gui_buffer = GetCurrGuiBuffer(SCR_ZMAEE_INPUT_WND);
#else
	mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_ZMAEE, zmaee_group_proc, NULL);
	mmi_frm_group_enter(GRP_ID_ZMAEE, MMI_FRM_NODE_SMART_CLOSE_FLAG);

	mmi_frm_scrn_enter(GRP_ID_ZMAEE, SCR_ZMAEE_INPUT_WND, NULL, ZMAEE_InputText_Entry, MMI_FRM_FULL_SCRN);
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
	ZMAEE_ShowCategory_InputText((U16*)sg_zmaee_input_cntx.title_str, 0,
								STR_GLOBAL_OK, IMG_GLOBAL_OK,
								STR_GLOBAL_BACK,IMG_GLOBAL_BACK,
								input_type,
								(U8*)sg_zmaee_input_cntx.content, sg_zmaee_input_cntx.max_len, /* 预留2个字节做结束符 */
								gui_buffer, 0, NULL);
#else
	ZMAEE_ShowCategory_InputText((U16*)sg_zmaee_input_cntx.title_str, 0,
								STR_GLOBAL_OK, IMG_GLOBAL_OK,
								STR_GLOBAL_BACK,IMG_GLOBAL_BACK,
								input_type,
								(U8*)sg_zmaee_input_cntx.content, sg_zmaee_input_cntx.max_len, /* 预留2个字节做结束符 */
								gui_buffer, 0, NULL, NULL);
#endif

    SetKeyHandler(ZMAEE_InputText_Cancel, KEY_END, KEY_EVENT_DOWN);
	ClearKeyHandler(KEY_END, KEY_EVENT_UP);
	SetLeftSoftkeyFunction(ZMAEE_InputText_OK, KEY_EVENT_UP);
	SetCategory5RightSoftkeyFunction(ZMAEE_InputText_Cancel, KEY_EVENT_UP);	
}

/**
 * 全屏输入框
 * 参数:
 * 	text						初始的文本内容，Unicode编码
 * 	text_len					初始文本内容的字符数
 * 	max_len					输入框的最大字符数
 * 	title_str					全屏输入框标题，Unicode编码，必须到callback被调用时才能被释放
 * 	type						输入类型
 *	callback					输入完成的回调函数
 * 	pUser					回调函数的用户数据
 * 返回:
 * 	void
 */
void ZMAEE_InputText(const short *text, int text_len, int max_len, const short *title_str, 
							ZMAEE_INPUT_TYPE type, ZMAEE_PFN_INPUTTEXT callback, void *pUser)
{	
	if(!title_str || !callback)	return;

	sg_zmaee_input_cntx.content = (short*)ZMAEE_Malloc(max_len * 2);
	if(!sg_zmaee_input_cntx.content)
		return;
	memset(sg_zmaee_input_cntx.content, 0, max_len * 2);

	if(text) {
		if(text_len >= max_len)
			text_len = max_len - 1;
		if(text_len > 0)
			memcpy(sg_zmaee_input_cntx.content, text, text_len * 2);		
	}

#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
	switch(type) {
		case ZMAEE_INPUT_TYPE_123:
			sg_zmaee_input_cntx.input_type = IMM_INPUT_TYPE_NUMERIC;
			break;
		case ZMAEE_INPUT_TYPE_ABC:
			sg_zmaee_input_cntx.input_type = (IMM_INPUT_TYPE_ENGLISH_SENTENCE | INPUT_TYPE_USE_ONLY_ENGLISH_MODES);
			break;
		case ZMAEE_INPUT_TYPE_PINYIN:
			sg_zmaee_input_cntx.input_type = IMM_INPUT_TYPE_SENTENCE;
			break;
		case ZMAEE_INPUT_TYPE_PWD:
			sg_zmaee_input_cntx.input_type = (IMM_INPUT_TYPE_ALPHANUMERIC_PASSWORD | INPUT_TYPE_USE_ONLY_ENGLISH_MODES);
			break;
		default:
			return;
	}
#else
	switch(type) {
		case ZMAEE_INPUT_TYPE_123:
			sg_zmaee_input_cntx.input_type = INPUT_TYPE_NUMERIC;
			break;
		case ZMAEE_INPUT_TYPE_ABC:
			sg_zmaee_input_cntx.input_type = INPUT_TYPE_ALPHANUMERIC_LOWERCASE;
			break;
		case ZMAEE_INPUT_TYPE_PINYIN:
			sg_zmaee_input_cntx.input_type = INPUT_TYPE_SM_PINYIN;
			break;
		case ZMAEE_INPUT_TYPE_PWD:
			sg_zmaee_input_cntx.input_type = INPUT_TYPE_ALPHANUMERIC_PASSWORD;
			break;
		default:
			return;
	}
#endif

	sg_zmaee_input_cntx.max_len = max_len;
	sg_zmaee_input_cntx.title_str = (short*)title_str;
	ZMAEE_Callback_Init(&(sg_zmaee_input_cntx.callback), (void*)callback, pUser);
	
	ZMAEE_InputText_Entry();
}

/********************************************************************************
 * 异步按键
 ********************************************************************************/
unsigned long sg_zmaee_async_key_state = 0;

/**
 * 获取异步按键状态
 * 参数:
 * 	void
 * 返回:
 * 	unsigned long			按键掩码
 */
unsigned long ZMAEE_GetAsyncKeyState(void)
{
	return sg_zmaee_async_key_state;
}

/********************************************************************************
 * 带Timer ID的定时器接口，为适配老接口
 ********************************************************************************/
#define ZMAEE_ETIMER_COUNT		4
static ZMAEE_CALLBACK sg_zmaee_etimer_callback[ZMAEE_ETIMER_COUNT] = {0};

#define ZMAEE_ETimer_Exec(idx)																	\
	do {																						\
		ZMAEE_TIMER_PFN callback = (ZMAEE_TIMER_PFN)sg_zmaee_etimer_callback[idx].pfn;			\
		void *pUser = sg_zmaee_etimer_callback[idx].pUser;										\
		int ret;																				\
																								\
		ret = ZMAEE_Callback_Valid(&sg_zmaee_etimer_callback[idx]);								\
		memset(&sg_zmaee_etimer_callback[idx], 0, sizeof(sg_zmaee_etimer_callback[idx]));		\
																								\
		if(ret) {																				\
			if(callback)																		\
				callback(idx, pUser);															\
		}																						\
	} while(0)										

static
int ZMAEE_ETimer_GetItem(ZMAEE_TIMER_PFN pfn)
{
	int i;

	for(i = 0; i < ZMAEE_ETIMER_COUNT; i++) {
		if(sg_zmaee_etimer_callback[i].pfn == (void*)pfn) {
			return i;
		}
	}

	for(i = 0; i < ZMAEE_ETIMER_COUNT; i++) {
		if(sg_zmaee_etimer_callback[i].pfn == NULL) {
			return i;
		}
	}

	return -1;
}

static
void ZMAEE_ETimer1_Callback(void)
{
	ZMAEE_ETimer_Exec(0);
}

static
void ZMAEE_ETimer2_Callback(void)
{
	ZMAEE_ETimer_Exec(1);
}

static
void ZMAEE_ETimer3_Callback(void)
{
	ZMAEE_ETimer_Exec(2);
}

static
void ZMAEE_ETimer4_Callback(void)
{
	ZMAEE_ETimer_Exec(3);
}

static
void* ZMAEE_ETimer_GetFunc(int idx)
{
	switch(idx) {
		case 0:
			return (void*)ZMAEE_ETimer1_Callback;
		case 1:
			return (void*)ZMAEE_ETimer2_Callback;
		case 2:
			return (void*)ZMAEE_ETimer3_Callback;
		case 3:
			return (void*)ZMAEE_ETimer4_Callback;
	}

	return NULL;
}


/**
 * 定时器，可后台运行
 * 参数:
 * 	mili_sec				定时器触发时间，单位:ms
 * 	callback				定时器回调函数
 * 返回:
 * 	void
 */
void ZMAEE_Start_Timer(unsigned long mili_sec, ZMAEE_TIMER_PFN pfn, void *pUser)
{
	int cb_idx;

	if(!pfn)
		return;

	cb_idx = ZMAEE_ETimer_GetItem(pfn);
	if(cb_idx < 0)
		return;

	ZMAEE_Callback_Init(&sg_zmaee_etimer_callback[cb_idx], (void*)pfn, pUser);

	StopTimer(ZMAEE_PLATFORM_TIMER0 + cb_idx);
	StartTimer(ZMAEE_PLATFORM_TIMER0 + cb_idx, mili_sec, (void (*)(void))ZMAEE_ETimer_GetFunc(cb_idx));
}

/**
 * 取消定时器
 * 参数:
 * 	callback				定时器回调函数
 * 返回:
 * 	void
 */
void ZMAEE_Stop_Timer(ZMAEE_TIMER_PFN pfn)
{
	int cb_idx;

	if(!pfn)
		return;

	cb_idx = ZMAEE_ETimer_GetItem(pfn);
	if(cb_idx < 0)
		return;

	StopTimer(ZMAEE_PLATFORM_TIMER0 + cb_idx);
	memset(&sg_zmaee_etimer_callback[cb_idx], 0, sizeof(sg_zmaee_etimer_callback[cb_idx]));
}


#endif	// __ZMAEE_APP__
