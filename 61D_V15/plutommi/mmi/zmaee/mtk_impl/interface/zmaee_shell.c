#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"

#include "kal_non_specific_general_types.h"
#include "nvram_enums.h"

#include "sim_common_enums.h"
#include "stack_common.h"
#include "stack_msgs.h"
#include "app_ltlcom.h"

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_nvram_gprot.h"
#include "browser_struct.h"
#else
#include "NVRAMType.h"
#include "NVRAMProt.h"
#include "wap_ps_struct.h"
#endif

#include "nvram_common_defs.h"

#include "gdi_internal.h"
#include "gui.h"
#if (ZM_AEE_MTK_SOFTVERN <0x11A0)
#include "QueueGprot.h"
#endif
#include "FontRes.h"
#include "SSCStringHandle.h"
#include "FontDcl.h"
#include "GlobalResDef.h"


extern S32 UI_device_width;
extern S32 UI_device_height;


extern void mmi_frm_sms_send_message(
        module_type MOD_SRC,
        module_type MOD_DEST,
        U8 MSG_SAP,
        const U16 MSG_ID,
        void *LOCAL,
        void *PEER);


zm_extern ZMAEE_TIMER_ITEM* 	ZMAEE_Timer_GetFreeItem(AEE_IShell *po, void *pfn);
zm_extern ZMAEE_TIMER_ITEM* 	ZMAEE_Timer_GetItemByIdx(AEE_IShell *po, int idx);
zm_extern int 					ZMAEE_Timer_GetItemByOwnerId(AEE_IShell *po, ZMAEE_TIMER_ITEM *item_arry[], int owner_id);
zm_extern const char* 			ZMAEE_IShell_GetWorkDir(AEE_IShell * po);
zm_extern const char* 			ZMAEE_IShell_GetCompanyName(AEE_IShell *po);
zm_extern unsigned long 		ZMAEE_IShell_GetSoftVersion(AEE_IShell *po);
zm_extern int 					ZMAEE_IShell_CreateInstance(AEE_IShell * po, ZM_AEECLSID clsId, void** ppobj);
zm_extern const char* 			ZMAEE_IShell_GetRootDir(AEE_IShell * po);
zm_extern char 					ZMAEE_IFileMgr_GetDriver(AEE_IFileMgr* po, int type);
zm_extern int 					ZMAEE_IFileMgr_Test(AEE_IFileMgr* po, const char* pszFile);
zm_extern int 					ZMAEE_IFileMgr_Release(AEE_IFileMgr* po);
zm_extern int 					ZMAEE_IFileMgr_GetInfo(AEE_IFileMgr* po, const char* pszFile, ZMAEE_FileInfo* pfi);
zm_extern AEE_IFile* 			ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode);
zm_extern int 					ZMAEE_IFile_Read(AEE_IFile * po, void * pDest, unsigned long nWant);
zm_extern int 					ZMAEE_IFile_Release(AEE_IFile* po);
zm_extern int 					ZMAEE_IShell_IsGlobalLibraryUseStaticMem(AEE_IShell *po);
zm_extern AEE_IShell* 			ZMAEE_GetShell(void);

int 		ZMAEE_Shell_FixLangByMCC(AEE_IShell *po, ZMAEE_LANG_TYPE *pLang);
void 		ZMAEE_Shell_GetOsVern(ZMAEEDeviceInfo *dev_info);

#ifdef WIN32
zm_extern int 					ZMAEE_StartPeriodHandler(ZM_AEECLSID clsId, void* pUser, ZMAEE_PFNPERIODCB pfn);
zm_extern void					ZMAEE_Exit_Ext(void);
zm_extern int					ZMAEE_IMessage_New(ZM_AEECLSID clsId, void **pObj);
zm_extern int					ZMAEE_IDesktop_New(ZM_AEECLSID clsId, void** pObj);
zm_extern int					ZMAEE_IUtil_New(ZM_AEECLSID clsId, void** pObj);
#endif

#define ZMAEE_CAP_TOUCHSCREEN			(1 << 4)
#define ZMAEE_CAP_GPS					(1 << 8)
#define ZMAEE_CAP_GSENSOR				(1 << 12)
#define ZMAEE_CAP_ROTATE				(1 << 16)

#define ZMAEE_CAP_KEYPAD_ONLY			(1 << 17)
#define ZMAEE_CAP_TOUCHSCREEN_ONLY		(1 << 18)

/**
 * 手机平台硬件配置
 */
//#if (ZMAEE_MODEL_NAME == 0xFF)
//#error ZMAEE_MODEL_NAME error in zmaee_priv.h
//#endif

#if (ZMAEE_KEYPAD_TYPE == 0xFF)
#error ZMAEE_KEYPAD_TYPE error in zmaee_priv.h
#endif

#if (ZMAEE_SUPPORT_ROTATE == 0xFF)
#error ZMAEE_SUPPORT_ROTATE error in zmaee_priv.h
#endif

#if (ZMAEE_SUPPORT_GPS == 0xFF)
#error ZMAEE_SUPPORT_GPS error in zmaee_priv.h
#endif

#if (ZMAEE_SUPPORT_GSENSOR == 0xFF)
#error ZMAEE_SUPPORT_GSENSOR error in zmaee_priv.h
#endif

#if (ZMAEE_SUPPORT_TOUCHSCREEN == 0xFF)
#error ZMAEE_SUPPORT_TOUCHSCREEN error in zmaee_priv.h
#endif

#if (ZMAEE_SUPPORT_SEPARE_APPANDGAME == 0xFF)
#error ZMAEE_SUPPORT_SEPARE_APPANDGAME error in zmaee_priv.h
#endif

#if (ZMAEE_FAE_BUILD_DATE == 0xFF)
#error ZMAEE_FAE_BUILD_DATE error in zmaee_priv.h
#endif

typedef struct {
	ZM_AEECLSID 	cls_id;
	int 			app_attr;
	char			param[256];
	int 			param_len;
	char 			appName[AEE_MAX_APPLET_NAME];
}ZMAEE_ENTRYAPPLET_PARAM;

int g_zmaee_fix_lang_by_mcc = 1;

/**
 * 获取设备信息
 * 参数:
 * 	po							AEE_IShell实例
 * 	pdi							输出参数，设备信息结构体指针
 * 返回:
 * 	E_ZM_AEE_FAILURE			获取失败
 * 	E_ZM_AEE_SUCCESS			获取成功
 */
int ZMAEE_IShell_GetDeviceInfo(AEE_IShell * po, ZMAEEDeviceInfo* pdi)
{
	int ret;
	short err_code;
	char *szPtr;
	ZMAEE_NVRAMINFO nvram_info = {0};

	// 参数校验
	if((po == NULL) || (pdi == NULL)) {
		return E_ZM_AEE_FAILURE;
	}

	// User ID
	ret = ReadRecord(NVRAM_EF_ZMAEE_CONFIG_LID, 1, &nvram_info, sizeof(nvram_info), &err_code);
	if((ret != sizeof(nvram_info)) || (err_code != NVRAM_READ_SUCCESS)) {
		pdi->nUserId = 0;
	} else {
		pdi->nUserId = nvram_info.user_id;
	}

	// Width, Height
	pdi->cxScreen = UI_device_width;
	pdi->cyScreen = UI_device_height;

	// Depth
	pdi->nColorDepth = 16;

	// keyboard
	pdi->bKbd = (ZMAEE_KEYPAD_TYPE & ZMAEE_HW_KEYPAD_NORMAL)? E_ZM_AEE_TRUE: E_ZM_AEE_FALSE;

	// touchscreen
#ifdef __MMI_TOUCH_SCREEN__
	pdi->bTouchScreen = E_ZM_AEE_TRUE;
#else
	pdi->bTouchScreen = E_ZM_AEE_FALSE;
#endif

	// max ram for use
	pdi->nMaxRam = ZMAEE_APPRAM_SIZE;

	// company
	{
		memset(pdi->szCompany, 0, sizeof(pdi->szCompany));
		strncpy(pdi->szCompany, ZMAEE_IShell_GetCompanyName(po), sizeof(pdi->szCompany));
	}

	// os
	strncpy(pdi->szOS, ZMAEE_OS_NAME, sizeof(pdi->szOS));

	// work folder
	szPtr = (char*)ZMAEE_IShell_GetWorkDir(po);
	strncpy(pdi->szWorkDir, szPtr, sizeof(pdi->szWorkDir));	
	
	// version
	pdi->nVersion = ZMAEE_IShell_GetSoftVersion(po);

	// dwLang
	{
		extern const sLanguageDetails mtk_gLanguageArray[MAX_LANGUAGES];
		extern U16 gCurrLangIndex;
	
		if(!strcmp((char*)mtk_gLanguageArray[gCurrLangIndex].aLangSSC, SSC_ENGLISH)) {
			pdi->dwLang = ZMAEE_LANG_TYPE_ENG;
		} else if(!strcmp((char*)mtk_gLanguageArray[gCurrLangIndex].aLangSSC, SSC_SCHINESE)) {
			pdi->dwLang = ZMAEE_LANG_TYPE_CHS;
		} else {
			pdi->dwLang = ZMAEE_LANG_TYPE_OTHER;
		}

		// Fixed dwLang by mcc
		if(g_zmaee_fix_lang_by_mcc) {
			ZMAEE_LANG_TYPE fixed_lang;

			if(ZMAEE_Shell_FixLangByMCC(po, &fixed_lang)) {
				pdi->dwLang = ZMAEE_LANG_TYPE_CHS;
			}
		}
	}

	// dwCapability
	if(ZMAEE_KEYPAD_TYPE & ZMAEE_HW_KEYPAD_NORMAL)
		pdi->dwCapabilities = ZMAEE_HW_KEYPAD_NORMAL | ZMAEE_HW_KEYPAD_OK | ZMAEE_HW_KEYPAD_ARROW | ZMAEE_HW_KEYPAD_SOFTKEY;
	else if(ZMAEE_KEYPAD_TYPE != 0)
		pdi->dwCapabilities = ZMAEE_KEYPAD_TYPE | ZMAEE_HW_KEYPAD_NORMAL;
	else
		pdi->dwCapabilities = ZMAEE_KEYPAD_TYPE;
	
#ifdef __MMI_TOUCH_SCREEN__
		pdi->dwCapabilities |= ZMAEE_CAP_TOUCHSCREEN;
#endif

#if (ZMAEE_SUPPORT_GPS == 1)
		pdi->dwCapabilities |= ZMAEE_CAP_GPS;
#endif

#if (ZMAEE_SUPPORT_GSENSOR == 1)
		pdi->dwCapabilities |= ZMAEE_CAP_GSENSOR;
#endif

#if (ZMAEE_SUPPORT_ROTATE == 1)
		pdi->dwCapabilities |= ZMAEE_CAP_ROTATE;
#endif

		{
			int key_pad = (ZMAEE_KEYPAD_TYPE & ZMAEE_HW_KEYPAD_NORMAL);
#ifdef __MMI_TOUCH_SCREEN__
			int touch_screen = E_ZM_AEE_TRUE;
#else
			int touch_screen = E_ZM_AEE_FALSE;
#endif

			if(key_pad && !touch_screen) {
				pdi->dwCapabilities |= ZMAEE_CAP_KEYPAD_ONLY;
			} else if(!key_pad && touch_screen) {
				pdi->dwCapabilities |= ZMAEE_CAP_TOUCHSCREEN_ONLY;
			}
		}

	// Model
#ifdef __ZMAEE_APP_TTS__
	strcpy(pdi->szModel, "TTS_");
	strncpy(pdi->szModel + 4, ZMAEE_MODEL_NAME, sizeof(pdi->szModel) / 2 - 5);
#else
	strncpy(pdi->szModel, ZMAEE_MODEL_NAME, sizeof(pdi->szModel) / 2 - 1);
#endif

	// OsVersion
	ZMAEE_Shell_GetOsVern(pdi);

	// FAEName
	strncpy(pdi->szFAEName, ZMAEE_FAE_NAME, sizeof(pdi->szFAEName) - 1);

	// Build Date
	{
		char temp_date[16] = {0};
		char *p_date = (char*)build_date_time();
		char *p_sep = NULL;

		memset(pdi->szBuildDate, 0, sizeof(pdi->szBuildDate));
		p_sep = strchr(p_date, '/');
		strncat(pdi->szBuildDate, p_date, (int)(p_sep - p_date));
		p_date = p_sep + 1;
		p_sep = strchr(p_date, '/');
		strncat(pdi->szBuildDate, p_date, (int)(p_sep - p_date));
		p_date = p_sep + 1;
		p_sep = strchr(p_date, ' ');
		strncat(pdi->szBuildDate, p_date, (int)(p_sep - p_date));
	}
	pdi->szModel[31] =1;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 设置定时器
 * 参数:
 * 	po							AEE_IShell实例
 * 	dwMsecs						定时器触发的毫秒数
 * 	pfn							定时器处理函数
 * 	owerId						群组ID
 * 	pUser						用户数据
 * 返回:
 * 	>= 0							Timer ID
 * 	E_ZM_AEE_FAILURE			创建失败
 */
int ZMAEE_IShell_SetTimer(AEE_IShell* po, long dwMsecs, ZMAEE_TIMER_PFN pfn, unsigned int owerId,  void* pUser)
{
	ZMAEE_TIMER_ITEM *timer_item;

	if((po == NULL) || (pfn == NULL)) {
		return E_ZM_AEE_FAILURE;
	}

	timer_item = ZMAEE_Timer_GetFreeItem(po, (void*)pfn);
	if(!timer_item) {
		return E_ZM_AEE_FAILURE;
	}

	timer_item->is_valid = 1;
	timer_item->expire_time = dwMsecs;
	timer_item->owner_id = owerId;
	ZMAEE_Callback_Init(&(timer_item->callback), (void*)pfn, pUser);

	gui_start_timer(dwMsecs, timer_item->sys_callback);

	return timer_item->timer_id;
}

/**
 * 取消定时器
 * 参数:
 * 	po							AEE_IShell实例
 * 	timerid						Timer ID
 * 返回:
 * 	E_ZM_AEE_FAILURE			失败
 * 	E_ZM_AEE_SUCCESS			成功
 */
int ZMAEE_IShell_CancelTimer(AEE_IShell* po, int timerid)
{
	ZMAEE_TIMER_ITEM *timer_item;

	if(po == NULL) {
		return E_ZM_AEE_FAILURE;
	}

	timer_item = ZMAEE_Timer_GetItemByIdx(po, timerid);
	if(!timer_item) {
		return E_ZM_AEE_FAILURE;
	}

	gui_cancel_timer(timer_item->sys_callback);
	timer_item->is_valid = 0;

	return E_ZM_AEE_SUCCESS;
}

/**
 * 取消群组定时器
 * 参数:
 * 	po							AEE_IShell实例
 * 	owerId						群组ID
 * 返回:
 * 	E_ZM_AEE_FAILURE			失败
 * 	E_ZM_AEE_SUCCESS			成功
 */
int ZMAEE_IShell_CancelOwnerTimer(AEE_IShell* po, unsigned int owerId)
{
	ZMAEE_TIMER_ITEM *timer_items[AEE_MAX_TIMER_HDLR] = {0};
	int i, item_count;

	if(po == NULL) {
		return E_ZM_AEE_FAILURE;
	}

	item_count = ZMAEE_Timer_GetItemByOwnerId(po, timer_items, owerId);
	for(i = 0; i < item_count; i++) {
		gui_cancel_timer(timer_items[i]->sys_callback);
		timer_items[i]->is_valid = 0;
	}

	return E_ZM_AEE_SUCCESS;
}

/**
 * 获取开机ticks
 * 参数:
 * 	po							AEE_IShell实例
 * 返回:
 * 	0							获取失败
 * 	>= 0							获取成功
 */
unsigned long ZMAEE_IShell_GetTickCount(AEE_IShell* po)
{
	unsigned long last_time;

	if(po == NULL) {
		return 0;
	}
	
#ifdef MMI_ON_HARDWARE_P
{
	kal_uint32 last_ticks;
	kal_get_time (&last_ticks);
	last_time = kal_ticks_to_milli_secs( last_ticks );
}
#else
	last_time = GetTickCount();
#endif

	return last_time;
}

#if (ZM_AEE_MTK_SOFTVERN >= 0x09B0)
void mmi_frm_sms_send_message(module_type MOD_SRC, 
										module_type MOD_DEST, 
										U8 MSG_SAP,
										const U16 MSG_ID,
										void* LOCAL,
										void* PEER)
{
	MYQUEUE Message;

	Message.oslSrcId = MOD_SRC;
	Message.oslDestId = MOD_DEST;
	Message.oslMsgId = (msg_type)MSG_ID;
	Message.oslDataPtr = LOCAL;
	Message.oslPeerBuffPtr = PEER;

	OslMsgSendExtQueue(&Message);
}
#endif

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0) && defined MT6252
static wap_browser_startup_req_struct	sg_zmaee_open_url_req = {0};

#ifdef DOORNET_BROWSER_SUPPORT
#include "browser_api.h"
#endif

int ZMAEE_OpenWapBrowser_PeriodCB(int nHandle, void* pUser)
{
	wap_browser_startup_req_struct *open_url_req = (wap_browser_startup_req_struct*)pUser;

#ifndef DOORNET_BROWSER_SUPPORT
	srv_brw_wap_browser_startup_req((void*)open_url_req);
#else
	char Urlbuffter[AEE_MAX_WAP_LEN] = {0};
	mmi_asc_to_ucs2(Urlbuffter, sg_zmaee_open_url_req.url);
	wap_start_browser(WAP_BROWSER_GOTO_URL, Urlbuffter);
#endif
	return 0;
}
#endif


/**
 * 打开WAP浏览器
 * 参数:
 * 	po							AEE_IShell实例
 * 	pszURL						URL网址				
 * 返回:
 * 	0							打开浏览器失败
 * 	1							打开浏览器成功
 */
unsigned long 	ZMAEE_IShell_OpenWapBrowser(AEE_IShell* po, const char* pszURL)
{
	if((po == NULL) || (pszURL == NULL)) {
		return 0;
	}

#ifdef MMI_ON_HARDWARE_P
{
	#ifdef __MMI_PLX_BROWSER__
			extern void EntryOfPlxBrowserView(const char*  szUtf8Url);
			char Urlbuffter[AEE_MAX_WAP_LEN] = {0};
			int url_len;			

			url_len = strlen(pszURL);
			memcpy(Urlbuffter, pszURL, (sizeof(Urlbuffter) < strlen(pszURL))? sizeof(Urlbuffter): strlen(pszURL));
			EntryOfPlxBrowserView(Urlbuffter);
			
			return 1;
	#else	// __MMI_PLX_BROWSER__
			char Urlbuffter[AEE_MAX_WAP_LEN] = {0};
			int url_len;			
	
			url_len = strlen(pszURL);
			memcpy(Urlbuffter, pszURL, (sizeof(Urlbuffter) < strlen(pszURL))? sizeof(Urlbuffter): strlen(pszURL));
			if(pszURL) {
			#ifndef MT6252
			#if 0
				wap_browser_startup_req_struct	*open_url_req;
				open_url_req = (wap_browser_startup_req_struct*)OslConstructDataPtr(sizeof(wap_browser_startup_req_struct));	
				open_url_req->type = 2;
				strcpy((S8*)open_url_req->url, (S8*)Urlbuffter); 
				mmi_frm_sms_send_message(MOD_MMI, wap_get_service_module(MSG_ID_WAP_BROWSER_STARTUP_REQ), 0, MSG_ID_WAP_BROWSER_STARTUP_REQ, (void *)open_url_req, NULL);
			#endif
			#else
				memset(&sg_zmaee_open_url_req, 0, sizeof(sg_zmaee_open_url_req));
				sg_zmaee_open_url_req.type = 2;
				strcpy((S8*)sg_zmaee_open_url_req.url, (S8*)Urlbuffter);
				ZMAEE_StartPeriodHandler(0, (void*)&sg_zmaee_open_url_req, ZMAEE_OpenWapBrowser_PeriodCB);
			#endif
				
				return 1;
			} else {
				return 0;
			}
	#endif	// __MMI_PLX_BROWSER__
}
#endif	// MMI_ON_HARDWARE_P

	return 0;
}

int ZMAEE_IShell_IsGlobalLibraryUseStaticMem(AEE_IShell *po)
{
#ifdef ZMAEE_GDLL_USE_APPMEM
	return E_ZM_AEE_FALSE;
#else
	return E_ZM_AEE_TRUE;
#endif
}



static
int ZMAEE_IShell_EntryAppletPeriodPCB(int nHandle, void* pUser)
{
	extern ZMAEE_AppletEntry(ZM_AEECLSID cls_id,const char * work_dir,void * param,int param_len,char * appName,char * appIcon,unsigned int nIconLen,int bkg,int app_mask,void(* reentry_func)(void), unsigned short scr_id);
	extern void ZMAEE_DWallPaper_ClrMask();
	extern int ZMAEE_DWallPaper_Validating(void);
	char* ZMAEE_IShell_GetAppDir(AEE_IShell *po, int app_attr);

	ZMAEE_ENTRYAPPLET_PARAM *applet_param = (ZMAEE_ENTRYAPPLET_PARAM*)pUser;
	char *work_dir;

	ZMAEE_Exit_Ext();
	if(ZMAEE_DWallPaper_Validating()) {		
		ZMAEE_DWallPaper_ClrMask();
	}

	work_dir = ZMAEE_IShell_GetAppDir(NULL, applet_param->app_attr);

	if(applet_param->cls_id == 1)
		applet_param->app_attr = ZMAEE_APPATTR_APPLET_APPLICATION | ZMAEE_APPATTR_APPLET_GAME;
	
	ZMAEE_AppletEntry(applet_param->cls_id, work_dir, applet_param->param, applet_param->param_len, applet_param->appName, 
		NULL, 0, 0, applet_param->app_attr, NULL, 0);
	ZMAEE_Free(applet_param);

	return 0;
}


/**
 * 加载app，用于动态壁纸加载app时使用，大厅或者其他app中加载另外一个app使用AEE_IShell_StartApplet
 * @ cls_id		class id
 * @ param		启动参数
 * @ param_len	启动参数字节长度
 * @ appName	UCS2编码的app名称
 * @ app_mask	app mask
 */
void ZMAEE_IShell_EntryApplet(AEE_IShell *po, ZM_AEECLSID cls_id, int app_attr, void* param, int param_len, char *appName)
{
	ZMAEE_ENTRYAPPLET_PARAM *applet_param = NULL;
	int cpy_len;

	if(!po || !appName)
		return;

	applet_param = (ZMAEE_ENTRYAPPLET_PARAM*)ZMAEE_Malloc(sizeof(ZMAEE_ENTRYAPPLET_PARAM));
	if(!applet_param)
		return;

	memset(applet_param, 0, sizeof(ZMAEE_ENTRYAPPLET_PARAM));
	applet_param->cls_id = cls_id;
	applet_param->app_attr = app_attr;

	if(param) {
		cpy_len = param_len;
		if(cpy_len > sizeof(applet_param->param))
			cpy_len = sizeof(applet_param->param);
		memcpy(applet_param->param, param, cpy_len);
		applet_param->param_len = cpy_len;
	}

	cpy_len = 2 * mmi_ucs2strlen(appName);
	if(cpy_len >= sizeof(applet_param->appName))
		cpy_len = sizeof(applet_param->appName) - 2;
	memcpy(applet_param->appName, appName, cpy_len);

	ZMAEE_StartPeriodHandler(0x0, (void*)applet_param, ZMAEE_IShell_EntryAppletPeriodPCB);
}

/**
 * 获取移植时的各类型app的工作目录
 * 参数:
 * @ app_attr			app类型，参考zmaee_typedef.h中的ZMAEE_APPATTR_APPLET_XXX
 * 返回:
 * 	!= NULL			工作目录，内存不用释放
 *	== NULL			po is null, or app_attr is invalid
 */
char* ZMAEE_IShell_GetAppDir(AEE_IShell *po, int app_attr)
{
	switch(app_attr) {
#if ZMAEE_SUPPORT_SEPARE_APPANDGAME
		case ZMAEE_APPATTR_APPLET_APPLICATION:
			return ZMAEE_ENTRY_APP;
		case ZMAEE_APPATTR_APPLET_GAME:
			return ZMAEE_ENTRY_GAME;
#else
		case ZMAEE_APPATTR_APPLET_APPLICATION:
		case ZMAEE_APPATTR_APPLET_GAME:
			return ZMAEE_ENTRY_WORKDIR;
#endif

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
		case ZMAEE_APPATTR_APPLET_KEYPADLOCK:			
			return ZMAEE_ENTRY_THEMEDIR;
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
		case ZMAEE_APPATTR_APPLET_IDLEAPP:
			return ZMAEE_ENTRY_THEMEDIR;			
#endif

#ifdef __ZMAEE_APP_THEME__
		case ZMAEE_APPATTR_APPLET_MAINMENU:
			return ZMAEE_ENTRY_THEMEDIR;			
#endif
	}

	return NULL;
}

/**
 * 获取项目移植了哪些大厅
 * RETURN:
 * 每个bit位表示一种类型的大厅，该bit位为0，表示未移植该大厅，该bit位为1，表示移植了该大厅
 * 掌盟互娱大厅必定会移植，所以不再包含在返回值当中。
 * 每个大厅对应的bit位位数请参考zmaee_typedef.h中的e_ZMAEE_SUPPORTHALL_BITS
 */
unsigned int ZMAEE_IShell_GetSupportHall(AEE_IShell *po)
{
	unsigned int ret = 0;

#ifdef __ZMAEE_APP_KEYPAD_LOCK__
	ret |= (1 << ZMAEE_KEYLOCK_BITS);
#endif

#ifdef __ZMAEE_APP_DWALLPAPER__
	ret |= (1 << ZMAEE_DWPAPER_BITS);
#endif

#ifdef __ZMAEE_APP_THEME__
	ret |= (1 << ZMAEE_THEME_BITS);
#endif

	return ret;
}

void ZMAEE_CreateVtableExt(void **p_vtbl, int nCount, ...)
{
 	int i;
 	va_list ap;

	if(p_vtbl == 0) return;

 	va_start(ap, nCount);
	for( i = 0; i < nCount; ++i)
 		p_vtbl[i] = va_arg(ap, void*);
	va_end(ap);
}

int ZMAEE_IShell_CreateInstanceEx(AEE_IShell * po, ZM_AEECLSID clsId, void** ppobj)
{
	switch(clsId) {
		case ZM_AEE_CLSID_MESSAGE:
			return ZMAEE_IMessage_New(clsId, ppobj);

		case ZM_AEE_CLSID_UTIL:
			return ZMAEE_IUtil_New(clsId, ppobj);

#ifdef __ZMAEE_APP_DESKTOP__
		case ZM_AEE_CLSID_DESKTOP:
			return ZMAEE_IDesktop_New(clsId, ppobj);
#endif
	}

	*ppobj = NULL;
	return E_ZM_AEE_CLASSNOTSUPPORT;
}

/**********************************************/
/** 内外单合并补丁
/**********************************************/

/**
 * RETURN:
 * 	E_ZM_AEE_TRUE			需要修正语言类型
 * 	E_ZM_AEE_FALSE			不需要修正语言类型
 */
int ZMAEE_Shell_FixLangByMCC(AEE_IShell *po, ZMAEE_LANG_TYPE *pLang)
{
	AEE_ITAPI *pTapi;
	char szMCC[8] = {0};
	int nSimIdx;

	if(ZMAEE_IShell_ActiveApplet(po) == ZMAEE_DOWNLOAD_CLSID) {
		ZMAEE_IShell_CreateInstance(po, ZM_AEE_CLSID_TAPI, (void**)&pTapi);
		nSimIdx = ZMAEE_ITAPI_GetActiveSimCard(pTapi);
		ZMAEE_ITAPI_GetImsi(pTapi, nSimIdx, szMCC, sizeof(szMCC) / sizeof(szMCC[0]));

		if(!strncmp(szMCC, "460", 3)) {
			if(pLang)
				*pLang = ZMAEE_LANG_TYPE_CHS;
			
			return E_ZM_AEE_TRUE;
		}
	}

	return E_ZM_AEE_FALSE;
}


static int ZMAEE_Shell_FixLangByMCC_Restore(int nHandle, void* pUser)
{
	AEE_IShell *pShell = (AEE_IShell*)pUser;
	AEE_IFileMgr *pFileMgr;
	char szWorkDir[64] = {0};

	// Restore
	ZMAEE_SetUserID(0);
	ZMAEE_IShell_CreateInstance(pShell, ZM_AEE_CLSID_FILEMGR, (void**)&pFileMgr);
	
	szWorkDir[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 0);
	strcat(szWorkDir, ZMAEE_WORK_ROOT_DIR);
	ZMAEE_IFileMgr_RmDir(pFileMgr, szWorkDir);
	
	if((szWorkDir[0] = ZMAEE_IFileMgr_GetDriver(pFileMgr, 1))) {
		ZMAEE_IFileMgr_RmDir(pFileMgr, szWorkDir);
	}

	return 0;
}

void ZMAEE_Shell_Switch_FixLangByMCC(void)
{
	unsigned short ucs2Disp[32] = {0};
	char ascDisp[64] = {0};
	AEE_IFileMgr *pFileMgr;
	AEE_IShell *pShell = ZMAEE_GetShell();

	g_zmaee_fix_lang_by_mcc = !g_zmaee_fix_lang_by_mcc;

	// Display Switch Status
	sprintf(ascDisp, "fix_lang_by_mcc=%d", g_zmaee_fix_lang_by_mcc);
	ZMAEE_Utf8_2_Ucs2(ascDisp, strlen(ascDisp), ucs2Disp, sizeof(ucs2Disp) / sizeof(ucs2Disp[0]));
	DisplayPopup((unsigned char*)ucs2Disp, IMG_GLOBAL_INFO, 0, 10000, 0);

	ZMAEE_StartPeriodHandler(0, (void*)pShell, ZMAEE_Shell_FixLangByMCC_Restore);
}

/**********************************************/
/** OS Version
/**********************************************/
void ZMAEE_Shell_GetOsVern(ZMAEEDeviceInfo *dev_info)
{
#ifdef MT6253
	#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)
		sprintf(dev_info->szOsVern, "53_09A");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x09B0)
		sprintf(dev_info->szOsVern, "53_09B");
	#else
		sprintf(dev_info->szOsVern, "53_10A");
	#endif
#endif

#if defined MT6235 || defined MT6236 || defined MT6235B
	#if (ZM_AEE_MTK_SOFTVERN < 0x09B0)	
		sprintf(dev_info->szOsVern, "36_09A");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x09B0)
		sprintf(dev_info->szOsVern, "36_09B");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x10A0)
		sprintf(dev_info->szOsVern, "36_10A");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x11A0)
		sprintf(dev_info->szOsVern, "36_11A");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x11B0)
		sprintf(dev_info->szOsVern, "36_11B");
	#endif
#endif

#ifdef MT6252
	#if (ZM_AEE_MTK_SOFTVERN == 0x10A0)
		sprintf(dev_info->szOsVern, "52_10A");
	#elif (ZM_AEE_MTK_SOFTVERN == 0x11B0)
		sprintf(dev_info->szOsVern, "52_11B");
	#endif
#endif

#ifdef MT6255
	#if (ZM_AEE_MTK_SOFTVERN == 0x11B0)
		sprintf(dev_info->szOsVern, "55_11B");
	#endif
#endif

#ifdef MT6250
	sprintf(dev_info->szOsVern, "50D_32x32");
#endif

}

#endif	// __ZMAEE_APP__

