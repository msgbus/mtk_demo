#ifdef __ZMAEE_APP__

#include "zmaee_priv.h"

#include "MMIDataType.h"
#include "MMI_features.h"
#include "kal_release.h"

#include "mcd_l4_common.h"
#include "l4c_common_enum.h"
#include "l4c_aux_struct.h"

#if (ZM_AEE_MTK_SOFTVERN < 0x11A0)
#include "l4c_rspfmttr.h"
#endif
#include "l4c2csm_ss_struct.h"

#include "csmss_common_enums.h"
#include "ps2sim_struct.h"
#include "sim_common_enums.h"
#include "smu_common_enums.h"
#include "rmmi_common_enum.h"
#include "rmmi_context.h"
#include "l4c_ss_parse.h"

#include "sim_common_enums.h"
#include "stack_common.h"
#include "stack_msgs.h"
#include "app_ltlcom.h"

#include "device.h"
#include "med_struct.h"
#include "stack_timer.h"
#include "l4a.h"

#include "GlobalConstants.h"

#if (ZM_AEE_MTK_SOFTVERN >= 0x0852)
#include "cbm_api.h"
#else
#include "EventsGprot.h"
#include "ConnectManageGProt.h"
#endif

#if (ZM_AEE_MTK_SOFTVERN > 0x08B0)
#include "mmi_frm_events_gprot.h"
#include "mmi_frm_queue_gprot.h"
#else
#include "QueueGprot_Int.h"
#include "EventsDef_Int.h"
#include "EventsGprot_Int.h"
#endif

#include "mmi_msg_struct.h"

#include "ProtocolEvents.h"


#include "app2soc_struct.h"
#include "soc_api.h"
#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
#include "gui.h"
#include "DtcntSrvIprot.h "
#endif

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
extern kal_uint8 custom_get_gprs_profile_num(void);
extern kal_uint8 custom_get_csd_profile_num(void);
#endif
extern int ZMAEE_GetActiveSimCard(void);
extern void ZMAEE_SetProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func);
extern void ZMAEE_ClearProtocolEventHandler(int sim_idx, unsigned short event, PsIntFuncPtr func);
extern int ZMAEE_ITAPI_Release(AEE_ITAPI* po);
#ifdef WIN32
extern int				ZMAEE_Tapi_GetSimEnum(int sim_idx);
extern int 				ZMAEE_Utf8_2_Ucs2(const char* , int , unsigned short* , int);
extern int				zmaee_stricmp(const char * string1, const char * string2);
extern S8				*mmi_ucs2ncpy(S8 *strDestination, const S8 *strSource , U32 size );
#endif

typedef struct
{
	void*				pVtbl;
	int					nRef;
	int					nSockId;
	int					nStat;/*0:idle, 1:connecting, 2:connected, 3:closed*/
	ZMAEE_LINKTYPE		nLinkType;
	ZMAEE_CALLBACK		pfnRead;
	ZMAEE_CALLBACK		pfnWrite;
	ZMAEE_CALLBACK		pfnConnect;
}ZMAEE_SOCKET;

typedef struct
{
	void* 				pVtbl;
	int					nRef;
	ZMAEE_LINKSTATE		nLinkStat[2];
	ZMAEE_CALLBACK		pFnDNS;
	ZMAEE_CALLBACK		pfnAPN;
	ZMAEE_SOCKET		szSockets[10];
#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	unsigned int		wap_acct_id;
	unsigned int		net_acct_id;
	unsigned char		app_id;
#endif

	unsigned int		dns_request_id;
}ZMAEE_NETMGR;

static ZMAEE_SOCKET*	ZMAEE_INetMgr_GetFreeSocket(ZMAEE_NETMGR* pThis);

/**
  * INetMgr 接口
  */
static int 				ZMAEE_INetMgr_AddRef(AEE_INetMgr* po);
zm_extern int 			ZMAEE_INetMgr_Release(AEE_INetMgr* po);
static int 				ZMAEE_INetMgr_State(AEE_INetMgr* po, ZMAEE_LINKTYPE type);
static int 				ZMAEE_INetMgr_SetApn(AEE_INetMgr* po, ZMAEE_LINKTYPE type,const ZMAEE_APN* pApn,ZMAEE_APN_PFN pFn, void* pUser);
static int 				ZMAEE_INetMgr_GetApn(AEE_INetMgr* po, ZMAEE_LINKTYPE type, ZMAEE_APN* pApn,ZMAEE_APN_PFN pFn, void* pUser);
static int 				ZMAEE_INetMgr_GetHostByName(AEE_INetMgr* po, const char* demain, zmaddr_32* dwAddr, ZMAEE_PFNDNSCB pfn, void* pUser);
static AEE_ISocket* 	ZMAEE_INetMgr_OpenSocket(AEE_INetMgr* po, ZMAEE_LINKTYPE lType, ZMAEE_SOCKTYPE sType);
static AEE_IHttp* 		ZMAEE_INetMgr_OpenHttp(AEE_INetMgr* po, ZMAEE_LINKTYPE type,AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
zm_extern int			ZMAEE_INetMgr_SupportWifi(AEE_INetMgr *po);
/**
  * SOCKET 接口
  */
static int 				ZMAEE_ISocket_AddRef(AEE_ISocket* po);
static int 				ZMAEE_ISocket_Release(AEE_ISocket* po);
static int 				ZMAEE_ISocket_Read(AEE_ISocket * po, void * pDest, unsigned long nWant);
static int 				ZMAEE_ISocket_Write(AEE_ISocket * po, void * pDest, unsigned long nWant);
static void 			ZMAEE_ISocket_Readable(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void * pUser);
static void 			ZMAEE_ISocket_Writeable(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void* pUser);
static void 			ZMAEE_ISocket_Cancel(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void * pUser);
static int 				ZMAEE_ISocket_Connect(AEE_ISocket* po, zmaddr_32 dwAddr, zmport_16 wPort, ZMAEE_PFNCONNNECTCB pfn, void* pUser);
static int 				ZMAEE_ISocket_Bind(AEE_ISocket* po, zmaddr_32 dwAddr, zmport_16 port);
static int 				ZMAEE_ISocket_SendTo(AEE_ISocket* po, void* pBuffer, int wBytes, zmaddr_32 dwAddr, zmport_16 wPort);
static int 				ZMAEE_ISocket_RecvFrom(AEE_ISocket* po, void* pBuff, int wBytes, zmaddr_32 *dwAddr, zmport_16* wPort);

static ZMAEE_NETMGR g_zmaee_netmgr={NULL, 1, {ZMAEE_DATALINK_DISCONNECT, ZMAEE_DATALINK_DISCONNECT}};

void ZMAEE_INetMgr_Init(void)
{
	int i;
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)&g_zmaee_netmgr;

	for(i = 0; i < sizeof(pThis->szSockets)/sizeof(pThis->szSockets[0]); ++i)
	{
		pThis->szSockets[i].nSockId = -1;
		pThis->szSockets[i].nStat = 0;
	}
}

zm_extern int ZMAEE_NetMgr_GetApnCnf(ZMAEE_LINKTYPE nType, ZMAEE_APN *pApn);
zm_extern int ZMAEE_NetMgr_LocalizeGateway(ZMAEE_LINKTYPE nType, zmaddr_32* dwAddr, zmport_16* wPort);
/**
 * @szImsi			输入参数，激活卡的IMSI号
 * @nType			输入参数，联网类型，wap or net
 * @pApn			输出参数，Apn的结构图，如果为NULL，则不填充
 * @szAddrGateway	输出参数，网关地址字符串，如果为NULL，则不输出
 * @nPort			输出参数，网关端口，如果为NULL，则不输出
void ZMAEE_INetMgr_GetNetAP(const char *szImsi, ZMAEE_LINKTYPE nType, 
							ZMAEE_APN *pApn, char *szAddrGateway, unsigned int *nPort)
{
}
*/

unsigned long zmaee_ipconvert(const char* ip)
{
	kal_bool ip_validity = 0;
	unsigned long dwAddr = 0;
	if(soc_ip_check((kal_char*)ip, (kal_uint8*)&dwAddr, &ip_validity) && ip_validity)
		return dwAddr;
	return 0;
}

int ZMAEE_INetMgr_GetImsi(char *szImsi, int nLen)
{
#ifndef MT6252
	AEE_ITAPI *iTapi = NULL;
#endif

	if(!szImsi)
		return -1;
	
#ifndef MT6252
	if(ZMAEE_ITAPI_New(ZM_AEE_CLSID_TAPI, (void**)&iTapi) != E_ZM_AEE_SUCCESS) {
		ZMAEE_DebugPrint("ZMAEE_INetMgr_GetImsi: ZMAEE_ITAPI_New failed");
		return -1;
	}

	if(ZMAEE_ITAPI_GetImsi(iTapi, ZMAEE_GetActiveSimCard(), szImsi, nLen) != E_ZM_AEE_SUCCESS) {
		ZMAEE_DebugPrint("ZMAEE_INetMgr_GetImsi: get imsi failed");
		return -1;
	}

	ZMAEE_ITAPI_Release(iTapi);
#else
	{
		extern unsigned char ZMAEE_Tapi_GetImsi(int sim, char *out_imsi_buffer, unsigned int buffer_size);
		int sim_idx = ZMAEE_GetActiveSimCard();
		if(ZMAEE_Tapi_GetImsi(ZMAEE_Tapi_GetSimEnum(sim_idx), szImsi, nLen) != E_ZM_AEE_TRUE) {
			ZMAEE_DebugPrint("ZMAEE_INetMgr_GetImsi: ZMAEE_Tapi_GetSimEnum failed");
			return -1;
		}
	}
#endif

	return 0;
}

int ZMAEE_INetMgr_New(ZM_AEECLSID clsId, void** ppobj)
{
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)&g_zmaee_netmgr;
	int i;

	if(pThis == NULL)
	{
		*ppobj = NULL;
		return E_ZM_AEE_NOMEMORY;
	}

	if(pThis->pVtbl == NULL)
	{
	#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
		pThis->wap_acct_id = pThis->net_acct_id = 0xFFFFFFFF;
		if(cbm_register_app_id(&pThis->app_id) != CBM_OK)
			pThis->app_id = 1;
	#endif
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, 10,
				ZMAEE_INetMgr_AddRef,
				ZMAEE_INetMgr_Release,
				ZMAEE_INetMgr_State,
				ZMAEE_INetMgr_SetApn,
				ZMAEE_INetMgr_GetApn,
				ZMAEE_INetMgr_GetHostByName,
				ZMAEE_INetMgr_OpenSocket,
				ZMAEE_INetMgr_OpenHttp,
				ZMAEE_INetMgr_SupportWifi);
		}
		
		for(i = 0; i < sizeof(pThis->szSockets)/sizeof(pThis->szSockets[0]); ++i)
		{
			pThis->szSockets[i].nSockId = -1;
			pThis->szSockets[i].nStat = 0;
			if(pThis->szSockets[i].pVtbl == NULL)
			{
				pThis->szSockets[i].pVtbl = ZMAEE_GetVtable(ZM_AEE_CLSID_SOCKET);
				if(pThis->szSockets[i].pVtbl == 0)
				{
					pThis->szSockets[i].pVtbl = ZMAEE_CreateVtable( ZM_AEE_CLSID_SOCKET, 11,
						ZMAEE_ISocket_AddRef,
						ZMAEE_ISocket_Release,
						ZMAEE_ISocket_Read,
						ZMAEE_ISocket_Write,
						ZMAEE_ISocket_Readable,
						ZMAEE_ISocket_Writeable,
						ZMAEE_ISocket_Cancel,
						ZMAEE_ISocket_Connect,
						ZMAEE_ISocket_Bind,
						ZMAEE_ISocket_SendTo,
						ZMAEE_ISocket_RecvFrom);
				}
			}
		}

	}

	*ppobj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
}

static int ZMAEE_INetMgr_AddRef(AEE_INetMgr* po)
{
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;
	if(po == NULL)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef++;

	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_INetMgr_Release(AEE_INetMgr* po)
{
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;
	if(po == NULL)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;

	return E_ZM_AEE_SUCCESS;
}

static int ZMAEE_INetMgr_State(AEE_INetMgr* po, ZMAEE_LINKTYPE type)
{
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;

	if(po == NULL)
		return E_ZM_AEE_BADPARAM;

	return pThis->nLinkStat[type];
}

static int ZMAEE_Encode_Data_AccountId(kal_uint32 nwk_account_id, int simId)
{
	int nNwkAccountId =  nwk_account_id;

#if (ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	nNwkAccountId = cbm_encode_data_account_id(nwk_account_id, simId, g_zmaee_netmgr.app_id, 0);
#else
	#ifdef __MMI_DUAL_SIM_MASTER__
		#if (ZM_AEE_MTK_SOFTVERN >=  0x08A0)
			nNwkAccountId = cbm_encode_data_account_id(nwk_account_id, simId, 0, 0);
		#else
			nNwkAccountId = always_ask_encode_data_account_id(nwk_account_id, simId+1, 0, 0);
		#endif
	#endif
#endif
	ZMAEE_DebugPrint("ZMAEE_Encode_Data_AccountId: nwk_account_id = %d, simId = %d, nNwkAccountId = %d",
		nwk_account_id, simId, nNwkAccountId);
	
	return nNwkAccountId;
}

void ZMAEE_INetMgr_SetApn_Resp(void* para)
{

#if(ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	
		if(ZMAEE_Callback_Valid(&g_zmaee_netmgr.pfnAPN)) 
		{
			ZMAEE_APN_PFN pfn;
	
			pfn = (ZMAEE_APN_PFN)g_zmaee_netmgr.pfnAPN.pfn;
			g_zmaee_netmgr.pfnAPN.pfn = 0;
			
			pfn(1, 0, g_zmaee_netmgr.pfnAPN.pUser);		
		}
		
#else  //ZM_AEE_MTK_SOFTVERN >= 0x10A0

	mmi_ps_set_gprs_data_account_rsp_struct *localPtr =  (mmi_ps_set_gprs_data_account_rsp_struct *)para;

	ZMAEE_ClearProtocolEventHandler(0, PRT_MMI_PS_SET_GPRS_DATA_ACCOUNT_RSP, (PsIntFuncPtr)ZMAEE_INetMgr_SetApn_Resp);
	if( localPtr != NULL && ZMAEE_Callback_Valid(&g_zmaee_netmgr.pfnAPN) )
	{
		ZMAEE_APN_PFN pfn;
		pfn  = (ZMAEE_APN_PFN)g_zmaee_netmgr.pfnAPN.pfn;
		g_zmaee_netmgr.pfnAPN.pfn = 0;
		pfn(localPtr->result,  0,  g_zmaee_netmgr.pfnAPN.pUser);
	}

#endif//ZM_AEE_MTK_SOFTVERN >= 0x10A0
}

static int ZMAEE_INetMgr_SetApn( AEE_INetMgr* po, 
	ZMAEE_LINKTYPE type, const ZMAEE_APN* pApn, ZMAEE_APN_PFN pFn, void* pUser)
{
#if(ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	
	int i, j;
	srv_dtcnt_result_enum ret;
	srv_dtcnt_store_prof_data_struct prof_info;
	srv_dtcnt_prof_gprs_struct prof_gprs;
	srv_dtcnt_sim_type_enum dtcnt_sim_type;
	srv_dtcnt_store_prof_qry_struct prof_qry = {0};
	U32 acct_id;
	ZMAEE_NETMGR *pThis = (ZMAEE_NETMGR*)po;
	unsigned int acctId;
	static unsigned short ucs2_acct_name[SRV_DTCNT_PROF_MAX_ACC_NAME_LEN] = {0};

	if(type == ZMAEE_WIFI) {
		int ret = CBM_ERROR;
		
#ifdef __MMI_WLAN_FEATURES__
		ret = cbm_get_valid_account_id(CBM_BEARER_WIFI, &pThis->net_acct_id);
#else
		pThis->net_acct_id = 0xFFFFFFFF;
#endif

		ZMAEE_Callback_Init(&g_zmaee_netmgr.pfnAPN, (void*)pFn, pUser);	
		gui_start_timer_ex(10, ZMAEE_INetMgr_SetApn_Resp, (void*)((ret == CBM_OK)? 1 : 0));
		return;
	}

	switch(ZMAEE_GetActiveSimCard()) {
		case 0:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_1; 
			break;
#ifdef __MMI_DUAL_SIM_MASTER__
		case 1:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_2;
			break;
#if (MMI_MAX_SIM_NUM >= 3)
		case 2:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_3;
			break;
#endif

#if (MMI_MAX_SIM_NUM >= 4)
		case 3:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_4;
			break;
#endif
#endif
	}

	memset(ucs2_acct_name, 0, sizeof(ucs2_acct_name));
	ZMAEE_Utf8_2_Ucs2(pApn->szName, strlen(pApn->szName), (unsigned short*)ucs2_acct_name, sizeof(ucs2_acct_name)/sizeof(ucs2_acct_name[0]));
	memset(&prof_gprs, 0, sizeof(prof_gprs));
	prof_gprs.APN = (const U8*)pApn->szAPN;
	prof_gprs.prof_common_header.sim_info = dtcnt_sim_type;
	prof_gprs.prof_common_header.AccountName = (const U8*)ucs2_acct_name;
	prof_gprs.prof_common_header.Auth_info.AuthType = (pApn->nAuthType)? SRV_DTCNT_PROF_GPRS_AUTH_TYPE_SECURE: SRV_DTCNT_PROF_GPRS_AUTH_TYPE_NORMAL;
	strcpy((char*)prof_gprs.prof_common_header.Auth_info.UserName, (const char*)pApn->szUser);
	strcpy((char*)prof_gprs.prof_common_header.Auth_info.Passwd, (const char*)pApn->szPasswd);
	memcpy(prof_gprs.prof_common_header.PrimaryAddr, &pApn->addrDNS1,4);
	prof_info.prof_data = (void*)&prof_gprs;
	prof_info.prof_fields = SRV_DTCNT_PROF_FIELD_ALL;
	prof_info.prof_type = SRV_DTCNT_BEARER_GPRS;

	prof_qry.qry_info.sim_qry_info = dtcnt_sim_type;
	prof_qry.qry_info.bearer_qry_info = SRV_DTCNT_BEARER_GPRS;
	prof_qry.qry_info.acc_type_info = SRV_DTCNT_PROF_TYPE_TOTAL;
	prof_qry.qry_info.filters = SRV_DTCNT_STORE_QRY_TYPE_TOTAL;
	ret = srv_dtcnt_store_qry_ids(&prof_qry);

	for(i = 0; i < prof_qry.num_ids; i++)
	{
		srv_dtcnt_prof_gprs_struct qry_prof_gprs = {0};
		srv_dtcnt_store_prof_data_struct qry_prof_info = {0};
		
		qry_prof_info.prof_data = &qry_prof_gprs;		
		ret = srv_dtcnt_store_qry_prof(prof_qry.ids[i], &qry_prof_info);
		if(ret == SRV_DTCNT_RESULT_SUCCESS)
		{
			if(!zmaee_stricmp(qry_prof_gprs.APN, pApn->szAPN) && 
				(qry_prof_gprs.prof_common_header.sim_info == dtcnt_sim_type) &&
				(qry_prof_info.prof_type == prof_info.prof_type))
			{
				acctId = prof_qry.ids[i];
				break;
			}
		}
	}

	if(i >= prof_qry.num_ids)
	{
		ret = srv_dtcnt_store_add_prof(&prof_info, &acctId);
		if(ret != SRV_DTCNT_RESULT_SUCCESS)
		{
			srv_dtcnt_prof_gprs_struct qry_prof_gprs = {0};
			srv_dtcnt_store_prof_data_struct qry_prof_info = {0};
			qry_prof_info.prof_data = &qry_prof_gprs;

			if(ZMAEE_NET == type)
				ret = srv_dtcnt_store_qry_prof(prof_qry.ids[0], &qry_prof_info);
			else
				ret = srv_dtcnt_store_qry_prof(prof_qry.ids[1], &qry_prof_info);
			
			if(ret == SRV_DTCNT_RESULT_SUCCESS)
			{
				if(ZMAEE_NET == type)
					acctId = prof_qry.ids[0];
				else
					acctId = prof_qry.ids[1];
				
				memset(ucs2_acct_name, 0, sizeof(ucs2_acct_name));
				mmi_ucs2ncpy((char*)ucs2_acct_name, 
							 (char*)qry_prof_gprs.prof_common_header.AccountName, 
							 sizeof(ucs2_acct_name) / 2 - 1);
				
				ret = srv_dtcnt_store_update_prof(acctId, &prof_info);
			}
		}
	}

	if(type == ZMAEE_WAP)
		pThis->wap_acct_id = (ret == SRV_DTCNT_RESULT_SUCCESS)? acctId: 0xFFFFFFFF;
	else
		pThis->net_acct_id = (ret == SRV_DTCNT_RESULT_SUCCESS)? acctId: 0xFFFFFFFF;	
	
	ZMAEE_Callback_Init(&g_zmaee_netmgr.pfnAPN, (void*)pFn, pUser);
	
	gui_start_timer_ex(10, ZMAEE_INetMgr_SetApn_Resp, (void*)(ret ? 1 : 0));

	return E_ZM_AEE_SUCCESS;
#else  // ZM_AEE_MTK_SOFTVERN >= 0x10A0

	mmi_ps_set_gprs_data_account_req_struct *myMsgPtr;
	U8	ref_count;
	U16	msg_len;
	MYQUEUE Message;
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;

	if(po == NULL || pApn == NULL||pFn == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->pfnAPN.pfn)
		return E_ZM_AEE_ITEMBUSY;	
	
	myMsgPtr = (mmi_ps_set_gprs_data_account_req_struct*) OslConstructDataPtr(sizeof(mmi_ps_set_gprs_data_account_req_struct));

 	if(myMsgPtr == NULL)
		return E_ZM_AEE_NOMEMORY;
		
	ref_count = myMsgPtr->ref_count;
	msg_len = myMsgPtr->msg_len;
	memset(myMsgPtr, 0, sizeof(mmi_ps_set_gprs_data_account_req_struct));
	myMsgPtr->ref_count = ref_count;
	myMsgPtr->msg_len = msg_len;
       
	myMsgPtr->gprs_account.context_id = (type==ZMAEE_WAP)?(ZMAEE_GPRS_WAP_PROFILEID):(ZMAEE_GPRS_NET_PROFILEID);
	myMsgPtr->gprs_account.authentication_type = pApn->nAuthType;

	myMsgPtr->gprs_account.name_dcs = 0;
	myMsgPtr->gprs_account.name_length = strlen(pApn->szName);
	strcpy ((char*)myMsgPtr->gprs_account.name, pApn->szName);

	strcpy ((char*)myMsgPtr->gprs_account.apn, pApn->szAPN);
	myMsgPtr->gprs_account.apn_length = strlen(pApn->szAPN);

	strcpy ((char*)myMsgPtr->gprs_account.user_name, pApn->szUser);
	strcpy ((char*)myMsgPtr->gprs_account.password, pApn->szPasswd);
	memcpy(&myMsgPtr->gprs_account.dns, &pApn->addrDNS1, 4);

#if (ZM_AEE_MTK_SOFTVERN >= 0x0936)
	myMsgPtr->gprs_account.dcomp_algo = 0x02;
	myMsgPtr->gprs_account.hcomp_algo = 0x02;
	myMsgPtr->gprs_account.pdp_type = 0x21;
	myMsgPtr->gprs_account.pdp_addr_len = 0x01; 
	myMsgPtr->profile_type = DATA_ACCT_GPRS_PROF;
#endif//ZM_AEE_MTK_SOFTVERN

	Message.oslSrcId=MOD_MMI;
	Message.oslDestId=MOD_L4C;
	Message.oslMsgId = PRT_MMI_PS_SET_GPRS_DATA_ACCOUNT_REQ;
	Message.oslDataPtr = (oslParaType *)myMsgPtr;
	Message.oslPeerBuffPtr= NULL;

	pThis->nLinkStat[type] = ZMAEE_DATALINK_CONNECTED;
	ZMAEE_Callback_Init(&pThis->pfnAPN, (void*)pFn, pUser);

	ZMAEE_SetProtocolEventHandler(0, PRT_MMI_PS_SET_GPRS_DATA_ACCOUNT_RSP, (PsIntFuncPtr)ZMAEE_INetMgr_SetApn_Resp);
	OslMsgSendExtQueue(&Message);

	return E_ZM_AEE_WOULDBLOCK;

#endif
}

extern void ZMAEE_INetMgr_GetApn_Resp(void* para)
{

#if(ZM_AEE_MTK_SOFTVERN >= 0x10A0)
	
	srv_dtcnt_prof_gprs_struct* prof_gprs;

	if(ZMAEE_Callback_Valid(&g_zmaee_netmgr.pfnAPN)) 
	{
		ZMAEE_APN_PFN pfn = 0;
		ZMAEE_APN apn;

		prof_gprs = (srv_dtcnt_prof_gprs_struct*)para;

		memset(&apn, 0, sizeof(ZMAEE_APN));
		strncpy(apn.szAPN, (const char*)prof_gprs->APN, sizeof(apn.szAPN));
		mmi_ucs2ncpy(apn.szName, (const char*)prof_gprs->prof_common_header.AccountName, sizeof(apn.szName) / 2 - 1);
		strncpy(apn.szUser, (const char*)prof_gprs->prof_common_header.Auth_info.UserName, sizeof(apn.szUser));
		strncpy(apn.szPasswd, (const char*)prof_gprs->prof_common_header.Auth_info.Passwd, sizeof(apn.szPasswd));

		pfn = (ZMAEE_APN_PFN)g_zmaee_netmgr.pfnAPN.pfn;
		g_zmaee_netmgr.pfnAPN.pfn = 0;
		pfn(1, &apn, g_zmaee_netmgr.pfnAPN.pUser);
	}
	
#else  //ZM_AEE_MTK_SOFTVERN >= 0x10A0

	mmi_ps_get_gprs_data_account_rsp_struct *localPtr;

	ZMAEE_ClearProtocolEventHandler(0, PRT_MMI_PS_GET_GPRS_DATA_ACCOUNT_RSP, (PsIntFuncPtr)ZMAEE_INetMgr_GetApn_Resp);
	if(ZMAEE_Callback_Valid(&g_zmaee_netmgr.pfnAPN))
	{
		ZMAEE_APN_PFN pfn;
		ZMAEE_APN apn;

		localPtr = (mmi_ps_get_gprs_data_account_rsp_struct *)para;
		
		memset(&apn, 0, sizeof(apn));
		
		if(localPtr->result)
		{
			memcpy(apn.szName, (const char*)localPtr->gprs_account.name, sizeof(apn.szName));
			strncpy(apn.szAPN, (const char*)localPtr->gprs_account.apn, sizeof(apn.szAPN));
			strncpy(apn.szUser, (const char*)localPtr->gprs_account.user_name, sizeof(apn.szUser));
			strncpy(apn.szPasswd, (const char*)localPtr->gprs_account.password, sizeof(apn.szPasswd));	
		}
		pfn  = (ZMAEE_APN_PFN)g_zmaee_netmgr.pfnAPN.pfn;
		g_zmaee_netmgr.pfnAPN.pfn = 0;
		pfn(localPtr->result, &apn, g_zmaee_netmgr.pfnAPN.pUser);
	}

#endif  //ZM_AEE_MTK_SOFTVERN >= 0x10A0
}

static int ZMAEE_INetMgr_GetApn( AEE_INetMgr* po, ZMAEE_LINKTYPE type,
									ZMAEE_APN* pApn, ZMAEE_APN_PFN pFn, void* pUser)
{
#if(ZM_AEE_MTK_SOFTVERN >= 0x10A0)

	int i;
	srv_dtcnt_result_enum ret;
	srv_dtcnt_store_prof_data_struct prof_info;
	srv_dtcnt_prof_gprs_struct prof_gprs;
	srv_dtcnt_sim_type_enum dtcnt_sim_type;
	ZMAEE_NETMGR *pThis = (ZMAEE_NETMGR*)po;
	unsigned int *acctId_ptr = (type == ZMAEE_WAP)? &pThis->wap_acct_id: &pThis->net_acct_id;

	if(type == ZMAEE_WIFI) {
		return E_ZM_AEE_BADPARAM;
	}

	if(*acctId_ptr == 0xFFFFFFFF)
		return E_ZM_AEE_FAILURE;

	switch(ZMAEE_GetActiveSimCard()) {
		case 0:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_1; 
			break;

#ifdef __MMI_DUAL_SIM_MASTER__
		case 1:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_2;
			break;
#if (MMI_MAX_SIM_NUM >= 3)
		case 2:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_3;
			break;
#endif

#if (MMI_MAX_SIM_NUM >= 4)
		case 3:
			dtcnt_sim_type = SRV_DTCNT_SIM_TYPE_4;
#endif
#endif

	}

	memset(&prof_gprs, 0, sizeof(prof_gprs));
	prof_gprs.prof_common_header.sim_info = dtcnt_sim_type;
	prof_gprs.prof_common_header.Auth_info.AuthType = SRV_DTCNT_PROF_GPRS_AUTH_TYPE_NORMAL;

	for(i = 0; i < 4; i++) {
		prof_gprs.prof_common_header.PrimaryAddr[i] = (U8)0;
	}
	prof_info.prof_data = (void*)&prof_gprs;
	prof_info.prof_fields = SRV_DTCNT_PROF_FIELD_ALL;
	prof_info.prof_type = SRV_DTCNT_BEARER_GPRS;

	ret = srv_dtcnt_store_qry_prof(*acctId_ptr, &prof_info); 

	ZMAEE_Callback_Init(&g_zmaee_netmgr.pfnAPN, (void*)pFn, pUser);
	
	gui_start_timer_ex(10, ZMAEE_INetMgr_GetApn_Resp, (void*)(ret? &prof_gprs: NULL));

	return E_ZM_AEE_SUCCESS;
	
	
#else  //ZM_AEE_MTK_SOFTVERN >= 0x10A0

	mmi_ps_get_gprs_data_account_req_struct *myMsgPtr;
	MYQUEUE Message;
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;

	if(po == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->pfnAPN.pfn)
		return E_ZM_AEE_ITEMBUSY;

	myMsgPtr = (mmi_ps_get_gprs_data_account_req_struct*) OslConstructDataPtr(sizeof(mmi_ps_get_gprs_data_account_req_struct));
	if(myMsgPtr == NULL)
		return E_ZM_AEE_NOMEMORY;

	myMsgPtr->profile_id = (type==ZMAEE_WAP)?(ZMAEE_GPRS_WAP_PROFILEID):(ZMAEE_GPRS_NET_PROFILEID);
	Message.oslSrcId=MOD_MMI;
	Message.oslDestId=MOD_L4C;
	Message.oslMsgId = PRT_MMI_PS_GET_GPRS_DATA_ACCOUNT_REQ;
	Message.oslDataPtr = (oslParaType *)myMsgPtr;
	Message.oslPeerBuffPtr= NULL;
	
	ZMAEE_Callback_Init(&pThis->pfnAPN, (void*)pFn, pUser);
	ZMAEE_SetProtocolEventHandler(0, PRT_MMI_PS_GET_GPRS_DATA_ACCOUNT_RSP, (PsIntFuncPtr)ZMAEE_INetMgr_GetApn_Resp);
	OslMsgSendExtQueue(&Message);

	return E_ZM_AEE_WOULDBLOCK;

#endif  //ZM_AEE_MTK_SOFTVERN >= 0x10A0
}

void ZMAEE_INetMgr_GetHostByName_Ind(void* inMsg)
{
	app_soc_get_host_by_name_ind_struct *dns_ind = (app_soc_get_host_by_name_ind_struct *)inMsg;

	if( dns_ind && (dns_ind->request_id == (kal_int32)&g_zmaee_netmgr) && (dns_ind->access_id == g_zmaee_netmgr.dns_request_id))
	{
		ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)&g_zmaee_netmgr;
		
		ZMAEE_ClearProtocolEventHandler(0, MSG_ID_APP_SOC_GET_HOST_BY_NAME_IND, (PsIntFuncPtr)ZMAEE_INetMgr_GetHostByName_Ind);

		if( ZMAEE_Callback_Valid( &pThis->pFnDNS ) )
		{
			zmaddr_32 dwAddr = 0;
			ZMAEE_PFNDNSCB pfn = (ZMAEE_PFNDNSCB)pThis->pFnDNS.pfn;

			if( dns_ind->result )
				memcpy(&dwAddr, dns_ind->addr, sizeof(zmaddr_32));
			pThis->pFnDNS.pfn = 0;

			pfn(pThis->pFnDNS.pUser , dns_ind->result,  dwAddr);
		}
	}
}

static int ZMAEE_INetMgr_GetHostByName( AEE_INetMgr* po, const char* domain, 
												zmaddr_32* dwAddr, ZMAEE_PFNDNSCB pfn, void* pUser)
{
	int result = 0;
	kal_int8 ret;
	kal_uint8 addr_len = 0;
	kal_bool ip_validity = 0;
	kal_uint32 nwk_account_id;
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;
	
	if(po == NULL || dwAddr == NULL)
		return E_ZM_AEE_BADPARAM;

	if(soc_ip_check((kal_char*)domain, (kal_uint8*)dwAddr, &ip_validity) && ip_validity) {
		memset(&pThis->pFnDNS, 0, sizeof(pThis->pFnDNS));
		return E_ZM_AEE_SUCCESS;
	}

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	nwk_account_id = ZMAEE_GPRS_NET_ACCOUNTID;
#else
	nwk_account_id = pThis->net_acct_id;
#endif
	nwk_account_id = ZMAEE_Encode_Data_AccountId(nwk_account_id, ZMAEE_GetActiveSimCard());

	ZMAEE_DebugPrint("ZMAEE_INetMgr_GetHostByName: domain = %s", domain);
	pThis->dns_request_id++;
	ret = soc_gethostbyname(MMI_FALSE, MOD_MMI, (kal_int32)pThis, domain, 
								(kal_uint8*) dwAddr, &addr_len, pThis->dns_request_id, nwk_account_id);
	ZMAEE_DebugPrint("ZMAEE_INetMgr_GetHostByName: ret = %d, dwAddr = %x, addr_len = %d", ret, *dwAddr, addr_len);
	if( ret == SOC_SUCCESS )
	{
		ZMAEE_DebugPrint("ZMAEE_INetMgr_GetHostByName: dwAddr = %x", *dwAddr);
		result =  E_ZM_AEE_SUCCESS;
	}
	else if(ret == SOC_WOULDBLOCK)
	{
		ZMAEE_Callback_Init(&pThis->pFnDNS, (void*)pfn, pUser);
		
		ZMAEE_ClearProtocolEventHandler(0, MSG_ID_APP_SOC_GET_HOST_BY_NAME_IND, (PsIntFuncPtr)ZMAEE_INetMgr_GetHostByName_Ind);
		ZMAEE_SetProtocolEventHandler(0, MSG_ID_APP_SOC_GET_HOST_BY_NAME_IND, (PsIntFuncPtr)ZMAEE_INetMgr_GetHostByName_Ind);
		result =  E_ZM_AEE_WOULDBLOCK;
	}
	else
		result = E_ZM_AEE_FAILURE;

	return result;
}

ZMAEE_SOCKET*	ZMAEE_INetMgr_GetFreeSocket(ZMAEE_NETMGR* pThis)
{
	int i;

	for(i = 0; i < sizeof(pThis->szSockets)/sizeof(pThis->szSockets[0]); ++i)
	{
		if(pThis->szSockets[i].nSockId < 0)
			return &pThis->szSockets[i];
	}

	return NULL;
}
void ZMAEE_INetMgr_InitNetworkResp(int result, ZMAEE_APN* pApn, void* pUser)
{
}

static AEE_ISocket* ZMAEE_INetMgr_OpenSocket(AEE_INetMgr* po, ZMAEE_LINKTYPE lType, ZMAEE_SOCKTYPE sType)
{
	ZMAEE_NETMGR* pThis = (ZMAEE_NETMGR*)po;
	kal_int8 sockid,result;
	kal_uint32 nwk_account_id;
	socket_type_enum ste;
	ZMAEE_SOCKET* pSocket;
	kal_uint8 option;
	
	if( po == NULL || lType<ZMAEE_WAP  || lType >ZMAEE_WIFI || sType < ZMAEE_SOCK_STREAM || sType > ZMAEE_SOCK_DGRAM) {
		ZMAEE_DebugPrint("ZMAEE_INetMgr_OpenSocket: Invalid Parameter");
		return NULL;
	}

	//设置上网账号
	//if( pThis->nLinkStat[lType] != ZMAEE_DATALINK_CONNECTED)
	{
		ZMAEE_APN apn = { 0 };
		
		if(lType != ZMAEE_WIFI) {
			if(0==ZMAEE_NetMgr_GetApnCnf(lType, &apn))
				return NULL;
		}
		ZMAEE_INetMgr_SetApn(po, lType, &apn, ZMAEE_INetMgr_InitNetworkResp, NULL);
	}

#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	nwk_account_id = (lType==ZMAEE_WAP)?(ZMAEE_GPRS_WAP_ACCOUNTID):(ZMAEE_GPRS_NET_ACCOUNTID);
#else
	nwk_account_id = (lType == ZMAEE_WAP)? pThis->wap_acct_id: pThis->net_acct_id;
#endif
	nwk_account_id = ZMAEE_Encode_Data_AccountId(nwk_account_id, ZMAEE_GetActiveSimCard());


	#if (ZM_AEE_MTK_SOFTVERN >=  0x08A0)
		ste = (sType==ZMAEE_SOCK_STREAM)?(SOC_SOCK_STREAM):(SOC_SOCK_DGRAM);
	#else
		ste = (sType==ZMAEE_SOCK_STREAM)?(SOCK_STREAM):(SOCK_DGRAM);
	#endif//ZM_AEE_MTK_SOFTVERN

	pSocket = ZMAEE_INetMgr_GetFreeSocket(pThis);
	if(pSocket == NULL) {
		ZMAEE_DebugPrint("ZMAEE_INetMgr_OpenSocket: Get Free Socket failed");
		return NULL;
	}

	sockid = soc_create(0, ste, 0, MOD_MMI, nwk_account_id);
	if(sockid < 0)	{
		ZMAEE_DebugPrint("ZMAEE_INetMgr_OpenSocket: soc_create failed, sockid = %d", sockid);
		return NULL;
	}

	option = SOC_READ | SOC_WRITE | SOC_CONNECT | SOC_CLOSE;
	result = soc_setsockopt(sockid, SOC_ASYNC, &option, sizeof(kal_uint8));
	
	option = KAL_TRUE;
	result = soc_setsockopt(sockid, SOC_NBIO, &option, sizeof(option));

	pSocket->nSockId = sockid;
	pSocket->nRef = 1;
	pSocket->nLinkType = lType;
	pSocket->nStat = 0;

	return (AEE_ISocket*)pSocket;
} 
 
static AEE_IHttp* ZMAEE_INetMgr_OpenHttp(AEE_INetMgr* po, ZMAEE_LINKTYPE type, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree) 
{
	extern int ZMAEE_IHttp_New(ZM_AEECLSID clsId, void** pObj,int linkType, AEE_ISocket* pSocket, AEE_PFN_MALLOC pMalloc, AEE_PFN_FREE pFree);
	AEE_IHttp* pHttp = 0;
	AEE_ISocket* pSocket;
	pSocket = ZMAEE_INetMgr_OpenSocket(po, type, ZMAEE_SOCK_STREAM);
	if(pSocket == NULL) {
		ZMAEE_DebugPrint("ZMAEE_INetMgr_OpenHttp: Open Socket failed");
		return NULL;
	}

	if(E_ZM_AEE_SUCCESS == ZMAEE_IHttp_New(ZM_AEE_CLSID_HTTP, (void**)&pHttp, type, pSocket, pMalloc, pFree))
		return pHttp;
	
	ZMAEE_ISocket_Release(pSocket);
	ZMAEE_DebugPrint("ZMAEE_INetMgr_OpenHttp: ZMAEE_IHttp_New Failed");
	return  NULL;
}

/**
  * SOCKET 接口
  */
static int 	ZMAEE_ISocket_AddRef(AEE_ISocket* po)
{
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;
	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

static int 	ZMAEE_ISocket_Release(AEE_ISocket* po)
{
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;
	pThis->nRef--;
	if(pThis->nRef <= 0)
	{
		soc_shutdown((kal_int8)pThis->nSockId, SHUT_RDWR);
		soc_close((kal_int8)pThis->nSockId);
		pThis->nSockId = -1;
		pThis->pfnConnect.pfn = 0;
		pThis->pfnRead.pfn = 0;
		pThis->pfnWrite.pfn = 0;
		
	}
	return E_ZM_AEE_SUCCESS;
}

static int 	ZMAEE_ISocket_Read(AEE_ISocket * po, void * pDest, unsigned long nWant)
{
	int nRetRead=0;
	kal_int32 ret;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->nStat != 2)
		return 0;

	ret = soc_recv(pThis->nSockId, pDest, (kal_int32)nWant, 0);
	if(ret > 0)
	{
		nRetRead = ret;
	}
	else if(ret == SOC_WOULDBLOCK)
	{
		nRetRead = E_ZM_AEE_WOULDBLOCK;
	}
	else
	{
		nRetRead = E_ZM_AEE_FAILURE;
	}

	return nRetRead;

}


static int 	ZMAEE_ISocket_Write(AEE_ISocket * po, void * pDest, unsigned long nWant)
{
	int nRetWrite=0;
	kal_int32 ret;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	if(pThis->nStat != 2)
		return 0;
	
	ret = soc_send(pThis->nSockId, pDest, (kal_int32)nWant, 0);
	if(ret > 0)
	{
		nRetWrite = ret;
	}
	else if(ret == SOC_WOULDBLOCK)
	{
		nRetWrite = E_ZM_AEE_WOULDBLOCK;
	}
	else
	{
		nRetWrite = E_ZM_AEE_FAILURE;
	}

	return nRetWrite;
	
}

static void ZMAEE_ISocket_Readable(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void * pUser)
{
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return;
	ZMAEE_Callback_Init(&pThis->pfnRead, (void*)pfn, pUser);
}

static void ZMAEE_ISocket_Writeable(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void* pUser)
{
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return;
	ZMAEE_Callback_Init(&pThis->pfnWrite, (void*)pfn, pUser);
}

static void ZMAEE_ISocket_Cancel(AEE_ISocket * po, AEE_PFN_NOTIFY pfn, void * pUser)
{
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return;
	
	if(pThis->pfnRead.pfn == (void*)pfn)
		pThis->pfnRead.pfn  = 0;
	else if(pThis->pfnWrite.pfn == (void*)pfn)
		pThis->pfnWrite.pfn = 0;
}

static int 	ZMAEE_ISocket_Connect(AEE_ISocket* po, zmaddr_32 dwAddr, zmport_16 wPort, 
										ZMAEE_PFNCONNNECTCB pfn, void* pUser)
{
	kal_int8 ret;
	int nRetCon=0;
	sockaddr_struct _sockaddr;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	ZMAEE_NetMgr_LocalizeGateway(pThis->nLinkType, &dwAddr, &wPort);
	ZMAEE_Callback_Init(&pThis->pfnConnect, (void*)pfn, pUser);
	ZMAEE_DebugPrint("ZMAEE_ISocket_Connect: dwAddr = 0x%08x, wPort = 0x%08x", dwAddr, wPort);

	memset(&_sockaddr, 0, sizeof(_sockaddr));
	_sockaddr.addr_len = 4;
	memcpy(_sockaddr.addr, &dwAddr, sizeof(dwAddr));
	_sockaddr.port = wPort;
	
	if(pThis->nStat == 2 || pThis->nStat == 1)
		return E_ZM_AEE_EXIST;

	ret = soc_connect(pThis->nSockId, &_sockaddr);
	ZMAEE_DebugPrint("ZMAEE_ISocket_Connect: ret = %d", ret);
	if( ret == SOC_SUCCESS)
	{
		nRetCon = E_ZM_AEE_SUCCESS;
		pThis->nStat = 2;
	}
	else if(ret == SOC_WOULDBLOCK)
	{
		pThis->nStat = 1;
		nRetCon = E_ZM_AEE_WOULDBLOCK;
	}
	else 
	{
		pThis->nStat = 0;
		nRetCon = E_ZM_AEE_FAILURE;
	}

	return nRetCon;
}

static int 	ZMAEE_ISocket_Bind(AEE_ISocket* po, zmaddr_32 dwAddr, zmport_16 port)
{
	kal_int8 ret;
	int nRetCon=0;
	sockaddr_struct _sockaddr;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	memset(&_sockaddr, 0, sizeof(_sockaddr));
	_sockaddr.addr_len = 4;
	memcpy(_sockaddr.addr, &dwAddr, sizeof(dwAddr));
	_sockaddr.port = port;
	
	ret = soc_bind(pThis->nSockId, &_sockaddr);
	if(ret == SOC_SUCCESS)
		nRetCon = E_ZM_AEE_SUCCESS;
	else
		nRetCon = E_ZM_AEE_FAILURE;

	return nRetCon;
}


static int 	ZMAEE_ISocket_SendTo(AEE_ISocket* po, void* pBuffer, int wBytes, zmaddr_32 dwAddr, zmport_16 wPort)
{
	kal_int32 ret;
	int nRetCon=0;
	sockaddr_struct _sockaddr;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	memset(&_sockaddr, 0, sizeof(_sockaddr));
	_sockaddr.addr_len = 4;
	memcpy(_sockaddr.addr, &dwAddr, sizeof(dwAddr));
	_sockaddr.port = wPort;

	ret = soc_sendto(pThis->nSockId, pBuffer, wBytes, 0, &_sockaddr);
	if(ret > 0)
		nRetCon = ret;
	else if(ret == SOC_WOULDBLOCK)
		nRetCon = E_ZM_AEE_WOULDBLOCK;
	else
		nRetCon = E_ZM_AEE_FAILURE;

	return nRetCon;

}

static int 	ZMAEE_ISocket_RecvFrom(AEE_ISocket* po, void* pBuff, int wBytes, zmaddr_32 *dwAddr, zmport_16* wPort)
{
	kal_int32 ret;
	int nRetCon=0;
	sockaddr_struct _sockaddr;
	ZMAEE_SOCKET* pThis = (ZMAEE_SOCKET*)po;
	if(pThis == NULL)
		return E_ZM_AEE_BADPARAM;

	ret = soc_recvfrom(pThis->nSockId, pBuff, wBytes, 0, &_sockaddr);
	if(ret > 0)
	{
		memcpy(dwAddr, _sockaddr.addr, sizeof(zmaddr_32));
		*wPort = _sockaddr.port;
		nRetCon = ret;
	}
	else if(ret == SOC_WOULDBLOCK)
		nRetCon = E_ZM_AEE_WOULDBLOCK;
	else
		nRetCon = E_ZM_AEE_FAILURE;

	return nRetCon;
}


int ZMAEE_INetMgr_EventHandle(void* inMsg)
{
	int i;
	app_soc_notify_ind_struct * soc_ind = (app_soc_notify_ind_struct *)inMsg;
	ZMAEE_NETMGR* pThis = &g_zmaee_netmgr;
	ZMAEE_SOCKET* pSocket = 0;
	
	if(soc_ind == NULL)
		return 0;

	ZMAEE_DebugPrint("ZMAEE_INetMgr_EventHandle: err_cause = %d, result = %d", soc_ind->error_cause,
					soc_ind->result);	
	ZMAEE_DebugPrint("ZMAEE_INetMgr_EventHandle: detail_cause = %d", soc_ind->detail_cause);

	for(i = 0; i < sizeof(pThis->szSockets)/sizeof(pThis->szSockets[0]); ++i)
	{
		ZMAEE_DebugPrint("ZMAEE_INetMgr_EventHandle: soc_ind->id = %d, szSoc[%d].id = %d", soc_ind->socket_id,
			i, pThis->szSockets[i].nSockId);
		if(pThis->szSockets[i].nSockId >= 0 && pThis->szSockets[i].nSockId == soc_ind->socket_id)
			break;
	}

	if(i == sizeof(pThis->szSockets)/sizeof(pThis->szSockets[0]))
		return 0;

	pSocket = pThis->szSockets + i;

	switch( soc_ind->event_type )
	{
	case SOC_CONNECT:
		{
			if(ZMAEE_Callback_Valid(&pSocket->pfnConnect))
			{
				ZMAEE_PFNCONNNECTCB pfn = (ZMAEE_PFNCONNNECTCB)pSocket->pfnConnect.pfn;

				if(soc_ind->result)
					pSocket->nStat = 2;
				else
					pSocket->nStat = 0;

				pfn(pSocket->pfnConnect.pUser, soc_ind->result);
			}
		}
		break;
	case SOC_READ:
		{
			if(ZMAEE_Callback_Valid(&pSocket->pfnRead))
			{
				AEE_PFN_NOTIFY pfn = (AEE_PFN_NOTIFY)pSocket->pfnRead.pfn;
				pfn(pSocket->pfnRead.pUser);
			}
		}
		break;
	case SOC_WRITE:
		{
			if(ZMAEE_Callback_Valid(&pSocket->pfnWrite))
			{
				AEE_PFN_NOTIFY pfn = (AEE_PFN_NOTIFY)pSocket->pfnWrite.pfn;
				pfn(pSocket->pfnWrite.pUser);
			}
		}
		break;
	case SOC_CLOSE:
		{
			pSocket->nStat = 3;
			if(ZMAEE_Callback_Valid(&pSocket->pfnRead))
			{
				AEE_PFN_NOTIFY pfn = (AEE_PFN_NOTIFY)pSocket->pfnRead.pfn;
				pfn(pSocket->pfnRead.pUser);
			}
		}
		break;
	}

	return 1;
}

/**
 * 查询设置是否支持wifi，从平台版本号105开始支持该接口
 * RETURN:
 * 	E_ZM_AEE_TRUE		支持wifi
 * 	E_ZM_AEE_FALSE		不支持wifi
 */
int ZMAEE_INetMgr_SupportWifi(AEE_INetMgr *po)
{
#ifdef __MMI_WLAN_FEATURES__
	return E_ZM_AEE_TRUE;
#endif

	return E_ZM_AEE_FALSE;
}


#endif
