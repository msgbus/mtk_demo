#ifdef __ZMAEE_APP__

#include "MMIDataType.h"
#include "MMI_features.h"

#include "kal_release.h"

#include "Unicodexdcl.h"

#include "fs_type.h"
#include "fs_func.h"
#include "fs_errcode.h"

#include "zmaee_priv.h"

#include "med_utility.h"
#include "app_mem.h"


extern void 	ZMAEE_DebugPrint(const char *pszFormat, ...);
extern void*	zmaee_memset(void * dest,char c,unsigned int count);
extern int		zmaee_strcmp(const char * string1,const char * string2);


#define 		ZMAEE_IFILE_FUNC_COUNT	10

static int 		ZMAEE_IFile_AddRef(AEE_IFile* po);
int 			ZMAEE_IFile_Release(AEE_IFile* po);
zm_extern int 	ZMAEE_IFile_Read(AEE_IFile * po, void * pDest, unsigned long nWant);
zm_extern int 	ZMAEE_IFile_Write(AEE_IFile * po, void * pSrc, unsigned long nWant);
static void 	ZMAEE_IFile_Readable(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void * pUser);
static void 	ZMAEE_IFile_Writeable(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void* pUser);
static void 	ZMAEE_IFile_Cancel(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void * pUser);
static int 		ZMAEE_IFile_Flush(AEE_IFile* po);
static int 		ZMAEE_IFile_Seek(AEE_IFile* po, int origin, int pos);
static int 		ZMAEE_IFile_Tell(AEE_IFile* po);

zm_extern AEE_IFile* 	AEE_IFileMgr_OpenResFile(AEE_IFileMgr* po, const char* pszFile, int mode);
zm_extern int 			AEE_IFileMgr_GetResInfo(AEE_IFileMgr* po, const char* pszFile, ZMAEE_FileInfo* pfi);
zm_extern int 			AEE_IFileMgr_ResTest(AEE_IFileMgr* po, const char* pszFile);


typedef struct
{
	void	*pVtbl;
	int		bValid;		// 0 - invalid, 1 - valid
	int     nRef;
	int	 	nHandle;
}ZMAEE_FILE;


static ZMAEE_FILE* ZMAEE_IFileMgr_GetFreeFHDR(void);


int  ZMAEE_IFile_New(ZM_AEECLSID clsId, void** pObj, int nHandle )
{
	ZMAEE_FILE* pThis = 0;

	pThis = ZMAEE_IFileMgr_GetFreeFHDR();	
	if(pThis == 0) return E_ZM_AEE_NOMEMORY;

	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IFILE_FUNC_COUNT,
				ZMAEE_IFile_AddRef,
				ZMAEE_IFile_Release,
				ZMAEE_IFile_Read,
				ZMAEE_IFile_Write,
				ZMAEE_IFile_Readable,
				ZMAEE_IFile_Writeable,
				ZMAEE_IFile_Cancel,
				ZMAEE_IFile_Flush,
				ZMAEE_IFile_Seek,
				ZMAEE_IFile_Tell);
		}
	}

	pThis->nRef = 1;
	pThis->nHandle = nHandle;

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
	
}

int 	ZMAEE_IFile_AddRef(AEE_IFile* po)
{
	ZMAEE_FILE* pThis = (ZMAEE_FILE*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef++;

	return E_ZM_AEE_SUCCESS;
	
}

int 	ZMAEE_IFile_Release(AEE_IFile* po)
{
	ZMAEE_FILE* pThis = (ZMAEE_FILE*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;
	if(pThis->nRef == 0)
	{
		FS_Close((FS_HANDLE)pThis->nHandle);
		pThis->nHandle = -1;
		pThis->bValid = 0;
	}
	return E_ZM_AEE_SUCCESS;
}

int 	ZMAEE_IFile_Read(AEE_IFile * po, void * pDest, unsigned long nWant)
{
	int nErr;
	unsigned int dwBytesRead = 0;
	int nBlock = 1024 * 10;
	int nRever = nWant;
	int nHasRead = 0;
	unsigned int nPos;
	unsigned int nFileSize = 0;
	ZMAEE_FILE *pThis = (ZMAEE_FILE*)po;
	
	char* tmpBuf = 0;
	
	if(pThis == NULL) {
		return E_ZM_AEE_FAILURE;
	}

	
	if(FS_GetFileSize(pThis->nHandle, &nFileSize) != 0)
		return E_ZM_AEE_FAILURE;
	
	if(nFileSize == 0 || FS_GetFilePosition(pThis->nHandle, &nPos) != 0)
		return E_ZM_AEE_FAILURE;
	
	if(((unsigned long)(pDest) & 0xF0000000) == 0xF0000000)
		tmpBuf = (char*)med_alloc_ext_mem_framebuffer(nBlock + 4);
	
	while((nRever > 0) && (nPos < nFileSize))
	{
		int nRead = (nRever > nBlock)? nBlock: nRever;
		
		if(tmpBuf)
		{
			nErr = FS_Read(pThis->nHandle, tmpBuf, nRead, &dwBytesRead);
			if(nErr == 0 && dwBytesRead > 0)
				memcpy(((char*)pDest + nHasRead), tmpBuf, dwBytesRead);
		}
		else
		{
			nErr = FS_Read(pThis->nHandle, ((char*)pDest + nHasRead), nRead, &dwBytesRead);
		}
		
		if(nErr != 0) {
			if(tmpBuf) {med_free_ext_mem((void**)&tmpBuf);}
			return 0;
		}
		if(dwBytesRead == 0) {
			if(tmpBuf) {med_free_ext_mem((void**)&tmpBuf);}
			return nHasRead;
		}
		
		nHasRead += dwBytesRead;
		nRever -= dwBytesRead;
	}
	if(tmpBuf) {med_free_ext_mem((void**)&tmpBuf);}
	return nHasRead;
}


int 	ZMAEE_IFile_Write(AEE_IFile * po, void * pSrc, unsigned long nWant)
{
	int nErr;
	unsigned int dwBytesWritten = 0;
	int  nBlock = 1024 * 10;
	int  nTotal = nWant;
	int  nRever = nTotal;
	int  nHasWrite = 0;
	ZMAEE_FILE *pThis = (ZMAEE_FILE*)po;
	char* tmpBuf = 0;

	if(pThis == NULL) {
		return E_ZM_AEE_FAILURE;
	}

	if(((unsigned long)(pSrc) & 0xF0000000) == 0xF0000000) 
		tmpBuf = (char*)med_alloc_ext_mem_framebuffer(nBlock + 4);
	
	while(nRever > 0)
	{
		int nWrite = (nRever > nBlock)? nBlock: nRever;
		
		if(tmpBuf)
		{
			memcpy(tmpBuf, ((char*)pSrc+nHasWrite), nWrite);
			nErr = FS_Write(pThis->nHandle,((char*)tmpBuf), nWrite, &dwBytesWritten);
		} else {
			nErr = FS_Write(pThis->nHandle,((char*)pSrc+nHasWrite), nWrite, &dwBytesWritten);
		}

		if(nErr != 0) {
			ZMAEE_IFile_Flush(po);
			if(tmpBuf) {med_free_ext_mem((void**)&tmpBuf);}
			return 0;
		}
		
		nHasWrite += dwBytesWritten;
		nRever -= dwBytesWritten;
	} 

	if(tmpBuf) {med_free_ext_mem((void**)&tmpBuf);}
	ZMAEE_IFile_Flush(po);
	return nHasWrite;
}


void ZMAEE_IFile_Readable(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void* pUser)
{
}

void ZMAEE_IFile_Writeable(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void* pUser)
{
}

void ZMAEE_IFile_Cancel(AEE_IFile * po, AEE_PFN_NOTIFY pfn, void * pUser)
{	
}

/**
 * this funtion flush cache memory to disk
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful.
 */
static
int ZMAEE_IFile_Flush(AEE_IFile* po)
{
	int nRet;
	ZMAEE_FILE* pThis = (ZMAEE_FILE*)po;
	if(pThis == NULL)
		return E_ZM_AEE_FAILURE;

	nRet = FS_Commit((FS_HANDLE)pThis->nHandle);
	return ((nRet==FS_NO_ERROR)? E_ZM_AEE_SUCCESS: E_ZM_AEE_FAILURE);
}

/**
 * This function moves the file pointer of the IFile Interface object a given distance from a specified origin. 
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful.
 */
static
int ZMAEE_IFile_Seek(AEE_IFile* po, int origin, int pos)
{
	int whence = 0;
	int nRet;
	ZMAEE_FILE* pThis = (ZMAEE_FILE*)po;
	if(pThis == NULL)
		return E_ZM_AEE_FAILURE;
	
	switch(origin)
	{
		case ZM_SEEK_START:
			whence = FS_FILE_BEGIN;
			break;
		case ZM_SEEK_END:
			whence = FS_FILE_END;
			break;
		case ZM_SEEK_CURRENT:
			whence = FS_FILE_CURRENT;
			break;
		default:
			return E_ZM_AEE_BADPARAM;
	}

	nRet = FS_Seek((FS_HANDLE)pThis->nHandle, pos, whence);

	return ((nRet >= 0)? E_ZM_AEE_SUCCESS: E_ZM_AEE_FAILURE);	
}

/**
 * This function  return file pointer position of the IFile object 
 * RETURN:
	 file pointer position, if successful. 
	 E_ZM_AEE_FAILURE, if unsuccessful. 
 */
static
int ZMAEE_IFile_Tell(AEE_IFile* po)
{
	UINT pos = 0;
	int nRet;
	ZMAEE_FILE* pThis = (ZMAEE_FILE*)po;
	if(pThis == 0)
		return E_ZM_AEE_FAILURE;

	nRet = FS_GetFilePosition((FS_HANDLE)pThis->nHandle, &pos);

	return ((nRet == FS_NO_ERROR)? (int)pos: E_ZM_AEE_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#define 				ZMAEE_IFILEMGR_FUNC_COUNT	15

static int 				ZMAEE_IFileMgr_AddRef(AEE_IFileMgr* po);
int 					ZMAEE_IFileMgr_Release(AEE_IFileMgr* po);
AEE_IFile* 				ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode);
zm_extern int 			ZMAEE_IFileMgr_GetInfo(AEE_IFileMgr* po, const char* pszFile, ZMAEE_FileInfo* pfi);
static int 				ZMAEE_IFileMgr_Remove(AEE_IFileMgr* po, const char* pszFile);
zm_extern int			ZMAEE_IFileMgr_MkDir(AEE_IFileMgr* po, const char* pszDir);
zm_extern int 			ZMAEE_IFileMgr_RmDir(AEE_IFileMgr* po, const char* pszDir);
zm_extern int 			ZMAEE_IFileMgr_Rename(AEE_IFileMgr* po, const char* pszSrc, const char* pszaDest);
zm_extern int 			ZMAEE_IFileMgr_Test(AEE_IFileMgr* po, const char* pszFile);
static int 				ZMAEE_IFileMgr_EnumInit(AEE_IFileMgr* po, const char * pszDir);
static int 				ZMAEE_IFileMgr_EnumNext(AEE_IFileMgr* po, ZMAEE_FileInfo* pfi);
static int				ZMAEE_IFileMgr_EnumEnd(AEE_IFileMgr* po);
zm_extern char 			ZMAEE_IFileMgr_GetDriver(AEE_IFileMgr* po, int type);
unsigned long 	ZMAEE_IFileMgr_GetFreeSpace(AEE_IFileMgr* po, char driver);
static int 				ZMAEE_IFileMgr_Hide(AEE_IFileMgr *po, const char *pszFile, int bHide);
extern int 				ZMAEE_Utf8_2_Ucs2(const char* , int , unsigned short* , int);
extern int 				ZMAEE_Ucs2_2_Utf8(const unsigned short * wcsSrc,int Size,char * pDst,int nSize);


typedef struct stZMAEE_FileMgr
{
	void			*pVtbl;
	int 			nRef;
	int 			nSearchHandle;
	int 			flag;
	char 			utf8_dir_path[AEE_MAX_PATH_NAME];
	ZMAEE_FILE		file_handler[AEE_MAX_FILE_HANDLE];
}ZMAEE_FILEMGR;

static ZMAEE_FILEMGR	sg_zmaee_filemgr = {0};


static
ZMAEE_FILE* ZMAEE_IFileMgr_GetFreeFHDR(void)
{
	int i;

	for(i = 0; i < AEE_MAX_FILE_HANDLE; i++) {
		if(sg_zmaee_filemgr.file_handler[i].bValid == 0) {
			sg_zmaee_filemgr.file_handler[i].bValid = 1;
			return &(sg_zmaee_filemgr.file_handler[i]);
		}
	}

	return NULL;
}

int ZMAEE_IFileMgr_New(ZM_AEECLSID clsId, void** pObj) 
{
	ZMAEE_FILEMGR*  pThis = 0;

	pThis = &sg_zmaee_filemgr;
	
	if(!pThis->pVtbl) {
		pThis->pVtbl = ZMAEE_GetVtable(clsId);
		if(pThis->pVtbl == 0)
		{
			pThis->pVtbl = ZMAEE_CreateVtable(clsId, ZMAEE_IFILEMGR_FUNC_COUNT,
				ZMAEE_IFileMgr_AddRef,
				ZMAEE_IFileMgr_Release,
				ZMAEE_IFileMgr_OpenFile,
				ZMAEE_IFileMgr_GetInfo,
				ZMAEE_IFileMgr_Remove,
				ZMAEE_IFileMgr_MkDir,
				ZMAEE_IFileMgr_RmDir,
				ZMAEE_IFileMgr_Rename,
				ZMAEE_IFileMgr_Test,
				ZMAEE_IFileMgr_EnumInit,
				ZMAEE_IFileMgr_EnumNext,
				ZMAEE_IFileMgr_EnumEnd,
				ZMAEE_IFileMgr_GetDriver,
				ZMAEE_IFileMgr_GetFreeSpace,
				ZMAEE_IFileMgr_Hide);
		}
	}

	if(pThis->nRef == 0) {
		pThis->nSearchHandle = -1;
		pThis->nRef = 1;
	} else {
		pThis->nRef++;
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
}


int ZMAEE_IFileMgr_AddRef(AEE_IFileMgr* po)
{
	ZMAEE_FILEMGR* pThis = (ZMAEE_FILEMGR*)po;
	if(pThis == 0 )
		return E_ZM_AEE_BADPARAM;
	pThis->nRef++;
	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_IFileMgr_Release(AEE_IFileMgr* po)
{
	ZMAEE_FILEMGR* pThis = (ZMAEE_FILEMGR*)po;
	if(pThis == 0 )
		return E_ZM_AEE_BADPARAM;

	pThis->nRef--;

	if(pThis->nRef == 0)
	{
		if(pThis->nSearchHandle >= 0)
		{
			FS_FindClose(pThis->nSearchHandle);
			pThis->nSearchHandle = -1;
		}
	}

	return E_ZM_AEE_SUCCESS;
}

unsigned short* ZMAEE_IFileMgr_ConvertFileName(const char* pszFile)
{	
	static unsigned short wcsFile[AEE_MAX_PATH_NAME] = {0};
	memset(wcsFile, 0, sizeof(wcsFile));
	ZMAEE_Utf8_2_Ucs2(pszFile, strlen(pszFile), wcsFile, sizeof(wcsFile)/sizeof(wcsFile[0]));
	return wcsFile;
}

AEE_IFile* ZMAEE_IFileMgr_OpenFile(AEE_IFileMgr* po, const char* pszFile, int mode)
{
	int nHandle = 0;
	UINT nFlag = 0;
	AEE_IFile* pFile=0;
	
	ZMAEE_FILEMGR* pThis = (ZMAEE_FILEMGR*)po;

	if(pThis == 0 ) {
		return NULL;
	}

	if((ZM_OFM_READONLY & mode) == ZM_OFM_READONLY)
	{
		nFlag = FS_READ_ONLY;
	}
	else if(((ZM_OFM_READWRITE & mode) == ZM_OFM_READWRITE) || ((ZM_OFM_APPEND&mode) == ZM_OFM_APPEND) || 
			((ZM_OFM_TRUNCATE & mode) == ZM_OFM_TRUNCATE))
	{
		nFlag = FS_READ_WRITE;
		if((ZM_OFM_TRUNCATE & mode) == ZM_OFM_TRUNCATE)
			nFlag |= FS_CREATE_ALWAYS;
		else if((ZM_OFM_CREATE & mode) == ZM_OFM_CREATE)
			nFlag |= FS_CREATE;
	}
	else { 
		ZMAEE_DebugPrint("ZMAEE_IFileMgr_OpenFile: bad flag, mode = %d", mode);
		return NULL;
	}

	if((ZM_OFM_COMMITED & mode) == ZM_OFM_COMMITED) {
		nFlag |= FS_COMMITTED;
	}

	if((ZM_OFM_OPENSHARED & mode) == ZM_OFM_OPENSHARED) {
		nFlag |= FS_OPEN_SHARED;
	}

	nHandle = FS_Open(ZMAEE_IFileMgr_ConvertFileName(pszFile), nFlag);
	if( nHandle > 0 )
	{
		if((ZM_OFM_APPEND&mode) == ZM_OFM_APPEND)
			FS_Seek(nHandle, 0, FS_FILE_END);
		ZMAEE_IFile_New(ZM_AEE_CLSID_FILE,  (void**)&pFile,  nHandle);
	}

	if(!pFile) {
		pFile = AEE_IFileMgr_OpenResFile(po, pszFile, mode);
	}

	return pFile;
}


/**
  * This function gets the information on a file identified by a filename. 
	The file information this function retrieves contains the file attributes, file creation date, and file size. 
  * @pszName  [in]	NULL-terminated string containing the filename. UTF8 format
  * @pfi  [out]  Placeholder for file information.	
  * RETURN :
	  E_ZM_AEE_SUCCESS: if successful. 
	  E_ZM_AEE_FAILURE: if unsuccessful. 
  */
int ZMAEE_IFileMgr_GetInfo(AEE_IFileMgr* po, const char* pszFile, ZMAEE_FileInfo* pfi)
{
	int nHandle = 0;
	int ret = E_ZM_AEE_FAILURE;
	FS_FileInfo npfi = {0};
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	if(pThis == 0||pfi == 0) return E_ZM_AEE_FAILURE;	
	
	nHandle = FS_Open(ZMAEE_IFileMgr_ConvertFileName(pszFile), FS_READ_ONLY);
	if(nHandle > 0)
	{
		ret = FS_GetFileInfo(nHandle, &npfi);
		if(ret == FS_NO_ERROR) 
		{
            if(npfi.DirEntry->Attributes & FS_ATTR_HIDDEN)
	            pfi->dwAttr |= ZM_FA_HIDDEN;
			
            if(npfi.DirEntry->Attributes & FS_ATTR_DIR)
           		pfi->dwAttr |= ZM_FA_DIR;
			
            if(npfi.DirEntry->Attributes & FS_ATTR_READ_ONLY)
           		pfi->dwAttr |= ZM_FA_READONLY;
			
            if(npfi.DirEntry->Attributes & FS_ATTR_SYSTEM)
           		pfi->dwAttr |= ZM_FA_SYSTEM;	
			
            if(npfi.DirEntry->Attributes & FS_ATTR_ARCHIVE)
            	pfi->dwAttr |= ZM_FA_NORMAL;
			
			memcpy((void*)&pfi->dwCreateDate,(void *)&(npfi.DirEntry->CreateDateTime),sizeof(npfi.DirEntry->CreateDateTime));
			pfi->dwSize = (unsigned long)npfi.DirEntry->FileSize;
					
			ZMAEE_Ucs2_2_Utf8((unsigned short *)npfi.FullName, mmi_ucs2strlen(npfi.FullName),pfi->szName,AEE_MAX_FILE_NAME);
			
		}
	
		FS_Close(nHandle);
		
		if(ret == FS_NO_ERROR)
			ret = E_ZM_AEE_SUCCESS;
		else 
			ret = E_ZM_AEE_FAILURE;
	}

	if(ret == E_ZM_AEE_FAILURE) {	
		ret = AEE_IFileMgr_GetResInfo(po, pszFile, pfi);
	}
	return ret;	
}

/**
 * This function is used to remove a file identified by a given file name. 
 * @pszFile ULL-terminated string containing the filename. UTF8 format.
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
static
int ZMAEE_IFileMgr_Remove(AEE_IFileMgr* po, const char* pszFile)
{
    int	  nRet=0;
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	
	if(pThis == 0 || pszFile == 0) return 0;
    nRet = FS_Delete((WCHAR *)ZMAEE_IFileMgr_ConvertFileName(pszFile));
	if(nRet == FS_NO_ERROR)
		return E_ZM_AEE_SUCCESS;
	else 
		return E_ZM_AEE_FAILURE;
  
} 

/**
 *This function is used to create a directory specified by a directory name. 
 * @pszDir ULL-terminated string containing the dir. UTF8 format.
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 	 
 */
int ZMAEE_IFileMgr_MkDir(AEE_IFileMgr* po, const char* pszDir)
{
	int i;
    int Ret = 0;
	FS_HANDLE hFile;
	S8 strDir[AEE_MAX_FILE_NAME];
	int nLen = strlen( pszDir);
	
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;	
	if(pThis == 0 || pszDir == 0) return E_ZM_AEE_FAILURE;
	
	for(i=3; i < nLen; ++i)
	{
		if((pszDir[i]=='\\') || (i == nLen - 1))
		{	
			unsigned short *ucs2_dir_path;
			memset(strDir, 0, sizeof(strDir));
			memcpy(strDir, pszDir, i+1);

			if(strDir[i] != '\\')
				strcat(strDir, "\\");

			ucs2_dir_path = ZMAEE_IFileMgr_ConvertFileName(strDir);
			hFile = FS_Open((WCHAR *)ucs2_dir_path , FS_OPEN_DIR | FS_READ_ONLY);
		
			if(hFile> 0)
			{
				FS_Close(hFile);
				continue;
			}
			Ret = FS_CreateDir((WCHAR *)ucs2_dir_path);

			kal_prompt_trace(MOD_MMI, "ZMAEE_IFileMgr_MkDir: strDir = %s, hFile = %d, Ret = %d", strDir, hFile, Ret);
		}
	}

	if(Ret == FS_NO_ERROR)
		return E_ZM_AEE_SUCCESS;
	else 
		return E_ZM_AEE_FAILURE;
  
}

/**
 * This function is used to remove a directory identified by a given directory name. 
 * @pszDir ULL-terminated string containing the dir. UTF8 format.
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
int ZMAEE_IFileMgr_RmDir(AEE_IFileMgr* po, const char* pszDir)
{
	int   nRet=0;
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	char *stack_buffer = NULL;
	int stack_size = 10 * 1024;
	
	if(pThis == 0 || pszDir == 0) 
		return E_ZM_AEE_FAILURE;

#ifdef __MED_IN_ASM__
	stack_buffer = (char*)applib_mem_ap_alloc_framebuffer(VAPP_ZMAEEPLAT, stack_size);
#else
	stack_buffer = (char*)med_alloc_ext_mem(stack_size);
#endif
	if(!stack_buffer)
		return E_ZM_AEE_FAILURE;

	nRet = FS_XDelete((WCHAR*)ZMAEE_IFileMgr_ConvertFileName(pszDir), 
			FS_FILE_TYPE | FS_DIR_TYPE | FS_RECURSIVE_TYPE,
			(BYTE*)stack_buffer, stack_size);
#ifdef __MED_IN_ASM__
	applib_mem_ap_free(stack_buffer);
#else
	med_free_ext_mem((void**)&stack_buffer);
#endif

	return E_ZM_AEE_SUCCESS;
}

/**
 * This function renames the file from the source name to the destination name. 
 * @ pszSrc  Source file to be renamed.  
 * @pszDest  Destination file.	
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
int ZMAEE_IFileMgr_Rename(AEE_IFileMgr* po, const char* pszSrc, const char* pszaDest)
{
	int   nRet=0;
	unsigned short* wcsFile = 0;
	unsigned short DestFile[AEE_MAX_PATH_NAME] = {0};
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;	
	if(pThis == 0 || pszSrc == 0 || pszaDest == 0) return E_ZM_AEE_FAILURE;
	wcsFile = ZMAEE_IFileMgr_ConvertFileName(pszSrc);
	ZMAEE_Utf8_2_Ucs2(pszaDest,strlen(pszaDest),DestFile,sizeof(DestFile)/sizeof(DestFile[0]));
	nRet = FS_Rename((WCHAR *)wcsFile,(WCHAR *)DestFile);
	if(nRet == FS_NO_ERROR)
		return E_ZM_AEE_SUCCESS;
	else 
		return E_ZM_AEE_FAILURE;
}

int ZMAEE_IFileMgr_Test_Ext(AEE_IFileMgr* po, const char* pszFile)
{
	int   nRet = 0;
	FS_HANDLE hFile;
	WCHAR *wcsFile;

	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;	
	if(pThis == 0 || pszFile == 0)
		return E_ZM_AEE_FAILURE;

	wcsFile = ZMAEE_IFileMgr_ConvertFileName(pszFile);
#if (ZM_AEE_MTK_SOFTVERN < 0x10A0)
	hFile = FS_Open(wcsFile, FS_OPEN_DIR | FS_READ_ONLY);
#else
	hFile = FS_Open(wcsFile, FS_READ_ONLY);
#endif
	if(hFile > 0) {
		FS_Close(hFile);
		return E_ZM_AEE_SUCCESS;
	} 

	return E_ZM_AEE_FAILURE;
}	

/**
 * This function tests if the specified file or directory exists. 
 * @pszFile ULL-terminated string containing the filename. UTF8 format.
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
int ZMAEE_IFileMgr_Test(AEE_IFileMgr* po, const char* pszFile)
{
	int   nRet = ZMAEE_IFileMgr_Test_Ext(po, pszFile);;

	if(nRet == E_ZM_AEE_SUCCESS)
		return nRet;
	
	return AEE_IFileMgr_ResTest(po, pszFile);
}	


/**
 * EnumInit
 * @pszDir	NULL-terminated string containing the root directory name. 
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
int ZMAEE_IFileMgr_EnumInit(AEE_IFileMgr* po, const char * pszDir)
{		 
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	char last_char;
	if(pThis == 0) 
		return E_ZM_AEE_FAILURE;

	if(pThis->nSearchHandle >= 0) {
		FS_FindClose(pThis->nSearchHandle);
		pThis->nSearchHandle = -1;
	}
	
	memset(pThis->utf8_dir_path, 0, sizeof(pThis->utf8_dir_path));
	memcpy(pThis->utf8_dir_path, pszDir, strlen(pszDir));

	last_char = pThis->utf8_dir_path[strlen(pszDir) - 1];
	if(last_char != '*') {
		if(last_char != '\\') {
			strcat(pThis->utf8_dir_path, "\\*");
		} else {
			strcat(pThis->utf8_dir_path, "*");
		}
	}
		
	pThis->flag = 1;
	
	return E_ZM_AEE_SUCCESS;
}

/**
 * This function returns file information for the next file or directory within the specified root directory
 * @pfi  [out]	Placeholder for file information.  
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
static
int ZMAEE_IFileMgr_EnumNext(AEE_IFileMgr* po, ZMAEE_FileInfo* pfi)
{
	FS_DOSDirEntry file_info={0};
	S16 InBuffer[AEE_MAX_FILE_NAME] ={0};
	int ferrno = 0;
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	if(pThis == 0) 
		return E_ZM_AEE_FAILURE;

EnumNext_Start:	
	zmaee_memset(InBuffer, 0, sizeof(InBuffer));
	if(pThis->flag == 1) 
	{
		ferrno = pThis->nSearchHandle = FS_FindFirst(ZMAEE_IFileMgr_ConvertFileName(pThis->utf8_dir_path), 0, 0 ,&file_info, (WCHAR*)InBuffer, sizeof(InBuffer) / 2);

		ZMAEE_Ucs2_2_Utf8((const unsigned short*)InBuffer, mmi_ucs2strlen((const S8*)InBuffer), pfi->szName,AEE_MAX_FILE_NAME);

		// ����".",".."Ŀ¼
		while(!zmaee_strcmp(pfi->szName, ".") || !zmaee_strcmp(pfi->szName, "..")) {
			pThis->flag = 2;
			goto EnumNext_Start;
		}
		
		if(pThis->nSearchHandle >= 0) 
		{	
			pfi->dwAttr = 0;
			if(file_info.Attributes & FS_ATTR_HIDDEN)
				pfi->dwAttr |= ZM_FA_HIDDEN;
			
            if(file_info.Attributes & FS_ATTR_DIR)
           		pfi->dwAttr |= ZM_FA_DIR;
			
            if(file_info.Attributes & FS_ATTR_READ_ONLY)
           		pfi->dwAttr |= ZM_FA_READONLY;
			
            if(file_info.Attributes & FS_ATTR_SYSTEM)
          		pfi->dwAttr |= ZM_FA_SYSTEM;

			if(pfi->dwAttr == 0)
          		pfi->dwAttr |= ZM_FA_NORMAL;

			memcpy((void*)&pfi->dwCreateDate,(void *)&(file_info.CreateDateTime),sizeof(file_info.CreateDateTime));
			pfi->dwSize = (unsigned long)file_info.FileSize;
			pThis->flag = 2;

			return E_ZM_AEE_SUCCESS;
		}
	}
	else if(pThis->flag == 2)
	{
		ferrno = FS_FindNext(pThis->nSearchHandle, &file_info, (WCHAR*)InBuffer, sizeof(InBuffer) / 2);		
		
		ZMAEE_Ucs2_2_Utf8((const unsigned short*)InBuffer, mmi_ucs2strlen((const S8*)InBuffer), pfi->szName,AEE_MAX_FILE_NAME);

		// ����".",".."Ŀ¼
		while(!zmaee_strcmp(pfi->szName, ".") || !zmaee_strcmp(pfi->szName, "..")) {
			goto EnumNext_Start;
		}

		if(ferrno == FS_NO_ERROR) {
			pfi->dwAttr = 0;
			if(file_info.Attributes & FS_ATTR_HIDDEN)
				pfi->dwAttr |= ZM_FA_HIDDEN;
			
            if(file_info.Attributes & FS_ATTR_DIR)
           		pfi->dwAttr |= ZM_FA_DIR;
			
            if(file_info.Attributes & FS_ATTR_READ_ONLY)
           		pfi->dwAttr |= ZM_FA_READONLY;
			
            if(file_info.Attributes & FS_ATTR_SYSTEM)
          		pfi->dwAttr |= ZM_FA_SYSTEM;

			if(pfi->dwAttr == 0)
          		pfi->dwAttr |= ZM_FA_NORMAL;
			
			memcpy((void*)&pfi->dwCreateDate,(void *)&(file_info.CreateDateTime),sizeof(file_info.CreateDateTime));
			pfi->dwSize = (unsigned long)file_info.FileSize;
			

			return E_ZM_AEE_SUCCESS;
		}
	}
	
	if(ferrno == FS_NO_MORE_FILES) {
		pThis->flag = 3;
	}

	return E_ZM_AEE_FAILURE;
}

/**
 * enum end
 * RETURN :
	 E_ZM_AEE_SUCCESS: if successful. 
	 E_ZM_AEE_FAILURE: if unsuccessful. 
 */
static
int ZMAEE_IFileMgr_EnumEnd(AEE_IFileMgr* po)
{	
	int ferrno = FS_NO_ERROR;
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;	
	if(pThis == 0) return E_ZM_AEE_FAILURE;

	if((pThis->flag == 2) || (pThis->flag == 3))
	{
		ferrno = FS_FindClose(pThis->nSearchHandle);
	} 

	if (ferrno == FS_NO_ERROR) 
	{
		pThis->flag = 0;
		pThis->nSearchHandle = -1;
		return E_ZM_AEE_SUCCESS;
	}
	else 
	{
		return E_ZM_AEE_FAILURE;	
	}
}

char ZMAEE_IFileMgr_GetDriver(AEE_IFileMgr* po, int type)
{	
	int ret_drv;
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;	
	if(pThis == 0) return 0;
	
	if(type == 0)
		{
		ret_drv = FS_GetDrive(FS_DRIVE_V_NORMAL, 2, FS_DRIVE_V_NORMAL | FS_DRIVE_I_SYSTEM);
		if(ret_drv <= 0)
			return 0;
		else 
			return (char)ret_drv;
		}
	else if(type == 1)
	{
		ret_drv = FS_GetDrive(FS_DRIVE_V_REMOVABLE, 1, FS_NO_ALT_DRIVE);
		if(ret_drv > 0) {
			if(FS_GetDevStatus(ret_drv, FS_MOUNT_STATE_ENUM) == FS_NO_ERROR)
				return (char)ret_drv;
		}
		
		return 0;
	}
	else 
		return 0;
}

unsigned long ZMAEE_IFileMgr_GetFreeSpace(AEE_IFileMgr* po, char driver)
{
	U64 ret_free_size=-1;
	unsigned short* wcsFile;
	char dir_name[AEE_MAX_PATH_NAME] = {0};
	
	int  result;
	FS_DiskInfo disc_info;

	dir_name[0] = driver;
	strcat(dir_name, ":\\");
	wcsFile = ZMAEE_IFileMgr_ConvertFileName((char *)&dir_name);
	FS_SetCurrentDir(wcsFile);
	result=FS_GetDiskInfo((WCHAR *)wcsFile, &disc_info, FS_DI_BASIC_INFO|FS_DI_FREE_SPACE);
	
	if( result > 0 && (result & (FS_DI_BASIC_INFO|FS_DI_FREE_SPACE)))
	{
		ret_free_size = (U64)disc_info.FreeClusters*disc_info.SectorsPerCluster*disc_info.BytesPerSector;
	}
	else
	{
		ret_free_size = 0;
	}

	if(ret_free_size > 0x7FFFFFFF)
		ret_free_size = 0x7FFFFFFF;

	return (unsigned long)ret_free_size;//WLJ

}

/**
 * Hide file file/directory or not
 * @ bHide	0 - unhide, 1 - hide
 * RETURN:
 * 	E_ZM_AEE_BADPARAM		po, pszFile is null, bHide is unexpected
 * 	E_ZM_AEE_FAILURE		operation failed
 * 	E_ZM_AEE_SUCCESS		operation success
 */
static
int ZMAEE_IFileMgr_Hide(AEE_IFileMgr *po, const char *pszFile, int bHide)
{
	ZMAEE_FILEMGR *pThis = (ZMAEE_FILEMGR*)po;
	int err, attr;
	unsigned short *ucs2_path;

	if(!pThis || !pszFile) {
		return E_ZM_AEE_BADPARAM;
	}

	ucs2_path = ZMAEE_IFileMgr_ConvertFileName(pszFile);
	attr = FS_GetAttributes(ucs2_path);

	if(bHide) {
		attr |= FS_ATTR_HIDDEN;
	} else {
		attr &= ~(FS_ATTR_HIDDEN);
	}

	err = FS_SetAttributes(ucs2_path, (BYTE)attr);
	if(err != FS_NO_ERROR) {
		return E_ZM_AEE_FAILURE;
	} else {
		return E_ZM_AEE_SUCCESS;
	}
}



#endif	// __ZMAEE_APP__
