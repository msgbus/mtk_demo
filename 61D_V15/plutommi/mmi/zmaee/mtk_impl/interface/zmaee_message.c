#ifdef __ZMAEE_APP__


#include "zmaee_priv.h"

#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
#include "CommonScreens.h"
#include "ProtocolEvents.h"
#include "customer_ps_inc.h"
#include "mmi_msg_context.h"
#include "MessagesL4Def.h"
#include "SmsPsHandler.h"
#include "l4c2smsal_struct.h"

#include "SMSApi.h"
#include "SMSStruct.h"
#include "SMSFunc.h"
#include "SmsAppGprot.h"
#include "SmsAppUtil.h"
#else
#include "MMI_include.h"
#include "Mmi_msg_context.h"
#include "SmsSrvGprot.h"
#endif

#define ZMAEE_IMESSAGE_FUNC_COUNT  5

zm_extern int 		ZMAEE_IMessage_AddRef(AEE_IMessage* po);
zm_extern int 		ZMAEE_IMessage_Release(AEE_IMessage* po);
zm_extern int 		ZMAEE_IMessage_GetSmsNum(AEE_IMessage *po, ZMAEE_SMS_TYPE type);
zm_extern int 		ZMAEE_IMessage_GetSmsContent(AEE_IMessage *po, ZMAEE_SMS_TYPE type, int index, ZMAEE_SMS_RECORD *record, ZMAEE_PFN_READSMS pfn, void *pUser,int change_status);
zm_extern int 		ZMAEE_IMessage_GetSmsMemoryStatus(AEE_IMessage *po, ZMAEE_SMS_MEMORYSTATUS *memory_status, ZMAEE_PFN_GETSMSMEMORY pfn, void *pUser);
static 	  int 		ZMAEE_Sms_Getnum(ZMAEE_SMS_TYPE type);
static    int 		ZMAEE_Sms_GetSim(U16 index);
static    int       ZMAEE_Sms_Get_ListDetails(ZMAEE_SMS_TYPE nType, unsigned short **pListArray, unsigned short *pListSize, int *pMsgboxEnum, int *pSim);
static    int 		ZMAEE_Sms_GetFilterCount(int bReadStatus, int nSim, unsigned short *pListArray, unsigned short nListSize);
static	  int 		ZMAEE_IMessage_CheckBoundary(ZMAEE_SMS_TYPE type, int index);
static    int 		ZMAEE_Sms_ConvertType(ZMAEE_SMS_TYPE *pType, int nMsgboxEnum);
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
extern 	  int		ZMAEE_ITAPI_GetSimCardStatus_Int(int sim_idx);
#endif

extern kal_uint8 	applib_dt_dow(kal_uint16 y, kal_uint8 m, kal_uint8 d);
#ifdef WIN32
extern int 			ZMAEE_Ucs2_2_Utf8(const unsigned short* wcsSrc, int nwcsLen, char* pDst, int nSize);
extern EMSData *	GetEMSDataForView(EMSData **p, U8 force);
#endif


#define ZMAEE_SMS_INITVARIABLE(ptr, val)		if(ptr) *ptr = val

typedef struct {
	int (*AddRef)(AEE_IMessage* po);
	int (*Release)(AEE_IMessage* po);
	int (*GetSmsNum)(AEE_IMessage *po, ZMAEE_SMS_TYPE type);
	int (*GetSmsContent)(AEE_IMessage *po, ZMAEE_SMS_TYPE type, int index, ZMAEE_SMS_RECORD *record, ZMAEE_PFN_READSMS pfn, void *pUser);
	int (*GetSmsMemoryStatus)(AEE_IMessage *po, ZMAEE_SMS_MEMORYSTATUS *memory_status, ZMAEE_PFN_GETSMSMEMORY pfn, void *pUser);
}ZMAEE_MESSAGE_VTBL;

typedef struct
{
	void* 				pVtbl;
	int					nRef;
	U16                 msg_id;
	ZMAEE_CALLBACK		get_content_cb;
	int					getting_sms;		// 0 - idle, 1 - getting
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	int					list_index;	
	int 				get_sms_memstat;		// 0 - idle,  non zero - getting, 0x1 means sim 
	ZMAEE_CALLBACK		get_sms_memstat_cb;
	ZMAEE_SMS_MEMORYSTATUS	mem_stat;
	int 				folder;
	int 				status;
#else
	int 				folder;
#endif
}ZMAEE_Message;

static ZMAEE_Message		sg_zmaee_message = {0};
static ZMAEE_MESSAGE_VTBL   sg_zmaee_vtbl = {0};


/**
 * RETURN:
 * 	0			success
 *	1			success, means unread sms
 *	2			success, means read sms
 * 	3			success, means all sim all box
 * 	-1			failed
 */

int ZMAEE_Sms_Get_ListDetails(ZMAEE_SMS_TYPE nType, unsigned short **pListArray, unsigned short *pListSize, int *pMsgboxEnum, int *pSim)
{
	int folder = nType & 0xFF00;
	int sim = nType & 0xFF;
	int ret = -1;

	ZMAEE_SMS_INITVARIABLE(pSim, sim);
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	switch(folder) {
		case ZMAEE_SMS_UNREAD:
			ZMAEE_SMS_INITVARIABLE(pListArray, mmi_frm_sms_inbox_list);
			ZMAEE_SMS_INITVARIABLE(pListSize, mmi_frm_sms_get_sms_list_size((U16)MMI_FRM_SMS_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, MMI_FRM_SMS_INBOX);
			ret = 1;
			break;

		case ZMAEE_SMS_READ:
			ZMAEE_SMS_INITVARIABLE(pListArray, mmi_frm_sms_inbox_list);
			ZMAEE_SMS_INITVARIABLE(pListSize, mmi_frm_sms_get_sms_list_size((U16)MMI_FRM_SMS_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, MMI_FRM_SMS_INBOX);
			ret = 2;
			break;

		case ZMAEE_SMS_INBOX:
			ZMAEE_SMS_INITVARIABLE(pListArray, mmi_frm_sms_inbox_list);
			ZMAEE_SMS_INITVARIABLE(pListSize, mmi_frm_sms_get_sms_list_size((U16)MMI_FRM_SMS_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, MMI_FRM_SMS_INBOX);
			ret = 0;
			break;

		case ZMAEE_SMS_OUTBOX:
			ZMAEE_SMS_INITVARIABLE(pListArray, mmi_frm_sms_outbox_list);
			ZMAEE_SMS_INITVARIABLE(pListSize, mmi_frm_sms_get_sms_list_size((U16)MMI_FRM_SMS_OUTBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, MMI_FRM_SMS_OUTBOX);
			ret = 0;
			break;

	#ifdef __MMI_MESSAGES_DRAFT_BOX__
		case ZMAEE_SMS_DRAFT:
			ZMAEE_SMS_INITVARIABLE(pListArray, mmi_frm_sms_drafts_list);
			ZMAEE_SMS_INITVARIABLE(pListSize, mmi_frm_sms_get_sms_list_size((U16)MMI_FRM_SMS_DRAFTS));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, MMI_FRM_SMS_DRAFTS);
			ret = 0;
			break;
	#endif
		case ZMAEE_SMS_ALLBOX:
			if(sim == ZMAEE_SMS_ALLSIM)
			{
				unsigned short sms_count = mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_INBOX)
										+ mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_OUTBOX)
									#ifdef __MMI_MESSAGES_DRAFT_BOX__
										+ mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_DRAFTS)
									#endif
									#ifdef __MMI_UNIFIED_MESSAGE__
										+ mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_UNSENT)
									#endif
									#ifdef __UNIFIED_MESSAGE_ARCHIVE_SUPPORT__
										+ mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_ARCHIVE)
									#endif
									#ifdef __UNIFIED_MESSAGE_SIMBOX_SUPPORT__
										+ mmi_frm_sms_get_sms_list_size(MMI_FRM_SMS_SIM)
									#endif
									;
				ZMAEE_SMS_INITVARIABLE(pListSize, sms_count);
				ret = 3;
			}
			break;
	}
#else
switch(folder) {		
		case ZMAEE_SMS_UNREAD:
			ZMAEE_SMS_INITVARIABLE(pListSize, srv_sms_get_list_size(SRV_SMS_BOX_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, SRV_SMS_BOX_INBOX);
			ret = 1;
			break;

		case ZMAEE_SMS_READ:
			ZMAEE_SMS_INITVARIABLE(pListSize, srv_sms_get_list_size(SRV_SMS_BOX_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, SRV_SMS_BOX_INBOX);
			ret = 2;
			break;

		case ZMAEE_SMS_INBOX:
			ZMAEE_SMS_INITVARIABLE(pListSize, srv_sms_get_list_size(SRV_SMS_BOX_INBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, SRV_SMS_BOX_INBOX);
			ret = 0;
			break;

		case ZMAEE_SMS_OUTBOX:
			ZMAEE_SMS_INITVARIABLE(pListSize, srv_sms_get_list_size(SRV_SMS_BOX_OUTBOX));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, SRV_SMS_BOX_OUTBOX);
			ret = 0;
			break;

		case ZMAEE_SMS_DRAFT:
			ZMAEE_SMS_INITVARIABLE(pListSize, srv_sms_get_list_size(SRV_SMS_BOX_DRAFTS));
			ZMAEE_SMS_INITVARIABLE(pMsgboxEnum, SRV_SMS_BOX_DRAFTS);
			ret = 0;
			break;
			
		case ZMAEE_SMS_ALLBOX:
			if(sim == ZMAEE_SMS_ALLSIM)
			{
				unsigned short sms_count = srv_sms_get_list_size(SRV_SMS_BOX_INBOX)
										 + srv_sms_get_list_size(SRV_SMS_BOX_OUTBOX)
										 + srv_sms_get_list_size(SRV_SMS_BOX_DRAFTS)
									#ifdef __SRV_SMS_UNSENT_LIST__
										 + srv_sms_get_list_size(SRV_SMS_BOX_UNSENT)
									#endif
									#ifdef __SRV_SMS_ARCHIVE__
										 + srv_sms_get_list_size(SRV_SMS_BOX_ARCHIVE)
									#endif
									#ifdef __SRV_SMS_SIMBOX_SUPPORT__
										 + srv_sms_get_list_size(SRV_SMS_BOX_SIM)
									#endif
									;
				ZMAEE_SMS_INITVARIABLE(pListSize, sms_count);
				ret = 3;
			}
			break;
	}

#endif
	return ret;
}


/**
 * 检验@index 是否超出了短信列表数组的范围
 * @type					必须为ZMAEE_SMS_INBOX,ZMAEE_SMS_OUTBOX或者ZMAEE_SMS_DRAFT中的一种
 * RETURN:
 *  E_ZM_AEE_TRUE		参数有效
 *  E_ZM_AEE_FALSE		参数无效
 */
int ZMAEE_IMessage_CheckBoundary(ZMAEE_SMS_TYPE type, int index)
{	
	U16 sys_folder = 0;
	U16 index_id = 0;
	U16 folder_sms_size = 0;

	if(ZMAEE_Sms_Get_ListDetails(type, NULL, NULL, (int*)&sys_folder, NULL) == -1){
			return E_ZM_AEE_BADPARAM;
	}

#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	folder_sms_size = mmi_frm_sms_get_sms_list_size(sys_folder);
#else
	folder_sms_size = srv_sms_get_list_size(sys_folder);	
#endif

	if(folder_sms_size && (index >= 0) && index < folder_sms_size)
		return E_ZM_AEE_TRUE;
	else
		return E_ZM_AEE_FALSE;
}


#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
/**
 * RETURN:
 * 	0			success
 *	1			success, means unread sms
 *	2			success, means read sms
 * 	3			success, means all sim all box
 * 	-1			failed
 */
int ZMAEE_Sms_ConvertType(ZMAEE_SMS_TYPE *pType, int nMsgboxEnum)
{
	if(!pType)
		return -1;

	switch(nMsgboxEnum) {
		case MMI_FRM_SMS_UNREAD:
			*pType = ZMAEE_SMS_INBOX | ZMAEE_SMS_UNREAD;
			break;
			
		case MMI_FRM_SMS_INBOX:
			*pType = ZMAEE_SMS_INBOX | ZMAEE_SMS_READ;
			break;

		case MMI_FRM_SMS_OUTBOX:
			*pType = ZMAEE_SMS_OUTBOX;
			break;

		case MMI_FRM_SMS_DRAFTS:
			*pType = ZMAEE_SMS_DRAFT;
			break;

		default:
			return -1;
	}

	return 0;
}

/**
 * @bReadStatus			0 - uncheck read status, 1 - unread, 2 - read
 * @nSim				ZMAEE_SMS_SIM1 - sim1, ZMAEE_SMS_SIM2 - sim2, and etc, ZMAEE_SMS_ALLSIM - all sim
 * RETURN:
 * 	>= 0				sms count
 * 	-1					failure
 */
int ZMAEE_Sms_GetFilterCount(int bReadStatus, int nSim, unsigned short *pListArray, unsigned short nListSize)
{
	int i;
	unsigned short msg_id;
	unsigned short msg_count = 0;

	if(!pListArray || nListSize < 0)
		return -1;

	for(i = 0; i < nListSize; i++) {
		int status = 0;			// 0 - uncheck read status, 1 - unread, 2 - read
		int sim_check = 1;		// 1 - valid, will be added in count, 0 - invalid		
		
		msg_id = pListArray[i];
		if(bReadStatus != 0) {
			status = (mmi_frm_sms_msg_box[msg_id].msgtype & MMI_FRM_SMS_UNREAD)? 1: 2;
		}
		
		if(nSim != ZMAEE_SMS_ALLSIM) {
			if(ZMAEE_Sms_GetSim(msg_id) != nSim)
				sim_check = 0;
		}

		if((status == bReadStatus) && sim_check)
			msg_count++;
	}

	return msg_count;
}

#else

/**
 * @bReadStatus			0 - uncheck read status, 1 - unread, 2 - read
 * @nSim				ZMAEE_SMS_SIM1 - sim1, ZMAEE_SMS_SIM2 - sim2, and etc, ZMAEE_SMS_ALLSIM - all sim
 * RETURN:
 * 	>= 0				sms count
 * 	-1					failure
 */
int ZMAEE_Sms_GetFilterCount(int bReadStatus, int nSim, unsigned short *pListArray, unsigned short nListSize)
{
	int i = 0;
	unsigned short msg_id = 0;
	unsigned short msg_count = 0;
	
	if(nListSize < 0)
		return -1;

	for(i = 0; i < nListSize; i++) {
		int status = 0;			// 0 - uncheck read status, 1 - unread, 2 - read
		int sim_check = 1;		// 1 - valid, will be added in count, 0 - invalid	
		
		msg_id = srv_sms_get_msg_id(sg_zmaee_message.folder, i);
		if(bReadStatus != 0) {
			int sms_status = 0;
			
			sms_status = srv_sms_get_msg_status(msg_id);
			if(sms_status == SRV_SMS_STATUS_UNREAD)
				status = 1;
			else if(sms_status == SRV_SMS_STATUS_READ)
				status = 2;
			else
				return -1;
		}
		
		//SIM卡是否匹配
		if(nSim != ZMAEE_SMS_ALLSIM) {
			if(ZMAEE_Sms_GetSim(msg_id) != nSim)
				sim_check = 0;
		}
		
		if((status == bReadStatus) && sim_check)
			msg_count++;
	}

	return msg_count;

}

#endif



int ZMAEE_IMessage_New(ZM_AEECLSID clsId, void **pObj)
{
	
	extern void ZMAEE_CreateVtableExt(void **p_vtbl, int nCount, ...); 
	ZMAEE_Message*  pThis = NULL;
	
	pThis = &sg_zmaee_message;
	
	if(!sg_zmaee_vtbl.AddRef) {
		ZMAEE_CreateVtableExt((void**)&sg_zmaee_vtbl, ZMAEE_IMESSAGE_FUNC_COUNT,
								ZMAEE_IMessage_AddRef,
								ZMAEE_IMessage_Release,
								ZMAEE_IMessage_GetSmsNum,
								ZMAEE_IMessage_GetSmsContent,
								ZMAEE_IMessage_GetSmsMemoryStatus);
	}

	pThis->pVtbl = &sg_zmaee_vtbl;
	if(pThis->nRef == 0) {
		pThis->nRef = 1;
	} else {
		pThis->nRef++;
	}

	*pObj = (void*)pThis;

	return E_ZM_AEE_SUCCESS;
}

int ZMAEE_IMessage_AddRef(AEE_IMessage* po)
{
	ZMAEE_Message* pThis = (ZMAEE_Message*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;

	pThis->nRef++;

	return E_ZM_AEE_SUCCESS;
	
}
int ZMAEE_IMessage_Release(AEE_IMessage* po)
{
	ZMAEE_Message* pThis = (ZMAEE_Message*)po;
	if(pThis == 0)
		return E_ZM_AEE_BADPARAM;
	
	pThis->nRef--;

	return E_ZM_AEE_SUCCESS;
}

/*
  *获取短信数目
  */
int ZMAEE_IMessage_GetSmsNum(AEE_IMessage *po, ZMAEE_SMS_TYPE type)
{
	if(!po)
	   return E_ZM_AEE_BADPARAM;
	
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	if(!mmi_sms_is_sms_ready())
#else
	if(!srv_sms_is_sms_ready())
#endif	
	   return E_ZM_AEE_FAILURE;

	return ZMAEE_Sms_Getnum(type);

}

int ZMAEE_Sms_GetSim(U16 index)
{	
	int sim_card = 0;
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	#ifdef 	__MMI_DUAL_SIM_MASTER__
		extern mmi_sms_sim_enum mmi_sms_get_msg_sim_id(U16 msg_index);

		if(mmi_sms_get_msg_sim_id(index) == SMS_SIM_1)
		   sim_card = ZMAEE_SMS_SIM1;
		else if(mmi_sms_get_msg_sim_id(index) == SMS_SIM_2)
		   sim_card = ZMAEE_SMS_SIM2;
		else
			return E_ZM_AEE_FAILURE;
	#else
		return ZMAEE_SMS_SIM1;
	#endif

#else
	if(srv_sms_get_msg_sim_id(index) == SRV_SMS_SIM_1)
	   sim_card = ZMAEE_SMS_SIM1;
	else if(srv_sms_get_msg_sim_id(index) == SRV_SMS_SIM_2)
	   sim_card = ZMAEE_SMS_SIM2;
	else
		return E_ZM_AEE_FAILURE;
#endif
	return sim_card;		
}


int ZMAEE_Sms_Getnum(ZMAEE_SMS_TYPE type)
{
	int sim = 0;
	int ret = 0;
	unsigned short nListSize = 0;
#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
	unsigned short *pListArray;

	ret = ZMAEE_Sms_Get_ListDetails(type, &pListArray, &nListSize, NULL, &sim);
	if(ret == 3)
		return nListSize;
	else if(ret == -1)
		return E_ZM_AEE_BADPARAM;
	else
		return ZMAEE_Sms_GetFilterCount(ret, sim, pListArray, nListSize);
#else
	int pMsgboxEnum = 0;
	ret = ZMAEE_Sms_Get_ListDetails(type, NULL,&nListSize,&pMsgboxEnum,&sim);
	sg_zmaee_message.folder = pMsgboxEnum;
	if(ret == 3)
		return nListSize;
	else if(ret == -1)
		return E_ZM_AEE_BADPARAM;
	else		
		return ZMAEE_Sms_GetFilterCount(ret,sim,NULL,nListSize);
#endif
}



#if (ZM_AEE_MTK_SOFTVERN <  0x09B0)
void ZMAEE_SmsContent_CB(void *data,module_type mod, U16 result)
{
    extern unsigned int zmaee_strlen(const char * str);
	extern void * zmaee_memcpy(void * dest, const void * src, unsigned int count);
	extern kal_uint8 applib_dt_dow(kal_uint16 y, kal_uint8 m, kal_uint8 d);
	
	ZMAEE_PFN_READSMS pfn = (ZMAEE_PFN_READSMS)sg_zmaee_message.get_content_cb.pfn;
	void *pUser = sg_zmaee_message.get_content_cb.pUser;

	sg_zmaee_message.getting_sms = 0;
	if(result == SMS_RESULT_OK) {
		ZMAEE_SMS_RECORD record = {0};
		//并不是对所有的都适用
		unsigned short type = sg_zmaee_message.folder;
		EMSData *pEms;
		U8* msg_address = NULL;
		U8* msg_time = NULL;
		int sms_sim = 0;
		U8 storage = 0;
		//date表示未读和已读状态1-2
		if(ZMAEE_Callback_Valid(&sg_zmaee_message.get_content_cb)) {
			GetEMSDataForView(&pEms, 0);
			if((ZMAEE_Sms_ConvertType(&record.type, type) == -1)) {		
				pfn(-1, NULL, pUser);
				return;
			}
			if(*(int*)data == 1)
				record.type = ZMAEE_SMS_INBOX | ZMAEE_SMS_UNREAD;
			
			//storage
			storage = mmi_frm_sms_get_sms_storage(type,sg_zmaee_message.list_index);
			if(storage == SMSAL_ME) {
				record.storage = ZMAEE_SMS_STORAGE_DEVICE;
			} else {
				record.storage = ZMAEE_SMS_STORAGE_SIM;
			}
			//content
			record.content = pEms->textBuffer;
			record.content_size = pEms->textLength;
			switch(pEms->dcs){
				case SMSAL_UCS2_DCS:
					record.dcs = ZMAEE_UCS2_CONT;
					break;
				case SMSAL_8BIT_DCS:
					record.dcs = ZMAEE_UTF8_CONT;
					break;
				case SMSAL_DEFAULT_DCS:
					record.dcs = ZMAEE_GSM7_CONT;
					break;
				default:
					record.dcs = ZMAEE_OTHER_CONT;
			}

			//sms number 
			msg_address = mmi_frm_sms_get_sms_address(type,sg_zmaee_message.list_index);
			if(!msg_address) {
				pfn(-1, NULL, pUser);
				return;
			} else {
				strncpy(record.number, (const char*)msg_address, sizeof(record.number) - 1);
			}
		
			//sms port   
			if(pEms->PortNum.isPortNumSet == TRUE) {
				record.dest_port = pEms->PortNum.dst_port;
				record.src_port = pEms->PortNum.src_port;
			}
			
			//sms type
			sms_sim = ZMAEE_Sms_GetSim(sg_zmaee_message.msg_id);
			if(E_ZM_AEE_FAILURE == sms_sim){
				pfn(-1, NULL, pUser);
				return;
			}
			record.type = record.type | sms_sim;
			
			//sms time 0-6
			msg_time = mmi_sms_get_timestamp(sg_zmaee_message.msg_id);
			if(!msg_time) {
				pfn(-1, NULL, pUser);
				return;
			} else {//若为发件箱或者草稿箱，则日期清空
				if(sg_zmaee_message.folder != MMI_FRM_SMS_INBOX){
					zmaee_memset((void*)&record.sms_time,0,sizeof(record.sms_time));
				}else{
					record.sms_time.nYear = msg_time[0] + 2000;
					if(record.sms_time.nYear > 2090) {
						record.sms_time.nYear -= 100;
					}
					record.sms_time.nMonth = msg_time[1];
					record.sms_time.nDay   = msg_time[2];
					record.sms_time.nHour  = msg_time[3];
					record.sms_time.nMin   = msg_time[4];
					record.sms_time.nSec   = msg_time[5];
					record.sms_time.nWeekDay = (int)applib_dt_dow(record.sms_time.nYear, record.sms_time.nMonth, record.sms_time.nDay);
				}
				}
			pfn(0, &record, pUser);
		}
	} else {
		
		if(ZMAEE_Callback_Valid(&sg_zmaee_message.get_content_cb)) {
			pfn(-1, NULL, pUser);
		}
	}
}

/**
 * 检验@index 是否超出了短信列表数组的范围
 * @type					必须为ZMAEE_SMS_INBOX,ZMAEE_SMS_OUTBOX或者ZMAEE_SMS_DRAFT中的一种
 * RETURN:
 *  E_ZM_AEE_TRUE		参数有效
 *  E_ZM_AEE_FALSE		参数无效
 */

int ZMAEE_IMessage_GetSmsContent(AEE_IMessage *po, ZMAEE_SMS_TYPE type, int index, ZMAEE_SMS_RECORD *record, ZMAEE_PFN_READSMS pfn, void *pUser,int change_status)
{	
	U8 index_id = 0;
	U16 sys_folder = 0;
	unsigned short *pListArray;
	unsigned short nListSize;
	ZMAEE_Message *pThis = (ZMAEE_Message*)po;
	
	if(!pThis)
		return E_ZM_AEE_BADPARAM;

	if(!mmi_sms_is_sms_ready())
		return E_ZM_AEE_FAILURE;
	
	if(pThis->getting_sms)
		return E_ZM_AEE_ITEMBUSY;

	if((type != ZMAEE_SMS_INBOX) && (type != ZMAEE_SMS_OUTBOX) && (type != ZMAEE_SMS_DRAFT))
		return E_ZM_AEE_BADPARAM;
	
	if(ZMAEE_IMessage_CheckBoundary(type, index) != E_ZM_AEE_TRUE)
		return E_ZM_AEE_BADPARAM;
	//这里 unread 和read传进去,系统的都是inbox
	if(ZMAEE_Sms_Get_ListDetails(type, &pListArray, &nListSize, (int *)&sys_folder, NULL) == -1){
		return E_ZM_AEE_BADPARAM;
	}
	sg_zmaee_message.msg_id = pListArray[index];
	sg_zmaee_message.list_index = index;
	sg_zmaee_message.folder = sys_folder;
	mmi_frm_sms_read_sms((PsFuncPtrU16)ZMAEE_SmsContent_CB, MOD_MMI, sys_folder, (U16)index, (!change_status)? MMI_FALSE: MMI_TRUE);
	memset(&pThis->get_content_cb, 0,sizeof(ZMAEE_CALLBACK));
	ZMAEE_Callback_Init(&pThis->get_content_cb, (void*)pfn, pUser);
	pThis->getting_sms = 1;
	return E_ZM_AEE_WOULDBLOCK;
}



/**
 * 获取短信存储信息
 * @memory_status	短信存储信息
 * RETURN:
 * 	E_ZM_AEE_BADPARAM			po is null, or memory_status is null
 * 	E_ZM_AEE_FAILURE			获取失败
 * 	E_ZM_AEE_ITEMBUSY			等待一段时间后重试
 * 	E_ZM_AEE_WOULDBLOCK		通过回调函数来获取短信存储信息
 * 	E_ZM_AEE_SUCCESS			获取成功
 */
int ZMAEE_IMessage_GetSmsMemoryStatus(AEE_IMessage *po, ZMAEE_SMS_MEMORYSTATUS *memory_status, ZMAEE_PFN_GETSMSMEMORY pfn, void *pUser)
{
	ZMAEE_Message *pThis = (ZMAEE_Message*)po;
	
	if(!pThis || !pfn)
		return E_ZM_AEE_BADPARAM;

	if(pThis->get_sms_memstat)
		return E_ZM_AEE_ITEMBUSY;
	
	if(!mmi_sms_is_sms_ready())
		return E_ZM_AEE_FAILURE;

	if(ZMAEE_ITAPI_GetSimCardStatus_Int(0))
	{	
		pThis->get_sms_memstat |= 0x1;
		mmi_frm_sms_send_message(MOD_MMI, MOD_L4C, 0, PRT_MSG_ID_MMI_SMS_GET_MSG_NUM_REQ, NULL, NULL);
	}
#ifdef __MMI_DUAL_SIM_MASTER__
	if(ZMAEE_ITAPI_GetSimCardStatus_Int(1))
	{
		pThis->get_sms_memstat |= 0x2;
		mmi_frm_sms_send_message(MOD_MMI, MOD_L4C_2, 0, PRT_MSG_ID_MMI_SMS_GET_MSG_NUM_REQ, NULL, NULL);
	}
#endif

	memset(&pThis->mem_stat, 0, sizeof(pThis->mem_stat));
	ZMAEE_Callback_Init(&pThis->get_sms_memstat_cb, (void*)pfn, pUser);

	return E_ZM_AEE_WOULDBLOCK;
}


/**
 * RETURN: 回调回来的信息
 * 	0				not handle
 * 	1				handle
 */
int ZMAEE_IMessage_GetSmsMemoryStatus_Resp(int mod_src, U16 eventID, void *MsgStruct, void *peerBuf)
{
	ZMAEE_Message *pThis = (ZMAEE_Message*)&sg_zmaee_message;

	if(pThis->get_sms_memstat) {
		if(mod_src == MOD_L4C)
		{
			MMI_FRM_SMS_GET_MSG_NUM_RSP_STRUCT *msgRsp = (MMI_FRM_SMS_GET_MSG_NUM_RSP_STRUCT*)MsgStruct;

			pThis->get_sms_memstat &= ~(0x1);
			if(msgRsp->result == TRUE)
			{
				pThis->mem_stat.device_total = msgRsp->total_me_num;
				pThis->mem_stat.sim1_total = msgRsp->total_sim_num;
				pThis->mem_stat.device_used = msgRsp->in_me_no + msgRsp->out_me_no;
				pThis->mem_stat.sim1_used = msgRsp->in_sim_no + msgRsp->out_sim_no;
			}
		}
#ifdef __MMI_DUAL_SIM_MASTER__
		else if(mod_src == MOD_L4C_2)
		{
			mmi_sms_get_msg_num_rsp_struct *msgRsp = (mmi_sms_get_msg_num_rsp_struct *)MsgStruct;

			pThis->get_sms_memstat &= ~(0x2);
			if(msgRsp->result == TRUE) {
				pThis->mem_stat.device_total = msgRsp->total_me_num;
				pThis->mem_stat.device_used = msgRsp->in_me_no + msgRsp->out_me_no;
				pThis->mem_stat.sim2_total = msgRsp->total_sim_num;
				pThis->mem_stat.sim2_used = msgRsp->in_sim_no + msgRsp->out_sim_no;
			}
		}
#endif		

		if(!pThis->get_sms_memstat) {
			ZMAEE_PFN_GETSMSMEMORY pfn = (ZMAEE_PFN_GETSMSMEMORY)pThis->get_sms_memstat_cb.pfn;
			void *pUser = (void*)pThis->get_sms_memstat_cb.pUser;

			if(ZMAEE_Callback_Valid(&pThis->get_sms_memstat_cb)) {
				pfn(&pThis->mem_stat, pUser);
			}
		}		

		return 1;
	}

	return 0;
}


#else   //(ZM_AEE_MTK_SOFTVERN <  0x09B0)

int ZMAEE_Sms_InitRecord(ZMAEE_SMS_RECORD *record, srv_sms_msg_data_struct *msg_data)
{
	int sms_sim = 0;
	EMSData*pEms =(EMSData*)(msg_data->content_ems);
	
	if(!msg_data){
		return E_ZM_AEE_BADPARAM;
	}
	
	//sms content&size
	record->content = (unsigned char*)pEms->textBuffer;
	record->content_size = (unsigned int)pEms->textLength;

	//sms num ucs2
	ZMAEE_Ucs2_2_Utf8((const unsigned short * )msg_data->number,wcslen((const wchar_t*)msg_data->number),record->number,sizeof(record->number)-1);

	//sms dcs
	switch(pEms->dcs){
		case SRV_SMS_DCS_7BIT:
			record->dcs = ZMAEE_GSM7_CONT;
			break;
		case SRV_SMS_DCS_8BIT:
			record->dcs = ZMAEE_UTF8_CONT;
			break;
		case SRV_SMS_DCS_UCS2:
			record->dcs = ZMAEE_UCS2_CONT;
			break;
		default:
			record->dcs = ZMAEE_OTHER_CONT;
	} 

	//sms port 
	if(pEms->PortNum.isPortNumSet){
		record->dest_port = pEms->PortNum.dst_port;
		record->src_port  = pEms->PortNum.src_port;
	}
	
	//sms type sim&folder
	sms_sim = ZMAEE_Sms_GetSim(sg_zmaee_message.msg_id);
	if(E_ZM_AEE_FAILURE != sms_sim){
		record->type = (sg_zmaee_message.folder) | sms_sim;
		if(sg_zmaee_message.folder == ZMAEE_SMS_INBOX) {
			record->type |= (msg_data->status == SRV_SMS_STATUS_UNREAD)? ZMAEE_SMS_UNREAD: ZMAEE_SMS_READ;
		}
	}

	
	//sms  storage
	if(msg_data->storage_type == SRV_SMS_STORAGE_ME)
		record->storage = ZMAEE_SMS_STORAGE_DEVICE;
	else 
		record->storage = ZMAEE_SMS_STORAGE_SIM;
	
	//sms time	sys 0 - 6
	record->sms_time.nDay   = msg_data->timestamp.nDay;
	record->sms_time.nHour  = msg_data->timestamp.nHour;
	record->sms_time.nMin   = msg_data->timestamp.nMin;
	record->sms_time.nMonth = msg_data->timestamp.nMonth;
	record->sms_time.nSec   = msg_data->timestamp.nSec;
	record->sms_time.nYear  = msg_data->timestamp.nYear;
	record->sms_time.nWeekDay = applib_dt_dow(msg_data->timestamp.nYear, msg_data->timestamp.nMonth, msg_data->timestamp.nDay);
	
	return E_ZM_AEE_SUCCESS;
}



void ZMAEE_SmsContent_CB(srv_sms_callback_struct* callback_data)
{
	ZMAEE_PFN_READSMS pfn = (ZMAEE_PFN_READSMS)sg_zmaee_message.get_content_cb.pfn;
	void *pUser = sg_zmaee_message.get_content_cb.pUser;
	srv_sms_msg_data_struct*    msg_data = (srv_sms_msg_data_struct *)(((srv_sms_read_msg_cb_struct*)callback_data->action_data)->msg_data);

	sg_zmaee_message.getting_sms = 0;
	if(callback_data->result){
		int sms_sim = 0;
		int	sms_init = 0;
		ZMAEE_SMS_RECORD record = {0};
		
		if(ZMAEE_Callback_Valid(&sg_zmaee_message.get_content_cb)){
			sms_init = ZMAEE_Sms_InitRecord(&record,msg_data);
			if(sms_init != E_ZM_AEE_SUCCESS){
				pfn(-1,NULL,pUser);
				goto zmaee_sms_back_end;
			}
		}
		pfn(0, &record, pUser);		
	}else {
		if(ZMAEE_Callback_Valid(&sg_zmaee_message.get_content_cb)){
			pfn(-1, NULL, pUser);			
		}
	}
zmaee_sms_back_end:
	if(msg_data)
		OslMfree(msg_data);
}



int ZMAEE_IMessage_GetSmsContent(AEE_IMessage *po, ZMAEE_SMS_TYPE type, int index, ZMAEE_SMS_RECORD *record, ZMAEE_PFN_READSMS pfn, void *pUser,int change_status)
{	
	U16							msg_id = 0;
	int 						pMsgboxEnum = 0;
	ZMAEE_Message*		 		pThis = (ZMAEE_Message*)po;	
	srv_sms_msg_data_struct*	msg_data = NULL;
	
	if(!po)
	   return E_ZM_AEE_BADPARAM;

	if(!srv_sms_is_sms_ready())
		return E_ZM_AEE_FAILURE;

	if(pThis->getting_sms)
		return E_ZM_AEE_ITEMBUSY;
	
	if((type != ZMAEE_SMS_INBOX) && (type != ZMAEE_SMS_OUTBOX) && (type != ZMAEE_SMS_DRAFT))
		return E_ZM_AEE_BADPARAM;
	
	if(ZMAEE_IMessage_CheckBoundary(type, index) != E_ZM_AEE_TRUE)
		return E_ZM_AEE_BADPARAM;

	if(ZMAEE_Sms_Get_ListDetails(type, NULL, NULL, &pMsgboxEnum, NULL) == -1)
		return E_ZM_AEE_FAILURE;
	
	msg_id = srv_sms_get_msg_id(pMsgboxEnum, (U16)index);
	sg_zmaee_message.msg_id = msg_id; 
	sg_zmaee_message.folder = type;	
	
	msg_data = (srv_sms_msg_data_struct*)OslMalloc(sizeof(srv_sms_msg_data_struct));
	if(!msg_data)
		return E_ZM_AEE_NOMEMORY;
	
	msg_data->para_flag = SRV_SMS_PARA_NUM  | SRV_SMS_PARA_SIM_ID | SRV_SMS_PARA_STORAGE_TYPE | SRV_SMS_PARA_TIMESTAMP |SRV_SMS_PARA_CONTENT_EMS;
	#if (ZM_AEE_MTK_SOFTVERN == 0x11A0)
	msg_data->content_ems = srv_sms_malloc_ems_data();
	#else
	msg_data->content_ems = (EMSData*)GetEMSDataForView(NULL, 1);
	#endif
	srv_sms_read_msg(msg_id, (!change_status)? MMI_FALSE: MMI_TRUE, msg_data,ZMAEE_SmsContent_CB,(void*)NULL);
	memset(&pThis->get_content_cb, 0,sizeof(ZMAEE_CALLBACK));
	ZMAEE_Callback_Init(&pThis->get_content_cb, (void*)pfn, pUser);
	pThis->getting_sms = 1;
	
	return E_ZM_AEE_WOULDBLOCK;
	
}


#if (ZM_AEE_MTK_SOFTVERN == 0x11A0)
int ZMAEE_IMessage_GetSmsMemoryStatus(AEE_IMessage *po, ZMAEE_SMS_MEMORYSTATUS *memory_status, ZMAEE_PFN_GETSMSMEMORY pfn, void *pUser)
{
	srv_sms_memory_status_struct status_data = {0};

		if(!po || !memory_status)
			return E_ZM_AEE_BADPARAM;

		if(!srv_sms_is_sms_ready())
			return E_ZM_AEE_FAILURE;

		if(!srv_sms_get_memory_status(SRV_SMS_SIM_1, &status_data)) {
			return E_ZM_AEE_FAILURE;
		}
		
		memory_status->device_total = status_data.me_total;
		memory_status->device_used	= status_data.me_used;
		memory_status->sim1_total = status_data.sim_total;
		memory_status->sim1_used = status_data.sim_used;

#ifdef __MMI_DUAL_SIM_MASTER__
		if(!srv_sms_get_memory_status(SRV_SMS_SIM_2, &status_data)) {
			return E_ZM_AEE_FAILURE;
		}

		memory_status->sim2_total = status_data.sim_total;
		memory_status->sim2_used = status_data.sim_used;
#endif

		return E_ZM_AEE_SUCCESS;	

}

#else	//(ZM_AEE_MTK_SOFTVERN == 0x11A0)

int ZMAEE_IMessage_GetSmsMemoryStatus(AEE_IMessage *po, ZMAEE_SMS_MEMORYSTATUS *memory_status, ZMAEE_PFN_GETSMSMEMORY pfn, void *pUser)
{
	srv_sms_memory_status_struct status_data = {0};

	if(!po || !memory_status)
		return E_ZM_AEE_BADPARAM;

	if(!srv_sms_is_sms_ready())
		return E_ZM_AEE_FAILURE;

	if(!srv_sms_get_memory_status(SRV_SMS_SIM_1, &status_data)) {
		return E_ZM_AEE_FAILURE;
	}
	
	memory_status->device_total = status_data.me_total;
	memory_status->device_used	= status_data.me_used;
	memory_status->sim1_total = status_data.sim_total;
	memory_status->sim1_used = status_data.sim_used;

#ifdef __MMI_DUAL_SIM_MASTER__
	if(!srv_sms_get_memory_status(SRV_SMS_SIM_2, &status_data)) {
		return E_ZM_AEE_FAILURE;
	}

	memory_status->sim2_total = status_data.sim_total;
	memory_status->sim2_used = status_data.sim_used;
#endif

	return E_ZM_AEE_SUCCESS;	
}
#endif   //(ZM_AEE_MTK_SOFTVERN == 0x11A0)
#endif   //(ZM_AEE_MTK_SOFTVERN <  0x09B0)

#endif	// __ZMAEE_APP__
