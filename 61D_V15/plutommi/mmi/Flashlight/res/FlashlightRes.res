/* Needed header files of the compile option in XML files, if you need others need to add here */
#include "mmi_features.h"
#include "custresdef.h"

/* Need this line to tell parser that XML start, must after all #include. */
<?xml version="1.0" encoding="UTF-8"?>

/* APP tag, include your app name defined in MMIDataType.h */
<APP id="APP_FLASHLIGHT">

    /* When you use any ID of other module, you need to add
       that header file here, so that Resgen can find the ID */
    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h"/>

    <!-----------------------------------------------------String Resource Area----------------------------------------------------->
    <STRING id="STRING_ID_MENU_FLASHLIGHT"/>
    <STRING id="STRING_ID_FLASHLIGHT_ON"/>
    <STRING id="STRING_ID_FLASHLIGHT_OFF"/>

    <!-----------------------------------------------------Image Resource Area------------------------------------------------------>
#if defined(__MMI_SHOW_STATUS_ICON_TORCH__)
    <IMAGE id = "IMG_SI_TORCH_ACTIVATED" force_type = "FORCE_ABM">RES_THEME_ROOT"\\\\default\\\\StatusIcon\\\\torch.png"</IMAGE>;
#endif

    <!------------------------------------------------------Menu Resource Area------------------------------------------------------>
    <MENU id="MENU_ID_FLASHLIGHT" type="APP_MAIN"
        str="STRING_ID_MENU_FLASHLIGHT" 
        highlight="mmi_flashlight_menu_highlight" 
        launch="mmi_flashlight_menu_entry">
        <MENUITEM_ID>MENU_ID_FLASHLIGHT_ON</MENUITEM_ID>
        <MENUITEM_ID>MENU_ID_FLASHLIGHT_OFF</MENUITEM_ID>
    </MENU>

    <MENUITEM id="MENU_ID_FLASHLIGHT_OFF" str="STR_GLOBAL_OFF"/>

    <MENUITEM id="MENU_ID_FLASHLIGHT_ON" str="STR_GLOBAL_ON"/>

    <!------------------------------------------------------Other Resource---------------------------------------------------------->
    <SCREEN id="GRP_ID_FLASHLIGHT"/>
    <SCREEN id="SCR_ID_FLASHLIGHT"/>
    
</APP>
