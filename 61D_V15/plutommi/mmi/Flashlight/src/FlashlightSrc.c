#include "MMI_features.h"
#include "MMI_include.h"
#include "ProfilesSrvGprot.h"
#include "IdleAppResDef.h"
#include "MMIDataType.h"
#include "GlobalResDef.h"

#include "mmi_rp_app_flashlight_def.h"

typedef struct
{
    MMI_BOOL m_bIsOn;
} FlashlightContextType;

static FlashlightContextType gFlashLightCntx = 
{
    MMI_FALSE,
};

#if defined(__DRV_FLASHLIGHT_SUPPORT__)
extern void drv_flashlight_switch(MMI_BOOL bStatus);
#endif

#if defined(__MMI_SHOW_STATUS_ICON_TORCH__)
extern void wgui_status_icon_bar_hide_icon(S32 icon_id);
extern void wgui_status_icon_bar_show_icon(S32 icon_id);
#endif

/*****************************************************************************
 * FUNCTION
 *  mmi_flashlight_is_on
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
MMI_BOOL mmi_flashlight_is_on(void)
{
    return gFlashLightCntx.m_bIsOn;
}

/*****************************************************************************
 * FUNCTION
 *  mmi_flashlight_is_on
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_flashlight_switch_internal(MMI_BOOL status)
{
	MMI_STR_ID l_aSTatus[] = {STRING_ID_FLASHLIGHT_OFF, STRING_ID_FLASHLIGHT_ON};

    do 
    {
        if (gFlashLightCntx.m_bIsOn == status)
        {
            break ;
        }

        gFlashLightCntx.m_bIsOn = status;

        if (status == MMI_FALSE)
        {
    #if defined(__MMI_SHOW_STATUS_ICON_TORCH__)
            wgui_status_icon_bar_hide_icon(STATUS_ICON_TORCH);
    #endif
        }
        else
        {
    #if defined(__MMI_SHOW_STATUS_ICON_TORCH__)
            wgui_status_icon_bar_show_icon(STATUS_ICON_TORCH);
    #endif
        }
    #if defined(__DRV_FLASHLIGHT_SUPPORT__)
        drv_flashlight_switch(status);
    #endif /*defined(__DRV_FLASHLIGHT_SUPPORT__)*/

        mmi_popup_display_simple_ext(
            l_aSTatus[status],
            MMI_EVENT_SUCCESS,
            GRP_ID_ROOT,
            NULL);
    } while (0);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_flashlight_switch
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_flashlight_switch(void *info)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    cui_menu_event_struct *menu_evt = (cui_menu_event_struct*)info;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if (menu_evt->highlighted_menu_id == MENU_ID_FLASHLIGHT_ON)
    {
		mmi_flashlight_switch_internal(1);
    }
    else if (menu_evt->highlighted_menu_id == MENU_ID_FLASHLIGHT_OFF)
    {
		mmi_flashlight_switch_internal(0);
    }

    mmi_frm_group_close(GRP_ID_FLASHLIGHT);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_flashlight_toggle
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_flashlight_toggle(void)
{
	mmi_flashlight_switch_internal(!gFlashLightCntx.m_bIsOn);
}

/*****************************************************************************
* FUNCTION
*  mmi_flashlight_main_proc
* DESCRIPTION
* PARAMETERS
*  void
* RETURNS
*  void
*****************************************************************************/
static mmi_ret mmi_flashlight_main_proc(mmi_event_struct* evt)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    cui_menu_event_struct *menu_evt = (cui_menu_event_struct*)evt;
    mmi_id curr_gid = mmi_frm_group_get_active_id();
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    switch (evt->evt_id)
    {
        case EVT_ID_SCRN_DEINIT:
            mmi_frm_group_close(GRP_ID_FLASHLIGHT);
            break;

        case EVT_ID_CUI_MENU_LIST_ENTRY:
            if(menu_evt->parent_menu_id == MENU_ID_FLASHLIGHT)
            {
                cui_menu_set_currlist_flags(curr_gid, CUI_MENU_NORMAL_RADIO_BUTTON_LIST); 
            }
            break;

        case EVT_ID_CUI_MENU_ITEM_SELECT:
            if(menu_evt->parent_menu_id == MENU_ID_FLASHLIGHT)
            {
                mmi_flashlight_switch(menu_evt);
            }
            break;  

        case EVT_ID_CUI_MENU_CLOSE_REQUEST:
            cui_menu_close(curr_gid);
            break;

        default:
            break;
    }

    return MMI_RET_OK;
}

/*****************************************************************************
 * FUNCTION
 *  HighlightFlashLightMenu
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
 void mmi_flashlight_menu_entry(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    MMI_ID menu_gid;
	
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    mmi_frm_group_create(GRP_ID_ROOT, GRP_ID_FLASHLIGHT, mmi_flashlight_main_proc, NULL);
    mmi_frm_group_enter(GRP_ID_FLASHLIGHT, MMI_FRM_NODE_SMART_CLOSE_FLAG);

    menu_gid = cui_menu_create(
                GRP_ID_FLASHLIGHT,
                CUI_MENU_SRC_TYPE_RESOURCE,
                CUI_MENU_TYPE_FROM_RESOURCE,
                MENU_ID_FLASHLIGHT,
                MMI_FALSE,
                NULL);

    cui_menu_set_radio_list_item(menu_gid, mmi_flashlight_is_on() ? MENU_ID_FLASHLIGHT_ON : MENU_ID_FLASHLIGHT_OFF);

    cui_menu_set_currlist_title(menu_gid, (WCHAR*) GetString(STRING_ID_MENU_FLASHLIGHT), NULL);

    cui_menu_run(menu_gid);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_flashlight_highlight
 * DESCRIPTION
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_flashlight_menu_highlight(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    ChangeCenterSoftkey(0, IMG_GLOBAL_COMMON_CSK);
    SetLeftSoftkeyFunction(mmi_flashlight_menu_entry, KEY_EVENT_UP);
    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
    SetCenterSoftkeyFunction(mmi_flashlight_menu_entry, KEY_EVENT_UP);
}
