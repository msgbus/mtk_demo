#ifndef __CUST_IMG_GROUP_DATA_HW_H_
#define __CUST_IMG_GROUP_DATA_HW_H_
#include "CustDataRes.h"
#include "ResCompressConfig.h"

#ifndef __MMI_RESOURCE_IMAGE_THIRD_ROM__
#ifdef __MMI_RESOURCE_LZMA_IMAGE_GROUP_COMPRESS__

#if ( !defined (__MTK_TARGET__) )
    #define __align(x)
#endif

extern const U8 image_group_0[];
extern const U8 image_group_1[];
extern const U8 image_group_2[];
extern const U8 image_group_3[];
extern const U8 image_group_4[];
extern const U8 image_group_5[];
extern const U8 image_group_6[];
extern const U8 image_group_7[];
extern const U8 image_group_8[];
extern const U8 image_group_9[];
extern const U8 image_group_10[];
extern const U8 image_group_11[];
extern const U8 image_group_12[];
extern const U8 image_group_13[];
extern const U8 image_group_14[];
extern const U8 image_group_15[];
extern const U8 image_group_16[];
extern const U8 image_group_17[];
extern const U8 image_group_18[];
extern const U8 image_group_19[];
extern const U8 image_group_20[];
extern const U8 image_group_21[];
extern const U8 image_group_22[];
extern const U8 image_group_23[];
extern const U8 image_group_24[];
extern const U8 image_group_25[];
extern const U8 image_group_26[];
extern const U8 image_group_27[];
extern const U8 image_group_28[];
extern const U8 image_group_29[];
extern const U8 image_group_30[];
extern const U8 image_group_31[];
extern const U8 image_group_32[];
extern const U8 image_group_33[];
extern const U8 image_group_34[];
extern const U8 image_group_35[];

#ifdef __MMI_RESOURCE_UT_SIMPLE_CACHE_SUPPORT__
extern U8 cache_image_group_0[];
extern U8 cache_image_group_1[];
extern U8 cache_image_group_2[];
extern U8 cache_image_group_3[];
extern U8 cache_image_group_4[];
extern U8 cache_image_group_5[];
extern U8 cache_image_group_6[];
extern U8 cache_image_group_7[];
extern U8 cache_image_group_8[];
extern U8 cache_image_group_9[];
extern U8 cache_image_group_10[];
extern U8 cache_image_group_11[];
extern U8 cache_image_group_12[];
extern U8 cache_image_group_13[];
extern U8 cache_image_group_14[];
extern U8 cache_image_group_15[];
extern U8 cache_image_group_16[];
extern U8 cache_image_group_17[];
extern U8 cache_image_group_18[];
extern U8 cache_image_group_19[];
extern U8 cache_image_group_20[];
extern U8 cache_image_group_21[];
extern U8 cache_image_group_22[];
extern U8 cache_image_group_23[];
extern U8 cache_image_group_24[];
extern U8 cache_image_group_25[];
extern U8 cache_image_group_26[];
extern U8 cache_image_group_27[];
extern U8 cache_image_group_28[];
extern U8 cache_image_group_29[];
extern U8 cache_image_group_30[];
extern U8 cache_image_group_31[];
extern U8 cache_image_group_32[];
extern U8 cache_image_group_33[];
extern U8 cache_image_group_34[];
extern U8 cache_image_group_35[];

#endif //__MMI_RESOURCE_UT_SIMPLE_CACHE_SUPPORT__


#endif  //__MMI_RESOURCE_LZMA_IMAGE_GROUP_COMPRESS__


#endif  // __MMI_RESOURCE_IMAGE_THIRD_ROM__

#endif//__CUST_IMG_GROUP_DATA_HW_H_
