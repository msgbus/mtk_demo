#include "CustDataRes.h"
#include "ResCompressConfig.h"
#ifdef __MMI_RESOURCE_IMAGE_THIRD_ROM__
#include "CustResDef.h"
const unsigned short  mtk_CurrMaxGroupsNum = 36;


const CUSTOM_IMAGE	mtk_nCustGroupNames[]={
    (U8*)0,
    (U8*)2964,
    (U8*)5850,
    (U8*)8602,
    (U8*)11226,
    (U8*)14087,
    (U8*)16815,
    (U8*)19651,
    (U8*)22483,
    (U8*)25207,
    (U8*)27037,
    (U8*)29263,
    (U8*)31843,
    (U8*)33899,
    (U8*)36333,
    (U8*)37971,
    (U8*)40257,
    (U8*)42357,
    (U8*)45131,
    (U8*)48125,
    (U8*)50965,
    (U8*)53809,
    (U8*)56727,
    (U8*)59765,
    (U8*)62137,
    (U8*)65181,
    (U8*)68199,
    (U8*)71081,
    (U8*)73741,
    (U8*)76777,
    (U8*)78229,
    (U8*)81105,
    (U8*)83890,
    (U8*)86850,
    (U8*)89382,
    (U8*)90798
};


#endif // __MMI_RESOURCE_IMAGE_THIRD_ROM__
