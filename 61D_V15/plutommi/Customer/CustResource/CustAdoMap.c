/**
 *	Copyright Notice
 *	?2002 - 2003, Pixtel Communications, Inc., 1489 43rd Ave. W.,
 *	Vancouver, B.C. V6M 4K8 Canada. All Rights Reserved.
 *  (It is illegal to remove this copyright notice from this software or any
 *  portion of it)
 */

/************************************************************** 
 FILENAME	: CustAudioMap.c 
 PURPOSE		: Resource Map file. 
 REMARKS		: nil 
 AUTHOR		: Customization Tool 
 DATE		: . 
 **************************************************************/
#include "CustDataRes.h"


unsigned short CurrMaxAudioId=31;
const CUSTOM_AUDIO_MAP AudioIdMap[]={
	{0},
	{1},
	{2},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
	{0},
};



const unsigned short CurrMaxSearchAudioId=5;
const CUSTOM_AUDIO_SEARCH_MAP AudioIdSearchMap[]={
	{1,1,0},
	{3,4,1},
	{7764,7765,3},
	{7771,7772,5},
	{7778,7801,7},
};



