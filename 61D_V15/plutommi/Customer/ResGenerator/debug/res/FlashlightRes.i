# 1 "temp/res/FlashlightRes.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/FlashlightRes.c"

# 1 "../../mmi/inc/mmi_features.h" 1
# 69 "../../mmi/inc/mmi_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/mmi_features.h" 2
# 3 "temp/res/FlashlightRes.c" 2
# 1 "../customerinc/custresdef.h" 1
# 195 "../customerinc/custresdef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/custresdef.h" 2
# 4 "temp/res/FlashlightRes.c" 2


<?xml version="1.0" encoding="UTF-8"?>


<APP id="APP_FLASHLIGHT">



    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h"/>

    <!-----------------------------------------------------String Resource Area----------------------------------------------------->
    <STRING id="STRING_ID_MENU_FLASHLIGHT"/>
    <STRING id="STRING_ID_FLASHLIGHT_ON"/>
    <STRING id="STRING_ID_FLASHLIGHT_OFF"/>

    <!-----------------------------------------------------Image Resource Area------------------------------------------------------>




    <!------------------------------------------------------Menu Resource Area------------------------------------------------------>
    <MENU id="MENU_ID_FLASHLIGHT" type="APP_MAIN"
        str="STRING_ID_MENU_FLASHLIGHT"
        highlight="mmi_flashlight_menu_highlight"
        launch="mmi_flashlight_menu_entry">
        <MENUITEM_ID>MENU_ID_FLASHLIGHT_ON</MENUITEM_ID>
        <MENUITEM_ID>MENU_ID_FLASHLIGHT_OFF</MENUITEM_ID>
    </MENU>

    <MENUITEM id="MENU_ID_FLASHLIGHT_OFF" str="STR_GLOBAL_OFF"/>

    <MENUITEM id="MENU_ID_FLASHLIGHT_ON" str="STR_GLOBAL_ON"/>

    <!------------------------------------------------------Other Resource---------------------------------------------------------->
    <SCREEN id="GRP_ID_FLASHLIGHT"/>
    <SCREEN id="SCR_ID_FLASHLIGHT"/>

</APP>
