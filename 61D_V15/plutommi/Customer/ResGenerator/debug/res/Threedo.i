# 1 "temp/res/Threedo.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/Threedo.c"


# 1 "../../mmi/inc/mmi_features.h" 1
# 69 "../../mmi/inc/mmi_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/mmi_features.h" 2
# 4 "temp/res/Threedo.c" 2
# 1 "../customerinc/custresdef.h" 1
# 195 "../customerinc/custresdef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/custresdef.h" 2
# 5 "temp/res/Threedo.c" 2


<?xml version="1.0" encoding="UTF-8"?>


<APP id="APP_THREEDO">




    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h"/>


    <RECEIVER id="EVT_ID_USB_ENTER_MS_MODE" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_USB_EXIT_MS_MODE" proc="threedo_process_system_event"/>

        <RECEIVER id="EVT_ID_SRV_SMS_READY" proc="threedo_process_system_event"/>

        <RECEIVER id="EVT_ID_SCR_LOCKER_LOCKED" proc="threedo_process_system_event"/>


    <RECEIVER id="EVT_ID_SRV_NW_INFO_STATUS_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_SERVICE_AVAILABILITY_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_SIGNAL_STRENGTH_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_LOCATION_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_ROAMING_STATUS_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_PROTOCOL_CAPABILITY_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_SIM_DN_STATUS_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_NW_INFO_REGISTER_FAILED" proc="threedo_process_system_event"/>


    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_AVAILABILITY_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_AVAILABLE" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_UNAVAILABLE" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_REFRESHED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_NO_SIM_AVAILABLE" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_HOME_PLMN_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_IMSI_CHANGED" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SIM_CTRL_EVENT_DETECTED" proc="threedo_process_system_event"/>


        <RECEIVER id="EVT_ID_POST_KEY" proc="threedo_process_system_event"/>
        <RECEIVER id="EVT_ID_PRE_KEY_EVT_ROUTING" proc="threedo_process_system_event"/>
        <RECEIVER id="EVT_ID_POST_KEY_EVT_ROUTING" proc="threedo_process_system_event"/>


        <RECEIVER id="EVT_ID_SRV_CHARBAT_NOTIFY" proc="threedo_process_system_event"/>
        <RECEIVER id="EVT_ID_SRV_SHUTDOWN_DEINIT" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SHUTDOWN_NORMAL_START" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_SHUTDOWN_FINAL_DEINIT" proc="threedo_process_system_event"/>
    <RECEIVER id="EVT_ID_SRV_DTCNT_ACCT_UPDATE_IND" proc="threedo_process_system_event"/>

    <TIMER id="THREEDO_TIMER_PUBLIC_ID"/>
        <TIMER id="THREEDO_TIMER_KEEP_ACTIVE"/>
        <TIMER id="THREEDO_TIMER_SOCKET_RECONN"/>
        <TIMER id="THREEDO_TIMER_WAITING_ACK"/>
        <TIMER id="THREEDO_TIMER_FILE_UPLOAD_DELAY"/>
        <TIMER id="THREEDO_TIMER_FILE_DOWNLOAD_DELAY"/>
        <TIMER id="THREEDO_TIMER_ADORECD_LIMITE"/>



        <EVENT id="EVT_ID_THREEDO_PRE_EXEC" type="SENDER"/>
        <EVENT id="EVT_ID_THREEDO_PRE_EXIT" type="SENDER"/>
        <EVENT id="EVT_ID_THREEDO_EXITED" type="SENDER"/>
        <EVENT id="EVT_ID_THREEDO_DOMAIN_NAME_RESOLUTION" type="SENDER"/>
        <EVENT id="EVT_ID_THREEDO_SOCKET_EVENT_NOTIFY" type="SENDER"/>



</APP>
