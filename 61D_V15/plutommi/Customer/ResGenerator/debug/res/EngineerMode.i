# 1 "temp/res/EngineerMode.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/EngineerMode.c"
# 956 "temp/res/EngineerMode.c"
# 1 "../../mmi/inc/mmi_features.h" 1
# 69 "../../mmi/inc/mmi_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/mmi_features.h" 2
# 957 "temp/res/EngineerMode.c" 2
# 1 "../customerinc/custresdef.h" 1
# 195 "../customerinc/custresdef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/custresdef.h" 2
# 958 "temp/res/EngineerMode.c" 2
# 1 "../../../interface/hal/drv_sw_def/drv_sw_features_color.h" 1
# 74 "../../../interface/hal/drv_sw_def/drv_sw_features_color.h"
# 1 "../../../hal/drv_def/drv_features_color.h" 1
# 74 "../../../hal/drv_def/drv_features_color.h"
# 1 "../../../hal/drv_def/drv_features_chip_select.h" 1
# 168 "../../../hal/drv_def/drv_features_chip_select.h"
# 1 "../../../hal/drv_def/drv_features_6261.h" 1
# 169 "../../../hal/drv_def/drv_features_chip_select.h" 2
# 75 "../../../hal/drv_def/drv_features_color.h" 2
# 75 "../../../interface/hal/drv_sw_def/drv_sw_features_color.h" 2
# 959 "temp/res/EngineerMode.c" 2

<?xml version = "1.0" encoding="UTF-8"?>

<APP id = "APP_ENGINEERMODE1">

<!----------------- Include Area ---------------------------------------------->

    <INCLUDE file = "MMI_features.h"/>
    <INCLUDE file = "MMIDataType.h"/>
    <INCLUDE file = "GlobalResDef.h"/>




<!----------------- Common Area ----------------------------------------------->

    <!----------------- Timer Id ------------------------------------------>
    <TIMER id = "EM_NOTIFYDURATION_TIMER"/>
    <TIMER id = "EM_RINGTONE_HIGHLIGHT_TIMER"/>
    <TIMER id = "EM_GPRS_PING_TIMER"/>
    <TIMER id = "EM_GPRS_SOC_DEMO_APP_TIMER"/>

    <!----------------- Event Id ------------------------------------------>

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_GLOBAL_PROCESSING">Processing...</STRING>
    <STRING id = "STR_ID_EM_SIM_CARD_IS_NOT_AVAILABLE">This SIM card is not available</STRING>
    <STRING id = "STR_ID_EM_WARNING">Not allow or conflict!</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_SET_LSK">Set</STRING>
    <STRING id = "STR_ID_EM_NOTICE_SUCCESS_REBOO">Done.\nPower off in 3 sec</STRING>
    <STRING id = "STR_ID_EM_GLOBAL_AUTO">Auto</STRING>
    <STRING id = "STR_ID_EM_TRUE">True</STRING>
    <STRING id = "STR_ID_EM_FALSE">False</STRING>





    <!----------------- Screen Id ----------------------------------------->
    <SCREEN id = "GRP_ID_EM_ROOT"/>
    <SCREEN id = "SCR_ID_EM_CMN_SCRN"/>
    <SCREEN id = "SCR_ID_EM_DUALMODE_SWITCH_PROGRESS"/>

<!----------------- Menu Id ------------------------------------------->
    <MENUSET id = "MENUSET_ON_OFF">
        <MENUITEM id = "MENU_ID_EM_MISC_ON" str = "STR_GLOBAL_ON"/>
        <MENUITEM id = "MENU_ID_EM_MISC_OFF" str = "STR_GLOBAL_OFF"/>
    </MENUSET>

    <MENUSET id = "MENUSET_TRUE_FALSE">
        <MENUITEM id = "MENU_ID_EM_TRUE" str = "STR_ID_EM_TRUE"/>
        <MENUITEM id = "MENU_ID_EM_FALSE" str = "STR_ID_EM_FALSE"/>
    </MENUSET>

<!----------------- Level1 Menu : Mainmenu ------------------------------------>

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_APP_NAME">Engineering mode</STRING>

    <!----------------- Image Id ------------------------------------------>
    <IMAGE id = "IMG_ID_EM_MAIN_ICON">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\TitleBar\\\\TB_EM.png"</IMAGE>

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_MAIN_MENU" type = "APP_MAIN" str = "STR_ID_EM_APP_NAME">





        <MENUITEM_ID>MENU_ID_EM_DEVICE</MENUITEM_ID>






        <MENUITEM_ID>MENU_ID_EM_MISC</MENUITEM_ID>
# 1065 "temp/res/EngineerMode.c"
   </MENU>

<!----------------- EM Item End ----------------------------------------------->


<!----------------- Level2 Menu | Network Setting ----------------------------->
# 1130 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Network Setting | Network Info ---------------------------->
# 1191 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Network Setting | Band Selection--------------------------->
# 1259 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Network Setting | Prefer Rat ------------------------------>
# 1279 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Network Setting | Cell Lock ------------------------------->
# 1295 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Network Setting | Network WCDMA PREFERRED --------------------------->
# 1312 "temp/res/EngineerMode.c"
<!----------------- EM Item End ------------------------------------------->

<!----------------- Network Setting | IVSR -------------------------------->
# 1336 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Network Setting | Network Event --------------------------->
# 1371 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Network Setting | PLMN List ------------------------------->
# 1389 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Network Setting | Service selection ------------------------------->
# 1401 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Network Setting | Gprs Connection ------------------------------->







<!----------------- EM Item End ----------------------------------------------->


<!----------------- Network Setting | 3G_info --------------------------------->
# 1496 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Network Setting | TDD --------------------------------->
# 1577 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->




<!----------------- Level2 Menu | Device -------------------------------------->

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEVICE">Device</STRING>

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_DEVICE" type = "APP_SUB" str = "STR_ID_EM_DEVICE">

        <MENUITEM_ID>MENU_ID_EM_DEV_SET_UART</MENUITEM_ID>






        <MENUITEM_ID>MENU_ID_EM_MM_COLOR</MENUITEM_ID>
# 1624 "temp/res/EngineerMode.c"
        <MENUITEM_ID>MENU_ID_EM_DEV_DCM_MODE</MENUITEM_ID>



                <MENUITEM_ID>MENU_ID_EM_DEV_BQB_MODE</MENUITEM_ID>
# 1656 "temp/res/EngineerMode.c"
        <MENUITEM_ID>MENU_ID_EM_DEV_FM_RADIO</MENUITEM_ID>
# 1698 "temp/res/EngineerMode.c"
    </MENU>
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | GPIO ----------------------------------------->
# 1726 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | MM Color ----------------------------------------->

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEV_MM_COLOR">MM Color Adjust</STRING>
    <STRING id = "STR_ID_EM_DEV_SHARP">Sharpness</STRING>
    <STRING id = "STR_ID_EM_DEV_CONTRAST">Contrast</STRING>
    <STRING id = "STR_ID_EM_DEV_SATURATION">Saturation</STRING>
    <!----------------- Menu Id ------------------------------------------->
    <MENUITEM id = "MENU_ID_EM_MM_COLOR" str = "STR_ID_EM_DEV_MM_COLOR"/>

<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | Backlight ------------------------------------------>
# 1760 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Device | EINT ----------------------------------------->
# 1775 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | ADC ------------------------------------------>
# 1790 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | CLAM ----------------------------------------->
# 1804 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | HW Level ----------------------------------------->
# 1831 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | Set UART ----------------------------------------->

    <!----------------- Screen Id ----------------------------------------->
    <SCREEN id = "SCR_ID_EM_DEV_UART_POWER_ONOFF_MENU"/>

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEV_SET_UART">Set UART</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_SET_UART_SETTING">UART setting</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_SET_UART_TST_PS">TST-PS config</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_SET_UART_TST_L1">TST-L1 config</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_SET_UART_PS">PS config</STRING> <!-- Engineer Mode Menu Caption String -->


    <STRING id = "STR_ID_EM_DEV_USB_PORT">USB port</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_USB_PORT2">USB port 2</STRING> <!-- Engineer Mode Menu Caption String -->






    <STRING id = "STR_ID_EM_DEV_UART_1">UART 1</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_UART_2">UART 2</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_UART_3">UART 3</STRING> <!-- Engineer Mode Menu Caption String -->
# 1873 "temp/res/EngineerMode.c"
    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_DEV_SET_UART" type = "APP_SUB" str = "STR_ID_EM_DEV_SET_UART">
        <MENUITEM id = "MENU_ID_EM_DEV_UART_SETTING" str = "STR_ID_EM_DEV_SET_UART_SETTING"/>
    </MENU>


<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | Sleep Mode --------------------------------------->
# 1894 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | DCM Mode ----------------------------------------->


    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEV_DCM_MODE">DCM mode</STRING> <!-- Engineer Mode Menu Caption String -->

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_DEV_DCM_MODE" type = "APP_SUB" str = "STR_ID_EM_DEV_DCM_MODE" flag = "RADIO">
        <MENUITEM id = "MENU_ID_EM_DEV_DCM_MODE_ON" str = "STR_GLOBAL_ON"/>
        <MENUITEM id = "MENU_ID_EM_DEV_DCM_MODE_OFF" str = "STR_GLOBAL_OFF"/>
    </MENU>


<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | BQB Mode ----------------------------------------->


    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEV_BQB_MODE">BQB mode</STRING> <!-- Engineer Mode Menu Caption String -->

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_DEV_BQB_MODE" type = "APP_SUB" str = "STR_ID_EM_DEV_BQB_MODE" flag = "RADIO">
        <MENUITEM id = "MENU_ID_EM_DEV_BQB_MODE_ON" str = "STR_GLOBAL_ON"/>
        <MENUITEM id = "MENU_ID_EM_DEV_BQB_MODE_OFF" str = "STR_GLOBAL_OFF"/>
    </MENU>


<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | NAND Format -------------------------------------->
# 1942 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | MTV Setting --------------------------------------->
# 1973 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Device | Video Setting --------------------------------------->
# 2024 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Device | Video Streaming setting --------------------------------------->
# 2046 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Device | USB ---------------------------------------------->
# 2083 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | Memory TEST ---------------------------------------------->
# 2104 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | FM Radio ----------------------------------------->

    <SCREEN id = "SCR_ID_EM_DEV_FM_RADIO_RSSI_INFO"/>




    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_DEV_FM_RADIO">FM radio</STRING> <!-- Engineer Mode Menu Caption String -->






    <STRING id = "STR_ID_EM_DEVICE_FM_RADIO_STATUS">Radio status</STRING> <!-- Engineer Mode Menu Caption String -->
    <STRING id = "STR_ID_EM_DEV_FMR_FRE_MHZ">Fre(KHz)</STRING> <!-- Engineer Mode FM Radio Stereo Menu Caption String -->
# 2133 "temp/res/EngineerMode.c"
    <STRING id = "STR_ID_EM_DEVICE_FM_RADIO_COMN_CMD">Radio common cmd</STRING>
    <STRING id = "STR_ID_EM_DEVICE_FM_RADIO_COMN_CMD_INPUT">Input:</STRING>
    <STRING id = "STR_ID_EM_DEVICE_FM_RADIO_COMN_CMD_OUTPUT">Cmd output</STRING>

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_DEV_FM_RADIO" type = "APP_SUB" str = "STR_ID_EM_DEV_FM_RADIO">
        <MENUITEM id = "MENU_ID_EM_DEVICE_FM_RADIO_STATUS" str = "STR_ID_EM_DEVICE_FM_RADIO_STATUS"/>



        <MENUITEM id = "MENU_ID_EM_DEVICE_FM_RADIO_COMN_CMD" str = "STR_ID_EM_DEVICE_FM_RADIO_COMN_CMD"/>
    </MENU>

<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | TST Output Mode----------------------------------->
# 2165 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->




<!----------------- Device | USB Logging -------------------------------------->
# 2190 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | CAM AF Constshot --------------------------------->
# 2294 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->




<!----------------- Device | Mini GPS ----------------------------------------->
# 2475 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | Motion ------------------------------------------->
# 2583 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Device | Speed lcd ------------------------------------>
# 2598 "temp/res/EngineerMode.c"
<!----------------- Speech lcd End ---------------------------------------->

<!----------------- Device | PMU register setting ------------------------->
# 2614 "temp/res/EngineerMode.c"
<!----------------- Speech lcd End ---------------------------------------->

<!----------------- Device | PXS sensor ------------------------------------------->
# 2655 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Device | WLAN EAP Mode ------------------------------------>
# 2686 "temp/res/EngineerMode.c"
<!------------------------- Switch End ---------------------------------------->


<!----------------- Device | NFC ----------------------------------------->
# 2914 "temp/res/EngineerMode.c"
<!----------------- Device | NFC end ------------------------------------->
<!----------------- EM Item End ----------------------------------------------->
# 3198 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Level2 Menu | GPRS Act ------------------------------------>
# 3372 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Level2 Menu | Misc.---------------------------------------->

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_MISC">Misc.</STRING>

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_MISC" type = "APP_SUB" str = "STR_ID_EM_MISC">
# 3403 "temp/res/EngineerMode.c"
        <MENUITEM_ID>MENU_ID_EM_MISC_MEMORY_DUMP</MENUITEM_ID>
# 3491 "temp/res/EngineerMode.c"
        <MENUITEM_ID>MENU_ID_EM_MRE</MENUITEM_ID>





    </MENU>

<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Auto Answer --------------------------------------->
# 3512 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | High Speed SIM ------------------------------------>
# 3526 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Misc. | Backlight Mode ------------------------------------>
# 3539 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Auto Reset -------------------------------->
# 3552 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Misc. | RAM Test ------------------------------------------>







<!----------------- EM Item End ----------------------------------------------->



<!----------------- Misc. | Memory Dump --------------------------------------->

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_MISC_MEMORY_DUMP">Memory dump</STRING>

    <!----------------- Menu Id ------------------------------------------->
    <MENU id = "MENU_ID_EM_MISC_MEMORY_DUMP" type = "APP_SUB" str = "STR_ID_EM_MISC_MEMORY_DUMP" flag = "RADIO">
        <MENUSET_ID>MENUSET_ON_OFF</MENUSET_ID>
    </MENU>

<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | AMR ----------------------------------------------->
# 3592 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Misc. | Internet Application ------------------------------>
# 3664 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Misc. | Touch Screen -------------------------------------->
# 3688 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Video High Bit Rate ------------------------------->
# 3702 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Misc. | Cell reselection ---------------------------------->
# 3722 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Cascade Menu ---------------------------------------->
# 3735 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Check Drive --------------------------------------->
# 3752 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Software Tracer ----------------------------------->
# 3766 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | DM Self Register --------------------------------------->
# 3806 "temp/res/EngineerMode.c"
<!----------------- DM Self Register Item End ----------------------------->

<!----------------- Misc. | DM Self Register --------------------------------------->
# 3835 "temp/res/EngineerMode.c"
<!----------------- DM Self Register Item End ----------------------------->


<!----------------- Misc. | DCD Network Paramter ------------------------------>
# 3867 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | FOTA Debug ---------------------------------------->
# 3894 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | DM Add APN ---------------------------------------->
# 3915 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Video Parameter ----------------------------------->
# 3940 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->
# 3975 "temp/res/EngineerMode.c"
<!----------------- Misc. | Software Patch ------------------------------------>
# 3986 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Level3 Menu | JAVA ---------------------------------------->
# 4012 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->
# 4167 "temp/res/EngineerMode.c"
<!----------------- JAVA | Heap Size Setting ---------------------------------->
# 4201 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- JAVA | Performance ---------------------------------------->
# 4229 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- JAVA | JUI Debug ------------------------------------------>
# 4253 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- MED Memory Setting ------------------------------------------>
# 4271 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Misc | Rf desense ----------------------------------------->
# 4288 "temp/res/EngineerMode.c"
<!----------------- Misc | Rf desense end ------------------------------------->


<!----------------- X download ----------------------------------------------->
# 4303 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Level3 Menu | MRE ---------------------------------------->
    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_MRE">MRE</STRING>

    <!----------------- Screen Id ----------------------------------------->
    <SCREEN id = "SCR_ID_EM_MISC_MRE_INFO"></SCREEN>

    <!----------------- Menu Id ------------------------------------------->

    <MENUITEM id = "MENU_ID_EM_MRE" str = "STR_ID_EM_MRE"/>


<!----------------- EM Item End ----------------------------------------------->


<!----------------- Misc. | Power on CPU query ------------------------------->
# 4337 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Level2 Menu | Debug Info ---------------------------------->
# 4362 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Debug Info | Last Exception ------------------------------->
# 4374 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Debug Info | System Stats --------------------------------->
# 4391 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Debug Info | Drive Letter --------------------------------->
# 4408 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Iperf ----------------------------------------------------->
# 4494 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Level2 Menu | Bluetooth ----------------------------------->
# 4527 "temp/res/EngineerMode.c"
<!----------------- Bluetooth Item End ----------------------------------------------->



<!----------------- Bluetooth | General Test --------------------------------->

    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_BT_BD_ADDR">BD address</STRING>
# 4637 "temp/res/EngineerMode.c"
<!----------------- Bluetooth | Avrcp connect tg actively -------------------->







<!----------------- Avrcp connect tg actively Item End ------------------------>



<!----------------- Bluetooth | RF Test --------------------------------------->
# 4708 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Bluetooth | Get Chip Version ------------------------------>
# 4721 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Bluetooth | UPF IOT Test ---------------------------------->
# 4735 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->





<!----------------- Level2 Menu | Profiling ----------------------------------->
    <SCREEN id = "GRP_ID_EM_MISC_MEDIA_MEMORY_SETTING"></SCREEN>
# 4794 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Profiling | SWLA ----------------------------->
# 4817 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->






<!----------------- Profiling | GDI Profiling -------------------------->
# 4887 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Profiling | MultiMedia Profiling -------------------------->
# 4955 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Profiling | Framework Profiling -------------------------->
# 4978 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Profiling | UI Benchmark ---------------------------------->
# 4997 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Profiling | Memory Monitor -------------------------------->
# 5030 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Profiling | Memory Stat. ---------------------------------->
# 5054 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Profiling | Venus Debug Panel ----------------------------->
# 5080 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- Profiling | FS Test -------------------------------------->
# 5094 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Profiling | CSB Browser ----------------------------------->
# 5110 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Profiling | Multimedia | xxx --------------------------------->
    <!----------------- Screen Id ----------------------------------------->
    <SCREEN id = "GRP_ID_EM_PROFILING_AUTO_PLAY"></SCREEN>
    <SCREEN id = "GRP_ID_EM_PROFILING_VDOPLY"></SCREEN>
# 5124 "temp/res/EngineerMode.c"
    <SCREEN id = "GRP_ID_EM_PROFILING_CAMERA"></SCREEN>
    <SCREEN id = "GRP_ID_EM_PROFILING_CAMERA_APP_CAPTURE"></SCREEN>
    <SCREEN id = "GRP_ID_EM_PROFILING_IMAGE_DECODE"></SCREEN>
    <SCREEN id = "GRP_ID_EM_PROFILING_VDOREC"></SCREEN>

    <!----------------- String Id ------------------------------------------->
# 5158 "temp/res/EngineerMode.c"
    <!----------------- String Id ------------------------------------------->
    <STRING id = "STR_ID_EM_PROFILING_EXTRA_LAYER">Extra layer</STRING>
    <STRING id = "STR_ID_EM_PROFILING_ROTATE">Rotate</STRING>
    <STRING id = "STR_ID_EM_PROFILING_PLAY_SIZE">Play size</STRING>
    <STRING id = "STR_ID_EM_PROFILING_TVOUT_MODE">TV-Out mode</STRING>
    <STRING id = "STR_ID_EM_PROFILING_ROTATE_0">Rotate 0</STRING>
    <STRING id = "STR_ID_EM_PROFILING_ROTATE_90">Rotate 90</STRING>
    <STRING id = "STR_ID_EM_PROFILING_ROTATE_180">Rotate 180</STRING>
    <STRING id = "STR_ID_EM_PROFILING_ROTATE_270">Rotate 270</STRING>
    <STRING id = "STR_ID_EM_PROFILING_TV_FULL_SCREEN">Full screen</STRING>
    <STRING id = "STR_ID_EM_PROFILING_TV_LCD_SCREEN">LCD screen</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_SQCIF">SQCIF (128x96)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_QCIF">SQCIF (128x96)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_CIF">CIF (352x288)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_QQVGA">QQVGA (160x120)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_QVGA">QVGA (320x240)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_VGA">VGA (640x480)</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SIZE_LCD">LCD Size</STRING>
    <STRING id = "STR_ID_EM_PROFILING_RECORD_SIZE">Record Size</STRING>
    <STRING id = "STR_ID_EM_PROFILING_PREVIEW_SIZE">Preview Size</STRING>
    <STRING id = "STR_ID_EM_PROFILING_SETTING">Setting</STRING>

    <!----------------- Menu Id ------------------------------------------->

    <MENU id="MENU_ID_EM_PROFILING_OPTION" type="OPTION" str="STR_GLOBAL_OPTIONS">
        <MENUITEM_ID>MENU_ID_EM_PROFILING_START</MENUITEM_ID>
        <MENUITEM_ID>MENU_ID_EM_PROFILING_SETTING</MENUITEM_ID>
    </MENU>
    <MENUITEM id="MENU_ID_EM_PROFILING_START" str="STR_GLOBAL_START"/>
    <MENUITEM id="MENU_ID_EM_PROFILING_SETTING" str="STR_GLOBAL_SETTINGS"/>



<!----------------- EM Item End ----------------------------------------------->


<!----------------- Profiling | Gesture Test --------------------------------->
# 5204 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->

<!----------------- Level2 Menu | RF Test Tool -------------------------------->
    <!----------------- Screen Id ----------------------------------------->
    <SCREEN id = "SCR_ID_EM_DUALMODE_SWITCH_PROGRESS"/>

    <!----------------- String Id ----------------------------------------->



    <STRING id = "STR_ID_EM_RF_TEST_GSM_CONFIRM_NOTIFY_TEST">Wifi must be off. Will power off after test.</STRING> <!-- Engineer Mode RF Test GSM Menu Caption String-->

    <!----------------- Menu Id ------------------------------------------->
# 5336 "temp/res/EngineerMode.c"
<!----------------- RF Test Tool | GSM RF Test -------------------------------->
# 5378 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- RF Test Tool | WIFI RF Test ------------------------------->
# 5509 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- Level2 Menu | VoIP ---------------------------------------->
# 5553 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->



<!----------------- VoIP | VoIP Settings -------------------------------------->






<!----------------- EM Item End ----------------------------------------------->



<!----------------- VoIP | SIP Support ---------------------------------------->
# 5579 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- VoIP | SIP Require ---------------------------------------->
# 5593 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->


<!----------------- VoIP | VoIP Log ------------------------------------------->
# 5608 "temp/res/EngineerMode.c"
<!----------------- EM Item End ----------------------------------------------->




<!----------------- Audio | CacheData ----------------------------------------->
    <CACHEDATA type="double" id="NVRAM_AUDIO_DEBUG_INFO1" restore_flag="FALSE" restore_id="@NVRAM_CACHE_ID_RESERVED">
        <DEFAULT_VALUE> [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Double cache </DESCRIPTION>
    </CACHEDATA>

    <CACHEDATA type="double" id="NVRAM_AUDIO_DEBUG_INFO2" restore_flag="FALSE" restore_id="@NVRAM_CACHE_ID_RESERVED">
        <DEFAULT_VALUE> [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Double cache </DESCRIPTION>
    </CACHEDATA>

    <CACHEDATA type="double" id="NVRAM_AUDIO_DEBUG_INFO3" restore_flag="FALSE" restore_id="@NVRAM_CACHE_ID_RESERVED">
        <DEFAULT_VALUE> [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Double cache </DESCRIPTION>
    </CACHEDATA>

    <CACHEDATA type="double" id="NVRAM_AUDIO_DEBUG_INFO4" restore_flag="FALSE" restore_id="@NVRAM_CACHE_ID_RESERVED">
        <DEFAULT_VALUE> [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Double cache </DESCRIPTION>
    </CACHEDATA>



<!----------------- EM Item End ----------------------------------------------->

<!----------------- GDI image decode ------------------------------------------>
# 5657 "temp/res/EngineerMode.c"
    <!----------------- String Id ----------------------------------------->
    <STRING id = "STR_ID_EM_IMEI_SETTING">IMEI Setting</STRING>
    <STRING id = "STR_ID_EM_IMEI1">IMEI1</STRING>
    <STRING id = "STR_ID_EM_IMEI2">IMEI2</STRING>
    <TIMER id="MMI_EM_IMEI_TIMER"/>

<!----------------- EM Item End ----------------------------------------------->

<!----------------- XML END --------------------------------------------------->

</APP>
