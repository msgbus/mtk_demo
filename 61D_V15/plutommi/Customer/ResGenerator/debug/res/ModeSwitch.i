# 1 "temp/res/ModeSwitch.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/ModeSwitch.c"
# 55 "temp/res/ModeSwitch.c"
# 1 "../../mmi/inc/MMI_features.h" 1
# 69 "../../mmi/inc/MMI_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/MMI_features.h" 2
# 56 "temp/res/ModeSwitch.c" 2
# 1 "../customerinc/CustResDef.h" 1
# 195 "../customerinc/CustResDef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/CustResDef.h" 2
# 57 "temp/res/ModeSwitch.c" 2
# 1 "../../mmi/setting/settinginc/PhoneSetupSlim.h" 1
# 59 "../../mmi/setting/settinginc/PhoneSetupSlim.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 60 "../../mmi/setting/settinginc/PhoneSetupSlim.h" 2
# 58 "temp/res/ModeSwitch.c" 2

<?xml version = "1.0" encoding = "UTF-8"?>

<APP id = "APP_MODE_SWITCH">

    <INCLUDE file = "mmi_rp_app_mainmenu_def.h"/>
    <INCLUDE file = "GlobalResDef.h"/>

    <!--------------------------------------------------------------------------------------------------------------------->
    <!--String Resource Area-->
    <!--String Resource Area-->
# 85 "temp/res/ModeSwitch.c"
    <STRING id="STR_ID_MODE_SWITCH_SETTING_2SIM"/>







    <STRING id="STR_ID_MODE_SWITCH_DUALCARD"/>
    <STRING id="STR_ID_MODE_SWITCH_CARD1"/>
    <STRING id="STR_ID_MODE_SWITCH_CARD2"/>
    <STRING id="STR_ID_MODE_SWITCH_FLIGHTMODE"/>

    <STRING id="STR_ID_MODE_SWITCH_QUERY_CONFIRM"/>
    <STRING id="STR_ID_MODE_SWITCH_QUERY_REMOVED_WITH_VALID_NW"/>


    <STRING id="STR_ID_MODE_SWITCH_RESET_MODE_CONFIRM"/>






    <STRING id="STR_ID_MODE_SWITCH_SWITCHING"/>
    <STRING id="STR_ID_MODE_SWITCH_INVALID"/>





        <!--End String Resource Area-->
    <!--End String Resource Area-->
        <!--------------------------------------------------------------------------------------------------------------------->


        <!--------------------------------------------------------------------------------------------------------------------->
        <!--Image Resource Area-->
        <!--Image Resource Area-->
# 134 "temp/res/ModeSwitch.c"
        <!--End Image Resource Area-->
        <!--End Image Resource Area-->
        <!--------------------------------------------------------------------------------------------------------------------->


        <!--------------------------------------------------------------------------------------------------------------------->
        <!--Audio Resource Area-->
        <!--Audio Resource Area-->
        <!--End Audio Resource Area-->
        <!--End Audio Resource Area-->
        <!--------------------------------------------------------------------------------------------------------------------->


        <!--------------------------------------------------------------------------------------------------------------------->
        <!--Media Resource Area-->
        <!--Media Resource Area-->
        <!--End Media Resource Area-->
        <!--End Media Resource Area-->
        <!--------------------------------------------------------------------------------------------------------------------->


        <!--------------------------------------------------------------------------------------------------------------------->
        <!--Screen ID Area-->
        <!--Screen ID Area-->
    <SCREEN id = "SCR_ID_MODE_SWITCH_START"/>
    <SCREEN id = "SCR_ID_MODE_SWITCH_SIM_SELECTION"/>
    <SCREEN id = "SCR_ID_MODE_SWITCH_QUERY_CONFIRM"/>
    <SCREEN id = "SCR_ID_MODE_SWITCH_SWITCH_PROGRESS"/>
    <SCREEN id = "GRP_ID_MODE_SWITCH_SETTING"/>
    <SCREEN id = "GRP_ID_MODE_SWITCH_TYPE"/>
    <SCREEN id = "GRP_ID_MODE_SWITCH_QUERY"/>
    <SCREEN id = "SCR_ID_MODE_SWITCH_END"/>






    <!--End Screen Resource Area-->
    <!--End Screen Resource Area-->
    <!--------------------------------------------------------------------------------------------------------------------->


    <!--------------------------------------------------------------------------------------------------------------------->
    <!--Menu Item Area-->
    <!--Menu Item Area-->
# 209 "temp/res/ModeSwitch.c"
        <MENU id="MENU_ID_MODE_SWITCH_SETTING" parent = "MAIN_MENU_SETTINGS_MENUID" type="APP_MAIN" str="STR_ID_MODE_SWITCH_SETTING_2SIM" highlight="mmi_highlight_mode_switch_setting">


            <MENUITEM_ID>MENU_ID_MODE_SWITCH_DUALCARD</MENUITEM_ID>
            <MENUITEM_ID>MENU_ID_MODE_SWITCH_CARD1</MENUITEM_ID>
            <MENUITEM_ID>MENU_ID_MODE_SWITCH_CARD2</MENUITEM_ID>
            <MENUITEM_ID>MENU_ID_MODE_SWITCH_FLIGHTMODE</MENUITEM_ID>
        </MENU>

        <MENUITEM id="MENU_ID_MODE_SWITCH_DUALCARD" str="STR_ID_MODE_SWITCH_DUALCARD"/>
        <MENUITEM id="MENU_ID_MODE_SWITCH_CARD1" str="STR_ID_MODE_SWITCH_CARD1"/>
        <MENUITEM id="MENU_ID_MODE_SWITCH_CARD2" str="STR_ID_MODE_SWITCH_CARD2"/>
        <MENUITEM id="MENU_ID_MODE_SWITCH_FLIGHTMODE" str="STR_ID_MODE_SWITCH_FLIGHTMODE"/>
# 261 "temp/res/ModeSwitch.c"
        <RECEIVER id="EVT_ID_SRV_NW_INFO_SERVICE_AVAILABILITY_CHANGED" proc="mmi_mode_switch_query_nw_attach_hdlr"/>
# 272 "temp/res/ModeSwitch.c"
        <!--End Menu Item Area-->
        <!--End Menu Item Area-->
        <!--------------------------------------------------------------------------------------------------------------------->

</APP>
