# 1 "temp/res/VDORecRes.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/VDORecRes.c"

# 1 "../../mmi/inc/MMI_features.h" 1
# 69 "../../mmi/inc/MMI_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/MMI_features.h" 2
# 3 "temp/res/VDORecRes.c" 2

# 1 "../../mmi/inc/MMI_features_video.h" 1
# 738 "../../mmi/inc/MMI_features_video.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 739 "../../mmi/inc/MMI_features_video.h" 2
# 748 "../../mmi/inc/MMI_features_video.h"
# 1 "../../mtkapp/camera/camerainc/CameraSensorCapability.h" 1
# 749 "../../mmi/inc/MMI_features_video.h" 2
# 5 "temp/res/VDORecRes.c" 2
# 1 "../customerinc/CustResDef.h" 1
# 195 "../customerinc/CustResDef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/CustResDef.h" 2
# 6 "temp/res/VDORecRes.c" 2



<?xml version="1.0" encoding="UTF-8"?>


<APP id="APP_VDOREC"
# 30 "temp/res/VDORecRes.c"
   name = "STR_ID_VDOREC_APP_NAME"


>




    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h,MainMenuDef.h"/>
    <INCLUDE file="mmi_rp_app_mainmenu_def.h"/>






        <STRING id="STR_ID_VDOREC_APP_NAME"/>
        <STRING id="STR_ID_VDOREC_CAMCODER_SETTING"/>
        <STRING id="STR_ID_VDOREC_VIDEO_SETTING"/>
        <STRING id="STR_ID_VDOREC_SIZE_LIMIT"/>
        <STRING id="STR_ID_VDOREC_TIME_LIMIT"/>
        <STRING id="STR_ID_VDOREC_NOTIFY_STORAGE_NOT_READY"/>
    <STRING id="STR_ID_VDOREC_NOTIFY_ERROR"/>

    <STRING id="STR_ID_VDOREC_NOTIFY_STORAGE_TOO_SLOW"/>
    <STRING id="STR_ID_VDOREC_NOTIFY_SYSTEM_TOO_BUSY"/>
# 112 "temp/res/VDORecRes.c"
                <STRING id="STR_ID_VDOREC_VIDEO_QTY"/>


                                <STRING id="STR_ID_VDOREC_VIDEO_QTY2_LOW"/>


                                <STRING id="STR_ID_VDOREC_VIDEO_QTY2_NORMAL"/>


                                <STRING id="STR_ID_VDOREC_VIDEO_QTY2_HIGH"/>


                                <STRING id="STR_ID_VDOREC_VIDEO_QTY2_FINE"/>
# 143 "temp/res/VDORecRes.c"
                <STRING id="STR_ID_VDOREC_BANDING"/>
                <STRING id="STR_ID_VDOREC_BANDING_50HZ"/>
                <STRING id="STR_ID_VDOREC_BANDING_60HZ"/>
# 295 "temp/res/VDORecRes.c"
        <STRING id="STR_ID_VDOREC_TO_CAMERA"/>
# 316 "temp/res/VDORecRes.c"
                <IMAGE id="IMG_ID_VDOREC_APP_ICON">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""timer_num_0.bmp"</IMAGE>
# 347 "temp/res/VDORecRes.c"
        <IMAGE id="IMG_ID_VDOREC_SK_RECORD">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record.bmp"</IMAGE>
        <IMAGE id="IMG_ID_VDOREC_SK_RECORD_SEL">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record.bmp"</IMAGE>
        <IMAGE id="IMG_ID_VDOREC_SK_PAUSE">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record_pause.bmp"</IMAGE>
        <IMAGE id="IMG_ID_VDOREC_SK_PAUSE_SEL">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record_pause.bmp"</IMAGE>
        <IMAGE id="IMG_ID_VDOREC_SK_RESUME">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record.bmp"</IMAGE>
        <IMAGE id="IMG_ID_VDOREC_SK_RESUME_SEL">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Camera\\\\Camera\\\\OSD\\\\""record.bmp"</IMAGE>
# 618 "temp/res/VDORecRes.c"
        <MENU id="MAIN_MENU_OPTION_VDOREC_APP" str="STR_ID_VDOREC_APP_NAME" highlight="mmi_vdorec_hilight_app" img="IMG_ID_VDOREC_APP_ICON"/>
        <MENU id="MENU_ID_VDOREC_APP" str="STR_ID_VDOREC_APP_NAME" highlight="mmi_vdorec_hilight_app" img="IMG_ID_VDOREC_APP_ICON" shortcut="ON" shortcut_img="IMG_ID_VDOREC_APP_ICON" launch="mmi_vdorec_lauch"/>


    <MENU id="MENU_ID_VDOREC_OPTION" type="OPTION" str="STR_GLOBAL_OPTIONS">
                <MENUITEM_ID>MENU_ID_VDOREC_TO_CAMERA</MENUITEM_ID>
                <MENUITEM_ID>MENU_ID_VDOREC_CAMCODER_SETTING</MENUITEM_ID>
                <MENUITEM_ID>MENU_ID_VDOREC_VIDEO_SETTING</MENUITEM_ID>
        <MENUITEM_ID>MENU_ID_VDOREC_WHITE_BALANCE</MENUITEM_ID>
                <MENUITEM_ID>MENU_ID_VDOREC_EFFECT_SETTING</MENUITEM_ID>
                <MENUITEM_ID>MENU_ID_VDOREC_SELF_RECORD</MENUITEM_ID>
                <MENUITEM_ID>MENU_ID_VDOREC_STORAGE</MENUITEM_ID>
            <MENUITEM_ID>MENU_ID_VDOREC_RESTORE_DEFAULT</MENUITEM_ID>
        </MENU>


        <MENUITEM id="MENU_ID_VDOREC_TO_CAMERA" str="STR_ID_VDOREC_TO_CAMERA"/>





                <MENUITEM id="MENU_ID_VDOREC_CAMCODER_SETTING" str="STR_ID_VDOREC_CAMCODER_SETTING"/>
                <MENUITEM id="MENU_ID_VDOREC_VIDEO_SETTING" str="STR_ID_VDOREC_VIDEO_SETTING"/>
# 722 "temp/res/VDORecRes.c"
                <MENUITEM id="MENU_ID_VDOREC_STORAGE" str="STR_GLOBAL_STORAGE" hint="mmi_vdorec_hint_storage"/>
# 779 "temp/res/VDORecRes.c"
    <SCREEN id="SCR_ID_VDOREC_APP"/>
    <SCREEN id="SCR_ID_VDOREC_OPTION"/>
    <SCREEN id="SCR_ID_VDOREC_CAMCODER_SETTING"/>
    <SCREEN id="SCR_ID_VDOREC_VIDEO_SETTING"/>
    <SCREEN id="SCR_ID_VDOREC_SAVING"/>
    <SCREEN id="SCR_ID_VDOREC_VIDEO_WB"/>
    <SCREEN id="SCR_ID_VDOREC_EFFECT_SETTING"/>
    <SCREEN id="SCR_ID_VDOREC_RESTORE_DEFAULT"/>
    <SCREEN id="SCR_ID_VDOREC_SAVE_CONFIRM"/>
    <SCREEN id="SCR_ID_VDOREC_PREVIEW_OPTION"/>
    <SCREEN id="SCR_ID_VDOREC_SAVED_PREVIEW"/>
    <SCREEN id="SCR_ID_VDOREC_SEND"/>
    <SCREEN id="SCR_ID_VDOREC_STORAGE"/>
    <SCREEN id="SCR_ID_VDOREC_END"/>







    <CACHEDATA type="short" id="NVRAM_VDOREC_FILENAME_SEQ_NO">
        <DEFAULT_VALUE> [0xFF, 0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Short Cache </DESCRIPTION>
    </CACHEDATA>







        <RECEIVER id="EVT_ID_USB_ENTER_MS_MODE" proc="mmi_vdorec_usb_mode_hdlr2"/>






</APP>
