# 1 "temp/res/app_gb_winguo_u.c"
# 1 "<built-in>"
# 1 "<command line>"
# 1 "temp/res/app_gb_winguo_u.c"

# 1 "../../mmi/inc/MMI_features.h" 1
# 69 "../../mmi/inc/MMI_features.h"
# 1 "../../mmi/inc/MMI_features_switch.h" 1
# 67 "../../mmi/inc/MMI_features_switch.h"
# 1 "../../mmi/inc/MMI_features_type.h" 1
# 68 "../../mmi/inc/MMI_features_switch.h" 2
# 70 "../../mmi/inc/MMI_features.h" 2
# 3 "temp/res/app_gb_winguo_u.c" 2
# 1 "../customerinc/custresdef.h" 1
# 195 "../customerinc/custresdef.h"
# 1 "../../mmi/inc/MMI_features.h" 1
# 196 "../customerinc/custresdef.h" 2
# 4 "temp/res/app_gb_winguo_u.c" 2


<?xml version="1.0" encoding="UTF-8"?>


<APP id="APP_GB_WINGUO"
# 19 "temp/res/app_gb_winguo_u.c"
    >



    <!--Include Area-->
    <INCLUDE file="GlobalResDef.h"/>

    <!-----------------------------------------------------String Resource Area----------------------------------------------------->


    <STRING id="STR_GB_WINGUO_LOG"/>
    <STRING id="STR_GB_WINGUO_SEARCH"/>
    <STRING id="STR_GB_WINGUO_HOMEPAGE"/>
    <STRING id="STR_GB_WINGUO_NET_PRESS_UP"/>

    <!-----------------------------------------------------Image Resource Area------------------------------------------------------>


    <IMAGE id="IMG_WG_WINGUO_LOGO_OUT">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Guobi\\\\Winguo\\\\IMG_GB_WG.png"</IMAGE>
    <IMAGE id="IMG_GB_WINGUO_SKINWHITE_SEARCH">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Guobi\\\\Winguo\\\\skinwhite_search.png"</IMAGE>
    <IMAGE id="IMG_WG_WINGUO_ICON">"..\\\\..\\\\Customer\\\\Images\\\\PLUTO240X320""\\\\MainLCD\\\\Guobi\\\\Winguo\\\\winguo_icon.png"</IMAGE>



    <SCREEN id="GRP_GB_WINGUO_SCREEN"/>
    <SCREEN id="SCR_GB_WINGUO_SCREEN"/>

    <!------------------------------------------------------Menu Resource Area------------------------------------------------------>


        <MENU id="MENU_ID_SEARCH_WEB_WINGUO_ENTRY" str="STR_GB_WINGUO_LOG" img="IMG_WG_WINGUO_LOGO_OUT" highlight="GB_WG_Search_Web_Highlight_Hdlr"></MENU>

</APP>
