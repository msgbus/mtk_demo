/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 * FactoryModeQuickTest.c
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *   This file is for Factory Mode Quick test
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 ********************************************************************************/
#include "MMI_features.h"
	 
#ifdef __MMI_FACTORY_MODE__
/*****************************************************************************
* Include Files                                                                
*****************************************************************************/
#if defined(__DCM_WITH_COMPRESSION_MMI_POOL_A__)
#include "dcmgr.h"
#elif defined (__DCM_WITH_COMPRESSION_MMI_AP__) && defined(__MTK_TARGET__)
#include "MMI_ap_dcm_config.h"  
#endif /* __DCM_WITH_COMPRESSION_MMI_POOL_A__ */
    
#if defined(__MMI_AP_DCM_FM__)
#pragma arm section rodata = "DYNAMIC_CODE_FM_RODATA" , code = "DYNAMIC_CODE_FM_ROCODE"
#elif defined(__DCM_WITH_COMPRESSION_MMI_POOL_A__)
#pragma arm section rodata = "DYNAMIC_CODE_COSMOS_FM_RODATA" , code = "DYNAMIC_CODE_COSMOS_FM_ROCODE"
#endif /* #ifdef __MMI_AP_DCM_FM__ */


#include "MMIDataType.h"
#include "kal_general_types.h"
#include "gdi_datatype.h"
#include "gui_data_types.h"
#include "GlobalConstants.h"
#include "ps_public_struct.h"
#include "stack_config.h"
#include "mmi_frm_input_gprot.h"
#include "stack_msgs.h"
#include "mmi_frm_events_gprot.h"
#include "GpioSrvGprot.h"
#include "gpiosrvgprot.h"
#include "Unicodexdcl.h"
#include "stdio.h"
#include "CustDataRes.h"
#include "GlobalResDef.h"

#include "TimerEvents.h"
#include "mmi_frm_timer_gprot.h"
#include "wgui_categories_util.h"
#include "kal_public_api.h"
#include "mmi_frm_history_gprot.h"
#include "CustMenuRes.h"
#include "GlobalMenuItems.h"
#include "wgui_categories_list.h"
#include "gui_typedef.h"
#include "wgui_include.h"
#include "string.h"
#include "mmi_frm_nvram_gprot.h"
#include "wgui_categories_text_viewer.h"
#include "nvram_defs.h"
#include "gui.h"
#include "FontRes.h"
#include "gdi_include.h"
#include "wgui_categories.h"
#include "custom_mmi_default_value.h"
#include "wgui.h"
#include "mmi_frm_scenario_gprot.h"
#include "mmi_msg_struct.h"
#include "AlarmFrameworkProt.h"
#include "mmi_res_range_def.h"
#include "mmi_frm_queue_gprot.h"
#include "app_ltlcom.h"
#include "wgui_touch_screen.h"
#include "gui_config.h"
#include "SensorSrvGport.h"


#include "ProtocolEvents.h"
#include "CommonScreens.h"
#include "PixcomFontEngine.h"

#include "resource_audio.h"
#include "IdleAppDef.h"
//#include "FactoryModeDef.h"

#include "EngineerModeUtil.h"
#include "EngineerModeGprot.h"
#include "mdi_datatype.h"
#include "mdi_include.h"
#include "mdi_audio.h"
#include "CommonScreens.h"
#include "fmt_struct.h"

#include "mmi_rp_srv_gpio_def.h"
#include "wgui_categories.h"

#include "dcl.h"


#if defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__)
#include "Msdc_def.h"
#endif 


#if defined __MMI_FM_CAMERA_PREVIEW__

#include "lcd_if.h"
#include "lcd_sw_rnd.h" /* LCD layer enable flag */
#include "mdi_datatype.h"
#include "med_api.h"    /* media task camera module */
#include "MMI_features_camera.h"
#include "resource_camera_skins.h"
#include "mdi_camera.h"
#include "CameraApp.h"

#if defined(ISP_SUPPORT)
#include "image_sensor.h"
#endif 
#endif /* __MMI_FM_CAMERA_PREVIEW__ */ 

#include "init.h"
#include "init_public.h"

#include "device.h"
#include "custom_em.h"
#ifdef __MMI_FM_KEYPAD_TEST__
#include "custom_fm.h"
#endif
#include "custom_equipment.h"
//#include "custom_hw_default.h"
#include "nvram_data_items.h"
#include "nvram_interface.h"
#include "lcd_sw_inc.h"
#include "resource_verno.h"

#ifdef __MTK_TARGET__
#include "l1audio.h"
#else 
#define L1SP_Tones kal_uint16
#endif 

#include "datetimetype.h"
#include "app_datetime.h"

#ifdef __MMI_FM_RADIO__
#include "aud_defs.h"
#ifdef __PLUTO_MMI_PACKAGE__
#include "mmi_rp_app_fmrdo_def.h"
#endif /*__PLUTO_MMI_PACKAGE__*/
#endif

#include "adc_channel.h"
#include "ShutdownSrvGprot.h"

#include "mmi_rp_app_factorymode_def.h"
#include "MenuCuiGprot.h"
#include "FactoryModeGProt.h"

/* For MRE version */
#include "vmsys.h"
#include  "stack_msgs.h"

#ifdef __GADGET_SUPPORT__
#include "WgtMgrSrvGprot.h"
#endif 

#include "PowerOnChargerProt.h"

#include "FileMgrSrvGProt.h"

#include "aud_main.h"
#include "FactoryModeConfig.h"
#include "FactoryModeProt.h"
#include "FactoryModeUtil.h"
//#include "AlarmGprot.h"
#ifdef __SUPPORT_ECHOLOOP__
#include "custom_em.h"
#endif /*__SUPPORT_ECHOLOOP__*/
#ifdef __SUPPORT_FM_RADIO__
#include "Mmi_rp_app_engineermode1_def.h"
#endif
#ifdef __SUPPORT_TP_TEST__
#include "mmi_rp_app_calibration_def.h"
#endif


/***************************************************************************** 
* Define
*****************************************************************************/
#define NOSENDKEY_CNT_START_SHOW   0
#define NOSENDKEY_CNT_START_HIDE   1
#define NOSENDKEY_CNT_STOP_SHOW    2
#define NOSENDKEY_UNCNT_STOP_SHOW  6
#define NOSENDKEY_UNCNT_STOP_HIDE  7

#define SENDKEY_CNT_START_SHOW     8
#define SENDKEY_CNT_START_HIDE     9
#define SENDKEY_CNT_STOP_SHOW      10
#define SENDKEY_UNCNT_STOP_SHOW    14
#define SENDKEY_UNCNT_STOP_HIDE    15

#define SENDKEYFLAG     0x08
#define CONTINUSFLAG    0x04
#define STOPFLAG        0x02
#define SHOWSCRNFLAG    0x01


/***************************************************************************** 
* typedef enum/struct/...
*****************************************************************************/

/***************************************************************************** 
* global val / function
*****************************************************************************/
extern U16 g_cameraStartFlag;
extern U8 gCurrentMode;

extern MMI_ID g_fm_gourp_id;

extern mmi_fm_item_cntxt_struct *g_fm_contxt;

/*-------------auto test-----------------*/
/* Auto Testing Settings */
extern S32 g_fm_PriorityValue ;
extern S16 g_fm_AutoTestListSize ;
extern U16 g_fm_HiliteAutoTestCurrSelIdx ;
extern U16 g_fm_HiliteAllTestListIdx ;
extern U16 g_fm_newPriority ;
extern AutoTestItemArray g_fm_nvramTestItemArray;
extern AutoTestResultArray g_fm_nvramTestResultArray;
extern rtc_format_struct g_fm_myclocktime;
extern PU8 g_fm_AutoTestSelectedItems[MAX_AUTO_TEST_ITEMS];

/* -----------Camera ------------- */
#if defined(__MMI_FM_CAMERA_PREVIEW__)
extern camera_context_struct g_camera_cntx;
extern const U8 camera_ev_command_map[];
extern const U8 camera_zoom_command_map[];
extern const U8 camera_effect_command_map[];
extern const U8 camera_wb_command_map[];
extern const U8 camera_banding_command_map[];
extern const U8 camera_jpg_size_command_map[];
extern const U8 camera_image_qty_command_map[];

/* extern gdi_color                     GDI_COLOR_BLACK; */
extern wgui_inline_item wgui_inline_items[];

extern gdi_handle wgui_base_layer;
extern gdi_handle wgui_layer_1;

#endif /* defined(__FM_CAMERA_PREVIEW__) */ 

#if defined(__SUPPORT_BLUETOOTH__)
extern BOOL gInBTTestMode;
static int BTFMresult = 0xff;
extern BOOL gbBTisPwredByFMtest;
extern MMI_BOOL srv_bt_cm_is_activated(void);
extern void mmi_bt_reset_power_on_nvram_flag(void);
extern void mmi_bt_test_power_on(void);
extern void FM_AutoTest_BT_Test(void);
#endif /*defined(__SUPPORT_BLUETOOTH__)*/

static BOOL gVbatPass = FALSE;
static BOOL gIChrPass = FALSE;

#ifdef __PY_FACTORY_MODE__

typedef struct
{
    U8  m_nCount;
    const U8 *m_pList;
} FMAutoTestList;

#define FM_AUTOTEST_LIST_ITEM(_item) {sizeof(_item) / sizeof(_item[0]), _item}

U8 g_FM_AutoTest_Mode = FM_AUTOTEST_MODE_INVALID;

const U8 Tests_9_Seq[] = {
#ifdef __PY_FACTORY_MODE__
		FM_TEST_MEMORYCARD,
#endif	
#ifdef __SUPPORT_SPEAKER_VIB__
    FM_TEST_SPEAKER_VIB,
#endif /*__SUPPORT_SPEAKER_VIB__*/
#ifdef __SUPPORT_TP_TEST__
    FM_TEST_CALIBRATION,
#endif /*__SUPPORT_TP_TEST__*/
#ifdef __SUPPORT_SIM_DETECT__
#ifndef __MMI_DUAL_SIM_MASTER__
    FM_TEST_SIM,
#else /*__MMI_DUAL_SIM_MASTER__*/
    FM_TEST_SIM1,
    FM_TEST_SIM2,
#endif /*__MMI_DUAL_SIM_MASTER__*/
#endif /*__SUPPORT_SIM_DETECT__*/
    FM_TEST_CAMERA,
#ifdef __SUPPORT_FLASHLIGHT__
    FM_TEST_FLASHLIGHT,
#endif /*__SUPPORT_FLASHLIGHT__*/
#ifdef __SUPPORT_ECHOLOOP__
	FM_TEST_ECHOLOOP,
#endif /*__SUPPORT_ECHOLOOP__*/
//    FM_TEST_MIC,
    FM_TEST_HEADSET,
    FM_TEST_KEYPAD,
#ifdef __SUPPORT_POWER_TEST__
    FM_TEST_VBAT,
    FM_TEST_ICHR,
#endif /*__SUPPORT_POWER_TEST__*/
//    FM_TEST_BATTERY,
#if defined(__SUPPORT_CHARGER__)
    FM_TEST_DEWAV_CHARGER,
#endif
#ifdef __SUPPORT_BLUETOOTH__
    FM_TEST_BT,
#endif /*__SUPPORT_BLUETOOTH__*/
#ifdef __SUPPORT_FM_RADIO__
    FM_AUTOTEST_FMRADIO,
#endif /*__SUPPORT_FM_RADIO__*/
};

const U8 Tests_664_Seq[] = {
    FM_TEST_MEMORYCARD,
    FM_TEST_SPEAKER,
    FM_TEST_VIB,
#ifdef __SUPPORT_TP_TEST__
    FM_TEST_CALIBRATION,
#endif /*__SUPPORT_TP_TEST__*/
#ifdef __SUPPORT_SIM_DETECT__
#ifndef __MMI_DUAL_SIM_MASTER__
    FM_TEST_SIM,
#else /*__MMI_DUAL_SIM_MASTER__*/
    FM_TEST_SIM1,
    FM_TEST_SIM2,
#endif /*__MMI_DUAL_SIM_MASTER__*/
#endif /*__SUPPORT_SIM_DETECT__*/
    FM_TEST_CAMERA,
#ifdef __SUPPORT_FLASHLIGHT__
    FM_TEST_FLASHLIGHT,
#endif /*__SUPPORT_FLASHLIGHT__*/
#ifdef __SUPPORT_ECHOLOOP__
	FM_TEST_ECHOLOOP,
#endif /*__SUPPORT_ECHOLOOP__*/
//    FM_TEST_MIC,
    FM_TEST_HEADSET,
    FM_TEST_KEYPAD,
#ifdef __SUPPORT_POWER_TEST__
    FM_TEST_VBAT,
    FM_TEST_ICHR,
#endif /*__SUPPORT_POWER_TEST__*/
//    FM_TEST_BATTERY,
#if defined(__SUPPORT_CHARGER__)
    FM_TEST_DEWAV_CHARGER,
#endif
#ifdef __SUPPORT_BLUETOOTH__
    FM_TEST_BT,
#endif /*__SUPPORT_BLUETOOTH__*/
#ifdef __SUPPORT_FM_RADIO__
    FM_AUTOTEST_FMRADIO,
#endif /*__SUPPORT_FM_RADIO__*/
};

FMAutoTestList gFmAutoTestList[] = 
{
    /* FM_AUTOTEST_MODE_664 */
    FM_AUTOTEST_LIST_ITEM(Tests_9_Seq),
    /* FM_AUTOTEST_MODE_8 */
    FM_AUTOTEST_LIST_ITEM(Tests_9_Seq),
    /* FM_AUTOTEST_MODE_9 */
    FM_AUTOTEST_LIST_ITEM(Tests_9_Seq)
};

static const U32 FM_AUTOTEST_MODE_TOTAL = sizeof(gFmAutoTestList) / sizeof(gFmAutoTestList[0]);

#endif /*__PY_FACTORY_MODE__*/

#if defined(__MMI_FM_CAMERA_PREVIEW__)
extern void mmi_fm_camera_entry_sublcd_screen(void);
#endif 


#if defined(__MMI_FM_TC01_CAMERA_TEST__)
extern void mmi_fm_camera_test_entry_cam_test(void);
#endif

/*--- For Quick test setting----- */
extern void mmi_fm_select_auto_test_setting(void);
extern void mmi_fm_WriteTestResultToNVRAM(void);
extern void FM_SendStopAudioReq(U8 index);
extern void FM_SetGpioReq(U8 type, U8 level);
extern void gui_touch_feedback_enable_tone_internal(void);
extern void gui_touch_feedback_enable_vibrate_internal(void);
extern void gui_touch_feedback_disable_tone_internal(void);
extern void gui_touch_feedback_disable_vibrate_internal(void);
extern void mmi_fm_ReadTimeFromNVRAM(void);
extern void mmi_fm_WriteCurrentTimeToNVRAM(void);
extern void mmi_fm_ReadTestResultFromNVRAM(void);
extern void FM_SendADCStartReq(void);
extern void FM_SetGpioVirbateReq(U8 level);

extern void InitFactoryModeEvent(void);
extern void DeinitFactoryModeEvent(void);
/***************************************************************************** 
* static  function
*****************************************************************************/
/*-----------auto test-------------------*/
static void EntryFMAutoTestStart(void);
static void EntryFMAutoTestReport(void);
static void FM_InitAutoTest(void);
static U8 mmi_fm_auto_test_delete_history_hdlr(void);
#ifdef __SUPPORT_LED_FEATURE__
static void FM_AutoTest_LED(void);
#endif /* __SUPPORT_LED_FEATURE__ */
static void FM_AutoTest_CLAM(void);
static void FM_AutoTest_Version(void);
static void FM_AutoTest_Backlight(void);
static void FM_AutoTest_Keypad(void);
static void FM_AutoTest_LCD(void);
static void FM_AutoTest_Receiver(void);
static void FM_AutoTest_MIC(void);
static void FM_AutoTest_Speaker(void);
static void FM_AutoTest_Headset(void);
static void FM_AutoTest_Battery(void);
static void FM_AutoTest_Melody(void);
static void FM_AutoTest_VIB(void);
static void FM_AutoTest_NAND(void);
#ifdef __MMI_FM_UART_TEST__
static void FM_AutoTest_UART(void);
#endif
static void FM_AutoTest_Double_Speaker(void);
static void FM_AutoTest_CAMERA(void);
static void FM_AutoTest_MemoryCard(void);
#ifdef __MMI_TOUCH_SCREEN__
static void FM_AutoTest_Pen_Parallel_Test(void);
#endif /* __MMI_TOUCH_SCREEN__ */ 
#ifndef __MMI_DUAL_SIM_MASTER__
static void FM_AutoTest_SIM(void);
#else /*__MMI_DUAL_SIM_MASTER__*/
static void FM_AutoTest_SIM1(void);
static void FM_AutoTest_SIM2(void);
#endif /*__MMI_DUAL_SIM_MASTER__*/
#ifdef __SUPPORT_SPEAKER_VIB__
static void FM_AutoTest_Speaker_Vib(void);
#endif /*__SUPPORT_SPEAKER_VIB__*/
#ifdef __SUPPORT_ECHOLOOP__
static void FM_AutoTest_EchoLoop(void);
#endif /*__SUPPORT_ECHOLOOP__*/
#ifdef __SUPPORT_POWER_TEST__
static void FM_AutoTest_VBat_Test(void);
static void FM_AutoTest_IChr_Test(void);
#endif
#ifdef __SUPPORT_FM_RADIO__
static void FM_AutoTest_FM_Test(void);
#endif
#ifdef __SUPPORT_FLASHLIGHT__
static void FM_AutoTest_FlashLight(void);
#endif /*__SUPPORT_FLASHLIGHT__*/
#ifdef __SUPPORT_TP_TEST__
static void FM_AutoTest_Calibration_Test(void);
#endif /*__SUPPORT_TP_TEST__*/
static void FM_ReTest(void);
static void FM_Handle_Pass_Key_Press(void);
static void FM_Handle_Fail_Key_Press(void);

//static void FM_Autotest_Stop_Test(void);

static void mmi_fm_select_atv(void);
static void mmi_fm_highlight_atv(void);

static void mmi_fm_reenter_auto_test(void);
static void mmi_fm_enter_auto_test(void);
static void	mmi_fm_enter_autotest_nand(void);

#ifdef __PY_FACTORY_MODE__
void FactoryModeSendMsg(U16 msg_id, void *local_param_ptr, void *peer_buf_ptr);
void FM_AutoTest_Write_Result(void);
#endif /*__PY_FACTORY_MODE__*/
#define IF_BREAK(_c)    if (_c) break
#define POWER_MEASURE_TOTAL     (5)
static MMI_BOOL gIsPowerTestPass = MMI_FALSE;


/*=============== Auto Testing =============== */
const testlet Tests[MAX_AUTO_TEST_ITEMS] = {    /* Maximum name length = MAX_TEST_ITEM_NAME_LEN*ENCODING_LENGTH */
#ifdef __SUPPORT_LED_FEATURE__
    {"LED", FM_AutoTest_LED},
#endif
    {"Clam", FM_AutoTest_CLAM},
    {"Version", FM_AutoTest_Version},
    {"Backlight", FM_AutoTest_Backlight},
#ifndef __MMI_MAINLCD_96X64__
    {"LCD", FM_AutoTest_LCD},
#endif
    {"KeyPad", FM_AutoTest_Keypad},
    {"Recevier", FM_AutoTest_Receiver},
    {"MIC", FM_AutoTest_MIC},
    {"Speaker", FM_AutoTest_Speaker},
    {"Headset", FM_AutoTest_Headset},
    {"Battery", FM_AutoTest_Battery},
    {"Melody", FM_AutoTest_Melody},
    {"Vibrator", FM_AutoTest_VIB},
    {"NAND", FM_AutoTest_NAND},   
#ifdef __MMI_FM_UART_TEST__
    {"UART", FM_AutoTest_UART},
#endif
    {"Doublespeaker", FM_AutoTest_Double_Speaker},     /* 17 */
#ifndef __MMI_MAINLCD_96X64__
    {"CAMERA", FM_AutoTest_CAMERA},     /* 18 */
#endif
    {"MemoryCard", FM_AutoTest_MemoryCard}  /* 19 */
#ifdef __MMI_TOUCH_SCREEN__
    , {"ParallelLine", FM_AutoTest_Pen_Parallel_Test} /* 20 */
    , {"NxMPoint", FM_AutoTest_Pen_N_Cross_M_Test}        /* 21 */
#endif /* __MMI_TOUCH_SCREEN__ */ 
#ifdef __SUPPORT_SPEAKER_VIB__
    , {"SpeakerVib", FM_AutoTest_Speaker_Vib}
#endif /*__SUPPORT_SPEAKER_VIB__*/
#ifdef __SUPPORT_SIM_DETECT__
#ifndef __MMI_DUAL_SIM_MASTER__
    , {"SIM", FM_AutoTest_SIM)}
#else /*__MMI_DUAL_SIM_MASTER__*/
    , {"SIM1", FM_AutoTest_SIM1}
    , {"SIM2", FM_AutoTest_SIM2}
#endif /*__MMI_DUAL_SIM_MASTER__*/
#endif /*__SUPPORT_SIM_DETECT__*/
#ifdef __SUPPORT_ECHOLOOP__
    , {"Echoloop", FM_AutoTest_EchoLoop}
#endif /*__SUPPORT_ECHOLOOP__*/
#ifdef __SUPPORT_POWER_TEST__
    , {"VBAT", FM_AutoTest_VBat_Test}
    , {"ICHR", FM_AutoTest_IChr_Test}
#endif
#ifdef __SUPPORT_BLUETOOTH__
    , {"BT", FM_AutoTest_BT_Test}
#endif
#ifdef __SUPPORT_FM_RADIO__
    , {"FMRadio", FM_AutoTest_FM_Test}
#endif
#ifdef __SUPPORT_FLASHLIGHT__
    , {"Flashlight", FM_AutoTest_FlashLight}
#endif /*__SUPPORT_FLASHLIGHT__*/
#ifdef __SUPPORT_TP_TEST__
    , {"Calibration", FM_AutoTest_Calibration_Test}
#endif /*__SUPPORT_TP_TEST__*/
};


/***************************************************************************** 
* Code body
*****************************************************************************/
#define MMI_FM_AUTO_TEST
	
/*--------------------------- AUTO TEST ----------------------------------------*/

static void FM_AutoTest_UpdateAdc_String(mmi_eq_adc_all_channel_ind_struct *pAdc)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	double vbat = 0, ibat = 0;
	mmi_fm_common_struct *common_cntx = &(g_fm_contxt->common_contxt);
    S32 l_nLength = MAX_TEST_STRING_BUF;
    S16 l_strTemp[MAX_TEST_STRING_BUF];
    
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
    do
    {
        if (FM_TEST_VBAT == common_cntx->currentTest)
        {
            vbat = ((double) pAdc->vbat) / 1000000.0;
            gIsPowerTestPass = (vbat >= VBAT_LOWER_BOUNDARY && vbat <= VBAT_UPPER_BOUNDARY) ? MMI_TRUE : MMI_FALSE;
        }
        else if (FM_TEST_ICHR == common_cntx->currentTest)
        {
            ibat = ((double) pAdc->charge_current) / 1000000.0;
            vbat = ((double) pAdc->vcharge) / 1000000.0;
            gIsPowerTestPass = (ibat >= 0.1 && vbat >=4.2) ? MMI_TRUE : MMI_FALSE;
        }
        else
        {
            break ;
        }

        sprintf((S8 *)common_cntx->EMFMAsciiDisplayBuf,
            "%s (%d/%d)\n",
            Tests[common_cntx->currentTest].name,
            common_cntx->currTestItem + 1,
            g_fm_AutoTestListSize);
        mmi_asc_to_ucs2((CHAR *) l_strTemp, (CHAR *) common_cntx->EMFMAsciiDisplayBuf);
        mmi_ucs2ncpy(common_cntx->EMFMUnicodeDisplayBuf, (S8 *) l_strTemp, l_nLength);

        IF_BREAK((l_nLength -= mmi_ucs2strlen(common_cntx->EMFMUnicodeDisplayBuf)) < 0);
        sprintf((S8 *)common_cntx->EMFMAsciiDisplayBuf,
            "Step:%d/%d\n",
            common_cntx->sequence_counter + 1, 
            POWER_MEASURE_TOTAL);
        mmi_asc_to_ucs2((CHAR *) l_strTemp, (CHAR *) common_cntx->EMFMAsciiDisplayBuf);
        mmi_ucs2ncat(common_cntx->EMFMUnicodeDisplayBuf, (S8 *) l_strTemp, l_nLength);

        IF_BREAK((l_nLength -= mmi_ucs2strlen(common_cntx->EMFMUnicodeDisplayBuf)) < 0);
        sprintf((S8 *)common_cntx->EMFMAsciiDisplayBuf, "vBat:%4.2fV\n", vbat);
        mmi_asc_to_ucs2((CHAR *) l_strTemp, (CHAR *) common_cntx->EMFMAsciiDisplayBuf);
        mmi_ucs2ncat(common_cntx->EMFMUnicodeDisplayBuf, (S8 *) l_strTemp, l_nLength);

        if (FM_TEST_ICHR == common_cntx->currentTest)
        {
            IF_BREAK((l_nLength -= mmi_ucs2strlen(common_cntx->EMFMUnicodeDisplayBuf)) < 0);
            sprintf((S8 *)common_cntx->EMFMAsciiDisplayBuf, "iBat:%4.2fA\n", ibat);
            mmi_asc_to_ucs2((CHAR *) l_strTemp, (CHAR *) common_cntx->EMFMAsciiDisplayBuf);
            mmi_ucs2ncat(common_cntx->EMFMUnicodeDisplayBuf, (S8 *) l_strTemp, l_nLength);
        }
    } while(0);
}

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTestUpdateVBatHdlr
 * DESCRIPTION
 *	
 * PARAMETERS
 *	inMsg		[?] 	
 * RETURNS
 *	void
 *****************************************************************************/
static void FM_AutoTestUpdateAdcStepHdlr(void *inMsg)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
    FM_AutoTest_UpdateAdc_String((mmi_eq_adc_all_channel_ind_struct*) inMsg);
    
    mmi_frm_scrn_enter(
        g_fm_gourp_id, 
        GLOBAL_SCR_DUMMY, 
        NULL,
        NULL,
        MMI_FRM_FULL_SCRN);

	mmi_frm_scrn_close_active_id();

	g_fm_contxt->common_contxt.sequence_counter++;

	mmi_frm_set_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
									NULL, MMI_FALSE);

	mmi_frm_set_protocol_event_handler(PRT_BATTERY_STATUS_IND, 
									(PsIntFuncPtr)BatteryStatusRsp, MMI_FALSE);

	FM_SendADCStopReq();
}


/*****************************************************************************
 * FUNCTION
 *	FM_ReTest
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void FM_ReTest(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;
	
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
	
	/* currTestItem--; */
	common_cntx->sequence_counter = 0;	 /* Reset the sequence counter */
	common_cntx->TestExecuted[common_cntx->currTestItem] = FALSE;
	EntryFMAutoTestStart();
}

/*****************************************************************************
 * FUNCTION
 *	FM_Handle_Pass_Key_Press
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void FM_Handle_Pass_Key_Press(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/

    mmi_fm_back_to_normal_state();    

	g_fm_nvramTestResultArray.result[g_fm_contxt->common_contxt.currTestItem] = FM_TEST_PASSED;
	g_fm_contxt->common_contxt.currTestItem++;
          
	mmi_fm_WriteTestResultToNVRAM();    

	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
    	
	EntryFMAutoTestStart();    
}

/*****************************************************************************
 * FUNCTION
 *	FM_Handle_Fail_Key_Press
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void FM_Handle_Fail_Key_Press(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/    
	mmi_fm_back_to_normal_state();

	g_fm_nvramTestResultArray.result[g_fm_contxt->common_contxt.currTestItem] = FM_TEST_FAILED;
	g_fm_contxt->common_contxt.currTestItem++;
	mmi_fm_WriteTestResultToNVRAM();

	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	
	EntryFMAutoTestStart();
}

/*****************************************************************************
 * FUNCTION
 *	FM_Autotest_set_key_handlers
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
extern void FM_Autotest_set_key_handlers(U8 is_reg_sendkey)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	if (is_reg_sendkey)
	SetKeyDownHandler(FM_ReTest, KEY_SEND);
	PowerAndEndKeyHandler();
	SetLeftSoftkeyFunction(FM_Handle_Pass_Key_Press, KEY_EVENT_UP);
	SetRightSoftkeyFunction(FM_Handle_Fail_Key_Press, KEY_EVENT_UP);
}
#if 0
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
/* under construction !*/
#endif
static MMI_RET mmi_fm_auto_test_group_proc(mmi_event_struct *evt)
{
	switch(evt->evt_id)
	{
		case EVT_ID_GROUP_DEINIT:
			StopTimer(FM_AUTO_TEST_COMMNON_TIMER);

			applib_mem_ap_free(g_fm_contxt);
			g_fm_contxt = 0;
			g_fm_gourp_id = 0;

			mmi_fm_auto_test_delete_history_hdlr();
			
			StopTimer(FM_LCD_COLOR_CHANGE_TIMER);
			FM_SendStopAudioReq(0);
			FM_SetGpioVirbateReq(VIBRATOR_OFF);
			
			FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
			mdi_audio_stop_id(TONE_KEY_NORMAL);
			
        //#ifdef __MMI_FM_CAMERA_PREVIEW__
        /* For camera */
        #ifdef __MMI_FM_CAMERA_PREVIEW__
			mmi_fm_close_camera();          
        #endif
			
			/* Enable tone internal */
			gui_touch_feedback_enable_tone_internal();
			gui_touch_feedback_enable_vibrate_internal();

            /* DCM Require */
            MMI_FM_DCM_POST_UNLOAD();
        #ifdef __SUPPORT_ECHOLOOP__
            kal_sleep_task(kal_milli_secs_to_ticks(100));
            //mmi_fm_set_loopback(MMI_FALSE);
            aud_util_proc_in_med(MOD_MMI,
                mmi_fm_set_loopback,
                MMI_FALSE,
                NULL);
        #endif /*__SUPPORT_ECHOLOOP__*/
        #if defined(__SUPPORT_BLUETOOTH__)
           gInBTTestMode = FALSE;
        #endif
			break;			 
	}
	return MMI_RET_OK;	  
}

MMI_BOOL NeedInitAuto = MMI_FALSE; /*It is to mark if need sync nvram info*/

MMI_BOOL g_is_first_enter = MMI_TRUE;     /*It is to mark if first enter quicktest without testing*/

static void mmi_fm_reenter_auto_test(void)
{
	 NeedInitAuto = MMI_TRUE;
	
	 mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_ROOT);
	 mmi_fm_enter_auto_test();	 
}

static void mmi_frm_clearResult()
{
    U32 i;
    for (i = 0; i < g_fm_AutoTestListSize; i++)
	{
		g_fm_nvramTestResultArray.result[i] = FM_TEST_UNTESTED;
	}
}

/*****************************************************************************
 * FUNCTION
 *	mmi_fm_enter_auto_test
 * DESCRIPTION
 *	Enter quick test
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void mmi_fm_enter_auto_test(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	U8 *guiBuffer;
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);

	 if (g_fm_contxt == NULL)
	 {
		//g_fm_contxt = MMI_FM_MALLOC(sizeof(mmi_fm_item_cntxt_struct));
		g_fm_contxt = applib_mem_ap_alloc(APPLIB_MEM_AP_ID_FM_COMM_MEM,sizeof(mmi_fm_item_cntxt_struct));
		memset(g_fm_contxt, 0, sizeof(mmi_fm_item_cntxt_struct));
		mmi_fm_init_mem_contxt();
	 }
	 
	 //mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_ROOT);
	 if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_ROOT, 
		NULL, 
		mmi_fm_enter_auto_test, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	

	common_cntx->autoTestMode = 1;

	guiBuffer =  mmi_frm_scrn_get_active_gui_buf();
	
	RegisterHighlightHandler(ExecuteCurrHiliteHandler);

	kal_wsprintf((WCHAR *) common_cntx->EMFMUnicodeDisplayBuf,"%w\n\n\n\nEndkey: %w",
	GetString(STR_ID_FM_AUTO_TEST_ROOT),GetString(STR_GLOBAL_EXIT));
	
	ShowCategory7Screen(
		STR_ID_FM_AUTO_TEST_ROOT,
		NULL,
		STR_GLOBAL_START,
		IMG_GLOBAL_OK,
		STR_ID_FM_AUTO_TEST_REPORT,
		IMG_GLOBAL_BACK,
		(PU8) common_cntx->EMFMUnicodeDisplayBuf,
		guiBuffer);

	SetLeftSoftkeyFunction(EntryFMAutoTestStart, KEY_EVENT_UP);
	SetRightSoftkeyFunction(EntryFMAutoTestReport, KEY_EVENT_UP);

#ifdef __COSMOS_MMI_PACKAGE__    
	SetKeyUpHandler(EntryFMAutoTestReport, KEY_BACK);
#endif

	if (NeedInitAuto == TRUE)    /*Reenter quick test , need read NVRAM info*/
	{
		FM_InitAutoTest();
		NeedInitAuto = MMI_FALSE;
	}

	/* If first enter quick test and select report, the NVRAM default test reslut
	is wrong, and there is no info to justify if it is the first entry through NVRAM data.
	So add this code.
	If it is first entry of EM quick, g_fm_nvramTestResultArray.result[] should be set
	FM_TEST_UNTESTED */

	if (g_is_first_enter)
	{
		mmi_frm_clearResult();
	}
}

/*****************************************************************************
 * FUNCTION
 *	EntryFMAutoTestStart
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMAutoTestStart(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	U16 testitem;
	U8 *guiBuffer;

	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
#if defined(__SUPPORT_BLUETOOTH__)
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_BT_TEST);
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_DUMMY);
	mmi_frm_scrn_close(g_fm_gourp_id, GLOBAL_SCR_DUMMY);
#endif
	/* currTestItem++; */
	common_cntx = &(g_fm_contxt->common_contxt);
	if (common_cntx->currTestItem == 0)  /* Testing just began, clear all tet results */
    {
        mmi_frm_clearResult();
		mmi_fm_ReadTimeFromNVRAM();
		g_is_first_enter = MMI_FALSE;
	}

	if (common_cntx->currTestItem < g_fm_AutoTestListSize)
	{
		/* Call test item according to the selected order */
		testitem = g_fm_nvramTestItemArray.priority[common_cntx->currTestItem];

		if (common_cntx->TestExecuted[common_cntx->currTestItem] == FALSE)	  /* prevent reentry of the same tests on interrupts */
		{
			/* Execute the test */
			//g_fm_contxt->common_contxt.NeedExitFunc = FALSE;
			(Tests[testitem].func) ();
			TONE_SetOutputVolume(255, 0);
			
			common_cntx->TestExecuted[common_cntx->currTestItem] = TRUE;
		}
	}
	else
	{
		
		mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);		
		if (mmi_frm_scrn_enter
		   (g_fm_gourp_id, 
			SCR_ID_FM_AUTO_TEST_START, 
			NULL, 
			NULL, 
			MMI_FRM_FULL_SCRN) != MMI_TRUE)
			{
				return;
			}
    #ifdef __PY_FACTORY_MODE__
        FM_AutoTest_Write_Result();
    #endif /*__PY_FACTORY_MODE__*/
		guiBuffer =  mmi_frm_scrn_get_active_gui_buf();
		ShowCategory7Screen(
			STR_ID_FM_AUTO_TEST_ROOT,
			NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
			STR_ID_FM_AUTO_TEST_REPORT,
			IMG_GLOBAL_OK,
			STR_GLOBAL_DONE,
			IMG_GLOBAL_BACK,
			(PU8) GetString(STR_ID_FM_AUTO_TEST_ALLDONE),
			guiBuffer);

		SetLeftSoftkeyFunction(EntryFMAutoTestReport, KEY_EVENT_UP);
		SetRightSoftkeyFunction(mmi_fm_reenter_auto_test, KEY_EVENT_UP);
	}
}

/*****************************************************************************
 * FUNCTION
 *	FM_Handle_RTC_Stop_Key_Press
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void FM_Handle_RTC_Stop_Key_Press(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	g_fm_contxt->common_contxt.sequence_counter = 0;
	StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
	mmi_fm_WriteCurrentTimeToNVRAM();
	mmi_fm_reenter_auto_test();
}

/*****************************************************************************
 * FUNCTION
 *	FM_ManualTest_RTC
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_ManualTest_RTC(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	CHAR uniBuf[240];
	
	U16 msec = 1000;
	U16 sec = 2;

	MYTIME t;
	rtc_format_struct currtime;

	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	//EntryNewScreen(SCR_ID_FM_RTC, NULL, NULL, NULL);

	common_cntx = &(g_fm_contxt->common_contxt);

	mmi_frm_scrn_close(g_fm_gourp_id,SCR_ID_FM_RTC);

		if (mmi_frm_scrn_enter
		 (g_fm_gourp_id, 
		  SCR_ID_FM_RTC, 
		  NULL,
		  NULL, 
		  MMI_FRM_FULL_SCRN) != MMI_TRUE)
		  {
		  	return;
		  }
	
	if (common_cntx->sequence_counter < sec)
	{
		common_cntx->sequence_counter++;
		StartTimer(FM_AUTO_TEST_COMMNON_TIMER, msec, FM_ManualTest_RTC);

		DTGetRTCTime(&t);

		/*
		currtime.rtc_sec = t.nSec;
		currtime.rtc_min = t.nMin;
		currtime.rtc_hour = t.nHour;
		currtime.rtc_day = t.nDay;
		currtime.rtc_mon = t.nMonth;
		*/

		//currtime = (rtc_format_struct)t;
		memcpy(&currtime,&t,sizeof(rtc_format_struct));
		
		currtime.rtc_wday = 0;
		currtime.rtc_year = (kal_uint8) (t.nYear - 2000);

		kal_wsprintf((WCHAR *) uniBuf,
		"Last RTC:\n%d.%d.%d %02d:%02d:%02d\nCurrent RTC:\n%d.%d.%d %02d:%02d:%02d\n\npoweroff in %d sec",
		2000 + g_fm_myclocktime.rtc_year,
			g_fm_myclocktime.rtc_mon,
			g_fm_myclocktime.rtc_day,
			g_fm_myclocktime.rtc_hour,
			g_fm_myclocktime.rtc_min,
			g_fm_myclocktime.rtc_sec,
			2000 + currtime.rtc_year,
			currtime.rtc_mon,
			currtime.rtc_day,
			currtime.rtc_hour,
			currtime.rtc_min,
			currtime.rtc_sec,
			sec - common_cntx->sequence_counter);

		ShowCategory7Screen(
			STR_ID_FM_AUTO_TEST_ROOT,
			NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
			(U16) NULL,
			IMG_GLOBAL_OK,
			STR_GLOBAL_STOP,
			IMG_GLOBAL_BACK,
			(PU8) uniBuf,
			NULL);

		SetRightSoftkeyFunction(FM_Handle_RTC_Stop_Key_Press, KEY_EVENT_UP);
	}
	else
	{
		common_cntx->sequence_counter = 0;
		StopTimer(FM_AUTO_TEST_COMMNON_TIMER);

		mmi_fm_WriteCurrentTimeToNVRAM();
		g_fm_contxt->rtc_contxt.gFactoryAlarm = 1;
		FactorySetAlarm(3);
	}

}


void mmi_fm_set_auto_test_goback_func() 
{
		SetRightSoftkeyFunction( mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
#if defined(__COSMOS_MMI_PACKAGE__)
	SetKeyUpHandler(mmi_frm_scrn_close_active_id, KEY_BACK);
#endif
}


void mmi_fm_set_auto_test_report(U32 untested, U8 *untested_items, U16 StringId, U8 *guiBuffer)
{
	U32 i = 0;

	mmi_fm_common_struct *common_cntx;

	common_cntx = &(g_fm_contxt->common_contxt);
	
	if (untested > 1)
	{
		sprintf((CHAR*) common_cntx->EMFMAsciiDisplayBuf, "\n\n%d Untested items:\n", untested);
	}
	else
	{
		sprintf((CHAR*) common_cntx->EMFMAsciiDisplayBuf, "\n\n1 Untested item:\n");
	}

	for (i = 0; i < untested; i++) 
	{
		CHAR str[40];

		if (i == untested - 1)
		{
			sprintf(str, "%d", 1 + untested_items[i]);
		}
		else
		{
			sprintf(str, "%d-", 1 + untested_items[i]);
		}

		strcat((CHAR*) common_cntx->EMFMAsciiDisplayBuf, (const CHAR*)str);
	}

	kal_wsprintf((WCHAR *) common_cntx->EMFMUnicodeDisplayBuf,
	"%w%s",
	GetString(StringId),(CHAR*) common_cntx->EMFMAsciiDisplayBuf);

	ShowCategory7Screen(
		STR_ID_FM_AUTO_TEST_REPORT,
		NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
		(U16) NULL,
		IMG_GLOBAL_OK,
		STR_GLOBAL_BACK,
		IMG_GLOBAL_BACK,
		(PU8) common_cntx->EMFMUnicodeDisplayBuf,
		guiBuffer);

	SetLeftSoftkeyFunction(NULL, KEY_EVENT_UP);

   SetRightSoftkeyFunction(mmi_fm_reenter_auto_test, KEY_EVENT_UP);

}
/*****************************************************************************
 * FUNCTION
 *	EntryFMAutoTestReport
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMAutoTestReport(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	U8 *guiBuffer;
	U32 i, passed, failed, untested;
	U8 fail_items[MAX_AUTO_TEST_ITEMS];
	U8 untested_items[MAX_AUTO_TEST_ITEMS];

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   NeedInitAuto = MMI_TRUE;

	 if (mmi_frm_scrn_enter
	     (g_fm_gourp_id, 
		   SCR_ID_FM_AUTO_TEST_REPORT, 
		   NULL, 
		   NULL, 
		   MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	 
	guiBuffer =  mmi_frm_scrn_get_active_gui_buf();
	
	for (i = 0, passed = 0, failed = 0, untested = 0; i < g_fm_AutoTestListSize; i++)
	{
		if (g_fm_nvramTestResultArray.result[i] == FM_TEST_UNTESTED)
		{
			untested_items[untested] = (U8) i;
			untested++;
		}
		else if (g_fm_nvramTestResultArray.result[i] == FM_TEST_FAILED)
		{
			fail_items[failed] = (U8) i;
			failed++;
		}
		else if (g_fm_nvramTestResultArray.result[i] >= FM_TEST_PASSED)
		{
			passed++;
		}
	}

	if (untested > 0)
	{
		mmi_fm_set_auto_test_report(untested, untested_items, STR_ID_FM_AUTO_TEST_UNFINISHED,guiBuffer);	  
	}
	else if (failed > 0)
	{
		mmi_fm_set_auto_test_report(failed, fail_items, STR_GLOBAL_FAILED, guiBuffer);
	}
	else
	{
		ShowCategory7Screen(
			STR_ID_FM_AUTO_TEST_REPORT,
			NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
			STR_ID_FM_RTC,
			IMG_GLOBAL_OK,
			STR_GLOBAL_BACK,
			IMG_GLOBAL_BACK,
			(PU8) GetString(STR_ID_FM_AUTO_TEST_ALLPASSED),
			guiBuffer);

		SetLeftSoftkeyFunction(FM_ManualTest_RTC, KEY_EVENT_UP);
	   SetRightSoftkeyFunction(mmi_fm_reenter_auto_test, KEY_EVENT_UP);
	}

}

/*****************************************************************************
 * FUNCTION
 *	InitFactoryModeSetting
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void InitFactoryModeSetting(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	gCurrentMode = FACTORY_MODE;
	mdi_audio_suspend_background_play();
	mdi_audio_stop_all();
	srv_gpio_play_pattern((srv_gpio_pattern_id_enum)srv_led_pattern_get_bg_pattern(), SRV_GPIO_PATN_PLAY_STOP);
}


/*****************************************************************************
 * FUNCTION
 *	FM_Autotest_test_done_USC2string
 * DESCRIPTION
 *	
 * PARAMETERS
 *	name		[IN]		
 * RETURNS
 *	void
 *****************************************************************************/
extern void FM_Autotest_test_done_USC2string(const CHAR *name)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	

	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	 common_cntx = &(g_fm_contxt->common_contxt);
	
#if (defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__))
	if (common_cntx->currentTest == FM_AUTOTEST_MEMORYCARD)
	{
		kal_wsprintf((WCHAR *) common_cntx->EMFMUnicodeDisplayBuf,
		"%s (%d/%d)\n%w.\n\nSendkey: %w\nEndkey: %w",
		(CHAR*) name,common_cntx->currTestItem + 1, g_fm_AutoTestListSize,
		g_fm_contxt->memcard_contxt.CardType,GetString(STR_ID_FM_AUTO_TEST_RETEST),GetString(STR_GLOBAL_EXIT));
	}
	else
#endif /*(defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__))*/
	{
	   kal_wsprintf((WCHAR *) common_cntx->EMFMUnicodeDisplayBuf,
		   "%s (%d/%d)\n%w.\n\nSendkey: %w\nEndkey: %w",
		   (CHAR*) name,common_cntx->currTestItem + 1, g_fm_AutoTestListSize,
		   GetString(STR_GLOBAL_DONE),GetString(STR_ID_FM_AUTO_TEST_RETEST),GetString(STR_GLOBAL_EXIT));		
	}
}


/*****************************************************************************
 * FUNCTION
 *	FM_InitAutoTest
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_InitAutoTest(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	U32 i;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
#ifdef __PY_FACTORY_MODE__
    if (g_FM_AutoTest_Mode < FM_AUTOTEST_MODE_TOTAL)
    {
        g_fm_nvramTestItemArray.count = gFmAutoTestList[g_FM_AutoTest_Mode].m_nCount;
        memcpy(
            g_fm_nvramTestItemArray.priority,
            gFmAutoTestList[g_FM_AutoTest_Mode].m_pList,
            gFmAutoTestList[g_FM_AutoTest_Mode].m_nCount);
        g_fm_AutoTestListSize = gFmAutoTestList[g_FM_AutoTest_Mode].m_nCount;
    }
    else
    {
#endif /*__PY_FACTORY_MODE__*/
	mmi_fm_ReadSettingsFromNVRAM(FM_SETTINGINFO);
	mmi_fm_ReadSettingsFromNVRAM(FM_RESULTINFO);
#ifdef __PY_FACTORY_MODE__
    }
#endif /*__PY_FACTORY_MODE__*/
	g_fm_contxt->common_contxt.currTestItem = 0;
	for (i = 0; i < MAX_AUTO_TEST_ITEMS; i++)
	{
		g_fm_contxt->common_contxt.TestExecuted[i] = FALSE;
	}
	InitFactoryModeSetting();
}

/*****************************************************************************
 * FUNCTION
 *	mmi_fm_auto_test_delete_history_hdlr
 * DESCRIPTION
 *	
 * PARAMETERS
 *	param		[?] 	
 * RETURNS
 *	
 *****************************************************************************/
U8 mmi_fm_auto_test_delete_history_hdlr(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	gCurrentMode = NORMAL_MODE;
	mdi_switch_device_ownership(MOD_MMI, MDI_DEVICE_AUDIO | MDI_DEVICE_CAMER | MDI_DEVICE_VIDEO);
	mdi_audio_start_background_timer();
	mdi_audio_resume_background_play();
	srv_gpio_play_pattern((srv_gpio_pattern_id_enum)srv_led_pattern_get_bg_pattern(), SRV_GPIO_PATN_PLAY_START);
	//mmi_profiles_restore_activated_profile();
	return FALSE;
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_LED
 * DESCRIPTION
 *	
 * PARAMETERS
 *	counter_flag:	if flag==0 then sequence_counter++ else sequence_counter = 0
 *	timer_flag:    if timer_flag == 0 then start timer,else stop timer
 *	is_show:	if is_show==0, show category
 *	is_reg_sendkey:   if is_reg_sendkey==1 register sendkey or not register
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_auto_test_common(U8 param ,U8 * StringId, U16 duration, FuncPtr func)
{
    U8 is_reg_sendkey = (SENDKEYFLAG == (param & SENDKEYFLAG))?1:0;        

    if(0 == (param & CONTINUSFLAG))        
    {        
        g_fm_contxt->common_contxt.sequence_counter++;        
    }
    else
    {
        g_fm_contxt->common_contxt.sequence_counter = 0;
        
    }

    if(0 == (param & STOPFLAG))
    {
        StartTimer(FM_AUTO_TEST_COMMNON_TIMER, duration, func); 
    }
    else
    {
        StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
    }    
    
    if(0 == (param & SHOWSCRNFLAG))
    {
        ShowCategory7Screen(
				STR_ID_FM_AUTO_TEST_ROOT,
				NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
				STR_ID_FM_AUTO_TEST_PASS,
				IMG_GLOBAL_OK,
				STR_ID_FM_AUTO_TEST_FAIL,
				IMG_GLOBAL_BACK,
				(U8*) StringId,
				NULL);
    }    
	FM_Autotest_set_key_handlers(is_reg_sendkey);    
}


#ifdef __SUPPORT_LED_FEATURE__

#define MMI_FM_QUICK_TEST_LED

static void mmi_fm_enter_quick_test_led(void);


static void FM_AutoTest_LED(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_led();
}



/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_LED
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_led(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
    S8 *StringInfo = NULL;
    U8  UcTestInfo = NOSENDKEY_CNT_START_SHOW;
    U16 UsDuration = LED_DURATION;
    FuncPtr FMAutoLEDFunc = FM_AutoTest_LED;
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_led, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	
	g_fm_contxt->common_contxt.currentTest = FM_TEST_LED;
	
	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_1, LED_LIGHT_LEVEL5);
            StringInfo = (U8*)GetString(STR_ID_FM_AUTOTEST_LED_R);
			break;
		case 1:
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_1, LED_LIGHT_LEVEL0);
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_2, LED_LIGHT_LEVEL5);
            StringInfo = (U8*)GetString(STR_ID_FM_AUTOTEST_LED_G);
			break;
		case 2:
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_2, LED_LIGHT_LEVEL0);
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_3, LED_LIGHT_LEVEL5);
            StringInfo = (U8*)GetString(STR_ID_FM_AUTOTEST_LED_B);
			break;
		default:
			FM_SetGpioReq(GPIO_DEV_LED_STATUS_3, LED_LIGHT_LEVEL0);
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_LED].name);
            
            UcTestInfo = SENDKEY_UNCNT_STOP_SHOW;
            StringInfo = g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf;
            UsDuration = 0;
            FMAutoLEDFunc = NULL;		  
	}
    
    mmi_fm_auto_test_common(UcTestInfo, StringInfo, UsDuration, FMAutoLEDFunc);
}

#endif /* __SUPPORT_LED_FEATURE__ */

#define MMI_FM_QUICK_TEST_CLAM

static void mmi_fm_enter_quick_test_clam(void);

void FM_AutoTest_CLAM(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_clam();
	
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_CLAM
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_clam(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   // EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);

	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_clam, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_CLAM_DETECT;

	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_CLAM),CLAM_DURATION,FM_AutoTest_CLAM);
			break;
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_CLAM].name);
			
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
	}
}

#define MMI_FM_QUICK_TEST_VERSION


static void mmi_fm_enter_quick_test_version(void);

void FM_AutoTest_Version(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_version();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Version
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_version(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
#if defined(__MTK_TARGET__)
	version_struct ver_struct;
#endif 

	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   common_cntx = &(g_fm_contxt->common_contxt);
   

	if (mmi_frm_scrn_enter
		   (g_fm_gourp_id, 
			SCR_ID_FM_AUTO_TEST_START, 
			NULL, 
			mmi_fm_enter_quick_test_version, 
			MMI_FRM_FULL_SCRN) != MMI_TRUE)
	{
		return;
	}

	common_cntx->currentTest = FM_TEST_SW_VERSION;

	switch (common_cntx->sequence_counter)
	{
		case 0:

			
#if defined(__MTK_TARGET__)
			/* Get sturctured version number */
			INT_VersionNumbers(&ver_struct);

			/* MCU (SW Ver.) */
			memcpy(common_cntx->EMFMAsciiDisplayBuf, (CHAR*) ver_struct.mcu_sw, MAX_TEST_STRING_BUF - 2);
			mmi_asc_to_ucs2((CHAR*) common_cntx->EMFMUnicodeDisplayBuf, (CHAR*) g_fm_contxt->common_contxt.EMFMAsciiDisplayBuf);



#else /* defined(__MTK_TARGET__)) */ 
			/* For PC Simulatior */
			mmi_asc_to_ucs2((CHAR*) common_cntx->EMFMUnicodeDisplayBuf, (CHAR*) "MTK_SW.V0");
#endif /* defined(__MTK_TARGET__)) */ 

			mmi_fm_auto_test_common(SENDKEY_CNT_START_SHOW, (U8*)common_cntx->EMFMUnicodeDisplayBuf,VERSION_DURATION,FM_AutoTest_Version);

		break;
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_VERSION].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW, (U8*)common_cntx->EMFMUnicodeDisplayBuf,0,NULL); 				
	}
}

#define MMI_FM_QUICK_TEST_BACKLIGHT

static void mmi_fm_enter_quick_test_backlight(void);

void FM_AutoTest_Backlight(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_backlight();
}

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Backlight
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_backlight(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   // EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);

		if (mmi_frm_scrn_enter
		   (g_fm_gourp_id, 
			SCR_ID_FM_AUTO_TEST_START, 
			NULL, 
			mmi_fm_enter_quick_test_backlight, 
			MMI_FRM_FULL_SCRN) != MMI_TRUE)
			{
				return;
			}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_BACKLIGHT;

	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
		   

			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_BACKLIGHT),BACKLIGHT_DURATION,FM_AutoTest_Backlight);
			
			break;
			
		case 1: 		   
			FM_SetGpioReq(GPIO_DEV_LED_MAINLCD, LED_LIGHT_LEVEL0);
			FM_SetGpioReq(GPIO_DEV_LED_KEY, LED_LIGHT_LEVEL0);
			
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_BACKLIGHT),BACKLIGHT_DURATION,FM_AutoTest_Backlight);

			break;
			
		case 2:
			FM_SetGpioReq(GPIO_DEV_LED_MAINLCD, LED_LIGHT_LEVEL5);

			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_BACKLIGHT),BACKLIGHT_DURATION,FM_AutoTest_Backlight);

			break;
			
		case 3:
			
			FM_SetGpioReq(GPIO_DEV_LED_MAINLCD, LED_LIGHT_LEVEL0);
			FM_SetGpioReq(GPIO_DEV_LED_KEY, LED_LIGHT_LEVEL5);

		  
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_BACKLIGHT),BACKLIGHT_DURATION,FM_AutoTest_Backlight);

			break;
			
		default:
			FM_SetGpioReq(GPIO_DEV_LED_MAINLCD, LED_LIGHT_LEVEL5);
			FM_SetGpioReq(GPIO_DEV_LED_KEY, LED_LIGHT_LEVEL0);

			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_BACKLIGHT].name);
			
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
			
	}

}

#define MMI_FM_QUICK_TEST_KEYPAD

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Keypad
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_Keypad(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
#ifdef __MMI_FM_KEYPAD_TEST__
	common_cntx->currentTest = FM_TEST_KEYPAD;
	mmi_fm_select_keypad_test();
#else
	g_fm_nvramTestResultArray.result[common_cntx->currTestItem] = FM_TEST_PASSED;
	common_cntx->currTestItem++;
	mmi_fm_WriteTestResultToNVRAM();
	EntryFMAutoTestStart();
#endif
}

#define MMI_FM_QUICK_TEST_LCD
#ifndef __MMI_MAINLCD_96X64__
/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_LCD
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_LCD(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	g_fm_contxt->common_contxt.currentTest = FM_TEST_LCD;

	g_fm_contxt->common_contxt.sequence_counter = 0;
	mmi_fm_handle_color_set_change();
}
#endif /* __MMI_MAINLCD_96X64__ */

#define MMI_FM_QUICK_TEST_RECEIVER

static void mmi_fm_enter_quick_test_receiver(void);


void FM_AutoTest_Receiver(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_receiver();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Receiver
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_receiver(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
    
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_receiver, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	common_cntx->currentTest = FM_TEST_RECEIVER;
    

	FM_SendStopAudioReq(0);
    
	//Media_Stop();
	g_fm_contxt->eachloop_contxt.ReceiverTestOn = TRUE;
	FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
	/* play 1K tone */
	TONE_SetOutputVolume(255, 0);
	mdi_audio_play_id(TONE_KEY_NORMAL, DEVICE_AUDIO_PLAY_INFINITE);
    

	if(0 == common_cntx->sequence_counter)
	{	    
        mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_RECEIVER),RECEIVER_DURATION,FM_AutoTest_Receiver);
    }
    else
    {
       g_fm_contxt->eachloop_contxt.ReceiverTestOn = FALSE;
       FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
	   /* stop 1K tone */
	    mdi_audio_stop_id(TONE_KEY_NORMAL);
	    FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_RECEIVER].name);
	    mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)common_cntx->EMFMUnicodeDisplayBuf,0,NULL); 
    }
    
}

#define MMI_FM_QUICK_TEST_MIC

void mmi_fm_enter_quick_test_mic(void);


void FM_AutoTest_MIC(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_mic();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_MIC
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_mic(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   // EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);
   common_cntx = &(g_fm_contxt->common_contxt);
    
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_mic, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

	common_cntx->currentTest = (U8)FM_TEST_MIC;
  
	FM_SendStopAudioReq(0);
	//Media_Stop();
	g_fm_contxt->eachloop_contxt.EchoLoopTestOn = MMI_TRUE;
	FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
	/* open loopback */
	kal_sleep_task(kal_milli_secs_to_ticks(100));
	//mmi_fm_set_loopback(MMI_TRUE);
	aud_util_proc_in_med(MOD_MMI,
		mmi_fm_set_loopback,
		MMI_TRUE,
		NULL);

	switch (common_cntx->sequence_counter)
	{
		case 0:

			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_MIC),MIC_DURATION,FM_AutoTest_MIC);
				
			break;
			
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_MIC].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)common_cntx->EMFMUnicodeDisplayBuf,0,NULL);
			
			
			g_fm_contxt->eachloop_contxt.EchoLoopTestOn = MMI_FALSE;
						/* close loopback */
			//mmi_fm_set_loopback(MMI_FALSE);
			aud_util_proc_in_med(MOD_MMI,
				mmi_fm_set_loopback,
				MMI_FALSE,
				NULL);
	}
}

#define MMI_FM_QUICK_TEST_SPEAKER

void mmi_fm_enter_quick_test_speaker(void);


void FM_AutoTest_Speaker(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_speaker();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Speaker
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_speaker(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	//EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);
	common_cntx = &(g_fm_contxt->common_contxt);
	
	
	  
	if (mmi_frm_scrn_enter
		 (g_fm_gourp_id, 
		  SCR_ID_FM_AUTO_TEST_START, 
		  NULL, 
		  mmi_fm_enter_quick_test_speaker, 
		  MMI_FRM_FULL_SCRN) != MMI_TRUE)
	{
		return;
	}

	common_cntx->currentTest = FM_TEST_SPEAKER;

    
	FM_SendStopAudioReq(0);
	g_fm_contxt->loudspk_contxt.LoudSpkTestOn = MMI_TRUE;
	FM_SendSetAudioModeReq(AUD_MODE_LOUDSPK);

    kal_sleep_task(kal_milli_secs_to_ticks(800));
    
	// play 1K tone
	TONE_SetOutputVolume(255, 0);	
	mdi_audio_play_id(TONE_KEY_NORMAL, DEVICE_AUDIO_PLAY_INFINITE);
    
    
	switch (common_cntx->sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_SPEAKER),SPEAKER_DURATION,FM_AutoTest_Speaker);

			break;
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_SPEAKER].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)common_cntx->EMFMUnicodeDisplayBuf,0,NULL);
			g_fm_contxt->loudspk_contxt.LoudSpkTestOn = MMI_FALSE;
			FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
			/* stop 1K tone */
			mdi_audio_stop_id(TONE_KEY_NORMAL);
			
			break;
	}
}

#define MMI_FM_QUICK_TEST_HEADSET

void mmi_fm_enter_quick_test_headset(void);


void FM_AutoTest_Headset(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_headset();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Headset
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_headset(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;
	
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   // EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);
   common_cntx = &(g_fm_contxt->common_contxt);
	
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_headset, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
		

	common_cntx->currentTest = FM_TEST_HEADSET;

    
	FM_SendStopAudioReq(0);
	//Media_Stop();
	g_fm_contxt->eachloop_contxt.HeadsetTestOn = TRUE;
#ifndef __PY_FACTORY_MODE__
	FM_SendSetAudioModeReq(AUD_MODE_HEADSET);
	// open loopback
	kal_sleep_task(kal_milli_secs_to_ticks(800));
	//mmi_fm_set_loopback(MMI_TRUE);
	aud_util_proc_in_med(MOD_MMI,
				mmi_fm_set_loopback,
				MMI_TRUE,
				NULL);
#endif /*__PY_FACTORY_MODE__*/

	switch (common_cntx->sequence_counter)
	{
		case 0:
    #ifdef __PY_FACTORY_MODE__
            if(srv_earphone_is_plug_in() == MMI_FALSE && g_FM_AutoTest_Mode != FM_AUTOTEST_MODE_664)
            {
                mmi_fm_auto_test_common(NOSENDKEY_UNCNT_STOP_SHOW,(U8*) GetString(STR_ID_FM_PLEASE_PLUG_IN_EARPHONE),HEADSET_DURATION,FM_AutoTest_Headset);
                SetLeftSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
                SetRightSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
                break;
            }
            common_cntx->sequence_counter ++;
		case 1:
            Media_Stop();
            FM_SendSetAudioModeReq(AUD_MODE_HEADSET);
            // open loopback
            kal_sleep_task(kal_milli_secs_to_ticks(200));
            //mmi_fm_set_loopback(MMI_TRUE);
            aud_util_proc_in_med(MOD_MMI,
                        mmi_fm_set_loopback,
                        MMI_TRUE,
                        NULL);
            FactoryModeSendMsg(MSG_ID_FM_TEST_ENTER_REQ_IND, NULL, NULL);           
            L1SP_SetMicrophoneVolume(255);
            L1SP_SetOutputVolume(255,0);
    #endif /*__PY_FACTORY_MODE__*/
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_HEADSET),HEADSET_DURATION,FM_AutoTest_Headset);
			
			break;
		default:

			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_HEADSET].name);
			g_fm_contxt->eachloop_contxt.HeadsetTestOn = FALSE;

			/* set headset mode */
			FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
			/* close loopback */
			kal_sleep_task(kal_milli_secs_to_ticks(800));
			//mmi_fm_set_loopback(MMI_FALSE);
			    aud_util_proc_in_med(MOD_MMI,
				mmi_fm_set_loopback,
				MMI_FALSE,
				NULL);
    #ifdef __PY_FACTORY_MODE__
            FactoryModeSendMsg(MSG_ID_FM_TEST_LEAVE_REQ_IND, NULL, NULL);
    #endif /*__PY_FACTORY_MODE__*/
			
			mmi_fm_auto_test_common(SENDKEY_CNT_STOP_SHOW,(U8*)common_cntx->EMFMUnicodeDisplayBuf,0,NULL);

	}
}

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTestUpdateADCHdlr
 * DESCRIPTION
 *	
 * PARAMETERS
 *	inMsg		[?] 	
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTestUpdateADCHdlr(void *inMsg)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	double vbat, temp;
	mmi_eq_adc_all_channel_ind_struct *adc_struct = (mmi_eq_adc_all_channel_ind_struct*) inMsg;
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
	
	vbat = ((double)adc_struct->vbat) / 1000000.0;
	temp = ((double)adc_struct->bat_temp) / 1000.0;

	if (vbat < VBAT_LOWER_BOUNDARY || vbat > VBAT_UPPER_BOUNDARY)
	{
		sprintf(
			(CHAR*) common_cntx->EMFMAsciiDisplayBuf,
			"%s%4.2f V\nValid Range:\n%4.2f V - %4.2f V\n\nsuggest: FAIL",
			"vbat=",
			vbat,
			VBAT_LOWER_BOUNDARY,
			VBAT_UPPER_BOUNDARY);
	}
	else if (temp < TEMP_LOWER_BOUNDARY || temp > TEMP_UPPER_BOUNDARY)
	{
		sprintf(
			(CHAR*) common_cntx->EMFMAsciiDisplayBuf,
			"%s%4.2f C\nValid Range:\n%d C - %d C\n\nsuggest: FAIL",
			"temp=",
			temp,
			TEMP_LOWER_BOUNDARY,
			TEMP_UPPER_BOUNDARY);
	}	
	else
	{
	    sprintf((CHAR*) common_cntx->EMFMAsciiDisplayBuf, "BATTERY OK!");		
	}

	mmi_asc_to_ucs2((CHAR *) common_cntx->EMFMUnicodeDisplayBuf, (CHAR*) common_cntx->EMFMAsciiDisplayBuf);


   mmi_frm_scrn_enter
		  (g_fm_gourp_id, 
		   GLOBAL_SCR_DUMMY, 
		   NULL, 
		   NULL, 
		   MMI_FRM_FULL_SCRN);
	mmi_frm_scrn_close_active_id();

	
	mmi_frm_set_single_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
									NULL);
	
	mmi_frm_set_single_protocol_event_handler(PRT_BATTERY_STATUS_IND, 
									(PsIntFuncPtr)BatteryStatusRsp);

	FM_SendADCStopReq();
}


#define MMI_FM_QUICK_TEST_BATTERY_CHARGER

void mmi_fm_autotest_battery_charger()
{	
	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_HIDE,0,ADC_DURATION,mmi_fm_autotest_battery_charger);

			break;
		default:
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_HIDE,0,0,NULL);
			
	}
}


static void mmi_fm_enter_quick_test_battery(void);


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Battery
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_Battery(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	g_fm_contxt->common_contxt.currentTest = FM_TEST_BATTERY;

	mmi_frm_set_single_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
									(PsIntFuncPtr)FM_AutoTestUpdateADCHdlr);

	FM_SendADCStartReq();

    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

	mmi_fm_enter_quick_test_battery();
}

static void mmi_fm_enter_quick_test_battery(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx = &(g_fm_contxt->common_contxt);

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   
    if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
	    SCR_ID_FM_AUTO_TEST_START, 
	    NULL, 
	    mmi_fm_enter_quick_test_battery, 
	    MMI_FRM_FULL_SCRN) != MMI_TRUE)
	  {
	 		return;
	  }

   ShowCategory7Screen(
		STR_ID_FM_AUTO_TEST_RESULT,
		NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
		STR_ID_FM_AUTO_TEST_PASS,
		IMG_GLOBAL_OK,
		STR_ID_FM_AUTO_TEST_FAIL,
		IMG_GLOBAL_BACK,
		(PU8) common_cntx->EMFMUnicodeDisplayBuf,
		NULL);

	mmi_fm_autotest_battery_charger();
}


#define MMI_FM_QUICK_TEST_MELODY

void mmi_fm_enter_quick_test_melody(void);


void FM_AutoTest_Melody(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_melody();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Melody
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_melody(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	//EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);

	
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_melody, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_MELODY;


	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_MELODY),MELODY_DURATION,FM_AutoTest_Melody);
			g_fm_contxt->eachloop_contxt.RingToneTestOn = TRUE;
			Media_SetOutputVolume(255, 0);
			/* stop MIDI */
			FM_SendStopAudioReq(0);
			/* play MIDI_1 */
			FM_SendPlayAudioReq(0);
			
			break;
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_MELODY].name);
			g_fm_contxt->eachloop_contxt.RingToneTestOn = FALSE;
			FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
			/* stop MIDI */
			FM_SendStopAudioReq(0);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*)g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);

	}
}

#define MMI_FM_QUICK_TEST_VIB

void mmi_fm_enter_quick_test_vib(void);


void FM_AutoTest_VIB(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_vib();
}

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_VIB
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_vib(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	//EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);

	
	if (mmi_frm_scrn_enter
	    (g_fm_gourp_id, 
		  SCR_ID_FM_AUTO_TEST_START, 
	  	NULL, 
		  mmi_fm_enter_quick_test_vib, 
	  	MMI_FRM_FULL_SCRN) != MMI_TRUE)
	  {
	  	return;
	  }

	g_fm_contxt->common_contxt.currentTest = FM_TEST_VIB;


	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
        case 2:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_VIB),VIB_DURATION,FM_AutoTest_VIB);
			FM_SetGpioVirbateReq(VIBRATOR_ON);
			break;	
            
		case 1:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTO_TEST_FAIL),VIB_DURATION,FM_AutoTest_VIB);
			FM_SetGpioVirbateReq(VIBRATOR_OFF);
			break;	
            
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_VIB].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
			FM_SetGpioVirbateReq(VIBRATOR_OFF);	
	}
}


#define MMI_FM_QUICK_TEST_NAND

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_NAND
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_NAND(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_autotest_nand();
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_NAND
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_autotest_nand(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/  
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_autotest_nand, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_NAND;


	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_NAND),3000,FM_AutoTest_NAND);			   		
			   		
			break;
		case 1:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_HIDE,0,2000,FM_AutoTest_NAND);		
			mmi_fm_select_nand_flash();
			break;

		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_NAND].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
	}
}


#define MMI_FM_QUICK_TEST_UART

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_UART
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
#ifdef __MMI_FM_UART_TEST__
void FM_AutoTest_UART(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
   // EntryNewScreen(SCR_ID_FM_AUTO_TEST_START, ExitFMAutoTestStart, NULL, NULL);
   mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	
	if (mmi_frm_scrn_enter
	    (g_fm_gourp_id, 
		   SCR_ID_FM_AUTO_TEST_START, 
		   NULL, 
		   FM_AutoTest_UART, 
		   MMI_FRM_FULL_SCRN) != MMI_TRUE)
	{
		return;
	}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_UART;


	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_UART),1000,FM_AutoTest_UART);
			
			break;
		case 1:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_HIDE,0,2000,FM_AutoTest_UART);
			mmi_fm_select_uart();	//EntryFMUARTMenu();
					
			break;

		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_UART].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
	}
}

#endif /*__MMI_FM_UART_TEST__*/

#define MMI_FM_QUICK_TEST_DOUBLE_SPEAKER

void mmi_fm_enter_quick_test_double_spk(void);


void FM_AutoTest_Double_Speaker(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	mmi_fm_enter_quick_test_double_spk();
}




/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Double_Speaker
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_double_spk(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		mmi_fm_enter_quick_test_double_spk, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

	g_fm_contxt->common_contxt.currentTest = FM_TEST_DOUBLE_SPEAKER;

	FM_SendStopAudioReq(0);
	//Media_Stop();
	g_fm_contxt->loudspk_contxt.LoudSpkTestOn = MMI_TRUE;
	FM_SendSetAudioModeReq(AUD_MODE_LOUDSPK);
	/* play 1K tone */
	TONE_SetOutputVolume(255, 0);

	/* sequence_counter=0; */
	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mdi_audio_play_id(TONE_KEY_NORMAL, DEVICE_AUDIO_PLAY_INFINITE);
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_LEFT_SPEAKER),SPEAKER_DURATION,FM_AutoTest_Double_Speaker);

			break;
		case 1:

			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_GLOBAL_STOP),1000,FM_AutoTest_Double_Speaker);
			mdi_audio_stop_id(TONE_KEY_NORMAL);
				
			break;
		case 2:
			mdi_audio_play_id(TONE_KEY_NORMAL, DEVICE_AUDIO_PLAY_INFINITE);
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_SHOW,(U8*) GetString(STR_ID_FM_AUTOTEST_RIGHT_SPEAKER),SPEAKER_DURATION,FM_AutoTest_Double_Speaker);
				
			break;
		default:
			FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_DOUBLE_SPEAKER].name);
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_SHOW,(U8*) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,0,NULL);
			g_fm_contxt->loudspk_contxt.LoudSpkTestOn = MMI_FALSE;

			FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
			/* stop 1K tone */
			mdi_audio_stop_id(TONE_KEY_NORMAL);
	}
}

#define MMI_FM_QUICK_TEST_CAMERA
#ifndef __MMI_MAINLCD_96X64__
/*****************************************************************************
 * FUNCTION
 *	EntryFMCameraTransientScreen
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
static void EntryFMCameraTransientScreen(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);  
	
	if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
		SCR_ID_FM_AUTO_TEST_START, 
		NULL, 
		FM_AutoTest_CAMERA, 
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	
	FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_CAMERA].name);

	ShowCategory7Screen(
		STR_ID_FM_AUTO_TEST_RESULT,
		NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
		STR_ID_FM_AUTO_TEST_PASS,
		IMG_GLOBAL_OK,
		STR_ID_FM_AUTO_TEST_FAIL,
		IMG_GLOBAL_BACK,
		(PU8) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,
		NULL);

	FM_Autotest_set_key_handlers(1);

}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_CAMERA
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_CAMERA(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

    static U16 isRedraw = 0;
	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	g_fm_contxt->common_contxt.currentTest = FM_TEST_CAMERA;

#ifdef __MMI_FM_CAMERA_PREVIEW__

	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
            if(0 == isRedraw)
            {

			g_fm_contxt->common_contxt.sequence_counter++;
			mmi_fm_camera_entry_preview_screen();
			StartTimer(FM_AUTO_TEST_COMMNON_TIMER, CAMERA_DURATION, FM_AutoTest_CAMERA);
            }
			break;
		default:
            isRedraw = 1;

			g_fm_contxt->common_contxt.sequence_counter = 0;
			StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
			mmi_fm_close_camera();
			EntryFMCameraTransientScreen();
			
            isRedraw = 0;
	}
 
#else
	{
	    EntryFMCameraTransientScreen();	
	}
#endif

}
#endif /* __MMI_MAINLCD_96X64__ */

#define MMI_FM_QUICK_TEST_MEMORYCARD

void mmi_fm_enter_quick_test_memorycard(void);



void FM_AutoTest_MemoryCard(void)
{
	mmi_frm_scrn_close(g_fm_gourp_id,SCR_ID_FM_AUTO_TEST_START);//SCR_ID_FM_MEMORY_CARD);
	mmi_fm_enter_quick_test_memorycard();
}




/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_MemoryCard
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void mmi_fm_enter_quick_test_memorycard(void)
{
#if defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__)
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	UI_character_type text1[32];
	U8 u8text[32];

	mmi_fm_memcard_struct *memcard_cntx;
#endif /* defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__) */ 

	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
	
	common_cntx->currentTest = FM_TEST_MEMORYCARD;

	
	if (mmi_frm_scrn_enter
		(g_fm_gourp_id,
		SCR_ID_FM_AUTO_TEST_START,//SCR_ID_FM_MEMORY_CARD,
		NULL,
		mmi_fm_enter_quick_test_memorycard,
		MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}

   // EntryNewScreen(SCR_ID_FM_MEMORY_CARD, NULL, FM_AutoTest_MemoryCard, NULL);

#if defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__)
	memcard_cntx = &(g_fm_contxt->memcard_contxt);

	if (common_cntx->sequence_counter < 6)
	{
		if (memcard_cntx->FM_Card_Status.present == TRUE)
		{
			common_cntx->sequence_counter = 6;
			switch (memcard_cntx->FM_Card_Status.type)
			{
				case MS_CARD:
					mmi_asc_to_ucs2((CHAR*) memcard_cntx->CardType, "MS_CARD.");
					break;
				case SD_CARD:
					mmi_asc_to_ucs2((CHAR*) memcard_cntx->CardType, "SD_CARD.");
					break;
				case MMC_CARD:
					mmi_asc_to_ucs2((CHAR*) memcard_cntx->CardType, "MMC_CARD.");
					break;
				default:
					mmi_asc_to_ucs2((CHAR*) memcard_cntx->CardType, "Error!!!");
			}
			StartTimer(FM_AUTO_TEST_COMMNON_TIMER, 300, FM_AutoTest_MemoryCard);
		}
		else
		{
			common_cntx->sequence_counter++;

			if (memcard_cntx->Card_Response_Sent == FALSE)
			{
				FM_MemoryCardReq();
			}

			sprintf(
				(CHAR*) u8text,
				"\nPlease Insert Memory Card....%d",
				(MEMORYCARD_DURATION / 1000) - common_cntx->sequence_counter);
			mmi_asc_to_ucs2((CHAR*) text1, (CHAR*) u8text);

			
			ShowCategory7Screen(
				STR_GLOBAL_MEMORY_CARD,
				NULL,
				STR_ID_FM_AUTO_TEST_PASS,
				0,
				STR_ID_FM_AUTO_TEST_FAIL,
				0,
				(PU8) text1,
				NULL);
							  
			FM_Autotest_set_key_handlers(1);

			StartTimer(FM_AUTO_TEST_COMMNON_TIMER, 1000, FM_AutoTest_MemoryCard);
			if ((common_cntx->sequence_counter >= (MEMORYCARD_DURATION / 1000) - 1) && memcard_cntx->FM_Card_Status.present == FALSE)
			{	/* No Card inserted. */
				mmi_asc_to_ucs2((CHAR*) memcard_cntx->CardType, "No Card.");

                //StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
				//StartTimer(FM_AUTO_TEST_COMMNON_TIMER, 300, FM_AutoTest_MemoryCard);
				//common_cntx->sequence_counter++;

				common_cntx->sequence_counter = 6;
			}
		}
	}
	else
#endif /* defined(__MSDC_MS__) || defined(__MSDC_SD_MMC__) || defined(__MSDC_MSPRO__) */ 
	{
		common_cntx->sequence_counter = 0;
		StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
		FM_Autotest_test_done_USC2string(Tests[FM_AUTOTEST_MEMORYCARD].name);
		ShowCategory7Screen(
			STR_ID_FM_AUTO_TEST_RESULT,
			NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
			STR_ID_FM_AUTO_TEST_PASS,
			IMG_GLOBAL_OK,
			STR_ID_FM_AUTO_TEST_FAIL,
			IMG_GLOBAL_BACK,
			(PU8) common_cntx->EMFMUnicodeDisplayBuf,
			NULL);

		FM_Autotest_set_key_handlers(1);
	}
}

#define MMI_FM_QUICK_TEST_TOUCH_SCREEN


#ifdef __MMI_TOUCH_SCREEN__

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTest_Pen_Parallel_Test
 * DESCRIPTION
 *	This function is for Parallel line Test in Auto Test mode
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTest_Pen_Parallel_Test(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	g_fm_contxt->common_contxt.currentTest = FM_TEST_PEN_PARALLEL_LINES;
	mmi_fm_select_pen_parallel_test();
}

#endif /*__MMI_TOUCH_SCREEN__*/


//-------------------SIM------------------------------------
#ifdef __SUPPORT_SIM_DETECT__

/*****************************************************************************
 * FUNCTION
 *  FM_AutoTest_Keypad
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_fm_enter_quick_test_sim(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx;
    MMI_BOOL isSimValid = MMI_FALSE;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);

    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
    
	if (mmi_frm_scrn_enter
        (g_fm_gourp_id, 
        SCR_ID_FM_AUTO_TEST_START, 
        NULL, 
        mmi_fm_enter_quick_test_sim, 
        MMI_FRM_FULL_SCRN) != MMI_TRUE)
	{
		return;
	}

    isSimValid =
    #ifdef __MMI_DUAL_SIM_MASTER__
        common_cntx->currentTest == FM_TEST_SIM2
        ? mmi_bootup_is_sim2_valid() :
    #endif /*__MMI_DUAL_SIM_MASTER__*/
        mmi_bootup_is_sim_valid();

    if(MMI_TRUE ==  isSimValid)
    {
        FM_Autotest_test_done_USC2string(Tests[common_cntx->currentTest].name);
        ShowCategory7Screen(
            STR_ID_FM_AUTO_TEST_RESULT,
            NULL,
            STR_ID_FM_AUTO_TEST_PASS,
            IMG_GLOBAL_OK,
            NULL,
            NULL,
            (PU8) common_cntx->EMFMUnicodeDisplayBuf,
            NULL);

        FM_Autotest_set_key_handlers(1);
    }
    else
    {
        FM_Autotest_test_done_USC2string(Tests[common_cntx->currentTest].name);  
        ShowCategory7Screen(
            STR_ID_FM_AUTO_TEST_RESULT,
            NULL,
            NULL,
            NULL,
            STR_ID_FM_AUTO_TEST_FAIL,
            IMG_GLOBAL_BACK,
            (PU8) common_cntx->EMFMUnicodeDisplayBuf,
            NULL);
        FM_Autotest_set_key_handlers(1);
    }
}

#ifndef __MMI_DUAL_SIM_MASTER__
void FM_AutoTest_SIM(void) {
    g_fm_contxt->common_contxt.currentTest = FM_TEST_SIM;
    mmi_fm_enter_quick_test_sim();
}
#else /*__MMI_DUAL_SIM_MASTER__*/
void FM_AutoTest_SIM1(void) {
    g_fm_contxt->common_contxt.currentTest = FM_TEST_SIM1;
    mmi_fm_enter_quick_test_sim();
}

void FM_AutoTest_SIM2(void) {
    g_fm_contxt->common_contxt.currentTest = FM_TEST_SIM2;
    mmi_fm_enter_quick_test_sim();
}
#endif /*__MMI_DUAL_SIM_MASTER__*/

#endif /*__SUPPORT_SIM_DETECT__*/
#ifdef __SUPPORT_SPEAKER_VIB__

#define FM_TEST_SPEAKER_VIB_FILENAME_LEN    40
#define FM_TEST_SPEAKER_VIB_FILENAME        "test.mp3"

/*****************************************************************************
 * FUNCTION
 *  mmi_test_play_mp3_callback
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_test_play_mp3_callback(mdi_result result, void* user_data)
{
    mdi_audio_stop_file();
}

void FM_AutoTest_Speaker_VIB_End_Key_Handler(void)
{
    if (FM_AUTOTEST_MODE_664 == g_FM_AutoTest_Mode)
    {
        mdi_audio_stop_string();
    }
    else
    {
        mdi_audio_stop_file();
    }
    FM_SetGpioReq(GPIO_DEV_VIBRATOR, VIBRATOR_OFF);
    StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
    g_fm_contxt->loudspk_contxt.LoudSpkTestOn = FALSE;
    FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
    /* stop 1K tone */
    TONE_Stop();
    mmi_idle_display();
}

void mmi_fm_enter_quick_test_speaker_vib(void) {
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    S32 result;
    S8 nTestFileNameS[FM_TEST_SPEAKER_VIB_FILENAME_LEN];
    U16 nTestFileName[FM_TEST_SPEAKER_VIB_FILENAME_LEN];
	mmi_fm_common_struct *common_cntx;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
    common_cntx->currentTest = FM_TEST_SPEAKER_VIB;

    //EntryNewScreen(GLOBAL_SCR_DUMMY, NULL, NULL, NULL);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
    
    if (mmi_frm_scrn_enter(
            g_fm_gourp_id, 
            SCR_ID_FM_AUTO_TEST_START, 
            NULL, 
            mmi_fm_enter_quick_test_speaker_vib, 
            MMI_FRM_FULL_SCRN) != MMI_TRUE) {
        return ;
    }

    sprintf(
        (S8*) nTestFileNameS,
        "%c:\\%s",
        FS_GetDrive( FS_DRIVE_V_REMOVABLE, 1, FS_NO_ALT_DRIVE ),
        FM_TEST_SPEAKER_VIB_FILENAME);

    mmi_asc_n_to_ucs2((S8 *)nTestFileName, (S8 *)nTestFileNameS, FM_TEST_SPEAKER_VIB_FILENAME_LEN);


    FM_SendStopAudioReq(0);
    Media_Stop();
    FM_SetGpioReq(GPIO_DEV_VIBRATOR, VIBRATOR_OFF);	
    g_fm_contxt->loudspk_contxt.LoudSpkTestOn = TRUE;
    FM_SendSetAudioModeReq(AUD_MODE_LOUDSPK);

    /* play 1K tone */    
    if(MMI_FALSE)
    {	
        TONE_SetOutputVolume(0xFF, 0);
        //TONE_Play((const L1SP_Tones*)test_tone);
    }

    switch (common_cntx->sequence_counter)
    {
        case 0:
            common_cntx->sequence_counter++;            
            if(MMI_TRUE)
            {
                Media_SetOutputVolume(255, 0);
                FM_SetGpioReq(GPIO_DEV_VIBRATOR, VIBRATOR_ON);
                if (FM_AUTOTEST_MODE_664 == g_FM_AutoTest_Mode)
                {
                    U8 l_nType, *l_pAudio;
                    U32 l_nSize;
                    l_pAudio = get_audio(AUD_ID_PROF_TONE1, &l_nType, &l_nSize);
                    result = mdi_audio_play_string_with_vol_path(
                                l_pAudio,
                                l_nSize,
                                l_nType,
                                DEVICE_AUDIO_PLAY_INFINITE,
                                NULL,
                                NULL,
                                MDI_AUD_VOL_EX(10),
                                MDI_AUD_PTH_EX(MDI_DEVICE_SPEAKER_BOTH));
                }
                else
                {
                    result = mdi_audio_play_file_with_vol_path(
                                nTestFileName,
                                DEVICE_AUDIO_PLAY_ONCE,
                                NULL,
                                mmi_test_play_mp3_callback,
                                NULL,
                                MDI_AUD_VOL_EX(10),
                                MDI_AUD_PTH_EX(MDI_DEVICE_SPEAKER_BOTH));
                }
                
                StartTimer(FM_AUTO_TEST_COMMNON_TIMER, 5000, mmi_fm_enter_quick_test_speaker_vib);
            }	
            else
            {
                StartTimer(FM_AUTO_TEST_COMMNON_TIMER, SPEAKER_DURATION, mmi_fm_enter_quick_test_speaker_vib);
            }	
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_ROOT,
                NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                STR_ID_FM_AUTO_TEST_FAIL,
                IMG_GLOBAL_BACK,
                (U8*) GetString(STR_ID_FM_AUTOTEST_SPEAKER),
                NULL);
            FM_Autotest_set_key_handlers(1); 
            SetKeyHandler(FM_AutoTest_Speaker_VIB_End_Key_Handler, KEY_END, KEY_EVENT_UP);
            if(MMI_TRUE)
            {
                SetLeftSoftkeyFunction(mmi_fm_enter_quick_test_speaker_vib, KEY_EVENT_UP);		
            }	
            break;
        default:
            common_cntx->sequence_counter = 0;          
            if(MMI_TRUE)
            {
                if (FM_AUTOTEST_MODE_664 == g_FM_AutoTest_Mode)
                {
                    mdi_audio_stop_string();
                }
                else
                {
                    mdi_audio_stop_file();
                }
                FM_SetGpioReq(GPIO_DEV_VIBRATOR, VIBRATOR_OFF);
            }	
            StopTimer(FM_AUTO_TEST_COMMNON_TIMER);

            g_fm_contxt->loudspk_contxt.LoudSpkTestOn = FALSE;

            FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
            /* stop 1K tone */
            TONE_Stop();

            {
                FM_Autotest_test_done_USC2string(Tests[FM_TEST_SPEAKER].name);
                ShowCategory7Screen(
                    STR_ID_FM_AUTO_TEST_RESULT,
                    NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
                    STR_ID_FM_AUTO_TEST_PASS,
                    IMG_GLOBAL_OK,
                    STR_ID_FM_AUTO_TEST_FAIL,
                    IMG_GLOBAL_BACK,
                    (PU8) common_cntx->EMFMUnicodeDisplayBuf,
                    NULL);

                FM_Autotest_set_key_handlers(1);
            }
    }
}

void FM_AutoTest_Speaker_Vib(void) {
    mmi_fm_enter_quick_test_speaker_vib();
}
#endif /*__SUPPORT_SPEAKER_VIB__*/
#ifdef __SUPPORT_ECHOLOOP__

void mmi_AutoTest_Echoloop(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    mmi_fm_common_struct *common_cntx;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    common_cntx = &(g_fm_contxt->common_contxt);
    common_cntx->currentTest = FM_TEST_ECHOLOOP;

    //EntryNewScreen(GLOBAL_SCR_DUMMY, NULL, NULL, NULL);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

    if (mmi_frm_scrn_enter(
            g_fm_gourp_id, 
            SCR_ID_FM_AUTO_TEST_START, 
            NULL, 
            mmi_AutoTest_Echoloop, 
            MMI_FRM_FULL_SCRN) != MMI_TRUE)
    {
        return ;
    }
    
#ifdef MMI_ON_HARDWARE_P
    FM_SendStopAudioReq(0);
    Media_Stop();

    switch (common_cntx->sequence_counter)
    {
        case 0:
            common_cntx->sequence_counter++;

            FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
            /* open loopback */
            kal_sleep_task(kal_milli_secs_to_ticks(100));
            //mmi_fm_set_loopback(MMI_TRUE);
            aud_util_proc_in_med(MOD_MMI,
                mmi_fm_set_loopback,
                MMI_TRUE,
                NULL);
            L1SP_SetMicrophoneVolume(255);
            L1SP_SetOutputVolume(255,0);
            g_fm_contxt->eachloop_contxt.EchoLoopTestOn = TRUE;

            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_ROOT,
                NULL,
                STR_GLOBAL_OK,
                IMG_GLOBAL_OK,
                NULL,
                IMG_GLOBAL_BACK,
                (U8*) L"Echo Loop Test"/*GetString(STR_ID_FM_AUTOTEST_MIC)*/,
                NULL);
            
            FM_Autotest_set_key_handlers(1);
            SetLeftSoftkeyFunction(mmi_AutoTest_Echoloop, KEY_EVENT_UP);
            SetRightSoftkeyFunction(NULL, KEY_EVENT_UP);


            break;
        default:
            common_cntx->sequence_counter = 0;
            g_fm_contxt->eachloop_contxt.EchoLoopTestOn = FALSE;

            /* set headset mode */
            FM_SendSetAudioModeReq(AUD_MODE_NORMAL);
            /* close loopback */
            kal_sleep_task(kal_milli_secs_to_ticks(100));
            //mmi_fm_set_loopback(MMI_FALSE);
            aud_util_proc_in_med(MOD_MMI,
                mmi_fm_set_loopback,
                MMI_FALSE,
                NULL);

#endif /* MMI_ON_HARDWARE_P */ 
            FM_Autotest_test_done_USC2string(Tests[FM_TEST_ECHOLOOP].name);
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_RESULT,
                NULL,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                STR_ID_FM_AUTO_TEST_FAIL,
                IMG_GLOBAL_BACK,
                (PU8) common_cntx->EMFMUnicodeDisplayBuf,
                NULL);

            FM_Autotest_set_key_handlers(1);
#ifdef MMI_ON_HARDWARE_P
    }
#endif /* MMI_ON_HARDWARE_P */ 
}


/*****************************************************************************
 * FUNCTION
 *  FM_AutoTest_Headset
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void FM_AutoTest_EchoLoop(void)
{
    mmi_AutoTest_Echoloop();
}

#endif /*__SUPPORT_ECHOLOOP__*/
#ifdef __SUPPORT_POWER_TEST__

void mmi_fm_autotest_power()
{	
	switch (g_fm_contxt->common_contxt.sequence_counter)
	{
		case 0:
			mmi_fm_auto_test_common(NOSENDKEY_CNT_START_HIDE,0,ADC_DURATION,mmi_fm_autotest_power);

			break;
		default:
			mmi_fm_auto_test_common(SENDKEY_UNCNT_STOP_HIDE,0,0,NULL);
			
	}
}

void mmi_fm_enter_quick_test_power(void)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	mmi_fm_common_struct *common_cntx = &(g_fm_contxt->common_contxt);

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
    if (mmi_frm_scrn_enter
	   (g_fm_gourp_id, 
	    SCR_ID_FM_AUTO_TEST_START, 
	    NULL, 
	    mmi_fm_enter_quick_test_power, 
	    MMI_FRM_FULL_SCRN) != MMI_TRUE)
    {
        return;
    }

    if (common_cntx->currentTest == FM_TEST_ICHR && !srv_charbat_is_charging()) {
        mmi_fm_auto_test_common(
            NOSENDKEY_UNCNT_STOP_SHOW,
            (U8 *)L"Please Insert Charger!",
            ADC_DURATION,
            mmi_fm_enter_quick_test_power);
        kal_prompt_trace(MOD_WAP, "[Julius] 1 common_cntx->sequence_counter = %d", common_cntx->sequence_counter);
        SetLeftSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
        SetRightSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
        return ;
    }

    if(gVbatPass == TRUE)
    {
	ShowCategory7Screen(
	     STR_ID_FM_AUTO_TEST_RESULT,
	     NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
	     STR_ID_FM_AUTO_TEST_PASS,
	     IMG_GLOBAL_OK,
	     NULL,
	     NULL,
	     (PU8) common_cntx->EMFMUnicodeDisplayBuf,
	     NULL);
	SetRightSoftkeyFunction(NULL, KEY_EVENT_UP); 
    }
    else
    {
        ShowCategory7Screen(
         STR_ID_FM_AUTO_TEST_RESULT,
         NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
         NULL,
         NULL,
         STR_ID_FM_AUTO_TEST_FAIL,
         IMG_GLOBAL_BACK,
         (PU8) common_cntx->EMFMUnicodeDisplayBuf,
         NULL);
	SetLeftSoftkeyFunction(NULL, KEY_EVENT_UP); 
    }

    mmi_fm_autotest_power();
}

void mmi_fm_enter_quick_test_ichr(void)
{
		/*----------------------------------------------------------------*/
		/* Local Variables												  */
		/*----------------------------------------------------------------*/
		mmi_fm_common_struct *common_cntx = &(g_fm_contxt->common_contxt);
	
		/*----------------------------------------------------------------*/
		/* Code Body													  */
		/*----------------------------------------------------------------*/
		if (mmi_frm_scrn_enter(
				g_fm_gourp_id, 
				SCR_ID_FM_AUTO_TEST_START, 
				NULL, 
				mmi_fm_enter_quick_test_ichr, 
				MMI_FRM_FULL_SCRN) != MMI_TRUE)
		{
			return;
		}
	
		if (common_cntx->currentTest == FM_TEST_ICHR && !srv_charbat_is_charging())
		{
			mmi_fm_auto_test_common(
				NOSENDKEY_UNCNT_STOP_SHOW,
				(U8 *)L"Please Insert Charger!",
				ADC_DURATION,
				NULL);
			StartTimer(FM_AUTO_TEST_COMMNON_TIMER, ADC_DURATION, FM_ReTest);
			SetLeftSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
			SetRightSoftkeyFunction(FM_ReTest, KEY_EVENT_UP);
			return ;
		}
	
		if (common_cntx->sequence_counter < POWER_MEASURE_TOTAL && gIsPowerTestPass == MMI_FALSE)
		{
			mmi_frm_set_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
				(PsIntFuncPtr)FM_AutoTestUpdateAdcStepHdlr, MMI_FALSE);
	
			FM_SendADCStartReq();
	
			StartTimer(FM_AUTO_TEST_COMMNON_TIMER, 500, mmi_fm_enter_quick_test_ichr);
			ShowCategory7Screen(
				STR_ID_FM_AUTO_TEST_RESULT,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL,
				(PU8) common_cntx->EMFMUnicodeDisplayBuf,
				NULL);
			FM_Autotest_set_key_handlers(MMI_FALSE);	
		}
		else
		{
			StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
			ShowCategory7Screen(
				STR_ID_FM_AUTO_TEST_RESULT,
				NULL,
				gIsPowerTestPass ? STR_ID_FM_AUTO_TEST_PASS : NULL,
				NULL,
				gIsPowerTestPass ? NULL : STR_ID_FM_AUTO_TEST_FAIL,
				NULL,
				(PU8) common_cntx->EMFMUnicodeDisplayBuf,
				NULL);
			common_cntx->sequence_counter = 0;
			FM_Autotest_set_key_handlers(MMI_TRUE);
		}
}


/*****************************************************************************
 * FUNCTION
 *	FM_AutoTestUpdateVBatHdlr
 * DESCRIPTION
 *	
 * PARAMETERS
 *	inMsg		[?] 	
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTestUpdateVBatHdlr(void *inMsg)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	double vbat, temp;
	mmi_eq_adc_all_channel_ind_struct *adc_struct = (mmi_eq_adc_all_channel_ind_struct*) inMsg;
	mmi_fm_common_struct *common_cntx;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
	
	vbat = ((double)adc_struct->vbat) / 1000000.0;

	if((vbat >= VBAT_LOWER_BOUNDARY) && (vbat <= VBAT_UPPER_BOUNDARY))
	{
    	    gVbatPass = TRUE;
	}
	
    sprintf(
        (CHAR*) common_cntx->EMFMAsciiDisplayBuf,
        "%s (%d/%d)\nCurrent: %4.2f V\nValid Range:\n%4.2f V - %4.2f V\n\nResult: %s",
        Tests[common_cntx->currentTest].name,
        common_cntx->currTestItem + 1, 
        g_fm_AutoTestListSize,
        vbat,
        VBAT_LOWER_BOUNDARY,
        VBAT_UPPER_BOUNDARY,
        vbat < VBAT_LOWER_BOUNDARY || vbat > VBAT_UPPER_BOUNDARY ? "Fail" : "Success");

    kal_prompt_trace(MOD_WAP, common_cntx->EMFMAsciiDisplayBuf);
	mmi_asc_to_ucs2((CHAR *) common_cntx->EMFMUnicodeDisplayBuf, (CHAR*) common_cntx->EMFMAsciiDisplayBuf);

    mmi_frm_scrn_enter
		  (g_fm_gourp_id, 
		   GLOBAL_SCR_DUMMY, 
		   NULL, 
		   NULL, 
		   MMI_FRM_FULL_SCRN);

	mmi_frm_scrn_close_active_id();

	
	mmi_frm_set_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
									NULL, MMI_FALSE);
	
	mmi_frm_set_protocol_event_handler(PRT_BATTERY_STATUS_IND, 
									(PsIntFuncPtr)BatteryStatusRsp, MMI_FALSE);

	FM_SendADCStopReq();
}

/*****************************************************************************
 * FUNCTION
 *  FM_AutoTest_VBat_Test
 * DESCRIPTION
 *  This function is for FM Test mode
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void FM_AutoTest_VBat_Test(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    g_fm_contxt->common_contxt.currentTest = FM_TEST_VBAT;

    mmi_frm_set_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
                                    (PsIntFuncPtr)FM_AutoTestUpdateVBatHdlr, MMI_FALSE);

    FM_SendADCStartReq();

    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

    mmi_fm_enter_quick_test_power();
}

/*****************************************************************************
 * FUNCTION
 *	FM_AutoTestUpdateVBatHdlr
 * DESCRIPTION
 *	
 * PARAMETERS
 *	inMsg		[?] 	
 * RETURNS
 *	void
 *****************************************************************************/
void FM_AutoTestUpdateIChrHdlr(void *inMsg)
{
	/*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/
	double adc_value, vchr_value;
	mmi_eq_adc_all_channel_ind_struct *adc_struct = (mmi_eq_adc_all_channel_ind_struct*) inMsg;
	mmi_fm_common_struct *common_cntx;
    MMI_BOOL bIsPass;

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
	common_cntx = &(g_fm_contxt->common_contxt);
	
	adc_value = ((double)adc_struct->charge_current) / 1000000.0;
    vchr_value = ((double) adc_struct->vcharge) / 1000000.0;
    
    bIsPass = (adc_value >= 0.1 && vchr_value >=4.2);
    gIChrPass = bIsPass;
    sprintf(
        (CHAR*) common_cntx->EMFMAsciiDisplayBuf,
        "%s (%d/%d)\nVoltage: %4.2f V\nCurrent: %4.2f A\n\nResult: %s",
        Tests[common_cntx->currentTest].name,
        common_cntx->currTestItem + 1, 
        g_fm_AutoTestListSize,
        vchr_value,
        adc_value,
        bIsPass ? "Success" : "Fail");

    kal_prompt_trace(MOD_WAP, common_cntx->EMFMAsciiDisplayBuf);
	mmi_asc_to_ucs2((CHAR *) common_cntx->EMFMUnicodeDisplayBuf, (CHAR*) common_cntx->EMFMAsciiDisplayBuf);

    mmi_frm_scrn_enter
		  (g_fm_gourp_id, 
		   GLOBAL_SCR_DUMMY, 
		   NULL, 
		   NULL, 
		   MMI_FRM_FULL_SCRN);

	mmi_frm_scrn_close_active_id();

	
	mmi_frm_set_protocol_event_handler(MSG_ID_MMI_EQ_ADC_ALL_CHANNEL_IND, 
									NULL, MMI_FALSE);
	
	mmi_frm_set_protocol_event_handler(PRT_BATTERY_STATUS_IND, 
									(PsIntFuncPtr)BatteryStatusRsp, MMI_FALSE);

	FM_SendADCStopReq();
}

/*****************************************************************************
 * FUNCTION
 *  FM_AutoTest_VBat_Test
 * DESCRIPTION
 *  This function is for FM Test mode
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void FM_AutoTest_IChr_Test(void)
	{
		/*----------------------------------------------------------------*/
		/* Local Variables												  */
		/*----------------------------------------------------------------*/
	
		/*----------------------------------------------------------------*/
		/* Code Body													  */
		/*----------------------------------------------------------------*/
		g_fm_contxt->common_contxt.currentTest = FM_TEST_ICHR;
	
		mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
	
		gIsPowerTestPass = MMI_FALSE;
		mmi_fm_enter_quick_test_ichr();
	}


#endif /*__SUPPORT_POWER_TEST__*/
#if defined(__SUPPORT_BLUETOOTH__)

void fm_set_bt_flag(S32 flag)
{
    BTFMresult = flag;

}

S32 fm_get_bt_flag(void)
{
    return BTFMresult;
}

/*****************************************************************************
 * FUNCTION
 *  EntryFMMenu
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void EnteyFMBTTest(void)		
{
	//U8 *history_buffer;
	/*----------------------------------------------------------------*/
	/* Code Body                                                      */
	/*----------------------------------------------------------------*/
    fm_set_bt_flag(0);
    //mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
#if defined(__SUPPORT_BLUETOOTH__)
	mmi_bt_test_power_on();
#endif
}


void FM_AutoTest_Stop_BT_Test(void)
{
    
    //EntryNewScreen(SCR_ID_BT_RESULT, NULL, FM_AutoTest_Stop_BT_Test, NULL);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

    mmi_frm_scrn_enter
    (g_fm_gourp_id, 
    SCR_ID_FM_AUTO_TEST_START, 
    NULL, 
    FM_AutoTest_Stop_BT_Test, 
    MMI_FRM_FULL_SCRN);
    StopTimer(BT_PLAY_TEST_AUDIO_TIMER);
    
    FM_Autotest_test_done_USC2string(Tests[FM_TEST_BT].name);
    ShowCategory7Screen(
        STR_ID_FM_AUTO_TEST_RESULT,
        NULL,
        STR_ID_FM_AUTO_TEST_PASS,
        IMG_GLOBAL_OK,
        NULL,
        NULL,
        (PU8) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,
        NULL);
    
    BTFMresult = 0xff;
//    DeleteScreens(SCR_ID_BT_TEST, SCR_ID_BT_RESULT);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_BT_TEST);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_BT_RESULT);
    
    FM_Autotest_set_key_handlers(1);
}


void FM_AutoTest_BT_Test(void)
{
    g_fm_contxt->common_contxt.currentTest = FM_TEST_BT;
    //EntryNewScreen(SCR_ID_BT_TEST, NULL, FM_AutoTest_BT_Test, NULL);
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_BT_TEST);

    mmi_frm_scrn_enter(
        g_fm_gourp_id, 
        SCR_ID_BT_TEST,//SCR_ID_BT_TEST, 
        NULL, 
        FM_AutoTest_BT_Test, 
        MMI_FRM_FULL_SCRN);
        
//    kal_prompt_trace(MOD_WAP,"FactoryModeSrc.c Line: %d",__LINE__);
    if(fm_get_bt_flag() == 0xff)
    {
        if(g_FM_AutoTest_Mode == FM_AUTOTEST_MODE_664)
        {
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_RESULT,
                NULL,
                STR_GLOBAL_START,
                IMG_GLOBAL_OK,
                NULL,
                NULL,
                (PU8) GetString(STR_ID_FM_BT_TEST),
                NULL);

            //FM_Autotest_set_key_handlers();
//            kal_prompt_trace(MOD_WAP,"FactoryModeSrc.c Line: %d",__LINE__);
            SetLeftSoftkeyFunction(EnteyFMBTTest, KEY_EVENT_DOWN);
        }
        else
        {
//            kal_prompt_trace(MOD_WAP,"FactoryModeSrc.c Line: %d",__LINE__);
            EnteyFMBTTest();
        }
    }
    else if(fm_get_bt_flag() == 0xfe)  /*测试完成*/
    {   
        if(g_FM_AutoTest_Mode == FM_AUTOTEST_MODE_664)
        {
            FM_Autotest_test_done_USC2string(Tests[FM_TEST_BT].name);
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_RESULT,
                NULL,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                STR_ID_FM_AUTO_TEST_FAIL,
                IMG_GLOBAL_BACK,
                (PU8) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,
                NULL);
        }
        fm_set_bt_flag(0xff);
        if(g_FM_AutoTest_Mode == FM_AUTOTEST_MODE_664)
        {
            FM_Autotest_set_key_handlers(1);
        }

        gInBTTestMode = FALSE;
        if(gbBTisPwredByFMtest && (srv_bt_cm_is_activated() == MMI_TRUE))
        {
            gbBTisPwredByFMtest = FALSE;
            mmi_bt_reset_power_on_nvram_flag();
        }

        if(g_FM_AutoTest_Mode != FM_AUTOTEST_MODE_664)	
        {
//            kal_prompt_trace(MOD_WAP,"FactoryModeSrc.c Line: %d",__LINE__);
            FM_Handle_Pass_Key_Press();
        }
	    mmi_bt_power_off_bt(NULL);
    }
    else    /*测试失败，搜索不成功*/
    {    
        ShowCategory7Screen(
            STR_ID_FM_AUTO_TEST_RESULT,
            NULL,
            NULL,
            NULL,
            STR_ID_FM_AUTO_TEST_FAIL,
            IMG_GLOBAL_BACK,
            (PU8) GetString(STR_ID_FM_BT_TEST),
            NULL);
        SetRightSoftkeyFunction(FM_Handle_Fail_Key_Press, KEY_EVENT_UP);
        #if defined(__MMI_FACTORYMODE_SEND_OPTIMIZATION__)
        if(g_FM_AutoTest_Mode != FM_AUTOTEST_MODE_664)
        {
            SetKeyHandler(FM_Handle_Fail_Key_Press, KEY_SEND, KEY_EVENT_UP);//注册 SEND 键 工装优化
        }
        #endif
        gInBTTestMode = FALSE;
        if(gbBTisPwredByFMtest && (srv_bt_cm_is_activated() == MMI_TRUE))
        {
            gbBTisPwredByFMtest = FALSE;
            mmi_bt_reset_power_on_nvram_flag();
        }
//        kal_prompt_trace(MOD_WAP,"FactoryModeSrc.c Line: %d",__LINE__); 
	    mmi_bt_power_off_bt(NULL);
    }
}

void FM_Test_BT_Test(void)
{

    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_BT_TEST);
    mmi_frm_scrn_enter(
        g_fm_gourp_id, 
        SCR_ID_FM_BT_TEST,//SCR_ID_BT_TEST, 
        NULL, 
        FM_Test_BT_Test, 
        MMI_FRM_FULL_SCRN);
        
    if(fm_get_bt_flag() == 0xff)
    {
            EnteyFMBTTest();
    }
    else if(fm_get_bt_flag() == 0xfe)  /*测试完成*/
    {
        ShowCategory7Screen(
        STR_ID_FM_BT_TEST,
        NULL,
        STR_GLOBAL_OK,
        IMG_GLOBAL_OK,
        STR_GLOBAL_BACK,
        IMG_GLOBAL_BACK,
        (PU8) GetString(STR_ID_FM_AUTO_TEST_PASS),
        NULL);

        fm_set_bt_flag(0xff);

        gInBTTestMode = FALSE;
        if(gbBTisPwredByFMtest && (srv_bt_cm_is_activated() == MMI_TRUE))
        {
            gbBTisPwredByFMtest = FALSE;
            mmi_bt_reset_power_on_nvram_flag();
        }

        mmi_bt_power_off_bt(NULL);
        SetLeftSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
    }
    else    /*测试失败，搜索不成功*/
    {    
        ShowCategory7Screen(
            STR_ID_FM_BT_TEST,
            NULL,
            STR_GLOBAL_OK,
            IMG_GLOBAL_OK,
            STR_GLOBAL_BACK,
            IMG_GLOBAL_BACK,
            (PU8) GetString(STR_ID_FM_AUTO_TEST_FAIL),
            NULL);
        gInBTTestMode = FALSE;
        if(gbBTisPwredByFMtest && (srv_bt_cm_is_activated() == MMI_TRUE))
        {
            gbBTisPwredByFMtest = FALSE;
            mmi_bt_reset_power_on_nvram_flag();
        }
        mmi_bt_power_off_bt(NULL);
        fm_set_bt_flag(0xff);
        SetLeftSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
	SetRightSoftkeyFunction(GoBackHistory, KEY_EVENT_UP);
    }
}
#endif /*__SUPPORT_BLUETOOTH__*/
#ifdef __SUPPORT_FM_RADIO__
#include "EngineerModeMultimedia.h"


/*****************************************************************************
 * FUNCTION
 *  FM_AutoTest_FM_Test
 * DESCRIPTION
 *  This function is for FM Test mode
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void FM_AutoTest_FM_Test(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    U8* guiBuffer;
    S8 stringbuf[110];
    S8 tmpStr[12];
    S8 tmpStr_UCS2[24];
    em_dev_fm_radio_struct fm_radio_cntx = {0};
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    g_fm_contxt->common_contxt.currentTest = FM_TEST_FMRADIO;	
 #ifdef __MMI_CLAMSHELL__
 	srv_prof_stop_tone(COVER_OPEN_TONE);
 	srv_prof_stop_tone(COVER_CLOSE_TONE);
 #endif
 
    FMR_SetOutputVolume(250,0);//modify by cather for bugid 24459 20100114
    switch(g_fm_contxt->common_contxt.sequence_counter)
    {
        case 0:
            g_fm_contxt->common_contxt.sequence_counter++;

            /* Entry New Screen & Show Category */	
            //EntryNewScreen(GLOBAL_SCR_DUMMY, NULL, NULL, NULL);
            mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

            mmi_frm_scrn_enter
                (g_fm_gourp_id, 
                SCR_ID_FM_AUTO_TEST_START, 
                NULL, 
                FM_AutoTest_FM_Test, 
                MMI_FRM_FULL_SCRN);
            //guiBuffer = GetCurrGuiBuffer (SCR_ID_FM_FM_TEST);
            
            /* set audio volume */
            mdi_audio_set_volume( AUD_VOLUME_FMR,  6/*fm_radio_cntx.RSSIInfoCurrVolume*/);
		    /* Dewav  moon 20100513 修改工程模式FM 测试声音过小*/

            /* power on FM Radio , and callback can be null if nothing to be handled when interrupted */
            mdi_fmr_power_on_with_path( MDI_DEVICE_SPEAKER2, (mdi_callback) NULL,NULL);  

            /* set default volume */
			//fm_radio_cntx.RSSIInfoCurrVolume = 1;  /* 0-6 */  /* Dewav  Derek.yu  2009.12.3  修改工程模式FM 测试声音过大*/

            /* set default FM frequency */
            fm_radio_cntx.rssi_info_freq = 1075;

            /* set FM Radio Frequency */
            mdi_fmr_set_freq( fm_radio_cntx.rssi_info_freq);
            /* initialize display string */
            memset(stringbuf, 0, 100);
            /* frequency */
            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "Fre:");
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);

            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "  %d.%d \n\n",  fm_radio_cntx.rssi_info_freq/10, fm_radio_cntx.rssi_info_freq%10);
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);

            /* volume */
            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "Vol:");
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);

            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "    %d\n\n", fm_radio_cntx.RSSIInfoCurrVolume);
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);

            /* RSSI_info */
            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "RSSI:");
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);

            memset(tmpStr, 0, 10);
            sprintf(tmpStr, "  %d", fm_radio_cntx.RSSIInfoCurrSignallevel);
            mmi_asc_to_ucs2(tmpStr_UCS2, tmpStr);
            mmi_ucs2cat(stringbuf, tmpStr_UCS2);
            
            ClearKeyHandler(KEY_END, KEY_EVENT_DOWN);

            ShowCategory7Screen(
                STR_ID_FM_FM_RADIO,
                0,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                NULL,
                IMG_GLOBAL_BACK,
                (PU8)stringbuf,
                NULL);

            //SetKeyHandler(FM_AutoTest_FM_Test, KEY_LSK, KEY_EVENT_UP);
            SetLeftSoftkeyFunction(FM_AutoTest_FM_Test, KEY_EVENT_UP);
            #if defined(__MMI_FACTORYMODE_SEND_OPTIMIZATION__)
            if (g_FM_AutoTest_Mode != FM_AUTOTEST_MODE_664)
            {
                SetKeyHandler(FM_AutoTest_FM_Test, KEY_SEND, KEY_EVENT_UP);
            }
            #endif
            //FM_Autotest_set_key_handlers();
            break;
        default :
            g_fm_contxt->common_contxt.sequence_counter = 0;
            StopTimer(EM_RINGTONE_HIGHLIGHT_TIMER);
            mdi_fmr_power_off();

            //EntryNewScreen(GLOBAL_SCR_DUMMY, NULL, NULL, NULL);
            mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

            mmi_frm_scrn_enter
            (g_fm_gourp_id, 
            SCR_ID_FM_AUTO_TEST_START, 
            NULL, 
            FM_AutoTest_FM_Test, 
            MMI_FRM_FULL_SCRN);
            if(srv_earphone_is_plug_in() == TRUE)
            {
                ShowCategory7Screen(
                    STR_ID_FM_AUTO_TEST_ROOT,
                    NULL,
                    0,
                    IMG_GLOBAL_OK,
                    STR_ID_FM_AUTO_TEST_FAIL,
                    IMG_GLOBAL_BACK,
                    (U8*) GetString(STR_ID_FM_PLEASE_PLUG_OUT_EARPHONE),
                    NULL);
                
                if(g_FM_AutoTest_Mode == FM_AUTOTEST_MODE_664)
                {
                    FM_Autotest_set_key_handlers(1);
                }
                else
                {
                    g_fm_contxt->common_contxt.sequence_counter++;
                    SetLeftSoftkeyFunction(FM_AutoTest_FM_Test, KEY_EVENT_UP);
                    SetRightSoftkeyFunction(FM_Handle_Fail_Key_Press, KEY_EVENT_UP);
                    #if defined(__MMI_FACTORYMODE_SEND_OPTIMIZATION__)
                    SetKeyHandler(FM_AutoTest_FM_Test, KEY_SEND, KEY_EVENT_UP);//注册 SEND 键 工装优化
                    #endif
                }
            }
            else
            {
                FM_Autotest_test_done_USC2string(Tests[FM_TEST_FMRADIO].name);  
                ShowCategory7Screen(
                        STR_ID_FM_AUTO_TEST_RESULT,
                        NULL,
                        STR_ID_FM_AUTO_TEST_PASS,     
                        IMG_GLOBAL_OK,
                        STR_ID_FM_AUTO_TEST_FAIL,
                        IMG_GLOBAL_BACK,
                        (PU8) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,
                        NULL);
                FM_Autotest_set_key_handlers(1);        
            }
        break;
    }
}

#endif /* #ifdef __SUPPORT_FM_RADIO__ */
#ifdef __SUPPORT_FLASHLIGHT__

/*****************************************************************************
* FUNCTION
*  FM_AutoTest_3DScreen
* DESCRIPTION
*  工程模式手电筒测试
* PARAMETERS
*  void
* RETURNS
*  void
*****************************************************************************/
void FM_AutoTest_FlashLight(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    g_fm_contxt->common_contxt.currentTest = FM_TEST_FLASHLIGHT;

    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);

    mmi_frm_scrn_enter
    (g_fm_gourp_id, 
    SCR_ID_FM_AUTO_TEST_START, 
    NULL, 
    FM_AutoTest_FlashLight, 
    MMI_FRM_FULL_SCRN);
    
    switch (g_fm_contxt->common_contxt.sequence_counter)
    {
        case 0:
            g_fm_contxt->common_contxt.sequence_counter ++;
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_ROOT,
                NULL,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                STR_ID_FM_AUTO_TEST_FAIL,
                IMG_GLOBAL_BACK,
                (U8*)GetString(STR_FLASH_LIGHT_TEST),
                NULL);
            
        #if defined(__DRV_FLASHLIGHT_SUPPORT__)
            drv_flashlight_switch(MMI_TRUE);
        #endif /*defined(__DRV_FLASHLIGHT_SUPPORT__)*/
            if(g_FM_AutoTest_Mode != FM_AUTOTEST_MODE_664)
            {	
                StartTimer(FM_AUTO_TEST_COMMNON_TIMER, LED_DURATION, FM_AutoTest_FlashLight);	
            }
            SetLeftSoftkeyFunction(FM_AutoTest_FlashLight, KEY_EVENT_UP);
	        SetRightSoftkeyFunction(FM_AutoTest_FlashLight, KEY_EVENT_UP);
            break;
        default:
            g_fm_contxt->common_contxt.sequence_counter = 0;
    	    StopTimer(FM_AUTO_TEST_COMMNON_TIMER);
            FM_Autotest_test_done_USC2string(Tests[FM_TEST_FLASHLIGHT].name);
            ShowCategory7Screen(
                STR_ID_FM_AUTO_TEST_RESULT,
                NULL,
                STR_ID_FM_AUTO_TEST_PASS,
                IMG_GLOBAL_OK,
                STR_ID_FM_AUTO_TEST_FAIL,
                IMG_GLOBAL_BACK,
                (PU8) g_fm_contxt->common_contxt.EMFMUnicodeDisplayBuf,
                NULL);

        #if defined(__DRV_FLASHLIGHT_SUPPORT__)
            drv_flashlight_switch(MMI_FALSE);
        #endif /*defined(__DRV_FLASHLIGHT_SUPPORT__)*/
            FM_Autotest_set_key_handlers(1);
            break;
    }
}
#endif /*__SUPPORT_FLASHLIGHT__*/
#ifdef __SUPPORT_TP_TEST__

void mmi_AutoTest_TouchPanel(void) {
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    mmi_fm_common_struct *common_cntx;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    common_cntx = &(g_fm_contxt->common_contxt);

    if (mmi_frm_scrn_enter(
            g_fm_gourp_id, 
            SCR_ID_FM_AUTO_TEST_START, 
            NULL, 
            mmi_AutoTest_TouchPanel, 
            MMI_FRM_FULL_SCRN) != MMI_TRUE)
    {
        return ;
    }

    switch (common_cntx->sequence_counter)
    {
    case 0:
        common_cntx->sequence_counter++;
        EntryPhnseFactoryDefaultCalibrationScr(mmi_AutoTest_TouchPanel);
        break;
    default:
        FM_Autotest_test_done_USC2string(Tests[FM_TEST_CALIBRATION].name);

        mmi_fm_auto_test_common(
            SENDKEY_UNCNT_STOP_SHOW,
            (PU8) common_cntx->EMFMUnicodeDisplayBuf,
            0,
            mmi_AutoTest_TouchPanel);
        break;
    }
}

void FM_AutoTest_Calibration_Test(void) {
    g_fm_contxt->common_contxt.currentTest = FM_TEST_CALIBRATION;
    mmi_frm_scrn_close(g_fm_gourp_id, SCR_ID_FM_AUTO_TEST_START);
    mmi_AutoTest_TouchPanel();
}

#endif /*__SUPPORT_TP_TEST__*/
/*****************************************************************************
 * FUNCTION
 *	EntryFMMenuAutoTestProcess
 * DESCRIPTION
 *	
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMMenuAutoTestProcess(void)
{
    /*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
    InitFactoryModeEvent();
    
	/* Disable tone internal */
	gui_touch_feedback_disable_tone_internal();
	gui_touch_feedback_disable_vibrate_internal();
	
	/* create group and init ASM  */	
	g_fm_contxt = applib_mem_ap_alloc(APPLIB_MEM_AP_ID_FM_COMM_MEM,sizeof(mmi_fm_item_cntxt_struct));
	
	if (g_fm_contxt == NULL)
	{
		DisplayPopup((PU8) GetString(STR_GLOBAL_INSUFFICIENT_MEMORY), NULL, 0, 1000, 0);
        MMI_FM_DCM_POST_UNLOAD();
		return; 		   
	}
	
	g_fm_gourp_id = mmi_frm_group_create(GRP_ID_ROOT,
					   GRP_ID_AUTO_GEN, mmi_fm_auto_test_group_proc, NULL);
	mmi_frm_group_enter(g_fm_gourp_id, MMI_FRM_NODE_NONE_FLAG);
	
	if (g_fm_contxt != NULL)
	{
	   memset(g_fm_contxt, 0, sizeof(mmi_fm_item_cntxt_struct));
	   mmi_fm_init_mem_contxt();
	}

	FM_InitAutoTest();
	/* Enter auto test */
	mmi_fm_enter_auto_test();
}


#if (defined(__MMI_AP_DCM_FM__) || defined(__DCM_WITH_COMPRESSION_MMI_POOL_A__))
#pragma arm section rodata , code
#endif /* (defined(__MMI_AP_DCM_FM__) || defined(__DCM_WITH_COMPRESSION_MMI_POOL_A__)) */


/*****************************************************************************
 * FUNCTION
 *	EntryFMMenuAutoTest
 * DESCRIPTION
 *	extern API. *#87#
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
extern void EntryFMMenuAutoTest(void)
{
    /*----------------------------------------------------------------*/
	/* Local Variables												  */
	/*----------------------------------------------------------------*/

	/*----------------------------------------------------------------*/
	/* Code Body													  */
	/*----------------------------------------------------------------*/
    
    MMI_FM_DCM_LOAD();
    
    EntryFMMenuAutoTestProcess();
}


#ifdef __PY_FACTORY_MODE__
void FactoryModeSendMsg(U16 msg_id, void *local_param_ptr, void *peer_buf_ptr)
{
#if defined(__MTK_TARGET__)
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    ilm_struct *ilm_ptr = NULL;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    ilm_ptr = allocate_ilm(MOD_MMI);
    ilm_ptr->src_mod_id = MOD_MMI;
    ilm_ptr->sap_id = DRIVER_PS_SAP;
    ilm_ptr->dest_mod_id = MOD_AUX;

    ilm_ptr->msg_id = (kal_uint16) msg_id;
    ilm_ptr->local_para_ptr = (local_para_struct*) local_param_ptr;
    ilm_ptr->peer_buff_ptr = (peer_buff_struct*) peer_buf_ptr;

    msg_send_ext_queue(ilm_ptr);
#endif
}

U8 FinalTestOK(void)
{
	U8 result = FINAL_TEST_SUCCESS;
#if 1
	S16 Ret,ErrorCode;
	U8 BarCode[MAX_SUB_MENU_SIZE];

	Ret = ReadRecord(NVRAM_EF_BARCODE_NUM_LID, 1, BarCode, 64, &ErrorCode);

	if (!(ErrorCode == NVRAM_READ_SUCCESS && Ret == NVRAM_EF_BARCODE_NUM_SIZE))
	{
		result = FINAL_TEST_INFO_ERR;
		return result;
	}
	if(BarCode[62] == 'P')
	{
		result = FINAL_TEST_SUCCESS;
	}
	else if (BarCode[62] == 'F')
	{
		result = FINAL_TEST_FAIL;
	}
	else
	{
		result = FINAL_TEST_NOT_DONE;
	}
#endif
	return result;
}

/*****************************************************************************
 * FUNCTION
 *  EntryFMMenuAutoTest_664
 * DESCRIPTION
 *  extern API. *79*664#
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void EntryFMMenuAutoTest_664(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    g_FM_AutoTest_Mode = FM_AUTOTEST_MODE_664;
    EntryFMMenuAutoTest();
}

/*****************************************************************************
 * FUNCTION
 *	EntryFMMenuAutoTest_9
 * DESCRIPTION
 *	extern API. *79*9#
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMMenuAutoTest_8(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(FinalTestOK() != FINAL_TEST_SUCCESS)
    {
        DisplayPopup(
            (kal_uint8*)L"Final test first please!",
            IMG_GLOBAL_WARNING,
            0,
            UI_POPUP_NOTIFYDURATION_TIME_2000,
            (U8) ERROR_TONE);
        return;
    }

    g_FM_AutoTest_Mode = FM_AUTOTEST_MODE_8;
    EntryFMMenuAutoTest();
}


/*****************************************************************************
 * FUNCTION
 *	EntryFMMenuAutoTest_9
 * DESCRIPTION
 *	extern API. *79*9#
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMMenuAutoTest_9(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if(FinalTestOK() != FINAL_TEST_SUCCESS)
    {
        DisplayPopup(
            (kal_uint8*)L"Final test first please!",
            IMG_GLOBAL_WARNING,
            0,
            UI_POPUP_NOTIFYDURATION_TIME_2000,
            (U8) ERROR_TONE);
        return;
    }

    g_FM_AutoTest_Mode = FM_AUTOTEST_MODE_9;
    EntryFMMenuAutoTest();
}

MMI_BOOL FM_AutoTest_Write_Barcode(S32 nIndex, U8 nParam)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    U8 l_aBarCode[MAX_SUB_MENU_SIZE];
    S16 l_nErr;
    MMI_BOOL l_bRet = MMI_FALSE;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    do
    {
        S32 nRet;
        if (nIndex < 0 || nIndex > 63)
        {
            break ;
        }

        nRet = ReadRecord(NVRAM_EF_BARCODE_NUM_LID, 1, l_aBarCode, NVRAM_EF_BARCODE_NUM_SIZE, &l_nErr);
        if (l_nErr != NVRAM_READ_SUCCESS || nRet != NVRAM_EF_BARCODE_NUM_SIZE)
        {
            break ;
        }

        l_aBarCode[nIndex] = nParam;
        
        nRet = WriteRecord(NVRAM_EF_BARCODE_NUM_LID, 1, l_aBarCode, NVRAM_EF_BARCODE_NUM_SIZE, &l_nErr);
        if (l_nErr != NVRAM_WRITE_SUCCESS || nRet != NVRAM_EF_BARCODE_NUM_SIZE)
        {
            break ;
        }

        l_bRet = MMI_TRUE;
    } while (0);

    return l_bRet;
}

void FM_AutoTest_Write_Result(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    U8 i;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    for (i = 0; i < g_fm_AutoTestListSize; i++)
    {
        if(g_fm_nvramTestResultArray.result[i] != FM_TEST_PASSED)
        {
            FM_AutoTest_Write_Barcode(50, 'F');
            return;
        }
    }

    FM_AutoTest_Write_Barcode(50, 'P');
}

#endif /*__PY_FACTORY_MODE__*/
#if defined(__MMI_C68_KLT_COMMON__)
/*****************************************************************************
 * FUNCTION
 *	EntryFMMenuAutoTest_9
 * DESCRIPTION
 *	extern API. *79*9#
 * PARAMETERS
 *	void
 * RETURNS
 *	void
 *****************************************************************************/
void EntryFMMenuAutoTest_99(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    g_FM_AutoTest_Mode = FM_AUTOTEST_MODE_99;
    mmi_frm_scrn_close(GRP_ID_ROOT, SCR_ID_KLT_TEST);
    mmi_frm_scrn_enter
	    (GRP_ID_ROOT, 
	    SCR_ID_KLT_TEST, 
	    NULL, 
	    EntryFMMenuAutoTest_99, 
	    MMI_FRM_FULL_SCRN);

        ShowCategory7Screen(
            STR_ID_FACTORY_TEST_MODE,
            NULL,
            STR_GLOBAL_AUTOMATIC,
            IMG_GLOBAL_OK,
            STR_GLOBAL_MANUAL,
            IMG_GLOBAL_BACK,
            NULL,
            NULL);
    	
        SetLeftSoftkeyFunction(EntryFMMenuAutoTest_664, KEY_EVENT_UP);
        SetRightSoftkeyFunction(EntryFMMenu, KEY_EVENT_UP);

}
#endif

#endif
