/*****************************************************************************
*  Copyright Statement: 
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2012
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 * EngineerModeIMEI.c
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *   This file is intends for engineer mode IMEI part for MT6260.
 *
 * Author:
 * -------
 * -------
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/
	 
#include "mmi_features.h"
#if defined(__MMI_ENGINEER_MODE__)

#if defined(__MMI_EM_WRITE_IMEI__)
#include "MMIDataType.h"
#include "InlineCuiGprot.h" // for cui_inline_item_caption_struct
#include "mmi_rp_app_engineermode1_def.h"  
#include "mmi_rp_app_phonesetting_new_def.h"
#include "GlobalResDef.h"
#include "ImeiSrvGprot.h"
#include "nvram_data_items.h"
#include "app_addr.h"

//#define __MMI_EM_DEBUG__
#define MMI_EM_DEV_IMEI_ITEM_COUNT      4

#define MMI_EM_CAPTION_IMEI1    	  	(CUI_INLINE_ITEM_ID_BASE+0)
#define MMI_EM_CAPTION_IMEI2     	    (CUI_INLINE_ITEM_ID_BASE+1)
#define MMI_EM_TEXT_EDIT_IMEI1   	    (CUI_INLINE_ITEM_ID_BASE+2)
#define MMI_EM_TEXT_EDIT_IMEI2   	    (CUI_INLINE_ITEM_ID_BASE+3)

#define MMI_EM_IMEI_SHUTDOWN_TIMEOUT    (2*1000)

typedef struct
{
    WCHAR imei1_buf[SRV_IMEI_MAX_LEN+1];
    WCHAR imei2_buf[SRV_IMEI_MAX_LEN+1];
} mmi_em_dev_imei_cntx_struct;

static MMI_BOOL mmi_em_wirte_imei(mmi_sim_enum sim, const char* imei);
static MMI_RET mmi_em_dev_imei_proc(mmi_event_struct* evt);

static const cui_inline_item_caption_struct g_mmi_em_imei1_cap = 
{
    STR_ID_EM_IMEI1,
};

static const cui_inline_item_caption_struct g_mmi_em_imei2_cap = 
{
    STR_ID_EM_IMEI2,
};

static const cui_inline_item_text_edit_struct g_mmi_em_mm_imei_edit =
{
    0, 0, SRV_IMEI_MAX_LEN+1, IMM_INPUT_TYPE_NUMERIC, 0, NULL
};

static const cui_inline_set_item_struct g_mmi_em_imei_inline[] = 
{
    {MMI_EM_CAPTION_IMEI1,      CUI_INLINE_ITEM_TYPE_CAPTION,    0,  (void*)&g_mmi_em_imei1_cap},
    {MMI_EM_TEXT_EDIT_IMEI1,    CUI_INLINE_ITEM_TYPE_TEXT_EDIT,  0,  (void*)&g_mmi_em_mm_imei_edit},
    {MMI_EM_CAPTION_IMEI2,      CUI_INLINE_ITEM_TYPE_CAPTION,    0,  (void*)&g_mmi_em_imei2_cap},
    {MMI_EM_TEXT_EDIT_IMEI2,    CUI_INLINE_ITEM_TYPE_TEXT_EDIT,  0,  (void*)&g_mmi_em_mm_imei_edit},
};

static const cui_inline_struct g_mmi_em_imei_inline_scrn = 
{
    MMI_EM_DEV_IMEI_ITEM_COUNT,
    STR_ID_EM_IMEI_SETTING,
    0,
    CUI_INLINE_SCREEN_LOOP|CUI_INLINE_SCREEN_DISABLE_DONE,
    NULL,
    (const cui_inline_set_item_struct*)&g_mmi_em_imei_inline
};

static mmi_em_dev_imei_cntx_struct mmi_em_dev_imei_cntx;

static void mmi_em_imei_reset(void)
{
    srv_alm_pwr_reset(MMI_TRUE, 3);
}

void mmi_em_dev_enter_imei(void)
{
    MMI_ID gid;
    MMI_ID inline_id;
	U8 imei[SRV_IMEI_MAX_LEN+1];
	MMI_BOOL ret;

    memset(&mmi_em_dev_imei_cntx, 0, sizeof(mmi_em_dev_imei_cntx));
    gid = mmi_frm_group_create_ex(GRP_ID_ROOT,
                  GRP_ID_EM_ROOT, 
                  mmi_em_dev_imei_proc, NULL,
                  MMI_FRM_NODE_SMART_CLOSE_FLAG);

    inline_id = cui_inline_create(gid, &g_mmi_em_imei_inline_scrn);
    kal_wsprintf(mmi_em_dev_imei_cntx.imei1_buf, "%d", 0);
    kal_wsprintf(mmi_em_dev_imei_cntx.imei2_buf, "%d", 0);
    memset(imei, 0, sizeof(imei));
    if(srv_imei_get_imei(MMI_SIM1, imei, sizeof(imei)))
    {
        kal_wsprintf(mmi_em_dev_imei_cntx.imei1_buf, "%s", imei);
    }

    memset(imei, 0, sizeof(imei));
    if(srv_imei_get_imei(MMI_SIM2, imei, sizeof(imei)))
    {
        kal_wsprintf(mmi_em_dev_imei_cntx.imei2_buf, "%s", imei);
    }
    
    cui_inline_set_value(inline_id, MMI_EM_TEXT_EDIT_IMEI1, (void *) mmi_em_dev_imei_cntx.imei1_buf);
    cui_inline_set_value(inline_id, MMI_EM_TEXT_EDIT_IMEI2, (void *) mmi_em_dev_imei_cntx.imei2_buf);
    
    cui_inline_run(inline_id);
}

static MMI_RET mmi_em_dev_imei_proc(mmi_event_struct* evt)
{
    cui_event_inline_common_struct* inline_evt = (cui_event_inline_common_struct*) evt;
	MMI_ID parent_id;
	U32 value;
	char imei_asc[SRV_IMEI_MAX_LEN+1];
    MMI_BOOL ret1,ret2;
    
    parent_id = inline_evt->sender_id;

    switch (inline_evt->evt_id)
    {
    case EVT_ID_CUI_INLINE_SUBMIT:
    case EVT_ID_CUI_INLINE_CSK_PRESS:			
	    cui_inline_get_value(parent_id, MMI_EM_TEXT_EDIT_IMEI1, (void *)mmi_em_dev_imei_cntx.imei1_buf);
	    cui_inline_get_value(parent_id, MMI_EM_TEXT_EDIT_IMEI2, (void *)mmi_em_dev_imei_cntx.imei2_buf);

        memset(imei_asc, 0, sizeof(imei_asc));
        mmi_wcs_to_asc((CHAR*)imei_asc, (WCHAR*)mmi_em_dev_imei_cntx.imei1_buf);
        ret1 = mmi_em_wirte_imei(MMI_SIM1, imei_asc);
        memset(imei_asc, 0, sizeof(imei_asc));
        mmi_wcs_to_asc((CHAR*)imei_asc, (WCHAR*)mmi_em_dev_imei_cntx.imei2_buf);
		ret2 = mmi_em_wirte_imei(MMI_SIM2, imei_asc);

        cui_inline_close(parent_id);
	    mmi_frm_group_close(GRP_ID_EM_ROOT);

        if(ret1 || ret2)
        {
            DisplayPopup((PU8) GetString(STR_ID_PHNSET_UART_SETUP_REBOOT_PROCESSING), IMG_GLOBAL_OK, 0, 1000, 0);
            StartTimer(MMI_EM_IMEI_TIMER, MMI_EM_IMEI_SHUTDOWN_TIMEOUT, mmi_em_imei_reset);
        }
        
        break;

    case EVT_ID_CUI_INLINE_ABORT:
        cui_inline_close(parent_id);
        break;
    default:
        break;
    }

    return MMI_RET_OK;
}

static MMI_BOOL mmi_em_wirte_imei(mmi_sim_enum sim, const char* imei)
{
    S16 error;
    S32 ret;
    kal_uint8 imei_bcd[NVRAM_EF_IMEI_IMEISV_SIZE] = {0};
    U8 old_imei[SRV_IMEI_MAX_LEN+1];
    
    if (imei == NULL)
    {
    #ifdef __MMI_EM_DEBUG__
        kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei sim:[%d] imei is NULL", sim);
    #endif
        return MMI_FALSE;
    }
    
    if(sim <= MMI_SIM_NONE || sim > NVRAM_EF_IMEI_IMEISV_TOTAL)
    {
    #ifdef __MMI_EM_DEBUG__
        kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei ERROR sim:[%d]", sim);
    #endif
        return MMI_FALSE;
    }

    if(strlen(imei) != SRV_IMEI_MAX_LEN)
    {
    #ifdef __MMI_EM_DEBUG__
        kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei ERROR imei:[%s] length[%d]", imei, strlen(imei));
    #endif
        return MMI_FALSE;
    }

    memset(old_imei, 0, sizeof(old_imei));
    if(srv_imei_get_imei(sim, old_imei, sizeof(old_imei)))
    {
        if(strcmp(old_imei, imei) == 0)
        {
            return MMI_FALSE;
        }
    }

#ifdef __MMI_EM_DEBUG__    
    kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei sim:[%d] imei[%s]", sim, imei);
#endif

    applib_addr_string_2_bcd((char*)imei, strlen(imei), &imei_bcd[0], NVRAM_EF_IMEI_IMEISV_SIZE);

#ifdef __MMI_EM_DEBUG__    
    kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei sim:[%d] imei bcd:[%x,%x,%x,%x,%x,%x,%x,%x]", 
        sim, imei_bcd[0], imei_bcd[1], imei_bcd[2], imei_bcd[3], imei_bcd[4], imei_bcd[5], imei_bcd[6], imei_bcd[7]);
#endif

    ret = WriteRecord(NVRAM_EF_IMEI_IMEISV_LID, sim, (void*)&imei_bcd[0], NVRAM_EF_IMEI_IMEISV_SIZE, &error);

#ifdef __MMI_EM_DEBUG__
    kal_prompt_trace(MOD_MMI, "mmi_em_wirte_imei sim:[%d] ret[%d], error[%d]", sim, ret, error);
#endif

    return ret != -1 ? MMI_TRUE : MMI_FALSE;

}

#endif
#endif //__MMI_ENGINEER_MODE__

