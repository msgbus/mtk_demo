/*=========================================================================
×  版权声明：
*  --------------------
* 版权所有广东国笔科技股份有限公司，未经授权禁止个人和任何组织拷贝和使用，违反必究。
*
*
*
*
*  ---------国笔科技股份有限公司-----------
*
*文件说明: gb_HWInterface.c
*国笔手写输入法与MTK平台对接的MMI代码
*
*
*
*版本: V20080822  by Guobi Jerry zhu++
*更新的版本后面会更新版本号。
*
*
*
*
*
*
*==========================================================================*/

#include "MMI_features.h"


//OPTION: 混合输入支持
#define __MMI_GUOBI_MIX_RECOGNIZE__    


#if defined(__MMI_GUOBI_HANDWRITING__) && defined(__MMI_TOUCH_SCREEN__)
#include "gui_data_types.h"


/*include guobi head files*/
#include "GBHW.h"
#include "ImeGprot.h"
#include "Handwriting_engine.h"
/*****************************************************************************
global data 

*****************************************************************************/
extern const unsigned long prv_HWDataArray[] ;
unsigned long *zmaee_prv_HWDataArray= NULL ;

static int   g_isGBHWinputmethodINIT =0; 

#ifdef __MMI_GUOBI_MIX_RECOGNIZE__    
#define GBHW_OPT_MIX_RANGE   (GBHW_OPT_RANGE_GB2312 | GBHW_OPT_RANGE_ASCII)
#endif 

extern const void* zmaee_gbhw_data_create(void);

/*****************************************************************************
function  define  

*****************************************************************************/

/*==================================================
*  void mmi_ime_hand_writing_initialize(mmi_imc_pen_handwriting_type_enum hw_type)
*
* 函数说明:    初始化国笔手写引擎

* 调用说明:   一般建议开机时候初始化，
增加g_isGBHWinputmethodINIT 保证系统只初始化一次，避免重复INIT
* 
* 
* 
*==================================================*/
void mmi_ime_hand_writing_initialize(mmi_imc_pen_handwriting_type_enum hw_type)
{
}

/*==================================================
*   
*void mmi_ime_hand_writing_deinitialize(void)

* 函数说明:   

* 调用说明: 
* 
* 
* 
*==================================================*/
void mmi_ime_hand_writing_deinitialize(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/

}



/*==================================================
*  S32 mmi_ime_hand_writing_get_candidates(
        mmi_ime_hand_writing_query_param_p query_param,
        mmi_ime_hand_writing_result_param_p result_param)
 
*
* 函数说明:     调用引擎得到书写候选

* 调用说明:   在imc_pen.c中被调用
* 
*    【IN   】 mmi_ime_hand_writing_query_param_p query_param
      【OUT】  mmi_ime_hand_writing_result_param_p result_param
* 
*==================================================*/

/*****************************************************************************
 * FUNCTION
 *  mmi_ime_hand_writing_hw_type_query
 * DESCRIPTION
 * PARAMETERS
 * RETURNS
 *  The query if the h_w_type is supported by the vendor
 *****************************************************************************/
MMI_BOOL mmi_ime_hand_writing_hw_type_query(mmi_imc_pen_handwriting_type_enum h_w_type)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    return MMI_TRUE;
}

/*****************************************************************************
* FUNCTION
*  mmi_ime_hand_writing_add_separator_to_candidate_buffer
* DESCRIPTION
*  add seperate for candidates list
* PARAMETERS
* RETURNS
*  void
*****************************************************************************/
void  mmi_ime_hand_writing_add_separator_to_candidate_buffer(UI_character_type * destination_buffer, UI_character_type * source_buffer, S32 num)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    S32 i = 0, j = 0;
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    while (i < num && source_buffer[i] != 0)
    {
        destination_buffer[j] = source_buffer[i];
        destination_buffer[j + 1] = 0; /* Default separator is '\0' */
        i++;
        j += 2;
    }

    destination_buffer[j] = 0;
}

#if defined(__MMI_HANDWRITING_PHRASE__)
/*****************************************************************************
 * FUNCTION
 *  mmi_ime_hand_writing_get_candidates_by_phrase
 * DESCRIPTION
 * PARAMETERS
 * RETURNS
 *  The query if the h_w_type is supported by the vendor
 *****************************************************************************/
S32 mmi_ime_hand_writing_get_candidates_by_phrase(
    mmi_ime_hand_writing_phrase_query_param_p query, 
    mmi_ime_hand_writing_phrase_result_param_p result)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    result.result_cnt = 0;
    result.next_page = MMI_FALSE;
    return 0;

}
#endif

#endif 
