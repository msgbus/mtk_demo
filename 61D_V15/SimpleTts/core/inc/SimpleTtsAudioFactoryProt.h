#ifndef _SIMPLE_TTS_AUDIO_FACTORY_PROT_H
#define _SIMPLE_TTS_AUDIO_FACTORY_PROT_H

#include "MMIDataType.h"

extern void mmi_simple_tts_get_audio(S8 nChar, S8 **pAudio, S32 *pSize);
extern void mmi_simple_tts_synthesis_audio(const S8* strNum, S8** ppBuf, S32* pSize);

#endif /*_SIMPLE_TTS_AUDIO_FACTORY_PROT_H*/
