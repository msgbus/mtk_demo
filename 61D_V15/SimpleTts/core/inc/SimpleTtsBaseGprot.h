#ifndef _SIMPLE_TTS_BUFFER_PROT_H
#define _SIMPLE_TTS_BUFFER_PROT_H

#include "MMIDataType.h"

extern void *srv_simple_tts_malloc(U32 size);

extern void srv_simple_tts_free(void **ptr);

extern void *srv_simple_tts_get_buffer(void);

extern void srv_simple_tts_free_buffer(void);

extern S32 srv_simple_tts_strlen(const S8 *text);

extern S8* srv_simple_tts_strncpy(S8* strDst ,const  S8* strSrc, U32 nSize);

extern S8* srv_simple_tts_strncat(S8* strDst ,const  S8* strSrc, U32 nSize);

extern void* srv_simple_tts_memset(void* pDst , S32 nSrc, U32 nSize);

extern void *srv_simple_tts_memcpy(void *pDst, const void *pSrc, U32 nSize);

#endif /*_SIMPLE_TTS_BUFFER_PROT_H*/
