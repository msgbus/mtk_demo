#ifndef _SIMPLE_TTS_PLAY_GPROT_H
#define _SIMPLE_TTS_PLAY_GPROT_H

extern void srv_simple_tts_play_char(S8 nChar, mdi_ext_callback cb);

extern void srv_simple_tts_stop_string(void);

extern void srv_simple_tts_play_string(const S8 *string, mdi_ext_callback cb);

extern void srv_simple_tts_set_play_cbfunc(FuncPtr cb);

extern void srv_simple_tts_common_cb(mdi_result result, void* user_data);

#endif /*_SIMPLE_TTS_PLAY_GPROT_H*/
