#ifndef _SIMPLE_TTS_AUDIO_FACTORY_DEF_H__
#define _SIMPLE_TTS_AUDIO_FACTORY_DEF_H__

#include "MMIDataType.h"

#ifdef WIN32
typedef const S8 SimpleTtsAudioType[];
#else
typedef __align(2) const S8 SimpleTtsAudioType[];
#endif

typedef struct
{
    S8 m_nChar;
    S32 m_nSize;
    const S8 *m_pAudio;
} SimpleTtsAudioListType;

#endif /*_SIMPLE_TTS_AUDIO_FACTORY_DEF_H__*/
