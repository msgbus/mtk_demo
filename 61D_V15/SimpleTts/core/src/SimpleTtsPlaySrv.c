#include "MMI_features.h"
#include "MMIDataType.h"
#include "mdi_datatype.h"
#include "mdi_audio.h"
#include "GpioSrvGprot.h"
#include "BTMMIObexGprots.h"

#include "SimpleTtsAudioFactoryProt.h"
#include "SimpleTtsBaseGprot.h"

FuncPtr gSimpleTtsPlayCallbackFunc = NULL;
extern MMI_BOOL mmi_simple_tts_playing_incoming;

static void srv_simple_tts_audio_play(S8* pData, S32 nSize, U8 nFormat, U8 nVol, mdi_ext_callback cb)
{
    U8 l_nPath;

#ifdef MDI_AUDIO_LOCK
    MDI_AUDIO_LOCK;
#endif

    l_nPath = MDI_DEVICE_SPEAKER_BOTH;    

    if(!mmi_simple_tts_playing_incoming && srv_earphone_is_plug_in())
    {
        l_nPath = MDI_DEVICE_SPEAKER2;
    }
#ifdef __MMI_BT_SUPPORT__
    else if(mmi_bt_is_receiving())
    {
        l_nPath = MDI_DEVICE_BT_HEADSET;
    }
#endif

    mdi_audio_play_string_with_vol_path_non_block(
        (void *) pData,
        (U32) nSize,
        (U8) nFormat,
        MDI_AUDIO_PLAY_ONCE,
        cb,
        NULL,
        nVol,
        l_nPath);

#ifdef MDI_AUDIO_UNLOCK
    MDI_AUDIO_UNLOCK;
#endif            
}
#ifdef __MMI_SIMPLE_TTS_WAV__
extern void srv_key_num_play(unsigned short index,unsigned char vol,unsigned char path);//gary test
#endif /*__MMI_SIMPLE_TTS_WAV__*/
void srv_simple_tts_play_char(S8 nChar, mdi_ext_callback cb)
{
    S8 *l_pData = NULL;
    S32 l_nSize = 0;

	//gary test
#ifdef __MMI_SIMPLE_TTS_WAV__
	U8 path;
	if((nChar>='0'&&nChar<='9')||(nChar=='*')||(nChar=='#'))
    {
	 	path = MDI_DEVICE_SPEAKER_BOTH;	

		if(srv_earphone_is_plug_in())
		{
			path = MDI_DEVICE_SPEAKER2;
		}
        #ifdef __MMI_BT_SUPPORT__
			if(mmi_bt_is_receiving())
			{
			path = MDI_DEVICE_BT_HEADSET;
			}
        #endif
 		srv_key_num_play(nChar,LEVEL5,path);
    }
	else
#endif /*__MMI_SIMPLE_TTS_WAV__*/
	//gary test .
	{
	    mmi_simple_tts_get_audio(nChar, &l_pData, &l_nSize);    

	    srv_simple_tts_audio_play(l_pData, l_nSize, MDI_FORMAT_AMR, LEVEL5, cb);
	}
}

void srv_simple_tts_stop_string(void)
{
    mdi_audio_stop_string();
    srv_simple_tts_free_buffer();
}

void srv_simple_tts_play_string(const S8 *string, mdi_ext_callback cb)
{
    S8 *l_pData = NULL;
    S32 l_nSize = 0;

    l_pData = srv_simple_tts_get_buffer();
    if(l_pData == NULL)
    {
        return ;
    }

    mmi_simple_tts_synthesis_audio(string, &l_pData, &l_nSize);    

    srv_simple_tts_audio_play(l_pData, l_nSize, MDI_FORMAT_AMR, LEVEL7, cb);
}

void srv_simple_tts_set_play_cbfunc(FuncPtr cb)
{
    gSimpleTtsPlayCallbackFunc = cb;
}

void srv_simple_tts_common_cb(mdi_result result, void* user_data)
{
    srv_simple_tts_free_buffer();
    if (gSimpleTtsPlayCallbackFunc != NULL)
    {
        (*gSimpleTtsPlayCallbackFunc)();
    }
}
