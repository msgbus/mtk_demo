#include "MMI_features.h"
#include "MMI_include.h"
#include "MMIDataType.h"
#include "med_utility.h"
#include "SimpleTtsBaseGprot.h"

#define MAX_SIMPTTS_BUFF_SIZE   (150 * 1024)

static void *gSimpleTtsBufferPtr = NULL;

void *srv_simple_tts_malloc(U32 size)
{
	return (void *)med_alloc_ext_mem(size);
}

void srv_simple_tts_free(void **ptr)
{
	med_free_ext_mem(ptr);
}

void *srv_simple_tts_get_buffer(void)
{
    if(gSimpleTtsBufferPtr == NULL)
    {
        gSimpleTtsBufferPtr = srv_simple_tts_malloc(MAX_SIMPTTS_BUFF_SIZE);
        if (gSimpleTtsBufferPtr != NULL)
        {
            memset(gSimpleTtsBufferPtr, 0, MAX_SIMPTTS_BUFF_SIZE);
        }
    }

    return gSimpleTtsBufferPtr;
}

void srv_simple_tts_free_buffer(void)
{
    if(gSimpleTtsBufferPtr)
    {
        srv_simple_tts_free((void **)&gSimpleTtsBufferPtr);
    }
}

S32 srv_simple_tts_strlen(const S8 *text)
{
    return strlen(text);
}

S8* srv_simple_tts_strncpy(S8* strDst ,const  S8* strSrc, U32 nSize)
{
	return (S8*)strncpy(strDst, strSrc, nSize); 
}

S8* srv_simple_tts_strncat(S8* strDst ,const  S8* strSrc, U32 nSize)
{
	return (S8*)strncat(strDst, strSrc, nSize); 
}

void* srv_simple_tts_memset(void* pDst , S32 nSrc, U32 nSize)
{
    return memset(pDst, nSrc, nSize);
}

void *srv_simple_tts_memcpy(void *pDst, const void *pSrc, U32 nSize)
{
	return (void*)memcpy(pDst, pSrc, nSize);
}

