#include "SimpleTtsAudioResource.h"
#include "SimpleTtsBaseGprot.h"

void mmi_simple_tts_get_audio(S8 nChar, S8 **pAudio, S32 *pSize)
{
    S32 i = 0;
    S32 total = 0;

    total = sizeof(gAudioMap)/sizeof(gAudioMap[0]);
    for(i = 0; i < total; i++)
    {
        if(gAudioMap[i].m_nChar == nChar)
        {
            *pSize = gAudioMap[i].m_nSize;
            *pAudio = (S8 *) gAudioMap[i].m_pAudio;
            return;
        }
    }
}

void mmi_simple_tts_synthesis_audio(const S8* strNum, S8** ppBuf, S32* pSize)
{
    S32 i = 0, l_nLen = 0, l_nSize = 0;
    S8 *l_pBuf;

    do
    {
        if (strNum == NULL)
        {
            break ;
        }

        l_nLen = srv_simple_tts_strlen(strNum);
        if (l_nLen == 0)
        {
            break ;
        }

        for (i = 0; i < l_nLen; i++)
        {
            mmi_simple_tts_get_audio(strNum[i], &l_pBuf, &l_nSize);
            if (l_nSize == 0)
            {
                continue ;
            }
            if (*pSize == 0)
            {
                srv_simple_tts_memcpy(*ppBuf, l_pBuf, l_nSize);
            }
            else
            {
                l_nSize -= STTS_AUDIO_HEAD_LENGTH;
                srv_simple_tts_memcpy(*ppBuf + *pSize, l_pBuf + STTS_AUDIO_HEAD_LENGTH, l_nSize);
            }
            *pSize += l_nSize;
        }
    } while(0);
}

