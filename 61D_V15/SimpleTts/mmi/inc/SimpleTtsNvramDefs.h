#ifndef _SIMPLE_TTS_NVRAM_DEFS_H
#define _SIMPLE_TTS_NVRAM_DEFS_H

#include "SimpleTtsDefs.h"

typedef enum 
{
    MASK_SIMPLE_TTS_ALL_ON = -1,
    MASK_SIMPLE_TTS_ALL_OFF,
#ifdef __MMI_STTS_TIME__
    MASK_SIMPLE_TTS_TIME = 1 << ID_SIMPLE_TTS_TIME,
#endif /*__MMI_STTS_TIME__*/
#ifdef __MMI_STTS_DIAL_KEY__
    MASK_SIMPLE_TTS_DIAL_KEY = 1 << ID_SIMPLE_TTS_DIAL_KEY,
#endif /*__MMI_STTS_DIAL_KEY__*/
#ifdef __MMI_STTS_INCOMING_CALL__
    MASK_SIMPLE_TTS_INCOMING = 1 << ID_SIMPLE_TTS_INCOMING,
#endif /*__MMI_STTS_INCOMING_CALL__*/
#ifdef __MMI_STTS_PHB_NUMBER__
    MASK_SIMPLE_TTS_PHB_NUM = 1 << ID_SIMPLE_TTS_PHB_NUM,
#endif /*__MMI_STTS_PHB_NUMBER__*/
#ifdef __MMI_STTS_STRIKE_THE_HOUR__
    MASK_SIMPLE_TTS_STRIKE_THE_HOUR = 1 << ID_SIMPLE_TTS_PHB_NUM,
#endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
} SimpleTtsMaskEnumType;

typedef struct
{
    U8          m_nMask;
    U8          m_nMaskNv;
    MMI_BOOL    m_lDeftVal[ID_SIMPLE_TTS_TOTAL + 1];
} SimpleTtsNvramContextType;

#endif /*_SIMPLE_TTS_NVRAM_DEFS_H*/
