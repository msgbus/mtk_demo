#ifndef _SIMPLE_TTS_NVRAM_GPROT_H
#define _SIMPLE_TTS_NVRAM_GPROT_H

extern void srv_simple_tts_set_status(S32 nIndex, MMI_BOOL bValue);

extern MMI_BOOL srv_simple_tts_get_status(S32 nIndex);

extern void srv_simple_tts_nvram_write(void);
    
extern void srv_simple_tts_nvram_read(void);

#endif /*_SIMPLE_TTS_NVRAM_GPROT_H*/
