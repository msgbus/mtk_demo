#ifndef _SIMPLE_TTS_DEFS_H
#define _SIMPLE_TTS_DEFS_H

typedef enum 
{
#ifdef __MMI_STTS_TIME__
    ID_SIMPLE_TTS_TIME,
#endif /*__MMI_STTS_TIME__*/
#ifdef __MMI_STTS_DIAL_KEY__
    ID_SIMPLE_TTS_DIAL_KEY,
#endif /*__MMI_STTS_DIAL_KEY__*/
#ifdef __MMI_STTS_INCOMING_CALL__
    ID_SIMPLE_TTS_INCOMING,
#endif /*__MMI_STTS_INCOMING_CALL__*/
#ifdef __MMI_STTS_PHB_NUMBER__
    ID_SIMPLE_TTS_PHB_NUM,
#endif /*__MMI_STTS_PHB_NUMBER__*/
#ifdef __MMI_STTS_STRIKE_THE_HOUR__
    ID_SIMPLE_TTS_STRIKE_THE_HOUR,
#endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
    ID_SIMPLE_TTS_TOTAL,
    ID_SIMPLE_TTS_START = 0
} SimpleTtsItemIndexEnumType;

#endif /*_SIMPLE_TTS_DEFS_H*/
