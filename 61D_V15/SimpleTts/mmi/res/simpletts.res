#include "MMI_features.h"
#include "custresdef.h"

<?xml version="1.0" encoding="UTF-8"?>
<APP id="APP_SIMPLETTS">

    <!--Include Area-->

    <!-----------------------------------------------------String Resource Area----------------------------------------------------->
    <STRING id="STR_ID_SIMPLE_TTS"/>
#ifdef __MMI_STTS_PHB_NUMBER__
    <STRING id="STR_ID_SIMPLE_TTS_PHB_NUM"/>
#endif /*__MMI_STTS_PHB_NUMBER__*/
#ifdef __MMI_STTS_DIAL_KEY__
    <STRING id="STR_ID_SIMPLE_TTS_DIAL_KEY"/>
#endif /*__MMI_STTS_DIAL_KEY__*/
#ifdef __MMI_STTS_INCOMING_CALL__
    <STRING id="STR_ID_SIMPLE_TTS_INCOMING_NUM"/>
#endif /*__MMI_STTS_INCOMING_CALL__*/
#ifdef __MMI_STTS_STRIKE_THE_HOUR__
    <STRING id="STR_ID_SIMPLE_TTS_STRIKE_THE_HOUR"/>
#endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
#ifdef __MMI_STTS_TIME__
    <STRING id="STR_ID_SIMPLE_TTS_TIME"/>
    <STRING id="STR_ID_SIMPLE_TTS_TIME_SWITCH"/>
    <STRING id="STR_ID_SIMPLE_TTS_TIME_DETAIL"/>
    <STRING id="STR_ID_SIMPLE_TTS_TIME_DETAIL_TEXT"/>
    <STRING id="STR_ID_SIMPLE_TTS_TIME_DETAIL_TEXT_8"/>
#ifdef __PROJ_T68__
	<STRING id="STR_ID_SIMPLE_TTS_WELCOME"/>
	<STRING id="STR_ID_SIMPLE_TTS_MINUTE"/>
#endif	/*__PROJ_T68__*/
#endif /*__MMI_STTS_TIME__*/


    <!-----------------------------------------------------Image Resource Area------------------------------------------------------>

    <!------------------------------------------------------Other Resource---------------------------------------------------------->
    <SCREEN id="GRP_ID_SIMPLE_TTS"/>
    <SCREEN id="SCR_ID_SIMPLE_TTS"/>
#ifdef __MMI_STTS_TIME__
    <SCREEN id="SCR_ID_SIMPLE_TTS_TIME_DETAIL"/>
#endif /*__MMI_STTS_TIME__*/

    <!------------------------------------------------------Menu Resource Area------------------------------------------------------>

    <MENU id="MENU_ID_SIMPLE_TTS"
        str="STR_ID_SIMPLE_TTS"
        highlight="mmi_simple_tts_main_highlight">
    #ifdef __MMI_STTS_PHB_NUMBER__
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_PHB_NUM</MENUITEM_ID>
    #endif /*__MMI_STTS_PHB_NUMBER__*/
    #ifdef __MMI_STTS_DIAL_KEY__
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_DIAL_NUM</MENUITEM_ID>
    #endif /*__MMI_STTS_DIAL_KEY__*/
    #ifdef __MMI_STTS_INCOMING_CALL__
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_INCOMING_NUM</MENUITEM_ID>
    #endif /*__MMI_STTS_INCOMING_CALL__*/
    #ifdef __MMI_STTS_STRIKE_THE_HOUR__
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_STRIKE_THE_HOUR</MENUITEM_ID>
    #endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
    </MENU>
#ifdef __MMI_STTS_PHB_NUMBER__
    <MENUITEM id="MENU_ID_SIMPLE_TTS_PHB_NUM" str="STR_ID_SIMPLE_TTS_PHB_NUM"/>
#endif /*__MMI_STTS_PHB_NUMBER__*/
#ifdef __MMI_STTS_DIAL_KEY__
    <MENUITEM id="MENU_ID_SIMPLE_TTS_DIAL_NUM" str="STR_ID_SIMPLE_TTS_DIAL_KEY"/>
#endif /*__MMI_STTS_DIAL_KEY__*/
#ifdef __MMI_STTS_INCOMING_CALL__
    <MENUITEM id="MENU_ID_SIMPLE_TTS_INCOMING_NUM" str="STR_ID_SIMPLE_TTS_INCOMING_NUM"/>
#endif /*__MMI_STTS_INCOMING_CALL__*/
#ifdef __MMI_STTS_STRIKE_THE_HOUR__
    <MENUITEM id="MENU_ID_SIMPLE_TTS_STRIKE_THE_HOUR" str="STR_ID_SIMPLE_TTS_STRIKE_THE_HOUR"/>
#endif /*__MMI_STTS_STRIKE_THE_HOUR__*/

#ifdef __MMI_STTS_TIME__
    <MENU id="MENU_ID_SIMPLE_TTS_PLAY_TIME"
        str="STR_ID_SIMPLE_TTS_TIME"
        highlight="mmi_simple_tts_play_time_highlight">
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_PLAY_TIME_SWITCH</MENUITEM_ID>
        <MENUITEM_ID>MENU_ID_SIMPLE_TTS_PLAY_TIME_DETAIL</MENUITEM_ID>
    </MENU>
    <MENUITEM id="MENU_ID_SIMPLE_TTS_PLAY_TIME_SWITCH" str="STR_ID_SIMPLE_TTS_TIME_SWITCH"/>
    <MENUITEM id="MENU_ID_SIMPLE_TTS_PLAY_TIME_DETAIL" str="STR_ID_SIMPLE_TTS_TIME_DETAIL"/>
#endif /*__MMI_STTS_TIME__*/

    <CACHEDATA type="byte" id="NVRAM_SIMPLE_TTS_SWITCH" restore_flag="TRUE">
        <DEFAULT_VALUE> [0xFF] </DEFAULT_VALUE>
        <DESCRIPTION> Byte Cache </DESCRIPTION>
    </CACHEDATA>

</APP>
