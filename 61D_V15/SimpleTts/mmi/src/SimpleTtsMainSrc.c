#include "MMI_features.h"
#include "MMI_include.h"
#include "ProfilesSrvGprot.h"
#include "IdleAppResDef.h"
#include "MMIDataType.h"
#include "GlobalResDef.h"

#include "SimpleTtsDefs.h"
#include "mmi_rp_app_simpletts_def.h"
#include "SimpleTtsNvramGprot.h"
#include "SimpleTtsPlayGprot.h"
#include "SimpleTtsBaseGprot.h"

#include "mmi_rp_cui_dialer_def.h"
#include "BootupSrvGprot.h"

#define IS_FALSE_BREAK(_val)    if (!_val) break

#define IF_LENGTH_ERR_BREAK(_length)  if (_length < 0) break 

#ifdef __MMI_STTS_TIME__
static void mmi_simple_tts_play_time_detail_entry(void);
#endif /*__MMI_STTS_TIME__*/

static U16 gTtsHiliteMenuId = 0;

static const mmi_menu_id gMenuItemMap[ID_SIMPLE_TTS_TOTAL + 1] =
{
#ifdef __MMI_STTS_TIME__
    MENU_ID_SIMPLE_TTS_PLAY_TIME_SWITCH,
#endif /*__MMI_STTS_TIME__*/
#ifdef __MMI_STTS_DIAL_KEY__
    MENU_ID_SIMPLE_TTS_DIAL_NUM,
#endif /*__MMI_STTS_DIAL_KEY__*/
#ifdef __MMI_STTS_INCOMING_CALL__
    MENU_ID_SIMPLE_TTS_INCOMING_NUM,
#endif /*__MMI_STTS_INCOMING_CALL__*/
#ifdef __MMI_STTS_PHB_NUMBER__
    MENU_ID_SIMPLE_TTS_PHB_NUM,
#endif /*__MMI_STTS_PHB_NUMBER__*/
#ifdef __MMI_STTS_STRIKE_THE_HOUR__
    MENU_ID_SIMPLE_TTS_STRIKE_THE_HOUR,
#endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
    0
};

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_play_time_detail_entry
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static MMI_BOOL mmi_simple_tts_get_status(mmi_menu_id nMenuId)
{
    S32 i;
    for (i = ID_SIMPLE_TTS_START; i < ID_SIMPLE_TTS_TOTAL; i ++)
    {
        if (gMenuItemMap[i] == nMenuId)
        {
            return srv_simple_tts_get_status(i);
        }
    }

    return MMI_FALSE;
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_play_time_detail_entry
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_simple_tts_set_status(mmi_menu_id nMenuId, MMI_BOOL bIsOn)
{
    S32 i;
    for (i = ID_SIMPLE_TTS_START; i < ID_SIMPLE_TTS_TOTAL; i ++)
    {
        if (gMenuItemMap[i] == nMenuId)
        {
            srv_simple_tts_set_status(i, bIsOn);
            srv_simple_tts_nvram_write();
            return ;
        }
    }
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_menu_item_highlight
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_simple_tts_menu_item_highlight(mmi_event_struct* evt)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    cui_menu_event_struct* menu_evt = (cui_menu_event_struct*)evt;
    MMI_BOOL bIsOn;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    switch (menu_evt->highlighted_menu_id)
    {
    #ifdef __MMI_STTS_TIME__
        case MENU_ID_SIMPLE_TTS_PLAY_TIME_DETAIL:
            cui_menu_change_left_softkey_string(
                menu_evt->sender_id,
                (WCHAR*)GetString(STR_GLOBAL_OK));
            break;
    #endif /*__MMI_STTS_TIME__*/
        default:
            bIsOn = mmi_simple_tts_get_status(menu_evt->highlighted_menu_id);
            cui_menu_change_left_softkey_string(
                menu_evt->sender_id,
                (WCHAR*)GetString(bIsOn == MMI_FALSE ? STR_GLOBAL_ON : STR_GLOBAL_OFF));
            cui_menu_set_item_hint(
                menu_evt->sender_id,
                menu_evt->highlighted_menu_id,
                (WCHAR*)GetString(bIsOn == MMI_FALSE ? STR_GLOBAL_OFF : STR_GLOBAL_ON));
            break;
    }
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_menu_item_select
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_simple_tts_menu_item_select(mmi_event_struct* evt)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
    cui_menu_event_struct* menu_evt = (cui_menu_event_struct*)evt;
    MMI_BOOL bIsOn;

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    switch(menu_evt->highlighted_menu_id)
    {
    #ifdef __MMI_STTS_TIME__
        case MENU_ID_SIMPLE_TTS_PLAY_TIME_DETAIL:
            mmi_simple_tts_play_time_detail_entry();
            break;
    #endif /*__MMI_STTS_TIME__*/
        default:
            bIsOn = mmi_simple_tts_get_status(menu_evt->highlighted_menu_id) == MMI_TRUE ? MMI_FALSE : MMI_TRUE;
            mmi_simple_tts_set_status(menu_evt->highlighted_menu_id, bIsOn);
            cui_menu_change_left_softkey_string(
                menu_evt->sender_id,
                (WCHAR*)GetString(bIsOn == MMI_FALSE ? STR_GLOBAL_ON : STR_GLOBAL_OFF));
            cui_menu_set_item_hint(
                menu_evt->sender_id,
                menu_evt->highlighted_menu_id,
                (WCHAR*)GetString(bIsOn == MMI_FALSE ? STR_GLOBAL_OFF : STR_GLOBAL_ON));
            break;
    }
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_main_proc
 * DESCRIPTION
 *  Entry function for TTS settings Main Proc
 * PARAMETERS
 *  evt         mmi_event_struct *
 * RETURNS
 *  mmi_ret
 *****************************************************************************/
static mmi_ret mmi_simple_tts_main_proc(mmi_event_struct *evt)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/

    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    switch(evt->evt_id)
    {
        case EVT_ID_GROUP_DEINIT:
        {
            break;
        }
        
        case EVT_ID_CUI_MENU_ITEM_SELECT:
        case EVT_ID_CUI_MENU_ITEM_TAP:
        case EVT_ID_CUI_MENU_ITEM_CSK_SELECT:
        {
            mmi_simple_tts_menu_item_select(evt);
            break;
        }    

        case EVT_ID_CUI_MENU_CLOSE_REQUEST:
        {
            cui_menu_event_struct* menu_evt = (cui_menu_event_struct*)evt;
            cui_menu_close(menu_evt->sender_id);
            break;
        }
        
        case EVT_ID_CUI_MENU_ITEM_HILITE:
        {
            mmi_simple_tts_menu_item_highlight(evt);
            break;
        }
    }
    return MMI_RET_OK;
}


/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_main_entry_scrn_internal
 * DESCRIPTION
 *  Entry function for TTS settings
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
static void mmi_simple_tts_main_entry_scrn_internal(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/    
    mmi_id menu_gid;
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    menu_gid = cui_menu_create(
                    GRP_ID_SIMPLE_TTS, 
                    CUI_MENU_SRC_TYPE_RESOURCE, 
                    CUI_MENU_TYPE_APPMAIN, 
                    gTtsHiliteMenuId, 
                    MMI_TRUE, 
                    NULL);

    if (menu_gid == GRP_ID_INVALID)
    {
        return;
    }

    cui_menu_run(menu_gid);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_main_entry
 * DESCRIPTION
 *  Entry function for TTS settings
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_simple_tts_main_entry(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/    
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    mmi_frm_group_create_ex(
        GRP_ID_ROOT,
        GRP_ID_SIMPLE_TTS,
        mmi_simple_tts_main_proc,
        NULL,
        MMI_FRM_NODE_SMART_CLOSE_FLAG);

    mmi_simple_tts_main_entry_scrn_internal();
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_main_highlight
 * DESCRIPTION
 *  highlight function for TTS settings
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_simple_tts_main_highlight(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/    
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    gTtsHiliteMenuId = MENU_ID_SIMPLE_TTS;
    SetLeftSoftkeyFunction(mmi_simple_tts_main_entry, KEY_EVENT_UP);
    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
    SetCenterSoftkeyFunction(mmi_simple_tts_main_entry, KEY_EVENT_UP);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_init
 * DESCRIPTION
 *  init application
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_simple_tts_init(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/    
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    srv_simple_tts_nvram_read();
}

static MMI_BOOL mmi_simple_tts_is_play_allowed(S32 nIndex)
{
    MMI_BOOL l_bRet = MMI_FALSE;
    do
    {
        if (srv_simple_tts_get_status(nIndex) == MMI_FALSE)
        {
            break ;
        }

        if(srv_prof_is_profile_activated(SRV_PROF_SILENT_MODE)== SRV_PROF_RET_PROFILE_ACTIVATED
            || srv_prof_is_profile_activated(SRV_PROF_MEETING_MODE)== SRV_PROF_RET_PROFILE_ACTIVATED)
        {
            break ;
        }

        if (srv_bootup_get_booting_mode() != SRV_BOOTUP_MODE_NORMAL)
        {
            break ;
        }

        l_bRet = MMI_TRUE;
    } while(0);

    return l_bRet;
}

#if defined(__MMI_STTS_STRIKE_THE_HOUR__) || defined(__MMI_STTS_TIME__)

static S8 *mmi_simple_tts_get_current_time_string(S8 *strTime, U32 nLength)
{
    MYTIME l_tCurr;
    S32 l_nLength = nLength;
    S8 l_strTemp[2] = {'\0'};
    U8 l_nTemp;

    do
    {
        if (strTime == NULL)
        {
            break ;
        }

        GetDateTime(&l_tCurr);

        srv_simple_tts_strncpy(strTime, "T", l_nLength);
        IF_LENGTH_ERR_BREAK(--l_nLength);

        l_nTemp = l_tCurr.nHour / 10;

        if (l_nTemp == 2)
        {
            srv_simple_tts_strncat(strTime, "2", l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }

        if (l_nTemp > 0)
        {
            srv_simple_tts_strncat(strTime, "S", l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }

        l_nTemp = l_tCurr.nHour % 10;

        if (l_nTemp != 0)
        {
            l_strTemp[0] = l_nTemp + '0';
            srv_simple_tts_strncat(strTime, l_strTemp, l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }
        else if (l_tCurr.nHour % 24 == 0)
        {
            srv_simple_tts_strncat(strTime, "0", l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }

        srv_simple_tts_strncat(strTime, "D", l_nLength);
        IF_LENGTH_ERR_BREAK(--l_nLength);

        if (l_tCurr.nMin == 0)
        {
            srv_simple_tts_strncat(strTime, "Z", l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }
        else
        {
            l_nTemp = l_tCurr.nMin / 10;

            if (l_nTemp != 1)
            {
                l_strTemp[0] = l_nTemp + '0';
                srv_simple_tts_strncat(strTime, l_strTemp, l_nLength);
                IF_LENGTH_ERR_BREAK(--l_nLength);
            }

            if (l_nTemp > 0)
            {
                srv_simple_tts_strncat(strTime, "S", l_nLength);
                IF_LENGTH_ERR_BREAK(--l_nLength);
            }

            l_nTemp = l_tCurr.nMin % 10;
            
            l_strTemp[0] = l_nTemp + '0';
            srv_simple_tts_strncat(strTime, l_strTemp, l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);

            srv_simple_tts_strncat(strTime, "F", l_nLength);
            IF_LENGTH_ERR_BREAK(--l_nLength);
        }
    } while (0);
    
    return strTime;
}

void mmi_simple_tts_play_time(void)
{
    S8 l_strTime[] = "T2S3D5S9F";
    srv_simple_tts_set_play_cbfunc(NULL);
    srv_simple_tts_play_string(
        mmi_simple_tts_get_current_time_string(l_strTime, sizeof(l_strTime)),
        srv_simple_tts_common_cb);

#ifdef __PROJ_T68__
	{
	WCHAR str[32]={0};
	WCHAR out[32]={0};
	char temp[32]={0};
	applib_time_struct time;
	
	extern MMI_ID mmi_popup_display_simple(
        WCHAR* title, 
        mmi_event_notify_enum event_type, 
        MMI_ID parent_id,
        void * user_tag);
	extern void applib_dt_get_date_time(applib_time_struct *t);
	
	applib_dt_get_date_time(&time);

	sprintf(temp,"%s",get_string(STR_ID_SIMPLE_TTS_WELCOME));
	app_ucs2_strncat(out,temp);

	memset(temp,0,sizeof(temp));	
	sprintf(temp,"%02d:%02d",time.nHour,time.nMin);
	app_widechar_ansii_to_unicode(str,temp);
	app_ucs2_strcat(out,str);
	
	mmi_popup_display_simple(out, MMI_EVENT_PROGRESS, GRP_ID_ROOT, NULL);

	}
#endif

}

#endif /*defined(__MMI_STTS_STRIKE_THE_HOUR__) || defined(__MMI_STTS_TIME__)*/

#if defined(__MMI_STTS_STRIKE_THE_HOUR__)

MMI_BOOL mmi_simple_tts_is_strike_the_hour_enable(void)
{
    return mmi_simple_tts_is_play_allowed(ID_SIMPLE_TTS_STRIKE_THE_HOUR);
}

#endif /*defined(__MMI_STTS_STRIKE_THE_HOUR__)*/

#ifdef __MMI_STTS_TIME__

MMI_BOOL mmi_simple_tts_is_play_time_enable(void)
{
    return mmi_simple_tts_is_play_allowed(ID_SIMPLE_TTS_TIME);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_main_highlight
 * DESCRIPTION
 *  highlight function for TTS settings
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_simple_tts_play_time_highlight(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/    
    
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    gTtsHiliteMenuId = MENU_ID_SIMPLE_TTS_PLAY_TIME;
    SetLeftSoftkeyFunction(mmi_simple_tts_main_entry, KEY_EVENT_UP);
    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
    SetCenterSoftkeyFunction(mmi_simple_tts_main_entry, KEY_EVENT_UP);
}

/*****************************************************************************
 * FUNCTION
 *  mmi_simple_tts_play_time_detail_entry
 * DESCRIPTION
 *  
 * PARAMETERS
 *  void
 * RETURNS
 *  void
 *****************************************************************************/
void mmi_simple_tts_play_time_detail_entry(void)
{
    /*----------------------------------------------------------------*/
    /* Local Variables                                                */
    /*----------------------------------------------------------------*/
#if defined(__MMI_F168__) || defined(__MMI_B68_MOFUT__)
    U8* message = (PU8) GetString(STR_ID_SIMPLE_TTS_TIME_DETAIL_TEXT_8);
#else		
    U8* message = (PU8) GetString(STR_ID_SIMPLE_TTS_TIME_DETAIL_TEXT);
#endif
    /*----------------------------------------------------------------*/
    /* Code Body                                                      */
    /*----------------------------------------------------------------*/
    if (mmi_frm_scrn_enter(
            GRP_ID_ROOT, 
            SCR_ID_SIMPLE_TTS_TIME_DETAIL, 
            NULL, 
            mmi_simple_tts_play_time_detail_entry, 
            MMI_FRM_FULL_SCRN) != MMI_TRUE)
    {
        return ;
    }
    
    ShowCategory7Screen(
         STR_ID_SIMPLE_TTS_TIME_DETAIL,
         NULL,//IMG_ID_FM_AUTO_TEST_ROOT,
         NULL,
         NULL,
         STR_GLOBAL_BACK,
         IMG_GLOBAL_BACK,
         message,
         NULL);

    SetRightSoftkeyFunction(mmi_frm_scrn_close_active_id, KEY_EVENT_UP);
}

#endif /*__MMI_STTS_TIME__*/

#ifdef __MMI_STTS_DIAL_KEY__

MMI_BOOL mmi_simple_tts_is_dial_key_enable(void)
{
    MMI_BOOL l_bRet = MMI_FALSE;

    do
    {
        MMI_ID l_nScrId = mmi_frm_scrn_get_active_id();

        if (SCR_ID_CUI_DIALER_MAIN != l_nScrId)
        {
            break ;
        }

        l_bRet = mmi_simple_tts_is_play_allowed(ID_SIMPLE_TTS_DIAL_KEY);
    } while (0);
    
    return l_bRet;
}

void mmi_simple_tts_play_dial_key(U8 nChar)
{
    if (mmi_simple_tts_is_dial_key_enable() && mmi_phb_check_ready(MMI_FALSE))
    {
        srv_simple_tts_play_char(nChar, NULL);
    }
}

#endif /*__MMI_STTS_DIAL_KEY__*/

#ifdef __MMI_STTS_INCOMING_CALL__
MMI_BOOL mmi_simple_tts_playing_incoming = MMI_FALSE;
MMI_BOOL mmi_simple_tts_play_incoming_call(const S8 *pNumber, FuncPtr cb)
{
    if (mmi_simple_tts_is_play_allowed(ID_SIMPLE_TTS_INCOMING))
    {
        extern void (*gSimpleTtsPlayCallbackFunc)();
        if (gSimpleTtsPlayCallbackFunc != cb)
        {
            mmi_simple_tts_playing_incoming = MMI_TRUE;
            srv_simple_tts_set_play_cbfunc(cb);
            srv_simple_tts_play_string(pNumber, srv_simple_tts_common_cb);
            mmi_simple_tts_playing_incoming = MMI_FALSE;
        }
        return MMI_TRUE;
    }

    return MMI_FALSE;
}

void mmi_simple_tts_stop_incoming_call(void)
{
    srv_simple_tts_set_play_cbfunc(NULL);
    srv_simple_tts_stop_string();
}

#endif /*__MMI_STTS_INCOMING_CALL__*/

#ifdef __MMI_STTS_PHB_NUMBER__

static U8 simple_tts_play_flag=0;

void mmi_simple_tts_play_phb_number(const S8 *pNumber)
{
    if (!mmi_simple_tts_is_play_allowed(ID_SIMPLE_TTS_PHB_NUM))
    {
        return ;
    }

	if(simple_tts_play_flag)
	{
  		srv_simple_tts_stop_string();//gary test
	}

	simple_tts_play_flag=1;
	 
    srv_simple_tts_play_string(pNumber, NULL);
}

void mmi_simple_tts_stop_phb_number(void)
{
	simple_tts_play_flag=0;
    srv_simple_tts_stop_string();
}

#endif /*__MMI_STTS_PHB_NUMBER__*/

void srv_simple_tts_profile_switched(void)
{
    MMI_BOOL enable = MMI_TRUE;
    
    if(srv_prof_is_profile_activated(SRV_PROF_SILENT_MODE)== SRV_PROF_RET_PROFILE_ACTIVATED
        || srv_prof_is_profile_activated(SRV_PROF_MEETING_MODE)== SRV_PROF_RET_PROFILE_ACTIVATED)
    {
        enable = MMI_FALSE;
    }
    
#ifdef __MMI_STTS_TIME__
    srv_simple_tts_set_status(ID_SIMPLE_TTS_TIME, enable);
#endif

#ifdef __MMI_STTS_DIAL_KEY__
    srv_simple_tts_set_status(ID_SIMPLE_TTS_DIAL_KEY, enable);
#endif

#ifdef __MMI_STTS_INCOMING_CALL__
    srv_simple_tts_set_status(ID_SIMPLE_TTS_INCOMING, enable);
#endif

#ifdef __MMI_STTS_PHB_NUMBER__
    srv_simple_tts_set_status(ID_SIMPLE_TTS_PHB_NUM, enable);
#endif

    srv_simple_tts_nvram_write();
}

