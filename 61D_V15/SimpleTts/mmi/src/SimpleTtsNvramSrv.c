#include "MMI_features.h"
#include "MMI_include.h"

#include "SimpleTtsNvramDefs.h"
#include "mmi_rp_app_simpletts_def.h"

static SimpleTtsNvramContextType gCntx =
{
    0xFF,
    0xFF,
    {
    #ifdef __MMI_STTS_TIME__
        MMI_TRUE,
    #endif /*__MMI_STTS_TIME__*/
    #ifdef __MMI_STTS_DIAL_KEY__
        MMI_TRUE,
    #endif /*__MMI_STTS_DIAL_KEY__*/
    #ifdef __MMI_STTS_INCOMING_CALL__
        MMI_TRUE,
    #endif /*__MMI_STTS_INCOMING_CALL__*/
    #ifdef __MMI_STTS_PHB_NUMBER__
        MMI_TRUE,
    #endif /*__MMI_STTS_PHB_NUMBER__*/
    #ifdef __MMI_STTS_STRIKE_THE_HOUR__
        MMI_TRUE,
    #endif /*__MMI_STTS_STRIKE_THE_HOUR__*/
        MMI_FALSE   /* No use, only for complie */
    }
};

void srv_simple_tts_set_status(S32 nIndex, MMI_BOOL bValue)
{
    if (bValue)
    {
        gCntx.m_nMask |= 1 << nIndex;
    }
    else
    {
        gCntx.m_nMask &= ~(1 << nIndex);
    }
}

MMI_BOOL srv_simple_tts_get_status(S32 nIndex)
{
    return (gCntx.m_nMask & (1 << nIndex)) != 0 ? MMI_TRUE : MMI_FALSE;
}

void srv_simple_tts_nvram_write(void)
{
    S16 l_nErr;

    if (gCntx.m_nMaskNv != gCntx.m_nMask)
    {
        gCntx.m_nMaskNv = gCntx.m_nMask;
        WriteValue(NVRAM_SIMPLE_TTS_SWITCH, &gCntx.m_nMaskNv, DS_BYTE, &l_nErr);
    }
}

void srv_simple_tts_nvram_read(void)
{
    S16 l_nErr;
    int i;
    ReadValue(NVRAM_SIMPLE_TTS_SWITCH, &gCntx.m_nMaskNv, DS_BYTE, &l_nErr);
    gCntx.m_nMask = gCntx.m_nMaskNv;
    if (NVRAM_READ_SUCCESS != l_nErr || 0xFF == gCntx.m_nMaskNv)
    {
        for (i = 0; i < ID_SIMPLE_TTS_TOTAL + 1;i ++)
        {
            srv_simple_tts_set_status(i, gCntx.m_lDeftVal[i]);
        }
        srv_simple_tts_nvram_write();
    }
}
