SRC_LIST = winguo_v4\winguo_ultra\src\GB_WG_ultra_app.c \
           winguo_v4\winguo_ultra\src\GB_WG_ultra_SerchScreen.c

# Define include path lists to INC_DIR

INC_DIR = plutommi\Service\SimCtrlSrv \
          plutommi\Service\UmmsSrv \
          plutommi\CUI\Inc \
          plutommi\Framework\BIDI\BIDIInc \
          plutommi\Service\SmsSrv \
          plutommi\mmi\ucm\ucminc  \
          plutommi\Framework\IndicLanguages\IndicLanguagesInc \
	  plutommi\mmi\browserapp\browser\browserinc \
	  plutommi\service\browsersrv

SRC_PATH = winguo_v4\winguo_ultra\src
