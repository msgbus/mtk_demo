INC_DIR +=  simpletts\core\inc \
            simpletts\mmi\inc 

SRC_PATH += simpletts\core\src \
            simpletts\mmi\src 

SRC_LIST += simpletts\core\src\SimpleTtsAudioFactory.c \
            simpletts\core\src\SimpleTtsPlaySrv.c \
            simpletts\core\src\SimpleTtsBaseSrv.c \
            simpletts\mmi\src\SimpleTtsMainSrc.c \
            simpletts\mmi\src\SimpleTtsNvramSrv.c 
            
